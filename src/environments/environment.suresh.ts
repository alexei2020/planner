export const environment = {
    production: false,
    endPoint: 'https://wsoauthproxy.default.172-16-11-151.nip.io:32443/api/v1/',
    baseUrl: `https://wsoauthproxy.default.172-16-11-151.nip.io:32443/`,
    promeUrl: 'https://wsoauthproxy.default.172-16-11-151.nip.io:32443/',
    apiUrl: `https://wsoauthproxy.default.172-16-11-151.nip.io:32443`,
    searchUrl: `https://wsoauthproxy.default.172-16-11-151.nip.io:32443`,
    hmr: false
};
