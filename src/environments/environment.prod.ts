export const environment = {
    production: true,
    endPoint: `${window.location.protocol}//${window.location.hostname}:9090/api/v1/`,
    baseUrl: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/`,
    promeUrl: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/`,
    apiUrl: `${window.location.protocol}//${window.location.hostname}:${window.location.port}`,
    searchUrl: `${window.location.protocol}//${window.location.hostname}:${window.location.port}`,
    hmr: false
};
