import { Inject, Injectable, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from '@angular/router';
import { Platform } from '@angular/cdk/platform';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as _ from 'lodash';
import { environment } from 'environments/environment';
import { reject } from 'lodash';
import { promise } from 'protractor';

@Injectable({
    providedIn: 'root'
})

export class FuseSearchBarService {
    private _searchURI = environment.searchUrl + '/nlp/';// environment.searchUrl;
    // Private
    private _configSubject: BehaviorSubject<any>;
    private readonly _defaultConfig: any;
    public _searchTerm: string = '';
    public aggregatorStats: BehaviorSubject<any[]>;
    public filterChange: BehaviorSubject<boolean>;
    public isEntireAppMode: BehaviorSubject<boolean>;
    public tableSearchTerm: BehaviorSubject<string>;
    constructor(public router: Router, private http: HttpClient) {
        this.aggregatorStats = new BehaviorSubject([]);
        this.filterChange = new BehaviorSubject(false);
        this.isEntireAppMode = new BehaviorSubject(false);
        this.tableSearchTerm = new BehaviorSubject('');
        this._defaultConfig = {
            didExpand: false
        }

        this._init();
    }

    private _init(): void {
        // Set the config from the default config
        this._configSubject = new BehaviorSubject(_.cloneDeep(this._defaultConfig));
    }

    set config(value) {
        // Get the value from the behavior subject
        let config = this._configSubject.getValue();

        // Merge the new config
        config = _.merge({}, config, value);

        // Notify the observers
        this._configSubject.next(config);
    }

    get config(): any | Observable<any> {
        return this._configSubject.asObservable();
    }

    get defaultConfig(): any {
        return this._defaultConfig;
    }

    search(text: string, loc: string, locName: string, tenantName: string) {
        console.log("search method", text);
        if (text && text.length > 0) {
            this._searchTerm = text;//keyword
            this.router.navigate(['/search-results', text], {
              queryParams: {
                loc: locName,
                tenant: tenantName
              },
            });
        }
    }

    getNlpData(keyword: string, loc: string = null) {
        return new Promise((resolve, reject) => {
            let url = "";
            if(loc)
                url = this._searchURI + loc + "/v1/request?query=" + keyword;
            else
                url = keyword;
            
            this.http.get(url).toPromise().then(res => resolve(res)).catch(err => reject(err));
        });
    }
    onFilterChanged(){
        this.filterChange.next(true);
    }

}
