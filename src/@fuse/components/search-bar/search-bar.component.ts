import { Component, EventEmitter, OnDestroy, OnInit, Output, ElementRef, ViewChild, HostListener, Renderer2, ChangeDetectorRef } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

import { Subject } from 'rxjs';
import { takeUntil, take, distinctUntilChanged } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSearchBarService } from '@fuse/services/search-bar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FuseLocationService } from 'app/services/location.service';
import { AnyAaaaRecord } from 'dns';

@Component({
    selector: 'fuse-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class FuseSearchBarComponent implements OnInit, OnDestroy {
    @ViewChild('filterBtn') public filterBtn: ElementRef;
    @ViewChild('filterPopup', { read: ElementRef }) public filterPopup: ElementRef
    @ViewChild('searchInput') public searchInput: ElementRef;
    @ViewChild('fuseSearchBar') public fuseSearchBar: ElementRef;
    @ViewChild('filterWrapper') public filterWrapper: ElementRef;

    public listItems = [
        { value: 'GLOBAL', name: 'Global' },
        { value: 'THIS_PAGE', name: 'This page' },
    ];

    fuseConfig: any;

    public showFilter: boolean;
    public aggregatorStats: any[];
    public isEntireAppMode: boolean = true;
    public show: boolean;
    public entireAppMode = { value: 'GLOBAL', name: 'Global' };

    model: string = null;
    location: string;
    locationName: string;
    tenantName: string;

    showSearchRangeWidget: boolean = false;
    selectedFilterItems: number = 0;
    @Output()
    input: EventEmitter<any>;

    @Output() isSearching: EventEmitter<boolean>;

    // Private
    private _unsubscribeAll: Subject<any>;
    private searchTextTimer: any;
    private searchText: string;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _searchBarService: FuseSearchBarService,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer,
        private activatedRoute: ActivatedRoute,
        private _locationService: FuseLocationService,
        private router: Router,
        private renderer: Renderer2,
        private cd: ChangeDetectorRef
    ) {

        iconRegistry.addSvgIcon('toolbar-search', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/toolbar/search.svg'));
        iconRegistry.addSvgIcon('toolbar-filter', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/toolbar/filter.svg'));
        iconRegistry.addSvgIcon('toolbar-close', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/toolbar/close.svg'));
        // Set the defaults
        this.input = new EventEmitter();
        this.isSearching = new EventEmitter(false);
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._searchBarService.aggregatorStats.pipe(takeUntil(this._unsubscribeAll)).subscribe(res => {
            this.aggregatorStats = res;
        })
        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                (config) => {
                    this.fuseConfig = config;
                }
            );
        this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(queryParams => {
            if (!queryParams['loc']) {
                return;
            }
            this.location = this._locationService.locationDataAsKeyValue().currentLocation;
            this.locationName = this._locationService.locationDataAsKeyValue().locationName;
            this.tenantName = this._locationService.locationDataAsKeyValue().tenantName;
        });
        this.router.events.subscribe((ev) => {
            this.selectedFilterItems = 0;
            // reset search bar state
            if (!this.isEntireAppMode) {
                // moving out of 'this page' mode
                this.showFilter = false;
                this.searchText = "";
                this.searchInput.nativeElement.value = "";
                this._searchBarService.tableSearchTerm.next(this.searchText);
                if (this.searchTextTimer) {
                    clearTimeout(this.searchTextTimer);
                }
            }
            this.isEntireAppMode = true;
            this.entireAppMode = { value: 'GLOBAL', name: 'Global' };
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this.renderer.removeClass(document.body, 'filtering');
        this.selectedFilterItems = 0;
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * onEnter
     */
    onEnter(): void {
        if (this.isEntireAppMode) {
            if (this.model && this.model.length > 0) {
                this._searchBarService.search(this.model, this.location, this.locationName, this.tenantName);        
            }
        } else {
            this._searchBarService.tableSearchTerm.next(this.searchText);
            if (this.searchTextTimer) {
                clearTimeout(this.searchTextTimer);
            }
            this.collapseToolbar();
            this.searchInput.nativeElement.blur();
        }
    }

    /**
     * Expand
     */
    expand(): void {
        this._searchBarService.config = { didExpand: true }
    }

    /**
     * Search
     *
     * @param event
     */
    search(event): void {
        this.searchText = event.target.value;
        this.model = this.searchText;
        // rate-limit sending searchText to components; searchText is sent only after a gap of 700ms
        // after the last keystroke
        if (this.searchTextTimer) {
            clearTimeout(this.searchTextTimer);
        }
        this.searchTextTimer = setTimeout(() => {
            if (!this.isEntireAppMode) {
                this._searchBarService.tableSearchTerm.next(this.searchText);
            }
            this.input.emit(this.searchText);
        }, 700);
    }

    toggleFilter() {
        this._searchBarService.config = { didExpand: true }
        if (this.model && this.model.length > 0 && this.isEntireAppMode)
            this._searchBarService.search(this.model, this.location, this.locationName, this.tenantName);

        if (this.showFilter){
            this.closeFilter();
            this.collapseToolbar();
        }
        else{
            this.openFilter();
        }
    }

    closeFilter() {
        this.showFilter = false;
        this.moveCanvas(false);
    }

    openFilter() {
        if (this.aggregatorStats.length) {
            this.showFilter = true;
            this.moveCanvas(true);
        }
        else {
            this.showFilter = false;
            this.moveCanvas(false);
        }
    }

    updateFilters() {
        this._searchBarService.onFilterChanged();
    }

    updateFilterDirection(event) {
        this.entireAppMode = event;
        this.isEntireAppMode = event.value === 'GLOBAL';
        if (this.isEntireAppMode) {
            this._searchBarService.isEntireAppMode.next(event);
            this.closeFilter();
        }
        // reset search-box
        this.searchText = "";
        this.searchInput.nativeElement.value = "";
        this._searchBarService.tableSearchTerm.next(this.searchText);
        if (this.searchTextTimer) {
            clearTimeout(this.searchTextTimer);
        }
    }

    public onToggleDropdown(): void {
        this.show = !this.show;
    }
    public close(): void {
        this.show = false;
    }
    onSearchLeaved(){
        
    }
    onSearchEntered(){
        this.isSearching.emit(true);
        this.showSearchRangeWidget = true;
    }
    @HostListener("document:click", ["$event"])
    public documentClick(event: any): void {
        // k-icon k-i-close-circle
        if(!this.didSearchBarClick(event.target)){
            if(!this.didSearchModeBtnClick(event.target))
                this.collapseToolbar();
            else
                this.searchInput.nativeElement.focus();
        }
    }
    private didSearchBarClick(target: any): boolean {
        return this.fuseSearchBar.nativeElement.contains(target) || 
            (this.filterPopup ? this.filterPopup.nativeElement.contains(target) : false) || 
            (this.filterPopup ? this.filterPopup.nativeElement.contains(target) : false) ||
            target.classList.contains("k-i-close-circle")
    }
    private didSearchModeBtnClick(target: any): boolean{
        return target.classList.contains("search-mode-item")
    }
    collapseToolbar(){
        this.isSearching.emit(false);
        this.showSearchRangeWidget = false;
        this.show = false;
        this.showFilter = false;
        this.searchInput.nativeElement.value = "";
        this.searchText = "";
        this.moveCanvas(false);
    }
    selectedItems(num: number){
        this.selectedFilterItems = num;
    }
    moveCanvas(move: boolean){
        const el = document.querySelector('.container-content');
        setTimeout(() => {
            let height = this.filterPopup ? this.filterPopup.nativeElement.offsetHeight : 0;    
            console.log(height);
            if(move){
                this.renderer.setStyle(el, 'margin-top', `${height}px`);
            } else {
                this.renderer.setStyle(el, 'margin-top', `${height}px`);
            }
        }, 100);

    }
}
