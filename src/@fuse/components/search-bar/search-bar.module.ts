import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { PopupModule } from '@progress/kendo-angular-popup';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { FuseSearchBarComponent } from './search-bar.component';
import { FuseSearchFilterComponent } from './search-filter/component'

@NgModule({
    declarations: [
        FuseSearchFilterComponent,
        FuseSearchBarComponent
    ],
    imports: [
        CommonModule,
        RouterModule,

        MatButtonModule,
        MatIconModule,

        ButtonsModule,
        TreeViewModule,
        PopupModule,
        DropDownsModule,
    ],
    exports: [
        FuseSearchBarComponent
    ]
})
export class FuseSearchBarModule
{
}
