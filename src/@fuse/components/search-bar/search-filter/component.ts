import { Component, Input, Output, OnInit, OnChanges, SimpleChanges, EventEmitter, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { TreeItemLookup } from '@progress/kendo-angular-treeview';
import _ from 'lodash';
import { ChipComponent } from '@progress/kendo-angular-buttons';

@Component({
    selector: 'fuse-search-filter',
    templateUrl: './component.html',
    styleUrls: ['./component.scss'],
})
export class FuseSearchFilterComponent implements OnInit, OnChanges, AfterViewInit {
    @Input() data;
    @Output() filterChange: any = new EventEmitter();
    @Output() selectedItems: any = new EventEmitter<number>();
    public checkedKeys: any[] = [];
    public expandedKeys: any[] = [];

    filterItems: any[] = [];
    public checkableSettings: CheckableSettings;
    constructor(private cd: ChangeDetectorRef){
        this.checkableSettings = {
            checkChildren: false,
            checkParents: false
        }
    }
    ngOnInit() {
        this.data.forEach((field, fieldIndex) => {
            this.expandedKeys.push(`${fieldIndex}`);
        });
        this.loadCheckedKeys();
    }
    ngAfterViewInit(){
        setTimeout(() => {
            this.selectedItems.emit(this.filterItems.length);    
        }, 100);
    }
    ngOnChanges(changes: SimpleChanges): void {
        // if (changes.data) {
        //     this.loadCheckedKeys();
        // }
    }

    private loadCheckedKeys(): void {
        this.checkedKeys = [];
        this.filterItems = [];
        this.data.forEach((field, fieldIndex) => {
            if(field.checked)
                this.checkedKeys.push(`${fieldIndex}`);
            field.keys.forEach((key, keyIndex) => {
                if (key.checked) {
                    this.checkedKeys.push(`${fieldIndex}_${keyIndex}`);

                    this.filterItems.push({
                        item: {
                            dataItem: {
                                ...key
                            },
                            index: `${fieldIndex}_${keyIndex}`
                        }
                    })
                }
            });
        });
        
        this.updateBadgeNumber();
    }

    public fetchChildren(node: any): Observable<any[]> {
        // Return the parent node's items collection as children
        return of(node.keys);
    }

    public hasChildren(node: any): boolean {
        // Check if the parent node has children
        return node.keys && node.keys.length > 0;
    }

    public handleChecking(itemLookup: TreeItemLookup): void {
        const hasKeys = !_.isEmpty(itemLookup.item.dataItem.keys);
        
        if (!hasKeys) {
            //if a child is clicked
            itemLookup.item.dataItem.checked = !itemLookup.item.dataItem.checked;
            //check parent if all children selected
            let canCheckParent = true;
            _.forEach(itemLookup.parent.item.dataItem.keys, (key) => {
                canCheckParent = key.checked && canCheckParent
            });
            //check if all children selected
            itemLookup.parent.item.dataItem.checked = canCheckParent;
        } else {
            //if parent is clicked
            let isChecked = itemLookup.item.dataItem.checked;
            if(isChecked === undefined)
                isChecked = false;
            itemLookup.item.dataItem.checked = !isChecked;
            // check all children if parent selected
            _.forEach(itemLookup.item.dataItem.keys, (key) => {
                key.checked = !isChecked;
            });
            
        }
        this.filterChange.emit();
        this.loadCheckedKeys();
    }
    onRemove({sender}){
        let removedItem: any = {};
        _.remove(this.filterItems, function(current){
            if(current.item.dataItem.keyName)
                if(current.item.dataItem.keyName == sender.label){
                    removedItem = current;
                    return true;

                } else return false;
            else
                if(current.item.dataItem.name == sender.label){
                    removedItem = current;
                    return true;
                } else return false;
        });
        //change data status
        this.data.forEach((field, fieldIndex) => {
            if(field.name === removedItem.item.dataItem.name)
                field.checked = false;
            field.keys.forEach((key, keyIndex) => {
                if(key.name === removedItem.item.dataItem.name)
                    key.checked = false;
                    //if one of its children is unchecked, parent is false
                    field.checked = false;
            });
        });
        this.filterChange.emit();
        this.loadCheckedKeys();
    }
    removeAllFilterItems(){
        this.filterItems = [];
        //change data status
        this.data.forEach((field, fieldIndex) => {
            field.checked = false;
            field.keys.forEach((key, keyIndex) => {
                key.checked = false;
            });
        });
        this.filterChange.emit();
        this.loadCheckedKeys();
    }
    updateBadgeNumber(){
        setTimeout(() => {
            this.selectedItems.emit(this.filterItems.length);    
        }, 0);
    }
    isChecked(dataItem: any, index: string){
        if(dataItem.keys !== undefined){
            let allChecked = true;
            let atLeastAchildIsChecked = false;
            dataItem.keys.forEach((key, keyIndex) => {
                atLeastAchildIsChecked = atLeastAchildIsChecked || key.checked;
                allChecked = allChecked && key.checked;
            });
            if(allChecked)
                return 'checked';
            else if(atLeastAchildIsChecked)
                return 'indeterminate';
            else
                return 'none';
        } else{
            if(dataItem.checked)
                return 'checked';
            else
                return 'none';
        }
    }
}
