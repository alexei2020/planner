import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { TranslateModule } from '@ngx-translate/core';
import { LayoutModule } from '@progress/kendo-angular-layout';

import { FuseNavigationComponent } from './navigation.component';
import { FuseNavVerticalItemComponent } from './vertical/item/item.component';
import { FuseNavVerticalCollapsableComponent } from './vertical/collapsable/collapsable.component';
import { FuseNavVerticalGroupComponent } from './vertical/group/group.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FuseDirectivesModule } from '@fuse/directives/directives';
import { MenuModule, ContextMenuModule } from '@progress/kendo-angular-menu';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,

        LayoutModule,
        MenuModule,
        ContextMenuModule,
        MatIconModule,
        MatRippleModule,
        MatMenuModule,
        BrowserModule,
        BrowserAnimationsModule,

        TranslateModule.forChild(),
        MatButtonModule,
        FuseDirectivesModule
    ],
    exports     : [
        FuseNavigationComponent
    ],
    declarations: [
        FuseNavigationComponent,
        FuseNavVerticalGroupComponent,
        FuseNavVerticalItemComponent,
        FuseNavVerticalCollapsableComponent
    ]
})
export class FuseNavigationModule
{
}
