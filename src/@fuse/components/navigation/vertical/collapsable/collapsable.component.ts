import { ChangeDetectorRef, Component, HostBinding, Input, OnDestroy, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { merge, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseNavigationItem } from '@fuse/types';
import { fuseAnimations } from '@fuse/animations';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

import { NavigationService } from '../../../../../app/services/navigation.service';
import { MenuEvent } from '@progress/kendo-angular-menu';

@Component({
    selector   : 'fuse-nav-vertical-collapsable',
    templateUrl: './collapsable.component.html',
    styleUrls  : ['./collapsable.component.scss'],
    animations : fuseAnimations
})
export class FuseNavVerticalCollapsableComponent implements OnInit, OnChanges, OnDestroy {
    @Input()
    item: FuseNavigationItem;

    @Input()
    menuLevel: number;

    @Output()
    menuItemSelected: EventEmitter<FuseNavigationItem>;

    @HostBinding('class')
    classes = 'nav-collapsable nav-item';

    menuItems: FuseNavigationItem[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {NavigationService} _navService
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {Router} _router
     */
    constructor(
        public _navService: NavigationService,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseNavigationService: FuseNavigationService
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.menuItemSelected = new EventEmitter();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Subscribe to navigation item
        merge(
            this._fuseNavigationService.onNavigationItemAdded,
            this._fuseNavigationService.onNavigationItemUpdated,
            this._fuseNavigationService.onNavigationItemRemoved
        ).pipe(takeUntil(this._unsubscribeAll))
         .subscribe(() => {

             // Mark for check
             this._changeDetectorRef.markForCheck();
         });
    }

    ngOnChanges(changes: SimpleChanges): void {
        const itemChanges = changes['item'];
        if (itemChanges && itemChanges.currentValue !== itemChanges.previousValue) {
            this.menuItems = this.getMenuItemsWithHeader();
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    
    onMenuItemSelected(event: MenuEvent): void {
        if (event) {
            event.preventDefault();
        }
        const item = event.item;
        
        this.menuItemSelected.next(item);
    }

    private getMenuItemsWithHeader(): FuseNavigationItem[] {
        const children = [{
                id: 'title',
                text: this.item.text,
                disabled: true
            },
            ...this.item.items
        ];

        const item = {
            ...this.item,
            items: children
        } as FuseNavigationItem;

        return [
            item
        ] as FuseNavigationItem[];
    }
}
