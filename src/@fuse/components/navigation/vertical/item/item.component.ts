import { ChangeDetectorRef, Component, HostBinding, Input, OnDestroy, OnInit, HostListener, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { merge, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseNavigationItem } from '@fuse/types';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FuseUtils } from '@fuse/utils';

@Component({
    selector   : 'fuse-nav-vertical-item',
    templateUrl: './item.component.html',
    styleUrls  : ['./item.component.scss'],
})
export class FuseNavVerticalItemComponent implements OnInit, OnDestroy
{
    @HostBinding('class')
    classes = 'nav-item';

    @Input()
    isIconOnly: boolean;

    @Input()
    item: FuseNavigationItem;

    @Input()
    isHoverEnabled: boolean;

    @Output()
    menuItemSelected: EventEmitter<FuseNavigationItem>;

    navigationList: string[] = [];

    // Private
    private _unsubscribeAll: Subject<any>;
    private isOpened = false;
    private readonly defaultScrollPosition = 0.8;
    private readonly minScrollDiff = 0.0001;
    private hoverSubscription: NodeJS.Timeout;

    /**
     * Constructor
     */
    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private fuseNavigationService: FuseNavigationService,
        private seltElement: ElementRef
    )
    {
        this.menuItemSelected = new EventEmitter();
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    ngOnInit(): void {
        // Get all ids of first level of menu
        this.navigationList = this.fuseNavigationService.getCurrentNavigation().map(item => item.id);

        // Subscribe to navigation item
        merge(
            this.fuseNavigationService.onNavigationItemAdded,
            this.fuseNavigationService.onNavigationItemUpdated,
            this.fuseNavigationService.onNavigationItemRemoved
        ).pipe(takeUntil(this._unsubscribeAll))
         .subscribe(() => {

             // Mark for check
             this.changeDetectorRef.markForCheck();
        });
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onMouseEnter(event: MouseEvent): void {
        if (!this.isIconOnly || this.isOpened || !this.isHoverEnabled) {
            return;
        }
        this.isOpened = true;

        const menuItemDiv = event.currentTarget as HTMLElement;
        this.hoverSubscription = setTimeout(() => {
            const top = FuseUtils.getTopOffset(menuItemDiv);

            const dummyDiv = this.createDummyDiv(menuItemDiv.clientWidth, menuItemDiv.clientHeight);
            const absoluteParentDiv = this.createAbsoluteItemParentDiv(top);
            this.seltElement.nativeElement.removeChild(menuItemDiv);
            absoluteParentDiv.appendChild(menuItemDiv);

            absoluteParentDiv.addEventListener('scroll', (scrollEvent: Event) => this.absoluteParentScroll(scrollEvent));

            this.seltElement.nativeElement.appendChild(dummyDiv);
            document.body.appendChild(absoluteParentDiv);
            absoluteParentDiv.scrollTop = this.defaultScrollPosition;
        }, 10);
    }

    onMouseLeave(event: MouseEvent): void {
        clearTimeout(this.hoverSubscription);
        if (!this.isIconOnly || !this.isOpened) {
            return;
        }
        const menuDiv = event.currentTarget as HTMLElement;
        this.detachMenu(menuDiv);
    }

    onItemClick(event: Event): void {
        if (!this.isIconOnly) {
            return;
        }
        const menuDiv = event.currentTarget as HTMLElement;
        this.detachMenu(menuDiv);

        this.menuItemSelected.next(this.item);
    }

    private absoluteParentScroll(scrollEvent: Event): void {
        const element = scrollEvent.currentTarget as HTMLElement;

        const isScrollAction = Math.abs(element.scrollTop - this.defaultScrollPosition) > this.minScrollDiff;
        if (!isScrollAction) {
            return;
        }

        const scrollableParent = FuseUtils.getScrollableParent(this.seltElement.nativeElement);

        const heightDiff = scrollableParent.scrollHeight - scrollableParent.offsetHeight;
        const scrollDiff = Math.max(heightDiff * 0.05, 10);

        const isScrollDown = element.scrollTop < this.defaultScrollPosition;
        scrollableParent.scrollTop = (isScrollDown) ? 
            Math.max(scrollableParent.scrollTop - scrollDiff, 0) :
            Math.max(scrollableParent.scrollTop + scrollDiff, heightDiff);

        this.detachMenu(element.children[0] as HTMLElement);       
        element.scrollTop = this.defaultScrollPosition; // restore default scroll
    }

    private detachMenu(menuDiv: HTMLElement): void {
        if (!menuDiv) {
            return;
        }

        const parentMenuDiv = menuDiv.parentElement;
        if (!parentMenuDiv) {
            this.isOpened = false;
            return;
        }

        const parent = this.seltElement.nativeElement;
        const dummyDiv = parent.children[0];

        try {
            parent.removeChild(dummyDiv);
            parent.appendChild(menuDiv);
            document.body.removeChild(parentMenuDiv);
        } catch (e) {
            // ignore error
        }
        this.isOpened = false;
    }

    private createDummyDiv(clientWidth: number, clientHeight: number): HTMLElement {
        const dummyDiv = document.createElement('div');
        dummyDiv.className= 'fuse-nav-item';
        dummyDiv.style.width = `${clientWidth}px`;
        dummyDiv.style.height = `${clientHeight}px`;

        return dummyDiv;
    }

    private createAbsoluteItemParentDiv(top: number): HTMLElement {
        const absoluteParentDiv = document.createElement('div');
        absoluteParentDiv.className = 'fuse-nav-item-absolute';
        absoluteParentDiv.style.top = `${top}px`;

        return absoluteParentDiv;
    }
}
