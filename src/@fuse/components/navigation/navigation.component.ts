import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild, OnDestroy, ElementRef, AfterViewInit } from '@angular/core';
import { merge, Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

import { NavigationService } from '../../../app/services/navigation.service';
import { PermissionsService } from 'app/services/permissions.service';
import { FuseNavigationItem, FuseMenuType } from '@fuse/types';
import { DrawerSelectEvent, DrawerComponent } from '@progress/kendo-angular-layout';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import _ from 'lodash';
import { LessStencilFunc } from 'three';

@Component({
    selector       : 'fuse-navigation',
    templateUrl    : './navigation.component.html',
    styleUrls      : ['./navigation.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FuseNavigationComponent implements OnInit, OnDestroy
{
    readonly FuseMenuTypes = FuseMenuType;
    @ViewChild('drawer') drawer: DrawerComponent;

    @Input()
    layout = 'vertical';

    navigation: FuseNavigationItem[];
    contextMenuItems: FuseNavigationItem[];

    selected: FuseNavigationItem;
    isMenuExpanded = false;
    menuState: FuseMenuType;
    scrollTop: number;

    // Private
    private _unsubscribeAll: Subject<any>;

    get FuseMenuType(): typeof FuseMenuType {
        return FuseMenuType;
    }

    constructor(
        private _navService: NavigationService,
        private _permService: PermissionsService,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseNavigationService: FuseNavigationService,
        private router: Router,
        private route: ActivatedRoute
    )
    {
        this.scrollTop = 0;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit(): void {
        this.menuState = this._navService.menuState;
        this.isMenuExpanded = this.menuState === FuseMenuType.IconWithText || this.menuState === FuseMenuType.Overlapping;

        this._navService.menuStateToggled
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((menuState: FuseMenuType) => {
                this.toggleMenu(menuState);
            });
        // Load the navigation either from the input or from the service
        // DS: it's a bad practice to set input parameter inside a control, create another Input variable if required
        this.navigation = this._fuseNavigationService.getCurrentNavigation();

        // Subscribe to the current navigation changes
        this._fuseNavigationService.onNavigationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                // Load the navigation
                this.navigation = this._fuseNavigationService.getCurrentNavigation();
                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Subscribe to navigation item
        merge(
            this._fuseNavigationService.onNavigationItemAdded,
            this._fuseNavigationService.onNavigationItemUpdated,
            this._fuseNavigationService.onNavigationItemRemoved
        ).pipe(takeUntil(this._unsubscribeAll))
         .subscribe(() => {
            // Load the navigation
            this.navigation = this.cloneWithFilter(this._fuseNavigationService.getCurrentNavigation());

             // Mark for check
             this._changeDetectorRef.markForCheck();
         });

        this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe((res) => {
                let navigation = _.cloneDeep(this.navigation);
                let path = window.location.hash.substr(1, window.location.hash.indexOf('?') - 1);
                if (path.lastIndexOf('/') > 2) {
                    path = path.substr(0, path.lastIndexOf('/'));
                }
                //
                navigation = _.cloneDeepWith(this.navigation, function(v ,k) {
                    if(k === 'selected'){
                        return false
                    }
                });
                this._fuseNavigationService.recursiveSelectNavigation(navigation, path);
                this.navigation = navigation;

                this._changeDetectorRef.markForCheck();
            });   
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    toggleMenu(menuState: FuseMenuType): void {
        this.menuState = menuState;

        this.drawer.toggle();
    }

    openMenu(): void {
        this._navService.toggleMenu();
    }

    onSelect(ev: DrawerSelectEvent): void {
        const selected = ev.item as FuseNavigationItem;

        this.onMenuItemSelected(selected);
    }

    onMenuItemSelected(selected: FuseNavigationItem): void {
        this.selected = selected;
        const tenant = this.route.snapshot.queryParams.tenant;
        const loc = this.route.snapshot.queryParams.loc;
        if (this.selected.type === 'item') {
            this.router.navigate([this.selected.url], {
                queryParams: {
                    tenant: tenant,
                    loc: loc
                },
            });
        }
    }

    cloneWithFilter(navigation: FuseNavigationItem[]): FuseNavigationItem[] {
        var navigationCopy = _.cloneDeep(navigation);
        return this.filterHideItems(navigationCopy);
    }

    filterHideItems(navigation: FuseNavigationItem[]): FuseNavigationItem[] {
        navigation.forEach(item => {
            if (item.items) item.items = this.filterHideItems(item.items);
        });
        return navigation.filter(item => !item.hidden);
    }
}
