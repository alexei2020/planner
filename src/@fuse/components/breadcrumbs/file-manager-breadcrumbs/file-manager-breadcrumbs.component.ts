import { Component, OnDestroy, OnInit } from '@angular/core';
import { FileItem } from 'app/resource/file-manager/shared/file-item';
import { BreadcrumbService } from 'app/main/shared/breadcrumb-bar.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-file-manager-breadcrumbs',
  templateUrl: './file-manager-breadcrumbs.component.html',
  styleUrls: ['./file-manager-breadcrumbs.component.scss'],
})
export class FileManagerBreadcrumbsComponent implements OnInit, OnDestroy {
  breadcrumbs: FileItem[] = [];

  private _unsubscribeAll: Subject<any>;

  constructor(private breadcrumbService: BreadcrumbService) { 
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.breadcrumbService.fileManagerBreadcrumbsChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe((items: FileItem[]) => {
      this.breadcrumbs = items;
    });
  }

  async onBreadcrumbClick(folder: FileItem): Promise<void> {
    this.breadcrumbService.fileManagerBreadcrumbFired(folder);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll?.next();
    this._unsubscribeAll?.complete();
  }
}
