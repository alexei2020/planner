import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerBreadcrumbsComponent } from './file-manager-breadcrumbs.component';

describe('FileManagerBreadcrumbsComponent', () => {
  let component: FileManagerBreadcrumbsComponent;
  let fixture: ComponentFixture<FileManagerBreadcrumbsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileManagerBreadcrumbsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerBreadcrumbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
