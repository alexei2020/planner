export interface IBreadCrumb {
    label: string;
    url: string;
    params?: object;
    handling?: string;
}
