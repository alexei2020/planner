import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuseBreadcrumbsComponent } from './breadcrumbs.component';

describe('FuseBreadcrumbsComponent', () => {
    let component: FuseBreadcrumbsComponent;
    let fixture: ComponentFixture<FuseBreadcrumbsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FuseBreadcrumbsComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FuseBreadcrumbsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
