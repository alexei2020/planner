import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';

import { FuseBreadcrumbsComponent } from './breadcrumbs.component';
import { FileManagerBreadcrumbsComponent } from './file-manager-breadcrumbs/file-manager-breadcrumbs.component';

@NgModule({
    declarations: [FuseBreadcrumbsComponent, FileManagerBreadcrumbsComponent],
    imports: [CommonModule, RouterModule, MatIconModule],
    exports: [FuseBreadcrumbsComponent, FileManagerBreadcrumbsComponent]
})
export class FuseBreadcrumbsModule {}
