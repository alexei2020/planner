import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, NavigationStart } from '@angular/router';
import { Subject } from 'rxjs';
import { distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';

import { BreadcrumbService } from '../../../app/main/shared/breadcrumb-bar.service';
import { PageType } from '../../../app/main/shared/breadcrumb-bar.service.gen';

import { IBreadCrumb } from './IBreadCrumb';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'fuse-breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls: ['./breadcrumbs.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class FuseBreadcrumbsComponent implements OnInit, OnDestroy {
    // Private
    private _unsubscribeAll;

    // Public
    public breadcrumbs: IBreadCrumb[];

    public extraBreadcrumb: IBreadCrumb = null;

    /**
     * Creates an instance of FuseBreadcrumbsComponent.
     * @param {Router} router
     * @param {ActivatedRoute} activatedRoute
     * @param {BreadcrumbService} breadcrumbService
     * @memberof FuseBreadcrumbsComponent
     */
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private breadcrumbService: BreadcrumbService,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer,
    ) {
        iconRegistry.addSvgIcon('breadcrumbs-home', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/breadcrumbs/home.svg'));
        iconRegistry.addSvgIcon('breadcrumbs-arrow-next', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/breadcrumbs/arrow-next.svg'));

        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);

        this.breadcrumbService.breadcrumbList
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((breadcrumbs) => {
            setTimeout(() => { 
              this.extraBreadcrumb = null;
              this.breadcrumbs = breadcrumbs; 
            }, 10);
          });
        this.breadcrumbService.addBreadcrumbFired
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((breadcrumb: IBreadCrumb) => {
             setTimeout(() => { 
                 this.extraBreadcrumb = breadcrumb;
             }, 10);
          });
        this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(res => {
          this.extraBreadcrumb = null;
          this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);
        })
        this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(() => {
              this.extraBreadcrumb = null;
              this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);
            });
    }

    /**
     * Recursively build breadcrumb according to activated route.
     * @param route
     * @param url
     * @param breadcrumbs
     */
    buildBreadCrumb(
        route: ActivatedRoute,
        url: string = '',
        breadcrumbs: IBreadCrumb[] = []
    ): IBreadCrumb[] {
        // If no routeConfig is avalailable we are on the root path
        let label =
            route.routeConfig && route.routeConfig.data
                ? route.routeConfig.data.breadcrumb
                : '';
        let path =
            route.routeConfig && route.routeConfig.data
                ? route.routeConfig.path
                : '';
        // If the route is dynamic route such as ':id', remove it
        const lastRoutePart = path.split('/').pop();
        const isDynamicRoute = lastRoutePart.startsWith(':');
        if (isDynamicRoute && !!route.snapshot) {
            const paramName = lastRoutePart.split(':')[1];
            path = path.replace(
                lastRoutePart,
                route.snapshot.params[paramName]
            );

            label = label && label.length > 0 ? label.replace(`:${paramName}`, route.snapshot.params[paramName]) : paramName;
        }

        // In the routeConfig the complete path is not available,
        // so we rebuild it each time
        const nextUrl = path ? `${url}/${path}` : url;

        const breadcrumb: IBreadCrumb = {
            label: label,
            url: nextUrl,
        };
        // Only adding route with non-empty label
        const newBreadcrumbs = breadcrumb.label
            ? [...breadcrumbs, breadcrumb]
            : [...breadcrumbs];
        if (route.firstChild) {
            // If we are not on our current path yet,
            // there will be more children to look after, to build our breadcumb
            return this.buildBreadCrumb(
                route.firstChild,
                nextUrl,
                newBreadcrumbs
            );
        }
        return newBreadcrumbs;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    onBreadcrumbClicked(breadcrumb: IBreadCrumb){
        const location = this.activatedRoute.snapshot.queryParams.loc;
        const backLocation = this.activatedRoute.snapshot.queryParams.backLocation;
        const tenant = this.activatedRoute.snapshot.queryParams.tenant;
        this.router.navigate([breadcrumb.url],{
            queryParams: {
                loc: backLocation === undefined ? location : backLocation,
                tenant: tenant
            },
        });
    }
}
