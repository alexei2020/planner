import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { FuseMatchMediaService } from '@fuse/services/match-media.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'fuse-shortcuts-2',
    templateUrl: './shortcuts-2.component.html',
    styleUrls: ['./shortcuts-2.component.scss'],
})
export class FuseShortcuts2Component
    implements OnInit, AfterViewInit, OnDestroy {
    shortcutItems: any[];
    mobileShortcutsPanelActive: boolean;

    @ViewChild('shortcuts2')
    shortcutsEl: ElementRef;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseMatchMediaService} _fuseMatchMediaService
     * @param {MediaObserver} _mediaObserver
     * @param {Renderer2} _renderer
     */
    constructor(
        private _fuseMatchMediaService: FuseMatchMediaService,
        private _mediaObserver: MediaObserver,
        private _renderer: Renderer2
    ) {
        // Set the defaults
        this.shortcutItems = [];

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // User's shortcut items
        this.shortcutItems = [
            {
                title: 'Calendar',
                type: 'item',
                icon: 'today',
                url: '/apps/calendar',
            },
            {
                title: 'Day',
                type: 'item',
                icon: 'view_day',
                url: '/apps/day',
            },
            {
                title: 'Week',
                type: 'item',
                icon: 'view_week',
                url: '/apps/week',
            },
            {
                title: 'Comfy',
                type: 'item',
                icon: 'view_comfy',
                url: '/apps/comfy',
            },
        ];
    }

    ngAfterViewInit(): void {
        // Subscribe to media changes
        this._fuseMatchMediaService.onMediaChange
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                if (this._mediaObserver.isActive('gt-sm')) {
                    this.hideMobileShortcutsPanel();
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Show mobile shortcuts
     */
    showMobileShortcutsPanel(): void {
        this.mobileShortcutsPanelActive = true;
        this._renderer.addClass(
            this.shortcutsEl.nativeElement,
            'show-mobile-panel'
        );
    }

    /**
     * Hide mobile shortcuts
     */
    hideMobileShortcutsPanel(): void {
        this.mobileShortcutsPanelActive = false;
        this._renderer.removeClass(
            this.shortcutsEl.nativeElement,
            'show-mobile-panel'
        );
    }
}
