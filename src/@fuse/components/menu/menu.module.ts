import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTreeModule } from '@angular/material/tree';
import { ToolBarModule } from '@progress/kendo-angular-toolbar';

import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { TreeListModule } from '@progress/kendo-angular-treelist';
import { PopupModule } from '@progress/kendo-angular-popup';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { EditorModule } from '@progress/kendo-angular-editor';
import { InputsModule } from '@progress/kendo-angular-inputs';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { LabelsMenuComponent } from './labels-menu/labels-menu.component';
import { LabelsTopMenuComponent } from './labels-top-menu/labels-top-menu.component';
import { FileManagerMenuComponent } from './filemanager-menu/filemanager-menu.component';
import { InventoryDetailMenuComponent } from './inventory-detail-menu/inventory-detail-menu.component';
import { DashboardMenuComponent } from './dashboard-menu/dashboard-menu.component';
import { TopologyMenuComponent } from './topology-menu/topology-menu.component';
import { RoomPlannerMenuComponent } from './roomplanner-menu/roomplanner-menu.component';
import { LocationPageMenuComponent } from './location-menu/location-menu.component';
import { DeviceUserRuleMenuComponent } from './deviceuserrule-menu/deviceuserrule-menu.component';
import { FileManagerToolbarSelectedRowsComponent } from "../../../app/resource/file-manager/file-manager-toolbar-selected-rows/file-manager-toolbar-selected-rows.component";
import { RevisionsMenuComponent } from './revisions-menu/revisions-menu.component';
import { ComponentsModule } from '../../../app/components/module'
@NgModule({
    declarations: [
        DashboardMenuComponent,
        TopologyMenuComponent,
        RoomPlannerMenuComponent,
        DeviceUserRuleMenuComponent,
        LabelsMenuComponent,
        LabelsTopMenuComponent,
        InventoryDetailMenuComponent,
        LocationPageMenuComponent,
        FileManagerMenuComponent,
        FileManagerToolbarSelectedRowsComponent,
        RevisionsMenuComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ButtonModule,
        EditorModule,
        CommonModule,
        RouterModule,
        FormsModule,
        FlexLayoutModule,

        MatButtonModule,
        MatCheckboxModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        MatTooltipModule,
        MatButtonToggleModule,
        MatTreeModule,

        DropDownsModule,
        TreeListModule,
        PopupModule,
        ToolBarModule,
        InputsModule,

        ComponentsModule,

        NgMultiSelectDropDownModule.forRoot(),
        DialogsModule,
    ],
    exports: [
        DashboardMenuComponent,
        TopologyMenuComponent,
        RoomPlannerMenuComponent,
        DeviceUserRuleMenuComponent,
        LabelsMenuComponent,
        LabelsTopMenuComponent,
        InventoryDetailMenuComponent,
        LocationPageMenuComponent,
        FileManagerMenuComponent,
        RevisionsMenuComponent
    ],
})
export class FuseMenu2Module { }
