import { some, isEmpty, filter, includes, split } from "lodash";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  HostListener,
  ElementRef,
} from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { NgForm } from "@angular/forms";

import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import cloneDeep from "lodash/cloneDeep";
import {
  FileManagerService,
  ToolbarState,
  CopyCutStore,
} from "../../../../app/resource/file-manager/file-manager.service";
import { FileItem } from "../../../../app/resource/file-manager/shared/file-item";
import { FileExtension } from "../../../../app/resource/file-manager/shared/file-type";
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: "fuse-filemanager-menu",
  templateUrl: "./filemanager-menu.component.html",
  styleUrls: ["./filemanager-menu.component.scss"],
})
export class FileManagerMenuComponent implements OnInit, OnDestroy {
  toolbarState: ToolbarState;
  selectedGridItems: FileItem[];
  copyCutStore: CopyCutStore;
  selectedAction: 'UPLOAD' | 'DOWNLOAD' | 'REFRESH' | 'COMPARE' | 'OTHERS' | null = null;
  constructor(
    private _fileManagerService: FileManagerService, 
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon('file-upload', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/file-manager/upload.svg'));
    iconRegistry.addSvgIcon('file-download', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/file-manager/download.svg'));
    iconRegistry.addSvgIcon('file-refresh', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/file-manager/refresh.svg'));
    iconRegistry.addSvgIcon('file-compare', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/file-manager/compare.svg'));
    iconRegistry.addSvgIcon('file-settings', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/file-manager/settings.svg'));
  }
  private _unsubscribeAll: Subject<any> = new Subject();
  ngOnInit() {
    this._fileManagerService.toolbarState
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((state: ToolbarState) => {
        this.toolbarState = state;
      });
    this._fileManagerService.selectedGridItems
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: FileItem[]) => {
        this.selectedGridItems = data;
      });
    this._fileManagerService.copyCutStore
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: CopyCutStore) => {
        this.copyCutStore = data;
      });
  }
  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  onCreateNewFolder() {
    this._fileManagerService.toolbarAction.next({
      type: "CreateNewFolder",
      payload: null,
    });
  }
  onUploadFile() {
    this.selectedAction = 'UPLOAD';
    this._fileManagerService.toolbarAction.next({
      type: "UploadFile",
      payload: null,
    });
  }
  onRefresh() {
    this.selectedAction = null
    this._fileManagerService.toolbarAction.next({
      type: "Refresh",
      payload: null,
    });
  }
  onCut() {
    this._fileManagerService.toolbarAction.next({
      type: "Cut",
      payload: null,
    });
  }
  onCopy() {
    this._fileManagerService.toolbarAction.next({
      type: "Copy",
      payload: null,
    });
  }
  onPaste() {
    this._fileManagerService.toolbarAction.next({
      type: "Paste",
      payload: null,
    });
  }
  onDeleteItems() {
    this._fileManagerService.toolbarAction.next({
      type: "Delete",
      payload: null,
    });
  }
  onDownloadFile() {
    this.selectedAction = 'DOWNLOAD';
    this._fileManagerService.toolbarAction.next({
      type: "DownloadFile",
      payload: null,
    });
  }
  onDiff() {
    this.selectedAction = 'COMPARE';
    this._fileManagerService.toolbarAction.next({
      type: "Diff",
      payload: null,
    });
  }
  onResetSelectedFiles(resetItems: FileItem[]): void {
    this.selectedGridItems = filter(
      this.selectedGridItems,
      (item: FileItem) => !includes(resetItems, item)
    );
    this._fileManagerService.selectedGridItems.next(this.selectedGridItems);
    const hasSelectedItems = !isEmpty(this.selectedGridItems);
    const hasSelectedFolder =
      hasSelectedItems && some(this.selectedGridItems, (file) => file.isDir);

    let canDiff =
      hasSelectedItems &&
      !hasSelectedFolder &&
      this.selectedGridItems.length === 2;
    if (canDiff) {
      const fileType1 = this._fileManagerService.getFileExtension(
        this.selectedGridItems[0].path
      );
      const fileType2 = this._fileManagerService.getFileExtension(
        this.selectedGridItems[1].path
      );
      canDiff =
        fileType1 === FileExtension.Plain && fileType2 === FileExtension.Plain;
    }

    this.toolbarState = {
      canCopyOrCut: hasSelectedItems,
      canPaste: !isEmpty(this.copyCutStore.selectedFiles),
      canDelete: hasSelectedItems,
      canDownload: hasSelectedItems && !hasSelectedFolder,
      canDiff: canDiff,
    };
    this._fileManagerService.toolbarState.next(this.toolbarState);
  }
  onOthersMenuClosed(){
    this.selectedAction = null;
    console.log("menu show---->", this.selectedAction);
  }
  onOthersMenuOpened(){
    this.selectedAction = 'OTHERS';
    console.log("menu hide---->", this.selectedAction);
  }
}
