import { v4 } from "uuid";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  HostListener,
  ElementRef,
} from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import {
  CancelEvent,
  EditEvent,
  RemoveEvent,
  SaveEvent,
  TreeListComponent,
} from "@progress/kendo-angular-treelist";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import cloneDeep from "lodash/cloneDeep";

import { BreadcrumbService } from "../../../../app/main/shared/breadcrumb-bar.service";
import { RoomPlannerService } from "../../../../app/roomplanner/services/room-planner.service";
import {
  FloorSpaceData,
  FloorSpaceService,
} from "../../../../app/roomplanner/services/floor-space.service";
import { WallJackService } from "../../../../app/roomplanner/services/wall-jack.service";
import Editor from "../../../../app/roomplanner/editor/Editor";
import { EditorMode } from "../../../../app/roomplanner/editor/managers/ViewManager";
import { IBreadCrumb } from '../../breadcrumbs/IBreadCrumb';
import { KendoConfirmDialog } from "app/main/shared/dialogs/kendo-confirm-dialog/kendo-confirm-dialog.component";
import {
  DialogCloseResult,
  DialogService,
  DialogResult,
} from "@progress/kendo-angular-dialog";

import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import {FuseLocationService} from '../../../../app/services/location.service';
import { truncate } from 'fs';
@Component({
  selector: "fuse-roomplanner-menu",
  templateUrl: "./roomplanner-menu.component.html",
  styleUrls: ["./roomplanner-menu.component.scss"],
})
export class RoomPlannerMenuComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<any> = new Subject();
  private apigroup: any;
  public breadcrumbs: IBreadCrumb[];

  editor: Editor;

  lastParams: Params = {};

  showTree = false;
  loadTree = true;
  allowCancel = false;
  dataTree = [];

  spaceId = "";

  settingsShow = false;
  controlShow = false;

  selectedAction: 'SELECTION' | 'MOVE' | 'SCALE' | 'PROPERTIES' | '3D-VIEW' | 'OTHERS' | null = null;
  is3DMode: boolean = true;
  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _dialogService: DialogService,
    private _breadcrumbService: BreadcrumbService,
    private _roomPlannerService: RoomPlannerService,
    private _floorSpaceService: FloorSpaceService,
    private _wallJackService: WallJackService,
    private _locationService: FuseLocationService,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
      iconRegistry.addSvgIcon(
        'select',
        sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/select.svg')
      );
      iconRegistry.addSvgIcon(
        'properties',
        sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/properties.svg')
      );
      iconRegistry.addSvgIcon(
        'edit',
        sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/edit.svg')
      );
      iconRegistry.addSvgIcon(
        'move',
        sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/move.svg')
      );
      iconRegistry.addSvgIcon(
        'rotate',
        sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/rotate.svg')
      );
      iconRegistry.addSvgIcon(
        'scale',
        sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/scale.svg')
      );
      iconRegistry.addSvgIcon(
        'settings',
        sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/settings.svg')
      );
      iconRegistry.addSvgIcon(
        'view',
        sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/view.svg')
      );
      iconRegistry.addSvgIcon(
        'setunit',
        sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/setunit.svg')
      );
      iconRegistry.addSvgIcon(
        '3d-view',
        sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/3d-view.svg')
      );
  }

  buildBreadCrumb(
      route: ActivatedRoute,
      url: string = '',
      breadcrumbs: IBreadCrumb[] = []
  ): IBreadCrumb[] {
      // If no routeConfig is avalailable we are on the root path
      let label =
          route.routeConfig && route.routeConfig.data
              ? route.routeConfig.data.breadcrumb
              : '';
      let path =
          route.routeConfig && route.routeConfig.data
              ? route.routeConfig.path
              : '';
      // If the route is dynamic route such as ':id', remove it
      const lastRoutePart = path.split('/').pop();
      const isDynamicRoute = lastRoutePart.startsWith(':');
      if (isDynamicRoute && !!route.snapshot) {
          const paramName = lastRoutePart.split(':')[1];
          path = path.replace(
              lastRoutePart,
              route.snapshot.params[paramName]
          );

          label = label && label.length > 0 ? label.replace(`:${paramName}`, route.snapshot.params[paramName]) : paramName;
      }

      // In the routeConfig the complete path is not available,
      // so we rebuild it each time
      const nextUrl = path ? `${url}/${path}` : url;

      const breadcrumb: IBreadCrumb = {
          label: label,
          url: nextUrl,
      };
      // Only adding route with non-empty label
      const newBreadcrumbs = breadcrumb.label
          ? [...breadcrumbs, breadcrumb]
          : [...breadcrumbs];
      if (route.firstChild) {
          // If we are not on our current path yet,
          // there will be more children to look after, to build our breadcumb
          return this.buildBreadCrumb(
              route.firstChild,
              nextUrl,
              newBreadcrumbs
          );
      }
      return newBreadcrumbs;
  }

  ngOnInit() {
    this.breadcrumbs = this.buildBreadCrumb(this._activatedRoute.root);
    
    this._breadcrumbService.breadcrumbList
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((breadcrumbs) => {
        setTimeout(() => { 
          this.breadcrumbs = breadcrumbs; 
        }, 10);
      });

    this._activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(res => {
      this.breadcrumbs = this.buildBreadCrumb(this._activatedRoute.root);
    })

    this._activatedRoute.queryParams
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((params) => {
        console.warn("_activatedRoute:", params);

        this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
        if(this.apigroup.split(".")[0] != this.lastParams.loc) { //detect loc Change
          (this.lastParams.loc || !params.name ) && this.resetHandler();
          this.allowCancel = false;
          this.loadLayers(params);
        }
        if (this.lastParams.loc !== this.apigroup) {
          if (this.lastParams.loc == null) {
            (this.lastParams.loc || !params.name ) && this.resetHandler();
          }

          this._wallJackService.location = this.apigroup;
          this._wallJackService.load();

          this._floorSpaceService.location = this.apigroup;
          this._floorSpaceService.load();

          console.warn("load");
        }

        if (this.lastParams.name !== params.name) {
          if (this.loadTree === false) {
            this.loadLayers(params);
          }
        }

        this.lastParams = params;
      });

    this._floorSpaceService
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(({ loading, items }) => {
        console.warn("_floorSpaceService");
        this.loadTree = loading;
        this.dataTree = items;

        if (this.loadTree === false) {
          this.loadLayers(this.lastParams);
          if(items.length === 0) {
            this.selectedItems = []
            this.selectedLayers = []
            this.computeTitle();
            this.handleLoad(this.selectedLayers[0]);
          }
        }
      });

    this.editor = this._roomPlannerService.editor;
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  /**
   *  Room Planner methods
   */

  async handleRestart() {
    this._floorSpaceService.restore();

    this._roomPlannerService.editor.restart();
  }

  async handleLoad(data) {
    await this._roomPlannerService.load(data);
  }

  handleCancel() {
    if(this.selectedItems.length < 1) {
      this.resetHandler()
      this._floorSpaceService.restore();
    } else {
      this.loadLayers(this._activatedRoute.snapshot.params)
    }

  }

  async handleSave() {
    this.spaceId = await this._roomPlannerService.save(this.selectedLayers);
  }

  /**
   *  Hierarchy methods
   */

  @ViewChild("overlay") overlay: ElementRef;
  @ViewChild("tree", { static: false }) tree: TreeListComponent;

  @ViewChild("controlAnchor") controlAnchor: ElementRef;
  @ViewChild("controlPopup", { read: ElementRef }) controlPopup: ElementRef;
  @ViewChild("settingsAnchor") settingsAnchor: ElementRef;
  @ViewChild("settingsPopup", { read: ElementRef }) settingsPopup: ElementRef;

  @HostListener("document:keydown", ["$event"])
  public onKeyDown(event: any): void {
    // Key: esc
    if (event.key === "Escape" || event.keyCode === 27) {
      this.closeEditor(this.tree);
      this.showTree = false;
      return;
    }

    // Key: del
    if (
      event.key === "Delete" ||
      event.key === "Backspace" ||
      event.keyCode === 8
    ) {
      return;
    }

    // Key: cmd|ctrl + shift + z
    if (
      (event.metaKey || event.ctrlKey) &&
      event.shiftKey &&
      (event.key === "z" || event.keyCode === 90)
    ) {
      this._roomPlannerService.editor.historyManager.redo();
      return;
    }

    // Key: cmd|ctrl + z
    if (
      (event.metaKey || event.ctrlKey) &&
      (event.key === "z" || event.keyCode === 90)
    ) {
      this._roomPlannerService.editor.historyManager.undo();
      return;
    }

    // Key: cmd|ctrl + x
    if (
      (event.metaKey || event.ctrlKey) &&
      (event.key === "x" || event.keyCode === 88)
    ) {
      this._roomPlannerService.editor.eventManager.handleCut();
      return;
    }

    // Key: cmd|ctrl + c
    if (
      (event.metaKey || event.ctrlKey) &&
      (event.key === "c" || event.keyCode === 67)
    ) {
      this._roomPlannerService.editor.eventManager.handleCopy();
      return;
    }

    // Key: cmd|ctrl + v
    if (
      (event.metaKey || event.ctrlKey) &&
      (event.key === "v" || event.keyCode === 86)
    ) {
      this._roomPlannerService.editor.eventManager.handlePaste();
      return;
    }

    // Key: g
    if (event.key === "g" || event.keyCode === 71) {
      this._roomPlannerService.editor.viewManager.changeGrid();
      return;
    }
  }

  @HostListener("document:click", ["$event"])
  public onDocumentClick(event: any): void {
    if (this.overlay?.nativeElement.contains(event.target)) {
      this.closeEditor(this.tree);
      this.showTree = false;
    }

    if (
      !this.controlAnchor?.nativeElement.contains(event.target) &&
      !this.controlPopup?.nativeElement.contains(event.target)
    ) {
      this.controlShow = false;
    } else if (this.controlPopup?.nativeElement.contains(event.target)) {
      this.controlShow = false;
    }

    if (
      !this.settingsAnchor?.nativeElement.contains(event.target) &&
      !this.settingsPopup?.nativeElement.contains(event.target)
    ) {
      this.settingsShow = false;
      
    } else if (this.settingsPopup?.nativeElement.contains(event.target)) {
      this.settingsShow = false;
    }
    if(!this.controlShow && !this.settingsShow)
      if(this.selectedAction === 'OTHERS' || this.selectedAction === 'PROPERTIES')
        this.selectedAction = null;
  }

  private originalValues: FloorSpaceData;

  public selectedOrder = ["Floor", "Suite", "Room", "Other"];
  public selectedItems = [];
  public selectedLayers = [];
  public selectedTitle = "";

  public editedItem: FloorSpaceData;

  public search = "";
  public filter = {
    logic: "or",
    filters: [
      {
        field: "name",
        operator: "contains",
        value: "",
      },
    ],
  };

  public isNew = false;
  public isEdit = false;

  /* */

  private computeTree() {
    const plannerData = this._roomPlannerService.editor.toJSON();
    if (!plannerData.entities.length) {
      return;
    }

    let uid = v4();
    let item = null;
    if (this.selectedLayers[0]) {
      item = this._floorSpaceService.preItems.find(
        (item) => item.uid === this.selectedLayers[0].uid
      );
      if (item) {
        item.plannerData = plannerData;
      }
    } else {
      item = {
        isPreview: true,
        isVirtual: true,

        root: null,

        kind: "FloorSpace",
        version: "0",
        uid: uid,
        id: uid,
        id1: uid,
        name1: "untitled",
        parentId: null,
        name: "untitled",
        type: "Other",
        floorPlan: "",
        plannerData,
      };

      this._floorSpaceService.preItems.push(item);
    }

    if (!item) {
      return;
    }

    this.dataTree = this._floorSpaceService.computeItems(
      this._floorSpaceService.preItems
    );

    this.selectedItems = item.plannerData.entities.reduce(
      (items, entity) => {
        if (entity.type === "Room" && entity.visible) {
          items.push({ itemKey: `${item.name}--${entity.name}` });
        }
        return items;
      },
      [{ itemKey: item.name }]
    );
    this.selectedLayers = this.selectedItems
      .map((item) => this.dataTree.find((i) => i.name1 === item.itemKey))
      .filter(Boolean)
      .sort(
        (a, b) =>
          this.selectedOrder.indexOf(a.type) -
          this.selectedOrder.indexOf(b.type)
      );

    this.computeTitle();
  }

  /* */

  openTree() {
    this.computeTree();
    this.showTree = true;
  }

  closeTree() {
    this.showTree = false;
  }

  /* */

  public selectHandler(selectedItems) {
    const otherLayers = selectedItems.filter((item) => {
      const first = selectedItems[0];
      const match = /^(.+)--/g.exec(first.itemKey);
      return !item.itemKey.startsWith(match ? match[1] : first.itemKey);
    });

    this.selectedItems = otherLayers.length ? otherLayers : selectedItems;

    this.closeEditor(this.tree);
    this.loadHandler();
  }

  /* */

  public loadHandler() {
    this._router.navigate([], {
      relativeTo: this._activatedRoute,
      queryParams: {
        name: this.selectedItems.length  ? this.selectedItems[0].itemKey : null,
      },
      queryParamsHandling: "merge",
    });

    setTimeout(() => {
      this.closeEditor(this.tree);
      this.showTree = false;
    }, 16);
  }

  public resetHandler() {
    this.search = "";
    this.filter = {
      logic: "or",
      filters: [
        {
          field: "name",
          operator: "contains",
          value: this.search,
        },
      ],
    };

    this.selectedItems = [];
    this.selectedLayers = [];
    this.selectedTitle = "";

    this._floorSpaceService.restore();

    this._roomPlannerService.editor.restart();

    this._router.navigate([], {
      relativeTo: this._activatedRoute,
      queryParams: {
        name: null
      },
      queryParamsHandling: "merge",
    });
  }

  public cancelSelection() {
    this.allowCancel = true;
    this.resetHandler();
  }

  /* */

  public editHandler(event: EditEvent, form: NgForm): void {
    // Close the current edited row, if any
    this.closeEditor(event.sender, event.dataItem);

    // Store a copy of the original item values in case editing is cancelled
    this.editedItem = event.dataItem;
    this.originalValues = { ...event.dataItem };

    // Put the row in edit mode
    event.sender.editRow(event.dataItem);

    this.isEdit = true;
  }

  public saveHandler(event: SaveEvent, form: NgForm): void {
    if (!form.valid) {
      return;
    }

    const itemData = { ...event.dataItem };

    const originalName = this.originalValues?.name || "";

    // this._floorSpaceService
    //     .save(itemData, event.isNew)
    //     .pipe(take(1))
    //     .subscribe((d) => {
    //         if (event.parent) {
    //             event.sender.reload(event.parent);
    //         }
    //
    //         this._roomPlannerService.next({
    //             type: 'editor/room-renamed',
    //             payload: {
    //                 curr: originalName,
    //                 next: itemData.name,
    //             },
    //         });
    //     });

    this.closeEditor(event.sender, event.dataItem, event.isNew);
  }

  public removeHandler(event: RemoveEvent, form: NgForm): void {
    const dialogRef = this._dialogService.open({
      title: "Remove floorspace",
      content: KendoConfirmDialog,
      width: "420px",
      height: "200px",
    });
    const dialogInstance = dialogRef.content.instance;
    dialogInstance.message = "Are you sure you want to remove floorspace?";
    dialogInstance.dialog = dialogRef;
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe(async (response: DialogResult) => {
      if (response instanceof DialogCloseResult || response.text == "No") {
      } else {
        if (event.dataItem.parentId) {
          const entity = this._roomPlannerService.editor.entityManager.entities.find(
            (entity) => event.dataItem.id.includes(entity.id)
          );
          if (entity) {
            this._roomPlannerService.editor.entityManager.removeEntity(entity);

            this.computeTree();
          }
        } else {
          if (
            this.selectedItems.some(
              (item) => item.itemKey === event.dataItem.name
            )
          ) {
            this.resetHandler();
          }

          await this._floorSpaceService.remove(event.dataItem).toPromise();
        }

        if (event.parent) {
          event.sender.reload(event.parent);
        }
      }
    });
  }

  public cancelHandler(event: CancelEvent, form: NgForm): void {
    this.closeEditor(event.sender, event.dataItem, event.isNew);
  }

  /* */

  private closeEditor(
    sender: TreeListComponent,
    dataItem: any = this.editedItem,
    isNew: boolean = false
  ): void {
    sender?.closeRow(dataItem, isNew);

    if (this.editedItem) {
      // Revert to the original values
      Object.assign(this.editedItem, this.originalValues);
    }

    this.editedItem = undefined;
    this.originalValues = undefined;

    this.isNew = false;
    this.isEdit = false;
  }

  /* */

  private loadLayers(params) {
    console.warn("loadLayers");
    if(!this.allowCancel && !params.name && this._floorSpaceService.preItems) {
      const preItems = this._floorSpaceService.preItems;
      const firstItems = preItems?.length > 0 && 
      preItems[0].plannerData.entities.reduce(
      (items, entity) => {
        if (entity.type === "Room" && entity.visible) {
          items.push({ itemKey: `${preItems[0].name}--${entity.name}` });
        }
        return items;
      },
      [{ itemKey: preItems[0].name }]
    );

    this._router.navigate([], {
      relativeTo: this._activatedRoute,
      queryParams: {
        name: firstItems.length ?  firstItems[0].itemKey : null,
      },
      queryParamsHandling: "merge",
    });
  
      setTimeout(() => {
        this.closeEditor(this.tree);
        this.showTree = false;
      }, 16);
    }
    if (this.spaceId) {
      const item = this._floorSpaceService.preItems.find(
        (item) => item.name1 === this.spaceId
      );
      if (item) {
        this.selectedItems = [];
        item.plannerData.entities.forEach((entity) => {
          if (entity.type === "Room" && entity.visible) {
            this.selectedItems.push({ itemKey: `${item.name}--${entity.name}` });
          }
        });

        this.loadHandler();
      }

      this.spaceId = "";

      return;
    }

    const item = this._floorSpaceService.preItems.find(
      (item) => item.name === params.name
    );
    if (item) {
      this.selectedItems = [];
      item.plannerData.entities.forEach((entity) => {
        if (entity.type === "Room" && entity.visible) {
          this.selectedItems.push({ itemKey: `${item.name}--${entity.name}` });
        }
      });
    }
    this.selectedLayers = this.selectedItems
      .map((item) => this.dataTree.find((i) => i.name1 === item.itemKey))
      .filter(Boolean)
      .sort(
        (a, b) =>
          this.selectedOrder.indexOf(a.type) -
          this.selectedOrder.indexOf(b.type)
      );

    this.computeTitle();

    if (this.selectedLayers.length && this.selectedLayers[0].plannerData) {
      this.selectedLayers[0].plannerData.entities.forEach((entity) => {
        if (entity.type === "Room") {
          entity.visible = this.selectedLayers.some(
            (item) => item.name === entity.name
          );
        }
      });

      this.handleLoad(this.selectedLayers[0]);
    }
  }

  /* */

  computeTitle() {
    const tree = this.unflatten(cloneDeep(this.dataTree));

    let allSelected = 0;
    let allCount = 0;
    this.traverse(tree, (item, parent) => {
      if (this.selectedLayers.some((layer) => layer.id === item.id)) {
        item.isSelected = true;

        allSelected++;
      }
    });

    // Total
    let selected = 0;

    // Floor
    tree.forEach((item) => {
      // Suite
      let level2Selected = 0;
      item.children.forEach((item) => {
        if (item.isSelected) {
          level2Selected++;
        }

        // Room/Other
        let level3Selected = 0;
        item.children.forEach((item) => {
          if (item.isSelected) {
            level3Selected++;
          }
        });
        if (item.isSelected) {
          if (level3Selected) {
            if (level3Selected === item.children.length) {
              selected += 1;
            } else {
              selected += 1 + level3Selected;
            }
          } else {
            selected += 1;
          }
        } else {
          selected += level3Selected;
        }
      });

      if (item.isSelected) {
        selected += 1;

        let allSelected = 0;
        let allCount = 0;
        this.traverse(item.children, (item, parent) => {
          if (item.isSelected) {
            allSelected++;
          }
          allCount++;
        });
        if (allSelected === allCount) {
          selected = 1;
        }
      }
    });

    this.selectedTitle = !this.selectedLayers[0]
      ? "Select Floorspace"
      : this.selectedLayers[0].name +
        (selected <= 1 ? "" : ` +${selected - 1}`);
  }

  /* */

  traverse(tree, iterator, parent = null) {
    tree.forEach((item, i) => {
      iterator(item, parent, i);
      this.traverse(item.children, iterator, item);
    });
  }

  unflatten(list) {
    const map = {};

    for (let i = 0; i < list.length; i += 1) {
      map[list[i].id] = i; // initialize the map
      list[i].children = []; // initialize the children
    }

    const roots = [];

    for (let i = 0; i < list.length; i += 1) {
      const node = list[i];

      if (node.parentId !== null) {
        list[map[node.parentId]].children.push(node);
      } else {
        roots.push(node);
      }
    }

    return roots;
  }
  onSelectionBtnClicked(){
    this.selectedAction = 'SELECTION';
    this.editor.viewManager.changeTransformMode('select')
  }
  onMoveBtnClicked(){
    this.selectedAction = 'MOVE';
    this.editor.viewManager.changeTransformMode('translate')
  }
  onPropertiesBtnClicked(){
    this.settingsShow = !this.settingsShow;
    this.selectedAction = this.settingsShow ? 'PROPERTIES' : null;
  }
  onOthersBtnClicked(){
    this.controlShow = !this.controlShow;
    this.selectedAction = this.controlShow ? 'OTHERS' : null;
  }
  on3DModeBtnClicked(){
    this.is3DMode = !this.is3DMode;
    this.editor.viewManager.changeViewMode();
    
  }
  onEditModeBtnClicked(){
    this.editor.viewManager.changeEditorMode();
  }
}
