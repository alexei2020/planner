import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomPlannerMenuComponent } from './roomplanner-menu.component';

describe('RoomPlannerMenuComponent', () => {
  let component: RoomPlannerMenuComponent;
  let fixture: ComponentFixture<RoomPlannerMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomPlannerMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomPlannerMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
