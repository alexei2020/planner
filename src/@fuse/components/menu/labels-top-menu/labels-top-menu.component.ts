import { Component, Input } from '@angular/core';
import { BreadcrumbService, EventType, } from '../../../../app/main/shared/breadcrumb-bar.service'
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
@Component({
    selector: 'fuse-labels-top-menu',
    templateUrl: './labels-top-menu.component.html',
    styleUrls: ['./labels-top-menu.component.scss']
})
export class LabelsTopMenuComponent {
    private _iconStyle: string;
    
    get iconStyle(): string{
        return this._iconStyle;
    }
    @Input() set iconStyle(style: string){
        this._iconStyle = style;
    }
    constructor(
        public _breadcrumbService: BreadcrumbService,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer,
    ) {
        iconRegistry.addSvgIcon('dashboard-settings', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/dashboard/settings.svg'));
    }

    add() {
        this._breadcrumbService.triggeredEvent.next({
            type: EventType.DEVICE_DETAIL_ADD
        })
    }
    clone() {
        this._breadcrumbService.triggeredEvent.next({
            type: EventType.DEVICE_DETAIL_CLONE
        })
    }
    delete() {
        this._breadcrumbService.triggeredEvent.next({
            type: EventType.DEVICE_DETAIL_DELETE
        })
    }
}
