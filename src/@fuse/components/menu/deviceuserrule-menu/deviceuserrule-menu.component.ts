import { Input, Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { BreadcrumbService } from '../../../../app/main/shared/breadcrumb-bar.service'
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
@Component({
    selector: 'fuse-deviceuserrule-menu',
    templateUrl: './deviceuserrule-menu.component.html',
    styleUrls: ['./deviceuserrule-menu.component.scss']
})
export class DeviceUserRuleMenuComponent implements OnInit {
    propertyDropdownData: any[];
    private _iconStyle: string;
    get iconStyle(): string{
        return this._iconStyle;
    }
    @Input() set iconStyle(style: string){
        this._iconStyle = style;
    }
    constructor(
        public _breadcrumbService: BreadcrumbService,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer
    ) {
        iconRegistry.addSvgIcon(
            'label-fill',
            sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/label_filled.svg')
        );
        iconRegistry.addSvgIcon(
            'label-outline',
            sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/label.svg')
        );        
    }
    ngOnInit() {

    }
    showLabels() {
        this._breadcrumbService.triggeredDeviceUserRuleEvent.next({
            type: "showLabels"
        })
    }
}
