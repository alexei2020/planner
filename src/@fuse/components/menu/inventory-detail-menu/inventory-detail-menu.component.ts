import { Input, Component, OnInit, OnDestroy } from '@angular/core';
import { BreadcrumbService, EventType, Event, FormValidationInfo } from '../../../../app/main/shared/breadcrumb-bar.service'
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Subject, BehaviorSubject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import _ from 'lodash';
@Component({
    selector: 'fuse-inventory-detail-menu',
    templateUrl: './inventory-detail-menu.component.html',
    styleUrls: ['./inventory-detail-menu.component.scss']
})
export class InventoryDetailMenuComponent implements OnInit, OnDestroy {
    propertyDropdownData: any[];
    dropdownList = [];
    dropdownSettings: IDropdownSettings = {};
    selectedItems: Array<any> = [];
    defaultItem: { name: string, id: number } = { name: "None", id: null };
    selectedPlatform$: BehaviorSubject<any[]> = new BehaviorSubject(null);
    platformSelected: any;
    selectedOptionalDetails$: BehaviorSubject<any[]> = new BehaviorSubject(null);
    //optionalDetailsSelected: any;
    
    private _iconStyle: string;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    get iconStyle(): string {
        return this._iconStyle;
    }
    @Input() set iconStyle(style: string) {
        this._iconStyle = style;
    }
    constructor(
        public _breadcrumbService: BreadcrumbService,
        private _router: Router,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer,
        private route: ActivatedRoute,
    ) {
		iconRegistry.addSvgIcon('breadcrumbs-arrow-next', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/breadcrumbs/arrow-next.svg'));

        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'Deselect All',
            itemsShowLimit: 1,
            allowSearchFilter: false
        };
    }
    ngOnInit() {
        this._breadcrumbService.dropdownList.pipe(takeUntil(this._unsubscribeAll)).subscribe((drop) => {
            this.dropdownList = drop
        });

        this._breadcrumbService.selectedItems.pipe(takeUntil(this._unsubscribeAll)).subscribe((sel) => {
            this.selectedItems = sel;
        });

	this._breadcrumbService.platformSelected.pipe(takeUntil(this._unsubscribeAll)).subscribe((platform) => {
		// case where switch details component forces the platform selection on click
		// of a switch component
		this.platformSelected = platform;
	});

        this.selectedPlatform$ = this._breadcrumbService.selectedPlatform;
        
        this.route.queryParams.subscribe(async (params) => {
            const platformId = params.platform;
            this.initSelectedPlatform(platformId);
        });
    }

    onChange(event) {
        this._breadcrumbService.selectedItemsChanges.next(event);
	this._breadcrumbService.platformSelected.next(this.platformSelected);
        this._router.navigate([], { queryParams: { port: event.item_id }, queryParamsHandling: 'merge', replaceUrl: true });
    }

    onItemSelect(item: any) {
        this._breadcrumbService.itemSelected.next(item);
    }

    onItemDeSelect(item: any) {
        this._breadcrumbService.deItemSelected.next(item);
    }

    onSelectAll(items: any) {
        this._breadcrumbService.allSelected.next(items);
    }

    onDeSelectAll(items: any) {
        this._breadcrumbService.deAllSelected.next(items);
    }

    showLabels() {
        this._breadcrumbService.triggeredLabelRuleEvent.next({
            type: "showLabels"
        })
    }

    ngOnDestroy(){
	this._unsubscribeAll.next();
	this._unsubscribeAll.complete();
    }

    updatePlatform(platform) {
        if (platform && platform.list && platform.list.length > 0) this.selectedOptionalDetails$.next(platform.list)
        this.platformSelected = platform.id ? platform : null;
        //this.optionalDetailsSelected = null;
        this._breadcrumbService.platformSelected.next(platform);
        //this._breadcrumbService.optionalDetailsSelected.next(null);
        console.log("platform--->", platform)
        this._router.navigate([], { queryParams: { platform: platform.id }, queryParamsHandling: 'merge', replaceUrl: true });
    }

    private async initSelectedPlatform(platformId: number): Promise<void> {
        if (_.isEmpty(platformId)) {
            return;
        }
        const platforms = await this._breadcrumbService.selectedPlatform.pipe(take(1)).toPromise();
        if (_.isEmpty(platforms)) {
            return;
        }
        const platform = _.find(platforms, (item) => item.id === platformId);
        if (!_.isEmpty(platform)) {
            const isSelectedEqualUrlPlatform = !_.isNull(this.platformSelected) && this.platformSelected.id === platform.id;
            if (!isSelectedEqualUrlPlatform) {
                this.updatePlatform(platform);
            }
        }
    }

    //updateOptionalDetails(option) {
    //    this.optionalDetailsSelected = option;
    //    this._breadcrumbService.optionalDetailsSelected.next(option);
    //}
}
