import { Input, Component, OnInit, OnDestroy } from '@angular/core';
import { BreadcrumbService, EventType, Event, FormValidationInfo } from '../../../../app/main/shared/breadcrumb-bar.service'
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PageType } from '../../../../app/main/shared/breadcrumb-bar.service.gen';
import _ from 'lodash';
import { Router } from '@angular/router';
import { ConfirmDeactivateGuard } from 'app/services/confirm-deactivation.service';
@Component({
    selector: 'fuse-labels-menu',
    templateUrl: './labels-menu.component.html',
    styleUrls: ['./labels-menu.component.scss']
})
export class LabelsMenuComponent implements OnInit, OnDestroy {
    propertyDropdownData: any[];
    private _iconStyle: string;
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    currentPage: PageType;
    formsInfo: {[key: string]: boolean} = {};
    isEditing: boolean = false;
    isInvalid: boolean = false;
    hideBar: boolean = true;
    container2: any;
    
    get iconStyle(): string{
        return this._iconStyle;
    }
    @Input() set iconStyle(style: string){
        this._iconStyle = style;
    }
    @Input() show_hide_btn: boolean;
    constructor(
        public _breadcrumbService: BreadcrumbService,
        private router: Router,
        private _deactivateService: ConfirmDeactivateGuard,
    ) { 
        this.router.events.subscribe((ev) => {
            if (this.show_hide_btn == true) {
                // We could have decided to stay on page due to unsaved changes
                // get the editing state from the service
                this.isEditing = this._deactivateService.editing();
            }
        });
    }
    ngOnInit() {
        this.container2 = document.getElementById('container-2');
        this._breadcrumbService.isDeviceDetailEditMode
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((info: FormValidationInfo) =>{
                if (!info) {
                    this.formsInfo = {};
                    this.isInvalid = false;
                    this.isEditing = false;
                    return;
                }
                this.formsInfo[info.name] = info.isValid;
                this.isInvalid = false;
                _.forOwn(this.formsInfo, (value: boolean, key: string ) => {
                    if (!value) {
                        this.isInvalid = true;
                    }
                });
                this.isEditing = true;
            });
	// Subscribe to the dashboard changes
        this._breadcrumbService.currentPage.pipe(takeUntil(this._unsubscribeAll)).subscribe(page => {
            setTimeout(() => {
		this.currentPage = page;
	    }, 10);
        });
    }
    showLabels() {
        this._breadcrumbService.triggeredLabelRuleEvent.next({
            type: "showLabels"
        })
    }
    hide() {
        if (this.show_hide_btn && !this.isEditing) {
            this.hideBar = true;
            this.container2.setAttribute('style', 'margin-bottom:0px');
        } else if (this.hideBar) {
            this.hideBar = false;
            this.container2.setAttribute('style', 'margin-bottom:40px');
        }
        return this.hideBar;
    }
    
    ngOnDestroy(){
	this._unsubscribeAll.next();
	this._unsubscribeAll.complete();
    }
    cancel() {
        this.isEditing = false;
        this._breadcrumbService.triggeredEvent.next({
            type: EventType.DEVICE_DETAIL_CANCEL
        })
    }
    save() {
        this._breadcrumbService.triggeredEvent.next({
            type: EventType.DEVICE_DETAIL_SAVE
        })
    }

}
