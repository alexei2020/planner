import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { GeolocationService } from '../../../../app/main/services/geolocation.service';
import { BreadcrumbService } from '../../../../app/main/shared/breadcrumb-bar.service';

@Component({
  selector: 'fuse-location-menu',
  templateUrl: './location-menu.component.html',
  styleUrls: ['./location-menu.component.scss'],
})
export class LocationPageMenuComponent implements OnInit, OnDestroy {
  public isSelected = false;
  public changesExist = false;

  private _unsubscribeAll: Subject<any> = new Subject();

  constructor(
    public _breadcrumbService: BreadcrumbService,
    private _geolocationService: GeolocationService
  ) {}

  ngOnInit(): void {
    this._geolocationService.isSelected.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: boolean) => {
      this.isSelected = res;
    });

    this._geolocationService.changesExist$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(() => {
        this.changesExist = true;
      });
  }

  // onEdit() {
  //     this._geolocationService.event.next({
  //         type: GeolocationEventType.EDIT
  //     })
  // }
  // onDelete() {
  //     this._geolocationService.event.next({
  //         type: GeolocationEventType.DELETE
  //     })
  // }
  // onAdd() {
  //     this._geolocationService.event.next({
  //         type: GeolocationEventType.ADD
  //     })
  // }
  // showAll() {
  //     this._geolocationService.event.next({
  //         type: GeolocationEventType.SHOW_ALL
  //     })
  // }
  onCancel(): void {
    location.reload();
  }

  onSave(): void {
    console.log('ON SAVE -> we should save location list and notify user');
    this._geolocationService.saveAll();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
