import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import {
    DialogService,
    DialogCloseResult,
} from '@progress/kendo-angular-dialog';

import { Dashboard } from '../../../../app/main/shared/dashboard.model';
import { BreadcrumbService, EventType } from '../../../../app/main/shared/breadcrumb-bar.service';
import { PageType } from '../../../../app/main/shared/breadcrumb-bar.service.gen';
import { LayoutService, TimerService } from '../../../../app/main/shared/services';
import { ManageChartDialogComponent } from '../../../../app/main/shared/dialogs/manage-chart-dialog/manage-chart-dialog.component';
import { Widget } from '../../../../app/main/widget';


@Component({
    selector: 'fuse-dashboard-menu',
    templateUrl: './dashboard-menu.component.html',
    styleUrls: ['./dashboard-menu.component.scss']
})
export class DashboardMenuComponent implements OnInit, OnDestroy {
    @Input() public isDetailPage: string;
    @Input() public isReportPage: string;

    public PageType = PageType;

    public layouts: any[];
    public layouts$: Subject<any>;
    public item: Dashboard[];
    public currentPage: string;
    public queryParams: any;
    public selectedLayout: any;
    public refreshList = [
        { label: 'Off', value: 'Off' },
        { label: '5s', value: 5000 },
        { label: '10s', value: 10000 },
        { label: '30s', value: 30000 },
        { label: '1m', value: 60000 },
        { label: '5m', value: 300000 },
        { label: '15m', value: 900000 },
        { label: '30m', value: 1800000 },
        { label: '1h', value: 3600000 },
        { label: '2h', value: 7200000 },
        { label: '1d', value: 86400000 }
    ];
    public rangeList = [
        { label: 'Last 5 minutes', short: '5m', start: 0, end: 0, step: 5 },
        { label: 'Last 15 minutes', short: '15m', start: 0, end: 0, step: 15 },
        { label: 'Last 30 minutes', short: '30m', start: 0, end: 0, step: 30 },
        { label: 'Last 1 hour', short: '1h', start: 0, end: 0, step: 60 },
        { label: 'Last 3 hours', short: '3h', start: 0, end: 0, step: 180 },
        { label: 'Last 6 hours', short: '6h', start: 0, end: 0, step: 360 },
        { label: 'Last 12 hours', short: '12h', start: 0, end: 0, step: 720 },
        { label: 'Last 24 hours', short: '24h', start: 0, end: 0, step: 1440 },
        { label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 },
        { label: 'Last month', short: '4w', start: 0, end: 0, step: 43200 }
    ];
    public refreshToggle = false;
    public refreshDashboard = false;
    public refreshTime = this.refreshList[0];
    public selectedRange = { label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 };
    public selectedRefreshInterval = this.refreshList[0];
    public editReport = false;
    public selectedReportItem = false;
    public reportItemUnlock = false;
    public isEditItem = false;
    public isEditStateItem = false;

    private _unsubscribeAll: Subject<any>;

    public selectedAction: 'ADD_CHART' | 'REFRESH' | 'OTHERS' | null = null;
    constructor(
        private _breadcrumbService: BreadcrumbService,
        private _timer: TimerService,
        private _router: Router,
        private route: ActivatedRoute,
        private dialogService: DialogService,
        private layoutService: LayoutService,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer,
    ) {
        iconRegistry.addSvgIcon('breadcrumbs-arrow-next', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/breadcrumbs/arrow-next.svg'));
        iconRegistry.addSvgIcon('dashboard-add', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/dashboard/add.svg'));
        iconRegistry.addSvgIcon('dashboard-refresh', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/dashboard/refresh.svg'));
        iconRegistry.addSvgIcon('dashboard-settings', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/dashboard/settings.svg'));
        this._unsubscribeAll = new Subject();

    }

    ngOnInit(): void {
        this.route.queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((params) => {
                let sr = this.rangeList.find((range) => {
                    return range.short === params['range'];
                });
                this.selectedRange = sr || { label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 };
                let sri = this.refreshList.find((range) => {
                    return range.label === params['refresh'];
                });
                this.selectedRefreshInterval = sri || { label: 'Off', value: 'Off' };
            });

        this._timer.setDateRangeObs(this._breadcrumbService.setRange(this.selectedRange));
        this._breadcrumbService.queryParams.next({
            range: this.selectedRange.short,
            refresh: this.selectedRefreshInterval.label
        });
        this._breadcrumbService.currentPage
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((res) => {
                if (!res) {
                    return;
                }
                setTimeout(() => {
                    this.currentPage = res;
                }, 100);
            });
        this._breadcrumbService.currentPageInfo.pipe(takeUntil(this._unsubscribeAll)).subscribe((info) => {
            // console.log('PAGE: ', info);
        });

        this._breadcrumbService.isEditStateItem.pipe(takeUntil(this._unsubscribeAll)).subscribe((editStateItem) => {
            this.isEditStateItem = editStateItem;
        });
        
        this._breadcrumbService.triggeredEvent.pipe(takeUntil(this._unsubscribeAll)).subscribe((event) => {
            if (event.type === EventType.REPORT_EDIT_ITEMS) {
                this.editReport = !this.editReport;
            }
            if (event.type === EventType.REPORT_ITEM_INIT) {
                this.selectedReportItem = true;
            }
            if (event.type === EventType.REPORT_ITEM_UNSAVE) {
                this.selectedReportItem = false;
            }
            if (event.type === EventType.REPORT_ITEM_IS_EDIT) {
                this.isEditItem = true;
            }
            if (event.type === EventType.REPORT_ITEM_NO_EDIT) {
                this.isEditItem = false;
            }
        });
        this.layouts$ = this._breadcrumbService.layouts;
        // setTimeout(() => {
        //     this._router.navigate([], {
        //         queryParams: {
        //             refresh: this.selectedRefreshInterval.label,
        //             range: this.selectedRange.short
        //         },
        //         queryParamsHandling: 'merge'
        //     });
        // }, 10);

        this.initPanel();
    }

    public initPanel(): void {
        //
        this.route.queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((params) => {
                if (this.queryParams) {
                    this.queryParams = {
                        ...this.queryParams,
                        ...params
                    };
                } else {
                    this.queryParams = params;
                }
                this.getLayoutByQueryParams();
            });
        //
        this._breadcrumbService.layouts
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((res) => {
                this.layouts = [];
                this.layouts = res;
                this.getLayoutByQueryParams();
            });
        //
        this._breadcrumbService.item
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((res) => {
                this.item = [];
                this.item = res;
            });

    }

    getLayoutByQueryParams(): void {
        if (this.route.snapshot.queryParams['layout'] && this.layouts) {
            const selectedLayout = this.layouts.filter(
                (item) => item.name === this.queryParams.layout
            );
            this.selectedLayout = selectedLayout[0];
        }
    }

    goBack(): void {
        this._router.navigate(['dashboard']);
    }

    // TODO AB maybe use this to add chart instead of onAddNewChart
    onNewWidget(): void {
        this._breadcrumbService.triggeredEvent.next({ type: EventType.DASHBOARD_ADD_LAYOUT });
    }

    onAddItem(): void {
        if (this.currentPage !== PageType.DASHBOARD) {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ADD });
        } else {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.DASHBOARD_ADD });
        }
    }

    onSaveItem(): void {
        if (this.currentPage !== PageType.DASHBOARD) {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_SAVE });
        } else {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.DASHBOARD_SAVE });
        }
    }

    onCloneLayout(): void {
        if (this.currentPage !== PageType.DASHBOARD) {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_CLONE })
        } else {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.DASHBOARD_CLONE });
        }
    }

    onDeleteLayout(): void {
        if (this.currentPage !== PageType.DASHBOARD) {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_DELETE })
        } else {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.DASHBOARD_DELETE });
        }
    }

    updateRange(range: any): void {
        this.selectedRange = range;
        this._timer.setDateRangeObs(this._breadcrumbService.setRange(range));

        this._breadcrumbService.queryParams.next({
            // ...this._breadcrumbService.queryParams.getValue(),
            range: range.short
        });
        this._router.navigate([], { queryParams: { range: range.short }, queryParamsHandling: 'merge' });
    }

    refreshToggleClicked(): void {
        this.refreshToggle = !this.refreshToggle;
    }

    onRefresh(): void {
        this.selectedAction = null;
        this.refreshDashboard = !this.refreshDashboard;
        this._timer.setRefreshObs(this.refreshDashboard);
        this.refreshTime = this.refreshList[0];
        this._timer.setIntervalObs(this.refreshList[0]);
    }

    refreshTimeSelected(interval: any): void {
        this.refreshTime = interval.value;
        this._timer.setIntervalObs(interval.value);

        this.selectedRefreshInterval = interval;
        this._breadcrumbService.queryParams.next({
            // ...this._breadcrumbService.queryParams.getValue(),
            refresh: interval.label
        });
        this._router.navigate([], { queryParams: { refresh: interval.label }, queryParamsHandling: 'merge' });


    }

    updateLayout(layout): void {
        // console.log('query params----->', this._breadcrumbService.queryParams);
        this.selectedLayout = layout;
        this._router.navigate([], { queryParams: { layout: layout.name }, queryParamsHandling: 'merge' });
        this._breadcrumbService.queryParams.next({
            layout: layout.name
        });
    }

    onAddTextBoxControl() {
        if (this.currentPage !== PageType.DASHBOARD) {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ADD_TEXT_CONTROL });
        } else {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.DASHBOARD_ADD });
        }
    }

    onEditReport() {
        this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_EDIT_ITEMS });
    }

    setEditorEvent(event: string) {
        this._breadcrumbService.editorEvent.next(event);
    }

    onPDFReport() {
        this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_TO_PDF });
    }

    onTimerReport() {
        this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ADD_TIMER_CONTROL });
    }


    onAddImageBoxControl() {
        if (this.currentPage !== PageType.DASHBOARD) {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ADD_IMAGE_CONTROL });
        } else {
            this._breadcrumbService.triggeredEvent.next({ type: EventType.DASHBOARD_ADD });
        }
    }

    onLineReport() {
        this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ADD_LINE });
    }

    // AB New charts
    onAddNewChart(): void {
        this.selectedAction = "ADD_CHART";
        const dialogRef = this.dialogService.open({
            // title: 'Add Panel',

            // Show component
            content: ManageChartDialogComponent,
            width: '40%',
            height: '70%',
            preventAction: (ev, dialog) => {
                if (ev instanceof DialogCloseResult || !ev.primary) {
                    return false;
                }
                return !dialog.content.instance.widgetForm.valid;
            }
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.dialog = dialogRef;
        dialogInstance.actionLabel1 = 'Cancel';
        dialogInstance.actionLabel2 = 'Add new';

        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
            this.selectedAction = null;
            if (result instanceof DialogCloseResult || result.text === 'Cancel') {
                // console.log('close');
            } else {
                // console.log('dialog result => ', result);
                const widget: Widget = {
                    x: 0, y: 0, rows: 12, cols: 48
                };
                const availableQueries = dialogInstance.availableQueries;
                const queries = dialogInstance.widgetForm.value.queries;
                widget.id = dialogInstance.widgetForm.value.id;
                widget.title = dialogInstance.widgetForm.value.title;
                widget.chartType = dialogInstance.widgetForm.value.type;
                widget.chartVariant = dialogInstance.widgetForm.value.variant;
                widget.chartQueries = [];
                widget.datapointCount = dialogInstance.datapointCount;
                widget.privilege = dialogInstance.widgetForm.value.privilege;
                widget.colorPalette = dialogInstance.widgetForm.value.colorPalette;
                widget.opacity = dialogInstance.widgetForm.value.opacity;

                queries.forEach((query) => {
                    widget.chartQueries.push({ color: query.color, showOnAxis: query.showOnAxis, query: availableQueries.find((option) => option.metadata.name === query.query) });
                });

                // this.onSubmitChart(dialogInstance.widgetForm.value);
                this.layoutService.addItem(widget);
            }
        });
    }

    onItemLockUnlock() {
        this.reportItemUnlock = !this.reportItemUnlock;
        this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ITEM_UNLOCK });
    }

    onEditReportItem() {
        this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ITEM_EDIT });
    }

    onRemoveReportItem() {
        this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ITEM_REMOVE });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    onOthersMenuOpened(){
        this.selectedAction = 'OTHERS';
    }
    onOthersMenuClosed(){
        this.selectedAction = null;
    }
}
