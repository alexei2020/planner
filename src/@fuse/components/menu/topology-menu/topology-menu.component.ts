import { Component, HostListener, ElementRef, ViewChild } from '@angular/core';
import { BreadcrumbService, TopologyViewType } from '../../../../app/main/shared/breadcrumb-bar.service';
import { PropertyTypeEnum } from 'app/main/apps/topology/shared/device';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
@Component({
  selector: 'fuse-topology-menu',
  templateUrl: './topology-menu.component.html',
  styleUrls: ['./topology-menu.component.scss'],
})
export class TopologyMenuComponent {
  propertyDropdownData: any[];
  tooltipForOrientation: string;
  showFilter: boolean = false;
  constructor(
    public _breadcrumbService: BreadcrumbService,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    
    iconRegistry.addSvgIcon('center-on-screen', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/topology/center-on-screen.svg'));
    iconRegistry.addSvgIcon('top-to-bottom', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/topology/top-to-bottom.svg'));
    iconRegistry.addSvgIcon('left-to-right', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/topology/left-to-right.svg'));
    setTimeout(() => {
      // initialize instances
      this._breadcrumbService.instances = {
        vendor: [],
        model: [],
        hostName: [],
        ipv4Net: [],
      };
      // initialize property dropdown
      _breadcrumbService.propertyDropdown.dropdownSettings = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'Deselect All',
        itemsShowLimit: 2,
        allowSearchFilter: false,
      };
      _breadcrumbService.propertyDropdown.dropdownList = [
        {
          item_id: PropertyTypeEnum.Vendor,
          item_text: 'Vendor',
        },
        {
          item_id: PropertyTypeEnum.Model,
          item_text: 'Model',
        },
        {
          item_id: PropertyTypeEnum.Ipv4Net,
          item_text: 'IP',
        },
        {
          item_id: PropertyTypeEnum.HostName,
          item_text: 'Hostname',
        },
      ];
      _breadcrumbService.viewDropdown.dropdownList = [
        {
          item_id: TopologyViewType.Left2Right,
          item_text: 'Left to Right',
        },
        {
          item_id: TopologyViewType.Top2Bottom,
          item_text: 'Top to Bottom',
        },
      ];
      _breadcrumbService.viewDropdown.selectedItem = _breadcrumbService.viewDropdown.dropdownList[0];
      _breadcrumbService.propertyDropdown.selectedItems = [];
      _breadcrumbService.instanceDropdown.dropdownSettings = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'Deselect All',
        itemsShowLimit: 2,
        allowSearchFilter: false,
      };
      _breadcrumbService.instanceDropdown.dropdownList = [];
      _breadcrumbService.instanceDropdown.selectedItems = [];
      this.tooltipForOrientation = this._breadcrumbService.viewDropdown.selectedItem.item_text == 'Left to Right' ? 'Adjust Orientation(Left-to-Right)' : 'Adjust Orientation(Top-to-Bottom)';
    }, 1);
  }

  onPropertyItemSelect($event): void {
    // this.updateInstanceList();
    this._breadcrumbService.shouldUpdateTopology.next(true);
  }

  onPropertySelectAll(items: Array<any>): void {
    this._breadcrumbService.propertyDropdown.selectedItems = items;
    // this.updateInstanceList();
    this._breadcrumbService.shouldUpdateTopology.next(true);
  }

  onPropertyDeSelectAll(items: Array<any>): void {
    this._breadcrumbService.propertyDropdown.selectedItems = items;
    // this.updateInstanceList();
    this._breadcrumbService.shouldUpdateTopology.next(true);
  }

  onPropertyItemDeSelect($event): void {
    // this.updateInstanceList();
    this._breadcrumbService.shouldUpdateTopology.next(true);
  }

  onInstanceItemSelect($event): void {
    // this.updateTopology();
    this._breadcrumbService.shouldUpdateTopology.next(true);
  }

  onInstanceSelectAll(items: Array<any>): void {
    this._breadcrumbService.instanceDropdown.selectedItems = items;
    // this.updateTopology();
    this._breadcrumbService.shouldUpdateTopology.next(true);
  }

  onInstanceDeSelectAll(items: Array<any>): void {
    this._breadcrumbService.instanceDropdown.selectedItems = items;
    // this.updateTopology();
    this._breadcrumbService.shouldUpdateTopology.next(true);
  }

  onInstanceItemDeSelect($event): void {
    // this.updateTopology();
    this._breadcrumbService.shouldUpdateTopology.next(true);
  }

  onViewItemChanged($event): void {
    this._breadcrumbService.shouldUpdateTopology.next(true);
  }

  onFitToScreenChange(): void {
    this._breadcrumbService.shouldFitToScreen.next(this._breadcrumbService.isFitToScreen);
  }
  onFitToScreenClicked(): void {
    this._breadcrumbService.isFitToScreen = !this._breadcrumbService.isFitToScreen;
    this._breadcrumbService.shouldFitToScreen.next(this._breadcrumbService.isFitToScreen);
  }
  onViewChangeClicked(): void{
    const selectedView = this._breadcrumbService.viewDropdown.dropdownList.filter(item=>item.item_id != this._breadcrumbService.viewDropdown.selectedItem.item_id)[0];
    this._breadcrumbService.viewDropdown.selectedItem = selectedView;
    this.tooltipForOrientation = this._breadcrumbService.viewDropdown.selectedItem.item_text == 'Left to Right' ? 'Adjust Orientation(Left-to-Right)' : 'Adjust Orientation(Top-to-Bottom)';
    this._breadcrumbService.shouldUpdateTopology.next(true);
  }
  updateFilters(){

  }
  closeFilter() {
    this.showFilter = false;
  }
  openFilter() {
    this.showFilter = true;
  }
}
