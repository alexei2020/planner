import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { AuditDetailHelperService, REVISION_ACTION, REVISION_STATE } from 'app/resource/audit-detail/services';

@Component({
    selector: 'app-revisions-menu',
    templateUrl: './revisions-menu.component.html',
    styleUrls: ['./revisions-menu.component.scss']
})
export class RevisionsMenuComponent implements OnInit, OnDestroy {
    private _ngUnsubscribe = new Subject();

    public compare = false;
    public view = false;
    public state: REVISION_STATE;
    public showAllControl = new FormControl(false);
    public showCardsControl = new FormControl(true);

    constructor(
        private _auditService: AuditDetailHelperService
    ) { }

    ngOnInit() {
        this.showCardsControl.valueChanges
            .pipe(
                distinctUntilChanged(),
                takeUntil(this._ngUnsubscribe)
            )
            .subscribe(value => {
                this.compare = false;
                this.view = false;
                this._auditService.revisionAction$.next({
                    type: REVISION_ACTION.CARDS,
                    value
                });
            });

        this.showAllControl.valueChanges
            .pipe(
                distinctUntilChanged(),
                takeUntil(this._ngUnsubscribe)
            )
            .subscribe(value => {
                this.compare = false;
                this.view = false;
                this._auditService.revisionAction$.next({
                    type: REVISION_ACTION.ALL,
                    value
                });
                this.showCardsControl.patchValue(true);
            });

        this._auditService.revisionSelect$
            .pipe(takeUntil(this._ngUnsubscribe))
            .subscribe(revisions => {
                switch (revisions.length) {
                    case 1:
                        this.view = true;
                        this.showCardsControl.enable();
                        break;

                    case 2:
                        this.view = false;
                        this.compare = true;
                        this.showCardsControl.enable();
                        break;

                    default:
                        this.view = false;
                        this.compare = false;
                        this.showCardsControl.disable();
                        break;
                }
            });

        this._auditService.revisionShow$
            .pipe(takeUntil(this._ngUnsubscribe))
            .subscribe(() => {
                this.showCardsControl.setValue(false);
            });

        this._auditService.revisionState$
            .pipe(takeUntil(this._ngUnsubscribe))
            .subscribe(state => this.state = state);
    }

    ngOnDestroy() {
        this._ngUnsubscribe.next();
        this._ngUnsubscribe.complete();
    }

    public onView() {
        this.showCardsControl.setValue(false);
    }

    public onDiff() {
        this.showCardsControl.setValue(false);
    }
}
