import { NgModule } from '@angular/core';

import { FuseIfOnDomDirective } from '@fuse/directives/fuse-if-on-dom/fuse-if-on-dom.directive';
import { FuseInnerScrollDirective } from '@fuse/directives/fuse-inner-scroll/fuse-inner-scroll.directive';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { FuseMatSidenavHelperDirective, FuseMatSidenavTogglerDirective } from '@fuse/directives/fuse-mat-sidenav/fuse-mat-sidenav.directive';
import { FuseNavigationMenuGlidsterDirective } from './fuse-navigation-menu-gridster/fuse-navigation-menu-gridster.directive';
import { UniqueValidatorDirective } from './unique-validator.directive';
import { AllowedCharactersValidatorDirective } from './allowed-characters-validator.directive';
import { DenyUppercaseCharactersValidatorDirective } from './deny-uppercase-characters-validator.directive';
import { TrailingCharactersRegexValidatorDirective } from './trailing-characters-validator.directive';
import { IPV4SubnetValidatorDirective } from './ip-v4-subnet-validator.directive';
import { RulesValidatorDirective } from './validator.directive';

@NgModule({
    declarations: [
        FuseIfOnDomDirective,
        FuseInnerScrollDirective,
        FuseMatSidenavHelperDirective,
        FuseMatSidenavTogglerDirective,
        FusePerfectScrollbarDirective,
        FuseNavigationMenuGlidsterDirective,
        UniqueValidatorDirective,
        AllowedCharactersValidatorDirective,
        DenyUppercaseCharactersValidatorDirective,
        TrailingCharactersRegexValidatorDirective,
        IPV4SubnetValidatorDirective,
	RulesValidatorDirective
    ],
    imports     : [],
    exports     : [
        FuseIfOnDomDirective,
        FuseInnerScrollDirective,
        FuseMatSidenavHelperDirective,
        FuseMatSidenavTogglerDirective,
        FusePerfectScrollbarDirective,
        FuseNavigationMenuGlidsterDirective,
        UniqueValidatorDirective,
        AllowedCharactersValidatorDirective,
        DenyUppercaseCharactersValidatorDirective,
        TrailingCharactersRegexValidatorDirective,
        IPV4SubnetValidatorDirective,
	RulesValidatorDirective
    ]
})
export class FuseDirectivesModule
{
}
