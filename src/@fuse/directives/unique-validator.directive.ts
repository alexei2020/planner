import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { map } from 'lodash';

export const uniqueValidationKey = 'unique';

@Directive({
  selector: '[appUnique]',
  providers: [{ provide: NG_VALIDATORS, useExisting: UniqueValidatorDirective, multi: true }]
})
export class UniqueValidatorDirective implements Validator {
  @Input('appUnique')
  values: string[];

  validate(control: AbstractControl): ValidationErrors | null {
    const validator = this.uniqueValidator(this.values);

    const result = validator(control);

    return result;
  }

  private uniqueValidator(values: string[], isIgnoreCase: boolean = true): ValidatorFn {
    const existingValues = isIgnoreCase ? map(values, (value) => value.toLowerCase()) : values;
  
    const validator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
      const currentValue = isIgnoreCase && control.value ? control.value.toLowerCase() : control.value;
      const isValid = existingValues.indexOf(currentValue) === -1;
      return !isValid ? { [uniqueValidationKey]: true } : null;
    };
  
    return validator;
  }
}
