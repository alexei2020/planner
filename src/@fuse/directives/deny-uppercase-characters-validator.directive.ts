import { Directive } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

export const denyUppercaseCharactersValidationKey = 'denyUppercaseCharacters';

@Directive({
  selector: '[appDenyUppercaseCharacters]',
  providers: [{ provide: NG_VALIDATORS, useExisting: DenyUppercaseCharactersValidatorDirective, multi: true }]
})
export class DenyUppercaseCharactersValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    const validator = this.denyUppercaseCharactersValidator();

    const result = validator(control);

    return result;
  }

  private denyUppercaseCharactersValidator(): ValidatorFn {
    const validationPattern = new RegExp(`[A-Z]`, 'g');
  
    const validator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
      const hasUppercase = validationPattern.test(control.value);
      return hasUppercase ? { [denyUppercaseCharactersValidationKey]: true } : null;
    };
  
    return validator;
  }
}
