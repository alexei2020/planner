import {
    Directive,
    OnDestroy,
    AfterViewInit,
    Input,
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GridsterConfig } from 'angular-gridster2';
import { NavigationService } from 'app/services/navigation.service';

@Directive({
    selector: '[fuseNavigationMenuGlidster]',
})
export class FuseNavigationMenuGlidsterDirective
    implements OnDestroy, AfterViewInit {
        
    @Input()
    options: GridsterConfig;

    private destroyed$: Subject<any>;

    constructor(private navService: NavigationService) {
        this.destroyed$ = new Subject();
    }

    ngAfterViewInit(): void {
        this.navService.menuStateToggled
            .pipe(takeUntil(this.destroyed$))
            .subscribe(() => {
                if (this.options) {
                    this.options.api.resize();
                    setTimeout(() => {
                        this.options.api.resize();
                    }, 300);
                }
            });
    }

    ngOnDestroy(): void {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
}
