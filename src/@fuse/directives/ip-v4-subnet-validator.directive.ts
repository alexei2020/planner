import { Directive } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

export const ipV4SubnetValidationKey = 'ipV4Subnet';

@Directive({
  selector: '[appIPV4Subnet]',
  providers: [{ provide: NG_VALIDATORS, useExisting: IPV4SubnetValidatorDirective, multi: true }]
})
export class IPV4SubnetValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    const validator = this.denyUppercaseCharactersValidator();

    const result = validator(control);

    return result;
  }

  private denyUppercaseCharactersValidator(): ValidatorFn {
    const validationPattern = new RegExp(`^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$`, 'ig');
  
    const validator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
      if (control.value === undefined || control.value === null || control.value === '') {
        return null;
      }

      const isValid = validationPattern.test(control.value);
      return !isValid ? { [ipV4SubnetValidationKey]: true } : null;
    };
  
    return validator;
  }
}
