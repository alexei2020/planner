import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[appValidateRules]',
  providers: [{ provide: NG_VALIDATORS, useExisting: RulesValidatorDirective, multi: true }]
})
export class RulesValidatorDirective implements Validator {
  @Input('validateRules')
  validateRules: { name: string, type: string, value: string }[];

  validate(control: AbstractControl): ValidationErrors | null {
    var result: any = null;
    this.validateRules.forEach( val => {
    	var validator: ValidatorFn;
	switch(val.type) {
	case 'pattern':
	    validator = this.patternValidator(val.name, val.value);
	    break;
	case 'range':
	    const range = val.value.split(" ").join("");
	    const min = parseInt(range.split("-")[0]);
	    const max = parseInt(range.split("-")[1]);
	    validator = this.rangeValidator(val.name, min, max);
	    break;
	default:
	    console.log("Unsupported validation type:", val);
	    return;
	}

	if (result === null) {
    		result = validator(control);
	}
    });

    return result;
  }

  private patternValidator(name: string, pattern: string): ValidatorFn {
    const validationPattern = new RegExp(pattern);
  
    const validator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
      if (control.value === undefined || control.value === null || !control.value.length) {
        return null;
      }
      const isValid = validationPattern.test(control.value);
      return !isValid ? { [name]: true } : null;
    };
  
    return validator;
  }

  private rangeValidator(name: string, min, max: number): ValidatorFn {
  
    const validator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
      console.log("validator: ", min, max);
      if (control.value === undefined || control.value === null || !control.value.length) {
        return null;
      }
      const isValid = (control.value >= min && control.value <= max);
      return !isValid ? { [name]: true } : null;
    };
  
    return validator;
  }
}
