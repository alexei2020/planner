import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

export const allowedCharactersValidationKey = 'allowedCharacters';

@Directive({
  selector: '[appAllowedCharacters]',
  providers: [{ provide: NG_VALIDATORS, useExisting: AllowedCharactersValidatorDirective, multi: true }]
})
export class AllowedCharactersValidatorDirective implements Validator {
  @Input('appAllowedCharacters')
  allowedCharacters: string[];

  validate(control: AbstractControl): ValidationErrors | null {
    const validator = this.allowedCharactersValidator(this.allowedCharacters);

    const result = validator(control);

    return result;
  }

  private allowedCharactersValidator(allowedCharacters: string[]): ValidatorFn {
    const characters = allowedCharacters.join('\\');
    const validationPattern = new RegExp(`^[\\w\\s${characters}]*$`, 'i');
  
    const validator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
      const isValid = validationPattern.test(control.value);
      return !isValid ? { [allowedCharactersValidationKey]: true } : null;
    };
  
    return validator;
  }
}
