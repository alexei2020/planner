import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

export const trailingCharactersValidationKey = 'trailingCharacters';

@Directive({
  selector: '[appTrailingCharactersRegex]',
  providers: [{ provide: NG_VALIDATORS, useExisting: TrailingCharactersRegexValidatorDirective, multi: true }]
})
export class TrailingCharactersRegexValidatorDirective implements Validator {
  @Input('appTrailingCharactersRegex')
  trailingCharactersRegex: string[];

  validate(control: AbstractControl): ValidationErrors | null {
    const validator = this.trailingCharactersRegexValidator(this.trailingCharactersRegex);

    const result = validator(control);

    return result;
  }

  private trailingCharactersRegexValidator(trailingCharactersRegex: string[]): ValidatorFn {
    const characters = trailingCharactersRegex.join('');
    const validationPattern = new RegExp(`^[${characters}]*$`, 'g');
  
    const validator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
      if (control.value === undefined || control.value === null || !control.value.length) {
        return null;
      }
      const testValue = control.value[0] + control.value[control.value.length - 1];
      const isValid = validationPattern.test(testValue);
      return !isValid ? { [trailingCharactersValidationKey]: true } : null;
    };
  
    return validator;
  }
}
