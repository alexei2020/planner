import { NgModule } from '@angular/core';

import { KeysPipe } from './keys.pipe';
import { GetByIdPipe } from './getById.pipe';
import { HtmlToPlaintextPipe } from './htmlToPlaintext.pipe';
import { FilterPipe } from './filter.pipe';
import { CamelCaseToDashPipe } from './camelCaseToDash.pipe';
import { NotIncludedCharactersPipe } from './not-included-characters.pipe';
import { JoinPipe } from './join.pipe';
import { FileSizePipe } from './file-size.pipe';

@NgModule({
    declarations: [
        KeysPipe,
        GetByIdPipe,
        HtmlToPlaintextPipe,
        FilterPipe,
        CamelCaseToDashPipe,
        NotIncludedCharactersPipe,
        JoinPipe,
        FileSizePipe
    ],
    imports     : [],
    exports     : [
        KeysPipe,
        GetByIdPipe,
        HtmlToPlaintextPipe,
        FilterPipe,
        CamelCaseToDashPipe,
        NotIncludedCharactersPipe,
        JoinPipe,
        FileSizePipe
    ]
})
export class FusePipesModule
{
}
