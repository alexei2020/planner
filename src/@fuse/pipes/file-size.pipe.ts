import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileSize',
})
export class FileSizePipe implements PipeTransform {
  private readonly sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  transform(value: number): string {
    const format = this.getFormattedBytes(value);

    return format;
  }

  // Utility method to format bytes
  private getFormattedBytes(bytes: number, decimals: number = 2): string {
    if (!bytes) {
      return `0 ${this.sizes[0]}`;
    }

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    const fl = parseFloat((bytes / Math.pow(k, i)).toFixed(dm));

    return `${fl} ${this.sizes[i]}`;
  }
}
