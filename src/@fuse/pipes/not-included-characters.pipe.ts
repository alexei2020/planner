import { Pipe, PipeTransform } from '@angular/core';
import { isEmpty, uniq } from 'lodash';

@Pipe({
  name: 'notIncludedCharacters'
})
export class NotIncludedCharactersPipe implements PipeTransform {
  transform(value: string, characters: string[]): string[] {
    if (!value || isEmpty(characters)) {
      return [];
    }

    const escapedCharacters = characters.join('\\');
    const replacePattern = new RegExp(`[\\w\\s${escapedCharacters}]+`, 'ig');

    const notIncludedCharacters = value.replace(replacePattern, '').split('');

    const uniqNotIncludedCharacters = uniq(notIncludedCharacters);

    return uniqNotIncludedCharacters;
  }
}
