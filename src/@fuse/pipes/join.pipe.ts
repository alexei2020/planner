import { Pipe, PipeTransform } from '@angular/core';
import { isEmpty } from 'lodash';

@Pipe({
  name: 'join'
})
export class JoinPipe implements PipeTransform {
  transform(values: string[], separator?: string): string {
    if (isEmpty(values)) {
      return '';
    }

    const joinedValues = values.join(separator);

    return joinedValues;
  }
}
