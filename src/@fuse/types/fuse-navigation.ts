import { MatMenuTrigger } from '@angular/material/menu';
import { DrawerItem } from '@progress/kendo-angular-layout';

export interface FuseNavigationItem extends DrawerItem
{
    id: string;
    text: string;
    type: 'item' | 'group' | 'collapsable';
    translate?: string;
    icon?: string;
    customIcon?: string
    hidden?: boolean;
    url?: string;
    classes?: string;
    exactMatch?: boolean;
    externalUrl?: boolean;
    openInNewTab?: boolean;
    function?: any;
    badge?: {
        title?: string;
        translate?: string;
        bg?: string;
        fg?: string;
    };
    items?: FuseNavigationItem[];

    disabled?: boolean;
    selected?: boolean;
    separator?: boolean;
}

export class FuseMenuCollapsableInfo {
    readonly menuLevel: number;
    readonly matMenuTrigger: MatMenuTrigger;

    constructor({ menuLevel, matMenuTrigger }: Partial<FuseMenuCollapsableInfo> = {}) {
        this.menuLevel = menuLevel;
        this.matMenuTrigger = matMenuTrigger;
    }
}

export enum FuseMenuType {
    None = 'None',
    IconOnly = 'IconOnly',
    IconWithText = 'IconWithText',
    Overlapping = 'Overlapping'
}
