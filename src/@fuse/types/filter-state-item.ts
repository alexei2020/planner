export class FilterStateItem {
  readonly field: string;
  readonly name: string;
  keys: FilterStateItemKey[];
  checked: boolean;

  constructor({ field, name, keys, checked }: Partial<FilterStateItem> = {}) {
    this.field = field;
    this.name = name;
    this.keys = keys;
    this.checked = checked;
  }
}

export class FilterStateItemKey {
  readonly name: string;
  readonly keyName: string;
  readonly count: number;
  checked: boolean;

  constructor({ name, keyName, count, checked }: Partial<FilterStateItemKey> = {}) {
    this.name = name;
    this.keyName = keyName;
    this.count = count;
    this.checked = checked;
  }
}
