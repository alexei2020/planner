import { Component, OnInit, OnDestroy } from '@angular/core';

import { NavigationService } from '../../../services/navigation.service';
import { FuseMenuType } from '@fuse/types';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatButtonToggleChange } from '@angular/material/button-toggle';

@Component({
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit, OnDestroy {
    isMenuWithTextChecked: boolean;

    get FuseMenuType(): typeof FuseMenuType {
        return FuseMenuType;
    }

    private unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(private navService: NavigationService) {}

    ngOnInit(): void {
        this.isMenuWithTextChecked = this.navService.menuType === FuseMenuType.IconWithText;

        this.navService.menuTypeChanged
            .pipe(takeUntil(this.unsubscribeAll))
            .subscribe((menuType: FuseMenuType) => {
                this.isMenuWithTextChecked = menuType === FuseMenuType.IconWithText;
            });
    }

    ngOnDestroy(): void {
        this.unsubscribeAll.next();
        this.unsubscribeAll.complete();
    }

    onMenuWithTextChanged(isMenuWithTextChecked: boolean): void {
        const menuType = isMenuWithTextChecked ? FuseMenuType.IconWithText : FuseMenuType.IconOnly;
        this.navService.changeMenuType(menuType);
    }
}
