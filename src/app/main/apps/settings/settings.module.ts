import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { FuseWidgetModule } from '@fuse/components';
import { GridsterModule } from 'angular-gridster2';
import { Route, RouterModule } from '@angular/router';

import { SettingsComponent } from './settings.component';
import { FormsModule } from '@angular/forms';
import { SwitchModule } from '@progress/kendo-angular-inputs';

export const SETTINGS_ROUTE: Route =
    {
        path: '',
        component: SettingsComponent,
    };

@NgModule({
    imports: [
        CommonModule,
        GridsterModule,
        FuseWidgetModule,
        MatIconModule,
        MatButtonModule,
        SwitchModule,
        RouterModule.forChild([SETTINGS_ROUTE]),
        MatCheckboxModule,
        FormsModule
    ],
    declarations: [SettingsComponent],
})
export class SettingsModule {}
