import { Injectable } from '@angular/core';
import {
  DeviceNode,
  Device,
  DevicePair,
  PropertyTypeEnum,
  NodeTypeEnum,
  PropertyNamePair,
  LeafDevices,
  LeafDevicesNodes,
  DeviceProperties,
} from './shared/device';
import _ from 'lodash';
import { LocationDataItem } from 'app/components/shared/location-item';
import { FilterStateItem, FilterStateItemKey } from '@fuse/types/filter-state-item';
import { StringMap } from 'app/typings/backendapi.gen';

export interface IMultiParent {
  parent: any;
  child: any;
  isCollapsed: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class TopologyDataManagementService {
  duplicateNodes: DevicePair[];
  exploreModeDuplicateNodes: DevicePair[];

  buildTopologyTreeData(
    leafDevicesList: LeafDevices[],
    currentLocationSubTree: LocationDataItem,
    collapsedItems: string[] = [],
    isRootCollapsed: boolean = false
  ): DeviceNode {
    const leafDevicesNodes: LeafDevicesNodes[] = [];

    _.forEach(leafDevicesList, (leafDevices) => {
      this.duplicateNodes = [];
      const devices = leafDevices.devices;

      const filteredDevices = _.filter(
        devices,
        (item: Device) => item.status.role.toLowerCase() === NodeTypeEnum.Distribution
      );
      const items = _.reduce(
        filteredDevices,
        (result: DeviceNode[], item: Device) => {
          const isCollapsed = _.includes(collapsedItems, item.metadata.name);
          const deviceNodes = this.buildAccessLevelData(devices, item.metadata.name, result, collapsedItems);
          const node = new DeviceNode({
            name: item.metadata.name,
            hiddenChildren: deviceNodes,
            children: !isCollapsed ? deviceNodes : [],
            isCollapsed: isCollapsed,
            ...item.status,
          });
          result.push(node);
          return result;
        },
        []
      );

      leafDevicesNodes.push({
        leafName: leafDevices.leafName,
        devicesNodes: items,
      });
    });
    if (_.isEmpty(currentLocationSubTree.children)) {
      const items = _.first(leafDevicesNodes).devicesNodes;
      const node: DeviceNode = new DeviceNode({
        name: currentLocationSubTree.name,
        hiddenChildren: items,
        children: !isRootCollapsed ? items : [],
        isRoot: true,
        isCollapsed: isRootCollapsed,
      });

      return node;
    }

    const children = this.buildDeviceNodes(currentLocationSubTree.children, leafDevicesNodes, collapsedItems);

    const deviceNode: DeviceNode = new DeviceNode({
      name: currentLocationSubTree.name,
      hiddenChildren: children,
      children: !isRootCollapsed ? children : [],
      isRoot: true,
      isCollapsed: isRootCollapsed,
    });

    return deviceNode;
  }

  private buildDeviceNodes(
    children: LocationDataItem[],
    leafDevicesNodes: LeafDevicesNodes[],
    collapsedItems: string[] = []
  ): DeviceNode[] {
    const newDeviceNodes: DeviceNode[] = [];

    _.forEach(children, (child) => {
      let items: DeviceNode[] = [];

      const isCollapsed = _.includes(collapsedItems, child.name);
      if (!_.isEmpty(child.children)) {
        items = this.buildDeviceNodes(child.children, leafDevicesNodes, collapsedItems);
        newDeviceNodes.push(
          new DeviceNode({
            name: child.name,
            hiddenChildren: items,
            children: !isCollapsed ? items : [],
            isSubRoot: true,
            isCollapsed: isCollapsed,
          })
        );
      }

      const leaf = _.filter(leafDevicesNodes, (item) => item.leafName === child.value);
      if (!_.isEmpty(leaf)) {
        const leafItem = _.first(leaf);
        if (leafItem.leafName === child.value) {
          newDeviceNodes.push(
            new DeviceNode({
              name: child.name,
              hiddenChildren: leafItem.devicesNodes,
              children: !isCollapsed ? leafItem.devicesNodes : [],
              isSubRoot: true,
              isCollapsed: isCollapsed,
            })
          );
        }
      }
    });
    return newDeviceNodes;
  }

  private buildAccessLevelData(
    devices: Device[],
    nodeMetadataName: string,
    deviceNodes: DeviceNode[],
    collapsedItems: string[] = []
  ): DeviceNode[] {
    const filteredDevices = _.chain(devices)
      .filter((item: Device) => item.status.role.toLowerCase() === NodeTypeEnum.Access)
      .filter((device: Device) => {
        if (!device.status.frontIntfs) {
          return false;
        }
        const deviceLinks = Object.values(device.status.frontIntfs);
        const remoteDevicesNames = _.map(deviceLinks, (link: any) => {
          if (!_.isEmpty(link.remoteDevice)) {
            return link.remoteDevice;
          }
        });
        const isExist = this.isExist(deviceNodes, device.metadata.name);
        if (remoteDevicesNames.includes(nodeMetadataName) && isExist) {
          this.duplicateNodes.push(
            new DevicePair({
              parentName: nodeMetadataName,
              childName: device.metadata.name,
            })
          );
        }
        return remoteDevicesNames.includes(nodeMetadataName) && !isExist;
      })
      .value();

    const children = _.map(filteredDevices, (node: Device) => {
      const isCollapsed = _.includes(collapsedItems, node.metadata.name);
      const childrenNodes = this.buildWirelessAccessPointLevelData(devices, node, deviceNodes);

      return new DeviceNode({
        name: node.metadata.name,
        hiddenChildren: childrenNodes,
        children: !isCollapsed ? childrenNodes : [],
        isCollapsed: isCollapsed,
        ...node.status,
      });
    });
    return children;
  }

  public getFlatDevices(devices: DeviceNode[]): any[] {
    const flatDevices = _.reduce(
      devices,
      (innerFlatDevices, device) => {
        if (device.children && device.children.length > 0) {
          const deviceChildren = this.getFlatDevices(device.children);
          innerFlatDevices = [...innerFlatDevices, ...deviceChildren];
        }
        innerFlatDevices.push(device);
        return innerFlatDevices;
      },
      []
    );

    return flatDevices;
  }

  public getMultiParents(root: any, deviceMultiPairs: DevicePair[]): IMultiParent[] {
    const multiParents: IMultiParent[] = [];
    const flatDevices = this.getFlatDevices(root.children);

    _.forEach(deviceMultiPairs, (item) => {
      const parent = _.find(flatDevices, (device) => device.data.name === item.parentName);
      const child = _.find(flatDevices, (device) => device.data.name === item.childName);

      if (parent && child) {
        multiParents.push({
          parent: parent,
          child: child,
          isCollapsed: parent.data.isCollapsed,
        } as IMultiParent);
      }
    });
    return multiParents;
  }

  getOnlyRootAndSubRoot(nodes: DeviceNode[]): DeviceNode[] {
    const children: DeviceNode[] = [];
    _.forEach(nodes, (node) => {
      if (node.isRoot || node.isSubRoot) {
        children.push(
          new DeviceNode({
            ...node,
            children: this.getOnlyRootAndSubRoot(node.children),
          })
        );
      }
    });
    return children;
  }

  getFilteredDeviceNode(filterStateItems: FilterStateItem[], deviceNode: DeviceNode): DeviceNode {
    const checkedFilterStateItems = _.filter(filterStateItems, (item) => {
      const filteredKeys = _.filter(item.keys, (key) => key.checked);
      return !_.isEmpty(filteredKeys);
    });
    if (_.isEmpty(checkedFilterStateItems)) {
      return deviceNode;
    }
    _.forEach(checkedFilterStateItems, (item) => {
      const filteredKeys = _.filter(item.keys, (key) => key.checked);
      item.keys = filteredKeys;
    });
    const mappedFilterStateItems = _.map(checkedFilterStateItems, (item) => {
      const checkedKeys = _.map(item.keys, (key) => key.keyName);
      const stringMap: StringMap<string[]> = {
        [item.field]: checkedKeys,
      };
      return stringMap;
    });
    const filteredDeviceNodes = this.getFilteredDevices(mappedFilterStateItems, deviceNode.children);

    let children: DeviceNode[] = [];
    if (_.isEmpty(filteredDeviceNodes)) {
      children = this.getOnlyRootAndSubRoot(deviceNode.children);
    } else {
      children = filteredDeviceNodes;
    }

    return {
      ...deviceNode,
      children: children,
    };
  }

  getFilteredDevices(mappedFilterStateItems: StringMap<string[]>[], devices: DeviceNode[]): DeviceNode[] {
    const model = _.find(mappedFilterStateItems, (item) => item[PropertyTypeEnum.Model]);
    const vendor = _.find(mappedFilterStateItems, (item) => item[PropertyTypeEnum.Vendor]);
    const hostName = _.find(mappedFilterStateItems, (item) => item[PropertyTypeEnum.HostName]);
    const ipv4Net = _.find(mappedFilterStateItems, (item) => item[PropertyTypeEnum.Ipv4Net]);

    const filteredDevices = _.filter(devices, (device) => {
      if (!_.isEmpty(device.children)) {
        device.children = this.getFilteredDevices(mappedFilterStateItems, device.children);
        const isChildrenMatched = !_.isEmpty(device.children);
        if (isChildrenMatched) {
          return true;
        }
      }

      const modelCondition = !_.isEmpty(model) && _.indexOf(model[PropertyTypeEnum.Model], device.model) !== -1;
      const hostNameCondition =
        !_.isEmpty(hostName) && _.indexOf(hostName[PropertyTypeEnum.HostName], device.hostName) !== -1;
      const vendorCondition = !_.isEmpty(vendor) && _.indexOf(vendor[PropertyTypeEnum.Vendor], device.vendor) !== -1;
      const ipv4NetCondition =
        !_.isEmpty(ipv4Net) && _.indexOf(ipv4Net[PropertyTypeEnum.Ipv4Net], device.ipv4Net) !== -1;

      return (
        modelCondition || hostNameCondition || vendorCondition || ipv4NetCondition || device.isSubRoot || device.isRoot
      );
    });

    return filteredDevices;
  }

  getFilteredExploredDeviceNode(nodeName: string, deviceNode: DeviceNode): DeviceNode {
    const originalDeviceNodes = _.cloneDeep(deviceNode.children);
    let filteredDeviceNodes = this.getFilteredExploredDevices(nodeName, deviceNode.children);
    filteredDeviceNodes = this.updateMultiParentsNodes(originalDeviceNodes, filteredDeviceNodes);

    const filteredExploredDevices = {
      ...deviceNode,
      children: filteredDeviceNodes,
    };

    return filteredExploredDevices;
  }

  getDeviceNodeLeaves(dataItem: any): string[] {
    if (_.isEmpty(dataItem)) {
      return [];
    }
    if (_.isEmpty(dataItem.children)) {
      return [dataItem.value];
    }
    const leaves = this.getLeaves(dataItem.children);
    return leaves;
  }

  public getLeaves(children: any[]): string[] {
    const leaves = _.reduce(
      children,
      (innerLeaves, item) => {
        if (item.children && item.children.length > 0) {
          const itemChildren = this.getLeaves(item.children);
          innerLeaves = [...innerLeaves, ...itemChildren];
        } else {
          innerLeaves.push(item.value);
        }
        return innerLeaves;
      },
      []
    );

    return leaves;
  }

  private updateMultiParentsNodes(originalDeviceNodes: DeviceNode[], filteredDeviceNodes: DeviceNode[]): DeviceNode[] {
    const flatOriginalDevicesNodes = this.getFlatDevices(originalDeviceNodes);
    const flatfilteredDevicesNode = this.getFlatDevices(filteredDeviceNodes);

    this.exploreModeDuplicateNodes = [];

    _.forEach(this.duplicateNodes, (duplicateNode) => {
      const parent = _.filter(flatfilteredDevicesNode, (node) => node.name === duplicateNode.parentName);
      let child = _.filter(flatfilteredDevicesNode, (node) => node.name === duplicateNode.childName);

      const isParentWithoutChild = !_.isEmpty(parent) && _.isEmpty(child);

      if (!isParentWithoutChild) {
        this.exploreModeDuplicateNodes.push(duplicateNode);
        return;
      }
      child = _.filter(flatOriginalDevicesNodes, (node) => node.name === duplicateNode.childName);

      if (_.isEmpty(child)) {
        this.exploreModeDuplicateNodes.push(duplicateNode);
        return;
      }
      this.putChildToParentNode(filteredDeviceNodes, child[0], duplicateNode.parentName);
    });

    return filteredDeviceNodes;
  }

  private putChildToParentNode(filteredDeviceNodes: DeviceNode[], child: DeviceNode, parentName: string): DeviceNode[] {
    _.forEach(filteredDeviceNodes, (node) => {
      if (node.name === parentName) {
        if (_.isEmpty(node.children)) {
          node.children = [];
        }
        node.children.push(child);
        return false;
      }
      if (!_.isEmpty(node.children)) {
        this.putChildToParentNode(node.children, child, parentName);
      }
    });

    return filteredDeviceNodes;
  }

  getFilteredExploredDevices(nodeName: string, devices: DeviceNode[]): DeviceNode[] {
    const filteredDevices = _.map(devices, (device) => {
      if (device.name === nodeName) {
        device.isExploredNode = true;
      }

      if (!_.isEmpty(device.children) && device.name !== nodeName) {
        const isItemContainsExploredNode = this.isNodeContainsExploredNode(device.children, nodeName);
        device.children = this.getFilteredExploredDevices(nodeName, device.children);
        const isChildrenMatched = !_.isEmpty(device.children);
        if (isChildrenMatched || isItemContainsExploredNode) {
          return device;
        }
      }

      if (device.name === nodeName) {
        device.children = _.map(
          device.children,
          (childNode) =>
            new DeviceNode({
              ...childNode,
              children: [],
            })
        );
        return device;
      }
      return null;
    }).filter((x) => x !== null);

    return filteredDevices;
  }

  private isNodeContainsExploredNode(nodes: DeviceNode[], exploredNodeName: string): boolean {
    let isContain: boolean;
    _.forEach(nodes, (item) => {
      if (item.name === exploredNodeName) {
        isContain = true;
        return false;
      }
      if (!_.isEmpty(item.children)) {
        const isItemContainsExploredNode = this.isNodeContainsExploredNode(item.children, exploredNodeName);
        if (isItemContainsExploredNode) {
          isContain = true;
          return false;
        }
      }
    });
    return isContain;
  }

  getCollapsedItems(nodes: DeviceNode[]): string[] {
    let collapsedItemNames: string[] = [];
    _.forEach(nodes, (node: DeviceNode) => {
      if (!_.isEmpty(node.hiddenChildren)) {
        collapsedItemNames = collapsedItemNames.concat(this.getCollapsedItems(node.hiddenChildren));
      }
      if (node.isCollapsed) {
        collapsedItemNames.push(node.name);
      }
    });
    return collapsedItemNames;
  }

  getFilterStateItems(devices: Device[], filterStateItems: FilterStateItem[]): FilterStateItem[] {
    const deviceProperties = this.getDeviceProperties(devices);

    const hostNameKey = 'hostName';
    const ipv4NetKey = 'ipv4Net';
    const modelKey = 'model';
    const vendorKey = 'vendor';
    _.forEach(filterStateItems, (item) => {
      let keys: FilterStateItemKey[] = [];
      if (item.field === PropertyTypeEnum.HostName) {
        keys = this.getFilterStateKeys(devices, deviceProperties.hostNames, hostNameKey);
      }
      if (item.field === PropertyTypeEnum.Ipv4Net) {
        keys = this.getFilterStateKeys(devices, deviceProperties.mgmtIps, ipv4NetKey);
      }
      if (item.field === PropertyTypeEnum.Model) {
        keys = this.getFilterStateKeys(devices, deviceProperties.models, modelKey);
      }
      if (item.field === PropertyTypeEnum.Vendor) {
        keys = this.getFilterStateKeys(devices, deviceProperties.vendors, vendorKey);
      }
      item.keys = keys;
    });
    return filterStateItems;
  }

  private getDeviceProperties(devices: Device[]): DeviceProperties {
    const vendors: string[] = [];
    const hostNames: string[] = [];
    const mgmtIps: string[] = [];
    const models: string[] = [];

    _.forEach(devices, (item) => {
      if (vendors.length === 0 || vendors.indexOf(item.status.vendor) === -1) {
        vendors.push(item.status.vendor);
      }
      if (models.length === 0 || models.indexOf(item.status.model) === -1) {
        models.push(item.status.model);
      }
      if (hostNames.length === 0 || hostNames.indexOf(item.status.hostName) === -1) {
        hostNames.push(item.status.hostName);
      }
      if (mgmtIps.length === 0 || mgmtIps.indexOf(item.status.ipv4Net) === -1) {
        mgmtIps.push(item.status.ipv4Net);
      }
    });

    const deviceProperties = new DeviceProperties({
      hostNames: hostNames,
      mgmtIps: mgmtIps,
      models: models,
      vendors: vendors,
    });
    return deviceProperties;
  }

  private getFilterStateKeys(devices: Device[], items: string[], key: string): FilterStateItemKey[] {
    const keys: FilterStateItemKey[] = [];
    _.forEach(items, (item) => {
      const filteredDevices = _.filter(devices, (device) => device.status[key] === item);
      const count = filteredDevices.length;
      const name = `${item} (${count})`;
      keys.push(
        new FilterStateItemKey({
          name: name,
          keyName: item,
          count: count,
          checked: false,
        })
      );
    });
    return keys;
  }

  private getInstanceNames(instances: PropertyNamePair[], propertyType: PropertyTypeEnum): string[] {
    const names = _.filter(instances, (instance) => instance.property === propertyType).map(
      (instance) => instance.name
    );

    return names;
  }

  private isExist(deviceNodes: DeviceNode[], nodeMetadataName: string): boolean {
    const flatDevices = this.getFlatDevices(deviceNodes);
    const found = _.find(flatDevices, (item) => item.name === nodeMetadataName);

    return !_.isEmpty(found);
  }

  private buildWirelessAccessPointLevelData(
    devices: Device[],
    deviceAccessNode: Device,
    deviceNodes: DeviceNode[]
  ): DeviceNode[] {
    if (!deviceAccessNode.status.frontIntfs) {
      return [];
    }
    const deviceLinks = Object.values(deviceAccessNode.status.frontIntfs);
    const remoteDevicesNames = _.filter(deviceLinks, (link: any) => !_.isEmpty(link.remoteDevice)).map(
      (link) => link.remoteDevice
    );

    const filterDevices = _.chain(devices)
      .filter((item: Device) => item.status.role.toLowerCase() === NodeTypeEnum.WirelessAccessPoint)
      .filter((innerDevice) => {
        const isExist = this.isExist(deviceNodes, innerDevice.metadata.name);
        if (remoteDevicesNames.includes(innerDevice.metadata.name) && isExist) {
          this.duplicateNodes.push(
            new DevicePair({
              parentName: deviceAccessNode.metadata.name,
              childName: innerDevice.metadata.name,
            })
          );
        }
        return remoteDevicesNames.includes(innerDevice.metadata.name) && !isExist;
      })
      .value();

    const children = _.map(
      filterDevices,
      (node) =>
        new DeviceNode({
          name: node.metadata.name,
          ...node.status,
        })
    );
    return children;
  }
}
