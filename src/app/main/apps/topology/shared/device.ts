import { INode } from './node';

export interface IDevices {
  apiVersion: string;
  items: IDevice[];
}

export interface IDevice {
  apiVersion: string;
  kind: string;
  metadata: IDeviceMetadata;
  status: IDeviceStatus;
}

export interface IDeviceMetadata {
  creationTimestamp: string;
  name: string;
  resourceVersion: string;
  selfLink: string;
  uid: string;
}

export interface IDeviceStatus {
  hostName: string;
  ipv4Net: string;
  model: string;
  pltfmCatalog: string;
  rearIntfs: any;
  frontIntfs: any;
  role: string;
  type: string;
  vendor: string;
}

export class Devices {
  readonly apiVersion: string;
  readonly items: Device[];

  constructor({ apiVersion, items }: Partial<Devices> = {}) {
    this.apiVersion = apiVersion;
    this.items = items;
  }
}

export class Device {
  readonly apiVersion: string;
  readonly kind: string;
  readonly metadata: DeviceMetadata;
  readonly status: DeviceStatus;

  constructor({ apiVersion, kind, metadata, status }: Partial<Device> = {}) {
    this.apiVersion = apiVersion;
    this.kind = kind;
    this.metadata = metadata;
    this.status = status;
  }
}

export class DeviceStatus {
  readonly hostName: string;
  readonly ipv4Net: string;
  readonly model: string;
  readonly pltfmCatalog: string;
  readonly rearIntfs: any;
  readonly frontIntfs: any;
  readonly role: string;
  readonly type: string;
  readonly vendor: string;

  constructor({
    hostName,
    ipv4Net,
    model,
    pltfmCatalog,
    rearIntfs,
    frontIntfs,
    role,
    type,
    vendor,
  }: Partial<DeviceStatus> = {}) {
    this.hostName = hostName;
    this.ipv4Net = ipv4Net;
    this.model = model;
    this.pltfmCatalog = pltfmCatalog;
    this.rearIntfs = rearIntfs;
    this.frontIntfs = frontIntfs;
    this.role = role;
    this.type = type;
    this.vendor = vendor;
  }
}

export class DeviceMetadata {
  readonly creationTimestamp: string;
  readonly name: string;
  readonly resourceVersion: string;
  readonly selfLink: string;
  readonly uid: string;

  constructor({ creationTimestamp, name, resourceVersion, selfLink, uid }: Partial<DeviceMetadata> = {}) {
    this.creationTimestamp = creationTimestamp;
    this.name = name;
    this.resourceVersion = resourceVersion;
    this.selfLink = selfLink;
    this.uid = uid;
  }
}

export class DeviceNode {
  readonly name: string;
  readonly hostName: string;
  readonly id: string;
  readonly ipv4Net: string;
  readonly macAddress: string;
  readonly model: string;
  readonly ports: [];
  readonly role: string;
  readonly serialNo: string;
  readonly vendor: string;
  readonly isRoot: boolean;
  readonly isSubRoot: boolean;
  children: DeviceNode[];
  hiddenChildren: DeviceNode[];
  isCollapsed: boolean;
  isExploredNode: boolean;

  constructor({
    name,
    hostName,
    id,
    ipv4Net,
    macAddress,
    model,
    ports,
    role,
    serialNo,
    vendor,
    isRoot,
    isSubRoot,
    children,
    hiddenChildren,
    isCollapsed,
    isExploredNode,
  }: Partial<DeviceNode> = {}) {
    this.name = name;
    this.hostName = hostName;
    this.id = id;
    this.ipv4Net = ipv4Net;
    this.macAddress = macAddress;
    this.model = model;
    this.ports = ports;
    this.role = role;
    this.serialNo = serialNo;
    this.vendor = vendor;
    this.isRoot = isRoot;
    this.isSubRoot = isSubRoot;
    this.children = children;
    this.hiddenChildren = hiddenChildren;
    this.isCollapsed = isCollapsed;
    this.isExploredNode = isExploredNode;
  }
}

export class DevicePair {
  readonly parentName: string;
  readonly childName: string;

  constructor({ parentName, childName }: Partial<DevicePair> = {}) {
    this.parentName = parentName;
    this.childName = childName;
  }
}

export class PropertyNamePair {
  readonly property: PropertyTypeEnum;
  readonly name: string;

  constructor(property: PropertyTypeEnum, name: string) {
    this.property = property;
    this.name = name;
  }
}

export enum PropertyTypeEnum {
  Model = 'model',
  Vendor = 'vendor',
  HostName = 'hostName',
  Ipv4Net = 'ipv4Net',
}

export enum NodeTypeEnum {
  Access = 'access',
  WirelessAccessPoint = 'wirelessap',
  WaveSurf = 'wavesurf',
  Distribution = 'distribution',
}

export class LeafDevices {
  readonly leafName: string;
  readonly devices: Device[];

  constructor(leafName: string, devices: Device[]) {
    this.leafName = leafName;
    this.devices = devices;
  }
}

export class LeafDevicesNodes {
  readonly leafName: string;
  readonly devicesNodes: DeviceNode[];

  constructor(leafName: string, devicesNodes: DeviceNode[]) {
    this.leafName = leafName;
    this.devicesNodes = devicesNodes;
  }
}

export class DeviceProperties {
  readonly vendors: string[];
  readonly hostNames: string[];
  readonly mgmtIps: string[];
  readonly models: string[];

  constructor({ vendors, hostNames, mgmtIps, models }: Partial<DeviceProperties> = {}) {
    this.vendors = vendors;
    this.hostNames = hostNames;
    this.mgmtIps = mgmtIps;
    this.models = models;
  }
}
