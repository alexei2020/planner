export enum TopologyMenuItemType {
  Explore = 'Explore',
}

export class TopologyMenuItem {
  field: string;
  text: string;

  constructor({ field, text }: Partial<TopologyMenuItem> = {}) {
    this.field = field;
    this.text = text;
  }
}
