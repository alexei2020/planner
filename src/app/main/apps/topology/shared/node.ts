import { DeviceNode } from './device';

export interface INode {
  data: DeviceNode;
  depth: number;
  height: number;
  x: number;
  y: number;
  dx: number;
  dy: number;
  parent: INode;
  children: INode[];
  hiddenChildren: INode[];
}

export class NodeOffset {
  offsetX: number;
  offsetY: number;

  constructor({ offsetX, offsetY }: Partial<NodeOffset> = {}) {
    this.offsetX = offsetX;
    this.offsetY = offsetY;
  }
}
