import { Injectable } from '@angular/core';
import {
  IDevices,
  Devices,
  Device,
  IDevice,
  IDeviceMetadata,
  DeviceMetadata,
  IDeviceStatus,
  DeviceStatus,
} from './shared/device';
import _ from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class DevicesMapService {
  public mapToDevices(dto: IDevices): Devices {
    if (_.isEmpty(dto)) {
      return;
    }
    const items = _.map(dto.items, (item) => this.mapToDevice(item));

    const devicesInfo = new Devices({
      apiVersion: dto.apiVersion,
      items: items,
    });

    return devicesInfo;
  }

  public mapToDevice(dto: IDevice): Device {
    if (_.isEmpty(dto)) {
      return;
    }

    const device = new Device({
      apiVersion: dto.apiVersion,
      kind: dto.kind,
      metadata: this.mapToDeviceMetadata(dto.metadata),
      status: this.mapToDeviceStatus(dto.status),
    });

    return device;
  }

  public mapToDeviceMetadata(dto: IDeviceMetadata): DeviceMetadata {
    if (_.isEmpty(dto)) {
      return;
    }

    const metadata = new DeviceMetadata({
      creationTimestamp: dto.creationTimestamp,
      name: dto.name,
      resourceVersion: dto.resourceVersion,
      selfLink: dto.selfLink,
      uid: dto.uid,
    });

    return metadata;
  }

  public mapToDeviceStatus(dto: IDeviceStatus): DeviceStatus {
    if (_.isEmpty(dto)) {
      return;
    }

    const status = new DeviceStatus({
      hostName: dto.hostName,
      ipv4Net: dto.ipv4Net,
      model: dto.model,
      pltfmCatalog: dto.pltfmCatalog,
      rearIntfs: dto.rearIntfs,
      frontIntfs: dto.frontIntfs,
      role: dto.role,
      type: dto.type,
      vendor: dto.vendor,
    });

    return status;
  }
}
