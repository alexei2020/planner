import { Component, OnDestroy } from '@angular/core';
import { BreadcrumbService } from '../../../../shared/breadcrumb-bar.service';
import { PageType } from '../../../../shared/breadcrumb-bar.service.gen';

@Component({
  selector: 'kd-topology',
  templateUrl: './topology.component.html',
  styleUrls: ['./topology.component.scss'],
})
export class TopologyComponent implements OnDestroy {
  public data: any;

  constructor(private _breadcrumbService: BreadcrumbService) {
    _breadcrumbService.currentPage.next(PageType.TOPOLOGY);
  }

  public generate(event: any): void {
    this.data = event.data;
  }

  ngOnDestroy(): void {
    this._breadcrumbService.currentPage.next(PageType.TOPOLOGY);
  }
}
