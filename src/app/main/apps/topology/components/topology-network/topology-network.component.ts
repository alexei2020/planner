import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import * as d3 from 'd3';
import _ from 'lodash';
import { FuseLocationService } from 'app/services/location.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DevicesApiService } from '../../devices-api.service';
import { DeviceNode, LeafDevices } from '../../shared/device';
import { BreadcrumbService, TopologyViewType } from 'app/main/shared/breadcrumb-bar.service';
import { TopologyDataManagementService } from '../../topology-data-management.service';
import { TopologyTreeDrawService } from '../../topology-tree-draw.service';
import { INode } from '../../shared/node';
import { FuseUtils } from '@fuse/utils';
import { ContextMenuComponent, ContextMenuSelectEvent } from '@progress/kendo-angular-menu';
import { TopologyMenuItemType, TopologyMenuItem } from '../../shared/topology-menu';
import { topologyConfig } from '../../topology.config';
import { LocationDataItem } from 'app/components/shared/location-item';
import { FilterStateItem } from '@fuse/types/filter-state-item';
import { FuseSearchBarService } from '@fuse/services/search-bar.service';

@Component({
  selector: 'app-topology-network',
  templateUrl: './topology-network.component.html',
  styleUrls: ['./topology-network.component.scss'],
})
export class TopologyNetworkComponent implements OnInit, OnDestroy {
  leavesNames: string[];

  @ViewChild('deviceTree')
  deviceTreeElement: ElementRef;

  @ViewChild(ContextMenuComponent, { static: true })
  readonly topologyContextMenu: ContextMenuComponent;

  contextMenuItems: TopologyMenuItem[];
  isLoading: boolean;

  private treeData: DeviceNode;
  private leafDevices: LeafDevices[];
  private unsubscribeAll: Subject<any>;
  private exploredTreeData: DeviceNode;
  private exploredNodeName: string;
  private currentLocationSubTree: LocationDataItem;

  private readonly topMenuHeight = 70;
  private readonly leftMenuWidth = 50;

  // todo
  private aggregatorStats: FilterStateItem[];

  constructor(
    private locationService: FuseLocationService,
    private devicesApiService: DevicesApiService,
    private breadcrumbService: BreadcrumbService,
    private topologyDataManagementService: TopologyDataManagementService,
    private topologyTreeDrawService: TopologyTreeDrawService,
    private fuseSearchBarService: FuseSearchBarService
  ) {
    this.unsubscribeAll = new Subject();
    this.contextMenuItems = topologyConfig.contextMenuItems;
    this.isLoading = false;
    this.aggregatorStats = topologyConfig.filterStateItems;
    this.fuseSearchBarService.aggregatorStats.next(this.aggregatorStats);
  }

  get svgWidth(): number {
    return window.innerWidth - this.leftMenuWidth;
  }

  get svgHeight(): number {
    return window.innerHeight - this.topMenuHeight;
  }

  get isExploredNode(): boolean {
    return this.topologyTreeDrawService.isExploredNode;
  }

  async ngOnInit(): Promise<void> {
    this.locationService.locationSubTreeData.pipe(takeUntil(this.unsubscribeAll)).subscribe((dataItem) => {
      if (dataItem && dataItem.APIGroupName !== undefined) {
        this.currentLocationSubTree = dataItem;
        this.leavesNames = this.topologyDataManagementService.getDeviceNodeLeaves(dataItem);
        this.initialize();
      }
    });

    this.breadcrumbService.shouldUpdateTopology.pipe(takeUntil(this.unsubscribeAll)).subscribe((data) => {
      if (data === true) {
        this.updateTopology();
      }
    });

    this.topologyTreeDrawService.shouldRedrawCollapsableTopology
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((node: INode) => {
        const collapsedItems = node.data
          ? this.topologyDataManagementService.getCollapsedItems(node.data.hiddenChildren)
          : [];
        this.updateTopology(collapsedItems, node.data.isCollapsed);
      });

    this.topologyTreeDrawService.shouldRedrawTopology.pipe(takeUntil(this.unsubscribeAll)).subscribe(() => {
      this.drawTree(this.treeData);
    });

    this.breadcrumbService.shouldFitToScreen
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((isFitToScreen: boolean) => {
        if (this.topologyTreeDrawService.isExploredNode) {
          this.drawTree(this.exploredTreeData);
        } else {
          this.drawTree(this.treeData);
        }
      });
    this.fuseSearchBarService.filterChange.pipe(takeUntil(this.unsubscribeAll)).subscribe((hasChanged: boolean) => {
      if (hasChanged) {
        this.updateTopology();
      }
    });
  }

  ngOnDestroy(): void {
    this.breadcrumbService.canShowTopologyMenu.next(false);
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
    this.fuseSearchBarService.aggregatorStats.next([]);
  }

  async initialize(): Promise<void> {
    this.isLoading = true;
    this.breadcrumbService.canShowTopologyMenu.next(true);

    const leafDevicesPromises = _.map(this.leavesNames, (leafName) =>
      this.devicesApiService.getLeafDevicesByCurrentLocation(leafName)
    );
    this.leafDevices = await Promise.all(leafDevicesPromises);

    this.treeData = this.topologyDataManagementService.buildTopologyTreeData(
      this.leafDevices,
      this.currentLocationSubTree
    );

    const devicesArray = _.map(this.leafDevices, (leaf) => leaf.devices);
    const devices = _.flatMap(devicesArray);

    this.aggregatorStats = this.topologyDataManagementService.getFilterStateItems(devices, this.aggregatorStats);

    this.fuseSearchBarService.aggregatorStats.next(this.aggregatorStats);
    this.breadcrumbService.shouldUpdateTopology.next(true);
    this.drawTree(this.treeData);
    this.isLoading = false;
  }

  backToWholeTopology(): void {
    this.topologyTreeDrawService.isExploredNode = false;
    this.drawTree(this.treeData);
  }

  onSelectContextMenuItem(e: ContextMenuSelectEvent): void {
    switch (e.item.field) {
      case TopologyMenuItemType.Explore: {
        this.exploreDeviceNode();
      }
    }
  }

  private exploreDeviceNode(collapsedItems: string[] = [], isRootCollapsed: boolean = false): void {
    if (!this.topologyTreeDrawService.currentNode) {
      return;
    }
    this.topologyTreeDrawService.isExploredNode = true;
    this.exploredNodeName = this.topologyTreeDrawService.currentNode.name;

    this.exploredTreeData = this.topologyDataManagementService.buildTopologyTreeData(
      this.leafDevices,
      this.currentLocationSubTree,
      collapsedItems,
      isRootCollapsed
    );

    this.exploredTreeData = this.topologyDataManagementService.getFilteredExploredDeviceNode(
      this.exploredNodeName,
      this.exploredTreeData
    );

    this.drawTree(this.exploredTreeData);
  }

  private updateTopology(collapsedItems: string[] = [], isRootCollapsed: boolean = false): void {
    this.isLoading = true;

    this.treeData = this.topologyDataManagementService.buildTopologyTreeData(
      this.leafDevices,
      this.currentLocationSubTree,
      collapsedItems,
      isRootCollapsed
    );

    const filterStateItems = _.cloneDeep(this.aggregatorStats);

    this.treeData = this.topologyDataManagementService.getFilteredDeviceNode(filterStateItems, this.treeData);

    if (!this.topologyTreeDrawService.isExploredNode) {
      this.drawTree(this.treeData);
      this.isLoading = false;
      return;
    }

    this.exploredTreeData = this.topologyDataManagementService.getFilteredExploredDeviceNode(
      this.exploredNodeName,
      this.treeData
    );
    this.drawTree(this.exploredTreeData);
    this.isLoading = false;
  }

  private drawTree(treeData: DeviceNode): void {
    const viewType = this.getCurrentViewType();

    if (!this.deviceTreeElement) {
      return;
    }
    const element = this.deviceTreeElement.nativeElement;
    d3.select(element).select('svg').remove();
    let root = this.tree(viewType, treeData);
    root = this.topologyTreeDrawService.updateNodeOffset(viewType, root, this.svgWidth, this.svgHeight);

    const sideSizeNodeOffset = this.topologyTreeDrawService.getSideSizeNodeOffset(root);
    const sideSize = sideSizeNodeOffset.offsetY - sideSizeNodeOffset.offsetX + root.dx * 2;
    const viewBoxParams = this.getViewBoxParams(viewType, sideSize);

    let svg = d3.select(element).append('svg');
    if (this.breadcrumbService.isFitToScreen || this.topologyTreeDrawService.isExploredNode) {
      svg = svg.attr('width', this.svgWidth).attr('height', this.svgHeight);
      svg = svg.attr('viewBox', viewBoxParams);
      svg = svg.call(d3.zoom().scaleExtent([0.5, 5]).on('zoom', this.zoom));
    } else {
      svg = svg.attr('width', this.svgWidth);
      svg = svg.attr('style', `min-height: ${this.svgHeight}`);
      svg = svg.attr('viewBox', viewBoxParams).call(d3.zoom().scaleExtent([0.5, 5]).on('zoom', this.zoomToScroll));
    }

    this.topologyTreeDrawService.drawTree(svg, root, viewType, this.topologyContextMenu);
  }

  private zoom(): void {
    d3.select('g')
      .transition()
      .attr(
        'transform',
        'translate(' + d3.event.transform.x + ',' + d3.event.transform.y + ')' + ' scale(' + d3.event.transform.k + ')'
      );
  }

  private getViewBoxParams(viewType: TopologyViewType, sideSize: number): string {
    if (viewType === TopologyViewType.Left2Right) {
      return `${[0, 0, this.svgWidth, sideSize]}`;
    }

    if (sideSize < this.svgWidth) {
      sideSize = this.svgWidth;
    }
    return `${[0, 0, this.svgWidth, this.svgHeight]}`;
  }

  private tree(viewType: TopologyViewType, data: DeviceNode): any {
    const root: any = d3.hierarchy(data);
    root.dx = 45;
    if (viewType === TopologyViewType.Left2Right) {
      root.dy = this.svgWidth / (root.height + 1);
    } else {
      root.dy = this.svgHeight / (root.height + 1);
    }

    return d3.tree().nodeSize([root.dx, root.dy])(root);
  }

  private getCurrentViewType(): TopologyViewType {
    const selectedViewType = this.breadcrumbService.viewDropdown.selectedItem;
    if (selectedViewType) {
      return selectedViewType.item_id as TopologyViewType;
    }
    return TopologyViewType.Left2Right;
  }

  private zoomToScroll(event: WheelEvent): void {
    const wheelEvent = event || (window.event as WheelEvent);
    if (!wheelEvent) {
      return;
    }
    const scrollableParent = FuseUtils.getScrollableParent(wheelEvent.target as HTMLElement);
    if (!scrollableParent) {
      return;
    }
    wheelEvent.preventDefault();

    const heightDiff = scrollableParent.scrollHeight - scrollableParent.offsetHeight;
    const scrollDiff = Math.max(heightDiff * 0.025, 10);

    const isScrollDown = wheelEvent.deltaY < 0;
    scrollableParent.scrollTop = isScrollDown
      ? Math.max(scrollableParent.scrollTop - scrollDiff, 0)
      : Math.min(scrollableParent.scrollTop + scrollDiff, heightDiff);

    setTimeout(() => {
      scrollableParent.scrollTop = isScrollDown
        ? Math.max(scrollableParent.scrollTop - scrollDiff, 0)
        : Math.min(scrollableParent.scrollTop + scrollDiff, heightDiff);
    }, 0);
  }
}
