import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopologyNetworkComponent } from './topology-network.component';

describe('TopologyNetworkComponent', () => {
  let component: TopologyNetworkComponent;
  let fixture: ComponentFixture<TopologyNetworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopologyNetworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopologyNetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
