import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopologyRoutingModule } from './topology.routing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TopologyComponent } from './components/topology/topology.component';
import { SharedModule } from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { GridsterModule } from 'angular-gridster2';
import { GridModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { ComponentsModule } from '../../../components/module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';
import { AngularResizedEventModule } from 'angular-resize-event';
import { AgmCoreModule } from '@agm/core';
import 'hammerjs';
import { TopologyNetworkComponent } from './components/topology-network/topology-network.component';
import { ContextMenuModule } from '@progress/kendo-angular-menu';
@NgModule({
  declarations: [TopologyComponent, TopologyNetworkComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    GridsterModule,
    TopologyRoutingModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    GridModule,
    InputsModule,
    FontAwesomeModule,
    MatChipsModule,
    MatButtonModule,
    ContextMenuModule,

    ComponentsModule,

    AngularResizedEventModule,

    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBhize4VO6Ul3vg63vlCeb94EYeK_Khtbc',
      libraries: ['places'],
    }),

    AngularSvgIconModule.forRoot(),
  ],
})
export class TopologyModule {}
