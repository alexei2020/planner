import { TestBed } from '@angular/core/testing';

import { TopologyTreeDrawService } from './topology-tree-draw.service';

describe('TopologyTreeDrawService', () => {
  let service: TopologyTreeDrawService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TopologyTreeDrawService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
