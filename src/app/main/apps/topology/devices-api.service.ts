import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { IDevices, Device, LeafDevices } from './shared/device';
import { DevicesMapService } from './devices-map.service';

@Injectable({
  providedIn: 'root',
})
export class DevicesApiService {
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient, private devicesMapService: DevicesMapService) {}

  async getDevicesByCurrentLocation(currentLocation: string): Promise<Device[]> {
    const url = `${this.apiUrl}/apis/${currentLocation}/v1/deviceuis`;

    let devicesResponse: IDevices;
    try {
      devicesResponse = await this.http.get<IDevices>(url).toPromise();
    } catch (error) {
      console.log(error);
    }

    const devices = this.devicesMapService.mapToDevices(devicesResponse);

    return devices ? devices.items : [];
  }

  async getLeafDevicesByCurrentLocation(currentLocation: string): Promise<LeafDevices> {
    const url = `${this.apiUrl}/apis/${currentLocation}/v1/deviceuis`;

    let devicesResponse: IDevices;
    try {
      devicesResponse = await this.http.get<IDevices>(url).toPromise();
    } catch (error) {
      console.log(error);
    }

    const devices = this.devicesMapService.mapToDevices(devicesResponse);

    const deviceItems = devices ? devices.items : [];
    const leafDevices = new LeafDevices(currentLocation, deviceItems)
 
    return leafDevices;
  }
}
