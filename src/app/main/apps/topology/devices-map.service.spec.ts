import { TestBed } from '@angular/core/testing';

import { DevicesMapService } from './devices-map.service';

describe('DevicesMapService', () => {
  let service: DevicesMapService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DevicesMapService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
