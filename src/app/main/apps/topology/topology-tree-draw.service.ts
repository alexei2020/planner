import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { ActivatedRoute, Router } from '@angular/router';
import { IMultiParent, TopologyDataManagementService } from './topology-data-management.service';
import _ from 'lodash';
import { NodeTypeEnum, DeviceNode } from './shared/device';
import { INode, NodeOffset } from './shared/node';
import { TopologyViewType } from 'app/main/shared/breadcrumb-bar.service';
import { Observable, Subject } from 'rxjs';
import { ContextMenuComponent } from '@progress/kendo-angular-menu';

@Injectable({
  providedIn: 'root',
})
export class TopologyTreeDrawService {
  isExploredNode: boolean;
  currentNode: DeviceNode;

  private _shouldRedrawCollapsableTopology$: Subject<INode>;
  private _shouldRedrawTopology$: Subject<boolean>;
  private tooltip: any;
  private doubleClicked = false;

  private readonly rootNodePath = './assets/images/topology/root-node.svg';
  private readonly rootEmptyNodePath = './assets/images/topology/root-empty-node.svg';
  private readonly rootNodeName = 'root-node';
  private readonly rootEmptyNodeName = 'root-empty-node';
  private readonly rectangleNodeName = 'rectangle-node';
  private readonly circleNodeName = 'circle-node';
  private readonly nodeWidth = 24;
  private readonly nodeHeight = 12;
  private readonly nodeRoundRadius = 6;

  get shouldRedrawTopology(): Observable<boolean> {
    return this._shouldRedrawTopology$.asObservable();
  }

  get shouldRedrawCollapsableTopology(): Observable<INode> {
    return this._shouldRedrawCollapsableTopology$.asObservable();
  }

  constructor(
    private router: Router,
    private topologyDataManagementService: TopologyDataManagementService,
    private route: ActivatedRoute
  ) {
    this._shouldRedrawCollapsableTopology$ = new Subject<INode>();
    this._shouldRedrawTopology$ = new Subject<boolean>();
  }

  public drawTree(svg: any, root: any, viewType: TopologyViewType, topologyContextMenu: ContextMenuComponent): void {
    const duplicateNodes = this.isExploredNode
      ? this.topologyDataManagementService.exploreModeDuplicateNodes
      : this.topologyDataManagementService.duplicateNodes;
    const multiParents = this.topologyDataManagementService.getMultiParents(root, duplicateNodes);

    const g = svg.append('g').attr('class', 'topology-tree');
    this.tooltip = d3.select('body').append('div').attr('class', 'topology-node-tooltip');
    this.drawLinks(g, root, viewType, multiParents);
    this.drawNodes(g, root, viewType, topologyContextMenu, multiParents);
  }

  updateNodeOffset(viewType: TopologyViewType, root: any, svgWidth: number, svgHeight: number): any {
    let nodeOffset: NodeOffset;
    if (viewType === TopologyViewType.Top2Bottom) {
      nodeOffset = this.getVerticalTypeNodeOffset(root, svgWidth, svgHeight);
    } else {
      nodeOffset = this.getHorizontalTypeNodeOffset(root, svgWidth);
    }
    root.each((d) => {
      d.x = d.x + nodeOffset.offsetX;
      d.y = d.y + nodeOffset.offsetY;
    });
    return root;
  }

  getSideSizeNodeOffset(root: any): NodeOffset {
    let x0 = Infinity;
    let x1 = -x0;

    root.each((d) => {
      if (d.x > x1) {
        x1 = d.x;
      }
      if (d.x < x0) {
        x0 = d.x;
      }
    });

    const nodeOffset = new NodeOffset({
      offsetX: x0,
      offsetY: x1,
    });

    return nodeOffset;
  }

  private getNodeClassName(d: INode): string {
    if (d.data.isRoot || d.data.isSubRoot) {
      return _.isEmpty(d.data.hiddenChildren) ? this.rootEmptyNodeName : this.rootNodeName;
    }
    if (d.data.role !== NodeTypeEnum.WirelessAccessPoint) {
      return this.rectangleNodeName;
    }
    return this.circleNodeName;
  }

  private drawLinks(rootSvg: any, root: any, viewType: TopologyViewType, multiParents: IMultiParent[]): void {
    const link = rootSvg
      .append('g')
      .attr('class', 'link topology-link')
      .selectAll('path')
      .data(root.links())
      .join('path')
      .attr('d', this.getLink(viewType));

    _.forEach(multiParents, (item: IMultiParent) => {
      if (!item.isCollapsed) {
        rootSvg
          .append('g')
          .attr('class', 'topology-link')
          .selectAll('path')
          .data(() => {
            return [{ source: item.parent, target: item.child }];
          })
          .join('path')
          .attr('d', this.getLink(viewType));
      }
    });
  }

  private drawNodes(
    rootSvg: any,
    root: any,
    viewType: TopologyViewType,
    topologyContextMenu: ContextMenuComponent,
    multiParents: IMultiParent[]
  ): void {
    const node = rootSvg
      .append('g')
      .attr('class', 'topology-node')
      .selectAll('g')
      .data(root.descendants())
      .join('g')
      .attr('transform', (d: INode) => this.getNodeTransformAttribute(viewType, d))
      .on('dblclick', (d: INode) => this.onDblClickNode(d))
      .on('contextmenu', (d: INode) => this.onContextMenuOpen(d, topologyContextMenu))
      .on('mouseover', (d: INode) => this.onMouseOverNode(d, this.tooltip))
      .on('mousemove', (d: INode) => this.onMouseMoveNode(d, this.tooltip))
      .on('mouseout', () => this.hideHtmlElement(this.tooltip))
      .on('click', (d: INode) => this.onClickNode(d, root));
    d3.select(window).on('resize', () => {
      this.resize();
    });

    d3.select('body').on('click', () => {
      topologyContextMenu.hide();
    });

    node.append('g').attr('class', (d) => this.getNodeClassName(d));

    node
      .select(`.${this.rootNodeName}`)
      .append('image')
      .attr('xlink:href', this.rootNodePath)
      .attr('x', -this.nodeWidth / 2)
      .attr('y', -this.nodeWidth / 2)
      .attr('width', this.nodeWidth)
      .attr('height', this.nodeWidth);

    node
      .select(`.${this.rootEmptyNodeName}`)
      .append('image')
      .attr('xlink:href', this.rootEmptyNodePath)
      .attr('x', -this.nodeWidth / 2)
      .attr('y', -this.nodeWidth / 2)
      .attr('width', this.nodeWidth)
      .attr('height', this.nodeWidth);

    node
      .select(`.${this.rectangleNodeName}`)
      .append('rect')
      .attr('style', (d: INode) => this.getNodeStyles(d, multiParents))
      .attr('x', () => -this.nodeWidth / 2)
      .attr('y', () => -this.nodeHeight / 2)
      .attr('width', () => this.nodeWidth)
      .attr('height', () => this.nodeHeight);

    node
      .select(`.${this.circleNodeName}`)
      .append('circle')
      .attr('style', (d: INode) => this.getNodeStyles(d, multiParents))
      .attr('r', this.nodeRoundRadius)
      .attr('cx', () => (viewType === TopologyViewType.Left2Right ? -this.nodeRoundRadius : 0))
      .attr('cy', () => (viewType === TopologyViewType.Left2Right ? 0 : this.nodeRoundRadius));

    const text = node
      .append('text')
      .attr('class', (d: INode) => this.getNodeLabelClass(viewType, d))
      .attr('dy', '0.31em')
      .attr('x', (d: INode) => this.getNodeLabelXValue(d))
      .attr('text-anchor', (d: INode) => this.getTextAnchor(d));

    text.append('tspan').attr('class', 'topology-node-label-first-line');
    text.append('tspan').attr('class', 'topology-node-label-second-line');
    text.append('tspan').attr('class', 'topology-node-label-collapsed-children-count');

    node
      .select('.topology-node-label-first-line')
      .text((d: INode) => this.getNodeLabelFirstLine(d))
      .attr('x', (d: INode) => this.getNodeLabelFirstLineXValue(viewType, d))
      .attr('y', (d: INode) => this.getNodeLabelFirstLineYValue(viewType, d))
      .attr('text-anchor', (d: INode) => this.getTextAnchor(d));

    node
      .select('.topology-node-label-second-line')
      .text((d: INode) => this.getNodeLabelSecondLine(d))
      .attr('x', (d: INode) => this.getNodeLabelSecondLineXValue(viewType, d))
      .attr('y', (d: INode) => this.getNodeLabelSecondLineYValue(viewType, d))
      .attr('text-anchor', (d: INode) => this.getTextAnchor(d));

    node
      .select('.topology-node-label-collapsed-children-count')
      .text((d: INode) => this.getNodeLabelCollapsedChildrenCount(d))
      .attr('x', (d: INode) => this.getNodeLabelCollapsedChildrenCountXValue(viewType, d))
      .attr('y', (d: INode) => this.getNodeLabelCollapsedChildrenCountYValue(viewType, d))
      .attr('text-anchor', (d: INode) => this.getTextAnchor(d));
  }

  private getVerticalTypeNodeOffset(root: any, svgWidth: number, svgHeight: number): NodeOffset {
    const minX = this.getMinXValue(root);
    const maxX = this.getMaxXValue(root);

    const offset = 100;

    const rootWidth = Math.abs(minX) + Math.abs(maxX) + offset;
    const offsetX: number = Math.abs((svgWidth - rootWidth) / 2);

    const minY = this.getMinYValue(root);
    const maxY = this.getMaxYValue(root);

    const rootHeight = Math.abs(minY) + Math.abs(maxY) + offset;
    let offsetY: number = Math.abs((svgHeight - rootHeight) / 2);

    if (rootHeight > svgHeight || offsetY < 100) {
      offsetY = 100;
    }
    const nodeOffset = new NodeOffset({
      offsetX: Math.abs(minX) + offsetX,
      offsetY: offsetY,
    });

    return nodeOffset;
  }

  private getHorizontalTypeNodeOffset(root: any, svgWidth: number): NodeOffset {
    const minX = this.getMinXValue(root);

    const offset = 100;
    const offsetX: number = Math.abs(minX) + 20;

    const minY = this.getMinYValue(root);
    const maxY = this.getMaxYValue(root);

    const rootWidth = Math.abs(minY) + Math.abs(maxY) + offset;
    const offsetY: number = Math.abs((svgWidth - rootWidth) / 2);

    const nodeOffset = new NodeOffset({
      offsetX: offsetX,
      offsetY: offsetY,
    });

    return nodeOffset;
  }

  private getMinXValue(root: any): number {
    let x: number = root.x;
    root.each((d) => {
      if (d.x < x) {
        x = d.x;
      }
    });
    return x;
  }

  private getMaxXValue(root: any): number {
    let x: number = root.x;
    root.each((d) => {
      if (d.x > x) {
        x = d.x;
      }
    });
    return x;
  }

  private getMinYValue(root: any): number {
    let y: number = root.y;
    root.each((d) => {
      if (d.y < y) {
        y = d.y;
      }
    });
    return y;
  }

  private getMaxYValue(root: any): number {
    let y: number = root.y;
    root.each((d) => {
      if (d.y > y) {
        y = d.y;
      }
    });
    return y;
  }

  private resize(): void {
    this._shouldRedrawTopology$.next();
  }

  private onClickNode(node: INode, root: any): void {
    setTimeout(() => {
      if (this.doubleClicked) {
        return;
      }
      this.hideHtmlElement(this.tooltip);
      if (this.isExploredNode && !this.hasExploredNodeParent(node)) {
        return;
      }
      const isCollapsed = !node.data.isCollapsed;
      node.data.isCollapsed = isCollapsed;
      if (root.data && root.data.hiddenChildren) {
        root.data.hiddenChildren = this.changeCollapseState(root.data.hiddenChildren, node.data.name, isCollapsed);
      }

      this._shouldRedrawCollapsableTopology$.next(root);
    }, 200);
  }

  private changeCollapseState(rootChildren: DeviceNode[], nodeName: string, isCollapsed: boolean): DeviceNode[] {
    _.forEach(rootChildren, (child) => {
      if (child.name === nodeName) {
        child.isCollapsed = isCollapsed;
        return false;
      }
      if (!_.isEmpty(child.hiddenChildren)) {
        this.changeCollapseState(child.hiddenChildren, nodeName, isCollapsed);
      }
    });
    return rootChildren;
  }

  private hasExploredNodeParent(node: INode): boolean {
    if (node.data.isExploredNode) {
      return true;
    }
    if (node.parent) {
      return this.hasExploredNodeParent(node.parent);
    }
    return false;
  }

  private getNodeLabelClass(viewType: TopologyViewType, node: INode): string {
    if (viewType === TopologyViewType.Top2Bottom && !node.data.isRoot) {
      return 'topology-node-label-top-down';
    }
  }

  private onDblClickNode(node: INode): void {
    this.doubleClicked = true;
    this.hideHtmlElement(this.tooltip);
    d3.selectAll('.topology-node-tooltip').remove();
    d3.selectAll('.topology-node-context-menu').remove();
    if (node.data.isRoot || node.data.isSubRoot) {
      this.router.navigate(['/topology'], {
        queryParams: { loc: node.data.name },
        queryParamsHandling: 'merge',
      });
    } else {
      const location = this.getLocation(node);
      const tenant = this.route.snapshot.queryParams.tenant;
      this.router.navigate(['/devices', node.data.name], {
        queryParams: { loc: location, tenant: tenant },
      });
    }

    setTimeout(() => {
      this.doubleClicked = false;
    }, 1000);
  }

  private getLocation(node: INode): string {
    if (node && (node.data.isRoot || node.data.isSubRoot)) {
      return node.data.name;
    }
    return this.getLocation(node.parent);
  }

  private onContextMenuOpen(node: INode, topologyContextMenu: ContextMenuComponent): void {
    if (node.data.isRoot || node.data.isSubRoot) {
      return;
    }
    this.currentNode = node.data;

    const top = d3.event.pageY - 10;
    const left = d3.event.pageX + 10;

    topologyContextMenu.show({ top: top, left: left });
    d3.event.preventDefault();
  }

  private onMouseOverNode(node: INode, tooltip: any): void {
    if (node.data.isRoot || node.data.isSubRoot) {
      tooltip.html(`${node.data.name}`);
    } else {
      tooltip.html(this.getTooltipView(node.data));
    }
  }

  private onMouseMoveNode(node: INode, tooltip: any): void {
    const top = node.data.isRoot || node.data.isSubRoot ? d3.event.pageY - 40 : d3.event.pageY - 120;
    const left = node.data.isRoot || node.data.isSubRoot ? d3.event.pageX - 70 : d3.event.pageX - 160;
    this.showHtmlElement(tooltip);
    return tooltip.style('top', `${top}px`).style('left', `${left}px`);
  }

  private showHtmlElement(element: any): void {
    element.style('visibility', 'visible');
  }

  private hideHtmlElement(element: any): void {
    element.style('visibility', 'hidden');
  }

  private getLink(viewType: TopologyViewType): any {
    if (viewType === TopologyViewType.Left2Right) {
      return d3
        .linkHorizontal()
        .x((d: any) => d.y)
        .y((d: any) => d.x);
    }
    return d3
      .linkVertical()
      .x((d: any) => d.x)
      .y((d: any) => d.y);
  }

  private getNodeTransformAttribute(viewType: TopologyViewType, node: INode): string {
    if (viewType === TopologyViewType.Left2Right) {
      return `translate(${node.y},${node.x})`;
    }
    return `translate(${node.x},${node.y})`;
  }

  private getNodeStyles(node: INode, multiParents: IMultiParent[]): string {
    const hasChildren =
      node.data && node.data.hiddenChildren && (node.data.hiddenChildren.length > 0 || node.data.children.length > 0);
    const isMultiParent = _.some(multiParents, (multi: IMultiParent) => multi.parent === node);

    const mainColor = '#3CBA0D';
    const backgrountColor = '#EBF0FA';
    const borderColor = node.data.isExploredNode ? 'blue' : mainColor;
    if (hasChildren || isMultiParent) {
      return `fill:${mainColor};stroke:${borderColor};stroke-width:2;`;
    }
    return `fill:${backgrountColor};stroke:${borderColor};stroke-width:2;`;
  }

  private getNodeLabelXValue(node: INode): number {
    return node.data.role !== NodeTypeEnum.WirelessAccessPoint ? -6 : 6;
  }

  private getNodeLabelFirstLine(node: INode): string {
    if (node.data.isRoot || node.data.isSubRoot) {
      return node.data.name;
    }
    return node.data.ipv4Net;
  }

  private getNodeLabelFirstLineXValue(viewType: TopologyViewType, node: INode): number {
    if (viewType === TopologyViewType.Top2Bottom && node.data.isRoot) {
      return 8;
    }
    if (node.data.role === NodeTypeEnum.WirelessAccessPoint) {
      return viewType === TopologyViewType.Left2Right ? 4 : -18;
    }
    if (node.data.isRoot) {
      return -12;
    }
    if (viewType === TopologyViewType.Top2Bottom) {
      return -20;
    }
    return 0;
  }

  private getNodeLabelFirstLineYValue(viewType: TopologyViewType, node: INode): number {
    if (viewType === TopologyViewType.Top2Bottom && node.data.isRoot) {
      return -18;
    }
    if (node.data.role === NodeTypeEnum.WirelessAccessPoint && viewType === TopologyViewType.Top2Bottom) {
      return 18;
    }
    if (node.data.role !== NodeTypeEnum.WirelessAccessPoint && !node.data.isRoot) {
      return -26;
    }
  }

  private getNodeLabelSecondLine(node: INode): string {
    if (node.data.isRoot || node.data.isSubRoot) {
      return;
    }
    return node.data.hostName;
  }

  private getNodeLabelSecondLineXValue(viewType: TopologyViewType, node: INode): number {
    if (node.data.role === NodeTypeEnum.WirelessAccessPoint) {
      return viewType === TopologyViewType.Top2Bottom ? -14 : 4;
    }
    if (viewType === TopologyViewType.Top2Bottom) {
      return -20;
    }
    return 0;
  }

  private getNodeLabelSecondLineYValue(viewType: TopologyViewType, node: INode): number {
    if (node.data.role === NodeTypeEnum.WirelessAccessPoint && viewType === TopologyViewType.Top2Bottom) {
      return 31;
    }
    if (node.data.role !== NodeTypeEnum.WirelessAccessPoint && !node.data.isRoot) {
      return -15;
    }
    if (node.data.role === NodeTypeEnum.WirelessAccessPoint) {
      return 12;
    }
  }

  private getNodeLabelCollapsedChildrenCount(node: INode): string {
    if (!node.data.isCollapsed || _.isEmpty(node.data.hiddenChildren)) {
      return '';
    }
    return `(+${node.data.hiddenChildren.length})`;
  }

  private getNodeLabelCollapsedChildrenCountXValue(viewType: TopologyViewType, node: INode): number {
    if (!node.data.isCollapsed) {
      return 0;
    }
    if (viewType === TopologyViewType.Top2Bottom && !node.data.isRoot) {
      if (node.data.isSubRoot) {
        return 18;
      }
      return 22;
    }
    if (node.data.isRoot || !node.data.isSubRoot) {
      return 30;
    }
    return 25;
  }

  private getNodeLabelCollapsedChildrenCountYValue(viewType: TopologyViewType, node: INode): number {
    if (!node.data.isCollapsed) {
      return 0;
    }
    if (viewType === TopologyViewType.Top2Bottom && !node.data.isRoot) {
      return -8;
    }
    return 3;
  }

  private getTextAnchor(node: INode): string {
    if (node.data.isRoot) {
      return 'end';
    } else if (node.data.role === NodeTypeEnum.WirelessAccessPoint) {
      return 'start';
    }
    return 'middle';
  }

  private getTooltipView(deviceNode: DeviceNode): string {
    return `<table>
    <tr>
      <td style="text-align: right">Host Name:</div>
      <td>${deviceNode.hostName}</div>
    </tr>
    <tr>
      <td style="text-align: right">Vendor:</div>
      <td>${deviceNode.vendor}</div>
    </tr>
    <tr>
      <td style="text-align: right">IP:</div>
      <td>${deviceNode.ipv4Net}</div>
    </tr>
    <tr>
      <td style="text-align: right">Model:</div>
      <td>${deviceNode.model}</div>
    </tr>
    <tr>
      <td style="text-align: right">Role:</div>
      <td>${deviceNode.role}</div>
    </tr>
    </table>`;
  }
}
