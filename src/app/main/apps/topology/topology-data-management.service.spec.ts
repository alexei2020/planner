import { TestBed } from '@angular/core/testing';

import { TopologyDataManagementService } from './topology-data-management.service';

describe('TopologyDataManagementService', () => {
  let service: TopologyDataManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TopologyDataManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
