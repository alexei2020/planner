import { FilterStateItem } from '@fuse/types/filter-state-item';
import { PropertyTypeEnum } from './shared/device';
import { TopologyMenuItem, TopologyMenuItemType } from './shared/topology-menu';

export const topologyConfig = {
  contextMenuItems: [
    new TopologyMenuItem({
      field: TopologyMenuItemType.Explore,
      text: TopologyMenuItemType.Explore,
    }),
  ],
  filterStateItems: [
    new FilterStateItem({
      field: PropertyTypeEnum.HostName,
      name: 'Host Name',
      keys: [],
    }),
    new FilterStateItem({
      field: PropertyTypeEnum.Ipv4Net,
      name: 'OOB Mgmt IP',
      keys: [],
    }),
    new FilterStateItem({
      field: PropertyTypeEnum.Vendor,
      name: 'Vendor',
      keys: [],
    }),
    new FilterStateItem({
      field: PropertyTypeEnum.Model,
      name: 'Model',
      keys: [],
    }),
  ],
};
