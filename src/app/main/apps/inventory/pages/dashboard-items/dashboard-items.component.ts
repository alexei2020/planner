import { Component, EventEmitter, OnInit } from '@angular/core';
import { CompactType, GridsterConfig, GridsterItem, GridsterItemComponent, GridsterPush, GridType } from 'angular-gridster2';

@Component({
    templateUrl: './dashboard-items.component.html',
    styleUrls: ['./dashboard-items.component.scss'],
})
export class DashboardItemsComponent implements OnInit {
    options: GridsterConfig;
    dashboard: Array<GridsterItem>;
    itemToPush: GridsterItemComponent;

    resizeEvent: EventEmitter<any> = new EventEmitter<any>();

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.options = {
            gridType: GridType.ScrollVertical,
            compactType: CompactType.None,
            margin: 5,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1000,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            minItemArea: 1,
            fixedColWidth: 250,
            fixedRowHeight: 250,
            itemResizeCallback: (item) => {
                // update DB with new size
                // send the update to widgets
                this.resizeEvent.emit(item);
            },
            pushItems: true,
            draggable: {
                enabled: true,
            },
            resizable: {
                enabled: true,
            },
        };

        this.dashboard = [
            { cols: 96, rows: 60, y: 0, x: 0, type: 'widgetA' },
            { cols: 12, rows: 22, y: 8, x: 22, type: 'widgetB' },
            { cols: 12, rows: 22, y: 22, x: 22, type: 'widgetB' },
            { cols: 12, rows: 22, y: 22, x: 22, type: 'widgetB' },
            { cols: 12, rows: 22, y: 22, x: 22, type: 'widgetB' },
            { cols: 12, rows: 22, y: 22, x: 10, type: 'widgetB' },
            { cols: 12, rows: 22, y: 22, x: 10, type: 'widgetB' },
            { cols: 12, rows: 22, y: 22, x: 4, type: 'widgetB' },
            { cols: 12, rows: 22, y: 22, x: 6, type: 'widgetB' },
        ];
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    changedOptions(): void {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    removeItem($event, item): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.dashboard.splice(this.dashboard.indexOf(item), 1);
    }

    addItem(): void {
        this.dashboard.push({ x: 0, y: 0, cols: 1, rows: 1 });
    }

    initItem(item: GridsterItem, itemComponent: GridsterItemComponent): void {
        this.itemToPush = itemComponent;
    }

    pushItem(): void {
        const push = new GridsterPush(this.itemToPush); // init the service
        this.itemToPush.$item.rows += 4; // move/resize your item
        if (push.pushItems(push.fromNorth)) {
            // push items from a direction
            push.checkPushBack(); // check for items can restore to original position
            push.setPushedItems(); // save the items pushed
            this.itemToPush.setSize();
            this.itemToPush.checkItemChanges(
                this.itemToPush.$item,
                this.itemToPush.item
            );
        } else {
            this.itemToPush.$item.rows -= 4;
            push.restoreItems(); // restore to initial state the pushed items
        }
        push.destroy(); // destroy push instance
        // similar for GridsterPushResize and GridsterSwap
    }
}
