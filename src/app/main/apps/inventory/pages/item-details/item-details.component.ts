import { Component, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompactType, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: "./item-details.component.html",
    styleUrls: ["./item-details.component.scss"],
})
export class ItemDetailsComponent implements OnInit {
    options: GridsterConfig;
    resizeEvent: EventEmitter<any> = new EventEmitter<any>();
    item1: GridsterItem;
    item2: GridsterItem;
    item3: GridsterItem;

    public data: any;

    private _unsubscribeAll: Subject<any>;

    constructor(private activatedRoute: ActivatedRoute) {
        this.options = {
            gridType: GridType.Fit,
            compactType: CompactType.None,
            margin: 5,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1000,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            minItemArea: 1,
            // fixedColWidth: 250,
            // fixedRowHeight: 250,
            itemResizeCallback: (item) => {
                // update DB with new size
                // send the update to widgets
                this.resizeEvent.emit(item);
            },
            pushItems: true,
            draggable: {
                enabled: true,
            },
            resizable: {
                enabled: true,
            },
        };
    }

    ngOnInit(): void {
        this.activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
            const itemId = params["id"];
        });
        this.data = {
            label: "PROPOSALS",
            count: 42,
            extra: {
                label: "Implemented",
                count: 8,
            },
        };

        this.item1 = { cols: 48, rows: 110, y: 0, x: 0 };
        this.item2 = { cols: 48, rows: 110, y: 0, x: 48 };
        this.item3 = { cols: 96, rows: 30, y: 110, x: 0 };
    }

    ngOnDestroy(): void {
		  this._unsubscribeAll?.next();
		  this._unsubscribeAll?.complete();
	  }
}
