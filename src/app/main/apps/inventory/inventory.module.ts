import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule, Routes } from '@angular/router';
import { FuseWidgetModule } from '@fuse/components';
import { GridsterModule } from 'angular-gridster2';

import { DevicesModule } from './components/devices/devices.module';
import { WidgetsModule } from './components/widgets/widgets.module';
import { InventoryComponent } from './inventory.component';
import { DashboardItemsComponent } from './pages/dashboard-items/dashboard-items.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ItemDetailsComponent } from './pages/item-details/item-details.component';
import { FuseDirectivesModule } from '@fuse/directives/directives';

const routes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
            breadcrumb: 'Dashboard',
        },
    },
    {
        path: 'device/:id',
        component: ItemDetailsComponent,
        data: {
            breadcrumb: 'Item Details',
        },
    },
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
];

@NgModule({
    imports: [
        CommonModule,
        GridsterModule,
        FuseWidgetModule,
        WidgetsModule,
        MatIconModule,
        MatButtonModule,
        DevicesModule,
        FuseDirectivesModule,
        RouterModule.forChild(routes),
    ],
    declarations: [
        DashboardComponent,
        ItemDetailsComponent,
        InventoryComponent,
        DashboardItemsComponent,
    ],
})
export class InventoryModule {}
