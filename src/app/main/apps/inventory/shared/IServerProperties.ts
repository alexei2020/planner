export interface IServerProperties {
    productName: string;
    serialNumber: string;
    pid: string;
    uuid: string;
    biosVersion: string;
    description: string;
    assetTag: string;
}
