import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { FuseWidgetModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { SampleModule } from '../sample/sample.module';
import { ParentDynamicComponent } from './parent-dynamic/parent-dynamic.component';
import { WidgetAComponent } from './widget-a/widget-a.component';
import { WidgetBComponent } from './widget-b/widget-b.component';

@NgModule({
    declarations: [ParentDynamicComponent, WidgetAComponent, WidgetBComponent],
    imports: [
        CommonModule,
        FuseSharedModule,
        FuseWidgetModule,
        MatCardModule,
        MatIconModule,
        MatButtonModule,
        SampleModule,
    ],
    exports: [ParentDynamicComponent],
})
export class WidgetsModule {}
