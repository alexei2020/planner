import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'inventory-widget-b',
    templateUrl: './widget-b.component.html',
    styleUrls: ['./widget-b.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: fuseAnimations,
})
export class WidgetBComponent implements OnInit, OnDestroy {
    public data: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor() {
        this.data = {
            label: 'PROPOSALS',
            count: 42,
            extra: {
                label: 'Implemented',
                count: 8,
            },
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    @Input()
    widget;
    @Input()
    resizeEvent: EventEmitter<any>;

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.resizeEvent
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((widget) => {
                if (widget === this.widget) {
                    // or check id , type or whatever you have there
                    // resize your widget, chart, map , etc.
                    console.log(widget);
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
