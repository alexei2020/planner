import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-parent-dynamic',
    templateUrl: './parent-dynamic.component.html',
    styleUrls: ['./parent-dynamic.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParentDynamicComponent implements OnInit {
    @Input()
    widget;
    @Input()
    resizeEvent;

    constructor() {}

    ngOnInit(): void {}
}
