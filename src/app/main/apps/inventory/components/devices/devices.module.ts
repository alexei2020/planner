import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FuseWidgetModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { ServerPropertiesComponent } from './server-properties/server-properties.component';

@NgModule({
    declarations: [ServerPropertiesComponent],
    imports: [
        CommonModule,
        FuseSharedModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        FuseWidgetModule,
    ],
    exports: [ServerPropertiesComponent],
})
export class DevicesModule {}
