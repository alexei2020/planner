export const server_properties_validation = {
    productName: [{ type: 'required', message: 'Product Name is required' }],
    serialNumber: [{ type: 'required', message: 'Serial Number is required' }],
    pid: [{ type: 'required', message: 'PID is required' }],
    uuid: [{ type: 'required', message: 'UUID is required' }],
    biosVersion: [{ type: 'required', message: 'BIOS Version is required' }],
    description: [{ type: 'required', message: 'Description is required' }],
    assetTag: [{ type: 'required', message: 'Asset Tag is required' }],
};
