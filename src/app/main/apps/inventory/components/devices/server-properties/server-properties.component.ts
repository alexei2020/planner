import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { IServerProperties } from '../../../shared/IServerProperties';
import { server_properties_validation } from './server-properties.validation';

@Component({
    selector: 'inventory-server-properties',
    templateUrl: './server-properties.component.html',
    styleUrls: ['./server-properties.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServerPropertiesComponent implements OnInit {
    private _server: IServerProperties;
    public serverForm: FormGroup;
    public validationMessages = server_properties_validation;

    get form() {
        return this.serverForm.controls;
    }

    @Input()
    get data(): IServerProperties {
        return this._server;
    }
    set data(value: IServerProperties) {
        this._server = value;
        this.serverForm.patchValue(value);
    }

    @Output()
    dataChange: EventEmitter<IServerProperties> = new EventEmitter<
        IServerProperties
    >();

    constructor(private fb: FormBuilder) {}

    ngOnInit(): void {
        this.serverForm = this.fb.group({
            productName: new FormControl('', Validators.required),
            serialNumber: new FormControl('', Validators.required),
            pid: new FormControl('', Validators.required),
            uuid: new FormControl('', Validators.required),
            biosVersion: new FormControl('', Validators.required),
            description: new FormControl('', Validators.required),
            assetTag: new FormControl('', Validators.required),
        });
    }

    submit() {
        if (this.serverForm.dirty && this.serverForm.valid) {
            this.dataChange.emit(
                Object.assign({}, this.data, this.serverForm.value)
            );
        }
    }
}
