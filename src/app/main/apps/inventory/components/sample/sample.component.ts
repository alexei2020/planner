import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DataBindingDirective } from '@progress/kendo-angular-grid';
import { process } from '@progress/kendo-data-query';
import { Subject } from 'rxjs';
import { isNullOrUndefined } from 'util';

import { employees } from './employees';
import { images } from './images';

@Component({
    selector: 'inventory-sample',
    templateUrl: './sample.component.html',
    styleUrls: ['./sample.component.scss'],
})
export class SampleComponent implements OnInit, OnDestroy {
    // Private
    private _unsubscribeAll: Subject<any>;

    @ViewChild(DataBindingDirective) dataBinding: DataBindingDirective;
    public gridData: any[] = employees;
    public gridView: any[];

    public mySelection: string[] = [];
    public clickedRowItem: any;

    /**
     * Constructor
     */
    constructor(private route: Router) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    public ngOnInit(): void {
        this.gridView = this.gridData;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    onCellClick({ dataItem }) {
        this.clickedRowItem = dataItem;
    }
    onDblClick() {
        if (isNullOrUndefined(this.clickedRowItem)) {
            return;
        }

        this.route.navigate(['inventory/device', this.clickedRowItem.id]);
    }

    public onFilter(inputValue: string): void {
        this.gridView = process(this.gridData, {
            filter: {
                logic: 'or',
                filters: [
                    {
                        field: 'full_name',
                        operator: 'contains',
                        value: inputValue,
                    },
                    {
                        field: 'job_title',
                        operator: 'contains',
                        value: inputValue,
                    },
                    {
                        field: 'budget',
                        operator: 'contains',
                        value: inputValue,
                    },
                    {
                        field: 'phone',
                        operator: 'contains',
                        value: inputValue,
                    },
                    {
                        field: 'address',
                        operator: 'contains',
                        value: inputValue,
                    },
                ],
            },
        }).data;

        this.dataBinding.skip = 0;
    }

    public showDetails(event: any, dataItem: any) {}

    public photoURL(dataItem: any): string {
        const code: string = dataItem.img_id + dataItem.gender;
        const image: any = images;

        return image[code];
    }

    public flagURL(dataItem: any): string {
        const code: string = dataItem.country;
        const image: any = images;

        return image[code];
    }
}
