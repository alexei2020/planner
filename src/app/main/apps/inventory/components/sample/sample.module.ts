import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { ExcelModule, GridModule, PDFModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';

import { RatingComponent } from './rating/rating.component';
import { SampleComponent } from './sample.component';

@NgModule({
    declarations: [SampleComponent, RatingComponent],
    imports: [
        CommonModule,
        RouterModule,
        GridModule,
        ChartsModule,
        InputsModule,
        PDFModule,
        ExcelModule,
    ],
    exports: [SampleComponent],
})
export class SampleModule {}
