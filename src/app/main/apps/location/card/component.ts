
import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
@Component({
    selector: 'location-card',
    templateUrl: './template.html',
    styleUrls: ['./style.scss']
})
export class LocationCardComponent implements OnInit {
    private _gridData: any[];
    skip = 0;
    get gridData(): any[] {
        return this._gridData;
    }
    @Input() set gridData(data: any[]) {
        this._gridData = data;
        this.loadItems();
    }
    pageSize = 5;
    gridView: GridDataResult;
    constructor() {
        // this.loadItems();
    }
    public pageChange(event: PageChangeEvent): void {
        this.skip = event.skip;
        this.loadItems();
    }
    private loadItems(): void {
        this.gridView = {
            data: this.gridData.slice(this.skip, this.skip + this.pageSize),
            total: this.gridData.length
        };
    }
    ngOnInit() {

    }
}
