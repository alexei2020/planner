import { Component, EventEmitter, Input, OnDestroy, Output, SimpleChanges, OnChanges } from '@angular/core';
import { DialogCloseResult, DialogService } from '@progress/kendo-angular-dialog';
import { NotificationService } from 'app/main/shared/services';
import { TenantDetailService } from 'app/resource/tenant.gen/services';
import _ from 'lodash';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DeleteTenantConfirmationModalComponent } from '../dialogs/delete-tenant-confirmation-modal/delete-tenant-confirmation-modal.component';
import { ManageTenantDialogComponent } from '../dialogs/manage-tenant-dialog/manage-tenant-dialog.component';
import { TenantItem } from '../shared/tenant-item';

@Component({
  selector: 'app-tenant-list',
  templateUrl: './tenant-list.component.html',
  styleUrls: ['./tenant-list.component.scss'],
})
export class TenantListComponent implements OnChanges, OnDestroy {
  public location: string;
  public fixedLocation = 'ws.io';

  // unsubscribe all
  private _unsubscribeAll: Subject<any>;
  private ws;

  @Input()
  public tenantsList: TenantItem[];

  @Input()
  public selectedTenant: TenantItem;

  @Input()
  public apiGroup: string;

  @Output()
  selectedTenantChanged: EventEmitter<TenantItem>;

  @Output()
  selectedTenantDblClickChanged: EventEmitter<TenantItem>;

  @Output()
  tenantsListChanges: EventEmitter<void>;

  constructor(
    private readonly service: TenantDetailService,
    private _dialogService: DialogService,
    private _notificationService: NotificationService
  ) {
    this._unsubscribeAll = new Subject();
    this.tenantsList = [];
    this.selectedTenantChanged = new EventEmitter();
    this.selectedTenantDblClickChanged = new EventEmitter();
    this.tenantsListChanges = new EventEmitter();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const tenantsListChanges = _.get(changes, 'tenantsList', null);

    if (_.isEmpty(tenantsListChanges)) {
      return;
    }
    if (!this.selectedTenant) {
      const selectedTenant = _.first(this.tenantsList);
      this.selectTenant(selectedTenant);
      return;
    }
    this.selectTenant(this.selectedTenant);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll?.next();
    this._unsubscribeAll?.complete();
    this.ws?.complete();
  }

  selectTenant(tenant: TenantItem): void {
    this.selectedTenantChanged.next(tenant);
  }

  onTenantDblClick(tenant: TenantItem): void {
    this.selectedTenantDblClickChanged.next(tenant);
  }

  openEditTenantDialog(tenant: TenantItem): void {
    const dialogRef = this._dialogService.open({
      title: 'Edit Tenant Info',
      content: ManageTenantDialogComponent,
    });

    const editLocationData = dialogRef.content.instance as ManageTenantDialogComponent;
    editLocationData.editMode = true;
    editLocationData.selectedTenant = tenant;
    editLocationData.scopeApiGroup = this.apiGroup;
    editLocationData.name = tenant.metadata.name;
    editLocationData.address = tenant.specAddress;

    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
      if (result instanceof DialogCloseResult) {
        return;
      }
      if (result.primary) {
        this._notificationService.openSnackBar('Tenant information was edited successful!', 'Dismiss');
        this.tenantsListChanges.next();
      }
    });
  }

  openDeleteTenantDialog(tenant: TenantItem): void {
    const dialogRef = this._dialogService.open({
      title: 'Delete Tenant Info',
      content: DeleteTenantConfirmationModalComponent,
    });

    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
      if (result instanceof DialogCloseResult) {
        return;
      }
      if (!result.primary) {
        return;
      }
      const id = tenant.metadata.name;
      const location = tenant.apiVersion.split('/', -1).slice(0, -1).join('/');
      this.service.delete(id, location).subscribe(
        () => {
          this._notificationService.openSnackBar('Tenant was deleted successfully!', 'Dismiss');
          this.tenantsListChanges.next();
        },
        (error) =>
          this._notificationService.openSnackBar(`Failed to delete tenant ${id}. Error: ${error.message}`, 'Dismiss')
      );
    });
  }
}
