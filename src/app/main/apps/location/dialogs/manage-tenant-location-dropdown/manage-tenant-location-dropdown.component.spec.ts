import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTenantLocationDropdownComponent } from './manage-tenant-location-dropdown.component';

describe('ManageTenantLocationDropdownComponent', () => {
  let component: ManageTenantLocationDropdownComponent;
  let fixture: ComponentFixture<ManageTenantLocationDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageTenantLocationDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTenantLocationDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
