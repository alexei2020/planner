import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  EventEmitter,
  Output,
  HostListener,
  ElementRef,
  ViewChild,
} from '@angular/core';
import {
  LocationDataItem,
  LocationItem,
  LocationListData,
  LocationListItem,
  Metadata,
} from 'app/components/shared/location-item';
import { FuseLocationDataManagementService } from 'app/services/location-data-management.service';
import { FuseLocationService } from 'app/services/location.service';
import { StringMap } from 'app/typings/backendapi.gen';
import { ILocation, ILocationDataItem, ILocations } from 'app/typings/location';
import _ from 'lodash';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TenantItem } from '../../shared/tenant-item';

@Component({
  selector: 'app-manage-tenant-location-dropdown',
  templateUrl: './manage-tenant-location-dropdown.component.html',
  styleUrls: ['./manage-tenant-location-dropdown.component.scss'],
})
export class ManageTenantLocationDropdownComponent implements OnInit, OnDestroy {
  locationDataSource: any[];
  filteredLocationData: any[];
  selectedLocation: string;
  show = false;
  searchTerm = '';

  currentLocation = 'ws.io';
  locationList: any[];
  data: {
    name: string;
    location: string | null;
  };

  @ViewChild('anchor')
  anchor: ElementRef;

  @ViewChild('popup', { read: ElementRef })
  popup: ElementRef;

  @Input()
  selectedTenant: TenantItem;

  @Input()
  parentLocationLink: string;

  @Input()
  editedLocationSelfLink: string;

  @Input() public location: string | null;

  @Output()
  locationChanged: EventEmitter<any>;

  private _unsubscribeAll: Subject<any>;

  constructor(
    private _locationService: FuseLocationService,
    private locationDataManagementService: FuseLocationDataManagementService
  ) {
    this._unsubscribeAll = new Subject();
    this.locationChanged = new EventEmitter();
  }

  async ngOnInit(): Promise<void> {
    this.data = {
      name: '',
      location: this.location,
    };
    if (!this.selectedTenant || !this.selectedTenant.name || this.selectedTenant.name.length <= 0) {
      return;
    }
    let accessibleData = await this._locationService
      .getAccessibleData(this.selectedTenant.name)
      .pipe(takeUntil(this._unsubscribeAll))
      .map((data) => data.spec.locations)
      .toPromise();
    const apiVersion = _.split(this.selectedTenant.apiVersion, '/');
    const apiGroup = `${this.selectedTenant.name}.${_.first(apiVersion)}`;
    this._locationService
      .getHierarchicalData(apiGroup)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((locationList: ILocations) => {
        this.locationList = [];
        if (this.data.location && this.data.location === 'ws.io') {
          accessibleData = {};
          accessibleData[this.data.location] = {
            APIGroupName: this.data.location,
            APIGroupUID: this.data.location,
            RBAC: undefined,
          };
          // @ts-ignore
          locationList.items = [new LocationItem({
              apiVersion: this.data.location + '/v1',
              name: this.data.location,
              metadata: new Metadata({
                name: this.data.location,
              }),
            }),
          ];
          // @ts-ignore
          locationList.apiVersion = 'ws.io/v1';
        }
        const data = locationList.items.map((item: ILocation) => {
          this.updateCurrentLocationData(locationList, accessibleData, this.selectedTenant);
        });
      });
  }

  getLocationListData(locationList: ILocations, accessibleData: StringMap<ILocationDataItem>): LocationListData {
    const innerLocationList: LocationListItem[] = [];

    const data = _.map(locationList.items, (item: ILocation) => {
      innerLocationList.push(
        new LocationListItem({
          label: item.metadata.name,
          name: item.metadata.name,
          value: accessibleData[item.metadata.name].APIGroupName,
        })
      );
      const parentLink = item.spec?.base.parent_link === undefined ? '' : item.spec.base.parent_link;

      return new LocationDataItem({
        name: item.metadata.name,
        selfLink: item.metadata.selfLink,
        parentLink: parentLink,
        value: accessibleData[item.metadata.name].APIGroupName,
        APIGroupName: accessibleData[item.metadata.name].APIGroupName,
        APIGroupUID: accessibleData[item.metadata.name].APIGroupUID,
        RBAC: accessibleData[item.metadata.name].RBAC,
      });
    });

    const locationListData = new LocationListData({
      locationList: innerLocationList,
      locationData: data,
    });

    return locationListData;
  }

  private updateCurrentLocationData(
    locationList: ILocations,
    accessibleData: StringMap<ILocationDataItem>,
    tenant: TenantItem
  ): void {
    this.locationList = [];

    const locationListData = this.getLocationListData(locationList, accessibleData);
    this.locationList = locationListData.locationData;

    const root = this.locationDataManagementService.getLocationRoot(locationListData.locationData);
    const parentLocation = this.getParentLocation(root, this.parentLocationLink);
    let selectedLoc = parentLocation?.name;
    if (!selectedLoc) {
      selectedLoc = tenant.name ? tenant.name : root.name;
      if (this.data.location === 'ws.io') {
        selectedLoc = this.data.location;
      }
    }
    this.selectedLocation = selectedLoc;
    this.locationDataSource = [root];
    this.filteredLocationData = this.locationDataSource;

    const currentLocationName = this._locationService.locationDataAsKeyValue().locationName;
    const subTree = this.getCurrentLocationSubTree(currentLocationName, this.filteredLocationData);
    if (this.data.location === 'ws.io') {
      return;
    }

    if (subTree) {
      this.data.location = subTree.APIGroupName;
    } else {
      console.log('Failed to find subtree');
    }
  }

  private getCurrentLocationSubTree(currentLocation: string, tree: LocationDataItem[]): LocationDataItem {
    let currentLocationSubTree: LocationDataItem;
    _.forEach(tree, (item) => {
      if (item.name === currentLocation) {
        currentLocationSubTree = item;
        return false;
      }
      if (!_.isEmpty(item.children)) {
        currentLocationSubTree = this.getCurrentLocationSubTree(currentLocation, item.children);
        if (!_.isEmpty(currentLocationSubTree)) {
          return false;
        }
      }
    });
    return currentLocationSubTree;
  }

  close(): void {
    this.show = false;
  }

  onToggle(): void {
    this.show = !this.show;
    if (this.show) {
      this.filteredLocationData = this.locationDataSource;
      this.searchTerm = '';
    }
  }

  updateLocation({ dataItem }: any): void {
    this.show = false;
    this.selectedLocation = dataItem.name;
    this.data.location = dataItem.APIGroupName;
    this.locationChanged.next(dataItem);
  }

  onKeyUp(value: string): void {
    this.filteredLocationData = this.hierarchicalSearch(this.locationDataSource, value);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  isDisabledNode = (dataItem: any) => {
    return dataItem.selfLink === this.editedLocationSelfLink;
  };

  @HostListener('document:click', ['$event'])
  documentClick(event: any): void {
    if (!this.isPopup(event.target)) {
      this.close();
    }
  }

  private isPopup(target: any): boolean {
    return (
      this.anchor.nativeElement.contains(target) || (this.popup ? this.popup.nativeElement.contains(target) : false)
    );
  }

  private contains(text: string, term: string): boolean {
    return text.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  }

  private hierarchicalSearch(items: LocationListItem[], term: string): LocationListItem[] {
    const hierarchicalItems = _.reduce(
      items,
      (acc: LocationListItem[], item) => {
        if (this.contains(item.name, term)) {
          acc.push(item);
        } else if (item.children && item.children.length > 0) {
          const newItems = this.hierarchicalSearch(item.children, term);

          if (newItems.length > 0) {
            acc.push({ ...item, name: item.name, children: newItems });
          }
        }
        return acc;
      },
      []
    );

    return hierarchicalItems;
  }

  private getParentLocation(location: LocationDataItem, link: string): LocationDataItem {
    if (location.selfLink === link) {
      return location;
    }
    const parentLocation = this.findParentLocation(location.children, link);
    return parentLocation;
  }

  private findParentLocation(locations: LocationDataItem[], link: string): LocationDataItem {
    let parentLocation: LocationDataItem = null;
    _.forEach(locations, (location) => {
      if (location.selfLink === link) {
        parentLocation = location;
        return false;
      }
      if (!_.isEmpty(location.children)) {
        parentLocation = this.findParentLocation(location.children, link);
      }
    });
    return parentLocation;
  }
}
