import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DialogContentBase, DialogRef} from '@progress/kendo-angular-dialog';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {GeolocationService, IGeoLocation} from '../../../../services/geolocation.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NotificationService} from '../../../../shared/services';
import { TenantItem } from '../../shared/tenant-item';
// import { environment } from '../../'
@Component({
  selector: 'app-manage-location-dialog',
  templateUrl: './manage-location-dialog.component.html',
  styleUrls: ['./manage-location-dialog.component.scss']
})
export class ManageLocationDialogComponent extends DialogContentBase implements OnInit, OnDestroy {

  @ViewChild('addressACRef')
  addressACRef: ElementRef;

  @Input() public set address(value: string) {
    this.formGroup.controls.address.setValue(value);
    this._address = value;
  }

  public get address(): string {
    return this._address;
  }

  @Input() public set name(value: string) {
    this.formGroup.controls.name.setValue(value);
    this._name = value;
  }

  @Input()
  selectedTenant: TenantItem;

  @Input()
  parentLocationLink: string;

  @Input()
  isParentLocation: boolean;

  public get name(): string {
    return this._name;
  }

  public formGroup: FormGroup = this._fb.group({
    name: [this.name, Validators.required],
    address: [this.address, Validators.required],
    autoAddress: new FormControl(),
  });

  private _name: string;
  private _address: string;
  private _parentLocationLink: string;

  private _unsubscribeAll: Subject<any> = new Subject();

  public editMode = false;
  public dialogTitle: string;
  public selectedLocation: IGeoLocation;
  public showAddressAC = true;
  public scopeApiGroup: string;

  constructor(
    private _dialog: DialogRef,
    private _fb: FormBuilder,
    private _geoLocationService: GeolocationService,
    private _notificationService: NotificationService
  ) {
    super(_dialog);

  }

  ngOnInit(): void {
    if (this.address) {
      this.showAddressAC = false;
    }
  }

  onCancelAction(): void {
    this._dialog.close({
      primary: false,
      text: 'Cancel'
    });
  }

  onSaveAction(): void {
    let location: IGeoLocation = {
      apiVersion: this.scopeApiGroup + '/v1',
      kind: 'Location',
      metadata: {
        name: this.formGroup.get('name').value
      },
      spec: {
        base: {
          parent_link: this.selectedLocation?.metadata?.selfLink || ''
        },
        address: this.formGroup.get('address').value,
      }
    };

    if (this.editMode) {
      const oldLocationName = this.selectedLocation?.metadata.name;
      location = this.selectedLocation;
      location.metadata.name = this.formGroup.get('name').value;
      location.spec = {
        ...location.spec,
        address: this.formGroup.get('address').value,
      };
      if (this._parentLocationLink) {
        location.spec.base.parent_link = this._parentLocationLink;
      }

      this._geoLocationService.update(this.scopeApiGroup, oldLocationName, location)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((res) => {
          console.log('Success EDIT result -> ', res);
          this._dialog.close({
            primary: true,
            text: 'Edited'
          });
        }, (error) => {
          this._notificationService.openSnackBar('Error: ' + error.message, 'Dismiss');
          this._dialog.close({
            primary: false,
            text: error
          });
        });

    } else {
      this._geoLocationService.add(this.scopeApiGroup, location)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((res) => {
          console.log('Success result -> ', res);
          this._dialog.close({
            primary: true,
            text: 'Saved'
          });
        }, (error) => {
          this._notificationService.openSnackBar('Error: ' + error.message, 'Dismiss');
          this._dialog.close({
            primary: false,
            text: error
          });
        });
    }
  }

  onAutocompleteSelected(event): void {
    console.log('AC -> ', event);
    this.showAddressAC = false;
  }

  async onLocationSelected(event) {
    console.log('ACL -> ', event);
    this.formGroup.get('address').setValue(this.addressACRef.nativeElement.value);
    //fetch timezone by lat/lng
    // const timezoneObj = await this._geoLocationService.getTimeZoneByLatLng(event.latitude, event.longitude).toPromise();
    // this.formGroup.get('timezone').setValue(timezoneObj.timeZoneId);
  }

  onAddressFocus(): void {
    this.showAddressAC = true;
  }

  onAddressBlur(): void {
    this.showAddressAC = !this.formGroup.get('address').value;
  }

  focusAddressAC(): void {
    if (this.showAddressAC) {
      this.addressACRef.nativeElement.focus();
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  onLocationChanged(parentLocation): void {
    this._parentLocationLink = parentLocation.selfLink;
  }
}
