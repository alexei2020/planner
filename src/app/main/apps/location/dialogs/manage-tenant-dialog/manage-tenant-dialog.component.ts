import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogContentBase, DialogRef } from '@progress/kendo-angular-dialog';
import { NotificationService } from 'app/main/shared/services';
import { TenantDetailService } from 'app/resource/tenant.gen/services';
import { Tenant } from 'app/typings/backendapi.gen';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TenantItem } from '../../shared/tenant-item';

@Component({
  selector: 'app-manage-tenant-dialog',
  templateUrl: './manage-tenant-dialog.component.html',
  styleUrls: ['./manage-tenant-dialog.component.scss'],
})
export class ManageTenantDialogComponent extends DialogContentBase implements OnInit, OnDestroy {
  @ViewChild('addressACRef')
  addressACRef: ElementRef;

  @Input() public set address(value: string) {
    this.formGroup.controls.address.setValue(value);
    this._address = value;
  }

  public get address(): string {
    return this._address;
  }

  @Input() public set name(value: string) {
    this.formGroup.controls.name.setValue(value);
    this._name = value;
  }

  public get name(): string {
    return this._name;
  }

  public formGroup: FormGroup = this._fb.group({
    name: [this.name, Validators.required],
    address: [this.address, Validators.required],
    autoAddress: new FormControl(),
  });

  public editMode = false;
  public dialogTitle: string;
  public location = 'ws.io';
  public selectedTenant: TenantItem;
  public showAddressAC = true;
  public scopeApiGroup: string;

  private _name: string;
  private _address: string;
  private _unsubscribeAll: Subject<any> = new Subject();

  constructor(
    private _dialog: DialogRef,
    private _fb: FormBuilder,
    private _notificationService: NotificationService,
    private readonly _tenantDetailService: TenantDetailService
  ) {
    super(_dialog);
  }

  ngOnInit(): void {
    if (this.address) {
      this.showAddressAC = false;
    }
  }

  onCancelAction(): void {
    this._dialog.close({
      primary: false,
      text: 'Cancel',
    });
  }

  onSaveAction(): void {
    if (this.editMode) {
      this.editTenant();
      return;
    }
    this.addTenant();
  }

  onAutocompleteSelected(event): void {
    this.showAddressAC = false;
  }

  onLocationSelected(event): void {
    this.formGroup.get('address').setValue(this.addressACRef.nativeElement.value);
  }

  onAddressFocus(): void {
    this.showAddressAC = true;
  }

  onAddressBlur(): void {
    this.showAddressAC = !this.formGroup.get('address').value;
  }

  focusAddressAC(): void {
    if (this.showAddressAC) {
      this.addressACRef.nativeElement.focus();
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  private editTenant(): void {
    const tenant = {
      apiVersion: `${this.scopeApiGroup}/v1`,
      kind: 'Tenant',
      metadata: {
        name: this.formGroup.get('name').value,
        uid: this.selectedTenant.metadata.uid,
        resourceVersion: this.selectedTenant.metadata.resourceVersion,
      },
      spec: {
        address: this.formGroup.get('address').value,
      },
    } as Tenant;

    this._tenantDetailService
      .update(tenant, this.scopeApiGroup)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (res) => {
          this._dialog.close({
            primary: true,
            text: 'Saved',
          });
        },
        (error) => {
          this._notificationService.openSnackBar(`Error: ${error.message}`, 'Dismiss');
          this._dialog.close({
            primary: false,
            text: error,
          });
        }
      );
  }

  private addTenant(): void {
    const tenant = {
      apiVersion: `${this.location}/v1`,
      kind: 'Tenant',
      metadata: {
        name: this.formGroup.get('name').value,
      },
      spec: {
        address: this.formGroup.get('address').value,
      },
    } as Tenant;
    this._tenantDetailService
      .add(tenant, this.scopeApiGroup)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (res) => {
          this._dialog.close({
            primary: true,
            text: 'Saved',
          });
        },
        (error) => {
          this._notificationService.openSnackBar('Error: ' + error.message, 'Dismiss');
          this._dialog.close({
            primary: false,
            text: error,
          });
        }
      );
  }
}
