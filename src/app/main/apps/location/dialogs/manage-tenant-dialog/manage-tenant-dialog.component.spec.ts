import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTenantDialogComponent } from './manage-tenant-dialog.component';

describe('ManageTenantDialogComponent', () => {
  let component: ManageTenantDialogComponent;
  let fixture: ComponentFixture<ManageTenantDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageTenantDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTenantDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
