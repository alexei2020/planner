import { Component } from '@angular/core';
import { DialogContentBase, DialogRef } from '@progress/kendo-angular-dialog';
import _ from 'lodash';
import { locationConfig } from './../../location.config';

@Component({
  selector: 'app-delete-tenant-confirmation-modal',
  templateUrl: './delete-tenant-confirmation-modal.component.html',
  styleUrls: ['./delete-tenant-confirmation-modal.component.scss'],
})
export class DeleteTenantConfirmationModalComponent extends DialogContentBase {
  confirmationText: string;
  constructor(private _dialog: DialogRef) {
    super(_dialog);
    this.confirmationText = '';
  }
  get expectedConfirmationText(): string {
    return locationConfig.delete.confirmationTenantText;
  }
  get isConfirmed(): boolean {
    const isConfirmed = _.isEqual(this.confirmationText.toLowerCase(), this.expectedConfirmationText.toLowerCase());
    return isConfirmed;
  }

  onCancel(): void {
    this._dialog.close({
      primary: false,
      text: 'Cancel',
    });
  }

  onDelete(): void {
    this._dialog.close({
      primary: true,
      text: 'Yes',
    });
  }
}
