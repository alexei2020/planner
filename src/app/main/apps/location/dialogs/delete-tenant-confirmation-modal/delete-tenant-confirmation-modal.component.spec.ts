import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTenantConfirmationModalComponent } from './delete-tenant-confirmation-modal.component';

describe('DeleteTenantConfirmationModalComponent', () => {
  let component: DeleteTenantConfirmationModalComponent;
  let fixture: ComponentFixture<DeleteTenantConfirmationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteTenantConfirmationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTenantConfirmationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
