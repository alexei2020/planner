import { Component, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { MenuEvent, MenuItemComponent } from '@progress/kendo-angular-menu';

@Component({
  selector: 'app-location-item-context-menu',
  templateUrl: './location-item-context-menu.component.html',
  styleUrls: ['./location-item-context-menu.component.scss'],
})
export class LocationItemContextMenuComponent {
  @Input()
  target: ElementRef;

  @Input()
  addButtonHidden: boolean;

  @Output()
  openAddItemDialog: EventEmitter<void>;

  @Output()
  openEditItemDialog: EventEmitter<void>;

  @Output()
  openDeleteItemDialog: EventEmitter<void>;

  addButtonTitle: string;
  editButtonTitle: string;
  deleteButtonTitle: string;

  constructor() {
    this.openAddItemDialog = new EventEmitter();
    this.openEditItemDialog = new EventEmitter();
    this.openDeleteItemDialog = new EventEmitter();
    this.addButtonTitle = 'Add';
    this.editButtonTitle = 'Edit';
    this.deleteButtonTitle = 'Delete';
  }

  onOpenAddItemDialog(): void {
    this.openAddItemDialog.next();
  }

  onOpenEditItemDialog(): void {
    this.openEditItemDialog.next();
  }

  onOpenDeleteItemDialog(): void {
    this.openDeleteItemDialog.next();
  }

  onSelectMenuItem(event: MenuEvent): void {
    const item: MenuItemComponent = event.item;

    if (item.text === this.addButtonTitle) {
      this.onOpenAddItemDialog();
      return;
    }

    if (item.text === this.editButtonTitle) {
      this.onOpenEditItemDialog();
      return;
    }

    this.onOpenDeleteItemDialog();
  }
}
