import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationItemContextMenuComponent } from './location-item-context-menu.component';

describe('LocationItemContextMenuComponent', () => {
  let component: LocationItemContextMenuComponent;
  let fixture: ComponentFixture<LocationItemContextMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationItemContextMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationItemContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
