export class TenantItem {
  readonly _id: string;
  readonly apiVersion: string;
  readonly kind: string;
  readonly metadata: TenantItemMetadata;
  readonly name: string;
  readonly specAddress: string;

  constructor({ _id, apiVersion, kind, metadata, name, specAddress }: Partial<TenantItem> = {}) {
    this._id = _id;
    this.apiVersion = apiVersion;
    this.kind = kind;
    this.metadata = metadata;
    this.name = name;
    this.specAddress = specAddress;
  }
}

export class TenantItemMetadata {
  readonly creationTimestamp: string;
  readonly modificationTimestamp: string;
  readonly name: string;
  readonly resourceVersion: string;
  readonly selfLink: string;
  readonly uid: string;

  constructor({
    creationTimestamp,
    modificationTimestamp,
    name,
    resourceVersion,
    selfLink,
    uid,
  }: Partial<TenantItemMetadata> = {}) {
    this.creationTimestamp = creationTimestamp;
    this.modificationTimestamp = modificationTimestamp;
    this.name = name;
    this.resourceVersion = resourceVersion;
    this.selfLink = selfLink;
    this.uid = uid;
  }
}
