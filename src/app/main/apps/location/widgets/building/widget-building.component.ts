import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'widget-building',
    templateUrl: './widget-building.component.html',
    styleUrls: ['./widget-building.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: fuseAnimations,
})
export class WidgetAComponent implements OnInit, OnDestroy {
    // Private
    private _unsubscribeAll: Subject<any>;

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    @Input()
    widget: any;

    @Input()
    resizeEvent: EventEmitter<any>;

    /**
     * Constructor
     */
    constructor() {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.resizeEvent
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((widget) => {
                if (widget === this.widget) {
                    // or check id , type or whatever you have there
                    // resize your widget, chart, map , etc.
                    console.log(widget);
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
