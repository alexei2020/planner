import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule, Routes } from '@angular/router';

import { GridsterModule } from 'angular-gridster2';
import { GridModule } from '@progress/kendo-angular-grid';
import { LocationPageComponent } from './location.component';
import { FuseLocationResolver } from '../../../services/location-resolverservice';

import { MatDialogModule } from '@angular/material/dialog';

import { AgmCoreModule } from '@agm/core';

import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { LocationCardComponent } from './card/component';
import { MatCardModule } from '@angular/material/card';
import { ComponentsModule } from '../../../components/module';
import { FuseDirectivesModule } from '@fuse/directives/directives';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { FlexModule } from '@angular/flex-layout';
import { AutoCompleteModule } from '@progress/kendo-angular-dropdowns';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { SharedModule } from '../../shared/shared.module';
import { ManageLocationDialogComponent } from './dialogs/manage-location-dialog/manage-location-dialog.component';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { ContextMenuModule } from '@progress/kendo-angular-menu';
import { TabStripModule } from '@progress/kendo-angular-layout';
import { TenantListComponent } from './tenant-list/tenant-list.component';
import { LocationItemContextMenuComponent } from './location-item-context-menu/location-item-context-menu.component';
import { ManageTenantDialogComponent } from './dialogs/manage-tenant-dialog/manage-tenant-dialog.component';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ManageTenantLocationDropdownComponent } from './dialogs/manage-tenant-location-dropdown/manage-tenant-location-dropdown.component';
import { DeleteTenantConfirmationModalComponent } from './dialogs/delete-tenant-confirmation-modal/delete-tenant-confirmation-modal.component';
import { RoomPlannerModule } from 'app/roomplanner/module';
export const LOCATION_PAGE_ROUTE: Route = {
  path: '',
  component: LocationPageComponent,
  resolve: { resolvedLocationData: FuseLocationResolver },
};

@NgModule({
  declarations: [
    LocationPageComponent,
    LocationCardComponent,
    ManageLocationDialogComponent,
    TenantListComponent,
    LocationItemContextMenuComponent,
    ManageTenantDialogComponent,
    ManageTenantLocationDropdownComponent,
    DeleteTenantConfirmationModalComponent,
  ],
  imports: [
    CommonModule,
    GridsterModule,
    GridModule,
    MatDialogModule,
    MatCardModule,
    ComponentsModule,
    FuseDirectivesModule,
    SharedModule,
    TreeViewModule,
    AutoCompleteModule,
    ButtonModule,
    ContextMenuModule,
    DialogModule,
    FlexModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAfPO_C_Wr_VMu7b7tG54VG-wiPGPFVqQw',
      libraries: ['places'],
    }),
    MatGoogleMapsAutocompleteModule,
    RouterModule.forChild([LOCATION_PAGE_ROUTE]),
    TabStripModule,
    PopupModule,
    RoomPlannerModule,
  ],
  exports: [RouterModule],
})
export class LocationPageModule {}
