import { AgmGeocoder, GeocoderRequest, MapTypeStyle } from '@agm/core';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseSearchBarService } from '@fuse/services/search-bar.service';
import { DialogCloseResult, DialogService } from '@progress/kendo-angular-dialog';
import { SelectEvent, TabStripComponent } from '@progress/kendo-angular-layout';
import { ElasticSearchParserService, ESResponse } from 'app/services/esparser.service';
import { IScope } from 'app/typings/location';
import { environment } from 'environments/environment';
import { ObservableInput, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { webSocket } from 'rxjs/webSocket';

import { FuseLocationService } from '../../../services/location.service';
import { GeolocationService, IGeoLocation } from '../../services/geolocation.service';
import { BreadcrumbService } from '../../shared/breadcrumb-bar.service';
import { PageType } from '../../shared/breadcrumb-bar.service.gen';
import { NotificationService } from '../../shared/services';
import { UDCriteria, UpDownCard } from '../../shared/up-down-card';
import { ManageLocationDialogComponent } from './dialogs/manage-location-dialog/manage-location-dialog.component';
import { Location, LocationMarker } from './location';
import { TenantItem, TenantItemMetadata } from './shared/tenant-item';
import { v4 as uuid } from 'uuid';
import { TenantDetailService } from 'app/resource/tenant.gen/services';
import { ManageTenantDialogComponent } from './dialogs/manage-tenant-dialog/manage-tenant-dialog.component';
import { FuseTenantService, GlobalTenants } from 'app/components/tenant/tenant.service';
import _ from 'lodash';
import { appsConfig } from '../apps.config';
import {LocationWidgetComponent} from '../../../components/location/component';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
})
export class LocationPageComponent implements OnInit, OnDestroy {
  home: string;
  apiGroup: string;
  scopes: IScope[];
  scopeLocation: Location;
  locationList: Location[] = [];
  originalLocationList: Location[] = [];
  expandedLocationKeys: string[] = [];
  locationMarkers: LocationMarker[] = [];
  selectedLocation: Location;
  latitude: number;
  longitude: number;
  searchList: [] = [];

  locationNameFromURL: string;
  mapStyle: MapTypeStyle;
  mapStyles: MapTypeStyle[];

  cards: UpDownCard[];

  tenantsList: TenantItem[];
  tenantsListDataSource: TenantItem[];
  selectedTenant: TenantItem;
  isTenantTabSelected = true;
  searchValue: string;

  locationRoom: string = appsConfig.room.location;
  layers = appsConfig.room.layers;

  private bodybuilder = require('bodybuilder');
  private body;

  public colFieldToESField = {
    'name': 'object.metadata.name.keyword',
    'spec.address': 'object.spec.address.keyword',
  };
  public aggregatorStats = [
    {
      field: 'object.spec.address.keyword',
      name: 'Address',
      keys: [],
    },
  ];
  public summaryCards = [];
  private locationTabIndex: number;

  public isLoading = true;
  public latestCopyrightYear: number;
  public location: string;
  public fixedLocation = 'ws.io';
  public tenantName: string;
  public locationName: string;
  public wsFilter: string;
  private ws;

  @ViewChild('tenantTabstrip')
  public tenantTabstrip: TabStripComponent;

  private _unsubscribeAll: Subject<any> = new Subject();

  bounds = [
    [-122.0833381802915, 37.38758131970851],
    [-122.0806402197085, 37.39027928029151],
  ];
  child: any[] = [];
  address: any;
  name: string;
  isSelected: boolean;
  zoom: number;
  markers: any[];

  get isTenantTab(): boolean {
    return this.tenantTabstrip && this.tenantTabstrip.tabs.first.active;
  }

  get is3dViewVisible(): boolean {
    if (!this.selectedLocation) {
      return false;
    }
    return (this.selectedLocation.items?.length || 0) === 0;
  }

  constructor(
    private _breadcrumbService: BreadcrumbService,
    private _geolocationService: GeolocationService,
    private _locationService: FuseLocationService,
    private activatedRoute: ActivatedRoute,
    private _dialogService: DialogService,
    private _notificationService: NotificationService,
    private _fuseSearchBarService: FuseSearchBarService,
    private _esParser: ElasticSearchParserService,
    private _tenantDetailService: TenantDetailService,
    private _fuseTenantService: FuseTenantService,
    private _router: Router,
  ) {
    this._breadcrumbService.currentPage.next(PageType.LOCATION);

    this.latitude = 36.778259;
    this.longitude = -119.417931;
    this.isSelected = false;
    this.zoom = 2.2;
    this._fuseSearchBarService.aggregatorStats.next(this.aggregatorStats);
    this.tenantsList = [];
    this.tenantsListDataSource = [];
    this.locationTabIndex = 1;
  }

  async ngOnInit(): Promise<void> {
    this.mapStyle = {
      stylers: [
        {
          saturation: -45,
          lightness: -100,
        },
      ],
    };
    this.mapStyles = [];
    this.mapStyles.push(this.mapStyle);

    const result = await this._locationService.getScope().pipe(take(1)).toPromise();
    this.scopes = result.spec.scopes;

    /* TODO:
            Check if selectedLoc exists
            Get Scope
            Get locations from scope
            Set location to selectedLoc if exists and if
        * */

    this._fuseSearchBarService.tableSearchTerm
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((searchValue: string) => {
        this.searchValue = searchValue;
        this.filterData(this.searchValue);
      });

    this._fuseSearchBarService.filterChange.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: boolean) => {
      if (res) {
        this.updateFilters();
      }
    });
    this._locationService.isBrowserTime.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: boolean) => {
      this.loadTenants();
    });

    this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe((queryParams) => {
      if (queryParams.selectedLoc) {
        this.locationNameFromURL = queryParams.selectedLoc;
      }

      this.location = this._locationService.locationDataAsKeyValue().currentLocation;
      this.tenantName = this._locationService.locationDataAsKeyValue().tenantName;
      this.locationName = this._locationService.locationDataAsKeyValue().locationName;
      this.apiGroup = this._locationService.locationDataAsKeyValue().tenantAPIGroupName;

      if (this.fixedLocation) {
        this.location = this.fixedLocation;
      }
      this.initTenant();

      this.loadData();

      if ((!queryParams['loc'] || queryParams['loc'] === this.location) && !queryParams['esFilter']) {
        return;
      }
      if (queryParams['esFilter']) {
        this.mergeFilters(JSON.parse(queryParams['esFilter']));
      }

      if (queryParams['wsFilter']) {
        this.wsFilter = queryParams['wsFilter'];
      }
      const wsUrl =
        `${environment.baseUrl}ws/list/${this.location}/v1/tenants`.replace('http', 'ws') +
        '?wsSelector=includeSubtree=true';
      if (this.ws) {
        this.ws.complete();
      }
      this.ws = webSocket(wsUrl);
      this.ws.pipe(takeUntil(this._unsubscribeAll)).subscribe((rsp) => {
        if (!rsp) {
          return;
        }
        this.loadTenants();
      });
      this.buildQuery();
      this.loadTenants();
    });
  }

  async onSelectedTenantChanged(selectedTenant: TenantItem): Promise<void> {
    if (!selectedTenant) {
      return;
    }
    this.selectedTenant = selectedTenant;
    this.tenantName = this.selectedTenant.name;
    await this.reloadLocationByTenant(this.selectedTenant);
  }

  async onSelectedTenantDblClickChanged(selectedTenant: TenantItem): Promise<void> {
    if (!selectedTenant) {
      return;
    }
    this.selectedTenant = selectedTenant;
    this.tenantName = this.selectedTenant.name;
    this.tenantTabstrip.selectTab(this.locationTabIndex);
    await this.reloadLocationByTenant(this.selectedTenant);
  }

  async reloadLocationByTenant(tenant: TenantItem): Promise<void> {
    // this.scopeLocation = this.locationList.find((item) => item.metadata.name === this.selectedTenant.name);
    const apiVersion = _.split(tenant.apiVersion, '/');
    if (_.isEmpty(apiVersion)) {
      return;
    }

    this.apiGroup = `${tenant.name}.${_.first(apiVersion)}`;
    this.home = tenant.name;
    await this.populateLocationsList(this.apiGroup, tenant.name);
  }

  loadTenants(): void {
    if (!this.location) {
      return;
    }

    let timeZoneID = '';
    if (this._locationService.isBrowserTime.getValue()) {
      const timezone = jstz.determine();
      timeZoneID = timezone.name();
    } else {
      timeZoneID = this._locationService.locationDataAsKeyValue().timezone;
    }
    this.isLoading = true;

    this._tenantDetailService
      .getTenants(this.location, this.body)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (res: ESResponse) => {
          this.tenantsList = [];
          this.tenantsListDataSource = [];
          const data = this._esParser.items(res);
          data?.map((item) => {
            const metadata = new TenantItemMetadata({
              creationTimestamp: item.metadata.creationTimestamp,
              modificationTimestamp: item.metadata.modificationTimestamp,
              name: item.metadata.name,
              resourceVersion: item.metadata.resourceVersion,
              selfLink: item.metadata.selfLink,
              uid: item.metadata.uid,
            });
            const tenantItem = new TenantItem({
              _id: uuid(),
              apiVersion: item.apiVersion,
              kind: item.kind,
              metadata: metadata,
              name: item.metadata.name,
              specAddress: item.spec.address,
            });
            this.tenantsList.push(tenantItem);
          });
          this.tenantsListDataSource = this.tenantsList;
          this.tenantsList = _.orderBy(this.tenantsList, ['name']);
          this.tenantsListDataSource = _.orderBy(this.tenantsListDataSource, ['name']);
          this.filterData(this.searchValue);
          this.initTenant();
          // Update aggregator stats from the response
          this.aggregatorStats.forEach((item, index) => {
            const keys = this._esParser.aggrStats(res, item.field);
            const newStats = keys.map((key) => {
              const exists = this.aggregatorStats[index].keys.find((el) => el.keyName === key.key);
              return {
                name: key.key + '(' + key.doc_count + ')',
                keyName: key.key,
                count: key.doc_count,
                checked: exists ? exists.checked : false,
              };
            });
            this.updateAggregatorStats(index, newStats);
          });
          // force a refresh on components reading aggregatorStats
          this.aggregatorStats = this.aggregatorStats.slice();
          this._fuseSearchBarService.aggregatorStats.next(this.aggregatorStats);

          // Read SummaryStats
          this.summaryCards.forEach((card, index) => {
            // Update only if we have asked for it
            const count = this._esParser.aggrStats(res, card.name);
            if (typeof count === 'number') {
              this.summaryCards[index].count = count;
            }
          });
          // force refresh on components reading summaryCards
          this.summaryCards = this.summaryCards.slice();

          this.isLoading = false;
        },
        (err) => {
          this.isLoading = false;
        }
      );
  }
  private buildQuery(updateSummary = true): void {
    this.body = this.bodybuilder();

    // Add static filters
    if (this.wsFilter) {
      this.wsFilter.split(',').forEach((filter) => {
        const rule = filter.split('=');
        this.body = this.body.query('match', rule[0], rule[1]);
      });
    }

    // Summary filters
    this.summaryCards
      .filter((c) => c.selected)
      .forEach((card) => {
        card.fields.forEach((field) => {
          this.body = this.body.query('bool', (f) => {
            field.values.forEach((value) => {
              f = f.orFilter('term', field.name, value);
            });
            return f;
          });
        });
      });

    // User specified filters
    this.aggregatorStats.forEach((field) => {
      if (field.keys.some((key) => key.checked)) {
        this.body = this.body.query('bool', (f) => {
          field.keys
            .filter((key) => key.checked)
            .forEach((key) => {
              f = f.orFilter('term', field.field, key.keyName);
            });
          return f;
        });
      }
    });

    // Collect aggregators.
    this.aggregatorStats.forEach((item) => {
      this.body = this.body.aggregation('terms', item.field);
    });

    // Summary aggregators
    if (updateSummary) {
      this.summaryCards.forEach((card) => {
        card.fields.forEach((field) => {
          this.body = this.body.aggregation('filter', card.name, (f) => {
            field.values.forEach((value) => {
              f = f.orFilter('term', field.name, value);
            });
            return f;
          });
        });
      });
    }

    this.body = this.body.build();
  }

  public updateFilters(): void {
    // If any filter is selected, we need to deselect summarycards Total
    const selected = this.aggregatorStats.some((item) => item.keys.some((key) => key.checked === true));
    if (selected && this.summaryCards.length) {
      this.summaryCards[0].selected = false;
    }

    this.buildQuery(false);
    this.loadTenants();
  }

  public mergeFilters(filter): void {
    filter.forEach((item) => {
      const aggrStat = this.aggregatorStats.find((stat) => item.field === stat.field);
      if (!aggrStat) {
        this.aggregatorStats.push({
          field: item.field,
          name: item.field.replace('object.', '').replace('.keyword', ''),
          keys: item.keys.map((key) => ({
            name: key.keyName,
            keyName: key.keyName,
            count: 0,
            checked: key.checked,
          })),
        });
        return;
      }
      // aggrStat exists
      aggrStat.keys = item.keys.map((key) => ({
        name: key.keyName,
        keyName: key.keyName,
        count: 0,
        checked: key.checked,
      }));
    });
  }

  public updateSummaryFilters(): void {
    let updateSummary = false;
    // If total is selected, then we clear all other filters
    if (this.summaryCards.length && this.summaryCards[0].selected) {
      this.aggregatorStats.forEach((item) => {
        item.keys.forEach((key) => (key.checked = false));
      });
      updateSummary = true;
    }
    this.buildQuery(updateSummary);
    this.loadTenants();
  }

  private updateAggregatorStats(index: number, newStats: any[]): void {
    // conditions to update:
    // 1. If none of the keys are 'checked'
    // 2. If any of the other fields have keys checked
    // 3. Always retain checked key after update even if response does not have it
    if (!this.aggregatorStats[index].keys.some((key) => key.checked)) {
      this.aggregatorStats[index].keys = newStats;
      return;
    }

    // This field has a 'checked' key
    const otherStats = this.aggregatorStats.filter((el, i) => i !== index);
    if (otherStats.some((item) => item.keys.some((key) => key.checked))) {
      const checkedKeys = this.aggregatorStats[index].keys.filter((key) => key.checked);
      // Add back checked keys, if not present
      checkedKeys.forEach((key) => {
        if (newStats.findIndex((entry) => entry.keyName === key.keyName) === -1) {
          key.count = 0;
          newStats.unshift(key);
        }
      });
      this.aggregatorStats[index].keys = newStats;
      return;
    }
  }

  async loadData(): Promise<void> {
    this.isLoading = true;
    const result = await this._locationService.getScope().pipe(take(1)).toPromise();
    this.scopes = result.spec.scopes;

    if (this.selectedTenant) {
      await this.populateLocationsList(this.apiGroup, this.selectedTenant?.name);
    }
    this.isLoading = false;
  }

  // Changed temporarily so that we find home location
  async populateLocationsList(apiGroup: string, locationName: string): Promise<void> {
    const result: { items: any[] } = await this._geolocationService.getLocations(apiGroup).pipe(take(1)).toPromise();
    const items = result.items;

    this.scopeLocation = items?.find((item) => item.metadata.name === locationName);

    if (!this.scopeLocation) {
      this.scopeLocation = _.first(items);
    }

    await this.selectLocation(this.scopeLocation);

    const scopeLocationMetadata = this.scopeLocation.metadata?.selfLink;
    this.locationList = items
      // Find all children of selected element
      .filter((item) => item.spec.base.parent_link === scopeLocationMetadata)
      // Add to children sub-children
      .map((item) => this.foundSubChildren(items, item));

    this.originalLocationList = this.locationList.slice();
    this.populateSearchList(items);
    this.populateSelectedLocationChildren(items);
  }

  public searchLocationItems(items: any[], term: string): any[] {
    return items.reduce((acc, item) => {
      if (this.contains(item.text, term)) {
        acc.push(item);
      } else if (item.items && item.items.length > 0) {
        const newItems = this.searchLocationItems(item.items, term);

        if (newItems.length > 0) {
          acc.push({ text: item.text, items: newItems });
        }
      }

      return acc;
    }, []);
  }

  public contains(text: string, term: string): boolean {
    return text.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  }

  public onFilterLocations(filter: any): void {
    this.locationList = this.searchLocationItems(this.originalLocationList, filter);
  }

  onMouseOverMaker(infoWindow, gm): void {
    if (gm.lastOpen && gm.lastOpen.isOpen) {
      gm.lastOpen.close();
    }
    gm.lastOpen = infoWindow;
    infoWindow.open();
  }

  onMouseOutMarker(infoWindow, gm): void {
    if (gm.lastOpen && gm.lastOpen.isOpen) {
      gm.lastOpen.close();
    }
    gm.lastOpen = infoWindow;
    infoWindow.close();
  }

  public isLocationExpanded = (dataItem: any, index: string) => {
    const isExpanded = this.expandedLocationKeys.indexOf(index) > -1;
    dataItem.isExpanded = isExpanded;
    return isExpanded;
  }

  public handleLocationCollapse(node): void {
    this.expandedLocationKeys = this.expandedLocationKeys.filter((k) => k !== node.index);
  }

  public handleLocationExpand(node): void {
    this.expandedLocationKeys = this.expandedLocationKeys.concat(node.index);
  }

  openAddLocationDialog(parentLocation: Location): void {
    LocationWidgetComponent.updatedLocation = true;
    const dialogRef = this._dialogService.open({
      title: 'Add New Location',
      content: ManageLocationDialogComponent,
    });

    const addLocationData = dialogRef.content.instance;
    addLocationData.dialogTitle = 'Add New Location';
    addLocationData.selectedLocation = parentLocation;
    addLocationData.scopeApiGroup = this.apiGroup;

    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
      if (result instanceof DialogCloseResult) {
        return;
      }
      if (result.primary) {
        this._notificationService.openSnackBar('Location added successfully!', 'Dismiss');
        this.isLoading = true;
        setTimeout(async () => {
          await this.loadData();
          this._locationService.updateForceRefreshLocationListValue(true);
          this.isLoading = false;
        }, 1500);
      }
    });
  }

  async openEditLocationDialog(location: Location, isParentLocation: boolean = false): Promise<void> {
    LocationWidgetComponent.updatedLocation = true;
    const dialogRef = this._dialogService.open({
      title: 'Edit Location Info',
      content: ManageLocationDialogComponent,
    });

    const editLocationData = dialogRef.content.instance;
    editLocationData.editMode = true;
    editLocationData.selectedLocation = location;
    editLocationData.scopeApiGroup = this.apiGroup;
    editLocationData.name = location.metadata.name;
    editLocationData.address = location.spec.address;
    editLocationData.selectedTenant = this.selectedTenant;
    editLocationData.parentLocationLink = location.spec.base.parent_link;
    editLocationData.isParentLocation = isParentLocation;
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
      if (result instanceof DialogCloseResult) {
        return;
      }
      if (result.primary) {
        this._notificationService.openSnackBar('Location information edit successful!', 'Dismiss');
        this.loadData();
        this._locationService.updateForceRefreshLocationListValue(true);
      }
    });
  }

  openDeleteLocationDialog(location: Location): void {
    LocationWidgetComponent.updatedLocation = true;
    if (location.items?.length > 0) {
      this._notificationService.openSnackBar(
        `You can't delete location which has children locations. Please delete children locations first`,
        'Dismiss'
      );
      return;
    }
    const dialogRef = this._dialogService.open({
      title: 'Are You sure?',
      content:
        'Are You sure that You want to delete ' + location.metadata.name + ' location and all of its child locations?',
      actions: [
        { text: 'No', primary: false },
        { text: 'Yes', primary: true },
      ],
    });

    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
      if (result instanceof DialogCloseResult) {
        return;
      }
      if (result.primary) {
        this.removeLocation(location);
        this._notificationService.openSnackBar('Location deleted successfully!', 'Dismiss');
        this.loadData();
        this._locationService.updateForceRefreshLocationListValue(true);
        // TODO: send changed list
        this.emitLocationChanges();
      }
    });
  }

  removeLocation(location: Location): void {
    LocationWidgetComponent.updatedLocation = true;
    this.removeLocationRecursive(location, this.originalLocationList);
    this._geolocationService.removeLocation(location);
    this.locationList = this.originalLocationList;
  }

  private removeLocationRecursive(location: Location, array: Location[]): void {
    if (!array || array.length === 0) {
      return;
    }
    const targetLocationIndex = array.findIndex((el) => el.metadata.uid === location.metadata.uid);
    if (targetLocationIndex !== -1) {
      array.splice(targetLocationIndex, 1);
      return;
    }
    for (const loc of array) {
      this.removeLocationRecursive(location, loc.items);
    }
  }

  async selectLocation(location: Location): Promise<void> {
    if (!this.scopes || !location) {
      return;
    }
    let selectedLocationObj = this.scopes.find((s) => {
      return s.locationName === location?.metadata?.name;
    });
    if (selectedLocationObj === undefined) {
      await this.loadData();

      selectedLocationObj = this.scopes.find((s) => {
        return s.locationName === location?.metadata?.name;
      });
      if (selectedLocationObj === undefined) {
        const apiVersion = _.split(location.apiVersion, '/');
        if (_.isEmpty(apiVersion)) {
          return;
        } 
        location.apiGroup = _.first(apiVersion);
      }
    }
    if (selectedLocationObj) {
      location.apiGroup = selectedLocationObj.apiGroup;
    }
    this.selectedLocation = location;
    // if no lat or long search by address
    if (this.selectedLocation?.status?.geo?.latitude === '' || this.selectedLocation?.status?.geo?.longitude === '') {
      const gcRequest: GeocoderRequest = {
        address: this.selectedLocation.spec.address,
      };
      new google.maps.Geocoder().geocode({ address: this.selectedLocation.spec.address }, (result, status) => {
        if (result.length > 0) {
          this.latitude = result[0].geometry.location.lat();
          this.longitude = result[0].geometry.location.lng();
        } else {
          this.latitude = 0;
          this.longitude = 0;
        }
        this.populateLocationMarkers();
        this.generateCards();
      });
    } else {
      this.latitude = parseFloat(this.selectedLocation.status.geo.latitude) || 0;
      this.longitude = parseFloat(this.selectedLocation.status.geo.longitude) || 0;
      this.zoom = 10;
      this.populateLocationMarkers();
      this.generateCards();
    }
  }

  populateSelectedLocationChildren(items): void {
    if (!this.selectedLocation) {
      return;
    }
    this.selectedLocation.items = [];
    items.forEach((item) => {
      if (item.spec.base?.parent_link) {
        if (this.selectedLocation.metadata.selfLink === item.spec.base.parent_link) {
          item.text = item.metadata.name;
          this.selectedLocation.items.push(item);
        }
      }
    });
  }

  populateLocationMarkers(): void {
    this.locationMarkers = [];
    const marker: LocationMarker = {
      location: this.selectedLocation.metadata.name,
      address: this.selectedLocation.spec.address || 'Unknown',
      latitude: parseFloat(this.selectedLocation.status.geo.latitude) || 0,
      longitude: parseFloat(this.selectedLocation.status.geo.longitude) || 0,
    };
    this.locationMarkers.push(marker);
    this.selectedLocation.items?.forEach((item) => {
      const itemMarker: LocationMarker = {
        location: item.metadata.name,
        address: item.spec.address,
        latitude: parseFloat(item.status.geo.latitude),
        longitude: parseFloat(item.status.geo.longitude),
      };
      this.locationMarkers.push(itemMarker);
    });
  }

  populateSearchList(locations): void {
    this.searchList = [];
    locations.forEach((loc) => {
      loc.text = loc.metadata.name;
    });
    this.searchList = locations;
  }

  // TODO: Temporary method for generating cards
  generateCards(): void {
    this.cards = [];
    const card1 = {
      title: 'Tunnel',
      query: '/apis/' + this.selectedLocation.apiGroup + '/v1/tunnelconfigs?wsSelector=includeSubtree=true',
      location: this.selectedLocation.metadata.name,
      upMatching: [
        {
          name: 'status.status',
          value: 'TunnelConnected',
          criteria: UDCriteria.EQUAL,
        },
      ],
      downMatching: [
        {
          name: 'status.status',
          value: 'TunnelConnected',
          criteria: UDCriteria.NOT_EQUAL,
        },
      ],
    };
    const card4 = {
      title: 'Device',
      query: '/apis/' + this.selectedLocation.apiGroup + '/v1/devices?wsSelector=includeSubtree=true',
      location: this.selectedLocation.metadata.name,
    };
    const card2 = {
      title: 'Endpoint',
      query: '/apis/' + this.selectedLocation.apiGroup + '/v1/endpoints?wsSelector=includeSubtree=true',
      upMatching: [
        {
          name: 'status.authStatus',
          value: 'AUTHENTICATED',
          criteria: UDCriteria.EQUAL,
        },
      ],
      downMatching: [
        {
          name: 'status.authStatus',
          value: 'AUTHENTICATED',
          criteria: UDCriteria.NOT_EQUAL,
        },
      ],
      location: this.selectedLocation.metadata.name,
    };
    const card3 = {
      title: 'User',
      query: '/apis/' + this.selectedLocation.apiGroup + '/v1/endpoints?wsSelector=includeSubtree=true',
      upMatching: [
        {
          name: 'status.authStatus',
          criteria: UDCriteria.EQUAL,
          value: 'AUTHENTICATED',
        },
        {
          name: 'status.userName',
          criteria: UDCriteria.NOT_EQUAL,
          value: '',
        },
      ],
      downMatching: [
        {
          name: 'status.authStatus',
          criteria: UDCriteria.NOT_EQUAL,
          value: 'AUTHENTICATED',
        },
        {
          name: 'status.userName',
          criteria: UDCriteria.NOT_EQUAL,
          value: '',
        },
      ],
      location: this.selectedLocation.metadata.name,
    };
    this.cards.push(card1);
    this.cards.push(card4);
    this.cards.push(card2);
    this.cards.push(card3);
  }

  isItemSelected(dataItem: Location): boolean {
    return dataItem?.metadata?.selfLink === this.selectedLocation?.metadata?.selfLink;
  }

  emitLocationChanges(): void {
    this._geolocationService.changesExist$.next();
  }

  geocoderErrorHandler(error): ObservableInput<any> {
    return null;
  }

  public onTabSelect(event: SelectEvent): void {
    this.isTenantTabSelected = event.index === 0;
  }

  openAddTenantOrLocationDialog(): void {
    if (this.isTenantTabSelected) {
      this.openAddTenantDialog();
    } else {
      this.openAddLocationDialog(null);
    }
  }

  openAddTenantDialog(): void {
    LocationWidgetComponent.updatedTenant = true;
    const dialogRef = this._dialogService.open({
      title: 'Add New Tenant',
      content: ManageTenantDialogComponent,
    });

    const addLocationData = dialogRef.content.instance;
    addLocationData.dialogTitle = 'Add New Tenant';
    addLocationData.scopeApiGroup = this.location;
    addLocationData.location = this.fixedLocation;

    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe(async (result) => {
      if (result instanceof DialogCloseResult) {
        return;
      }
      if (result.primary) {
        this._notificationService.openSnackBar('Tenant was added successfully!', 'Dismiss');

        this.isLoading = true;
        setTimeout(async () => {
          await this.loadData();
          await this.onTenantsListChanged();
          this.isLoading = false;
        }, 1500);
      }
    });
  }

  ngOnDestroy(): void {
    this._breadcrumbService.currentPage.next(PageType.DEFAULT);
    this._geolocationService.isSelected.next(false);

    this._unsubscribeAll?.next();
    this._unsubscribeAll?.complete();
    this._fuseSearchBarService.aggregatorStats.next([]);
  }

  async onTenantsListChanged(): Promise<void> {
    this.loadTenants();
    const tenants: GlobalTenants = await this._fuseTenantService.getTenants().toPromise();
    this._fuseTenantService.tenants.next(tenants);
  }

  // Recursed function for found all sub-children
  private foundSubChildren(list, item): any {
    const items = list.filter((children) => children.spec.base.parent_link === item.metadata.selfLink);

    if (items.length === 0) {
      return item;
    }

    return {
      ...item,
      items: items.map((child) => this.foundSubChildren(list, child)),
      text: item.metadata.name,
    };
  }

  private filterData(filter: string): void {
    if (_.isEmpty(filter)) {
      this.tenantsList = this.tenantsListDataSource;
      this.locationList = this.originalLocationList;
      return;
    }
    this.tenantsList = _.filter(
      this.tenantsListDataSource,
      (s) => s.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );
    this.onFilterLocations(filter);
  }

  private initTenant(): void {
    const newTenant = this.tenantsList.find((tenant) => tenant.name === this.tenantName);
    if (!newTenant) {
      return;
    }
    LocationWidgetComponent.tenantName = newTenant.name;
    if (!this.tenantName) {
      this.tenantName = newTenant.name;
      this._router.navigate(['location'], {
        queryParams: {tenant: this.tenantName, loc: this.tenantName},
      });
    }
    this.selectedTenant = newTenant;
    this.home = this.selectedTenant.name;
  }
}
