export interface Location {
  apiVersion?: string;
  kind?: string;
  text?: string;
  metadata?: {
    continue?: string;
    resourceVersion?: string;
    selfLink: string;
    name?: string;
    uid?: string;
  };
  spec?: {
    base?: any;
    description?: string;
    address: string;
    hq?: boolean;
  };
  status?: {
    geo: {
      address: string;
      latitude: string;
      longitude: string;
      timeZoneID: string;
    };
  } ;
  items?: Location[];
  apiGroup?: string;
}

export interface LocationMarker {
  location: string;
  address: string;
  latitude: number;
  longitude: number;
}
