import { NgModule } from '@angular/core';
import { GridsterModule } from 'angular-gridster2';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { WidgetModule } from '../../widget/widget.module';
import { ChartDashboardComponent } from './components/dashboard/dashboard.chart.component';
import { PanelViewHeaderComponent } from './components/view-panel-header/view-panel-header.component';
import { ViewPanelComponent } from './components/view-panel/view-panel.component';

import { ChartPanelRoutingRoutingModule } from './components/dashboard/dashboard.chart-routing.module';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { DeviceSharedModule } from 'app/modules/device-shared.gen'
import { ComponentsModule } from 'app/components/module';
import { ChartDashboardDetailPageComponent } from './components/dashboard-detail/component';
import {DialogsModule} from '@progress/kendo-angular-dialog';
import {TabStripModule} from '@progress/kendo-angular-layout';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {LabelModule} from '@progress/kendo-angular-label';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {ClipboardModule} from '@angular/cdk/clipboard';

const SharedVisualModules = [
	DeviceSharedModule,
  ];

@NgModule({
	declarations: [
		ChartDashboardComponent, 
		ChartDashboardDetailPageComponent,
		PanelViewHeaderComponent, 
		ViewPanelComponent,
	],
	exports: [
		ChartDashboardComponent, 
		RouterModule
	],
	imports: [
		SharedModule, 
		WidgetModule, 
		HttpClientModule, 
		ComponentsModule,
		GridsterModule, 
		ChartPanelRoutingRoutingModule,
        TabStripModule,
        InputsModule,
        LabelModule,
        DropDownsModule,
        ButtonsModule,
        DialogsModule,
        ClipboardModule,
		SharedVisualModules,
		NgMultiSelectDropDownModule.forRoot()
	]
})
export class ChartDashboardModule { }
