import { ViewChild, Input, Output, EventEmitter, Component, OnInit } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
@Component({
	selector: 'app-view-panel-header',
	templateUrl: './view-panel-header.component.html',
	styleUrls: ['./view-panel-header.component.scss']
})
export class PanelViewHeaderComponent implements OnInit {
	@ViewChild(MatMenuTrigger, { static: false })
	trigger: MatMenuTrigger;
	menuToggle: boolean;
	detailView: boolean;
	@Input() widget: any;
	@Input() item: any;
	@Input() isTableWidget = false;

    public searchText = null;

	constructor(private _location: Location) { }

	ngOnInit() { }
	open() {
		this.trigger.openMenu();
	}
}
