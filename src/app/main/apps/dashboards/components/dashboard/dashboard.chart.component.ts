import { Component, OnInit, OnDestroy } from "@angular/core";
import { map, filter, first } from "rxjs/operators";
import { Router, ActivatedRoute } from "@angular/router";
import { GridsterConfig, GridsterItem, GridType } from "angular-gridster2";
import { LayoutService } from "../../../../shared/services";
import { DashboardService } from "../../../../shared/services";
import { TimerService, NotificationService } from "../../../../shared/services";

import {
  BreadcrumbService,
  EventType,
} from "../../../../shared/breadcrumb-bar.service";

import { FuseConfigService } from "@fuse/services/config.service";
import { ConfigService } from "../../../../services/config.service";

import {
  DARK_THEME,
  DEFAULT_THEME,
  Dark_echarts,
  Default_echarts,
} from "../../../../theme";
export enum DashboardType {
  DEFAULT_DASHBOARD = "dashboard",
  NS_DASHBOARD = "nsdashboard",
}
import {
  DialogCloseResult,
  DialogService,
  DialogResult,
} from "@progress/kendo-angular-dialog";
import { Subject, BehaviorSubject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { MatDialog } from "@angular/material/dialog";
// import { DialogService } from "app/main/shared/dialog/dialog.service";
import { AddDialogComponent } from "app/main/shared/add-dialog/add-dialog.component";
import { Dashboard } from "app/main/shared/dashboard.model";
import { WidgetDialogComponent } from "app/main/shared/widget-dialog/widget-dialog.component";
import { ConfirmDialogComponent } from "app/main/shared/confirm-dialog/confirm-dialog.component";
import { KendoDashboardDialog } from "app/main/shared/dialogs/kendo-dashboard-dialog/kendo-dashboard-dialog.component";
import { KendoConfirmDialog } from "app/main/shared/dialogs/kendo-confirm-dialog/kendo-confirm-dialog.component";
import { PageType } from "app/main/shared/breadcrumb-bar.service.gen";
import { FuseLocationService } from 'app/services/location.service';

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.chart.component.html",
  styleUrls: ["./dashboard.chart.component.scss"],
})
export class ChartDashboardComponent implements OnInit, OnDestroy {
  public prevSelectedLayout: any[];
  public dashboardLayouts: any[];
  public queryParams: any = {};
  public dashboardType: DashboardType;
  public timer$: any = new BehaviorSubject(null);
  public refreshTimer$: any = new BehaviorSubject(null);
  public interval$: any = new BehaviorSubject(null);
  public period$: any = new BehaviorSubject(null);
  public panelChart = true;
  public dashboards: Dashboard[];
  public selectedLayout: any;
  public dashboardConfigarations: GridsterItem[] = [];
  private _unsubscribeAll: Subject<any>;

  public options: GridsterConfig;
  ItemType: any;
  get unitHeight() {
    return this.layoutService.unitHeight;
  }

  // get options(): GridsterConfig {
  //     return this.layoutService.options;
  // }

  get layout(): GridsterConfig {
    return this.layoutService.layout;
  }

  constructor(
    public layoutService: LayoutService,
    private dashboardSvc: DashboardService,
    private router: Router,
    private route: ActivatedRoute,
    private _breadcrumbService: BreadcrumbService,
    private _timer: TimerService,
    private _fuseThemeConfigService: FuseConfigService,
    private _configService: ConfigService,
    private _noticeService: NotificationService,
    private _dialogService: DialogService,
    private _dialog: MatDialog,
    private _dashboardService: DashboardService,
    private _locationService: FuseLocationService,
  ) {
    this._unsubscribeAll = new Subject();
    this.options = {
      gridType: GridType.VerticalFixed,
      compactType: "compactUp",
      margin: 30,
      outerMarginBottom: 0,
      outerMarginTop: 0,
      outerMarginLeft: 30,
      outerMarginRight: 18,
      minCols: 96,
      maxCols: 96,
      minRows: 96,
      maxRows: 6144,
      maxItemCols: 96,
      minItemCols: 1,
      maxItemRows: 1000,
      minItemRows: 1,
      maxItemArea: 500000,
      fixedRowHeight: 10,
      minItemArea: 1,
      mobileBreakpoint: 300,
      keepFixedHeightInMobile: false,
      scrollVertical: true,
      compactUp: false,
      draggable: {
        delayStart: 0,
        enabled: true,
        ignoreContentClass: "gridster-item-content",
        ignoreContent: true,
        dragHandleClass: "drag-handler",
        stop: ($event) => {},
      },
      resizable: {
        enabled: true,
      },
      swap: false,
      pushItems: true,
    };
  }

  ngOnInit(): void {
    this._timer.setPanel(false);

    this._breadcrumbService.triggeredEvent
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((event) => {
        switch (event.type) {
          case EventType.DASHBOARD_ADD:
            this.addItem();
            break;

          case EventType.DASHBOARD_SAVE:
            this.saveItem();
            break;

          case EventType.DASHBOARD_CLONE:
            this.cloneItem();
            break;

          case EventType.DASHBOARD_DELETE:
            this.deleteItem();
            break;

          case EventType.DASHBOARD_ADD_LAYOUT:
            this.newWidget();
            break;

          default:
            break;
        }
      });

    this._fuseThemeConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config) => {
        if (config.colorTheme === "theme-blue-gray-dark") {
          this._configService.setSelectedThemeObs({
            echart: Dark_echarts,
            theme: DARK_THEME,
          });
        } else {
          this._configService.setSelectedThemeObs({
            echart: Default_echarts,
            theme: DEFAULT_THEME,
          });
        }
      });

    this._breadcrumbService.layouts
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res) => {
        this.dashboardLayouts = res;
      });
    this.route.params.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
      this._breadcrumbService.currentPage.next(PageType.DASHBOARD);
    });

    this.route.queryParams
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((params) => {
        this.queryParams.layout = params.layout;
        this.queryParams.loc = this._locationService.locationDataAsKeyValue().currentLocation;
        // get dashboard
        this.dashboardSvc
          .getDashboard(this._locationService.locationDataAsKeyValue().currentLocation)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(
            (res: any) => {
              this.dashboardLayouts = [];
              setTimeout(() => {
                const dashboard = res.items;
                this.dashboards = dashboard;
                this._breadcrumbService.item.next(this.dashboards);
                dashboard.map((item) => {
                  this.dashboardLayouts.push({
                    type: item.metadata.name,
                    name: item.metadata.name,
                    uid: item.metadata.uid,
                    layout: item.spec.layout,
                  });
                });
                if (
                  this.queryParams === undefined ||
                  this.queryParams.layout === undefined
                ) {
                  this.router.navigate(["dashboard"], {
                    queryParams: {
                      layout: this.dashboardLayouts[0]?.name,
                    },
                    replaceUrl: true,
                  });
                  this._breadcrumbService.queryParams.next({
                    layout: this.dashboardLayouts[0]?.name,
                  });
                  if (this.layoutService.layout.length === 0) {
                    if (this.dashboardLayouts[0])
                      this.layoutService.layout = JSON.parse(
                        this.dashboardLayouts[0].layout
                      );
                  }
                } else {
                  this.selectedLayout = this.dashboardLayouts.filter(
                    (item) => item.name === this.queryParams.layout
                  );
                  if (
                    this.selectedLayout &&
                    this.selectedLayout.length > 0 &&
                    this.layoutService.layout.length === 0
                  ) {
                    this.layoutService.layout = this.selectedLayout[0].layout
                      ? JSON.parse(this.selectedLayout[0].layout)
                      : [];
                  } else {
                    if (this.layoutService.layout.length === 0) {
                      if (this.dashboardLayouts[0]) {
                        this.layoutService.layout = this.dashboardLayouts[0]
                          .layout
                          ? JSON.parse(this.dashboardLayouts[0].layout)
                          : [];
                        this.router.navigate(["dashboard"], {
                          queryParams: {
                            layout: this.dashboardLayouts[0].name,
                          },
                          replaceUrl: true,
                        });
                        this._breadcrumbService.queryParams.next({
                          layout: this.dashboardLayouts[0].name,
                        });
                      }
                      this.layoutService.layout.forEach((widget) => {
                        widget.resizeEnabled = false;
                        widget.dragEnabled = false;
                      });
                    }
                  }
                }

                this._breadcrumbService.layouts.next(this.dashboardLayouts);
              }, 10);
            },
            () => {
              this.dashboardLayouts = [];
              this._breadcrumbService.layouts.next(this.dashboardLayouts);
              this.dashboards = [];
              this._breadcrumbService.item.next([]);
              this.layoutService.layout = [];
              console.log("Error loading dashboard");
            }
          );

        if (
          (this.queryParams === undefined ||
            this.queryParams.layout === undefined) &&
          this.dashboardLayouts !== undefined &&
          this.dashboardLayouts &&
          this.dashboardLayouts.length > 0
        ) {
          this.router.navigate(["dashboard"], {
            queryParams: {
              layout: this.dashboardLayouts[0].name,
            },
            replaceUrl: true,
          });
          this._breadcrumbService.queryParams.next({
            layout: this.dashboardLayouts[0].name,
          });
          this.layoutService.layout = JSON.parse(
            this.dashboardLayouts[0].layout
          );
        } else {
          if (this.dashboardLayouts && this.dashboardLayouts.length > 0) {
            this.selectedLayout = this.dashboardLayouts.filter(
              (item) => item.name === this.queryParams.layout
            );
            if (this.prevSelectedLayout && this.prevSelectedLayout.length > 0) {
              console.log("preveSelectedLayout---->", this.prevSelectedLayout);
              console.log("selectedLayout---->", this.selectedLayout);
              if (this.selectedLayout[0]) {
                if (
                  this.prevSelectedLayout[0].uid !== this.selectedLayout[0].uid
                ) {
                  this.layoutService.layout = this.selectedLayout[0].layout
                    ? JSON.parse(this.selectedLayout[0].layout)
                    : [];
                } else this.layoutService.layout = [];
              } else {
                this.layoutService.layout = [];
              }
            } else {
              if (this.layoutService.layout.length === 0) {
                this.layoutService.layout = this.selectedLayout[0].layout
                  ? JSON.parse(this.selectedLayout[0].layout)
                  : [];
              } else {
                this.layoutService.layout = JSON.parse(this.selectedLayout[0].layout);
              }
            }
            this.prevSelectedLayout = this.selectedLayout;
          }
          console.log(this.layoutService.layout);
          setTimeout(() => {
            this._breadcrumbService.queryParams.next(params);
          }, 10);
        }
      });

    this.refreshTimer$ = this._timer
      .getRefreshObs()
      .pipe(filter((res) => res !== undefined));
    this.interval$ = this._timer.getIntervalObs().pipe(filter((res) => !!res));

    this.period$ = this._timer.getDateRangeObs().pipe(
      filter((res) => !!res),
      map((res) => this._breadcrumbService.setRange(res))
    );

    // get queries
    this.dashboardSvc.getQueries(this._locationService.locationDataAsKeyValue().currentLocation).pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
      this.dashboardSvc.setQueriesObs(res["items"]);
    });

    // get charts
    this.dashboardSvc
      .getChartPanels(this._locationService.locationDataAsKeyValue().currentLocation)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res) => {
        this.dashboardSvc.setChartsObs(res["items"]);
      });
  }

  ngOnDestroy(): void {
    this._breadcrumbService.currentPage.next(PageType.DEFAULT);
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  public editWidget(widget): void {
    this.layoutService.editItem(widget);
  }

  public removeWidget(id): void {
    this.layoutService.deleteItem(id);
  }

  removeDocumentWidget(id): void {
    const dialogRef = this._dialogService.open({
      title: "Remove Widget",
      content: KendoConfirmDialog,
      width: "420px",
      height: "200px",
    });
    const dialogInstance = dialogRef.content.instance;
    dialogInstance.message = "Are you sure you want to remove widget?";
    dialogInstance.dialog = dialogRef;
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
      if (response instanceof DialogCloseResult || response.text == "No") {
      } else {
        this.layoutService.layout.splice(id, 1);
      }
    });
  }

  public gridsterStateChanged(): void {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
  }

  changeGridsterState(widget?: any): void {
    if (widget) {
      widget.resizeEnabled = !widget.resizeEnabled;
      widget.dragEnabled = !widget.dragEnabled;
    }
    this.gridsterStateChanged();
  }

  setHeightGridster(
    tableHeight,
    targetItem,
    tableHeaderHeight,
    otherItems
  ): void {
    const previousRow = targetItem.rows;
    targetItem.rows = Math.ceil((tableHeight + tableHeaderHeight) / 15);
    this.gridsterStateChanged();
    if (otherItems) {
      otherItems = otherItems.map((item) => {
        if (previousRow < targetItem.rows) {
          if (item.y > targetItem.y) {
            item.y = item.y + (targetItem.rows - previousRow);
          }
        } else {
          if (item.y > targetItem.y) {
            item.y = item.y - (previousRow - targetItem.rows);
          }
        }
        return item;
      });
    }
    this.gridsterStateChanged();
  }

  addItem(): void {
    const dialogRef = this._dialogService.open({
      title: "Add a Dashboard",
      content: KendoDashboardDialog,
      width: "300px",
      height: "200px",
    });
    const dialogInstance = dialogRef.content.instance;
    dialogInstance.name = "";
    dialogInstance.dialog = dialogRef;
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
      if (response instanceof DialogCloseResult || response.text == "Cancel") {
      } else {
        const newItem: Dashboard = {
          apiVersion: `${this._locationService.locationDataAsKeyValue().currentLocation}/v1`,
          kind: "Dashboard",
          metadata: {
            name: dialogInstance.form.value.name,
          },
          spec: { layout: `[]` },
        };
        this._dashboardService
          .addDashboard(newItem, this._locationService.locationDataAsKeyValue().currentLocation)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(
            (dashboard: Dashboard) => {
              this._noticeService.openSnackBar(
                "Dashboard add successfully",
                ""
              );
              const layout = {
                type: dashboard.metadata.name,
                name: dashboard.metadata.name,
                layout: dashboard.spec.layout,
                uid: dashboard.metadata.uid,
              };
              this.dashboards.push(dashboard);
              this.dashboardLayouts.push(layout);
              this._breadcrumbService.layouts.next(this.dashboardLayouts);
              this._breadcrumbService.item.next(this.dashboards);
              this.updateLayout(layout);
              this.getLayoutByQueryParams();
            },
            () => {
              this._noticeService.openSnackBar(`Failed to added dashboard`, "");
            }
          );
      }
    });
  }

  private saveItem(): void {
    const layout = this.layoutService.layout;
    const selectedUserItem = this.dashboards.filter(
      (item) => item.metadata.name === this.selectedLayout[0].type
    )[0];
    selectedUserItem.spec.layout = JSON.stringify(layout);
    this._dashboardService
      .saveDashboard(
        selectedUserItem.metadata.name,
        selectedUserItem,
        this._locationService.locationDataAsKeyValue().currentLocation
      )
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (res: any) => {
          const objIndex = this.dashboards.findIndex(
            (item) => item.metadata.name === this.selectedLayout[0].type
          );
          this.dashboards[objIndex] = res;
          this.dashboardLayouts[objIndex] = {
            type: res.metadata.name,
            name: res.metadata.name,
            layout: res.spec.layout,
          };
          this._breadcrumbService.layouts.next(this.dashboardLayouts);
          this._breadcrumbService.item.next(this.dashboards);
          this.updateLayout(this.selectedLayout[0]);
          this._noticeService.openSnackBar("Dashboard saved successfully", "");
        },
        () => {
          this._noticeService.openSnackBar("Failed to saved dashboard", "");
        }
      );
  }

  cloneItem(): void {
    const dialogRef = this._dialogService.open({
      title: "Add a Dashboard",
      content: KendoDashboardDialog,
      width: "300px",
      height: "200px",
    });
    const dialogInstance = dialogRef.content.instance;
    dialogInstance.name = "Copy of " + this.selectedLayout[0].name;
    dialogInstance.dialog = dialogRef;
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
      if (response instanceof DialogCloseResult || response.text == "Cancel") {
      } else {
        //save dashboard info
        let selectedUserDashboard = this.dashboards.find(
          (item) => item.metadata.name === this.selectedLayout[0].type
        );

        if (selectedUserDashboard) {
          selectedUserDashboard = {
            ...selectedUserDashboard,
            metadata: {
              name: dialogInstance.form.value.name,
            },
            spec: {
              layout: JSON.stringify(this.layoutService.layout),
            },
          };

          this._dashboardService
            .cloneDashboard(selectedUserDashboard, this._locationService.locationDataAsKeyValue().currentLocation)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
              (dashboard: Dashboard) => {
                // Add regexp
                /// [a-z0-9]([-a-z0-9][a-z0-9])?(.[a-z0-9]([-a-z0-9][a-z0-9])?)*
                const layout = {
                  type: dashboard.metadata.name,
                  name: dashboard.metadata.name,
                  layout: dashboard.spec.layout,
                  uid: dashboard.metadata.uid,
                };
                this.dashboardLayouts.push(layout);
                this.dashboards.push(dashboard);
                this._noticeService.openSnackBar(
                  "Dashboard saved successfully",
                  ""
                );
                this._breadcrumbService.layouts.next(this.dashboardLayouts);
                this._breadcrumbService.item.next(this.dashboards);
                this.updateLayout(layout);
              },
              () => {
                this._noticeService.openSnackBar(
                  "Failed to saved dashboard",
                  ""
                );
              }
            );
        }
      }
    });
  }

  deleteItem(): void {
    const dialogRef = this._dialogService.open({
      title: "Confirm Dialog",
      content: KendoConfirmDialog,
      width: "420px",
      height: "200px",
    });
    const dialogInstance = dialogRef.content.instance;
    dialogInstance.message = "Are you sure to delete the current dashboard?";
    dialogInstance.dialog = dialogRef;
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
      if (response instanceof DialogCloseResult || response.text == "No") {
      } else {
        this._dashboardService
          .deleteDashboard(this.selectedLayout[0].name, this._locationService.locationDataAsKeyValue().currentLocation)
          .pipe(first())
          .subscribe(
            () => {
              this.layoutService.layout = [];
              this._noticeService.openSnackBar(
                "Dashboard removed successfully",
                ""
              );
              this._breadcrumbService.item.next(
                this.dashboards.filter(
                  (item) => item.metadata.uid !== this.selectedLayout[0].uid
                )
              );
              this._breadcrumbService.layouts.next(
                this.dashboardLayouts.filter(
                  (item) => item.uid !== this.selectedLayout[0].uid
                )
              );
              this.router.navigate(["dashboard"], {
                queryParams: {
                },
                replaceUrl: true,
              });
            },
            () => {
              this._noticeService.openSnackBar(
                "Failed to removed dashboard",
                ""
              );
            }
          );
      }
    });
  }

  newWidget(): void {
    const dialogRef = this._dialog.open(WidgetDialogComponent, {
      width: "600px",
    });
    dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
      if (res) {
        this.layoutService.addItem(res);
      }
    });
  }

  getLayoutByQueryParams(): void {
    if (this.route.snapshot.queryParams["layout"] && this.dashboardLayouts) {
      const selectedLayout = this.dashboardLayouts.filter(
        (item) => item.name === this.queryParams.layout
      );
      this.selectedLayout = selectedLayout[0];
    }
  }

  updateLayout(layout): void {
    this.router.navigate(["dashboard"], {
      queryParams: {
        layout: layout.name,
      },
      replaceUrl: true,
    });
  }
}
