import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewPanelComponent } from '../view-panel/view-panel.component';
import { ChartDashboardComponent } from './dashboard.chart.component'
import { FuseLocationResolver } from '../../../../../services/location-resolverservice';
import { ReportsDetailPageComponent } from 'app/pages/reports/detail/component';
import { ReportChartViewComponent } from 'app/pages/reports/chart/component';
import { LocationResolver } from 'app/components/location/location.resolver';
const LIST_ROUTE = {
    path: '',
    component: ChartDashboardComponent,
    resolve: { resolvedLocationData: FuseLocationResolver, resolveData: LocationResolver }
}

const VIEW_PANEL = {
    path: ':id',
    component: ViewPanelComponent,
    reolsve: { resolveData: LocationResolver },
    data: {
        parent: LIST_ROUTE,
        breadcrumb: 'detail',
    }
}

const PREVIEW_RUNTIME_CHART = {
    path: ':pageName/:resourceName',
    component: ReportsDetailPageComponent,
    reolsve: { resolveData: LocationResolver },
    data: {
        breadcrumb: 'Details :resourceName',
        parent: LIST_ROUTE,
    },
}



@NgModule({
    imports: [
        RouterModule.forChild([
            LIST_ROUTE,
            VIEW_PANEL,
            PREVIEW_RUNTIME_CHART,
        ])
    ],
    exports: [RouterModule]
})
export class ChartPanelRoutingRoutingModule { }
