import { Input, Output, EventEmitter, Component, OnInit, ViewChild, ViewEncapsulation, OnDestroy } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';

import { Observable, Subject } from 'rxjs';
import { TimerService } from '../../../../shared/services';
import { filter, map, takeUntil } from 'rxjs/operators';

import { BreadcrumbService } from '../../../../shared/breadcrumb-bar.service';
import { ReportMenuService } from '../../../../shared/services/report-menu.service';
import { ItemType } from '../../../../shared/services/report-menu.enum.gen';
import { AddItemDialogComponent } from '../../../../shared/add-item-dialog/add-item-dialog.component';
import { KendoReportItemDialog } from "../../../../shared/dialogs/kendo-report-item-dialog/kendo-report-item-dialog.component";
import {
  DialogCloseResult,
  DialogService,
  DialogResult,
} from "@progress/kendo-angular-dialog";

@Component({
	selector: 'app-panel-header',
	templateUrl: './panel-header.component.html',
	styleUrls: ['./panel-header.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class PanelHeaderComponent implements OnInit, OnDestroy {
	@ViewChild(MatMenuTrigger) private menuTrigger: MatMenuTrigger;

	trigger: MatMenuTrigger;
	menuToggle: boolean;
	detailView: boolean;

	@Input() widget: any;
	@Input() selectedRange: any;
	@Input() refreshTime: any;

	@Output() edit: any = new EventEmitter();
	@Output() view: any = new EventEmitter();
	@Output() panelJson: any = new EventEmitter();
	@Output() remove: any = new EventEmitter();
	@Output() rangeSelection: any = new EventEmitter();
	@Output() refreshSelection: any = new EventEmitter();
	@Output() refreshIntervalSelection: any = new EventEmitter();
	@Output() gridsterStateChanged: any = new EventEmitter();

	public refreshList = [
		{ label: 'Off', value: 'Off' },
		{ label: '5s', value: 5000 },
		{ label: '10s', value: 10000 },
		{ label: '30s', value: 30000 },
		{ label: '1m', value: 60000 },
		{ label: '5m', value: 300000 },
		{ label: '15m', value: 900000 },
		{ label: '30m', value: 1800000 },
		{ label: '1h', value: 3600000 },
		{ label: '2h', value: 7200000 },
		{ label: '1d', value: 86400000 }
	];
	public rangeList = [
		{ label: 'Last 5 minutes', short: '5m', start: 0, end: 0, step: 5 },
		{ label: 'Last 15 minutes', short: '15m', start: 0, end: 0, step: 15 },
		{ label: 'Last 30 minutes', short: '30m', start: 0, end: 0, step: 30 },
		{ label: 'Last 1 hour', short: '1h', start: 0, end: 0, step: 60 },
		{ label: 'Last 3 hours', short: '3h', start: 0, end: 0, step: 180 },
		{ label: 'Last 6 hours', short: '6h', start: 0, end: 0, step: 360 },
		{ label: 'Last 12 hours', short: '12h', start: 0, end: 0, step: 720 },
		{ label: 'Last 24 hours', short: '24h', start: 0, end: 0, step: 1440 },
		{ label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 }
	];
	public refreshToggle = false;
	public timer$: Observable<any>;
	public interval$: Observable<any>;
	public isPanelChart$: Observable<any>;
	public refreshDashboard = false;

	private _unsubscribeAll: Subject<any>;
	
	constructor(
		private _timer: TimerService,
		private _breadcrumbService: BreadcrumbService,
		private _reportMenuService: ReportMenuService,
		private _dialogService: DialogService
	) { }

	ngOnInit() {
		this.isPanelChart$ = this._timer.getPanel();
		this.timer$ = this._timer.getDateRangeObs().pipe(
			filter(res => !!res),
			map(res => this._breadcrumbService.setRange(res)));

		this.interval$ = this._timer.getIntervalObs()
			.pipe(filter(res => !!res))
	}

	open() {
		this.trigger.openMenu();
	}

	updateRange(range: any) {
		this.selectedRange = range.label;
		this.rangeSelection.emit(range);
	}

	onRefresh() {
		this.refreshDashboard = !this.refreshDashboard;
		this.refreshSelection.emit(this.refreshDashboard);
		this.refreshTime = this.refreshList[0].value;
		this.refreshIntervalSelection.emit(this.refreshList[0].label);
	}

	refreshToggleClicked() {
		this.refreshToggle == !this.refreshToggle;
	}

	refreshIntervalSelected(time: any) {
		this.refreshTime = time.label;
		this.refreshIntervalSelection.emit(time.value);
	}

	changeGridsterState(widget: any) {
		widget.resizeEnabled = !widget.resizeEnabled;
		widget.dragEnabled = !widget.dragEnabled;
		this.gridsterStateChanged.emit(widget.resizeEnabled);
	}

	setForDevice() {
		this._reportMenuService.setItemObs(ItemType.CHART, this.widget.id, this.widget);
	}

	onAddToReport(item: any) {
	   	const dialogRef = this._dialogService.open({
      			title: "Show in reports",
      			content: KendoReportItemDialog,
      			width: "300px",
      			height: "200px",
    		});
    		const dialogInstance = dialogRef.content.instance;
    		dialogInstance.dialog = dialogRef;
    		dialogInstance.data = {
      			title: "Show in reports",
      			item: { ...item, reportType: ItemType.CHART },
    		};
    		dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
	  }
	  
	  ngOnDestroy(): void {
		this._unsubscribeAll?.next();
		this._unsubscribeAll?.complete();
	  }
}
