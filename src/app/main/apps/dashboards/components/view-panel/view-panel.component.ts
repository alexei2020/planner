import {Component, OnInit, OnDestroy, ViewChild, ElementRef} from '@angular/core';
import { Widget } from '../../../../widget/interfaces/widget';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable, Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { GridsterConfig, GridType, CompactType, GridsterItem } from 'angular-gridster2';

import { WidgetDialogComponent } from '../../../../shared/widget-dialog/widget-dialog.component';
import { LayoutService } from '../../../../shared/services/layout.service';
import { BreadcrumbService } from '../../../../shared/breadcrumb-bar.service';
import { PageType } from '../../../../shared/breadcrumb-bar.service.gen';
import { TimerService, DashboardService } from 'app/main/shared/services';
import {DialogCloseResult, DialogService} from '@progress/kendo-angular-dialog';
import {JsonDialogComponent} from '../../../../shared/dialogs/json-dialog/json-dialog.component';
import {ManageChartDialogComponent} from '../../../../shared/dialogs/manage-chart-dialog/manage-chart-dialog.component';
import { KendoConfirmDialog } from "../../../../shared/dialogs/kendo-confirm-dialog/kendo-confirm-dialog.component";

@Component({
    selector: 'app-view-panel',
    templateUrl: './view-panel.component.html',
    styleUrls: ['./view-panel.component.scss']
})
export class ViewPanelComponent implements OnInit {
    public widget: Widget;
    public filter: string;
    public resetDataSource: any;
    public timer$: Observable<any>;
    public refreshTimer$: Observable<any>;
    public interval$: Observable<any>;
    public options: GridsterConfig;
    public item1: GridsterItem;
    public item2: GridsterItem;

    public isTableWidget = false;
    public tableSearchText = '';
    public tablePageSize = 10;

    public previousLayout: string;

    timer: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private location: Location,
        private _timer: TimerService,
        // private dialogService: DialogService,
        private layoutService: LayoutService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private _dialog: MatDialog,
        private _breadcrumbService: BreadcrumbService,
        private _dashboard: DashboardService,
        private _dialogService: DialogService,
    ) {
        this._unsubscribeAll = new Subject();
        this.options = {
            gridType: GridType.VerticalFixed,
            compactType: CompactType.None,
            margin: 20,
            outerMargin: true,
            outerMarginLeft: 30,
            outerMarginRight: 18,
            outerMarginTop: 0,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1000,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            minItemArea: 1,
            fixedRowHeight: 10,
            resizable: {
                enabled: false
            },
            draggable: {
                delayStart: 0,
                enabled: false,
                ignoreContentClass: 'gridster-item-content',
                ignoreContent: true,
                dragHandleClass: 'drag-handler',
                stop: ($event) => { }
            }
        };

        this.item1 = { cols: 96, rows: 12, y: 0, x: 0 };
        this.item2 = { cols: 96, rows: 12, y: 0, x: 0 };

        this.layoutService.getSelectedItemObs().pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
            if (res) {
                this.widget = { ...res };
                setTimeout(() => {
                    this.resetDataSource = Math.random();
                }, 2000);
            }
        });

        this.timer$ = this._timer.getDateRangeObs().pipe(filter(res => !!res));

        this.refreshTimer$ = this._timer.getRefreshObs().pipe(filter(res => res !== undefined));

        this.interval$ = this._timer.getIntervalObs().pipe(tap(res => console.log('Test result', res)), filter(res => !!res));
    }

    ngOnInit(): void {
        this.activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
            this.previousLayout = this.activatedRoute.snapshot.queryParams['layout'];
            // this._breadcrumbService.queryParams.next({
            //     previousLayout: this.previousLayout
            // });
            if (!this.widget) {
                const location = this.activatedRoute.snapshot.queryParams['loc'];
                const layoutName = this.activatedRoute.snapshot.queryParams['layout'];
                const chartId = params.id;
                this._dashboard.getDashboard(location)
                    .subscribe((res: any) => {
                        console.log('view panel---->', res);
                        console.log('view layoutname---->', layoutName);
                        const currentLayout = res.items.find(item => item.metadata.name === layoutName);
                        let layout = null;
                        if ( currentLayout && currentLayout.length > 0)
                        {
                            layout = currentLayout[0].spec.layout;
                        }
                        this.widget = !layout ? null : JSON.parse(layout).find(item => item.id === chartId);
                    });
            }
        });
        this._breadcrumbService.currentPage.next(PageType.DASHBOARD_DETAIL);

        this.isTableWidget = this.widget?.chartType === 'table';
    }

    public onFilter(filter: string): void {
        this.filter = filter;
    }

    public jsonWidget(widget: Widget): void {
        this.layoutService.setSelectedChartObs(widget.id);
        const dialogRef = this._dialogService.open({
            title: 'Chart JSON',

            // Show component
            content: JsonDialogComponent,
            width: '80%',
            height: '40%',
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.dialog = dialogRef;
        dialogInstance.widget = this.widget;

        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
            console.log(result);
            if (result instanceof DialogCloseResult) {

            } else {
                // create widget object and emit it
                const widget = result as unknown as Widget;
                this.layoutService.editItem(widget);
            }
        });
    }
    // public viewWidget(): void {
    //     // this.location.back();
    //     console.log("go back clicked--->")
    //     this.router.navigate([ 'dashboard' ]);
    // }

    public removeWidget(widget: Widget): void {
        const dialogRef = this._dialogService.open({
            title: 'Remove Widget',
	    content: KendoConfirmDialog,
            width: '390px',
            height: '200px',
        });
	const dialogInstance = dialogRef.content.instance;
    	dialogInstance.dialog = dialogRef;
    	dialogInstance.message = "Are you sure you want to remove widget?";
        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
            if (res instanceof DialogCloseResult || res.text=="No") {
                return;
            }
            this.layoutService.deleteItem(widget.id);
            this.router.navigate(['dashboard'], {
                queryParamsHandling: "merge"
            });
        });
    }
    public editWidget(widget: Widget): void {
        const dialogRef = this._dialogService.open({
            // title: 'Edit chart options',

            // Show component
            content: ManageChartDialogComponent,

            width: '40%',
            height: '70%',
            preventAction: (ev, dialog) => {
                if (ev instanceof DialogCloseResult || !ev.primary) {
                    return false;
                }
                return !dialog.content.instance.widgetForm.valid;
            }
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.dialog = dialogRef;
        dialogInstance.widget = widget;
	dialogInstance.actionLabel1 = "Cancel";
    	dialogInstance.actionLabel2 = "Update chart";
        dialogInstance.populateForEdit(widget);

        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
            if (result instanceof DialogCloseResult || result.text === 'Cancel') {
                console.log('close');
            } else {
                console.log('dialog result => ', result);
                let widget: Widget = {
                    x: 0, y: 0, rows: 12, cols: 48
                };
                widget = dialogInstance.widget;
                const availableQueries = dialogInstance.availableQueries;
                const queries = dialogInstance.widgetForm.value.queries;
                widget.id = dialogInstance.widgetForm.value.id;
                widget.title = dialogInstance.widgetForm.value.title;
                widget.chartType = dialogInstance.widgetForm.value.type;
                widget.chartVariant = dialogInstance.widgetForm.value.variant;
                widget.chartQueries = [];
                widget.datapointCount = dialogInstance.datapointCount;
                widget.privilege = dialogInstance.widgetForm.value.privilege;
                widget.colorPalette = dialogInstance.widgetForm.value.colorPalette;
                widget.opacity = dialogInstance.widgetForm.value.opacity;

                queries.forEach((query) => {
                    widget.chartQueries.push({color: query.color, showOnAxis: query.showOnAxis, query: availableQueries.find((option) => option.metadata.name === query.query)});
                });

                this.widget = {... widget};
                this.layoutService.editItem(widget);
            }
        });

    }

    public filterChanged(event): void {
        this.filter = event;
    }

    public updateOptions(): void {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    changeGridsterState(widget: any, general = true): void {
        if (general) {
            widget.resizeEnabled = !widget.resizeEnabled;
            widget.dragEnabled = !widget.dragEnabled;
        }
        this.updateOptions();
    }

    setHeightGridster(tableHeight, targetItem, tableHeaderHeight, otherItem): void {
        const previousRow = targetItem.rows;
        targetItem.rows = Math.ceil((tableHeight + tableHeaderHeight) / 28);
        this.updateOptions();
        if (otherItem) {
                if (previousRow < targetItem.rows) {
                    if (otherItem.y > targetItem.y) {
                        otherItem.y = otherItem.y + (targetItem.rows - previousRow);
                    }
                } else {
                    if (otherItem.y > targetItem.y) {
                        otherItem.y = otherItem.y - (previousRow - targetItem.rows);
                    }
                }
        }
        this.updateOptions();
    }

    onPageSizeSelected(pageSize): void {
        console.log(pageSize);
        this.tablePageSize = pageSize.value;
    }

    onSearchTextEntered(searchText): void {
        console.log(searchText);
        this.tableSearchText = searchText.value;
    }

    ngOnDestroy(): void {
		this._unsubscribeAll?.next();
		this._unsubscribeAll?.complete();
	  }
}
