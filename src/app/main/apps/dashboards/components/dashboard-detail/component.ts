
import { Component, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { CompactType, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ItemType } from 'app/main/shared/services/report-menu.enum.gen';

@Component({
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class ChartDashboardDetailPageComponent implements OnInit, OnDestroy {
    public pageName: string;
    public resourceName: string;
    public options: GridsterConfig;
    public resizeEvent: EventEmitter<any> = new EventEmitter<any>();
    public data: any;
    public item1: GridsterItem;
    public item2: GridsterItem;
    public item3: GridsterItem;

    private _unsubscribeAll: Subject<any> = new Subject();

    constructor(
        private activatedRoute: ActivatedRoute
    ) {
        this.options = {
            swapping: false,
            itemResizeCallback: (item) => {
                this.resizeEvent.emit(item);
            },
            gridType: GridType.VerticalFixed,
            compactType: CompactType.None,
            margin: 5,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1300,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            minItemArea: 1,
            fixedRowHeight: 10,
            resizable: {
                enabled: false
            },
            draggable: {
                delayStart: 0,
                enabled: false,
                ignoreContentClass: 'gridster-item-content',
                ignoreContent: true,
                dragHandleClass: 'drag-handler',
                stop: () => { }
            },
        };
    }

    ngOnInit(): void {
        this.activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
            this.resourceName = params['resourceName'];
            this.pageName = params['pageName'];
        });
        this.item1 = { reportType: ItemType.DEVICE_GENERAL_DETAILCARD, id: this.resourceName, cols: 48, rows: 48, y: 0, x: 0 };
        this.item2 = { reportType: ItemType.DEVICE_IP_DETAILCARD, id: this.resourceName, cols: 48, rows: 48, y: 0, x: 48 };
        this.item3 = { cols: 96, rows: 38, y: 0, x: 0 };
    }

    changeGridsterState(widget?: any) {
        if (widget) {
            widget.resizeEnabled = !widget.resizeEnabled;
            widget.dragEnabled = !widget.dragEnabled;
        }
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    onAddToReport(item: any) {
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
