import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { LocationResolver } from 'app/components/location/location.resolver';
import { routesGen } from './apps.module.gen';
import {LayoutResolver} from "../../layout/components/layout.resolver";

const routes = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    {
        path: 'dashboard?layout=top10',
        resolve: {layoutResolve: LayoutResolver}
    },
    {
        path: 'dashboard?layout=dashboard',
        resolve: {layoutResolve: LayoutResolver}
    },
    {
        path: 'dashboard',
        data: {breadcrumb: 'Dashboard'},
        resolve: {resolveData: LocationResolver},
        loadChildren: () =>
            import('./dashboards/dashboard.chart.module').then(
                (m) => m.ChartDashboardModule
            ),
    },
    {
        path: 'reports',
        resolve: {resolveData: LocationResolver},
        data: {breadcrumb: 'Reports'},
        loadChildren: () =>
            import('../../resource/reports/module').then(
                (m) => m.ReportsModule
            ),
    },
    {
        path: 'topology',
        resolve: {resolveData: LocationResolver},
        data: { breadcrumb: 'Topology' },
        loadChildren: () =>
            import('./topology/topology.module').then(
                (m) => m.TopologyModule
            ),
    },
    {
        path: 'inventory',
        resolve: {resolveData: LocationResolver},
        loadChildren: () =>
            import('./inventory/inventory.module').then(
                (m) => m.InventoryModule
            ),
        data: {
            breadcrumb: 'Inventory',
        },
    },
    {
        path: 'location',
        resolve: {resolveData: LocationResolver},
        data: {breadcrumb: 'Tenant & Location'},
        loadChildren: () =>
            import('./location/location.module').then(
                (m) => m.LocationPageModule
            ),

    },
    {
        path: 'room',
        resolve: {resolveData: LocationResolver},
        data: { breadcrumb: 'Room' },
        loadChildren: () =>
            import('../../main/room/room.module').then(
                (m) => m.RoomModule,
            ),
    },
    {
        path: '3d-view',
        resolve: {resolveData: LocationResolver},
        data: { breadcrumb: '3D View' },
        loadChildren: () =>
            import('../../roomplanner/router').then(
                (m) => m.RoomPlannerRouter,
            ),

    },
    {
        path: 'labels',
        resolve: {resolveData: LocationResolver},
        data: { breadcrumb: 'Labels' },
        loadChildren: () =>
            import('../../resource/label/module').then(
                (m) => m.LabelModule,
            )
    },
    {
        path: 'settings',
        resolve: {resolveData: LocationResolver},
        data: { breadcrumb: 'Settings' },
        loadChildren: () =>
            import('./settings/settings.module').then(
                (m) => m.SettingsModule,
            )
    },
    {
        path: "file-manager",
        resolve: {resolveData: LocationResolver},
        data: { breadcrumb: "File Manager" },
        loadChildren: () =>
            import("../../pages/file-manager/module").then((m) => m.FileManagerPageModule),
    },
    {
        path: "schedule",
        resolve: {resolveData: LocationResolver},
        data: { breadcrumb: "Schedule" },
        loadChildren: () =>
            import("../../pages/schedule/module").then((m) => m.SchedulePageModule),
    },
    {
        path: "search-results/:keyword",
        resolve: {resolveData: LocationResolver},
        data: { breadcrumb: "Search Results" },
        loadChildren: () =>
            import("../../pages/search-results/module").then((m) => m.SearchResultsPageModule),
    },
    {
        path: 'notifications',
        loadChildren: () =>
            import('./platform-notification/platform-notification.module').then(
                (m) => m.PlatformNotificationModule
            ),
        data: {
            breadcrumb: 'Notifications',
        },
    },
].concat(routesGen);

@NgModule({
    imports: [RouterModule.forChild(routes), FuseSharedModule],
})
export class AppsModule { }
