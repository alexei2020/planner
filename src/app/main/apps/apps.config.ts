export const appsConfig = {
  room: {
    location: 'ws.io',
    layers: [
      '3ca0ebce-befe-4d10-80bd-e35a3a861fd6--54FCBF8C-041F-4301-94E4-CFC76D647DF1',
      '3ca0ebce-befe-4d10-80bd-e35a3a861fd6--00BF9ABB-A204-491A-80EC-A1A1EC43824D',
      '3ca0ebce-befe-4d10-80bd-e35a3a861fd6--62B86F40-CB7C-4D10-A9B5-E5850ECAC092',
      '3ca0ebce-befe-4d10-80bd-e35a3a861fd6--131A9B2A-C6BC-4279-A2FF-DAC25EFB7114',
      '3ca0ebce-befe-4d10-80bd-e35a3a861fd6--A2F962D1-E34C-4730-ABC4-3A97C99F7C42',
      '3ca0ebce-befe-4d10-80bd-e35a3a861fd6--4C1BB1AE-0162-473C-BDFB-020CF1BEA148',
      '3ca0ebce-befe-4d10-80bd-e35a3a861fd6--0B46B1A0-E038-4337-B3EC-8ECD573296D6',
      '3ca0ebce-befe-4d10-80bd-e35a3a861fd6--F369B785-9690-439B-917F-6FF7A796123F',
      '3ca0ebce-befe-4d10-80bd-e35a3a861fd6--1655BF3B-F975-4D61-BC3D-FAA442BAEFE4',
    ],
  },
};
