import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatformNotificationComponent } from './platform-notification.component';

describe('PlatformNotificationComponent', () => {
  let component: PlatformNotificationComponent;
  let fixture: ComponentFixture<PlatformNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
