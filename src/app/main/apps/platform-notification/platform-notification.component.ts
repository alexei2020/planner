import { Component, ViewChild } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

import { DataBindingDirective, GridDataResult } from '@progress/kendo-angular-grid';
import { Subject } from 'rxjs';

import { PlatformNotificationsService } from 'app/services/platform-notification/platform-notifications.service';
import { takeUntil } from 'rxjs/operators';
import {FuseLocationService} from '../../../services/location.service';


@Component({
    templateUrl: './platform-notification.component.html',
    styleUrls: ['./platform-notification.component.scss'],
})
export class PlatformNotificationComponent {

    @ViewChild(DataBindingDirective) dataBinding: DataBindingDirective;
    public gridView: GridDataResult;

    public clickedRowItem: any;
    private apigroup: any = 'ws.io';

    public stateOfNotification = [
        { label: 'Acknowledged', value: true },
        { label: 'Not acknowledged', value: false },
    ];

    // TODO: improve general icons list
    private icons = [
        { name: 'notification-critical', path: 'assets/icons/notifications/critical.svg' },
        { name: 'notification-debug', path: 'assets/icons/notifications/debug.svg' },
        { name: 'notification-emergency', path: 'assets/icons/notifications/emergency.svg' },
        { name: 'notification-error', path: 'assets/icons/notifications/error.svg' },
        { name: 'notification-info', path: 'assets/icons/notifications/info.svg' },
        { name: 'notification-notice', path: 'assets/icons/notifications/notice.svg' },
        { name: 'notification-warning', path: 'assets/icons/notifications/warning.svg' },
    ];

    private _unsubscribeAll: Subject<any> = new Subject();

    /**
     * Constructor
     */
    constructor(
        private _platformNotificationsService: PlatformNotificationsService,
        private _locationService: FuseLocationService,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer,
    ) {
        this.icons.forEach(icon => {
            iconRegistry.addSvgIcon(icon.name, sanitizer.bypassSecurityTrustResourceUrl(icon.path));
        });
        this.gridView = {
            data: [],
            total: 0
        };
    }

    /**
     * On init
     */
    public ngOnInit(): void {
        this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
        this.gridView = {
            data: [],
            total: 0
        };
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    public removeNotification(notification) {
        console.log(notification);
    }

    public updateNotification(value, notification) {
        console.log(value, notification);
    }

    public getValueObject(state: boolean) {
        return this.stateOfNotification.find(item => item.value === state);
    }
}
