import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from "@angular/forms";

import { FuseWidgetModule } from '@fuse/components';


import { ChartsModule } from '@progress/kendo-angular-charts';
import { ExcelModule, GridModule, PDFModule, BodyModule, SharedModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { GridsterModule } from 'angular-gridster2';

import { PlatformNotificationRoutingModule } from './platform-notification-routing.module';
import { PlatformNotificationComponent } from './platform-notification.component';


@NgModule({
    imports: [
        BodyModule, 
        ChartsModule,
        CommonModule,
        ExcelModule,
        FormsModule,
        FuseWidgetModule,
        GridModule,
        GridsterModule,
        InputsModule,
        DropDownsModule,
        MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatIconModule, 
        PlatformNotificationRoutingModule,
        PDFModule,
        RouterModule,
        SharedModule,
    ],
    declarations: [PlatformNotificationComponent],
})
export class PlatformNotificationModule { }
