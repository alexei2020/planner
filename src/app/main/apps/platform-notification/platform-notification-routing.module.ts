import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { PlatformNotificationComponent } from './platform-notification.component';


export const PLATFORM_NOTIFICATIONS_ROUTE: Route = {
    path: '',
    component: PlatformNotificationComponent,
};


@NgModule({
    imports: [
        RouterModule.forChild([PLATFORM_NOTIFICATIONS_ROUTE]),
    ],
})
export class PlatformNotificationRoutingModule { }
