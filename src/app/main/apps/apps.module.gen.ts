
export const routesGen = [
    {
        path: 'audits',
        data: {breadcrumb: 'Change Logs'},
        loadChildren: () =>
            import('../../resource/audit.gen/module').then(
                (m) => m.AuditModule
            ),

    },
    {
        path: 'endpoints',
        data: {breadcrumb: 'EndPoints'},
        loadChildren: () =>
            import('../../resource/endpoint.gen/module').then(
                (m) => m.EndpointModule
            ),

    },
    {
        path: 'endpointsets',
        data: {breadcrumb: 'Endpoint Sets'},
        loadChildren: () =>
            import('../../resource/endpointset.gen/module').then(
                (m) => m.EndpointsetModule
            ),

    },
    {
        path: 'networktemplates',
        data: {breadcrumb: 'Feature Templates'},
        loadChildren: () =>
            import('../../resource/networktemplate.gen/module').then(
                (m) => m.NetworktemplateModule
            ),

    },
    {
        path: 'platformcomponents',
        data: {breadcrumb: 'Device Platform Units'},
        loadChildren: () =>
            import('../../resource/platformcomponent.gen/module').then(
                (m) => m.PlatformcomponentModule
            ),

    },
    {
        path: 'admitdevices',
        data: {breadcrumb: 'Admitdevices'},
        loadChildren: () =>
            import('../../resource/admitdevice.gen/module').then(
                (m) => m.AdmitdeviceModule
            ),

    },
    {
        path: 'attributesets',
        data: {breadcrumb: 'Attributesets'},
        loadChildren: () =>
            import('../../resource/attributeset.gen/module').then(
                (m) => m.AttributesetModule
            ),

    },
    {
        path: 'devices',
        data: {breadcrumb: 'Devices'},
        loadChildren: () =>
            import('../../resource/device.gen/module').then(
                (m) => m.DeviceModule
            ),

    },
    {
        path: 'deviceattributesets',
        data: {breadcrumb: 'Deviceattributesets'},
        loadChildren: () =>
            import('../../resource/deviceattributeset.gen/module').then(
                (m) => m.DeviceattributesetModule
            ),

    },
    {
        path: 'deviceuis',
        data: {breadcrumb: 'Deviceuis'},
        loadChildren: () =>
            import('../../resource/deviceui.gen/module').then(
                (m) => m.DeviceuiModule
            ),

    },
    {
        path: 'endpointauxs',
        data: {breadcrumb: 'Endpointauxs'},
        loadChildren: () =>
            import('../../resource/endpointaux.gen/module').then(
                (m) => m.EndpointauxModule
            ),

    },
    {
        path: 'files',
        data: {breadcrumb: 'Files'},
        loadChildren: () =>
            import('../../resource/file.gen/module').then(
                (m) => m.FileModule
            ),

    },
    {
        path: 'floorspaces',
        data: {breadcrumb: 'Floorspaces'},
        loadChildren: () =>
            import('../../resource/floorspace.gen/module').then(
                (m) => m.FloorspaceModule
            ),

    },
    {
        path: 'ifinterfaces',
        data: {breadcrumb: 'Ifinterfaces'},
        loadChildren: () =>
            import('../../resource/ifinterface.gen/module').then(
                (m) => m.IfinterfaceModule
            ),

    },
    {
        path: 'locations',
        data: {breadcrumb: 'Locations'},
        loadChildren: () =>
            import('../../resource/location.gen/module').then(
                (m) => m.LocationModule
            ),

    },
    {
        path: 'radios',
        data: {breadcrumb: 'Radios'},
        loadChildren: () =>
            import('../../resource/radio.gen/module').then(
                (m) => m.RadioModule
            ),

    },
    {
        path: 'radioinfos',
        data: {breadcrumb: 'Radioinfos'},
        loadChildren: () =>
            import('../../resource/radioinfo.gen/module').then(
                (m) => m.RadioinfoModule
            ),

    },
    {
        path: 'snapshottasks',
        data: {breadcrumb: 'Snapshottasks'},
        loadChildren: () =>
            import('../../resource/snapshottask.gen/module').then(
                (m) => m.SnapshottaskModule
            ),

    },
    {
        path: 'ssids',
        data: {breadcrumb: 'Ssids'},
        loadChildren: () =>
            import('../../resource/ssid.gen/module').then(
                (m) => m.SsidModule
            ),

    },
    {
        path: 'subinterfaces',
        data: {breadcrumb: 'Subinterfaces'},
        loadChildren: () =>
            import('../../resource/subinterface.gen/module').then(
                (m) => m.SubinterfaceModule
            ),

    },
    {
        path: 'tenants',
        data: {breadcrumb: 'Tenants'},
        loadChildren: () =>
            import('../../resource/tenant.gen/module').then(
                (m) => m.TenantModule
            ),

    },
    {
        path: 'walljacks',
        data: {breadcrumb: 'Walljacks'},
        loadChildren: () =>
            import('../../resource/walljack.gen/module').then(
                (m) => m.WalljackModule
            ),

    },
];
