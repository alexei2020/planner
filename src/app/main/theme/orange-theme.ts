export const ORANGE_THEME = {
    name: 'orange',
    colors: [
        '#f45a38',
        '#ffb802',
        '#ffde2e',
        '#ff896e',
        '#c62300',
        '#b58200',
    ],
    colorOpacity: 1
};
