export const VIBRANT_THEME = {
    name: 'vibrant',
    colors: [
        '#ed4b4a',
        '#3acf49',
        '#0b92fb',
        '#ff7e36',
        '#c06dff',
        '#ffc043',
    ],
    colorOpacity: 1
};
