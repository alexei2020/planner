export const TRADITIONAL1_THEME = {
    name: 'traditional1',
    colors: [
        '#CCDC3B',
        '#005770',
        '#EE524E',
        '#00ADC7',
        '#FFC903',
        '#5482B7',
    ],
    colorOpacity: 1
};
export const TRADITIONAL2_THEME = {
    name: 'traditional2',
    colors: [
        '#BF6CFE',
        '#EC4A49',
        '#3BCF49',
        '#0A92FB',
        '#FD7D37',
        '#0182C1',
    ],
    colorOpacity: 1
};
export const TRADITIONAL3_THEME = {
    name: 'traditional3',
    colors: [
        '#C80B7C',
        '#E5C703',
        '#7BC906',
        '#00ADD0',
        '#6F3271',
        '#0182C1',
    ],
    colorOpacity: 1
};
