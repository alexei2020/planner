const palette = {
	primary: '#3366ff',
	success: '#00d68f',
	info: '#0095ff',
	warning: '#ffaa00',
	danger: '#ff3d71'
};

export const DEFAULT_THEME = {
	name: 'default',
	variables: {
		fontMain: 'Open Sans, sans-serif',
		fontSecondary: 'Raleway, sans-serif',

		bg: '#ffffff',
		bg2: '#f7f9fc',
		bg3: '#edf1f7',
		bg4: '#e4e9f2',

		border: '#ffffff',
		border2: '#f7f9fc',
		border3: '#edf1f7',
		border4: '#e4e9f2',
		border5: '#c5cee0',

		fg: '#8f9bb3',
		fgHeading: '#1a2138',
		fgText: '#1a2138',
		fgHighlight: palette.primary,
		layoutBg: '#f7f9fc',
		separator: '#edf1f7',

		primary: palette.primary,
		success: palette.success,
		info: palette.info,
		warning: palette.warning,
		danger: palette.danger,

		primaryLight: '#598bff',
		successLight: '#2ce69b',
		infoLight: '#42aaff',
		warningLight: '#ffc94d',
		dangerLight: '#ff708d'
	},
    colors: [
        '#ffb74d',
        '#a1887e',
        '#65b5f5',
        '#b9dcd7',
        '#ffddb8',
        '#e0bae6',
        '#fcbacc',
        '#c8e6c8',
        '#ffe0b1',
        '#d7cbc8'
    ]
};
export const FUSE_THEME = {
    name: 'fuse',
    variables: {
        tomato: '#f44336',
        crimson: '#e91e63',
        darkorchid: '#9c27b0',
        slateblue: '#673ab7',
        deepskyblue: '#00a9f4',
        darkslateblue: '#3f51b5',
        dodgerblue: '#2196f3',
        darkcyan: '#009688',
        yellowgreen: '#8bc34a',
        greenyellow: '#cddc39',
        gold: '#ffeb3b',
        darkorange: '#ff9800',
        dimgray: '#795548',
        darkgray: '#9e9e9e',
        mediumseagreen: '#4caf50',
        tomato300: '#ef534f',
        slateblue300: '#5c6cbf',
        darkseagreen300: '#81c683',
        khaki: '#fff176',
        sandybrown: '#ffb74d',
        rosybrown: '#a1887e',
        cornflowerblue: '#65b5f5',
        powderblue: '#b9dcd7',
        peachpuff: '#ffddb8',
        thistle: '#e0bae6',
        pink: '#fcbacc',
        lightgreen: '#c8e6c8',
        navajowhite: '#ffe0b1',
        lightgray: '#d7cbc8'
    },
    colors: [
        '#f44336',
        '#e91e63',
        '#9c27b0',
        '#673ab7',
        '#00a9f4',
        '#3f51b5',
        '#2196f3',
        '#009688',
        '#8bc34a',
        '#cddc39',
        '#ffeb3b',
        '#ff9800',
        '#795548',
        '#9e9e9e',
        '#4caf50',
        '#ef534f',
        '#5c6cbf',
        '#81c683',
        '#fff176',
        '#ffb74d',
        '#a1887e',
        '#65b5f5',
        '#b9dcd7',
        '#ffddb8',
        '#e0bae6',
        '#fcbacc',
        '#c8e6c8',
        '#ffe0b1',
        '#d7cbc8'
    ]
};
const baseThemeVariables = DEFAULT_THEME.variables;
export const Default_echarts = {
	bg: baseThemeVariables.bg,
	textColor: baseThemeVariables.fgText,
	axisLineColor: baseThemeVariables.fgText,
	splitLineColor: baseThemeVariables.separator,
	itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
	tooltipBackgroundColor: baseThemeVariables.primary,
	areaOpacity: '0.7'
};
