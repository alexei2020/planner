export const MODERN1_THEME = {
    name: 'modern1',
    colors: [
        '#a6adba',
        '#f3d99a',
        '#7fddc3',
        '#f5b0c0',
        '#9faafd',
        '#4c72fd',
    ],
    colorOpacity: 1
};

export const MODERN2_THEME = {
    name: 'modern2',
    colors: [
        '#e8914f',
        '#8ecae8',
        '#47777f',
        '#989ca0',
        '#c2b1a9',
        '#a6adba',
    ],
    colorOpacity: 1
};
