export const BLUE_THEME = {
    name: 'blue',
    colors: [
        '#027bd4',
        '#49b3ff',
        '#4adeff',
        '#7ba2ff',
        '#eeab02',
        '#ffd602',
    ],
    colorOpacity: 1
};
