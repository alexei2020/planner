export const PASTEL_THEME = {
    name: 'pastel',
    colors: [
        '#ef5323',
        '#002b49',
        '#73cee1',
        '#ec008b',
        '#cc24df',
        '#ffae34',
    ],
    colorOpacity: 1
};
