import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Subject } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';

import { v4 as uuid } from 'uuid';

import { DashboardService } from '../services/dashboard.service';
import { ChartType } from '../../widget/interfaces/widget';
import { ConfigService } from 'app/main/services/config.service';
import {Query} from '../../widget/interfaces/query';
import {FuseLocationService} from '../../../services/location.service';

@Component({
	selector: 'app-widget-dialog',
	templateUrl: './widget-dialog.component.html',
	styleUrls: ['./widget-dialog.component.scss']
})
export class WidgetDialogComponent implements OnInit {
	public data: any;
	public formSubmitted: boolean;
	public queryOption: Array<Query> = [];
	public chartsOption: Array<ChartType> = [];
	public privilageList = [];
	public queryTypes: Array<Query> = [];
	public selectedTypes = [];
	public maxCount = 0;
	public colors: any;
	public isActiveForm = true;
	public widgetForm: FormGroup = this._fb.group({
		id: [uuid()],
		title: ['', Validators.required],
		query: this._fb.array([]),
		type: ['', Validators.required],
		privilege: ['', Validators.required]
	});

	private destroy$: Subject<boolean> = new Subject<boolean>();

	constructor(
		private _fb: FormBuilder,
		public dialogRef: MatDialogRef<WidgetDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public formData,
		private dashboardSvc: DashboardService,
		private configSvc: ConfigService,
    private _locationService: FuseLocationService,
  ) {
		this.data = formData ? JSON.stringify({
			title: formData.title,
			type: formData.type,
			privilege: formData.privilege,
			query: formData.query,
			chartOptions: formData.chartOptions
		}, null, 2) : '';
		const location = this._locationService.locationDataAsKeyValue().currentLocation;

		this.dashboardSvc.getChartNamespaces(location)
			.pipe(takeUntil(this.destroy$))
			.subscribe((res) => {
				if (res && res['items'].length > 0) {
					this.privilageList = res['items'];
				} else {
					this.privilageList = JSON.parse(localStorage.getItem('privilage'));
				}
			});

		this.dashboardSvc.getQueriesObs()
			.pipe(takeUntil(this.destroy$))
			.subscribe((res) => {
				if (res && res.length > 0) {
					this.queryOption = res;
				} else {
					this.queryOption = JSON.parse(localStorage.getItem('queries'));
				}
			});

		this.dashboardSvc.getChartPanels(location)
			.pipe(takeUntil(this.destroy$))
			.subscribe((res) => {
				if (res && res['items'].length > 0) {
					this.chartsOption = res['items'];
				} else {
					this.chartsOption = JSON.parse(localStorage.getItem('charts'));
				}
			});

		this.widgetForm.get('type').valueChanges
			.pipe(takeUntil(this.destroy$))
			.subscribe((val: any) => {
				let chart = this.chartsOption.find((chart) => chart.metadata.name === val);
				this.maxCount = chart && chart.spec.max_queries ? chart.spec.max_queries : 1;
				this.clearItem();
			});

		this.widgetForm.get('privilege').valueChanges
			.pipe(
				takeUntil(this.destroy$),
				switchMap((privilage: string) => this.dashboardSvc.getChartQueryList(location, privilage))
			)
			.subscribe((res: any) => {
				this.queryTypes = res['items'];
			});


		//get chart styles
		this.configSvc.getSelectedThemeObs()
			.pipe(takeUntil(this.destroy$))
			.subscribe((config: any) => {
				this.colors = config.theme.variables;
			});

		this.widgetForm.get('query').valueChanges
			.pipe(takeUntil(this.destroy$))
			.subscribe((queries: any) => {
				this.selectedTypes = queries;
			});
	}

	isSelected(query: string) {
		return !!this.selectedTypes.find(item => item.query === query);
	}

	clearItem() {
		const control = <FormArray>this.widgetForm.controls['query'];
		while (control.length) {
			control.removeAt(0);
		}
	}

	addItem() {
		const control = <FormArray>this.widgetForm.controls['query'];
		const addrCtrl = this.initAction();
		control.push(addrCtrl);
		this.maxCount--;
	}

	removeItem(i: number) {
		const control = <FormArray>this.widgetForm.controls['query'];
		control.removeAt(i);
		this.maxCount++;
	}

	initAction(): FormGroup {
		return this._fb.group({
			query: ['', Validators.required]
		});
	}

	ngOnInit() {
		if (this.formData) {
			let data = {
				id: this.formData.id,
				title: this.formData.title,
				type: this.formData.type.metadata.name,
				privilege: this.formData.privilege || 'endpoint'
			};
			this.widgetForm.patchValue(data);
			this.maxCount = this.formData.type.spec.max_queries ? this.formData.type.spec.max_queries : 1;
			this.transformItem(this.formData.query);
		}
	}

	transformItem(query) {
		const control = <FormArray>this.widgetForm.controls['query'];
		while (control.length) {
			control.removeAt(0);
		}
		query.forEach((term) => {
			control.push(
				this._fb.group({
					query: [term.metadata.name]
				})
			);
			this.maxCount--;
		});
	}

	submitForm() {
		this.formSubmitted = false;
		if (this.isActiveForm) {
			if (this.widgetForm.valid) {
				let realValue = this.widgetForm.value;

				realValue['type'] = this.chartsOption.find((chart) => chart.metadata.name === realValue.type);
				let queries = this.widgetForm.value.query.map((item) => item.query);
				let queriesObject = [];
				queries.forEach((query) => {
					queriesObject.push(this.queryOption.find((option) => option.metadata.name === query));
				});
				realValue['query'] = queriesObject;
				if (this.formData) {
					this.dialogRef.close({ ...this.formData, ...realValue });
				} else {
					this.dialogRef.close({ ...realValue, ...{ x: 0, y: 0, rows: 20, cols: 48 } });
				}
			} else {
				this.formSubmitted = true;
			}
		} else if (this.data) {
			let realValue = JSON.parse(this.data);
			if (this.formData) {
				this.dialogRef.close({ ...this.formData, ...realValue });
			} else {
				this.dialogRef.close({ ...realValue, ...{ id: uuid(), x: 0, y: 0, rows: 20, cols: 48 } });
			}
		}

	}

	tabChanged(tabChangeEvent: MatTabChangeEvent): void {
		this.isActiveForm = tabChangeEvent.index === 0 ? true : false;
	}

	// Destroy object in memory (if not will be memory leeks)
	ngOnDestroy() {
		this.destroy$.next(true);
		this.destroy$.unsubscribe();
	}
}
