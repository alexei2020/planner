interface StringMap {
	[key: string]: string;
}
export interface Metadata {
	name?: string;
	namespace?: string;
	labels?: StringMap;
	annotations?: StringMap;
	creationTimestamp?: string;
	uid?: string;
	generation?: number;
	selfLink?: string;
	resourceVersion?: string;
}

// ChartQuery
export interface ChartQuery {
	spec?: ChartQuerySpec;
	metadata?: Metadata;
	status?: ChartQueryStatus;
}

export interface ChartQueryStatus {
}

export interface ChartQuerySpec {
	query_info: QueryInfo;
}

export interface QueryInfo {
	title?: string;
	port?: number;
	base_url?: string;
	prev_url?: string;
	total_url?: string;
	prev_data_url?: string;
	chart_url?: string;
	total_label?: string;
	params?: string;
	query_category?: string;
	x_axis_label?: string;
	y_axis_label?: string;
	all_data_url?: string;
	filtered_data_url?: string;
	units?: string;
}

// Dashboard
export interface Dashboard {
	apiVersion: string;
	kind: string;
	spec?: DashboardSpec;
	metadata?: Metadata;
	status?: DashboardStatus;
}

export interface DashboardStatus {
}

export interface DashboardSpec {
	layout: string;
}

// ChartPanel
export interface ChartPanel {
	spec: ChartPanelSpec;
	metadata: Metadata;
	status: ChartPanelStatus;
}

export interface ChartPanelStatus {
}

export interface ChartPanelSpec {
	title?: string;
	category?: string;
	max_queries?: number;
	panel_datapoint_count?: number;
	detail_view_datapoint_count?: number;
	chart_info?: ChartInfo;
}

export interface ChartInfo {
	type: string;
}

