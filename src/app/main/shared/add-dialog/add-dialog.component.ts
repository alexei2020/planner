import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 * @title Dialog Overview
 */
@Component({
  selector: 'dashboard-add-dialog',
  templateUrl: 'add-dialog.component.html',
  styleUrls: ['add-dialog.component.scss'],
})
export class AddDialogComponent implements OnInit {
  public form: FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<AddDialogComponent>) {
    this.form = this.formBuilder.group({
      name: [data && data.name ? data.name : '', [Validators.required, Validators.pattern('^[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*$')]]
    });
  }

  ngOnInit() { }

  closeDialog() {
    this.dialogRef.close(false);
  }

}
