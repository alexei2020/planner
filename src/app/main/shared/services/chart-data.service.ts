import * as _moment from 'moment';
import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BarChartSeriesOptions} from '../../widget/interfaces/barChartOptions';
import {Query} from '../../widget/interfaces/query';
import {takeUntil} from 'rxjs/operators';
import {forkJoin, Observable, Subject} from 'rxjs';
import {Widget} from '../../widget';
import {BreadcrumbService} from '../breadcrumb-bar.service';

const moment = _moment;

export interface ChartDataObject {
    widget: Widget;
    data: any;
}

@Injectable({
  providedIn: 'root'
})
export class ChartDataService {

    private baseUrl = environment.promeUrl;
    private objName: string;
    private namespace: string;

    private renderUnit: string;

    // Subjects
    private unsubscribe$: Subject<void> = new Subject<void>();
    public dataFetched$: Subject<ChartDataObject> = new Subject<ChartDataObject>();
    public seriesFormatted$: Subject<ChartDataObject> = new Subject<ChartDataObject>();
    public noDataFetched$: Subject<Widget> = new Subject<Widget>();

    constructor(
        private http: HttpClient,
        private _breadcrumbService: BreadcrumbService
    ) {
        this._breadcrumbService.currentPageInfo
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((cpi) => {
                this.objName = cpi?.name;
                this.namespace = cpi?.namespace;
            });
    }

    // Get data URL from base and query
    private getDataUrl(query: string): string {
        return this.baseUrl + query;
    }

    // Get observable for HTTP GET-ting the data URL
    getData(query): Observable<any> {
        return this.http.get(this.getDataUrl(query));
    }

    // Format query with parameters
    getQueryForUrl(url: string, duration, startTime, endTime, step, apigroup): string {
        url = this.replace(url, '+', '%20');
        url = this.replace(url, '+', '%20');
        url = this.replace(url, '{{DURATION}}', `${duration}`);
        url = this.replace(url, '{{DURATION}}', `${duration}`);
        url = this.replace(url, '{{DURATION}}', `${duration}`);
        url = this.replace(url, '{{startTime}}', `${startTime}`);
        url = this.replace(url, '{{endTime}}', `${endTime}`);
        url = this.replace(url, '{{step}}', `${step}`);
        url = this.replace(url, '{{STARTTIME}}', `${startTime}`);
        url = this.replace(url, '{{ENDTIME}}', `${endTime}`);
        url = this.replace(url, '{{STEP}}', `${step}`);
        url = this.replace(url, '{{APIGROUP}}', `${apigroup}`);
        url = this.replace(url, '{{OBJNAME}}', `${this.objName}`);
        url = this.replace(url, '{{OBJNAME}}', `${this.objName}`);
        url = this.replace(url, '{{NAMESPACE}}', `${this.namespace}`);
        return url;
    }

    // Get data from all queries for widget
    getDataFromQueries(widget: Widget, queries: Query[], queryParameters): void {
        const results = [];
        queries.forEach((query: Query, index) => {
            const dataUrl = query.spec.query_info.base_url;
            const unitType = query.spec.query_info.units;
            const queryTitle = query.spec.query_info.title;
            const queryCategory = query.spec.query_info.query_category;
            const queryUrl = this.getQueryForUrl(dataUrl, queryParameters.duration,
                queryParameters.startTime, queryParameters.endTime, queryParameters.step,
                queryParameters.apigroup);

            this.getData(queryUrl)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    (result: any) => {
                        if (result.data.result.length === 0) {
                            this.noDataFetched$.next(widget);
                            return;
                        }
                        result.data.queryTitle = queryTitle;
                        result.data.queryNo = index + 1;
                        result.data.unitType = unitType;
                        result.data.queryCategory = queryCategory;
                        results.push(result);
                        if (results.length === queries.length) {
                            this.dataFetched$.next({widget, data: results});
                        }
                    },
                    (error: any) => {
                            this.noDataFetched$.next(widget);
                            console.log("ERROOORRRRRR", error);
                            return;
                    }
                );
        });
    }

    getSummaryChartDataFromQueries(widget: Widget, queries: Query[], queryParameters): void {
        const dataUrl = queries[0].spec.query_info.base_url;
        const prevUrlString = queries[0].spec.query_info.prev_url;
        const totalUrlString = queries[0].spec.query_info.total_url;
        const chartUrlString = queries[0].spec.query_info.chart_url;
        const queryUrl = this.getQueryForUrl(dataUrl, queryParameters.duration, queryParameters.startTime, queryParameters.endTime, queryParameters.step, queryParameters.apigroup);
        const prevUrl = this.getQueryForUrl(prevUrlString, queryParameters.duration,
            queryParameters.startTime, queryParameters.endTime, queryParameters.step, queryParameters.apigroup);
        const totalUrl = this.getQueryForUrl(totalUrlString, queryParameters.duration,
            queryParameters.startTime, queryParameters.endTime, queryParameters.step, queryParameters.apigroup);
        const chartUrl = this.getQueryForUrl(chartUrlString, queryParameters.duration,
            queryParameters.startTime, queryParameters.endTime, queryParameters.step, queryParameters.apigroup);

        forkJoin(
            this.getData(queryUrl),
            this.getData(prevUrl),
            this.getData(totalUrl),
            this.getData(chartUrl)
        ).pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (result: any) => {
                    let noData = true;
                    if (result.length !== 0) {
                        noData = result[0].data.result.length === 0;
                    }
                    if (noData) {
                        this.noDataFetched$.next(widget);
                        return;
                    }
                    this.dataFetched$.next({widget, data: result});
                },
                (error: any) => {
                    this.noDataFetched$.next(widget);
                    console.log("ERROOORRRRRR SUmmarty", error);
                    return;
                });
    }
    getSummarySeriesFromResults(widget: Widget, result): void {
        let data;
        let percentage;
        result[0].data['name'] = widget.chartQueries[0].query.spec.query_info.title;
        data = result[0].data;
        percentage = {
            value: result[2].data.result[0]?.value[1] || 0,
            label: widget.chartQueries[0].query.spec.query_info.total_label
        };
        const currentData = result[0].data.result;
        const previousData = result[1].data.result;
        const totalData = result[2].data.result;
        let realValue = 0;
        let valueChange = 0;

        if (currentData.length > 0 && previousData.length > 0 && totalData.length > 0 ) {
            realValue = parseInt(currentData[0].value[1], 0) / parseInt(totalData[0].value[1], 0) * 100;
            valueChange = Math.round(currentData[0].value[1]) - Math.round(previousData[0].value[1]);
        }

        const change = valueChange;
        let changeRate;
        if (valueChange < 0) {
            changeRate = Math.abs(valueChange);
        } else {
            changeRate = valueChange;
        }
        const seriesData = result[3].data;
        this.seriesFormatted$.next({widget, data: {data, percentage, realValue, change, changeRate, seriesData}});
    }


    // Get formatted series from query results
    getSeriesFromResults(widget: Widget, results): void {
        const series = [];
        // console.log('WIDGET ---> ', widget);
        results.forEach((result, idx, array) => {
            switch (result.data.queryCategory) {
                case 'time-series':
                    series.push(this.getTimeSeries(widget, result, result.data.unitType));
                    break;
                case 'summary':
                    series.push(this.getSummarySeries(widget, result, result.data.unitType));
                    break;
                case 'discrete-data':
                default:
                    series.push(this.getDescreteSeries(widget, result, result.data.unitType));
            }
            if (idx === array.length - 1) {
                this.seriesFormatted$.next({widget, data: series});
            }
        });

    }

    // Get summary type series
    private getSummarySeries(widget: Widget, res, unitType): ChartDataObject {
        const results = res.data.result;
        const dateList = [];
        const dataArray = [];
        results.forEach((result) => {
            let name: string;
            const metric = result.metric;
            for (const key in metric) {
                name = metric[key];
            }
            dateList.push(name);
            dataArray.push({
                data: [ {
                    value: Math.round(result.value[1]),
                    name
                } ],
                detail: {
                    formatter: (value) => {
                        return this.getFormattedValue(value, unitType);
                    },
                    fontSize: 15
                }
            });
        });
        const chartOptions = new BarChartSeriesOptions();
        chartOptions.series = dataArray;
        return {widget, data: chartOptions};
    }

    // Get time series
    private getTimeSeries(widget: Widget, res, unitType): ChartDataObject {
        let dateList = [];
        let seriesData;
        const series = [];
        const results = res.data.result;
        const name = res.data.queryTitle;


        // console.log('TimeSeries', results);
        results.forEach((result, i) => {
            if (i === 0) {
                dateList = result.values.map((date) => date[0]);
            }
            seriesData = result.values.map((date) => parseFloat(date[1]));

            series.push({
                type: 'bar',
                name,
                animationDelay: (idx) => idx * 10 + (res.data.queryNo + 100),
                data: seriesData
            });
        });

        // AB round label to unitSize
        // let interval: number;
        // let splitNumber = 8;
        // let maxValue = seriesData.reduce((a, b) => {
        //     return Math.max(a, b);
        // });

        // let maxValueIn5s = Math.ceil(maxValue / 5) * 5;
        // let maxFormattedValue = this.getFormattedBytes(maxValue, 0);
        // let wantedSize = maxFormattedValue.size;
        // interval = this.roundTo(Math.ceil(maxValue / splitNumber), maxFormattedValue.value.toString().length * 5);
        // interval = this.convertBytesToSize(interval, 2, wantedSize).value;
        //
        // interval = Math.ceil(interval / 5) * 5;
        // interval = this.convertSizeToBytes(interval, wantedSize);
        //
        //
        // let maxValueInSize = this.convertBytesToSize(maxValue, 0, wantedSize);
        // let maxValueInSizeNum = Math.ceil(maxValueInSize.value / 5) * 5;
        // maxValueInSizeNum = this.convertSizeToBytes(maxValueInSizeNum, wantedSize);

        const chartOptions = new BarChartSeriesOptions();
        chartOptions.legend = {
            data: name
        };
        chartOptions.xAxis = {
            data: dateList,
            axisLabel: {
                color: '#949BB9',
                fontFamily: 'IBM Plex Sans',
                fontSize: 10,
                fontWeight: 'normal',
                formatter: (time) => this.timeFormatter(time)
            },
            axisPointer: {
                label: {
                    formatter: (axisValue) =>
                        this.timeFormatter(axisValue.value),
                },
            }
        };
        chartOptions.yAxis = {
            axisPointer: {
                label: {
                    formatter: (value) => {
                        console.log('Plain value TIME ', value);
                        console.log('Unit type TIME', unitType);
                        const formattedValue = this.getFormattedValue(value.value, unitType);
                        return formattedValue;
                    },
                },
            },
            axisLabel: {
                show: true,
                color: '#949BB9',
                fontFamily: 'IBM Plex Sans',
                fontSize: 10,
                fontWeight: 'normal',
                formatter: (value) => {
                    // console.log('Plain value TIME ', value);
                    // console.log('Unit type TIME', unitType);
                    // const formattedValue = this.getFormattedValue(value, unitType);
                    // return formattedValue;
                    // if (unitType === 'bytes') {
                    //     const formattedValue = this.convertBytesToSize(value, 1, wantedSize);
                    //     return formattedValue.value + ' ' + formattedValue.size;
                    // } else {
                        return this.getFormattedValue(value, unitType);
                    // }
                }
            },
        };

        chartOptions.series = series;
        chartOptions.animationEasing = 'elasticOut';
        chartOptions.animationDelayUpdate = (idx) => idx * 5;

        return {widget, data: chartOptions};
    }

    // Get descrete series
    private getDescreteSeries(widget: Widget, results, unitType): ChartDataObject {
        const key = Object.keys(results.data.result[0].metric)[0];
        const displayLabel = widget.chartQueries[0].query.spec.query_info.display_label || 'objname';

        // sort in descending order
        let sortedResults = results.data.result.sort((a, b) => {
            return b.value[1] - a.value[1];
        });

        const xAxisList: Array<any> = [];
        const dataArray: Array<any> = [];
        let otherCategoryValue = 0;
        // let interval: number;
        // let splitNumber = 8;
        // let maxValue = sortedResults[0].value[1];
        // // let maxValueIn5s = Math.ceil(maxValue / 5) * 5;
        // let maxFormattedValue = this.getFormattedBytes(maxValue, 0);
        // let wantedSize = maxFormattedValue.size;
        // interval = this.roundTo(Math.ceil(maxValue / splitNumber), maxFormattedValue.value.toString().length * 5);
        // interval = this.convertBytesToSize(interval, 2, wantedSize).value;
        //
        // interval = Math.ceil(interval / 5) * 5;
        // interval = this.convertSizeToBytes(interval, wantedSize);
        //
        //
        // let maxValueInSize = this.convertBytesToSize(maxValue, 0, wantedSize);
        // let maxValueInSizeNum = Math.ceil(maxValueInSize.value / 5) * 5;
        // maxValueInSizeNum = this.convertSizeToBytes(maxValueInSizeNum, wantedSize);



        sortedResults.forEach(
            (result, index, resultsArray) => {
            let name: string;
            const metric = result.metric;
            if (JSON.stringify(metric) === JSON.stringify({})) {
                name = 'Undefined';
            }
            name = metric[displayLabel];

            // Used earlier to get last parameter from metric object
            // for (const key in metric) {
            //     name = metric[key];
            // }
            // name = metric[displayLabel];
            if (index > 4) {
                otherCategoryValue += parseFloat(result.value[1]);
            } else {
                xAxisList.push(name);
                dataArray.push({
                    name,
                    type: 'bar',
                    label: {
                        show: true,
                        position: 'bottom',
                        color: '#949BB9',
                        fontFamily: 'IBM Plex Sans',
                        fontSize: 10,
                        fontWeight: 'normal',
                        formatter: (value) => {
                            return value.seriesName;
                        },
                    },
                    data: [
                        result.value[1]
                    ],
                });
            }
            if (index === resultsArray.length - 1) {
               if (otherCategoryValue > 0) {
                   xAxisList.push('Other');
                   dataArray.push({
                       name: 'Other',
                       type: 'bar',
                       label: {
                           show: true,
                           position: 'bottom',
                           formatter: 'Other',
                           color: '#949BB9',
                           fontFamily: 'IBM Plex Sans',
                           fontSize: 10,
                           fontWeight: 'normal'
                       },
                       data: [
                           otherCategoryValue
                       ]
                   });
                }
           }
        });

        const chartOptions = new BarChartSeriesOptions();

        chartOptions.legend = {
            data: xAxisList
        };
        chartOptions.xAxis = [
            {
                type: 'category',
                axisLabel: {
                    color: '#949BB9',
                    fontFamily: 'IBM Plex Sans',
                    fontSize: 10,
                    fontWeight: 'normal'
                }
            }
        ];
        chartOptions.yAxis = [
            {
                type: 'value',
                axisLabel: {
                    show: true,
                    color: '#949BB9',
                    fontFamily: 'IBM Plex Sans',
                    fontSize: 10,
                    fontWeight: 'normal',
                    formatter: (value) => {
                        // console.log('DESCRETE raw value -> ', value);
                        // console.log('Unit type', unitType);
                        // const formattedValue = this.getFormattedValue(value, unitType);
                        // return formattedValue;
                        // return value + ' ' + wantedSize;
                            return this.getFormattedValue(value, unitType);
                    }
                }
            }
        ];
        chartOptions.series = dataArray;

        // console.log('Data array', dataArray);
        return {widget, data: chartOptions};

    }

    getTableChartData(widget: Widget, queryParameters): void {
        // console.log('WIDG ', widget);
        const query: Query = widget.chartQueries[0].query;
        const columns = query.spec.query_info.columns;
        const baseUrl = query.spec.query_info.base_url;
        // console.log('QUERY ', query);
        const queryUrl = this.getQueryForUrl(baseUrl, queryParameters.duration, queryParameters.startTime, queryParameters.endTime, queryParameters.step, queryParameters.apigroup);

        this.getData(queryUrl)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((result) => {
                const tableData = [];
                const results = result.data.result;
                let valueUnitType;
                let renderType;

                columns.forEach((col) => {
                    if (col.data_type === 'value') {
                        valueUnitType = col.units;
                        renderType = col.render_type;
                    }
                });
                results.forEach((d) => {
                    const row = d.metric;
                    row.value = this.getFormattedValue(d.value[1], valueUnitType);
                    if (renderType === 'percentage' || renderType === 'progress-percentage') {
                        row.value = Math.round(parseInt(row.value, 0));
                    }
                    tableData.push(row);
                });
                this.dataFetched$.next({widget, data: tableData});
            });
    }

    // Utility method to replace in string
    private replace(value, matchingString, replacerString): string {
        return value.replace(matchingString, replacerString);
    }
    // Utility method to format time
    private timeFormatter(time: any): string {
        return `${moment.unix(time).format('M/D/Y')}\n${moment.unix(time).format('h:mm a')}`;
    }

    // spec.query_info.units - bytes, msec
    // Utility method to format value based on unitType
    getFormattedValue(value, unitType, decimals = 0): string {
        let formattedValue;
        switch (unitType) {
            case 'bytes':
                formattedValue = this.formatBytes(value, decimals);
                break;
            case 'msec':
                formattedValue = this.formatMsec(value);
                break;
            case 'percent':
                formattedValue = this.formatPercent(value);
                break;
            default:
                formattedValue = value;
                break;
        }
        return formattedValue;
    }

    // Utility method to format percent
    private formatPercent(percent): string {
        return percent + '%';
    }

    // Utility method to format bytes
    private formatBytes(bytes, decimals = 2): string {
        // console.log('BYTES -> ', bytes, decimals);
        if (bytes === 0)  {
            return '0';
        }

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));
        const fl = parseFloat((bytes / Math.pow(k, i)).toFixed(dm));
        // const fn = this.roundTo(Math.ceil(fl), 1);
        // console.log('FLOAT -> ', fl);
        // console.log('ROUNDED -> ', fn);
        // console.log('FORMATTED -> ', fl + sizes[i]);
        return fl + ' ' + sizes[i];
    }

    // Utility method to format bytes
    private getFormattedBytes(bytes, decimals = 2): {value: number, size: string} {
        // console.log('BYTES -> ', bytes, decimals);
        if (bytes === 0)  {
            return {value: 0, size: ''};
        }

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));
        const fl = parseFloat((bytes / Math.pow(k, i)).toFixed(dm));

        return {
            value: fl,
            size: sizes[i]
        };
    }

    private convertSizeToBytes(value, size): number {
        const k = 1024;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = sizes.findIndex((s) => {
            return s === size;
        });
        const bytes = parseInt((value * Math.pow(k,i)).toFixed(0), 0);

        return bytes;
    }

    private convertBytesToSize(bytes, decimals = 2, size = 'MB'): {value: number, size: string} {
        if (bytes === 0)  {
            return {value: 0, size};
        }
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = sizes.findIndex((s) => {
           return s === size;
        });

        // const i = Math.floor(Math.log(bytes) / Math.log(k));
        const fl = parseFloat((bytes / Math.pow(k, i)).toFixed(dm));

        return {
            value: fl,
            size: sizes[i]
        };
    }

    // Utility for rounding
    private roundTo(n, d): number {
        return Math.floor((n + d - 1) / d) * d;
    }

    // Utility method to format msecs
    private formatMsec(milisec): string {

        if (milisec < 1000) {
            return milisec + ' msec';
        }

        const seconds = (milisec / 1000);

        const minutes = (milisec / (1000 * 60));

        const hours = (milisec / (1000 * 60 * 60));

        const days = (milisec / (1000 * 60 * 60 * 24));
        if (seconds < 60) {
            return seconds + ' sec';
        } else if (minutes < 60) {
            return minutes + ' min';
        } else if (hours < 24) {
            return hours + ' hrs';
        } else {
            return days + ' days';
        }
    }

    // Utility method to format value label
    formatValueLabel(series, splitNumber, unitType): any {
      let options: any;
      const allSeries = [];
      let maxValue: any;
      let maxValueFormatted: any;
      let maxValueInSize: any;
      let wantedSize: any;
      let interval: any;
      // Find max value from all series
      series.forEach((s) => {
          s.data.forEach((d) => {
              allSeries.push(d);
          });
      });
      maxValue = allSeries.reduce((a, b) => {
          return Math.max(a, b);
      });

      // Check unitType
      if (!unitType) {
        maxValueInSize = Math.ceil(maxValue / 5) * 5;
        if (maxValueInSize <= 5) {
          interval = 1;
        } else {
          interval = Math.ceil(maxValueInSize / splitNumber);
        }
      } else if (unitType === 'bytes') {
        // Find unitSize from max value
        maxValueFormatted = this.getFormattedBytes(maxValue, 0);
        wantedSize = maxValueFormatted.size;
        maxValueInSize = this.convertBytesToSize(maxValue, 0, wantedSize);
        maxValueInSize = Math.ceil(maxValueInSize.value / 5) * 5;
        // If max value 5 or less interval should be 1
        if (maxValueInSize <= 5) {
          interval = 1;
        } else {
          interval = Math.ceil(maxValue / splitNumber);
          interval = this.convertBytesToSize(interval, 2, wantedSize).value;
          interval = Math.ceil(interval / 5) * 5;
        }
        maxValueInSize = this.convertSizeToBytes(maxValueInSize, wantedSize);

        // Calculate the interval based on max value and splitNumber
        interval = this.convertSizeToBytes(interval, wantedSize);
      }

      // Set number of splits, interval, min and max
      options = {
          splitNumber: splitNumber,
          interval: interval,
          min: 0,
          max: maxValueInSize,
          wantedSize
      };

      return options;
    }
}
