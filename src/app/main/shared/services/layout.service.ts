import { Injectable } from '@angular/core';
import {
    CompactType,
    GridsterConfig,
    GridsterItem,
    GridType,
    GridsterItemComponent
} from 'angular-gridster2';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Widget } from '../../widget/interfaces/widget';
import {BarChartLayoutOptions} from '../../widget/interfaces/barChartOptions';
import {DARK_THEME, DEFAULT_THEME} from '../../theme';
import {FUSE_THEME} from '../../theme/default-theme';
import {ORANGE_THEME} from '../../theme/orange-theme';
import {BLUE_THEME} from '../../theme/blue-theme';
import {VIBRANT_THEME} from '../../theme/vibrant-theme';
import {PASTEL_THEME} from '../../theme/pastel-theme';
import {TRADITIONAL1_THEME} from '../../theme/traditional-theme';
import {TRADITIONAL2_THEME} from '../../theme/traditional-theme';
import {TRADITIONAL3_THEME} from '../../theme/traditional-theme';
import {MODERN1_THEME, MODERN2_THEME} from '../../theme/modern-theme';
@Injectable({
    providedIn: 'root'
})

export class LayoutService {
    unitHeight;
    public selectedChart$ = new Subject();
    selectedItem: BehaviorSubject<Widget | null> = new BehaviorSubject(null);
    public edited$ = new Subject();
    public itemResized$: Subject<GridsterItemComponent> = new Subject<GridsterItemComponent>();

    public chartOptions: any;
    public layout: Array<Widget> = [];
    public options: GridsterConfig = {
        gridType: GridType.VerticalFixed,
        compactType: CompactType.None,
        margin: 20,
        outerMargin: true,
        outerMarginLeft: 0,
        outerMarginRight: 0,
        outerMarginTop: 0,
        resizable: {
            enabled: false
        },
        draggable: {
          delayStart: 0,
          enabled: false,
          ignoreContentClass: 'gridster-item-content',
          ignoreContent: true,
          dragHandleClass: 'drag-handler',
          stop: ($event) => { }
        },
        minCols: 96,
        maxCols: 96,
        minRows: 96,
        maxRows: 1000,
        maxItemCols: 96,
        minItemCols: 1,
        maxItemRows: 1000,
        minItemRows: 1,
        maxItemArea: 500000,
        fixedRowHeight: 10,
        itemResizeCallback: (gridsterItem: GridsterItem, gridsterItemComponent: GridsterItemComponent) => {
            this.itemResized$.next(gridsterItemComponent);
        }
    };

    constructor() {
        // this.layout = this.items;
    }


    getSelectedChartObs(): Observable<any> {
        return this.selectedChart$.asObservable();
    }

    setSelectedChartObs(item: any): void {
        this.selectedChart$.next(item);
    }
    getEditedObs(): Observable<any> {
        return this.edited$.asObservable();
    }
    getSelectedItemObs(): Observable<Widget | null> {
        return this.selectedItem.asObservable();
    }

    setSelectedItemObs(selectedItem: any): void {
        this.selectedItem.next(selectedItem);
    }
    setEditedObs(range: any): void {
        this.edited$.next(range);
    }
    public itemResize(): void {
        // if (itemComponent.gridster.curRowHeight > 1) {
        // 	this.unitHeight = itemComponent.gridster.curRowHeight;
        // }
        // itemComponent.gridster.curRowHeight += (item.cols * 100 - item.rows) / 10000;
    }

    changedOptions(): void {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }
    deleteItem(id: string): void {
        const item = this.layout.find((d) => d.id === id);
        this.layout.splice(this.layout.indexOf(item), 1);
    }
    addItem(widget: Widget): void {
        this.layout.push(widget);
    }
    editItem(widget: Widget): void {
        for (let i = 0; i < this.layout.length; i++) {
            if (this.layout[i].id == widget.id) {
                this.layout[i] = widget;
                this.setSelectedItemObs({ ...widget });
                this.setEditedObs({ ...widget });
            }
        }
    }

    getChartLayout(widget: Widget): BarChartLayoutOptions {

        // const options = widget['chartOptions'];
        const chartOptions = new BarChartLayoutOptions();
        let colors = DEFAULT_THEME.colors;

        switch (widget.colorPalette) {
            case 'dark':
                colors = DARK_THEME.colors;
                break;
            case 'fuse':
                colors = FUSE_THEME.colors;
                break;
            case 'orange':
                colors = ORANGE_THEME.colors;
                break;
            case 'blue':
                colors = BLUE_THEME.colors;
                break;
            case 'vibrant':
                colors = VIBRANT_THEME.colors;
                break;
            case 'pastel':
                colors = PASTEL_THEME.colors;
                break;
            case 'traditional1':
                colors = TRADITIONAL1_THEME.colors;
                break;
            case 'traditional2':
                colors = TRADITIONAL2_THEME.colors;
                break;
            case 'traditional3':
                colors = TRADITIONAL3_THEME.colors;
                break;
            case 'modern1':
                colors = MODERN1_THEME.colors;
                break;
            case 'modern2':
                colors = MODERN2_THEME.colors;
                break;
        }

        chartOptions.color = [...colors];
        // apply alpha to colors
        chartOptions.color.forEach((color, index, sourceArr) => {
            sourceArr[index] =  this.addAlpha(color, widget.opacity);
        });

        chartOptions.legend = {
            type: 'scroll',
            left: 'center',
            textStyle: {
                color: '#949BB9',
                fontFamily: 'IBM Plex Sans',
                fontSize: 10,
                fontWeight: 'normal'
            },
            itemGap: 20
        };
        chartOptions.tooltip = {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow',
            },
        };
        chartOptions.grid = {
            top: '40px',
            bottom: '15px',
            left: '0',
            right: '10px',
            containLabel: true,
            // show: true,
            // backgroundColor: '#ff9800'
        };

        return chartOptions;


        // TODO
        // this.widget['chartOptions'] = {
        //     color: this.options.color,
        //     legend: {
        //         textStyle: this.options.legend.textStyle,
        //     },
        //     tooltip: this.options.tooltip,
        //     grid: this.options.grid,
        //     xAxis: this.options.xAxis,
        //     yAxis: [
        //         {
        //             nameTextStyle: this.options.yAxis[0].nameTextStyle,
        //             type: this.options.yAxis[0].type,
        //             axisLine: this.options.yAxis[0].axisLine,
        //             splitLine: this.options.yAxis[0].splitLine,
        //             axisLabel: {
        //                 show: this.options.yAxis[0].axisLabel.show,
        //                 textStyle: this.options.yAxis[0].axisLabel.textStyle,
        //             },
        //         },
        //     ],
        // };
    }
    addAlpha(color: string, opacity: number): string {
        // // remove #hash
        // color = color.substring(1);
        // // coerce values so ti is between 0 and 1.
        // const _opacity = Math.round(Math.min(Math.max(opacity || 1, 0), 1) * 255);
        // return '#' + color + _opacity.toString(16).toUpperCase();
        const colorHex = this.hexToRgb(color);
        console.log(colorHex);
        return 'rgba(' + colorHex.r + ', ' + colorHex.g + ', ' + colorHex.b + ', ' + opacity + ')';
    }

    removeAlpha(color: string): string {
        // remove last two chars - alpha
        // return color.substr(0, 7);
        const rgbaArr = color.split(',');
        return rgbaArr[0] + ', ' + rgbaArr[1] + ', ' + rgbaArr[2] + ', 1)';
    }

    hexToRgb(hex): {r: number, g: number, b: number} {
        const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }
}
