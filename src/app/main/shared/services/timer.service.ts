import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
@Injectable({
	providedIn: 'root'
})
export class TimerService {
	public refresh$ = new BehaviorSubject(null);
	public dateRange$ = new BehaviorSubject(null);
	public timer$ = new BehaviorSubject(null);
	public panel$ = new BehaviorSubject(null);
	constructor() { }

	getDateRangeObs(): Observable<any> {
		return this.dateRange$;
	}

	setDateRangeObs(range: any) {
		this.dateRange$.next(range);
	}

	getRefreshObs(): Observable<any> {
		return this.refresh$.asObservable();
	}

	setRefreshObs(refresh: any) {
		this.refresh$.next(refresh);
	}

	getIntervalObs(): Observable<any> {
		return this.timer$.asObservable();
	}

	setIntervalObs(interval: any) {
		this.timer$.next(interval);
	}

	getPanel(): Observable<any> {
		return this.panel$.asObservable();
	}

	setPanel(isPanel) {
		this.panel$.next(isPanel);
	}
}
