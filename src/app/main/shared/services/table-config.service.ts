import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {Widget} from '../../widget';

@Injectable({
  providedIn: 'root'
})
export class TableConfigService {

    // Subjects
    public pageSizeChanged$: Subject<any> = new Subject<any>();
    public searchTextChanged$: Subject<any> = new Subject<any>();

  constructor() {

  }

  changePageSize(widget: Widget, pageSize: number): void {
      this.pageSizeChanged$.next({widget, pageSize});
  }
  changeSearchText(widget: Widget, searchText: string): void {
      this.searchTextChanged$.next({widget, searchText});
  }
}
