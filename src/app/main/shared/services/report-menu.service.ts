import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ItemType} from './report-menu.enum.gen';
import {environment} from '../../../../environments/environment';


export enum ItemTitle {
    DEVICE_LIST = 'Devices',
    DEVICE_ITEM_IP = 'IP',
    DEVICE_ITEM_DETAIL = 'General',
    DEVICE_USER_LIST = 'Device Users List',
    DEVICE_USER_RULE_LIST = 'Device User Roles List',
    DEVICE_USER_ITEM = 'Device User Rule Details',
}


@Injectable({
    providedIn: 'root'
})
export class ReportMenuService {

    public reportItems: BehaviorSubject<any[]> = new BehaviorSubject([]);
    public charts: BehaviorSubject<any[]> = new BehaviorSubject([]);
    public reports: any;

    private selectedItems: { reportItem: ItemType, id: any, value: any }[] = [];
    apiUrl: string = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    getSelectedItemObs(): Observable<{ reportItem: ItemType, id: any, value: any }[]> {
        return this.reportItems.asObservable();
    }

    setItemObs(reportItem: ItemType, id?: any, value?) {
        if (id) {
            if (!this.selectedItems.some(element => element.reportItem === reportItem && id === element.id)) {
                this.selectedItems.push({reportItem: reportItem, id, value});
            } else {
                this.selectedItems = this.selectedItems.filter(element => element.reportItem !== reportItem && id !== element.id);
            }
        } else {
            if (!this.selectedItems.some(element => element.reportItem === reportItem)) {
                this.selectedItems.push({reportItem: reportItem, id, value});
            } else {
                this.selectedItems = this.selectedItems.filter(element => element.reportItem !== reportItem);
            }
        }
        this.reportItems.next(this.selectedItems);
    }

    itemIsChecked(reportItem: ItemType, id?: any) {
        return this.selectedItems.some(element => element.reportItem === reportItem && id === element.id);
    }

    addReports(payload, scope: string) {
        return this.http.post(`${this.apiUrl}/apis/${scope}/v1/reports`, payload);
    }

    saveReport(id, payload, scope: string) {
        return this.http.put(`${this.apiUrl}/apis/${scope}/v1/reports/${id}`, payload);
    }

    cloneReport(payload, scope: string) {
        return this.http.post(`${this.apiUrl}/apis/${scope}/v1/reports`, payload);
    }

    deleteReport(name: string, scope: string) {
        return this.http.delete(`${this.apiUrl}/apis/${scope}/v1/reports/${name}`);
    }

    getReports(scope: string) {
        return this.http.get(`${this.apiUrl}/apis/${scope}/v1/reports`);
    }
}
