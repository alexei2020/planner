import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Widget } from '../../widget/interfaces/widget';

@Injectable({
	providedIn: 'root'
})
export class DashboardService {
	apiUrl: string = environment.apiUrl;
	selectedItem: BehaviorSubject<Widget | null> = new BehaviorSubject(null);
	queries: BehaviorSubject<any[]> = new BehaviorSubject([]);
	charts: BehaviorSubject<any[]> = new BehaviorSubject([]);

	constructor(private http: HttpClient) {}

	getSelectedItemObs(): Observable<Widget | null> {
		return this.selectedItem.asObservable();
	}

	setSelectedItemObs(selectedItem: any) {
		this.selectedItem.next(selectedItem);
	}

	getQueriesObs(): Observable<any[]> {
		return this.queries.asObservable();
	}

	setQueriesObs(queries: any) {
		this.queries.next(queries);
	}

	getChartsObs(): Observable<any[]> {
		return this.charts.asObservable();
	}

	setChartsObs(queries: any) {
		this.charts.next(queries);
	}

	getQueries(scope: string) {
		return this.http.get(`${this.apiUrl}/apis/${scope}/v1/chartquerys`);
	}

	saveDashboard(id, payload, scope: string) {
		return this.http.put(`${this.apiUrl}/apis/${scope}/v1/dashboards/${id}`, payload);
	}

	addDashboard(payload, scope: string) {
		return this.http.post(`${this.apiUrl}/apis/${scope}/v1/dashboards`, payload);
	}

	cloneDashboard(payload, scope: string) {
		return this.http.post(`${this.apiUrl}/apis/${scope}/v1/dashboards`, payload);
	}

	getDashboardByName(name: string, scope: string) {
		return this.http.get(`${this.apiUrl}/apis/${scope}/v1/dashboards/${name}`);
	}
	
	getDashboard(scope: string) {
		return this.http.get(`${this.apiUrl}/apis/${scope}/v1/dashboards`);
	}

	deleteDashboard(name: string, scope: string) {
		return this.http.delete(`${this.apiUrl}/apis/${scope}/v1/dashboards/${name}`);
	}

	getChartPanels(scope: string) {
		return this.http.get(`${this.apiUrl}/apis/${scope}/v1/chartpanels`);
	}

	getChartNamespaces(scope: string) {
		return this.http.get(`${this.apiUrl}/api/v1/namespaces`);
	}

	getChartQueryList(scope: string, privilege: any) {
		return this.http.get(`${this.apiUrl}/apis/${scope}/v1/namespaces/${privilege}/chartquerys`);
	}
}
