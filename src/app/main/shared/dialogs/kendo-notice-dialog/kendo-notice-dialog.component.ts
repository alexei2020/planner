import { Component, OnInit, Input } from "@angular/core";
import { DialogContentBase, DialogRef } from "@progress/kendo-angular-dialog";

@Component({
  selector: "kendo-notice-dialog",
  templateUrl: "./kendo-notice-dialog.component.html",
  styleUrls: ["./kendo-notice-dialog.component.scss"],
})
export class KendoNoticeDialog extends DialogContentBase implements OnInit {
  @Input() public message: string;

  public dialog: DialogRef;
  data: any;

  constructor() {
    super(null);
  }

  ngOnInit(): void {

  }
  onOkBtnClicked(){
    this.dialog.close({ text: 'OK' });
  }
}
