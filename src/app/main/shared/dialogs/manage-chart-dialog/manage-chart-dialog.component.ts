import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CHART_PALETTES, CHART_TYPES, ChartDataModel} from '../../chart-data.model';
import { v4 as uuid } from 'uuid';
import {DashboardService} from '../../services';
import {switchMap, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {Query} from '../../../widget/interfaces/query';
import {DARK_THEME, DEFAULT_THEME} from '../../../theme';
import {FUSE_THEME} from '../../../theme/default-theme';
import {Widget} from '../../../widget';
import {DialogContentBase, DialogRef} from '@progress/kendo-angular-dialog';
import {ORANGE_THEME} from '../../../theme/orange-theme';
import {BLUE_THEME} from '../../../theme/blue-theme';
import {VIBRANT_THEME} from '../../../theme/vibrant-theme';
import {PASTEL_THEME} from '../../../theme/pastel-theme';
import {TRADITIONAL1_THEME} from '../../../theme/traditional-theme';
import {TRADITIONAL2_THEME} from '../../../theme/traditional-theme';
import {TRADITIONAL3_THEME} from '../../../theme/traditional-theme';
import {MODERN1_THEME, MODERN2_THEME} from '../../../theme/modern-theme';
import {FuseLocationService} from '../../../../services/location.service';

@Component({
    selector: 'app-manage-chart-dialog',
    templateUrl: './manage-chart-dialog.component.html',
    styleUrls: ['./manage-chart-dialog.component.scss']
})
export class ManageChartDialogComponent extends DialogContentBase implements OnInit, OnDestroy {
    @Input() public actionLabel1: string;
    @Input() public actionLabel2: string;

    public isEdit = false;
    public dialog: DialogRef;
    public data: any;
    public widgetForm: FormGroup = this._fb.group({
        id: [uuid()],
        title: ['', Validators.required],
        type: ['bar', Validators.required],
        variant: [''],
        privilege: ['', Validators.required],
        opacity: [1, Validators.required],
        colorPalette: ['', Validators.required],
        queries: this._fb.array([])
    });
    public formSubmitted = false;
    public availableQueries: Query[] = [];
    public chartTypes = [];
    public chartVariants = [];
    public privilegeList: { text: string, value: string }[] = [];
    public queriesList: { text: string, value: string }[] = [];
    public colorPalettes = CHART_PALETTES;
    public queryColors = [];
    public selectedChartType: any;
    public selectedChartVariation: any;
    public maxQueries = 0;
    public dataPointCount = 50;
    public widget: Widget;
    public queries: FormArray;
    public chartIcon: string;

    @Input() public chartData: ChartDataModel;

    private unsubscribe$: Subject<boolean> = new Subject<boolean>();

    constructor(
      private _fb: FormBuilder,
      private _locationService: FuseLocationService,
      private dashboardService: DashboardService
  ) {
      super(null);
    }

    ngOnInit(): void {
        const location = this._locationService.locationDataAsKeyValue().currentLocation;
        this.dataPointCount = 50;
        if (this.widget) {
            this.data = JSON.stringify(this.widget, null, 2);
        }

        this.resetQueries();
        // Populate chartTypes
        this.populateChartTypes();
        // Select Bar chart as default
        if (!this.isEdit) {
            this.onSelectChartType('bar');
        }

        // Get privileges
        this.dashboardService.getChartNamespaces(location)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((res) => {
                this.privilegeList = [];
                let privileges: any[] = [];
                if (res && res['items'].length > 0) {
                    privileges = res['items'];
                } else {
                    privileges = JSON.parse(localStorage.getItem('privilage'));
                }
                privileges.forEach((privilege) => {
                    this.privilegeList.push({
                        text: privilege.metadata.name,
                        value: privilege.metadata.name
                    });
                });
            });

        this.widgetForm.get('privilege').valueChanges
            .pipe(
                takeUntil(this.unsubscribe$),
                switchMap((privilege: string) =>
                {
                    // Remove already added queries if any
                    this.resetFormQueries();
                    return this.dashboardService.getChartQueryList(location, privilege);
                })
            )
            .subscribe((res: any) => {
                // Clear queriesList
                this.queriesList = [];
                // Setup new queries list
                this.availableQueries = res['items'] || [];
                this.availableQueries.forEach((aq) => {
                    this.queriesList.push({
                        text: aq.spec.query_info.title,
                        value: aq.metadata.name
                    });
                });
                // Refresh queries list based on chartType
                this.filterQueriesByChartType();
            });
    }

    // Generate chartVariants for selected chart type
    onSelectChartType(chartTypeEvent: any): void {
        this.widgetForm.controls['variant'].reset();
        this.selectedChartVariation = null;
        if (!chartTypeEvent) {
            this.chartVariants = [];
            return;
        }

        this.populateChartVariations(chartTypeEvent);
        // Select default chart variant
        this.onSelectChartVariation(this.chartVariants[0].value || null);
        this.selectChartIcon(chartTypeEvent);
        this.filterQueriesByChartType();
        this.resetQueries();
        this.addQueryItem();

        // create queries FormArray by selected chartType
        // this.queriesFG = this.getQueriesFG();

        console.log('Selected chart type ', this.selectedChartType, this.maxQueries);
    }

    // On select chart variation
    onSelectChartVariation(chartVariationEvent: any): void {
        if (!chartVariationEvent) {
            this.maxQueries = 1;
            return;
        }
        this.widgetForm.patchValue({
            variant: chartVariationEvent
        });
        this.setChartVariation(chartVariationEvent);
        this.setupMaxQueries();
        console.log('Selected chart variation ', this.selectedChartVariation, this.maxQueries);
    }

    // Populate queryColors based on selected colorPalette
    onSelectColorPalette(colorPaletteEvent: any): void {
        this.queryColors = [];
        this.populateQueryColors(colorPaletteEvent);
    }

    private filterQueriesByChartType(): void {
        this.queriesList = [];
        // if no selectedChartType just exit
        if (!this.selectedChartType) {
            return;
        }
        let queryCategory = 'discrete-data';

        switch (this.selectedChartType.value) {
            case 'bar':
            case 'pie':
                queryCategory = 'discrete-data';
                break;
            case 'graph':
            case 'scatter':
                queryCategory = 'time-series';
                break;
            case 'gauge':
            case 'summary':
                queryCategory = 'summary';
                break;
            case 'table':
                queryCategory = 'table';
        }

        this.availableQueries.forEach((aq) => {
            if (queryCategory === aq.spec.query_info.query_category) {
                this.queriesList.push({
                    text: aq.spec.query_info.title,
                    value: aq.metadata.name
                });
            }
        });
    }

    // // Add variant control
    // addVariantControl(): void {
    //     console.log('Add variant ');
    //     this.selectedChartVariation = this.widgetForm.get('variant') as FormControl;
    //     this.selectedChartVariation.
    //     // const addrCtrl = this.initAction();
    //     this.queries.push(this.createQueryItem());
    //     this.maxQueries--;
    // }

    // Add query item
    addQueryItem(): void {
        console.log('Add query item');
        // this.queries = this.widgetForm.get('queries') as FormArray;
        // const addrCtrl = this.initAction();
        this.queries.push(this.createQueryItem());
        this.maxQueries--;
    }
    // Remove query item
    removeQueryItem(i: number): void {
        console.log('Remove query item ' + i);
        // const control = this.widgetForm.get('queries') as FormArray;
        this.queries.removeAt(i);
        this.maxQueries++;
    }

    // Reset queries
    resetQueries(): void {
        this.queries?.clear();
        // Set queries
        this.queries = this.widgetForm.get('queries') as FormArray;
        // this.addQueryItem();
    }

    // Clears queries on form and adds 1 default queryItem
    resetFormQueries(): void {
        this.queries = this.widgetForm.get('queries') as FormArray;
        this.queries.clear();
        this.addQueryItem();
    }

    createQueryItem(): FormGroup {
        if (!this.selectedChartType) {
            return null;
        }
        switch (this.selectedChartType.value) {
            case 'gauge':
            case 'pie':
                // just queries
                return this._fb.group({
                    query: ['', Validators.required]
                });
            case 'summary':
            case 'table':
            case 'bar':
                // queries, color
                return this._fb.group({
                    query: ['', Validators.required],
                    color: ['']
                });
            case 'graph':
            case 'scatter':
                // queries, color and showOnAxis
                return this._fb.group({
                    query: ['', Validators.required],
                    color: [''],
                    showOnAxis: [false]
                });
        }
    }

    private populateChartTypes(): void {
        this.chartTypes = CHART_TYPES.map((ct) => {
            return {
                text: ct.text,
                value: ct.value
            };
        });
    }

    private setChartType(chartType): void {
        this.widgetForm.get('type').setValue(chartType);
    }

    private setChartVariation(chartVariation): void {
        if (!this.selectedChartType.variations) {
            return;
        }
        this.selectedChartVariation = this.selectedChartType.variations.find((cv) => {
            return cv.value === chartVariation;
        });
    }

    private populateChartVariations(chartType): void {
        this.selectedChartType = CHART_TYPES.find((ct) => {
            return ct.value === chartType;
        });
        this.chartVariants = this.selectedChartType.variations || [];
        this.setupMaxQueries();
    }
    private populateQueryColors(colorPalette): void {
        // TODO reset all colors when palette changes
        // this.widgetForm.controls['queries']['controls'].forEach((quer) => {
        //     quer['color'].
        // });
        if (!colorPalette) {
            this.queryColors = [];
            return;
        }
        let qc = [];
        switch (colorPalette) {
            case 'dark':
                qc = DARK_THEME.colors;
                break;
            case 'fuse':
                qc = FUSE_THEME.colors;
                break;
            case 'orange':
                qc = ORANGE_THEME.colors;
                break;
            case 'blue':
                qc = BLUE_THEME.colors;
                break;
            case 'vibrant':
                qc = VIBRANT_THEME.colors;
                break;
            case 'pastel':
                qc = PASTEL_THEME.colors;
                break;
            case 'traditional1':
                qc = TRADITIONAL1_THEME.colors;
                break;
            case 'traditional2':
                qc = TRADITIONAL2_THEME.colors;
                break;
            case 'traditional3':
                qc = TRADITIONAL3_THEME.colors;
                break;
            case 'modern1':
                qc = MODERN1_THEME.colors;
                break;
            case 'modern2':
                qc = MODERN2_THEME.colors;
                break;
            default:
                qc = DEFAULT_THEME.colors;
        }
        this.queryColors = qc.map((c) => {
            return {
                text: c,
                value: c
            };
        });
    }

    populateForEdit(widget: Widget): void {
        this.isEdit = true;
        const location = this._locationService.locationDataAsKeyValue().currentLocation;
        // RESET widgetForm
        this.widgetForm.reset();
        // Set basic form fields
        this.widgetForm.get('id').setValue(widget.id);
        this.widgetForm.get('title').setValue(widget.title);
        this.widgetForm.get('privilege').setValue(widget.privilege);
        this.widgetForm.get('colorPalette').setValue(widget.colorPalette);
        this.widgetForm.get('opacity').setValue(widget.opacity);
        // Populate colors
        this.populateQueryColors(widget.colorPalette);
        // Generate ChartTypes
        this.populateChartTypes();
        // Set ChartType
        this.setChartType(widget.chartType);
        // Populate ChartVariations
        this.populateChartVariations(widget.chartType);
        // Set ChartVariation
        this.widgetForm.get('variant').setValue(widget.chartVariant);
        this.setChartVariation(widget.chartVariant);
        this.setupMaxQueries();
        // Populate Queries
        this.dashboardService.getChartQueryList(location, widget.privilege).pipe(takeUntil(this.unsubscribe$)).subscribe((res: any) => {
            this.queriesList = [];
            this.availableQueries = res['items'];
            this.availableQueries.forEach((aq) => {
                this.queriesList.push({
                    text: aq.spec.query_info.title,
                    value: aq.metadata.name
                });
            });
            // Filter queries by ChartType
            this.filterQueriesByChartType();
            // Reset queries
            this.resetQueries();
            // Set queries
            // this.queries = this.widgetForm.get('queries') as FormArray;

            switch (widget.chartType) {
                case 'gauge':
                case 'pie':
                    // just queries
                    widget.chartQueries.forEach((query) => {
                        const qControl = this._fb.group({
                            query: [query.query.metadata.name, Validators.required]
                        });
                        this.queries.push(qControl);
                        this.maxQueries--;
                    });
                    break;
                case 'table':
                case 'summary':
                case 'bar':
                    // queries, color and showOnAxis
                    widget.chartQueries.forEach((query) => {
                        const qControl = this._fb.group({
                            query: [query.query.metadata.name, Validators.required],
                            color: [query.color]
                        });
                        this.queries.push(qControl);
                        this.maxQueries--;
                    });
                    break;
                case 'graph':
                case 'scatter':
                    // queries, color and showOnAxis
                    widget.chartQueries.forEach((query) => {
                        const qControl = this._fb.group({
                            query: [query.query.metadata.name, Validators.required],
                            color: [query.color],
                            showOnAxis: [query.showOnAxis]
                        });
                        this.queries.push(qControl);
                        this.maxQueries--;
                    });
            }
        });
        this.selectChartIcon(widget.chartType);
    }

    setupMaxQueries(): void {
        if (!this.selectedChartVariation) {
            this.maxQueries = this.selectedChartType.maxQueries || 0;
        } else {
            this.maxQueries = this.selectedChartVariation?.maxQueries || 0;
        }
    }

    selectChartIcon(chartType): void {
        switch (chartType) {
            case 'bar':
                this.chartIcon = 'bar_chart';
                break;
            case 'graph':
                this.chartIcon = 'show_chart';
                break;
            case 'pie':
                this.chartIcon = 'pie_chart';
                break;
            case 'scatter':
                this.chartIcon = 'scatter_plot';
                break;
            case 'table':
                this.chartIcon = 'table_chart';
                break;
            case 'summary':
                this.chartIcon = 'insert_chart';
                break;
            case 'gauge':
                this.chartIcon = 'speed';
                break;
        }
    }

    hasQueryColor(): boolean {
        return this.selectedChartType?.value === 'graph' ||
            this.selectedChartType?.value === 'summary' ||
            this.selectedChartType?.value === 'table' ||
            this.selectedChartType?.value === 'bar';
    }

    hasShowOnAxis(): boolean {
        return this.selectedChartType?.value === 'graph';
    }

    onClose(event): void {
        this.dialog.close(event);
    }

    onCancelAction(): void {
        this.dialog.close({ text: 'Cancel' });
    }
    onConfirmAction(): void {
        this.dialog.close({ text: 'Save' });
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next(true);
        this.unsubscribe$.complete();
    }
}
