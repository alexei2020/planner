import { Component, OnInit, Input } from "@angular/core";
import { DialogContentBase, DialogRef } from "@progress/kendo-angular-dialog";
import { FormControl, FormGroup, FormBuilder } from "@angular/forms";
@Component({
  selector: "kendo-dashboard-dialog",
  templateUrl: "./kendo-dashboard-dialog.component.html",
  styleUrls: ["./kendo-dashboard-dialog.component.scss"],
})
export class KendoDashboardDialog extends DialogContentBase implements OnInit {
  @Input() public name: string;
  public form: FormGroup;
  public dialog: DialogRef;

  constructor(private dlg: DialogRef, private formBuilder: FormBuilder) {
    super(dlg);
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(this.name),
    });
  }
  onCancelBtnClicked() {
    this.dialog.close({ text: "Cancel" });
  }
  onSaveBtnClicked() {
    this.dialog.close({ text: "Save" });
  }
}
