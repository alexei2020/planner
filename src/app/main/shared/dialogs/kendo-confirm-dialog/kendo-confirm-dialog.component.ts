import { Component, OnInit, Input } from "@angular/core";
import { DialogContentBase, DialogRef } from "@progress/kendo-angular-dialog";

@Component({
  selector: "kendo-confirm-dialog",
  templateUrl: "./kendo-confirm-dialog.component.html",
  styleUrls: ["./kendo-confirm-dialog.component.scss"],
})
export class KendoConfirmDialog extends DialogContentBase implements OnInit {
  @Input() public message: string;

  public dialog: DialogRef;
  data: any;

  constructor() {
    super(null);
  }

  ngOnInit(): void {

  }
  onNoBtnClicked(){
    this.dialog.close({ text: 'No' });
  }
  onYesBtnClicked(){
    this.dialog.close({ text: 'Yes' });
  }
}
