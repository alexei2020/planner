import { Component, OnInit } from '@angular/core';
import {Widget} from '../../../widget';
import {NotificationService} from '../../services';
import {DialogContentBase, DialogRef} from '@progress/kendo-angular-dialog';

@Component({
  selector: 'app-json-dialog',
  templateUrl: './json-dialog.component.html',
  styleUrls: ['./json-dialog.component.scss']
})
export class JsonDialogComponent extends DialogContentBase implements OnInit {

    public dialog: DialogRef;
    widget: Widget;
    data: any;

    constructor(
      private notificationService: NotificationService
    ) {
        super(null);
    }

    ngOnInit(): void {
      if (this.widget) {
          this.data = JSON.stringify(this.widget, null, 2);
      }
    }

    update(): void {
        try {
            const widget: Widget = JSON.parse(this.data);
            this.dialog.close(widget);
        } catch (e) {
            this.notificationService.openSnackBar('Error in JSON content', '');
        }
    }


    onCopyToClipboard(data): void {
        this.notificationService.openSnackBar('Content copied to clipboard', '');
    }

}
