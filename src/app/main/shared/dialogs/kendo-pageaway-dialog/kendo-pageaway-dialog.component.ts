import { Component, OnInit, Input } from "@angular/core";
import { DialogContentBase, DialogRef } from "@progress/kendo-angular-dialog";

@Component({
  selector: "kendo-pageaway-dialog",
  templateUrl: "./kendo-pageaway-dialog.component.html",
  styleUrls: ["./kendo-pageaway-dialog.component.scss"],
})
export class KendoPageawayDialog extends DialogContentBase implements OnInit {
  @Input() public message: string;

  public dialog: DialogRef;

  constructor() {
    super(null);
  }

  ngOnInit(): void {

  }
  onNavigateAction(){
    this.dialog.close({ text: 'Yes' });
  }
  onCancelAction(){
    this.dialog.close({ text: 'No' });
  }
  onSaveAction(){
    this.dialog.close({ text: 'Save' });
  }
}
