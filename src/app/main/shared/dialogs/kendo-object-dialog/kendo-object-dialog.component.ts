import { Component, OnInit, Input } from '@angular/core';
import { DialogContentBase, DialogRef } from '@progress/kendo-angular-dialog';
import {FuseLocationService} from '../../../../services/location.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {DropdownListItem, FuseTenantService} from '../../../../components/tenant/tenant.service';
import * as _ from 'lodash';
import {ILocation, ILocationDataItem, ILocations} from '../../../../typings/location';
import {StringMap} from '../../../../typings/backendapi.gen';
import {FuseLocationDataManagementService} from '../../../../services/location-data-management.service';
import {LocationItem, Metadata, LocationDataItem, LocationListData, LocationListItem} from '../../../../components/shared/location-item';
@Component({
  selector: 'kendo-object-dialog',
  templateUrl: './kendo-object-dialog.component.html',
  styleUrls: ['./kendo-object-dialog.component.scss'],
})
export class KendoObjectDialog extends DialogContentBase implements OnInit {
  public locationDataSource: any[];
  public filteredLocationData: any[];
  public selectedKeys: Array<any>;
  public keys: string[] = [];
  public show = false;
  public searchTerm = '';
  // Private
  currentLocation = 'ws.io';
  locationList: any[];
  private _location: string;

  public data: {
    name: string;
    location: string | null;
  };
  @Input() public location: string | null;
  public dialog: DialogRef;
  private _unsubscribeAll: Subject<any>;
  constructor(
    private dlg: DialogRef,
    private _fuseTenantService: FuseTenantService,
    private _locationService: FuseLocationService,
    private locationDataManagementService: FuseLocationDataManagementService
  ) {
    super(dlg);
    this._unsubscribeAll = new Subject();
  }


  ngOnInit(): void {
    this.data = {
      name: '',
      location: this.location,
    };
    this._fuseTenantService.selectedTenant
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(async (tenant: DropdownListItem) => {
        if (!tenant || !tenant.name || tenant.name.length <= 0) {
          this._location = 'ws.io';
          tenant.name = 'ws.io';
          tenant.APIGroupName = 'ws.io';
          tenant.location = 'ws.io';
          this.locationList = [];
          this.data.location = 'ws.io';
          this.processLocation({}, this.locationList, tenant);
          return;
        }
        let accessibleData = await this._locationService
          .getAccessibleData(tenant.name)
          .pipe(takeUntil(this._unsubscribeAll))
          .map((data) => data?.spec.locations)
          .toPromise();
        this._locationService
          .getHierarchicalData(tenant.APIGroupName)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((locationList: ILocations) => {
            this.locationList = [];
            this._location = this._locationService.locationDataAsKeyValue().currentLocation;
            // accessibleData[item.metadata.name].APIGroupName
            accessibleData = this.processLocation(accessibleData, locationList, tenant);
          });
      });
  }

  private processLocation(accessibleData: StringMap<ILocationDataItem>, locationList: any, tenant: DropdownListItem) {
    if (this.data.location && this.data.location === 'ws.io') {
      accessibleData = {};
      accessibleData[this.data.location] = {
        APIGroupName: this.data.location,
        APIGroupUID: this.data.location,
        RBAC: undefined
      };
      // @ts-ignore
      locationList.items = [new LocationItem({
        apiVersion: this.data.location + '/v1',
        name: this.data.location,
        metadata: new Metadata({
          name: this.data.location
        })
      })];
      // @ts-ignore
      locationList.apiVersion = 'ws.io/v1';
    }
    const data = locationList.items.map((item: ILocation) => {
      this.updateCurrentLocationData(locationList, accessibleData, tenant);
    });
    return accessibleData;
  }

  getLocationListData(locationList: ILocations, accessibleData: StringMap<ILocationDataItem>): LocationListData {
    const innerLocationList: LocationListItem[] = [];

    const data = _.map(locationList.items, (item: ILocation) => {
      innerLocationList.push(
        new LocationListItem({
          label: item.metadata.name,
          name: item.metadata.name,
          value: accessibleData[item.metadata.name].APIGroupName,
        })
      );
      const parentLink = item.spec?.base.parent_link === undefined ? '' : item.spec.base.parent_link;

      return new LocationDataItem({
        name: item.metadata.name,
        selfLink: item.metadata.selfLink,
        parentLink: parentLink,
        value: accessibleData[item.metadata.name].APIGroupName,
        APIGroupName: accessibleData[item.metadata.name].APIGroupName,
        APIGroupUID: accessibleData[item.metadata.name].APIGroupUID,
        RBAC: accessibleData[item.metadata.name].RBAC,
      });
    });

    const locationListData = new LocationListData({
      locationList: innerLocationList,
      locationData: data,
    });

    return locationListData;
  }

  private updateCurrentLocationData(
      locationList: ILocations,
      accessibleData: StringMap<ILocationDataItem>,
      tenant: DropdownListItem
  ): void {
      this.locationList = [];

      const locationListData = this.getLocationListData(locationList, accessibleData);
      this.locationList = locationListData.locationList;

      const root = this.locationDataManagementService.getLocationRoot(locationListData.locationData);
      let selectedLoc = tenant.location ? tenant.location : root.name;
      if (this.data.location === 'ws.io') {
        selectedLoc = this.data.location;
      }
      this.selectedKeys = [selectedLoc];
      this.locationDataSource = [root];
      this.filteredLocationData = this.locationDataSource;

      const currentLocationName = this._locationService.locationDataAsKeyValue().locationName;
      const subTree = this.getCurrentLocationSubTree(currentLocationName, this.filteredLocationData);
      if (this.data.location === 'ws.io') {
        return;
      }

      if (subTree) {
        this.data.location = subTree.APIGroupName;
      }
      else {
        console.log('Failed to find subtree');
      }
  }

  private getCurrentLocationSubTree(currentLocation: string, tree: LocationDataItem[]): LocationDataItem {
    let currentLocationSubTree: LocationDataItem;
    _.forEach(tree, (item) => {
      if (item.name === currentLocation) {
        currentLocationSubTree = item;
        return false;
      }
      if (!_.isEmpty(item.children)) {
        currentLocationSubTree = this.getCurrentLocationSubTree(currentLocation, item.children);
        if (!_.isEmpty(currentLocationSubTree)) {
          return false;
        }
      }
    });
    return currentLocationSubTree;
  }

  close(): void {
    this.show = false;
  }

  onToggle(): void {
    this.show = !this.show;
    if (this.show) {
      this.filteredLocationData = this.locationDataSource;
      this.searchTerm = '';
    }
  }

  updateLocation({ dataItem }: any): void {
    this.show = false;
    this.selectedKeys = [dataItem.name];
    this.data.location = dataItem.APIGroupName;
  }

  onKeyUp(value: string): void {
    this.filteredLocationData = this.hierarchicalSearch(this.locationList, value);
  }

  contains(text: string, term: string): boolean {
    return text.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  }

  search(): any[] {
    return [];
  }

  hierarchicalSearch(items: LocationListItem[], term: string): LocationListItem[] {
    const hierarchicalItems = _.reduce(
      items,
      (acc: LocationListItem[], item) => {
        if (this.contains(item.label, term)) {
          acc.push(item);
        } else if (item.children && item.children.length > 0) {
          const newItems = this.search();

          if (newItems.length > 0) {
            acc.push({ ...item, name: item.name, children: newItems });
          }
        }
        return acc;
      },
      []
    );

    return hierarchicalItems;
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  onCancelAction(): void {
    this.dialog.close({ text: 'Cancel' });
  }

  onConfirmAction(): void {
    this.dialog.close({ text: 'Ok' });
  }

  get nameRules(): any {
      return [
          { name: 'firstAlphaNumeric', type: 'pattern', value: '^([A-Za-z0-9].*)$' },
          { name: 'allowedCharacters', type: 'pattern', value: `^[a-zA-Z0-9]([a-zA-Z0-9+_.@\-]*[a-zA-Z0-9+_.@\-])?(\.[a-zA-Z0-9+_.@\-]([a-zA-Z0-9+_.@\-]*[a-zA-Z0-9+_.@\-])?)*$` },
      ];
  }
}
