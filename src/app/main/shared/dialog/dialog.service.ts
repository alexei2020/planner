
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';

import { LocationEditDialogComponent } from '../edit-location-dialog/edit-location-dialog.component';
import { LocationAddDialogComponent } from '../add-location-dialog/add-location-dialog.component'
import { AddObjectDialogComponent } from '../add-object-dialog/add-object-dialog.component'
import { NoticeDialogComponent } from '../notice-dialog/notice-dialog.component';
@Injectable({
  providedIn: 'root'
})
export class DialogService {

  private subject = new Subject<any>();

  constructor(private dialog: MatDialog) { }

  setFormValue(form: any, invalidFlag, editedFlag) {
    this.subject.next({ data: form, invalidFlag: invalidFlag, editedFlag: editedFlag });
  }

  getFormValue(): any {
    return this.subject.asObservable();
  }

  openConfirmDialog(data): MatDialogRef<ConfirmDialogComponent, any> {
    return this.dialog.open(ConfirmDialogComponent, {
      width: '390px',
      panelClass: 'confirm-dialog-container',
      disableClose: true,
      position: { top: 'calc(50vh - 80px)' },
      data: data
    });
  }

  openNoticeDialog(data): MatDialogRef<NoticeDialogComponent, any> {
    return this.dialog.open(NoticeDialogComponent, {
      width: '390px',
      panelClass: 'notice-dialog-container',
      disableClose: true,
      position: { top: 'calc(50vh - 80px)' },
      data: data
    });
  }

  openLocationEditDialog(data): MatDialogRef<LocationEditDialogComponent, any>{
    return this.dialog.open(LocationEditDialogComponent, {
      width: '400px',
      panelClass: 'location-edit-dialog-container',
      disableClose: true,
      position: { top: 'calc(50vh - 200px)' },
      data: data
    })
  }
  openLocationAddDialog(data): MatDialogRef<LocationAddDialogComponent, any>{
    return this.dialog.open(LocationAddDialogComponent, {
      width: '400px',
      panelClass: 'location-add-dialog-container',
      disableClose: true,
      position: { top: 'calc(50vh - 200px)' },
      data: data
    })
  }
  openAddObjectDialog(data): MatDialogRef<AddObjectDialogComponent, any>{
    return this.dialog.open(AddObjectDialogComponent, {
      width: '400px',
      panelClass: 'object-add-dialog-container',
      disableClose: true,
      position: { top: 'calc(50vh - 200px)' },
      data: data
    })
  }
}
