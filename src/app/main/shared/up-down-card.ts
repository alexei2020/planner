export interface UpDownCard {
  title: string;
  query: string;
  data?: any[];
  upMatching?: UpDownMatchingCriteria[];
  downMatching?: UpDownMatchingCriteria[];
  location: string;
}

export interface UpDownMatchingCriteria {
  name: string;
  value: any;
  criteria: UDCriteria;
}

export enum UDCriteria {
  EQUAL,
  NOT_EQUAL,
  GT,
  LT
}
