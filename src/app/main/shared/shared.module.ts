import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';
import {NoticeDialogComponent} from './notice-dialog/notice-dialog.component';
import {MaterialModule} from './material';
import {NgxEchartsModule} from 'ngx-echarts';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CenterMenuDirective} from './directives/center-menu.directive';
import {WidgetDialogComponent} from './widget-dialog/widget-dialog.component';
import {ClipboardDirective} from './directives/clipboard.directive';
import {RoundPipe} from './pipes/round.pipe';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {AceEditorModule} from 'ng2-ace-editor';
import {AddDialogComponent} from './add-dialog/add-dialog.component';
import {AddItemDialogComponent} from './add-item-dialog/add-item-dialog.component';
import {AddItemDashboardDialogComponent} from './add-item-dashboard-dialog/add-item-dashboard-dialog.component';
import {LocationEditDialogComponent} from './edit-location-dialog/edit-location-dialog.component';
import {LocationAddDialogComponent} from './add-location-dialog/add-location-dialog.component';
import {AddObjectDialogComponent} from './add-object-dialog/add-object-dialog.component';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';

import {AgmCoreModule} from '@agm/core';

import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';
import {ManageChartDialogComponent} from './dialogs/manage-chart-dialog/manage-chart-dialog.component';
import {TabStripModule} from '@progress/kendo-angular-layout';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {LabelModule} from '@progress/kendo-angular-label';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {JsonDialogComponent} from './dialogs/json-dialog/json-dialog.component';
import {KendoConfirmDialog} from './dialogs/kendo-confirm-dialog/kendo-confirm-dialog.component';
import {KendoNoticeDialog} from './dialogs/kendo-notice-dialog/kendo-notice-dialog.component';
import {KendoDashboardDialog} from './dialogs/kendo-dashboard-dialog/kendo-dashboard-dialog.component';
import {KendoReportItemDialog} from './dialogs/kendo-report-item-dialog/kendo-report-item-dialog.component';
import {KendoDashboardItemDialog} from './dialogs/kendo-dashboard-item-dialog/kendo-dashboard-item-dialog.component';
import {KendoObjectDialog} from './dialogs/kendo-object-dialog/kendo-object-dialog.component';
import {KendoPageawayDialog} from './dialogs/kendo-pageaway-dialog/kendo-pageaway-dialog.component';
import {DialogsModule} from '@progress/kendo-angular-dialog';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {UpDownWidgetComponent} from './widgets/up-down-widget/up-down-widget.component';
import {UpDownGroupWidgetComponent} from './widgets/up-down-group-widget/up-down-group-widget.component';
import {SummaryCardsWidgetComponent} from './widgets/summary-cards-widget/summary-cards-widget';
import {SummaryCardWidgetComponent} from './widgets/summary-cards-widget/summary-card-widget/summary-card-widget';
import {FuseSharedModule} from '@fuse/shared.module';
import {IconsModule} from '@progress/kendo-angular-icons';
import {TooltipModule} from '@progress/kendo-angular-tooltip';
import {TreeViewModule} from '@progress/kendo-angular-treeview';
import {PopupModule} from '@progress/kendo-angular-popup';

@NgModule({
  declarations: [
    ConfirmDialogComponent,
    NoticeDialogComponent,
    AddDialogComponent,
    LocationEditDialogComponent,
    LocationAddDialogComponent,
    AddObjectDialogComponent,
    AddItemDialogComponent,
    AddItemDashboardDialogComponent,
    WidgetDialogComponent,
    CenterMenuDirective,
    JsonDialogComponent,
    KendoReportItemDialog,
    KendoConfirmDialog,
    KendoNoticeDialog,
    KendoDashboardItemDialog,
    KendoDashboardDialog,
    KendoObjectDialog,
    KendoPageawayDialog,
    ClipboardDirective,
    RoundPipe,
    ManageChartDialogComponent,
    UpDownWidgetComponent,
    UpDownGroupWidgetComponent,
    SummaryCardsWidgetComponent,
    SummaryCardWidgetComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AceEditorModule,
    FormsModule,
    FlexLayoutModule,
    DragDropModule,
    MaterialModule,
    AngularSvgIconModule,
    NgMultiSelectDropDownModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBHO-0OpniAovUy5QXNZPmKvMAYrshyrAY',
      libraries: ['places']
    }),
    MatGoogleMapsAutocompleteModule,
    TabStripModule,
    InputsModule,
    LabelModule,
    DropDownsModule,
    ButtonsModule,
    DialogsModule,
    ClipboardModule,
    FuseSharedModule,
    IconsModule,
    TooltipModule,
    TreeViewModule,
    PopupModule,
  ],
  exports: [
    CommonModule,
    FlexLayoutModule,
    DragDropModule,
    MaterialModule,
    NgxEchartsModule,
    ReactiveFormsModule,
    FormsModule,
    CenterMenuDirective,
    RoundPipe,
    AngularSvgIconModule,
    NgMultiSelectDropDownModule,
    UpDownWidgetComponent,
    UpDownGroupWidgetComponent,
    SummaryCardsWidgetComponent,
    SummaryCardWidgetComponent,
  ],
  entryComponents: [
    ConfirmDialogComponent,
    NoticeDialogComponent,
    WidgetDialogComponent,
    JsonDialogComponent,
    AddDialogComponent,
    LocationEditDialogComponent,
    LocationAddDialogComponent,
    AddObjectDialogComponent,
    AddItemDialogComponent,
    KendoReportItemDialog,
  ]
})
export class SharedModule {
}
