import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 * @title Dialog Overview
 */
@Component({
    selector: 'add-location-dialog',
    templateUrl: 'add-location-dialog.component.html',
    styleUrls: ['add-location-dialog.component.scss'],
})
export class LocationAddDialogComponent implements OnInit {
    public form: FormGroup;
    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        private formBuilder: FormBuilder,
        public dialogRef: MatDialogRef<LocationAddDialogComponent>) {
        this.form = this.formBuilder.group({
            name: [data && data.name ? data.name : '']
        });
    }

    ngOnInit() { }

    closeDialog() {
        this.dialogRef.close(null);
    }

}
