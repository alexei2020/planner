import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { ReportMenuService } from '../services/report-menu.service';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from '../services';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/**
 * @title Dialog Overview
 */
@Component({
  selector: 'add-item-dialog',
  templateUrl: 'add-item-dialog.component.html',
  styleUrls: ['add-item-dialog.component.scss'],
})
export class AddItemDialogComponent implements OnInit {
  public form: FormGroup;
  public reports = [];
  public prevChoice = [];
  public toRemoveList = [];
  public toAddList = [];
  public selectData = [];
  private _unsubscribeAll: Subject<any> = new Subject();
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<AddItemDialogComponent>,
    private formBuilder: FormBuilder,
    private _reportMenuService: ReportMenuService,
    private _route: ActivatedRoute,
    private _noticeService: NotificationService
  ) {
    this.form = this.formBuilder.group({
      selectedItems: [[]],
    });

    this._reportMenuService.getReports(this._route.snapshot.queryParams["loc"]).pipe(takeUntil(this._unsubscribeAll)).subscribe(({ items }: any) => {
      this.reports = items.map((item: any) => item = { ...item, ...{ spec: { layout: JSON.parse(item.spec.layout) } } });
      this.reports.forEach((value: any) => {
        this.selectData.push({ text: value.metadata.name, id: value.metadata.uid });
        if (value.spec.layout && value.spec.layout.length > 0) {
          const typeMatch = value.spec.layout.filter((layout: any) => layout.reportType === this.data.item.reportType);
          if (typeMatch.length > 0) {
            const matchId = typeMatch.find((layout: any) => (!layout.id || layout.id === this.data.item.id)) ? value : null;
            if(matchId && matchId.metadata) {
              this.prevChoice.push({ text: matchId.metadata.name, id: matchId.metadata?.uid });
            }
          }
        }
      });
      this.form.get('selectedItems').patchValue(this.prevChoice);
    });
  }

  ngOnInit() {
    this.form.get('selectedItems').valueChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe((value) => {
      this.toRemoveList = this.prevChoice.filter(item => !value.some(value => value.id === item.id));
      this.toAddList = value.filter(item => !this.prevChoice.some(value => value.id === item.id));
    });
  }

  closeDialog() {
    this.dialogRef.close(false);
  }

  saveReports() {
    const removedData = this.reports.filter(item => this.toRemoveList.some(record => record.id === item.metadata.uid));
    removedData.forEach(report => {
      const indexOfRecord = report.spec.layout.findIndex(record => record.reportType === this.data.item.reportType && (!this.data.item.id || record.id === this.data.item.id));
      if (indexOfRecord !== -1) {
        report.spec.layout = report.spec.layout.filter((item, index) => index !== indexOfRecord);
      }
    });

    const addedData = this.reports.filter(item => this.toAddList.some(record => record.id === item.metadata.uid));
    addedData.forEach(report => {
      const indexOfRecord = report.spec.layout.findIndex(record => record.reportType === this.data.item.reportType && (!this.data.item.id || record.id === this.data.item.id));
      if (indexOfRecord === -1) {
        report.spec.layout.push(this.data.item);
      }
    });

    combineLatest([...removedData, ...addedData].map((report: any) => this.setLayoutItem(report))).pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
      this._noticeService.openSnackBar('Added to reports successfully', '');
      this.dialogRef.close(false);
    },
      () => {
        this._noticeService.openSnackBar(`Failed added to report`, '');
      });
  }

  setLayoutItem(report) {
   
    const layouts = report.spec.layout;

    report = {
      ...report,
      spec: {
        layout: JSON.stringify(layouts)
      }
    }
    return this._reportMenuService.saveReport(report.metadata.name, report, this._route.snapshot.queryParams["loc"]).pipe(takeUntil(this._unsubscribeAll));
  }

  public get disabledByRows() {
    let toRemoveList = this.prevChoice.filter(item => !this.form.value.selectedItems.some(value => value.id === item.id));
    let toAddList = this.form.value.selectedItems.filter(item => !this.prevChoice.some(value => value.id === item.id));
    return toRemoveList.length > 0 || toAddList.length > 0;
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
