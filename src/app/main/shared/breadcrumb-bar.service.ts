import { Injectable } from '@angular/core';
import { Params } from '@angular/router';

import * as _moment from 'moment';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Subject, ReplaySubject, BehaviorSubject } from 'rxjs';

import { Dashboard } from '../shared/dashboard.model';
import { PageType } from './breadcrumb-bar.service.gen';

import { IBreadCrumb } from '../../../@fuse/components/breadcrumbs/IBreadCrumb';
import { FileItem } from 'app/resource/file-manager/shared/file-item';
import _ from 'lodash';
import { Device } from '../apps/topology/shared/device';

const moment = _moment;

export interface IDropdownItem {
  item_text: string;
  item_id: string;
}

export interface InstanceInterface {
	vendor: Array<string>,
	model: Array<string>,
	hostName: Array<string>,
	ipv4Net: Array<string>
}
export interface PropertyInterface {
	vendor: string,
	model: string,
	hostName: string,
	ipv4Net: string
}
export interface IMultiSelectDropdown {
  dropdownSettings: IDropdownSettings;
  dropdownList: Array<IDropdownItem>;
  selectedItems: Array<IDropdownItem>;
}
export interface ISelectDropdown {
  dropdownList: Array<IDropdownItem>;
  selectedItem: IDropdownItem;
}
export enum TopologyViewType {
  Left2Right = 'Left2Right',
  Top2Bottom = 'Top2Bottom',
}
export enum EventType {
	DASHBOARD_SAVE,
	DASHBOARD_ADD_LAYOUT,
	DASHBOARD_DELETE,
	DASHBOARD_CLONE,
	DASHBOARD_ADD,
	DASHBOARD_CHANGE_LAYOUT,
	DASHBOARD_SET_RANGE,
	DASHBOARD_REFRESH,
	DASHBOARD_SET_INTERVAL,

	REPORT_EDIT_ITEMS,
	REPORT_EDIT_TITLE,
	REPORT_SAVE,
	REPORT_DELETE,
	REPORT_CLONE,
	REPORT_TO_PDF,
	REPORT_ADD_TEXT_CONTROL,
	REPORT_ADD_IMAGE_CONTROL,
	REPORT_ADD_TIMER_CONTROL,
	REPORT_ADD_LINE,
	REPORT_ADD,
	REPORT_CHANGE_LAYOUT,
	REPORT_SET_RANGE,
	REPORT_REFRESH,
	REPORT_SET_INTERVAL,
	REPORT_ITEM_NO_EDIT,

	REPORT_ITEM_UNLOCK,
	REPORT_ITEM_EDIT,
	REPORT_ITEM_STATE_EDIT,
	REPORT_ITEM_REMOVE,
	REPORT_ITEM_INIT,
	REPORT_ITEM_UNSAVE,
	REPORT_ITEM_IS_EDIT,

	DEVICE_DETAIL_SAVE,
	DEVICE_DETAIL_CANCEL,
	DEVICE_DETAIL_ADD,
	DEVICE_DETAIL_CLONE,
	DEVICE_DETAIL_DELETE
}
export interface Event {
	type: EventType,
	param?: string
}
export interface CurrentPageInfo {
	detailsPage: boolean,
	kind?: string,
	name?: string,
	namespace?: string
}

export class FormValidationInfo {
	name: string;
	isValid: boolean;

	constructor({name, isValid}: Partial<FormValidationInfo> = {}) {
		this.name = name || 'default';
		this.isValid = isValid;
	}
}

@Injectable({ providedIn: 'root' })
export class BreadcrumbService {
	//multi select
	shouldUpdateTopology = new Subject();
  	shouldFitToScreen = new Subject();
  	instances = {} as InstanceInterface;
  	propertyDropdown = {} as IMultiSelectDropdown;
  	instanceDropdown = {} as IMultiSelectDropdown;
  	viewDropdown = {} as ISelectDropdown;

  	isFitToScreen = false;

  	didTopologyComponentMounted = false;
  	canShowTopologyMenu = new Subject<boolean>();
  	pageInfo: any;

	//layout menu variables
	layouts = new BehaviorSubject<any[]>(null);
	item = new BehaviorSubject<Dashboard[]>(null);
	// switch subjects
	dropdownList: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
	allSelected: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
	deAllSelected: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
	itemSelected: Subject<any> = new Subject<any>();
	deItemSelected: Subject<any> = new Subject<any>();
	selectedItems: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
	selectedItemsChanges: BehaviorSubject<any[]> = new BehaviorSubject([]);


	//report menu variables
	// reportLayouts = new Subject<any[]>();
	// report = new Subject<any[]>();
	isEditStateItem = new BehaviorSubject<boolean>(false);
	currentPage = new BehaviorSubject<PageType>(null);
	currentPageInfo = new BehaviorSubject<CurrentPageInfo>(null);
	triggeredEvent = new Subject<Event>();
	editorEvent = new Subject<string>();
	queryParams = new BehaviorSubject<Params>({});

	breadcrumbList = new ReplaySubject<Array<IBreadCrumb>>();
	private addBreadcrumbSubject = new Subject<IBreadCrumb>();
	addBreadcrumbFired = this.addBreadcrumbSubject.asObservable();

	//device user rules
	triggeredDeviceUserRuleEvent = new Subject<any>();

	//device user rules 
	triggeredLabelRuleEvent = new Subject<any>();
	iconStyle = new Subject<string>();

	selectedPlatform = new BehaviorSubject<any[]>(null);
	platformSelected = new BehaviorSubject<any>(null);
	optionalDetailsSelected = new BehaviorSubject<any>(null);

	//enable or disable the save/cancel buttons in device detail page
	isDeviceDetailEditMode = new Subject<FormValidationInfo>();

	// FileManager
	private fileManagerBreadcrumbs: FileItem[] = [];
	private fileManagerBreadcrumbsChangedSubject = new BehaviorSubject<FileItem[]>([]);
	private fileManagerBreadcrumbsFiredSubject = new BehaviorSubject<FileItem>(null);
	fileManagerBreadcrumbsChanged = this.fileManagerBreadcrumbsChangedSubject.asObservable();
	fileManagerBreadcrumbsFired = this.fileManagerBreadcrumbsFiredSubject.asObservable();


	constructor() {
		this.resetPageInfo();
		this.instances = {
      			vendor: [],
      			model: [],
      			hostName: [],
      			ipv4Net: [],
    		};
  	}
	public resetPageInfo() {
		this.pageInfo = {
			distribution: {
				pageSize: 100,
				skip: 0,
				ratio: 3,
				count: 0,
				offset: 0
			},
			access: {
				pageSize: 100,
				skip: 0,
				ratio: 4,
				count: 0,
				offset: 0
			},
			'wireless-access-point': {
				pageSize: 100,
				skip: 0,
				ratio: 3,
				count: 0,
				offset: 0
			}
		}
	}
	public setRange(range: any) {
		switch (range.short) {
			case '5m': {
				range.start = moment().subtract(5, 'm').unix();
				range.end = moment().unix();
				break;
			}
			case '15m': {
				range.start = moment().subtract(15, 'm').unix();
				range.end = moment().unix();
				break;
			}
			case '30m': {
				range.start = moment().subtract(30, 'm').unix();
				range.end = moment().unix();
				break;
			}
			case '1h': {
				range.start = moment().subtract(1, 'h').unix();
				range.end = moment().unix();
				break;
			}
			case '3h': {
				range.start = moment().subtract(3, 'h').unix();
				range.end = moment().unix();
				break;
			}
			case '6h': {
				range.start = moment().subtract(6, 'h').unix();
				range.end = moment().unix();
				break;
			}
			case '12h': {
				range.start = moment().subtract(12, 'h').unix();
				range.end = moment().unix();
				break;
			}
			case '24h': {
				range.start = moment().subtract(24, 'h').unix();
				range.end = moment().unix();
				break;
			}
			case '2d': {
				range.start = moment().subtract(2, 'd').unix();
				range.end = moment().unix();
				break;
			}
			case '4w': {
				range.start = moment().subtract(4, 'w').unix();
				range.end = moment().unix();
				break;
			}
		}
		return range;
	}
	updateFileManagerBreadcrumbs(items: FileItem[]): void {
		this.fileManagerBreadcrumbs = items;
		this.fileManagerBreadcrumbsChangedSubject.next(this.fileManagerBreadcrumbs);
	}

	fileManagerBreadcrumbFired(item: FileItem): void {
		this.fileManagerBreadcrumbsFiredSubject.next(item);
	}

	addBreadcrumb(breadcrumb: IBreadCrumb): void {
		this.addBreadcrumbSubject.next(breadcrumb);
	}
  initializeInstancesByDevices(devices: Device[]): void {
    _.forEach(devices, (item) => {
      if (this.instances.vendor.length === 0 || this.instances.vendor.indexOf(item.status.vendor) === -1) {
        this.instances.vendor.push(item.status.vendor);
      }
      if (this.instances.model.length === 0 || this.instances.model.indexOf(item.status.model) === -1) {
        this.instances.model.push(item.status.model);
      }
      if (this.instances.hostName.length === 0 || this.instances.hostName.indexOf(item.status.hostName) === -1) {
        this.instances.hostName.push(item.status.hostName);
      }
      if (this.instances.ipv4Net.length === 0 || this.instances.ipv4Net.indexOf(item.status.ipv4Net) === -1) {
        this.instances.ipv4Net.push(item.status.ipv4Net);
      }
    });
  }

  updateInstanceDropdownList(): void {
    _.forEach(this.propertyDropdown.selectedItems, (item: IDropdownItem) => {
      _.forEach(this.instances[item.item_id], (element: string) => {
        this.instanceDropdown.dropdownList.push({
          item_id: `${item.item_id}_${element}`,
          item_text: element,
        });
      });
    });
  }

  updateFilterSelectedItems(): void {
    const selectedItems = [];
    if (_.isEmpty(this.propertyDropdown.selectedItems)) {
      return;
    }
    if (_.isEmpty(this.instanceDropdown.dropdownList)) {
      this.instanceDropdown.selectedItems = [];
      return;
    }
    this.instanceDropdown.selectedItems.forEach((item: IDropdownItem) => {
      _.forEach(this.instanceDropdown.dropdownList, (instanceItem) => {
        if (instanceItem.item_id === item.item_id) {
          selectedItems.push(item);
        }
      });
    });
    this.instanceDropdown.selectedItems = selectedItems;
  }
}
