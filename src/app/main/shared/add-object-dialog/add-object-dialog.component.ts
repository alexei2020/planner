import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseLocationService } from '../../../services/location.service';

@Component({
    selector: 'add-object-dialog',
    templateUrl: './add-object-dialog.component.html',
    styleUrls: ['./add-object-dialog.component.scss'],
})
export class AddObjectDialogComponent implements OnInit, OnDestroy {
    data: {
    	name: string;
    	location: string;
    };
    private _unsubscribeAll: Subject<any>;
    public locationList = [];

    constructor(
	private _locationService: FuseLocationService,
        @Inject(MAT_DIALOG_DATA) public location,
        public dialogRef: MatDialogRef<AddObjectDialogComponent>) {
    	this.data = {
      	    name: '',
      	    location: location,
    	};
	this._unsubscribeAll = new Subject();
    }

    ngOnInit() {
	if (this.data.location) {
		this.locationList.push(this.data.location);
		return;
	}
	this._locationService.getScope().pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
	    this.locationList.push(res.spec.home);
	    this.data.location = res.spec.home;
            res.spec.scopes.map(item => {
		if (!this.locationList.includes(item.apiGroup)) {
                	this.locationList.push(item.apiGroup);
            	}
	    });

        });
    }

    ngOnDestroy() {
	// Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onCancelAction(): void {
    	this.dialogRef.close(null);
    }

    onConfirmAction(): void {
    	this.dialogRef.close(this.data);
    }

    get nameRules(): any {
        return [
            { name: 'maxLength', type: 'pattern', value: '^(.{1,63})$' },
	    { name: 'noUpperCase', type: 'pattern', value: '^([^A-Z]+)$' },
	    { name: 'firstAlphaNumeric', type: 'pattern', value: '^([A-Za-z0-9].*)$' },
	    { name: 'lastAlphaNumeric', type: 'pattern', value: '^(.*[A-Za-z0-9])$' },
	    { name: 'allowedCharacters', type: 'pattern', value: '^([a-z0-9\\-]+)$' },
        ];
    }
}
