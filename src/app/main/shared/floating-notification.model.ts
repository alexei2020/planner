export interface FloatingNotificationModel {
	text: string;
	type: string;
}
