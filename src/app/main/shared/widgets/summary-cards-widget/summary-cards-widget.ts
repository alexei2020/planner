import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';

export interface SummaryCard {
  name: string;
  color: string;
  count: number;
  selected: boolean;
  fields: any[];
}

@Component({
  selector: 'app-summary-cards-widget',
  templateUrl: './summary-cards-widget.html',
  styleUrls: ['./summary-cards-widget.scss']
})
export class SummaryCardsWidgetComponent implements OnInit {

  @Input() cards: SummaryCard[];
  @Output() selectChange: any = new EventEmitter();

  cardWidth = 31;

  constructor() { }

  ngOnInit(): void {
    this.cardWidth = Math.floor(100 / this.cards.length) - 2;
  }

  handleSelectChange(name): void {
    // reset all other selects
    this.cards.filter((c) => c.name !== name).forEach((card) => {
      card.selected = false;
    });
    this.selectChange.emit();
  }

}
