import {Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import {SummaryCard} from '../summary-cards-widget';

@Component({
  selector: 'app-summary-card-widget',
  templateUrl: './summary-card-widget.html',
  styleUrls: ['./summary-card-widget.scss']
})
export class SummaryCardWidgetComponent implements OnInit {

  @Input() card: SummaryCard;
  @Output() selectChange: any = new EventEmitter();

  ngOnInit(): void {
  }

  cardSelect(){
    this.card.selected = true;
    this.selectChange.emit(this.card.name);
  }
}
