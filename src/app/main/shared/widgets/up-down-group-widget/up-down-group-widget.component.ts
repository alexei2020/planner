import {Component, Input, OnInit} from '@angular/core';
import {UpDownCard} from '../../up-down-card';

@Component({
  selector: 'app-up-down-group-widget',
  templateUrl: './up-down-group-widget.component.html',
  styleUrls: ['./up-down-group-widget.component.scss']
})
export class UpDownGroupWidgetComponent implements OnInit {

  @Input()
  columns = 3;

  @Input()
  cards: UpDownCard[];

  cardWidth = 31;

  constructor() { }

  ngOnInit(): void {
    this.cardWidth = Math.floor(100 / this.columns) - 2;
  }

}
