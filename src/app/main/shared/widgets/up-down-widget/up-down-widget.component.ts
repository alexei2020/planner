import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {UDCriteria, UpDownCard} from '../../up-down-card';
import {environment} from '../../../../../environments/environment'

@Component({
  selector: 'app-up-down-widget',
  templateUrl: './up-down-widget.component.html',
  styleUrls: ['./up-down-widget.component.scss']
})
export class UpDownWidgetComponent implements OnInit, OnDestroy {

    title: string;
    upValue: number;
    downValue: number;
    api: string;

    @Input()
    location: string;

    @Input()
    card: UpDownCard;

    private _unsubscribeAll: Subject<any> = new Subject();

  constructor(
      private _httpClient: HttpClient,
  ) { }

  ngOnInit(): void {
      this.title = 'Undefined title';
      this.upValue = 0;
      this.downValue = 0;
      this.location = 'ws.io';
      this.api = '/apis/' + this.location + '/v1/endpoints';
      this.getData();
  }

  getData(): void {
    if (this.card.data) {
      // TODO: get data from the card data
      return;
    }
    this._httpClient.get(environment.apiUrl + this.card.query)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((res: any) => {
          console.log('CARD DATA => ', res);

	  if (res.items === undefined)
            return;

          // TODO: temporary when no criteria
          if (!this.card.upMatching && !this.card.downMatching) {
            this.upValue = res.items.length;
            return;
          }

          res.items.forEach((item) => {

            // if (!this.card.upMatching && !this.card.downMatching) {
            //   return;
            // }

            // Proveriti sve kriterijume za Up
            if (this.card.upMatching?.every((mc) => {
                const fieldKeysArray = mc.name.split('.');
                const fieldValue = this.getFieldValue(item, fieldKeysArray);
                return this.isCriteriaMatched(fieldValue, mc.criteria, mc.value);
            })) {
              this.upValue++;
            }
            // Povecati za jedan ako ostane ispunjen

            //Proveriti sve kriterijume za Down
            if (this.card.downMatching?.every((mc) => {
                const fieldKeysArray = mc.name.split('.');
                const fieldValue = this.getFieldValue(item, fieldKeysArray);
                return this.isCriteriaMatched(fieldValue, mc.criteria, mc.value);
            })) {
              this.downValue++;
            }

          });
        });
  }

  isCriteriaMatched(value, criteria, targetValue): boolean {
    switch (criteria) {
      case UDCriteria.EQUAL:
        return value === targetValue;
      case UDCriteria.NOT_EQUAL:
        return value !== targetValue;
      case UDCriteria.GT:
        return value > targetValue;
      case UDCriteria.LT:
        return value < targetValue;
      default:
        return false;
    }
  }
    // Recursively get fieldValue
  getFieldValue(item: any, fields: string[]): any {
      if (fields.length > 1) {
          item = item[fields[0]];
          fields.shift();
      } else {
          return item[fields[0]];
      }
      return this.getFieldValue(item, fields);
  }


  ngOnDestroy(): void {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

}
