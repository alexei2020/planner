import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NotificationService, DashboardService } from '../services';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/**
 * @title Dialog Overview
 */
@Component({
  selector: 'add-item-dashboard-dialog',
  templateUrl: 'add-item-dashboard-dialog.component.html',
  styleUrls: ['add-item-dashboard-dialog.component.scss'],
})
export class AddItemDashboardDialogComponent implements OnInit {
  public form: FormGroup;
  public dashboards = [];
  public prevChoice = [];
  public selectData = [];
  public toRemoveList = [];
  public toAddList = [];
  private _unsubscribeAll: Subject<any> = new Subject();
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<AddItemDashboardDialogComponent>,
    private formBuilder: FormBuilder,
    private _dashboardService: DashboardService,
    private _route: ActivatedRoute,
    private _noticeService: NotificationService,

  ) {
    this.form = this.formBuilder.group({
      selectedItems: [[]],
    });

    this._dashboardService.getDashboard(this._route.snapshot.queryParams["loc"]).pipe(takeUntil(this._unsubscribeAll)).subscribe(({ items }: any) => {
      this.dashboards = items.map((item: any) => item = { ...item, ...{ spec: { layout: JSON.parse(item.spec.layout) } } });
      this.dashboards.forEach((value: any) => {
        this.selectData.push({ text: value.metadata.name, id: value.metadata.uid });
        if (value.spec.layout && value.spec.layout.length > 0) {
          const typeMatch = value.spec.layout.filter((layout: any) => layout.reportType === this.data.item.reportType);
          if (typeMatch.length > 0) {
            const matchId = typeMatch.find((layout: any) => (!layout.id || layout.id === this.data.item.id)) ? value : null
            this.prevChoice.push({ text: matchId.metadata.name, id: matchId.metadata.uid });
          }
        }
      });
      this.form.get('selectedItems').patchValue(this.prevChoice);
    });
  }

  ngOnInit() { 
    this.form.get('selectedItems').valueChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe((value) => {
      this.toRemoveList = this.prevChoice.filter(item => !value.some(value => value.id === item.id));
      this.toAddList = value.filter(item => !this.prevChoice.some(value => value.id === item.id));
    });
   }

  closeDialog() {
    this.dialogRef.close(false);
  }

  saveDashboard() {
    const removedData = this.dashboards.filter(item => this.toRemoveList.some(record => record.id === item.metadata.uid));
    removedData.forEach(report => {
      const indexOfRecord = report.spec.layout.findIndex(record => record.reportType === this.data.item.reportType && (!this.data.item.id || record.id === this.data.item.id));
      if (indexOfRecord !== -1) {
        report.spec.layout = report.spec.layout.filter((item, index) => index !== indexOfRecord);
      }
    });

    const addedData = this.dashboards.filter(item => this.toAddList.some(record => record.id === item.metadata.uid));
    addedData.forEach(report => {
      const indexOfRecord = report.spec.layout.findIndex(record => record.reportType === this.data.item.reportType && (!this.data.item.id || record.id === this.data.item.id));
      if (indexOfRecord === -1) {
        report.spec.layout.push(this.data.item);
      }
    });

    combineLatest([...removedData, ...addedData].map((report: any) => this.setLayoutItem(report))).pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
      this._noticeService.openSnackBar('Added to reports successfully', '');
      this.dialogRef.close(false);
    },
      () => {
        this._noticeService.openSnackBar(`Failed added to report`, '');
      });
  }

  setLayoutItem(report) {
    
    const layouts = report.spec.layout;

    report = {
      ...report,
      spec: {
        layout: JSON.stringify(layouts)
      }
    }
    return this._dashboardService.saveDashboard(report.metadata.name, report, this._route.snapshot.queryParams["loc"]).pipe(takeUntil(this._unsubscribeAll));
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
