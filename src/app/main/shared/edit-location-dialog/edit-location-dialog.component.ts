import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Location, Appearance, GermanAddress } from '@angular-material-extensions/google-maps-autocomplete';
// import {} from '@types/googlemaps';
import PlaceResult = google.maps.places.PlaceResult;
/**
 * @title Dialog Overview
 */
@Component({
    selector: 'edit-location-dialog',
    templateUrl: 'edit-location-dialog.component.html',
    styleUrls: ['edit-location-dialog.component.scss'],
})
export class LocationEditDialogComponent implements OnInit {
    public form: FormGroup;
    latitude: any;
    longitude: any;
    canSave: boolean;
    address: string;

    selected: any;
    parentList: string[];
    type: string;
    show: boolean;

    @ViewChild('addressAuto', {static: true}) addressAuto:any;
    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        private formBuilder: FormBuilder,
        public dialogRef: MatDialogRef<LocationEditDialogComponent>) {
        this.latitude = '';
        this.longitude = '';
        this.canSave = false;
        this.address = '';
        this.form = this.formBuilder.group({
            name: data && data.name ? data.name : '',
            address: data && data.address ? data.address : '',
            addressAuto: '',
            latitude: data && data.latitude ? data.latitude : '',
            longitude: data && data.longitude ? data.longitude : '',
            selected: this.selected,
            selectedParent: data && data.selectedParent ? data.selectedParent : '',
        });
        this.parentList = data && data.parentList ? data.parentList : [];
        this.selected = data.selectedParent;
        this.type = data && data.type ? data.type : 'add';
        this.show = true;
    }

    ngOnInit() {
    }

    closeDialog() {
        this.dialogRef.close(null);
    }
    onAutocompleteSelected(result: PlaceResult) {
        console.log('onAutocompleteSelected: ', result);
        this.canSave = true;
        this.address = result.formatted_address;
        this.form.get('address').setValue(this.address);
        this.show = true;
    }

    onLocationSelected(location: Location) {
        console.log(location);
        this.latitude = location.latitude;
        this.longitude = location.longitude;
        this.form.get('latitude').setValue(this.latitude);
        this.form.get('longitude').setValue(this.longitude);
    }
    onFocus(){
        this.show = false;
        // this.addressAuto.nativeElement.focus();
    }
}
