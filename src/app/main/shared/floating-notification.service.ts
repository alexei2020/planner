import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { FloatingNotificationModel } from './floating-notification.model';

@Injectable({ providedIn: 'root' })
export class FloatingNotificationService {
    private subject = new Subject<FloatingNotificationModel>();

    constructor() {
    }

    sendMessage(messageObj: FloatingNotificationModel) {
        this.subject.next(messageObj);
         setTimeout(() => {
             this.clearMessages();
         }, 5000);
    }

    clearMessages() {
        this.subject.next();
    }

    getMessage(): Observable<FloatingNotificationModel> {
        return this.subject.asObservable();
    }
}
