import {
  Component,
  OnInit,
  Input,
  ElementRef,
  Renderer2,
  AfterViewInit,
  Output,
  EventEmitter,
  OnDestroy,
  ContentChildren,
  QueryList
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { StackEffects } from './stack-effects';
import { StackItemComponent } from './stack-item.component';

interface EffectItem {
  index: number;
  element: HTMLElement;
  position: number;
  clicks: number;
  clickTimeout: number;
}

@Component({
  selector: 'se-container',
  templateUrl: './stack-effects.component.html',
  styleUrls: ['./stack-effects.component.scss']
})
export class StackEffectsComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input()
  effect = StackEffects.SimpleSpread;

  @Input()
  width = `300px`;

  @Input()
  height = `200px`;

  @Input()
  selectionCount = 2;

  private _active = true;

  @Input()
  set activate(value: boolean) {
    console.log(value);
    this._active = value;

    if (!this.elements.length) {
      return;
    }

    if (value) {
      this.active();
    } else {
      this.deactive();
    }
  }

  @Output()
  select = new EventEmitter();

  @Output()
  dblclick = new EventEmitter();

  @Output()
  animationEnded = new EventEmitter();

  @ContentChildren(StackItemComponent)
  children: QueryList<StackItemComponent>;

  elements: EffectItem[] = [];
  currentSelectIndex = 0;
  selectedIndexes: number[] = [];

  private _ngUnsubscribe = new Subject();

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    this.renderer.setStyle(this.elementRef.nativeElement, 'width', this.width);
    this.renderer.setStyle(this.elementRef.nativeElement, 'height', this.height);
  }

  ngOnDestroy() {
    this._ngUnsubscribe.next();
    this._ngUnsubscribe.complete();
  }

  ngAfterViewInit() {
    this.children.changes
      .pipe(takeUntil(this._ngUnsubscribe))
      .subscribe(() => {
        this.initElements();
      });

    this.initElements();
  }

  initElements() {
    let itemTimeout = null;

    setTimeout(() => {
      this.elements = [];
      this.selectedIndexes = [];
      this.currentSelectIndex = 0;
      this.select.emit([]);
      const items = this.elementRef.nativeElement.getElementsByClassName('nse-item');

      for (let i = 0; i < items.length; i++) {
        const element: HTMLElement = items.item(i);
        const item: EffectItem = {
          element,
          index: i,
          position: i,
          clicks: 0,
          clickTimeout: null
        };
        this.elements.push(item);
        element.addEventListener('click', (e) => {
          e.preventDefault();
          clearTimeout(itemTimeout);
          itemTimeout = setTimeout(() => {
            if (!item.clicks) {
              this.selectItem(i);
            }
            item.clicks = 0;
          }, 200);
        });
        element.addEventListener('dblclick', (e) => {
          e.preventDefault();
          clearTimeout(itemTimeout);
          item.clicks = 1;

          if (this.selectedIndexes.includes(i)) {
            this.dblclick.emit(this.selectedIndexes);
          } else {
            this.dblclick.emit([i]);
          }
          this.deactive();
        });
      }
      this.initializeStyles();

      if (this._active) {
        this.active();
      }
    });
  }

  setStyleOnSelectedItems() {
    this.elements.forEach((el, index) => {
      if (this.selectedIndexes.includes(index)) {
        this.renderer.addClass(el.element, 'nse-selected');
      } else {
        this.renderer.removeClass(el.element, 'nse-selected');
      }
    });
  }

  selectItem(index: number) {
    this.currentSelectIndex = index;

    if (this.selectedIndexes.includes(index)) {
      this.selectedIndexes.splice(this.selectedIndexes.indexOf(index), 1);
    } else if (this.selectedIndexes.length < this.selectionCount) {
      this.selectedIndexes.push(index);
    }

    this.active();
    this.setStyleOnSelectedItems();
    this.select.emit(this.selectedIndexes);
  }

  initializeStyles() {
    switch (this.effect) {
      case StackEffects.SimpleSpread:
        this.elements.forEach((el, index) => {
          this.renderer.setStyle(el.element, 'z-index', 99 - index);
        });
        break;
    }
  }

  deactive() {
    switch (this.effect) {
      case StackEffects.SimpleSpread:
        this.elements.forEach((el, index) => {
          this.setDeactiveStyle(el.element, index);
        });
        break;
    }
    this.callAnimationEnded();
  }

  /**
   * When mouseover
   */
  active() {
    let center = Math.floor(this.elements.length / 2);

    switch (this.effect) {
      case StackEffects.SimpleSpread:
        this.elements.forEach((el, index) => {
          index -= this.currentSelectIndex;

          if (index < 0) {
            index = this.elements.length + index;
          }

          if (index < center) {
            this.setActiveStyle(el.element, center - index, 'left');
          } else if (index > center) {
            this.setActiveStyle(el.element, index - center, 'right');
          } else {
            this.setActiveStyle(el.element, 0, 'center');
          }
          el.position = index;
        });
        break;
    }

    this.callAnimationEnded();
  }

  callAnimationEnded() {
    setTimeout(() => {
      this.animationEnded.emit();
    }, 300)
  }


  /**
   * Set style on an item when it's activated
   */
  setActiveStyle(item: HTMLElement, index: number, d: 'left' | 'right' | 'center' | 'top' | 'bottom', p?) {
    this.renderer.setStyle(item, 'border', '1px solid #ececec');
    this.renderer.setStyle(item, 'opacity', 1);

    switch (this.effect) {
      case StackEffects.SimpleSpread:
        if (d === 'center') {
          this.renderer.setStyle(item, 'left', 0);
          this.renderer.setStyle(item, 'top', 0);
          this.renderer.setStyle(item, 'z-index', 50);
        } else {
          this.renderer.setStyle(item, 'left', `${d === 'left' ? -index * 40 : index * 40}px`);
          this.renderer.setStyle(item, 'top', `${d === 'left' ? index * 40 : index * -40}px`);
          this.renderer.setStyle(item, 'z-index', 50 + (d === 'left' ? index : -index));
        }
        break;
    }
  }

  /**
   * Set style on an item when it's deactivated
   */
  setDeactiveStyle(item: HTMLElement, index: number, d?: 'left' | 'right' | 'center') {
    this.renderer.removeStyle(item, 'box-shadow');

    switch (this.effect) {
      case StackEffects.SimpleSpread:
        this.renderer.setStyle(item, 'top', 0);
        this.renderer.setStyle(item, 'left', 0);
        this.renderer.setStyle(item, 'transform', 'scale(1)');
        break;
    }
  }

  handleCentralizedEffect(activate: boolean = false) {
    const center = Math.floor(this.elements.length / 2);
    for (let i = 1; i <= center; i++) {
      let pos = -i;
      if (pos < 0) { pos = this.elements.length + pos; }
      const item = this.elements[pos];
      item.position = center - i;
      if (activate) {
        this.setActiveStyle(item.element, i, 'left');
      }
      else {
        this.setDeactiveStyle(item.element, i, 'left');
      }
    }
    for (let i = 1; i < this.elements.length - center; i++) {
      let pos = i;
      if (pos >= this.elements.length) { pos = pos - this.elements.length; }
      const item = this.elements[pos];
      item.position = center + i;
      if (activate) {
        this.setActiveStyle(item.element, i, 'right');
      }
      else {
        this.setDeactiveStyle(item.element, i, 'right');
      }
    }
    const selectedItem = this.elements[0];
    selectedItem.position = center;
    this.setDeactiveStyle(selectedItem.element, 0, 'center');
    if (activate) {
      this.setActiveStyle(selectedItem.element, null, 'center');
    }
  }
}
