import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StackEffectsComponent } from './stack-effects.component';
import { StackItemComponent } from './stack-item.component';


@NgModule({
  declarations: [StackEffectsComponent, StackItemComponent],
  imports: [
    CommonModule
  ],
  exports: [StackEffectsComponent, StackItemComponent]
})
export class StackEffectsModule { }
