import { Component, Input } from '@angular/core';
import { DiffResults, DiffTableFormat } from 'ngx-text-diff/lib/ngx-text-diff.model';

@Component({
    selector: 'app-content-diff',
    templateUrl: './content-diff.component.html',
    styleUrls: [
        './content-diff.component.scss'
    ]
})
export class ContentDiffComponent {
    @Input()
    loading = false;

    @Input()
    failed = false;

    @Input()
    supported = true;

    @Input()
    left = '';

    @Input()
    right = '';

    format: DiffTableFormat = 'LineByLine';
    hideMatchingLines = false;
    diffsCount = 0;

    onCompareResults(diff: DiffResults): void {
        this.diffsCount = diff.diffsCount;
    }
}
