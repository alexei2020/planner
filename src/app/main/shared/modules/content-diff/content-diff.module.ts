import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxTextDiffModule } from 'ngx-text-diff';
import { InputsModule } from '@progress/kendo-angular-inputs';

import { ContentDiffComponent } from './content-diff.component';

@NgModule({
    declarations: [
        ContentDiffComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        FlexLayoutModule,
        NgxTextDiffModule,
        InputsModule
    ],
    exports: [
        ContentDiffComponent
    ]
})
export class ContentDiffModule { }
