import EChartOption = echarts.EChartOption;
import SeriesBar = echarts.EChartOption.SeriesBar;

export class BarChartLayoutOptions implements EChartOption<SeriesBar> {
    color: any;
    legend: any;
    tooltip: any;
    grid: any;
    series?: any;
}
export class BarChartSeriesOptions implements EChartOption<SeriesBar> {
    legend: any;
    xAxis: any;
    yAxis: any;
    series: any;
    animationEasing?: any;
    animationDelayUpdate?: any;
    color?: string[];
}
