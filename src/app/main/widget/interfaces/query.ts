export interface Query {
    apiVersion?: string;
    kind?: string;
    metadata?: {
        creationTimestamp?: string;
        generation?: number;
        name?: string;
        resourceVersion?: string;
        selfLink?: string;
        uid?: string;
    };
    spec?: {
        query_info?: {
            chart_url?: string;
            total_url?: string;
            all_data_url?: string;
            filtered_data_url?: string;
            prev_data_url?: string;
            prev_url?: string;
            base_url?: string;
            param?: string;
            port?: number;
            query_category?: string;
            x_axis_label?: string;
            title?: string;
            units?: string;
            total_label?: string;
            columns?: QueryColumn[];
            display_label?: string;
        };
        category?: string;
    };
}
export interface QueryColumn {
    data_type?: string;
    label?: string;
    query?: string;
    render_type?: string;
    title?: string;
    units?: string;
}
