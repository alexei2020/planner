import { GridsterItem } from 'angular-gridster2';
import {Query} from './query';

export interface Widget extends GridsterItem {
    id?: string;
    title?: string;
    query?: Array<Query>;
    type?: ChartType;
    chartOptions?: any;
    chartType?: string;
    chartVariant?: string;
    chartQueries?: ChartQueryItem[];
    datapointCount?: number;
    privilege?: string;
    colorPalette?: string;
    opacity?: number;
}

export interface ChartType {
    apiVersion?: string;
    kind?: string;
    metadata?: {
        creationTimestamp?: string;
        generation?: number;
        name?: string;
        resourceVersion?: string;
        selfLink?: string;
        uid?: string;
    };
    spec?: {
        chartInfo?: string;
        category?: any;
        title?: any;
        max_queries?: any;
        detail_view_datapoint_count?: any;
        panel_datapoint_count?: any;
    };
}

export interface ChartQueryItem {
    query: Query;
    color?: string;
    showOnAxis?: boolean;
}
export interface ChartOptions {
    type: string;
    variant?: string;
    queries: ChartQueryItem[];
    colorPalette: string[];
    datapointCount?: number;
    xAxis?: string[];
    yAxis?: string[];
}

