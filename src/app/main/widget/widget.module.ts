import { GridModule, PDFModule, ExcelModule, BodyModule, SharedModule as SharedModuleKendo} from '@progress/kendo-angular-grid';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { LineDoubleChartComponent } from './components/line-chart-double/line-chart-double.component';

import { TableComponent } from './components/table/table.component';
import { TableBarComponent } from './components/table/table-bar';
import { TablePieComponent } from './components/table/table-pie';
import { TableSummaryComponent } from './components/table/table-summary';
import { TableSummaryChartComponent } from './components/table/table-summary-chart';

import { GaugeModule } from 'angular-gauge';

import { KendoTableComponent } from './components/kendo-table/kendo-table.component';

import { InputsModule } from '@progress/kendo-angular-inputs';
import { PopupModule } from '@progress/kendo-angular-popup';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { UploadsModule } from '@progress/kendo-angular-upload';
import { TooltipModule } from '@progress/kendo-angular-tooltip';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { ScrollViewModule } from '@progress/kendo-angular-scrollview';


import 'hammerjs';

import { SharedChartWidget } from './components/shared-chart-widget/shared-chart-widget.component';
import { PanelHeaderComponent } from '../apps/dashboards/components/panel-header/panel-header.component';
import { SharedChartHeaderWidgetComponent } from './components/shared-chart-header-widget/shared-chart-header-widget.component';
import { SharedTableHeaderWidgetComponent } from './components/shared-table-header-widget/shared-table-header-widget.component';
import {ChartsModule} from '@progress/kendo-angular-charts';
import { SharedTableViewHeaderWidgetComponent } from './components/shared-table-view-header-widget/shared-table-view-header-widget.component';
import { BarChartComponent } from './components/charts/bar-chart/bar-chart.component';
import { GraphChartComponent } from './components/charts/graph-chart/graph-chart.component';
import { PieChartComponent } from './components/charts/pie-chart/pie-chart.component';
import { GaugeChartComponent } from './components/charts/gauge-chart/gauge-chart.component';
import { ScatterChartComponent } from './components/charts/scatter-chart/scatter-chart.component';
import { SummaryChartComponent } from './components/charts/summary-chart/summary-chart.component';
import { TableChartComponent } from './components/charts/table-chart/table-chart.component';
import { SummaryItemComponent } from './components/charts/summary-chart/summary-item/summary-item.component';
import { ButtonModule } from '@progress/kendo-angular-buttons';

const COMPONENTS = [
    LineDoubleChartComponent,
    TableComponent,
    TableBarComponent,
    TablePieComponent,
    TableSummaryComponent,
    TableSummaryChartComponent,
    TablePieComponent,
    KendoTableComponent,
    SharedChartWidget,
    SharedChartHeaderWidgetComponent,
    SharedTableHeaderWidgetComponent,
    SharedTableViewHeaderWidgetComponent,
    PanelHeaderComponent,
    // CHARTS
    BarChartComponent,
    GraphChartComponent,
    PieChartComponent,
    GaugeChartComponent,
    ScatterChartComponent,
    SummaryItemComponent,
    SummaryChartComponent,
    TableChartComponent
];
@NgModule({
    declarations: [ ...COMPONENTS ],
    imports: [
        SharedModule,
        SharedModuleKendo,
        BodyModule,
        GaugeModule.forRoot(),
        GridModule,
        PDFModule,
        ExcelModule,
        InputsModule,
        PopupModule,
        TooltipModule,
        LayoutModule,
        ChartsModule,
        ScrollViewModule,
        UploadsModule,
        DropDownsModule,
        ButtonModule
    ],
    exports: [ ...COMPONENTS ]
})
export class WidgetModule {}
