import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class DatasourceService {
	//prometheus endpoint
	url: string = `api/v1/query_range?query=`;
	constructor(private http: HttpClient) {}

	getMetric(query) {
		return this.http.get(`${this.url}${query}`);
	}
}
