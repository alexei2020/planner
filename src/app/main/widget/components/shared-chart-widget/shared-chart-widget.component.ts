import {Component, Input, OnInit, EventEmitter, Output, SimpleChanges, OnChanges, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { Widget } from '../..';

import { LayoutService } from 'app/main/shared/services/layout.service';
import {ManageChartDialogComponent} from '../../../shared/dialogs/manage-chart-dialog/manage-chart-dialog.component';
import {DialogCloseResult, DialogService} from '@progress/kendo-angular-dialog';
import {JsonDialogComponent} from '../../../shared/dialogs/json-dialog/json-dialog.component';
import {BreadcrumbService} from '../../../shared/breadcrumb-bar.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import { KendoConfirmDialog } from '../../../shared/dialogs/kendo-confirm-dialog/kendo-confirm-dialog.component';
import {ChartDataService} from '../../../shared/services/chart-data.service';


@Component({
    selector: 'shared-chart-widget',
    templateUrl: './shared-chart-widget.component.html',
    styleUrls: ['./shared-chart-widget.component.scss']
})
export class SharedChartWidget implements OnInit, OnChanges, OnDestroy {

    @Input() public item: Widget;
    @Input() public index: any = 1;
    @Input() public options: any;
    @Input() public isViewPanel = false;

    @Input() public period;
    @Input() public refreshTimer;
    @Input() public interval;

    @Output() editWidgetChange = new EventEmitter();
    @Output() removeWidgetChange = new EventEmitter();
    @Output() filterChange = new EventEmitter();

    public chartVariant: string;
    public chartOptions: any[];
    public colorPalette: any[];
    public noData = false;

    private unsubscribe$: Subject<void> = new Subject<void>();

    constructor(
        private _dialog: MatDialog,
        private _dialogService: DialogService,
        private _router: Router,
        private _layoutService: LayoutService,
        private _breadcrumbService: BreadcrumbService,
        private _chartDataService: ChartDataService
    ) { }

    ngOnInit(): void {
        // If no data fetched show NO DATA
        this._chartDataService.noDataFetched$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((widget: Widget) => {
                if (this.item.id === widget.id){
                    this.noData = true;
                }
            });
        // If data is fetched don't show NO DATA
        this._chartDataService.dataFetched$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(({widget, data}) => {
                if (this.item.id === widget.id){
                    this.noData = false;
                }
            });
    }

    ngOnChanges(changes: SimpleChanges): void { }

    public removeWidget(widget: Widget): void {
        const dialogRef = this._dialogService.open({
            title: 'Remove Widget',
            content: KendoConfirmDialog,
            width: '390px',
            height: '200px',
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.message = 'Are you sure you want to remove widget?';
        dialogInstance.dialog = dialogRef;
        dialogRef.result.pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
            if (res instanceof DialogCloseResult || res.text === 'No') {
                return;
            }
            this.removeWidgetChange.emit(widget.id);
        });
    }

    public jsonWidget(widget: Widget, index): void {
        const dialogRef = this._dialogService.open({
            title: 'Chart JSON',
            content: JsonDialogComponent,
            width: '80%',
            height: '400px',
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.dialog = dialogRef;
        dialogInstance.widget = this.item;

        dialogRef.result.pipe(takeUntil(this.unsubscribe$)).subscribe((result) => {
            console.log(result);
            if (result instanceof DialogCloseResult) {

            } else {
                // create widget object and emit it
                this.editWidgetChange.emit(result);
            }
        });
    }

    public viewWidget(widget: Widget): void {
        this._layoutService.setSelectedItemObs({ ...widget });
        this._layoutService.getSelectedItemObs()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((result) => {
                console.log(result);
            });
        this._router.navigate(['/dashboard', widget.id], { queryParamsHandling: 'preserve' })
            .then((result) => {
                if (result) {

                }
            });
    }

    public editWidget(widget: Widget): void {
        const dialogRef = this._dialogService.open({
            // title: 'Edit chart options',

            // Show component
            content: ManageChartDialogComponent,

            width: '80%',
            height: '70%',
            preventAction: (ev, dialog) => {
                if (ev instanceof DialogCloseResult || !ev.primary) {
                    return false;
                }
                return !dialog.content.instance.widgetForm.valid;
            }
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.dialog = dialogRef;
        dialogInstance.widget = widget;
        dialogInstance.actionLabel1 = 'Cancel';
        dialogInstance.actionLabel2 = 'Update chart';
        dialogInstance.populateForEdit(widget);

        dialogRef.result.pipe(takeUntil(this.unsubscribe$)).subscribe((result) => {
            if (result instanceof DialogCloseResult || result.text === 'Cancel') {
                console.log('close');
            } else {
                console.log('dialog result => ', result);
                let widget: Widget = {
                    x: 0, y: 0, rows: 12, cols: 48
                };
                widget = dialogInstance.widget;
                const availableQueries = dialogInstance.availableQueries;
                const queries = dialogInstance.widgetForm.value.queries;
                widget.id = dialogInstance.widgetForm.value.id;
                widget.title = dialogInstance.widgetForm.value.title;
                widget.chartType = dialogInstance.widgetForm.value.type;
                widget.chartVariant = dialogInstance.widgetForm.value.variant;
                widget.chartQueries = [];
                widget.datapointCount = dialogInstance.datapointCount;
                widget.privilege = dialogInstance.widgetForm.value.privilege;
                widget.colorPalette = dialogInstance.widgetForm.value.colorPalette;
                widget.opacity = dialogInstance.widgetForm.value.opacity;

                queries.forEach((query) => {
                    widget.chartQueries.push({color: query.color, showOnAxis: query.showOnAxis, query: availableQueries.find((option) => option.metadata.name === query.query)});
                });
                this.item = {...widget};
                this.editWidgetChange.emit(widget);
            }
        });
    }

    public onCustomFilterByType(event): void {
        const filter = event && event.filters ? event.filters : null;
        this.filterChange.emit(filter);
    }

    public onFilterByType(event): void {
        this.filterChange.emit(event);
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}

