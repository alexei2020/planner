import { Component, ViewChild, Input, Output, EventEmitter, OnInit, SimpleChanges } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { BehaviorSubject } from 'rxjs';
import { BreadcrumbService } from 'app/main/shared/breadcrumb-bar.service';

@Component({
	selector: 'shared-chart-header-widget',
	templateUrl: './shared-chart-header-widget.component.html',
	styleUrls: ['./shared-chart-header-widget.component.scss']
})
export class SharedChartHeaderWidgetComponent implements OnInit {
	@ViewChild(MatMenuTrigger) private menuTrigger: MatMenuTrigger;

	trigger: MatMenuTrigger;
	menuToggle: boolean;
	detailView: boolean;

	@Input() widget: any;


	@Input() refreshTime: any = { label: 'Off', value: 'Off' };
	@Input() selectedRange: any = { label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 };

	@Input() isMenu = true;
	@Input() isView = true;
	@Input() isJsonPanel = true;
	@Input() isRemove = true;
	@Input() isReturn = false;
	@Input() isLock = true;
	@Input() isFixedPosition = true;

	@Output() view: any = new EventEmitter();
	@Output() panelJson: any = new EventEmitter();
	@Output() remove: any = new EventEmitter();
	@Output() return: any = new EventEmitter();



	@Output() rangeSelection: any = new EventEmitter(true);
	@Output() refreshSelection: any = new EventEmitter(true);
	@Output() refreshIntervalSelection: any = new EventEmitter(true);
	@Output() gridsterStateChanged: any = new EventEmitter();

	@Output() selectedRangeObs: any = new BehaviorSubject({ label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 });
	@Output() refreshTimeObs: any = new BehaviorSubject(null);

	public refreshList = [
		{ label: 'Off', value: 'Off' },
		{ label: '5s', value: 5000 },
		{ label: '10s', value: 10000 },
		{ label: '30s', value: 30000 },
		{ label: '1m', value: 60000 },
		{ label: '5m', value: 300000 },
		{ label: '15m', value: 900000 },
		{ label: '30m', value: 1800000 },
		{ label: '1h', value: 3600000 },
		{ label: '2h', value: 7200000 },
		{ label: '1d', value: 86400000 }
	];
	public rangeList = [
		{ label: 'Last 5 minutes', short: '5m', start: 0, end: 0, step: 5 },
		{ label: 'Last 15 minutes', short: '15m', start: 0, end: 0, step: 15 },
		{ label: 'Last 30 minutes', short: '30m', start: 0, end: 0, step: 30 },
		{ label: 'Last 1 hour', short: '1h', start: 0, end: 0, step: 60 },
		{ label: 'Last 3 hours', short: '3h', start: 0, end: 0, step: 180 },
		{ label: 'Last 6 hours', short: '6h', start: 0, end: 0, step: 360 },
		{ label: 'Last 12 hours', short: '12h', start: 0, end: 0, step: 720 },
		{ label: 'Last 24 hours', short: '24h', start: 0, end: 0, step: 1440 },
		{ label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 }
	];
	public refreshToggle = false;
	public refreshDashboard = false;

	constructor(private _breadcrumbService: BreadcrumbService) {
		this.selectedRangeObs.next(this._breadcrumbService.setRange(this.selectedRange));
	}

	ngOnInit() {
		// console.log(this.isJsonPanel)
	 }

	ngOnChanges(changes: SimpleChanges) {
		// console.log(changes);
	}

	open() {
		this.trigger.openMenu();
	}

	updateRange(range: any) {
		this.selectedRange = this._breadcrumbService.setRange(range);
		this.selectedRangeObs.next(this._breadcrumbService.setRange(range));
		this.rangeSelection.emit(this._breadcrumbService.setRange(range));
	}

	changeGridsterState(widget?: any) {
        if (widget) {
            widget.resizeEnabled = !widget.resizeEnabled;
            widget.dragEnabled = !widget.dragEnabled;
        }
		this.gridsterStateChanged.emit(widget.resizeEnabled);
	}

	refreshToggleClicked() {
		this.refreshToggle == !this.refreshToggle;
	}

	onRefresh() {
		this.refreshTime = this.refreshList[0];
		this.refreshTimeObs.next(this.refreshList[0].value);
	}

	refreshTimeSelected(time) {
		this.refreshTime = time;
		this.refreshTimeObs.next(time.value)
	}

	onRemove() {
		this.remove.emit(this.widget);
	}
}
