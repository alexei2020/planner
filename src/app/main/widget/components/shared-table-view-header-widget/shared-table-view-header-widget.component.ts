import {Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { KendoReportItemDialog } from "../../../shared/dialogs/kendo-report-item-dialog/kendo-report-item-dialog.component";
import { KendoDashboardItemDialog } from "../../../shared/dialogs/kendo-dashboard-item-dialog/kendo-dashboard-item-dialog.component";
import {
  DialogCloseResult,
  DialogService,
  DialogResult,
} from "@progress/kendo-angular-dialog";

import {TableConfigService} from '../../../shared/services/table-config.service';
import {Widget} from '../..';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-shared-table-view-header-widget',
  templateUrl: './shared-table-view-header-widget.component.html',
  styleUrls: ['./shared-table-view-header-widget.component.scss']
})
export class SharedTableViewHeaderWidgetComponent implements OnInit, OnDestroy, OnChanges {
    @Input() item: any;
    @Input() widget: Widget;
    @Input() isControlPanel?: any;
    @Input() index: any;
    @Input() resizeable?: any;

    public pageSize = { label: '10', value: 10 };
    public pageSizes = [{ label: '5', value: 5 }, { label: '10', value: 10 }, { label: '20', value: 20 }];
    public groupable = false;
    public height = 0;

    public searchText = null;

    private unsubscribe$: Subject<void> = new Subject<void>();
    
    constructor(
      private _dialogService: DialogService,
      private el: ElementRef,
      private tableConfigService: TableConfigService
    ) { }

    ngOnInit(): void {
        setTimeout(() => {
            this.height = this.el.nativeElement.offsetHeight;
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        console.log(changes)
        if (changes.resizeble && changes.resizeble.currentValue) {
            this.height = this.el.nativeElement.offsetHeight;
        }
    }

    onAddToReport(): void {
      const dialogRef = this._dialogService.open({
        title: "Show in reports",
        content: KendoReportItemDialog,
        width: "300px",
        height: "200px",
      });
      const dialogInstance = dialogRef.content.instance;
      dialogInstance.dialog = dialogRef;
      dialogInstance.data = {
        title: "Show in reports",
        item: this.item,
      };
      dialogRef.result.pipe(takeUntil(this.unsubscribe$)).subscribe((response: DialogResult) => {});
    }

    onAddToDashboard(): void {
      const dialogRef = this._dialogService.open({
        title: "Show in dashboards",
        content: KendoDashboardItemDialog,
        width: "300px",
        height: "200px",
      });
      const dialogInstance = dialogRef.content.instance;
      dialogInstance.dialog = dialogRef;
      dialogInstance.data = {
        title: "Show in dashboards",
        item: this.item,
      };
      dialogRef.result.pipe(takeUntil(this.unsubscribe$)).subscribe((response: DialogResult) => {});
    }

    onPageSizeSelected(pageSize): void {
        // this.pageSizeSelected.emit(pageSize);
        this.tableConfigService.changePageSize(this.widget, pageSize.value);
    }

    onSearchTextEntered(e): void {
        // console.log(searchText);
        // console.log(this.searchText);
        // if (this.searchText?.length > 2) {
        //     this.searchTextEntered.emit(this.searchText);
        // }
        this.tableConfigService.changeSearchText(this.widget, this.searchText);
    }

    ngOnDestroy(): void {
      this.unsubscribe$?.next();
      this.unsubscribe$?.complete();
  }
}
