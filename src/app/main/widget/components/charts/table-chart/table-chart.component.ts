import {
    ChangeDetectorRef,
    Component,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {DataBindingDirective} from '@progress/kendo-angular-grid';
import {process, SortDescriptor} from '@progress/kendo-data-query';
import {Subject, timer} from 'rxjs';
import {ChartDataService} from '../../../../shared/services/chart-data.service';
import {LayoutService} from '../../../../shared/services';
import {TableConfigService} from '../../../../shared/services/table-config.service';
import {takeUntil} from 'rxjs/operators';
import {GridsterItemComponent} from 'angular-gridster2';
import {FUSE_THEME} from '../../../../theme/default-theme';
import {AbstractChartComponent} from '../abstract-chart.component';
import {FuseLocationService} from '../../../../../services/location.service';

@Component({
  selector: 'app-table-chart',
  templateUrl: './table-chart.component.html',
  styleUrls: ['./table-chart.component.scss']
})
export class TableChartComponent extends AbstractChartComponent implements OnInit, OnChanges, OnDestroy {

    @Input()
    public isViewPanel: boolean;

    @ViewChild(DataBindingDirective) dataBinding: DataBindingDirective;

    public pageSize = 10;
    public isLoading = false;
    public columns: any[];

    public sort: SortDescriptor[];

    public gridData: any[];
    public gridView: any[];
    public gridHeight;

    private filterParameters = {
        startTime: 1596832294,
        endTime: 1597005094,
        duration: '2d',
        step:  2880,
        apigroup: ''
    };
    public startTime = 1596832294;
    public endTime = 1597005094;
    public duration = '2d';
    public step = 15;
    private apigroup: any;


    private unsubscribe$: Subject<void> = new Subject<void>();
    private unsubscribeTimer$: Subject<void> = new Subject<void>();

    constructor(
        private cd: ChangeDetectorRef,
        private chartDataService: ChartDataService,
        private layoutService: LayoutService,
        private tableConfigService: TableConfigService,
        private _locationService: FuseLocationService,
    ) {
        super();

    }

    ngOnInit(): void {
        this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
        this.ngOnChanges(undefined);
        this.getData();

        // AB redraw chart when gridster item resized
        this.layoutService.itemResized$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((gridsterItemComponent: GridsterItemComponent) => {
                if (gridsterItemComponent.item.id === this.widget.id) {
                    // this.chartWidth = gridsterItemComponent.width;
                    this.gridHeight = gridsterItemComponent.height - 60;
                }
            });

        this.columns = this.widget.chartQueries[0].query.spec.query_info.columns;
        this.sort = [
            {
                field: this.columns[0].label,
                dir: 'asc'
            }
        ];

        this.chartDataService.dataFetched$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((dataFetched) => {
                if (this.widget.id === dataFetched.widget.id) {
                    this.isLoading = false;
                    this.gridData = dataFetched.data;
                    this.gridView = this.gridData;
                }
            });

        this.tableConfigService.pageSizeChanged$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((e) => {
                if (e.widget.id === this.widget.id) {
                    this.pageSize = e.pageSize;
                }
            });
        this.tableConfigService.searchTextChanged$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((e) => {
                if (e.widget.id === this.widget.id) {
                    this.filterData(e.searchText);
                }
            });

        this.gridView = this.gridData;
    }

    ngOnChanges(changes: SimpleChanges): void {
        let loadData = false;

        if (changes && changes.widget && changes.widget.isFirstChange()) {
            return;
        }

        if (this.widget && this.duration) {
            loadData = true;
        }

        if (changes && changes.selectedRange && changes.selectedRange.currentValue) {
            this.startTime = changes.selectedRange.currentValue.start;
            this.endTime = changes.selectedRange.currentValue.end;
            this.duration = changes.selectedRange.currentValue.short;
            this.step = Math.round(
                (changes.selectedRange.currentValue.end -
                    changes.selectedRange.currentValue.start) /
                this.widget.datapointCount
            );
            loadData = true;
        }

        if (changes &&
            changes.refreshTime &&
            changes.refreshTime.currentValue !== undefined
        ) {
            loadData = true;
        }

        if (changes && changes.interval && changes.interval.currentValue) {
            this.unsubscribeTimer$.next();
            if (
                changes.interval.currentValue &&
                typeof changes.interval.currentValue.value === 'number'
            ) {
                const setInterval = timer(
                    0,
                    changes.interval.currentValue.value
                );
                setInterval
                    .pipe(takeUntil(this.unsubscribeTimer$))
                    .subscribe(() => {
                        this.getData();
                    });
            } else {
                this.unsubscribeTimer$.next();
            }
        }

        // TODO: refactor this
        this.filterParameters.startTime = this.startTime;
        this.filterParameters.endTime = this.endTime;
        this.filterParameters.step = this.step;
        this.filterParameters.duration = this.duration;
        this.filterParameters.apigroup = this.apigroup;

        if (loadData && this.selectedRange) {
            this.getData();
        }
        this.cd.detectChanges();
    }


    getData(): void {
        this.isLoading = true;

        this.filterParameters.startTime = this.selectedRange.start;
        this.filterParameters.endTime = this.selectedRange.end;
        this.filterParameters.step = Math.round((this.selectedRange.end -
          this.selectedRange.start) / this.widget.datapointCount);
        this.filterParameters.duration = this.selectedRange.short;
        this.filterParameters.apigroup = this.apigroup;
        this.chartDataService.getTableChartData(this.widget, this.filterParameters);
        // console.log('GET DATA');
    }

    getProgressBarColor(value): string {
        if (value <= 50) {
            return 'green';
        } else if (value > 50 && value <= 70) {
            return FUSE_THEME.variables.darkorange;
        } else if (value > 70) {
            return 'red';
        }
    }

    public gridDataChanged(event): void {
        // console.log('Grid data changed', event);
    }

    // public sortChanged(sort: SortDescriptor[]): void {
    //     this.sort = sort;
    //     this.loadData();
    // }
    //
    // private loadData(): void {
    //   this.gridView = {
    //       data: orderBy(this.gridData, this.sort),
    //       total: this.gridData.length
    //   };
    // }

    filterData(searchText: string): void {
        const columnFilters = this.columns.map((col) => {
            if (!col.label) {
                return {
                    field: 'value',
                    operator: 'contains',
                    value: searchText
                };
            }
            return {
                field: col.label,
                operator: 'contains',
                value: searchText
            };
        });

        this.gridView = process(this.gridData, {
            filter: {
                logic: 'or',
                filters: columnFilters
            }
        }).data;
        this.dataBinding.skip = 0;
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

}
