import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output, SimpleChanges
} from '@angular/core';
import {Widget} from '../../..';
import {ECharts} from 'echarts';
import {BarChartLayoutOptions, BarChartSeriesOptions} from '../../../interfaces/barChartOptions';
import {Query} from '../../../interfaces/query';
import {Subject, timer} from 'rxjs';
import {ConfigService} from '../../../../services/config.service';
import {LayoutService, PanelService} from '../../../../shared/services';
import {ChartDataService} from '../../../../shared/services/chart-data.service';
import {takeUntil} from 'rxjs/operators';
import {AbstractChartComponent} from '../abstract-chart.component';
import {FuseLocationService} from '../../../../../services/location.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent extends AbstractChartComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
    @Input()
    public widget: Widget;

    @Input()
    public index: any;

    @Input('reset')
    set reset(data: boolean) {
        if (data) {
            this.getData();
        }
    }

    @Output()
    filter: EventEmitter<any> = new EventEmitter();

    public startTime: any = 1581722395;
    public endTime: any = 1581723395;
    public duration: any = 1581722395;
    public step: any = 15;
    public url: any;
    public echartsInstance: ECharts;
    public themeSubscription: any;
    public options: any = {};
    public colors: any;
    public echarts: any;
    public pending: boolean;
    public fielName: string;
    public unitType: any;
    public apigroup: any;

    public chartData: BarChartSeriesOptions;
    public percentArray = [];
    public dataTotal = 0;

    private noOfQueries: number;
    private widgetQueries: Query[];
    private filterParameters = {
        startTime: 1581722395,
        endTime: 1581722395,
        duration: 1581722395,
        step:  15,
        apigroup: ''
    };

    private chartInitialized: Subject<ECharts> = new Subject<ECharts>();
    private chartLayoutSet: Subject<BarChartLayoutOptions> = new Subject<BarChartLayoutOptions>();
    private chartDataFetched: Subject<any[]> = new Subject<any[]>();

    private unsubscribe$: Subject<void> = new Subject<void>();
    private unsubscribeTimer$: Subject<void> = new Subject<void>();

    constructor(
        private configSvc: ConfigService,
        private cd: ChangeDetectorRef,
        private panelService: PanelService,
        private layoutService: LayoutService,
        private chartDataService: ChartDataService,
        private _locationService: FuseLocationService,
    ) {
        super();

    }
    ngOnInit(): void {
        this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
        this.ngOnChanges(undefined);
        this.pending = false;

        // When chart is initialized, init the layout
        this.chartInitialized
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((e: ECharts) => {
                this.getData();
            });
        // When data is fetched and formatted, update data on charts
        this.chartDataFetched
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartDataSeries: any[]) => {
                const seriesData = [];
                const legend = { data: []};
                this.percentArray = [];
                this.dataTotal = 0;

                this.resetChartData(this.echartsInstance);
                chartDataSeries.forEach((cd) => {
                    cd.data.series.forEach((s) => {
                        legend.data.push(s.name);
                        seriesData.push({value: s.data[0], name: s.name});
                        this.percentArray.push({value: s.data[0], name: s.name});
                        this.dataTotal += parseFloat(s.data[0]);
                    });
                });
                this.chartData.series = {
                    type: 'pie',
                    radius: '50%',
                    center: ['50%', '50%']
                };

                this.chartData.series.data = seriesData;
                this.chartData.legend = legend;

                setTimeout(() => {
                    this.echartsInstance.setOption(this.chartData);
                    this.setChartLayout();
                }, 10);
            });
        // Set the layout
        this.chartLayoutSet
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((layoutOptions: BarChartLayoutOptions) => {
                this.pending = false;
                this.renderChart(layoutOptions);
            });

        // When data is fetched from URL format the series
        this.chartDataService.dataFetched$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id){
                    this.pending = false;
                    this.chartDataService.getSeriesFromResults(this.widget, chartObject.data);
                }
            });

        // When series are formatted set the data on chart
        this.chartDataService.seriesFormatted$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id) {
                    this.chartDataFetched.next(chartObject.data);
                }
            });

    }

    ngOnChanges(changes: SimpleChanges): void {
        let loadData = false;
        if (changes?.widget && changes.widget.isFirstChange()) {
            return;
        }
        if (this.widget && this.duration) {
            loadData = true;
        }

        if (changes && changes.selectedRange && changes.selectedRange.currentValue) {
            this.duration = changes.selectedRange.currentValue.short;
            this.step = Math.round((changes.selectedRange.currentValue.end - changes.selectedRange.currentValue.start) / this.widget.datapointCount);
            loadData = true;
        }

        if (changes && changes.refreshTime && changes.refreshTime.currentValue !== undefined) {
            loadData = true;
        }

        if (changes && changes.interval && changes.interval.currentValue) {
            this.unsubscribeTimer$.next();
            if (changes.interval.currentValue && typeof changes.interval.currentValue.value === 'number') {
                const setInterval = timer(0, changes.interval.currentValue.value);
                setInterval.pipe(takeUntil(this.unsubscribeTimer$)).subscribe(() => this.getData());
            } else {
                this.unsubscribeTimer$.next();
            }
        }

        // TODO: refactor this
        this.filterParameters.startTime = this.startTime;
        this.filterParameters.endTime = this.endTime;
        this.filterParameters.step = this.step;
        this.filterParameters.duration = this.duration;
        this.filterParameters.apigroup = this.apigroup;

        if (loadData) {
            this.getData();
        }
        this.cd.detectChanges();
    }
    ngAfterViewInit(): void {
        this.cd.detectChanges();
    }

    onChartInit(e: ECharts): void {
        this.echartsInstance = e;
        this.chartInitialized.next(e);
    }

    onChartEvent(event: any): void {
        const data = {};
        data['name'] = this.fielName;
        data['filters'] = event['selected'];
        this.filter.emit(data);
    }

    setChartLayout(): void {
        const options = this.layoutService.getChartLayout(this.widget);
        this.percentArray.forEach((p) => {
            p.percent = p.value / this.dataTotal * 100;
        });
        // Format legend
        options.legend.formatter = (name) => {
            const index = this.percentArray.findIndex((el) => {
                return el.name === name;
            });
            if (index > -1) {
                return this.percentArray[index].percent?.toFixed(0) + ' %';
            }
            // let newName;
            // if (name.length > 5) {
            //     newName = name.substr(0, 3) + '...';
            //     return newName;
            // }
            // return name;

        };
        options.legend.tooltip = {
            show: true,
            trigger: 'item',
            formatter: (a) => {
                return a.name;
            }
        };
        options.tooltip = {
            trigger: 'item',
            formatter: (a) => {
                return a.marker + ' ' + a.data.name + ': ' + this.chartDataService.getFormattedValue(a.data.value, this.widget.chartQueries[0].query.spec.query_info.units)
                    + ' (' + a.percent + '%)';
            }
        };
        options.series = this.chartData.series;
        options.series.label = {
               color: '#949BB9',
               fontFamily: 'IBM Plex Sans'};
        options.series.itemStyle = {
            // opacity: this.widget.opacity,

        };
        // options.series.emphasis = {
        //     itemStyle: {
        //         color: this.layoutService.removeAlpha(options.color[0])
        //     }
        // };
        options.grid.left = 0;
        options.grid.right = 0;
        options.grid.bottom = 0;

        this.chartLayoutSet.next(options);
    }

    getData(): void {
        this.filterParameters.startTime = this.selectedRange.start;
        this.filterParameters.endTime = this.selectedRange.end;
        this.filterParameters.step = Math.round((this.selectedRange.end -
            this.selectedRange.start) / this.widget.datapointCount);
        this.filterParameters.duration = this.selectedRange.short;
        this.filterParameters.apigroup = this.apigroup;

        this.pending = true;
        this.widgetQueries = this.widget.chartQueries.map((q) => {
            return q.query;
        });
        this.noOfQueries = this.widgetQueries.length;
        this.chartDataService
            .getDataFromQueries(this.widget, this.widgetQueries, this.filterParameters);

    }

    renderChart(options): void {
        switch (this.widget.chartVariant) {
            case 'pie':
                this.renderPie(options);
                break;
            case 'doughnut':
                this.renderDoughnut(options);
                break;
        }
    }

    private renderPie(options): void {
        //console.log('Rendering pie');
        options.series.top = 40;
        options.series.radius = '90%';
        options.series.center = ['50%', '50%'];
        this.echartsInstance.setOption(options);
    }

    private renderDoughnut(options): void {
        //console.log('Rendering doughnut');
        options.series.top = 40;
        options.series.radius = ['45%', '90%'];
        options.series.center = ['50%', '50%'];
        this.echartsInstance.setOption(options);
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.unsubscribeTimer$.next();
        this.unsubscribeTimer$.complete();
    }
}
