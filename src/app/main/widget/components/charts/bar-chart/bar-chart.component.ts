import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output, SimpleChanges
} from '@angular/core';
import {ECharts} from 'echarts';
import {Query} from '../../../interfaces/query';
import {Subject, timer} from 'rxjs';
import {BarChartLayoutOptions} from '../../../interfaces/barChartOptions';
import {ConfigService} from '../../../../services/config.service';
import {LayoutService, PanelService} from '../../../../shared/services';
import {ChartDataService} from '../../../../shared/services/chart-data.service';
import {takeUntil} from 'rxjs/operators';
import {AbstractChartComponent} from '../abstract-chart.component';
import {FuseLocationService} from '../../../../../services/location.service';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent extends AbstractChartComponent
    implements OnInit, OnChanges, AfterViewInit, OnDestroy {
    @Input('reset')
    set reset(data: boolean) {
        if (data) {
            this.getData();
        }
    }

    @Output()
    filter: EventEmitter<any> = new EventEmitter();

    public pending: boolean;
    public startTime: any = 1581722395;
    public endTime: any = 1581723395;
    public duration: any = 1581722395;
    public step: any = 15;
    public url: any;
    public echartsInstance: ECharts;
    public themeSubscription: any;
    public options: any = {};
    public colors: any;
    public echarts: any;
    public interval;
    public chartData;
    public fieldName: string;
    private apigroup: any;

    private isDataRefresh = false;
    private chartWidth: number;
    private chartHeight: number;
    private noOfQueries: number;
    private widgetQueries: Query[];
    private filterParameters = {
        startTime: 1581722395,
        endTime: 1581722395,
        duration: 1581722395,
        step:  15,
        apigroup: ''
    };

    private xAxis;
    private yAxis;

    private chartInitialized: Subject<ECharts> = new Subject<ECharts>();
    private chartLayoutSet: Subject<BarChartLayoutOptions> = new Subject<BarChartLayoutOptions>();
    private chartDataFetched: Subject<any[]> = new Subject<any[]>();

    private unsubscribe$: Subject<void> = new Subject<void>();
    private unsubscribeTimer$: Subject<void> = new Subject<void>();

    constructor(
        private configSvc: ConfigService,
        private cd: ChangeDetectorRef,
        private panelService: PanelService,
        private layoutService: LayoutService,
        public chartDataService: ChartDataService,
        private _locationService: FuseLocationService,
    ) {
        super();

        // AB redraw chart when gridster item resized
        // this.layoutService.itemResized$
        //     .pipe(takeUntil(this.unsubscribe$))
        //     .subscribe((gridsterItemComponent: GridsterItemComponent) => {
        //         console.log('Probao BAR');
        //         if (gridsterItemComponent.item.id === this.widget.id) {
        //             this.chartWidth = gridsterItemComponent.width;
        //             this.chartHeight = gridsterItemComponent.height - 60;
        //             const numOfSplits = Math.ceil(this.chartHeight / 60);
        //             if (this.echartsInstance) {
        //                 this.echartsInstance?.setOption({
        //                     yAxis: [
        //                         {
        //                             splitNumber: numOfSplits,
        //                         },
        //                     ],
        //                 });
        //             }
        //         }
        //     });
    }

    ngOnInit(): void {
        // Populate location
        this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
        this.ngOnChanges(undefined);
        this.pending = false;

        // When chart is initialized, init the layout
        this.chartInitialized
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((e: ECharts) => {
                // console.log('1 CHART INITIALIZED');
                this.getData();
            });
        // When data is fetched and formatted, update data on charts
        this.chartDataFetched
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartDataSeries: any[]) => {
                // console.log('4 DATA FETCHED');
                const data = chartDataSeries[0].data;
                // const legend = data.legend;
                // const series = [];

                this.resetChartData(this.echartsInstance);

                // data.series.forEach((s) => {
                //     series[0].data.push(s.data[0]);
                // });
                // chartDataSeries.forEach((cd) => {
                //     // cd.data.series.forEach((s) => {
                //     //     s.itemStyle = {
                //     //         opacity: this.chartDataOpacity
                //     //     };
                //     // });
                //     series.push(cd.data.series[0]);
                //     legend.push(cd.data.legend.data);
                //     // xAxisList.push(cd.data.xAxis);
                //     // yAxisList.push(cd.data.yAxis);
                // });
                this.chartData.series = data.series;
                this.chartData.legend = data.legend;
                this.xAxis = data.xAxis[0];
                this.yAxis = data.yAxis[0];
                this.chartData.xAxis = this.xAxis;
                this.chartData.yAxis = this.yAxis;
                // Format legend
                this.chartData.legend.formatter = (name) => {
                    let newName;
                    if (name.length > 5) {
                        newName = name.substr(0, 3) + '...';
                        return newName;
                    }
                    return name;
                };
                this.chartData.legend.tooltip = {
                    show: true,
                    trigger: 'item',
                    formatter: (a) => {
                        return a.name;
                    }
                };
                setTimeout(() => {
                    this.echartsInstance.setOption(this.chartData);
                    this.setChartLayout();
                }, 10);
            });
        // Set the layout
        this.chartLayoutSet
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((layoutOptions: BarChartLayoutOptions) => {

                // console.log('5 LAYOUT SET');
                this.pending = false;
                this.renderChart(layoutOptions);
                // this.echartsInstance.setOption(layoutOptions);
            });

        // When data is fetched from URL format the series
        this.chartDataService.dataFetched$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id){

                    // console.log('2 DATA FETCHED on service');
                    this.pending = false;
                    this.chartDataService.getSeriesFromResults(this.widget, chartObject.data);
                }
            });

        // When series are formatted set the data on chart
        this.chartDataService.seriesFormatted$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id) {
                    // console.log('3 SERIES FORMATTED on service');
                    this.chartDataFetched.next(chartObject.data);
                }
            });
    }

    // Callback method when chart initializes
    onChartInit(e: ECharts): void {
        // console.log('EchartInitialized ', e);
        this.echartsInstance = e;
        this.chartInitialized.next(e);
    }

    onChartClick(event: any, type: string): void {
        const data = {};
        data['name'] = this.fieldName;
        data['filters'] = event['selected'];
        this.filter.emit(data);
    }

    ngOnChanges(changes: SimpleChanges): void {
        let loadData = false;

        // First time change
        if (changes?.widget?.isFirstChange()) {
            return;
        }

        // Widget and duration are loaded
        if (this.widget && this.duration) {
            loadData = true;
        }

        // Selected range has changed
        if (changes?.selectedRange?.currentValue) {
            this.startTime = changes.selectedRange.currentValue.start;
            this.endTime = changes.selectedRange.currentValue.end;
            this.duration = changes.selectedRange.currentValue.short;
            this.step = Math.round(
                (changes.selectedRange.currentValue.end -
                    changes.selectedRange.currentValue.start) /
                this.widget.datapointCount
            );
            loadData = true;
        }

        // Refresh time has changed
        if (changes?.refreshTime?.currentValue) {
            loadData = true;
        }

        // Interval has changed
        if (changes?.interval?.currentValue) {
            this.unsubscribeTimer$.next();
            if (typeof changes?.interval?.currentValue.value === 'number') {
                const setInterval = timer(
                    0,
                    changes.interval.currentValue.value
                );
                setInterval
                    .pipe(takeUntil(this.unsubscribeTimer$))
                    .subscribe(() => {
                        this.getData();
                    });
            } else {
                this.unsubscribeTimer$.next();
            }
        }

        // TODO: refactor this
        this.filterParameters.startTime = this.startTime;
        this.filterParameters.endTime = this.endTime;
        this.filterParameters.step = this.step;
        this.filterParameters.duration = this.duration;
        this.filterParameters.apigroup = this.apigroup;

        // If data needs to be loaded
        if (loadData && this.selectedRange) {
            this.getData();
        }
        this.cd.detectChanges();
    }

    // Set the layout options for chart
    // TODO read chartOptions and add here
    setChartLayout(): void {
        const options = this.layoutService.getChartLayout(this.widget);

        options.tooltip = {
            formatter: (a) => {
                return a.marker + ' ' + a.seriesName + ' : '
                    + this.chartDataService.getFormattedValue(a.value, this.widget.chartQueries[0].query.spec.query_info.units, 2);
            }
        };
        this.chartLayoutSet.next(options);
    }

    // Get data from backend by using widget queries
    getData(): void {
        this.filterParameters.startTime = this.selectedRange.start;
        this.filterParameters.endTime = this.selectedRange.end;
        this.filterParameters.step = Math.round((this.selectedRange.end -
            this.selectedRange.start) / this.widget.datapointCount);
        this.filterParameters.duration = this.selectedRange.short;
        this.filterParameters.apigroup = this.apigroup;

        this.pending = true;
        this.widgetQueries = this.widget.chartQueries.map((q) => {
            return q.query;
        });
        this.noOfQueries = this.widgetQueries.length;
        this.chartDataService
            .getDataFromQueries(this.widget, this.widgetQueries, this.filterParameters);

    }

    renderChart(options): void {
        switch (this.widget.chartVariant) {
            case 'horizontal':
                this.renderHorizontal(options);
                break;
            case 'vertical':
                this.renderVertical(options);
                break;
        }
    }

    private renderHorizontal(options): void {
        //console.log('Rendering horizontal bar');

        let totalValue = 0;
        const legendData = [];
        options.xAxis = this.chartData.yAxis;
        options.yAxis = this.chartData.xAxis;
        options.series = this.chartData.series;
        options.series.forEach((s, index) => {
            s.itemStyle = {
                color: this.widget.chartQueries[0].color === 'auto' ?
                    null : this.layoutService.addAlpha(this.widget.chartQueries[0].color, this.widget.opacity)
            };
            s.label = {
                show: true,
                position: 'left',
                color: '#949BB9'
            };
            // s.emphasis = {
            //     itemStyle: {
            //         color: this.widget.chartQueries[0].color === 'auto' ?
            //             this.layoutService.removeAlpha(options.color[index]) :
            //             this.widget.chartQueries[0].color
            //     }
            // };
            legendData.push({value: parseFloat(s.data[0]), name: s.name});
            totalValue += parseFloat(s.data[0]);

        });
        legendData.forEach((ld) => {
            ld.percent = ld.value / totalValue * 100;
        });
        options.legend.formatter = (name) => {
            const index = legendData.findIndex((el) => {
                return el.name === name;
            });
            if (index > -1) {
                return legendData[index].percent?.toFixed(0) + ' %';
            }

        };

        const xAxisLabels = this.chartDataService.formatValueLabel(options.series, 8, this.widget.chartQueries[0].query.spec.query_info.units);
        if (options.xAxis && xAxisLabels) {
            options.xAxis.splitNumber = xAxisLabels.splitNumber;
            options.xAxis.interval = xAxisLabels.interval;
            options.xAxis.min = 0;
            options.xAxis.max = xAxisLabels.max;
            options.xAxis.formatter = (value) => {
                if (this.widget.chartQueries[0].query.spec.query_info.units === 'bytes') {
                    return value + ' ' + xAxisLabels.wantedSize;
                } else {
                    return this.chartDataService.getFormattedValue(value, this.widget.chartQueries[0].query.spec.query_info.units);
                }
            };
        }

        // this.setFontFamily(options);
        options.grid.left = '40px';
        options.grid.right = '13px';
        options.grid.bottom = '10px';
        this.echartsInstance.setOption(options);
    }

    private renderVertical(options): void {
        // console.log('Rendering vertical bar');
        let totalValue = 0;
        const legendData = [];
        options.xAxis = this.chartData.xAxis;
        options.yAxis = this.chartData.yAxis;
        options.series = this.chartData.series;
        options.series.forEach((s, index) => {
            s.itemStyle = {
                color: this.widget.chartQueries[0].color === 'auto' ?
                    null :
                    this.layoutService.addAlpha(this.widget.chartQueries[0].color, this.widget.opacity)
            };
            s.label = {
                color: '#949BB9'
            };
            // s.emphasis = {
            //   itemStyle: {
            //       color: this.widget.chartQueries[0].color === 'auto' ?
            //           this.layoutService.removeAlpha(options.color[index]) :
            //           this.widget.chartQueries[0].color
            //   }
            // };
            legendData.push({value: parseFloat(s.data[0]), name: s.name});
            totalValue += parseFloat(s.data[0]);
        });
        legendData.forEach((ld) => {
            ld.percent = ld.value / totalValue * 100;
        });
        options.legend.formatter = (name) => {
            const index = legendData.findIndex((el) => {
                return el.name === name;
            });
            if (index > -1) {
                return legendData[index].percent?.toFixed(0) + ' %';
            }

        };

        this.setFontFamily(options);
        const yAxisLabels = this.chartDataService.formatValueLabel(options.series, 8, this.widget.chartQueries[0].query.spec.query_info.units);
        if (options.yAxis && yAxisLabels) {
            options.yAxis.splitNumber = yAxisLabels.splitNumber;
            options.yAxis.interval = yAxisLabels.interval;
            options.yAxis.min = 0;
            options.yAxis.max = yAxisLabels.max;
            options.yAxis.formatter = (value) => {
                if (this.widget.chartQueries[0].query.spec.query_info.units === 'bytes') {
                    return value + ' ' + yAxisLabels.wantedSize;
                } else {
                    return this.chartDataService.getFormattedValue(value, this.widget.chartQueries[0].query.spec.query_info.units);
                }
            };
        }

        this.echartsInstance.setOption(options);
    }

    ngAfterViewInit(): void {

    }

    // On destroy unsubscribe all subscriptions
    ngOnDestroy(): void {
        // this.themeSubscription.unsubscribe();
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.unsubscribeTimer$.next();
        this.unsubscribeTimer$.complete();
    }
}
