import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output, SimpleChanges
} from '@angular/core';
import {Widget} from '../../..';
import {ECharts} from 'echarts';
import {BarChartLayoutOptions, BarChartSeriesOptions} from '../../../interfaces/barChartOptions';
import {Query} from '../../../interfaces/query';
import {Subject, timer} from 'rxjs';
import {ConfigService} from '../../../../services/config.service';
import {LayoutService, PanelService} from '../../../../shared/services';
import {ChartDataService} from '../../../../shared/services/chart-data.service';
import {takeUntil} from 'rxjs/operators';
import {DEFAULT_THEME} from '../../../../theme';
import {AbstractChartComponent} from '../abstract-chart.component';
import {FuseLocationService} from '../../../../../services/location.service';

@Component({
  selector: 'app-graph-chart',
  templateUrl: './graph-chart.component.html',
  styleUrls: ['./graph-chart.component.scss']
})
export class GraphChartComponent extends AbstractChartComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {

    @Input()
    public widget: Widget;

    @Input()
    public index: number;

    @Input('reset')
    set reset(data: boolean) {
        if (data) {
            this.getData();
        }
    }

    @Output()
    filter: EventEmitter<any> = new EventEmitter();

    public startTime: any = 1581722395;
    public endTime: any = 1581723395;
    public duration: any = 1581722395;
    public step: any = 15;
    public url: any;
    public echartsInstance: ECharts;
    public themeSubscription: any;
    public options: any = {};
    public colors: any;
    public echarts: any;
    public pending: boolean;
    private apigroup: any;
    public icon = [
        {
            title: 'Events',
            name: 'throughput-wireless',
            icon: 'circle'
        },
        {
            title: 'Faults',
            name: 'throughput-wired',
            icon: 'image://assets/icons/charts/line-chart-fault.png'
        },
        {
            title: 'Audit logs',
            name: 'throughput-all',
            icon: 'image://assets/icons/charts/line-chart-triangle.png'
        }
    ];


    public chartData: BarChartSeriesOptions;

    private isDataRefresh = false;
    private chartWidth: number;
    private chartHeight: number;
    private noOfQueries: number;
    private widgetQueries: Query[];
    private filterParameters = {
        startTime: 1581722395,
        endTime: 1581722395,
        duration: 1581722395,
        step:  15,
        apigroup: ''
    };

    private chartInitialized: Subject<ECharts> = new Subject<ECharts>();
    private chartLayoutSet: Subject<BarChartLayoutOptions> = new Subject<BarChartLayoutOptions>();
    private chartDataFetched: Subject<any[]> = new Subject<any[]>();

    private unsubscribe$: Subject<void> = new Subject<void>();
    private unsubscribeTimer$: Subject<void> = new Subject<void>();

    constructor(
        private configSvc: ConfigService,
        private cd: ChangeDetectorRef,
        private panelService: PanelService,
        private layoutService: LayoutService,
        private chartDataService: ChartDataService,
        private _locationService: FuseLocationService,
    ) {
        super();

        // AB redraw chart when gridster item resized
        // this.layoutService.itemResized$
        // .pipe(takeUntil(this.unsubscribe$))
        // .subscribe((gridsterItemComponent: GridsterItemComponent) => {
        //     if (gridsterItemComponent.item.id === this.widget.id) {
        //         this.chartWidth = gridsterItemComponent.width;
        //         this.chartHeight = gridsterItemComponent.height - 60;
        //         const numOfSplits = Math.ceil(this.chartHeight / 60);
        //         this.echartsInstance?.setOption({
        //             yAxis: [
        //                 {
        //                     splitNumber: numOfSplits,
        //                 },
        //             ],
        //         });
        //         // this.echartsInstance?.resize();
        //     }
        // });

        // this.layoutService.getEditedObs().pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
        // 	if (res.id === this.widget.id) {
        // 		this.widget = res;
        // 		this.drawLine(this.formatSeries(this.seriesData));
        // 	}
        // });
        // this.layoutService.getSelectedChartObs().pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
        // 	if (res === this.index || res === this.widget.id) {
        // 		this.layoutService.chartOptions = this.widget['chartOptions'];
        // 	}
        // });
        // //get chart styles
        // this.themeSubscription = this.configSvc
        // 	.getSelectedThemeObs()
        // 	.pipe(takeUntil(this.unsubscribe$))
        // 	.subscribe((config: any) => {
        // 		this.colors = config.theme.variables;
        // 		this.echarts = config.echart;
        // 		if (this.seriesData && this.widget) {
        // 			this.drawLine(this.formatSeries(this.seriesData));
        // 		}
        // 	});
    }

    ngOnInit(): void {
        this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
        this.ngOnChanges(undefined);
        this.pending = false;

        // When chart is initialized, init the layout
        this.chartInitialized
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((e: ECharts) => {
                this.getData();
            });
        // When data is fetched and formatted, update data on charts
        this.chartDataFetched
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartDataSeries: any[]) => {
                const xAxisList = [];
                const yAxisList = [];
                const legend = [];
                const series = [];
                this.resetChartData(this.echartsInstance);

                chartDataSeries.forEach((cd) => {
                    cd.data.series[0].type = 'line';
                    // Delete animation
                    delete cd.data.series[0].animationDelay;
                    series.push(cd.data.series[0]);
                    legend.push(cd.data.legend.data);
                    xAxisList.push(cd.data.xAxis);
                    yAxisList.push(cd.data.yAxis);
                });
                this.chartData.series = series;
                this.chartData.legend.data = legend;
                this.chartData.xAxis = xAxisList[0];
                this.chartData.yAxis = yAxisList[0];

                const yAxisLabels = this.chartDataService.formatValueLabel(this.chartData.series, 8, this.widget.chartQueries[0].query.spec.query_info.units);

                this.chartData.yAxis.splitNumber = yAxisLabels.splitNumber;
                this.chartData.yAxis.interval = yAxisLabels.interval;
                this.chartData.yAxis.min = 0;
                this.chartData.yAxis.max = yAxisLabels.max;
                this.chartData.yAxis.formatter = (value) => {
                        if (this.widget.chartQueries[0].query.spec.query_info.units === 'bytes') {
                            return value + ' ' + yAxisLabels.wantedSize;
                        } else {
                            return this.chartDataService.getFormattedValue(value, this.widget.chartQueries[0].query.spec.query_info.units);
                        }
                    };

                setTimeout(() => {
                    this.echartsInstance.setOption(this.chartData);
                    this.setChartLayout();
                }, 10);
            });
        // Set the layout
        this.chartLayoutSet
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((layoutOptions: BarChartLayoutOptions) => {
                this.pending = false;
                this.renderChart(layoutOptions);
            });

        // When data is fetched from URL format the series
        this.chartDataService.dataFetched$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id){
                    this.pending = false;
                    this.chartDataService.getSeriesFromResults(this.widget, chartObject.data);
                }
            });

        // When series are formatted set the data on chart
        this.chartDataService.seriesFormatted$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id) {
                    this.chartDataFetched.next(chartObject.data);
                }
            });
    }

    onChartInit(e: ECharts): void {
        this.echartsInstance = e;
        this.chartInitialized.next(e);
    }

    onChartLegendSelected(event: any, type: string): void {
        this.filter.emit(event['selected']);
    }


    ngOnChanges(changes: SimpleChanges): void {
        let loadData = false;

        // TODO fix datapoint count
        this.widget.datapointCount = 50;

        if (changes?.widget && changes.widget.isFirstChange()) {
            return;
        }
        if (this.widget && this.duration) {
            loadData = true;
        }
        if (changes && changes.selectedRange && changes.selectedRange.currentValue) {
            this.startTime = changes.selectedRange.currentValue.start;
            this.endTime = changes.selectedRange.currentValue.end;
            this.duration = changes.selectedRange.currentValue.short;
            this.step = Math.round((changes.selectedRange.currentValue.end - changes.selectedRange.currentValue.start) / this.widget.datapointCount);
            loadData = true;
        }

        if (changes && changes.refreshTime && changes.refreshTime.currentValue !== undefined) {
            loadData = true;
        }

        if (changes && changes.interval && changes.interval.currentValue) {
            this.unsubscribeTimer$.next();
            if (changes.interval.currentValue && typeof changes.interval.currentValue.value === 'number') {
                const setInterval = timer(0, changes.interval.currentValue.value);
                setInterval.pipe(takeUntil(this.unsubscribeTimer$)).subscribe(() => this.getData());
            } else {
                this.unsubscribeTimer$.next();
            }
        }
        // TODO: refactor this
        this.filterParameters.startTime = this.startTime;
        this.filterParameters.endTime = this.endTime;
        this.filterParameters.step = this.step;
        this.filterParameters.duration = this.duration;
        this.filterParameters.apigroup = this.apigroup;

        if (loadData) {
            this.getData();
        }
    }

    setChartLayout(): void {
        const options = this.layoutService.getChartLayout(this.widget);
        options.tooltip = {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            },
            formatter: (a) => {
                let response = a.marker + ' ' + a.seriesName + ': ' + this.chartDataService.getFormattedValue(a.value, this.widget.chartQueries[0].query.spec.query_info.units);
                if (Array.isArray(a)) {
                    response = '';
                    a.forEach((av) => {
                        response += av.marker
                            + ' ' + av.seriesName
                            + ': '
                            + this.chartDataService.getFormattedValue(av.value, this.widget.chartQueries[0].query.spec.query_info.units)
                            + '<br />';
                    });
                }
                return response;
            }
        };
        options.grid.bottom = 0;
        this.chartLayoutSet.next(options);
    }

    getData(): void {
        this.filterParameters.startTime = this.selectedRange.start;
        this.filterParameters.endTime = this.selectedRange.end;
        this.filterParameters.step = Math.round((this.selectedRange.end -
            this.selectedRange.start) / this.widget.datapointCount);
        this.filterParameters.duration = this.selectedRange.short;
        this.filterParameters.apigroup = this.apigroup;

        this.pending = true;
        this.widgetQueries = this.widget.chartQueries.map((q) => {
            return q.query;
        });
        this.noOfQueries = this.widgetQueries.length;
        this.chartDataService
            .getDataFromQueries(this.widget, this.widgetQueries, this.filterParameters);

    }

    renderChart(options): void {
        switch (this.widget.chartVariant) {
            case 'line':
                this.renderLine(options);
                break;
            case 'area':
                this.renderArea(options);
                break;
            case 'bar':
                this.renderBar(options);
        }
    }

    private renderLine(options): void {
        //console.log('Rendering line');
        // options.xAxis = this.yAxis;
        // options.yAxis = this.chartData.yAxis;
        // options.yAxis.forEach((axis, i) => {
        //     axis.show = this.widget.chartQueries[i].showOnAxis;
        // })
        options.series = this.chartData.series;
        options.series[0].type = 'line';
        options.series.forEach((ser, i) => {
            ser.itemStyle = {
                color: this.widget.chartQueries[i].color
            };
            ser.lineStyle = {
                color: this.widget.chartQueries[i].color
            };
        });
        // options.series[0].tooltip = {
        //     formatter: '{b0}: {c0}<br />blackssd {b1}: {c1}'
        // };
        // options.series.forEach((cd) => {
        //     cd.data.series[0].type = 'line';
        //     cd.data.series[0].tooltip = {
        //         formatter: '{b0}: {c0}<br />blackssd {b1}: {c1}'
        //     };
        //     series.push(cd.data.series[0]);
        //     legend.push(cd.data.legend.data);
        //     xAxisList.push(cd.data.xAxis);
        //     yAxisList.push(cd.data.yAxis);
        // });
        // options.series.forEach((s) => {
        //     s.label = {
        //         show: false
        //     };
        //     s.itemStyle = {
        //         opacity: this.chartDataOpacity
        //     };
        // });


        this.echartsInstance.setOption(options);
    }

    private renderArea(options): void {
        //console.log('Rendering area');
        options.series = this.chartData.series;
        options.series.forEach((ser, i, arr) => {
            ser.type = 'line';
            ser.areaStyle = {
                opacity: this.widget.opacity,
                color: this.widget.chartQueries[i].color
            };
            ser.z = arr.length - i;
            // ser.itemStyle = {
            //     color: this.widget.chartQueries[i].color
            // };
        });
        options.series[0].stack = this.widget.chartQueries[1].query.metadata.name;
        // options.xAxis = this.xAxis;
        // options.yAxis = this.yAxis;
        // options.series = this.chartData.series;
        // options.series.forEach((s) => {
        //     s.itemStyle = {
        //         opacity: this.chartDataOpacity
        //     };
        // });
        this.echartsInstance.setOption(options);
    }

    private renderBar(options): void {
        //console.log('Rendering bar');
        options.series = this.chartData.series;
        options.series.forEach((ser, i) => {
            ser.type = 'bar';
            ser.itemStyle = {
                color: this.widget.chartQueries[i].color
            };
        });
        // options.xAxis = this.xAxis;
        // options.yAxis = this.yAxis;
        // options.series = this.chartData.series;
        // options.series.forEach((s) => {
        //     s.itemStyle = {
        //         opacity: this.chartDataOpacity
        //     };
        // });
        this.echartsInstance.setOption(options);
    }

    ngAfterViewInit(): void {

    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.unsubscribeTimer$.next();
        this.unsubscribeTimer$.complete();
    }
}
