import {Input} from '@angular/core';
import {Widget} from '../..';
import {BarChartSeriesOptions} from '../../interfaces/barChartOptions';
import ECharts = echarts.ECharts;

export abstract class AbstractChartComponent {

    @Input()
    public widget: Widget;

    @Input()
    public chartVariant: string;
    @Input()
    public queries: any[];

    @Input()
    public selectedRange: any;
    @Input()
    public refreshTime: any;
    @Input()
    public interval: any;
    @Input()
    public colorPalette: any;
    @Input()
    public chartOptions: any;
    // TODO maybe make a type for options

    public chartData: BarChartSeriesOptions;

    resetChartData(echart: ECharts): void {
        this.chartData = new BarChartSeriesOptions();
        this.chartData.legend = {
            data: []
        };
        this.chartData.series = [];
        // this.chartData.xAxis = [];
        // this.chartData.yAxis = [];
        echart?.setOption(this.chartData);
    }

    setFontFamily(options): void {
        const axisLabel = {
            color: '#949BB9',
            fontFamily: 'IBM Plex Sans',
            fontSize: 10,
            fontWeight: 'normal'
        };
        if (options.xAxis) {
            options.xAxis.axisLabel = axisLabel;
        } else {
            options.xAxis = {
                axisLabel: axisLabel
            };
        }
        if (options.yAxis) {
            options.yAxis.axisLabel = axisLabel;
        } else {
            options.yAxis = {
                axisLabel: axisLabel
            };
        }
    }
    // TODO add method to style axis

}
