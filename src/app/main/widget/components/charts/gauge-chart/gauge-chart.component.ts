import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ElementRef, EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit, Output, SimpleChanges
} from '@angular/core';
import {Widget} from '../../..';
import {ECharts} from 'echarts';
import {Query} from '../../../interfaces/query';
import {Subject, timer} from 'rxjs';
import {BarChartLayoutOptions, BarChartSeriesOptions} from '../../../interfaces/barChartOptions';
import {ConfigService} from '../../../../services/config.service';
import {LayoutService, PanelService} from '../../../../shared/services';
import {ChartDataService} from '../../../../shared/services/chart-data.service';
import {takeUntil} from 'rxjs/operators';
import {FUSE_THEME} from '../../../../theme/default-theme';
import {AbstractChartComponent} from '../abstract-chart.component';
import {FuseLocationService} from '../../../../../services/location.service';

@Component({
  selector: 'app-gauge-chart',
  templateUrl: './gauge-chart.component.html',
  styleUrls: ['./gauge-chart.component.scss']
})
export class GaugeChartComponent extends AbstractChartComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
    public startTime: any = 1581722395;
    public endTime: any = 1581723395;
    public duration: any = 1581722395;
    public step: any = 15;
    public url: any;
    public echartsInstance: ECharts;
    public options: any = {};
    public echarts: any;
    public pending: boolean;
    public unitType;
    private apigroup: any;
    private noOfQueries: number;
    private widgetQueries: Query[];
    private filterParameters = {
        startTime: 1581722395,
        endTime: 1581722395,
        duration: 1581722395,
        step:  15,
        apigroup: ''
    };

    percentageValue: (value: number) => string;

    private chartInitialized: Subject<ECharts> = new Subject<ECharts>();
    private chartLayoutSet: Subject<BarChartLayoutOptions> = new Subject<BarChartLayoutOptions>();
    private chartDataFetched: Subject<BarChartSeriesOptions> = new Subject<BarChartSeriesOptions>();


    private unsubscribe$: Subject<void> = new Subject<void>();
    private unsubscribeTimer$: Subject<void> = new Subject<void>();

    @Input('reset')
    set reset(data: boolean) {
        if (data) {
            this.getData();
        }
    }

    @Input() public widget: Widget;
    @Input() public index: any;

    @Output()
    filter: EventEmitter<any> = new EventEmitter();

    constructor(
        private configSvc: ConfigService,
        private cd: ChangeDetectorRef,
        private panelService: PanelService,
        private layoutService: LayoutService,
        private chartDataService: ChartDataService,
        private _locationService: FuseLocationService,
    ) {
        super();

        this.percentageValue = (value: number) => {
            return `${Math.round(value)} ${this.unitType} `;
        };
    }

    ngOnInit(): void {
        this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
        this.ngOnChanges(undefined);
        this.pending = false;

        // When chart is initialized, init the layout
        this.chartInitialized
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((e: ECharts) => {
                this.getData();
            });
        // When data is fetched and formatted, update data on charts
        this.chartDataFetched
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((options: BarChartSeriesOptions) => {

                this.resetChartData(this.echartsInstance);

                options.series[0].type = 'gauge';
                options.series[0].radius = '100%';

                this.chartData.series = options.series[0];
                delete this.chartData.legend;

                setTimeout(() => {
                    this.echartsInstance.setOption(this.chartData);
                    this.setChartLayout();
                }, 10);
            });
        // Set the layout
        this.chartLayoutSet
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((layoutOptions: BarChartLayoutOptions) => {
                this.pending = false;
                this.renderChart(layoutOptions);
            });

        // When data is fetched from URL format the series
        this.chartDataService.dataFetched$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id){
                    this.pending = false;
                    this.chartDataService.getSeriesFromResults(this.widget, chartObject.data);
                }
            });

        // When series are formatted set the data on chart
        this.chartDataService.seriesFormatted$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id) {
                    chartObject.data.forEach((s) => {
                        this.chartDataFetched.next(s.data);
                    });
                }
            });
    }

    onChartInit(e: ECharts): void {
        this.echartsInstance = e;
        this.chartInitialized.next(e);
    }



    public ngOnChanges(changes: SimpleChanges): void {
        let loadData = false;
        if (changes?.widget && changes.widget.isFirstChange()) {
            return;
        }
        if (this.widget && this.duration) {
            loadData = true;
        }
        if (changes?.selectedRange && changes.selectedRange.currentValue) {
            this.duration = changes.selectedRange.currentValue.short;
            this.step = Math.round((changes.selectedRange.currentValue.end - changes.selectedRange.currentValue.start) / this.widget.datapointCount);
            loadData = true;
        }

        if (changes?.refreshTime && changes.refreshTime.currentValue !== undefined) {
            loadData = true;
        }

        if (changes?.interval && changes.interval.currentValue) {
            this.unsubscribeTimer$.next();
            if (changes?.interval.currentValue && typeof changes.interval.currentValue.value === 'number') {
                const setInterval = timer(0, changes.interval.currentValue.value);
                setInterval.pipe(takeUntil(this.unsubscribeTimer$)).subscribe(() => this.getData());
            } else {
                this.unsubscribeTimer$.next();
            }
        }

        // TODO: refactor this
        this.filterParameters.startTime = this.startTime;
        this.filterParameters.endTime = this.endTime;
        this.filterParameters.step = this.step;
        this.filterParameters.duration = this.duration;
        this.filterParameters.apigroup = this.apigroup;


        if (loadData) {
            this.getData();
        }
        this.cd.detectChanges();

    }
    setChartLayout(): void {
        // Initialize layout of chart
        const options = this.layoutService.getChartLayout(this.widget);
        options.series = this.chartData.series;
        options.series.title = {
            show: false
        };
        options.grid.left = 0;
        options.grid.right = 0;
        options.grid.bottom = 0;
        options.grid.top = 0;
        this.chartLayoutSet.next(options);
    }

    getData(): void {
        this.filterParameters.startTime = this.selectedRange.start;
        this.filterParameters.endTime = this.selectedRange.end;
        this.filterParameters.step = Math.round((this.selectedRange.end -
            this.selectedRange.start) / this.widget.datapointCount);
        this.filterParameters.duration = this.selectedRange.short;
        this.filterParameters.apigroup = this.apigroup;

        this.pending = true;
        this.widgetQueries = this.widget.chartQueries.map((q) => {
            return q.query;
        });
        this.noOfQueries = this.widgetQueries.length;
        this.chartDataService
            .getDataFromQueries(this.widget, this.widgetQueries, this.filterParameters);

    }

    ngAfterViewInit(): void {

    }

    private renderChart(options): void {
        //console.log('Rendering gauge chart');
        options.series.axisLine = {
            lineStyle: {
                opacity: this.widget.opacity,
                color: [
                    [0.2, FUSE_THEME.variables.mediumseagreen],
                    [0.8, FUSE_THEME.variables.dodgerblue],
                    [1, FUSE_THEME.variables.tomato]
                ]
            }
        };
        this.echartsInstance.setOption(options);
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.unsubscribeTimer$.next();
        this.unsubscribeTimer$.complete();
    }
}
