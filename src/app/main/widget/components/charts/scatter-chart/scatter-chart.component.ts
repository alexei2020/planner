import {
    AfterViewInit, ChangeDetectorRef,
    Component,
    EventEmitter,
    HostListener,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output, SimpleChanges
} from '@angular/core';
import {ECharts} from 'echarts';
import {BarChartLayoutOptions, BarChartSeriesOptions} from '../../../interfaces/barChartOptions';
import {Query} from '../../../interfaces/query';
import {Subject, timer} from 'rxjs';
import {ConfigService} from '../../../../services/config.service';
import {LayoutService, PanelService} from '../../../../shared/services';
import {ChartDataService} from '../../../../shared/services/chart-data.service';
import {takeUntil} from 'rxjs/operators';
import {DEFAULT_THEME} from '../../../../theme';
import {AbstractChartComponent} from '../abstract-chart.component';
import {FuseLocationService} from '../../../../../services/location.service';

@Component({
  selector: 'app-scatter-chart',
  templateUrl: './scatter-chart.component.html',
  styleUrls: ['./scatter-chart.component.scss']
})
export class ScatterChartComponent extends AbstractChartComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
    public startTime: any = 1581722395;
    public endTime: any = 1581723395;
    public duration: any = 1581722395;
    public step: any = 15;
    public url: any;
    public echartsInstance: ECharts;
    public themeSubscription: any;
    public options: any = {};
    public colors: any;
    public echarts: any;
    public pending: boolean;
    public seriesData = [];

    private xAxis = [];
    private yAxis = [];

    public chartData: BarChartSeriesOptions;

    private isDataRefresh = false;
    private chartWidth: number;
    private chartHeight: number;
    private noOfQueries: number;
    private widgetQueries: Query[];
    private filterParameters = {
        startTime: 1581722395,
        endTime: 1581722395,
        duration: 1581722395,
        step:  15,
        apigroup: ''
    };

    private chartInitialized: Subject<ECharts> = new Subject<ECharts>();
    private chartLayoutSet: Subject<BarChartLayoutOptions> = new Subject<BarChartLayoutOptions>();
    private chartDataFetched: Subject<any[]> = new Subject<any[]>();

    private apigroup: any = 'fanta.ws.io';
    private unsubscribe$: Subject<void> = new Subject<void>();
    private unsubscribeTimer$: Subject<void> = new Subject<void>();

    @HostListener('window:resize', ['$event'])
    onResize(): void {
        this.echartsInstance.resize();
        this.cd.detectChanges();
    }
    @Input()
    public index: number;
    @Input('reset')
    set reset(data: boolean) {
        if (data) {
            this.getData();
        }
    }
    @Output() filter: EventEmitter<any> = new EventEmitter();



    constructor(
        private configSvc: ConfigService,
        private cd: ChangeDetectorRef,
        private panelService: PanelService,
        private layoutService: LayoutService,
        private chartDataService: ChartDataService,
        private _locationService: FuseLocationService,
    ) {
        super();

        // this.layoutService.getEditedObs().pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
        // 	if (res.id === this.widget.id) {
        // 		this.widget = res;
        // 		this.drawLine(this.formatSeries(this.seriesData));
        // 	}
        // });
        //
        // this.layoutService.getSelectedChartObs().pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
        // 	if (res === this.index || res === this.widget.id) {
        // 		this.layoutService.chartOptions = this.widget['chartOptions'];
        // 	}
        // });
    }

    ngOnInit(): void {
        this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
        this.ngOnChanges(undefined);



        // When chart is initialized, init the layout
        this.chartInitialized
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((e: ECharts) => {
                this.getData();
            });
        // When data is fetched and formatted, update data on charts
        this.chartDataFetched
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartDataSeries: any[]) => {
                // let xAxisList = [];
                // let yAxisList = [];
                const legend = [];
                const series = [];

                this.resetChartData(this.echartsInstance);

                chartDataSeries.forEach((cd) => {
                    cd.data.series[0].type = 'scatter';
                    series.push(cd.data.series[0]);
                    legend.push(cd.data.legend.data);
                    // xAxisList.push(cd.data.xAxis);
                    // yAxisList.push(cd.data.yAxis);
                    this.xAxis.push(cd.data.xAxis);
                    this.yAxis.push(cd.data.yAxis);
                });
                this.chartData.series = series;
                this.chartData.legend.data = legend;
                this.chartData.xAxis = this.xAxis[0];
                this.chartData.yAxis = this.yAxis;
                setTimeout(() => {
                    this.echartsInstance.setOption(this.chartData);
                    this.setChartLayout();
                }, 10);
            });
        // Set the layout
        this.chartLayoutSet
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((layoutOptions: BarChartLayoutOptions) => {
                this.pending = false;
                this.renderChart(layoutOptions);
            });

        // When data is fetched from URL format the series
        this.chartDataService.dataFetched$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id){
                    this.pending = false;
                    this.chartDataService.getSeriesFromResults(this.widget, chartObject.data);
                }
            });

        // When series are formatted set the data on chart
        this.chartDataService.seriesFormatted$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id) {
                    this.chartDataFetched.next(chartObject.data);
                }
            });
    }
    ngAfterViewInit(): void {
        this.cd.detectChanges();
    }

    onChartInit(e: ECharts): void {
        this.echartsInstance = e;
        this.chartInitialized.next(e);
    }

    onChartLegendSelected(event: any, type: string): void {
        this.filter.emit(event['selected']);
    }

    ngOnChanges(changes: SimpleChanges): void {
        let loadData = false;

        // TODO fix datapoint count
        this.widget.datapointCount = 50;

        if (changes?.widget && changes.widget.isFirstChange()) {
            return;
        }
        if (this.widget && this.duration) {
            loadData = true;
        }
        if (changes?.selectedRange && changes.selectedRange.currentValue) {
            this.startTime = changes.selectedRange.currentValue.start;
            this.endTime = changes.selectedRange.currentValue.end;
            this.step = Math.round((changes.selectedRange.currentValue.end - changes.selectedRange.currentValue.start) / this.widget.datapointCount);
            loadData = true;
        }

        if (changes?.refreshTime && changes.refreshTime.currentValue) {
            loadData = true;
        }

        if (changes?.interval && changes.interval.currentValue) {
            this.unsubscribeTimer$.next();
            if (changes?.interval.currentValue && typeof changes.interval.currentValue.value === 'number') {
                const setInterval = timer(0, changes.interval.currentValue.value);
                setInterval.pipe(takeUntil(this.unsubscribeTimer$)).subscribe(() => this.getData());
            } else {
                this.unsubscribeTimer$.next();
            }
        }

        // TODO: refactor this
        this.filterParameters.startTime = this.startTime;
        this.filterParameters.endTime = this.endTime;
        this.filterParameters.step = this.step;
        this.filterParameters.duration = this.duration;
        this.filterParameters.apigroup = this.apigroup;

        if (loadData) {
            this.getData();
        }
        this.cd.detectChanges();
    }


    setChartLayout(): void {
        const options = this.layoutService.getChartLayout(this.widget);
        options.tooltip = {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            },
            formatter: (a) => {
                let response = a.marker + ' ' + a.seriesName + ': ' + this.chartDataService.getFormattedValue(a.value, this.widget.chartQueries[0].query.spec.query_info.units);
                if (Array.isArray(a)) {
                    response = '';
                    a.forEach((av) => {
                        response += av.marker + ' ' +
                            av.seriesName + ': ' +
                            this.chartDataService.getFormattedValue(av.value, this.widget.chartQueries[0].query.spec.query_info.units) +
                            '<br />';
                    });
                }
                return response;
            }
        };
        options.grid.bottom = 0;
        this.chartLayoutSet.next(options);
    }

    getData(): void {
        this.filterParameters.startTime = this.selectedRange.start;
        this.filterParameters.endTime = this.selectedRange.end;
        this.filterParameters.step = Math.round((this.selectedRange.end -
            this.selectedRange.start) / this.widget.datapointCount);
        this.filterParameters.duration = this.selectedRange.short;
        this.filterParameters.apigroup = this.apigroup;

        this.pending = true;
        this.widgetQueries = this.widget.chartQueries.map((q) => {
            return q.query;
        });
        this.noOfQueries = this.widgetQueries.length;
        this.chartDataService
            .getDataFromQueries(this.widget, this.widgetQueries, this.filterParameters);
    }

    renderChart(options): void {
        //console.log('Rendering scatter chart');
        // options.series = this.chartData.series;
        // options.series.forEach((ser, i) => {
        //     ser.itemStyle = {
        //         color: this.widget.chartQueries[i].color
        //     };
        // });
        options.xAxis = this.xAxis[0];
        options.yAxis = this.yAxis;
        options.xAxis.axisLabel = {
            align: 'right'
        };
        this.setFontFamily(options);
        this.echartsInstance.setOption(options);
    }


    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.unsubscribeTimer$.next();
        this.unsubscribeTimer$.complete();
    }
}
