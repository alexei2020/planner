import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    HostListener,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core';
import {Widget} from '../../../..';
import {Query} from '../../../../interfaces/query';
import {forkJoin, Subject} from 'rxjs';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {PanelService, TimerService} from '../../../../../shared/services';
import {delay, takeUntil} from 'rxjs/operators';
import {FuseLocationService} from '../../../../../../services/location.service';

@Component({
  selector: 'app-summary-item',
  templateUrl: './summary-item.component.html',
  styleUrls: ['./summary-item.component.scss']
})
export class SummaryItemComponent implements OnInit, AfterViewInit, OnDestroy {

    public isActive = true;
    public changeRate: string;
    public pending = true;
    public detailView: boolean;
    public startTime: any = 1581722395;
    public endTime: any = 1581723395;
    public duration: any = 1581722395;
    public step: any = 15;
    public data: any;
    public interval: number;
    public realValue: number;
    private apigroup: any;

    private unsubscribe$: Subject<void> = new Subject<void>();

    @Input()
    public item: Widget;
    @Input()
    public index: any;
    @Input()
    public query: Query;
    @Output()
    allLoaded: EventEmitter<boolean> = new EventEmitter();
    @Output()
    filter: EventEmitter<any> = new EventEmitter();

    @HostListener('window:resize', ['$event'])
    onResize(): void {
        this.cd.detectChanges();
    }

    constructor(
        private route: Router,
        private location: Location,
        private cd: ChangeDetectorRef,
        private panelService: PanelService,
        private timerService: TimerService,
        private _locationService: FuseLocationService,
    ) {
        this.route.events.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
            const url = this.location.path().split('/');

            this.detailView = url[2] && url[2] === 'panel';
        });

        this.timerService.getRefreshObs().pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
            if (res) {
                this.getData();
            }
        });
        this.timerService.getIntervalObs().pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
            const self = this;
            if (res && typeof res.value === 'number') {
                window.clearInterval(this.interval);
                this.interval = window.setInterval(() => {
                    self.getData();
                }, res.value);
            } else {
                window.clearInterval(this.interval);
            }
        });
    }

    ngOnInit(): void {
        this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
        this.timerService.getDateRangeObs().pipe(takeUntil(this.unsubscribe$), delay(0)).subscribe((res: any) => {
            if (res) {
                this.startTime = res.start;
                this.endTime = res.end;
                this.duration = res.short;
                this.step = Math.round((res.end - res.start) / this.item.datapointCount);
                this.getData();
            }
        });
        this.cd.detectChanges();
    }

    onTableClick($event: any): void {
        this.isActive = !this.isActive;
        const data = {};
        data[$event] = this.isActive;
        this.filter.emit(data);
    }

    replace(value: string, matchingString: string, replacerString: string): string {
        return value.replace(matchingString, replacerString);
    }

    ngAfterViewInit(): void {
        // this.timerService.getDateRangeObs().pipe(takeUntil(this.unsubscribe$)).subscribe((res: any) => {
        //     if (res) {
        //         this.startTime = res.start;
        //         this.endTime = res.end;
        //         this.duration = res.short;
        //         this.step = Math.round((res.end - res.start) / this.item.datapointCount);
        //         this.getData();
        //     }
        // });
        // this.cd.detectChanges();
    }

    getData(): void {
        if (this.query.spec.query_info !== undefined) {
            let url = this.query.spec.query_info.base_url;
            url = this.replace(url, '+', '%2B');
            url = this.replace(url, '{{DURATION}}', `${this.duration}`);
            url = this.replace(url, '{{DURATION}}', `${this.duration}`);
            url = this.replace(url, '{{STARTTIME}}', `${this.startTime}`);
            url = this.replace(url, '{{ENDTIME}}', `${this.endTime}`);
            url = this.replace(url, '{{STEP}}', `${this.step}`);
            url = this.replace(url, '{{STEP}}', `${this.step}`);
            url = this.replace(url, '{{APIGROUP}}', `${this.apigroup}`);

            let prev_url = this.query.spec.query_info.prev_url;
            prev_url = this.replace(prev_url, '+', '%20');
            prev_url = this.replace(prev_url, '+', '%20');
            prev_url = this.replace(prev_url, '{{DURATION}}', `${this.duration}`);
            prev_url = this.replace(prev_url, '{{DURATION}}', `${this.duration}`);
            prev_url = this.replace(prev_url, '{{STARTTIME}}', `${this.startTime}`);
            prev_url = this.replace(prev_url, '{{ENDTIME}}', `${this.endTime}`);
            prev_url = this.replace(prev_url, '{{STEP}}', `${this.step}`);
            prev_url = this.replace(prev_url, '{{STEP}}', `${this.step}`);
            prev_url = this.replace(prev_url, '{{APIGROUP}}', `${this.apigroup}`);

            let total_url = this.query.spec.query_info.total_url;
            total_url = this.replace(total_url, '+', '%20');
            total_url = this.replace(total_url, '+', '%20');
            total_url = this.replace(total_url, '{{DURATION}}', `${this.duration}`);
            total_url = this.replace(total_url, '{{DURATION}}', `${this.duration}`);
            total_url = this.replace(total_url, '{{STARTTIME}}', `${this.startTime}`);
            total_url = this.replace(total_url, '{{ENDTIME}}', `${this.endTime}`);
            total_url = this.replace(total_url, '{{STEP}}', `${this.step}`);
            total_url = this.replace(total_url, '{{STEP}}', `${this.step}`);
            total_url = this.replace(total_url, '{{APIGROUP}}', `${this.apigroup}`);

            this.pending = true;

            forkJoin(
                this.panelService.getPanelData(url),
                this.panelService.getPanelData(prev_url),
                this.panelService.getPanelData(total_url)
            ).subscribe(
                (res: any) => {
                    res[0].data['name'] = this.query.spec.query_info.title;
                    this.data = res[0].data;
                    const currentData = res[0].data;
                    const previousData = res[1].data;
                    const totalData = res[2].data;
                    if (currentData.result[0] && totalData.result[0]) {
                        this.realValue =
                            parseInt(currentData.result[0].value[1], 0) / parseInt(totalData.result[0].value[1], 0) * 100;
                    }
                    if (currentData.result[0] && previousData.result[0]) {
                        const change =
                            Math.round(currentData.result[0].value[1]) - Math.round(previousData.result[0].value[1]);
                        this.changeRate =
                            change < 0
                                ? `- ${Math.abs(change)} from ${this.duration} ago`
                                : `+ ${change} from ${this.duration} ago`;

                    }
                    this.allLoaded.emit(true);
                    this.pending = false;
                },
                (error) => {
                    console.log('Error', error);
                    this.pending = false;
                }
            );
        }
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
