import {
    ChangeDetectorRef,
    Component,
    EventEmitter,
    HostListener,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output, SimpleChanges,
    ViewChild, ViewChildren
} from '@angular/core';
import {BaseChartDirective, Color, Label} from 'ng2-charts';
import {Widget} from '../../..';
import {ChartDataSets, ChartType} from 'chart.js';
import {ECharts} from 'echarts';
import {Query} from '../../../interfaces/query';
import {Subject, timer} from 'rxjs';
import {BarChartLayoutOptions} from '../../../interfaces/barChartOptions';
import {Location} from '@angular/common';
import {ConfigService} from '../../../../services/config.service';
import {LayoutService, PanelService} from '../../../../shared/services';
import {ChartDataService} from '../../../../shared/services/chart-data.service';
import {takeUntil} from 'rxjs/operators';
import {GridsterItemComponent} from 'angular-gridster2';
import {AbstractChartComponent} from '../abstract-chart.component';
import {SummaryItemComponent} from './summary-item/summary-item.component';
import {FuseLocationService} from '../../../../../services/location.service';

@Component({
  selector: 'app-summary-chart',
  templateUrl: './summary-chart.component.html',
  styleUrls: ['./summary-chart.component.scss']
})
export class SummaryChartComponent extends AbstractChartComponent implements OnInit, OnChanges, OnDestroy {


    @ViewChild(BaseChartDirective) chartSummaryContainer: BaseChartDirective;

    @Input() public widget: Widget;
    @Input() public index: number;

    @Output() total = new EventEmitter();
    @Output() filter: EventEmitter<any> = new EventEmitter();

    public chartLabels: Label[] = [];
    private apigroup: any;
    public isSmallSize = false;

    @ViewChildren(SummaryItemComponent)
    listSummaryItems: SummaryItemComponent[];

    public options: any = {};
    public isActive = true;
    public changeRate: number;
    public change: number;
    public pending: boolean;
    public value = 80;
    public startTime: any = 1581722395;
    public endTime: any = 1581723395;
    public duration: any = 1581722395;
    public step = 15;
    public data: any;
    public colors: any = {};
    public interval: number;
    public seriesData = [];
    public realValue: number;

    public echartsInstance: ECharts;
    private isDataRefresh = false;
    public chartWidth: number;
    public chartHeight: number;
    private noOfQueries: number;
    private widgetQueries: Query[];
    public filterParameters = {
        startTime: 1581722395,
        endTime: 1581722395,
        duration: 1581722395 || '2m',
        step:  15,
        apigroup: ''
    };

    private xAxis;
    private yAxis;

    private chartInitialized: Subject<ECharts> = new Subject<ECharts>();
    private chartLayoutSet: Subject<BarChartLayoutOptions> = new Subject<BarChartLayoutOptions>();
    private chartDataFetched: Subject<any> = new Subject<any>();


    private unsubscribe$: Subject<void> = new Subject<void>();
    private unsubscribeTimer$: Subject<void> = new Subject<void>();

    private _isAllPending: boolean;

    public get allIsPending(): boolean {
        // if (!this.listSummaryItems) {
        //     this._isAllPending = true;
        //     return this._isAllPending;
        // }
        this._isAllPending = this.listSummaryItems?.some((item) =>  {
            return item.pending;
        });
        return this._isAllPending;
    }

    public set allIsPending(value) {
        this._isAllPending = value;
    }

    @HostListener('window:resize', ['$event'])
    onResize(): void {
        this.cd.detectChanges();
    }

    constructor(
        private location: Location,
        private configSvc: ConfigService,
        private cd: ChangeDetectorRef,
        private panelService: PanelService,
        private layoutService: LayoutService,
        private chartDataService: ChartDataService,
        private _locationService: FuseLocationService,
    ) {
        super();
        // this.resetChartData(this.echartsInstance);

        // AB redraw chart when gridster item resized
        this.layoutService.itemResized$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((gridsterItemComponent: GridsterItemComponent) => {
                // console.log('Radi RESIZE');
                if (gridsterItemComponent.item.id === this.widget.id) {
                    // this.chartWidth = gridsterItemComponent.width;
                    const aspectRatio = gridsterItemComponent.width /  gridsterItemComponent.height;
                    this.chartHeight = gridsterItemComponent.height - 100;
                    this.chartWidth = this.chartHeight * aspectRatio;
                    this.isSmallSize = this.chartHeight <= 150 || this.chartWidth <= 336;
                }
            });
    }

    ngOnInit(): void {
        this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
        this.listSummaryItems = [];
        // this.allIsPending = false;
        this.ngOnChanges(undefined);
        this.pending = false;
        // this.resetChartData(this.echartsInstance);

        // When chart is initialized, init the layout
        this.chartInitialized
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((e: ECharts) => {
                this.getData();
            });
        // When data is fetched and formatted, update data on charts
        this.chartDataFetched
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartDataSeries: any) => {
                this.resetChartData(this.echartsInstance);
                this.data = chartDataSeries.data;
                this.total.emit(chartDataSeries.percentage);
                this.realValue = chartDataSeries.realValue;
                this.change = chartDataSeries.change;
                this.changeRate = chartDataSeries.changeRate;
                this.seriesData = [];
                this.chartData.series = [];

                chartDataSeries.seriesData.result[0].values.forEach((val) => {
                    this.seriesData.push(parseInt(val[1], 0));
                });
                this.chartData.xAxis = {
                        show: false,
                        type: 'category'
                    };
                this.chartData.yAxis = {
                        show: false,
                        type: 'value'
                    };
                this.chartData.series = {
                        data: this.seriesData,
                        type: 'bar'
                    };
                setTimeout(() => {
                    this.echartsInstance?.setOption(this.chartData);
                    this.setChartLayout();
                }, 10);
            });
        // Set the layout
        this.chartLayoutSet
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((layoutOptions: BarChartLayoutOptions) => {

                // console.log('SET layout', layoutOptions);
                this.pending = false;
                this.renderChart(layoutOptions);
            });

        // When data is fetched from URL format the series
        this.chartDataService.dataFetched$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id){
                    this.pending = false;
                    this.chartDataService.getSummarySeriesFromResults(this.widget, chartObject.data);
                }
            });

        // When series are formatted set the data on chart
        this.chartDataService.seriesFormatted$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((chartObject) => {
                if (this.widget.id === chartObject.widget.id) {
                    this.chartDataFetched.next(chartObject.data);
                }
            });
    }
    onChartInit(e: ECharts): void {
        this.echartsInstance = e;
        this.chartInitialized.next(e);
    }

    public onTableClick($event?: any): void {
        this.isActive = !this.isActive;
        const data = {};
        data['name'] = 'all';
        data['filter'] = !this.isActive;
        this.filter.emit(data);
    }

    ngOnChanges(changes: SimpleChanges): void {
        // if (this.widget?.chartVariant === 'without-chart') {
        //     return;
        // }

        let loadData = false;

        // TODO fix datapoint count
        this.widget.datapointCount = 9;

        if (changes?.widget && changes.widget.isFirstChange()) {
            return;
        }
        if (this.widget && this.duration) {
            loadData = true;
        }
        if (changes && changes.selectedRange && changes.selectedRange.currentValue) {
            this.startTime = changes.selectedRange.currentValue.start;
            this.endTime = changes.selectedRange.currentValue.end;
            this.duration = changes.selectedRange.currentValue.short;
            this.step = Math.round((changes.selectedRange.currentValue.end - changes.selectedRange.currentValue.start) / this.widget.datapointCount);
            loadData = true;
        }

        if (changes && changes.refreshTime && changes.refreshTime.currentValue !== undefined) {
            loadData = true;
        }

        if (changes && changes.interval && changes.interval.currentValue) {
            this.unsubscribeTimer$.next();
            if (changes.interval.currentValue && typeof changes.interval.currentValue.value === 'number') {
                const setInterval = timer(0, changes.interval.currentValue.value);
                setInterval.pipe(takeUntil(this.unsubscribeTimer$)).subscribe(() => this.getData());
            } else {
                this.unsubscribeTimer$.next();
            }
        }

        // TODO: refactor this
        this.filterParameters.startTime = this.startTime;
        this.filterParameters.endTime = this.endTime;
        this.filterParameters.step = this.step;
        this.filterParameters.duration = this.duration;
        this.filterParameters.apigroup = this.apigroup;

        if (loadData) {
            this.getData();
        }
        this.cd.detectChanges();
    }

    setChartLayout(): void {
        // Initialize layout of chart
        const options = this.layoutService.getChartLayout(this.widget);
        options.tooltip = {
            formatter: (a) => {
                // console.log(a);
                return a.marker + ' ' + this.chartDataService.getFormattedValue(a.value, this.widget.chartQueries[0].query.spec.query_info.units);
            }
        };
        // options.grid.bottom = '60px';
        this.chartLayoutSet.next(options);
    }

    public getData(): void {
        this.filterParameters.startTime = this.selectedRange.start;
        this.filterParameters.endTime = this.selectedRange.end;
        this.filterParameters.step = Math.round((this.selectedRange.end -
            this.selectedRange.start) / this.widget.datapointCount);
        this.filterParameters.duration = this.selectedRange.short;
        this.filterParameters.apigroup = this.apigroup;

        this.pending = true;
        this.widgetQueries = this.widget.chartQueries.map((q) => {
            return q.query;
        });
        this.noOfQueries = this.widgetQueries.length;
        this.chartDataService
            .getSummaryChartDataFromQueries(this.widget, this.widgetQueries, this.filterParameters);
    }

    renderChart(options): void {
        switch (this.widget.chartVariant) {
            case 'without-chart':
                this.renderWithoutChart(options);
                break;
            case 'with-chart':
                this.renderWithChart(options);
                break;
            case 'percentage':
                this.renderPercentage(options);
                break;
        }
    }

    private renderPercentage(options): void {
        //console.log('Rendering percentage');
        this.echartsInstance?.setOption(options);
    }

    private renderWithoutChart(options): void {
        //console.log('Rendering without chart');
        // this.echartsInstance.setOption(options);
    }

    private renderWithChart(options): void {
        //console.log('Rendering with chart');

        options.xAxis = this.chartData.xAxis;
        options.yAxis = this.chartData.yAxis;
        // this.chartData.xAxis = {
        //         show: false,
        //         type: 'category'
        //     };
        // this.chartData.yAxis = {
        //         show: false,
        //         type: 'value'
        //     };
        options.color = [this.widget.chartQueries[0].color];
        // options.grid.left = 0;
        // options.grid.right = 0;
        // options.grid.bottom = 220;
        // options.grid.top = 0;
        this.echartsInstance.setOption(options);
    }

    updateFilter(e): void {
        const filter = this.listSummaryItems.map((item) => ({
            name: item.data.name,
            state: item.isActive,
        })).reduce((acc, cur, i) => {
            acc[cur.name] = cur.state;
            return acc;
        }, {});

        this.filter.emit(filter);
    }


    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
