import { Component, OnInit, AfterViewInit, ViewChild, ChangeDetectorRef, Input, SimpleChanges, Output, EventEmitter, OnDestroy } from '@angular/core';

import { Subject, timer } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';

import { DataBindingDirective } from '@progress/kendo-angular-grid';
import { process, GroupDescriptor } from '@progress/kendo-data-query';

import { PanelService } from '../../../shared/services/panel.service';
import { AbstractWidgetComponent } from '../abstract-widget/abstract-widget.component';
import {FuseLocationService} from '../../../../services/location.service';

@Component({
	selector: 'app-kendo-table',
	templateUrl: 'kendo-table.component.html',
	styleUrls: ['kendo-table.component.scss']
})
export class KendoTableComponent extends AbstractWidgetComponent implements OnInit, OnDestroy, AfterViewInit {
	@ViewChild('grid') private grid;
	@Input() pageSize = 10;
	@Input() searchText;
	@Input() objName;
	@Input() namespace;
	@Output() setRows: any = new EventEmitter();

	public _excludeSegmentItemID: any;
	public isShowTable: any;
	public _excludeSegmentItemName: any;
	public panelData$: Subject<any> = new Subject;
	public skip = 0 as number;
	private listOfObservable: any[] = [];
	private unsubscribeTimer$: Subject<void> = new Subject<void>();
	private _unsubscribeAll: Subject<void> = new Subject<void>();
	private apigroup: any = 'fanta.ws.io';

	constructor(
		private cd: ChangeDetectorRef,
		private panelService: PanelService,
    private _locationService: FuseLocationService,
	) {
		super();
	}


	public gridView: any[];

	@Input('filter')
	set filter(query: any) {
		if (query) {
			const array = Object.keys(query).filter(key => !!query[key]);
			this._excludeSegmentItemName = [...array];
			this.currentView = array.length > 0 && Object.keys(query).length === array.length ? 'all' : 'filter';
			this.getData();
		}
	}

	_filter: string;
	displayedColumns = [];
	dataSource: any;
	startTime: any = 1581722395;
	endTime: any = 1581723395;
	step: any = 1;
	currentView = 'all';
	pending: boolean;
	themeSubscription: any;
	options: any = {};
	colors: any;
	echarts: any;
	interval;
	duration;
	public groupable = false;
	public previousSize: number;
	public mySelection: string[] = [];
	public groups: GroupDescriptor[] = [];
	replace(value, matchingString, replacerString) {
	  // TODO: Replace all occurances
		if (value) return value.replace(matchingString, replacerString).replace(matchingString, replacerString);
		else return '';
	}
	queryName(name: string) {
		switch (name) {
			case 'throughput-wired':
				return '{{ELLIPEE}}';
			case 'throughput-wireless':
				return '{{ELLIPEE}}';
			case 'traffic-by-app-category':
				return '{{APPCATEGORY}}';
			case 'traffic-by-suite':
				return '{{SUITE}}';
			case 'traffic-by-app-name':
				return '{{APPNAME}}';
			case 'traffic-by-os':
				return '{{OS}}';
			case 'traffic-by-room':
				return '{{ROOM}}';
			case 'traffic-by-vendor':
				return '{{VENDOR}}';
			case 'traffic-by-room':
				return '{{ROOM}}';
			default:
				return '{{ELLIPEE}}';
		}
	}

	ngOnChanges(changes: SimpleChanges): void {
		let loadData = false;

		if (changes['_excludeSegmentItemName']) {
			this.getFilterData();
		}
	
		if (changes.selectedRange && changes.selectedRange.currentValue) {
			this.duration = changes.selectedRange.currentValue.short;
			this.startTime = changes.selectedRange.currentValue.start;
			this.endTime = changes.selectedRange.currentValue.end;
			this.step = Math.round((changes.selectedRange.currentValue.end - changes.selectedRange.currentValue.start) / this.widget.datapointCount);
			loadData = true;
		}

		if (changes.refreshTime && changes.refreshTime.currentValue !== undefined) {
			loadData = true;
		}

		if (changes.interval && changes.interval.currentValue) {
			this.unsubscribeTimer$.next();
			if (changes.interval.currentValue && typeof changes.interval.currentValue.value === 'number') {
				const setInterval = timer(0, changes.interval.currentValue.value);
				setInterval.pipe(takeUntil(this.unsubscribeTimer$)).subscribe(() => this.getData())
			} else {
				this.unsubscribeTimer$.next();
			}
		}

		if ((changes.pageSize && changes.pageSize.currentValue)) {
			console.log(changes.pageSize.currentValue);
			this.setHeight();
		}

		if (loadData) {
			this.getData();
		} else if (changes.searchText) {
			this.onFilter(changes.searchText.currentValue);
		}

		this.cd.detectChanges();
	}

	getData() {
		this.currentView == 'all' ? this.getAllData() : this.getFilterData();
	}

	getFilterData() {
		let filters = [];
		if (this.widget.chartQueries.length > 1) {
			filters = this.widget.chartQueries.filter((itemQ) =>
				this._excludeSegmentItemName.includes(itemQ.query.spec.query_info.title)
			).map(query => ({
				url: query.query.spec.query_info.filtered_data_url,
				metricName: query.query.metadata.name,
				title: query.query.spec.query_info.title,
			}));
		} else {
			filters = this._excludeSegmentItemName.map(title => ({
				url: this.widget.chartQueries[0].query.spec.query_info.filtered_data_url,
				metricName: this.widget.chartQueries[0].query.metadata.name,
				title: title,
			}))
		}

		this._getFilterData(filters);
	}

	getAllData() {
		let url = this.widget.chartQueries[0].query.spec.query_info.all_data_url;
			// this.widget.chartType === 'summary'
			// 	? this.widget.chartQueries[2].query.spec.query_info.all_data_url
			// 	: this.widget.chartQueries[0].query.spec.query_info.all_data_url;
		url = this.replace(url, '+', '%2B');
		url = this.replace(url, '{{STARTTIME}}', `${this.startTime}`);
		url = this.replace(url, '{{ENDTIME}}', `${this.endTime}`);
		url = this.replace(url, '{{DURATION}}', `${this.duration}`);
		url = this.replace(url, '{{DURATION}}', `${this.duration}`);
		url = this.replace(url, '{{STEP}}', `${this.step}`);
		url = this.replace(url, '{{APIGROUP}}', `${this.apigroup}`);
		url = this.replace(url, '{{OBJNAME}}', `${this.objName}`);
		this.pending = true;
		this.panelService.getPanelData(url).pipe(takeUntil(this._unsubscribeAll)).subscribe(
			(res: any) => {
				const data = res.data.result;
				const column = [];
				for (const key in data[0].metric) {
					column.push(this.replace(key, '.', '_'));
				}
				let metricData = data.map(function(res){
				  let res1 = {};
				  for(var k in res.metric) {
				    res1[k.replace('.', '_').replace('.', '_')] = res.metric[k];
				  }
				  return res1;
				});
				this.displayedColumns = column;
				this.dataSource = metricData.map((result, index) => ({ id: index, ...result}));
				this.panelData$.next(this.dataSource);
				this.pending = false;
				this.setHeight();
			},
			() => {
				this.pending = false;
			}
		);
	}

	public ngOnInit(): void {
		this.panelData$.next(this.dataSource);
    this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
	}

    ngAfterViewInit() {
        this.cd.detectChanges();
    }

	public onFilter(inputValue: string): void {
		this.panelData$.next(process(this.dataSource, {
			filter: {
				logic: 'or',
				filters: this.displayedColumns.map((col) => ({
					field: col,
					operator: 'contains',
					value: inputValue
				}))
			}
		}).data);
		this.grid.skip = 0;
	}

	public onExportPDFHandler() {
		this.grid.saveAsPDF();
	}

	public onExportExcelHandler() {
		this.grid.saveAsExcel();
	}

	public onGroupable() {
		this.groupable = !this.groupable;
		this.groups = [];
		this.setHeight();
	}

	setHeight() {
		setTimeout(() => {
			if (this.grid && this.grid.wrapper) {
				let size = 0;
				if (this.grid.wrapper) {
					const header = this.grid.wrapper.nativeElement.querySelector('.k-grouping-header'); 
					const grid = this.grid.wrapper.nativeElement.querySelector('.k-grid-aria-root');
					const pager = this.grid.wrapper.nativeElement.querySelector('.k-pager-wrap');
					if (header) {
						size += header.offsetHeight;
					}
					if (grid) {
						size += grid.offsetHeight;
					}
					if (pager) {
						size += pager.offsetHeight;
					}
				}

				if (size == 0) {
					size = this.grid.wrapper.nativeElement.offsetHeight;
				}

				this.previousSize = size;
				console.log(size)

				this.setRows.emit(size);
			}
		});
	}

	private _getFilterData(filtredData: { url: string, metricName: string, title: string }[] = []) {

		if (this.listOfObservable && this.listOfObservable.length > 0) {
			this.listOfObservable.forEach(item => item.unsubscribe());
		}

		this.listOfObservable = [];
		const numberOfCalls = filtredData.length

		// Clean table only when we stop previous observation
		this.dataSource = [];
		if (!numberOfCalls || numberOfCalls == 0) {
			this.panelData$.next(this.dataSource);
			return;
		}
		filtredData.forEach(item => {
			let url = item.url;
			const REPLACE = this.queryName(item.metricName);
			url = this.replace(url, '+', '%2B');
			url = this.replace(url, REPLACE, `"${item.title}"`);
			url = this.replace(url, REPLACE, `"${item.title}"`);
			url = this.replace(url, '{{DURATION}}', `${this.duration}`);
			url = this.replace(url, '{{DURATION}}', `${this.duration}`);
			url = this.replace(url, '{{STARTTIME}}', `${this.startTime}`);
			url = this.replace(url, '{{ENDTIME}}', `${this.endTime}`);
			url = this.replace(url, '{{STEP}}', `${this.step}`);
			url = this.replace(url, '{{APIGROUP}}', `${this.apigroup}`);
			url = this.replace(url, '{{OBJNAME}}', `${this.objName}`);
			this.pending = true;
			this.listOfObservable.push(this.panelService.getPanelData(url).pipe(take(1)).subscribe(
				(res: any) => {
					const data = res.data.result;
					const column = [];
					for (const key in data[0].metric) {
						column.push(this.replace(key, '.', '_'));
					}
				let metricData = data.map(function(res){
				  let res1 = {};
				  for(var k in res.metric) {
						res1[k.replace('.', '_').replace('.', '_')] = res.metric[k];
				  }
				  return res1;
				});
				  this.dataSource = metricData.map((result, index) => ({ id: index, ...result}));
					this.panelData$.next(this.dataSource);
					this.pending = false;
				},
				(error) => {
					this.pending = false;
				}
			));
		})
	}

	ngOnDestroy(): void {
        this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
    }
}
