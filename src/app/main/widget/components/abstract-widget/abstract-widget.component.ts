import { Input } from '@angular/core';
import { Widget } from '../../interfaces/widget';
import {BarChartSeriesOptions} from '../../interfaces/barChartOptions';
import ECharts = echarts.ECharts;
import {FUSE_THEME} from '../../../theme/default-theme';

export abstract class AbstractWidgetComponent {

    @Input() public widget: Widget;
    @Input() public selectedRange: any;
    @Input() public refreshTime: any;
    @Input() public interval: any;


    public chartData: BarChartSeriesOptions;
    public colorScheme = [
      FUSE_THEME.variables.crimson,
      FUSE_THEME.variables.darkorchid,
      FUSE_THEME.variables.cornflowerblue,
      FUSE_THEME.variables.darkorange,
      FUSE_THEME.variables.yellowgreen,
      FUSE_THEME.variables.dimgray,
    ];

    resetChartData(echart: ECharts): void {
        this.chartData = new BarChartSeriesOptions();
        this.chartData.legend = {
            data: []
        };
        this.chartData.series = [];
        this.chartData.xAxis = [];
        this.chartData.yAxis = [];
        echart?.setOption(this.chartData);
    }
}
