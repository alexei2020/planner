import {
    Component,
    Input,
    OnInit,
    SimpleChanges,
    OnChanges,
    ElementRef,
    Output,
    EventEmitter, OnDestroy
} from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

import { MatDialog } from '@angular/material/dialog';


import { KendoReportItemDialog } from "../../../shared/dialogs/kendo-report-item-dialog/kendo-report-item-dialog.component";
import { KendoDashboardItemDialog } from "../../../shared/dialogs/kendo-dashboard-item-dialog/kendo-dashboard-item-dialog.component";
import {
  DialogCloseResult,
  DialogService,
  DialogResult,
} from "@progress/kendo-angular-dialog";
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FuseSearchBarService } from '@fuse/services/search-bar.service';

@Component({
	selector: 'shared-table-header-widget',
	templateUrl: './shared-table-header-widget.component.html',
	styleUrls: ['./shared-table-header-widget.component.scss']
})
export class SharedTableHeaderWidgetComponent implements OnInit, OnDestroy, OnChanges {
    @Input() item: any;
    @Input() isMenuPanel?: any;
    @Input() isContolPanel?: any;
    @Input() index: any;
    @Input() resizeble?: any;
    @Input() isMenuDisabled?: boolean;
    @Input() isSearchBoxDisabled?: boolean;

    @Output() 
    filterChanged: EventEmitter<string>;

    @Output() 
    pageSizeChanged: EventEmitter<number>;
 
    public pageSize = { label: '20', value: 20 };
    public pageSizes = [
        { label: '10', value: 10 },
        { label: '20', value: 20 },
        { label: '50', value: 50 },
        { label: '100', value: 100 },
    ];
    public groupable = false;
    public height = 0;

    public searchText = null;

    private unsubscribe$: Subject<void> = new Subject<void>();

    constructor(
      private _dialogService: DialogService,
      private el: ElementRef,
      iconRegistry: MatIconRegistry,
      sanitizer: DomSanitizer,
      private _searchBarService: FuseSearchBarService
    ) {
      iconRegistry.addSvgIcon(
        "table-header-search",
        sanitizer.bypassSecurityTrustResourceUrl(
          "assets/images/icons/table-header/table-header-search.svg"
        )
      );
      iconRegistry.addSvgIcon(
        "table-header-add",
        sanitizer.bypassSecurityTrustResourceUrl(
          "assets/images/icons/table-header/table-header-add.svg"
        )
      );

      this.filterChanged = new EventEmitter();
      this.pageSizeChanged = new EventEmitter();
    }

    ngOnInit(): void {
      this._searchBarService.tableSearchTerm.pipe(takeUntil(this.unsubscribe$)).subscribe((term: string)=>{
        this.searchText = term;
        this.filterChanged.next(term);
      })
        setTimeout(() => {
            this.height = this.el.nativeElement.offsetHeight;
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.resizeble && changes.resizeble.currentValue) {
            this.height = this.el.nativeElement.offsetHeight;
        }
    }

    onFilterChanged(filter: string): void {
      this.filterChanged.next(filter);
    }

    onPageSizeChanged(size: { label: string, value: number }): void {
      this.pageSizeChanged.next(size.value);
    }

    onAddToReport(): void {
      const dialogRef = this._dialogService.open({
        title: "Show in reports",
        content: KendoReportItemDialog,
        width: "300px",
        height: "200px",
      });
      const dialogInstance = dialogRef.content.instance;
      dialogInstance.dialog = dialogRef;
      dialogInstance.data = {
        title: "Show in reports",
        item: this.item,
      };
      dialogRef.result.pipe(takeUntil(this.unsubscribe$)).subscribe((response: DialogResult) => {});
    }

    onAddToDashboard(): void {
      const dialogRef = this._dialogService.open({
        title: "Show in dashboards",
        content: KendoDashboardItemDialog,
        width: "300px",
        height: "200px",
      });
      const dialogInstance = dialogRef.content.instance;
      dialogInstance.dialog = dialogRef;
      dialogInstance.data = {
        title: "Show in dashboards",
        item: this.item,
      };
      dialogRef.result.pipe(takeUntil(this.unsubscribe$)).subscribe((response: DialogResult) => {});
    }

    ngOnDestroy(): void {
      this.unsubscribe$?.next();
      this.unsubscribe$?.complete();
  }
}
