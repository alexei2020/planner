import {
	Component,
	OnInit,
	HostListener,
	AfterViewInit,
	ChangeDetectorRef,
	Input,
	ElementRef,
	OnDestroy
} from '@angular/core';
import { Widget } from '../../interfaces/widget';
import { ConfigService } from '../../../services/config.service';
import { DatasourceService } from '../../services/datasource.service';
import { PanelService } from '../../../shared/services/panel.service';
import { TimerService } from '../../../shared/services/timer.service';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
	selector: 'app-table-bar',
	template: `
	<div class="table-container mat-elevation-z8">
	    <div class="view-header">
      <div class="field">
    <mat-form-field appearance="outline">
      <input matInput (keyup)="applyFilter($event.target.value)" placeholder="Filter">
    </mat-form-field>
      </div>
  </div>
  <mat-table #table [dataSource]="dataSource" matSort>
    <ng-container [matColumnDef]="col" *ngFor="let col of displayedColumns">
      <mat-header-cell *matHeaderCellDef mat-sort-header> {{ col }} </mat-header-cell>
      <mat-cell *matCellDef="let element"> {{ element[col] }} </mat-cell>
    </ng-container>
    <mat-header-row *matHeaderRowDef="displayedColumns"></mat-header-row>
    <mat-row *matRowDef="let row; columns: displayedColumns;"></mat-row>
  </mat-table>
</div>
	`,
	styleUrls: [ './table.component.scss' ]
})
export class TableBarComponent implements AfterViewInit, OnDestroy {
	private unsubscribe$: Subject<void> = new Subject<void>();
	@Input() public widget: Widget;
	@Input('filter')
	set filter(query: string) {
		if (query) {
			this.dataSource = this.storeSource;
			// console.log(this.widget);
			// console.log(query);
			// this._excludeSegmentItemName = query;
			// this.getFilterData();
		} else {
			this.storeSource = this.dataSource;
			this.dataSource = new MatTableDataSource([]);
		}
	}

	@Input('reset')
	set reset(data: boolean) {
		if (data) {
			this.getAllData();
		}
	}

	_excludeSegmentItemID: string;
	_excludeSegmentItemName: string;
	_filter: string;
	displayedColumns = [];
	dataSource: any = new MatTableDataSource([]);
	storeSource = new MatTableDataSource([]);

	startTime: any = 1581722395;
	endTime: any = 1581723395;
	step: any = 1;
	currentView = 'all';
	pending: boolean;
	themeSubscription: any;
	options: any = {};
	colors: any;
	echarts: any;
	interval;
	duration;

	constructor(
		private configSvc: ConfigService,
		private cd: ChangeDetectorRef,
		private panelService: PanelService,
		private timerService: TimerService
	) {
		this.timerService.getRefreshObs().pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
			if (res) {
				console.log('--refresh obs table-bar');
				this.getData();
			}
		});
		this.timerService.getIntervalObs().pipe(takeUntil(this.unsubscribe$)).subscribe((res) => {
			const self = this;
			if (res && typeof res.value === 'number') {
				window.clearInterval(this.interval);
				this.interval = window.setInterval(function() {
					self.getData();
				}, res.value);
			} else {
				window.clearInterval(this.interval);
			}
		});
	}
	replace(value, matchingString, replacerString) {
		return value.replace(matchingString, replacerString);
	}

	applyFilter(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
		this.dataSource.filter = filterValue;
	}
	queryName(name: string) {
		switch (name) {
			case 'throughput-wired':
				return '{{ELLIPEE}}';
			case 'throughput-wireless':
				return '{{ELLIPEE}}';
			case 'traffic-by-app-category':
				return '{{APPCATEGORY}}';
			case 'traffic-by-suite':
				return '{{SUITE}}';
			case 'traffic-by-app-name':
				return '{{APPNAME}}';
			case 'traffic-by-os':
				return '{{OS}}';
			case 'traffic-by-room':
				return '{{ROOM}}';
			case 'traffic-by-vendor':
				return '{{VENDOR}}';
			case 'traffic-by-room':
				return '{{ROOM}}';
			default:
				return '{{ELLIPEE}}';
		}
	}
	ngAfterViewInit() {
		this.dataSource.filterPredicate = (data: any, filter: string) => {
			for (const key in data) {
				return data[key].toLowerCase().includes(filter);
			}
		};
		this.timerService.getDateRangeObs().pipe(takeUntil(this.unsubscribe$)).subscribe((res: any) => {
			if (res) {
				console.log('--daterange obs table-bar');
				this.startTime = res.start;
				this.endTime = res.end;
				this.duration = res.short;
				this.step = Math.round((res.end - res.start) / this.widget.type.spec.panel_datapoint_count);
				this.getData();
			}
		});
		this.cd.detectChanges();
	}
	getData() {
		this.currentView == 'all' ? this.getAllData() : this.getFilterData();
	}

	getFilterData() {
		this.dataSource = [];
		this.dataSource = [];

		let url = this.widget.chartQueries[0].query.spec.query_info.filtered_data_url;
		const name = this.widget.chartQueries[0].query.metadata.name;
		const REPLACE = this.queryName(name);
		url = this.replace(url, '+', '%2B');
		url = this.replace(url, REPLACE, `"${this._excludeSegmentItemName}"`);
		url = this.replace(url, REPLACE, `"${this._excludeSegmentItemName}"`);
		url = this.replace(url, '{{DURATION}}', `${this.duration}`);
		url = this.replace(url, '{{DURATION}}', `${this.duration}`);
		url = this.replace(url, '{{STARTTIME}}', `${this.startTime}`);
		url = this.replace(url, '{{ENDTIME}}', `${this.endTime}`);
		url = this.replace(url, '{{STEP}}', `${this.step}`);
		this.pending = true;
		this.panelService.getPanelData(url).pipe(takeUntil(this.unsubscribe$)).subscribe(
			(res: any) => {
				const data = res.data.result;
				const column = [];
				for (const key in data[0].metric) {
					column.push(key);
				}
				// this.displayedColumns = [ ...column ];
				const arr = [ ...this.dataSource, ...data.map((result) => result.metric) ];
				this.dataSource = new MatTableDataSource(arr);
			},
			(error) => {
				this.pending = false;
			}
		);
	}
	getAllData() {
		this.dataSource = new MatTableDataSource([]);
		console.log('--- in table-bar all data---');
		let url =
			this.widget.type.metadata.name == 'summary-bar'
				? this.widget.chartQueries[2].query.spec.query_info.all_data_url
				: this.widget.chartQueries[0].query.spec.query_info.all_data_url;
		if (typeof url === 'undefined') {
			return;
		}
		url = this.replace(url, '+', '%2B');
		url = this.replace(url, '{{STARTTIME}}', `${this.startTime}`);
		url = this.replace(url, '{{ENDTIME}}', `${this.endTime}`);
		url = this.replace(url, '{{startTime}}', `${this.startTime}`);
		url = this.replace(url, '{{endTime}}', `${this.endTime}`);
		url = this.replace(url, '{{DURATION}}', `${this.duration}`);
		url = this.replace(url, '{{DURATION}}', `${this.duration}`);
		url = this.replace(url, '{{step}}', `${this.step}`);
		this.pending = true;
		this.panelService.getPanelData(url).pipe(takeUntil(this.unsubscribe$)).subscribe(
			(res: any) => {
				const data = res.data.result;
				const column = [];
				for (const key in data[0].metric) {
					column.push(key);
				}
				this.displayedColumns = column;
				const arr = [ ...this.dataSource, ...data.map((result) => result.metric) ];
				this.dataSource = new MatTableDataSource(arr);

				console.log(this.dataSource);
			},
			(error) => {
				this.pending = false;
			}
		);
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}
}
