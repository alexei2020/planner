import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoomPlannerModule } from '../../roomplanner/module';

import { RoomComponent } from './room.component';

const routes: Routes = [
    {
        path: '**',
        component: RoomComponent,
    },
];

@NgModule({
    declarations: [
        RoomComponent,
    ],
    imports: [
        RoomPlannerModule,
        RouterModule.forChild(routes),
    ],
})
export class RoomModule {
}
