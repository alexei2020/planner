import { Component } from '@angular/core';
import { appsConfig } from '../apps/apps.config';

@Component({
  selector: 'room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
})
export class RoomComponent {
  location = appsConfig.room.location;
  layers = appsConfig.room.layers;
}
