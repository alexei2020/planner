import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { Metadata } from '../../typings/backendapi';
import { Location } from '../apps/location/location';

export interface IGeoLocation {
  apiVersion?: string;
  kind?: string;
  metadata?: Metadata;
  spec?: {
    base?: {
      parent_link?: string;
    };
    description?: string;
    address?: string;
    hq?: boolean;
  };
  status?: {
    base?: {};
    geo?: {
      address?: string;
      latitude?: string;
      longitude?: string;
      timeZoneID?: string;
    }
  };
}
export interface IGeoLocations {
  apiVersion: string;
  items: IGeoLocation[];
  kind: string;
  metadata: {
    continue: string;
    resourceVersion: string;
    selfLink: string;
  };
}

export enum EventType {
  EDIT,
  ADD,
  DELETE,
  SHOW_ALL,
  NULL,
}
export interface Event {
  type: EventType;
}
@Injectable({
  providedIn: 'root',
})
export class GeolocationService {
  private _googleMapAPIURI = "https://maps.googleapis.com/maps/api/";
  public isSelected = new BehaviorSubject(false);
  public selectedNode = new BehaviorSubject(null);
  public changesExist = false;

  toBeDeletedLocations: Location[] = [];

  public changesExist$: Subject<void> = new Subject<void>();

  public event = new BehaviorSubject<Event>({ type: EventType.NULL });

  private _locationData: BehaviorSubject<any>;

  constructor(private http_: HttpClient) {
    this._locationData = new BehaviorSubject({});
  }

  get<T = any>(location: string) {
    const apiURI = this.getAPIURI(name);
    return this.http_
      .get(`${apiURI}`)
      .pipe(map(this.handleSuccess)) as Observable<T>;
  }

  protected handleSuccess(res: any) {
    return res;
  }

  protected handleError(error: any) {
    return throwError(error);
  }

  async removeLocation(location: Location) {
    this.toBeDeletedLocations.push(location);
    await this.saveAll();
  }

  update(
    apiGroup: string,
    name: string,
    location: IGeoLocation
  ): Observable<any> {
    const httpOptions = {
      method: 'PUT',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    const apiURI = this.getAPIURI(apiGroup);
    return this.http_.put<any>(`${apiURI}/${name}`, location, httpOptions);
  }
  delete(apiGroup: string, name: string): Promise<any> {
    const apiURI = this.getAPIURI(apiGroup);
    return this.http_.delete(`${apiURI}/${name}`).toPromise();
  }
  add(apiGroup: string, location: IGeoLocation): Observable<any> {
    const httpOptions = {
      method: 'POST',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    const apiURI = this.getAPIURI(apiGroup);
    return this.http_.post<any>(`${apiURI}`, location, httpOptions);
  }
  getLocations(scope: string): Observable<any> {
    const apiURI = this.getAPIURI(scope);
    return this.http_
      .get(`${apiURI}`)
      .pipe(map(this.handleSuccess)) as Observable<any>;
  }
  getLocationByName(name: string): Observable<any> {
    const apiURI = this.getAPIURI(name);
    return this.http_
      .get(`${apiURI}/${name}`)
      .pipe(map(this.handleSuccess)) as Observable<any>;
  }
  getAPIURI(scope: string): string {
    return `${environment.apiUrl}` + '/apis/' + scope + '/v1/locations';
  }
  getLatLngByAddress(address: string): Observable<any>{
    const uri = `${this._googleMapAPIURI}geocode/json?address=${encodeURIComponent(address)}&key=${environment.GOOGLE_MAPS_API_KEY}`;
    return this.http_
    .get(uri)
    .pipe(map(this.handleSuccess)) as Observable<any>;
  }
  getTimeZoneByLatLng(lat: string, lng: string): Observable<any>{
    const now = new Date()
    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    const timestamp = Math.floor(today.getTime()/1000);
    const uri = `${this._googleMapAPIURI}timezone/json?location=${lat},${lng}&timestamp=${timestamp}&key=${environment.GOOGLE_MAPS_API_KEY}`;
    return this.http_
      .get(uri)
      .pipe(map(this.handleSuccess)) as Observable<any>;
  }
  async saveAll(): Promise<any> {
    while (this.toBeDeletedLocations.length > 0) {
      const location = this.toBeDeletedLocations[0];
      const apiGroup = location.apiVersion.split("/")[0];
      await this.delete(apiGroup, location.metadata.name);
      this.toBeDeletedLocations.shift();
    }
  }
}
