import { FuseConfig } from '@fuse/types';

/**
 * Default Fuse Configuration
 *
 * You can edit these options to change the default options. All these options also can be
 * changed per component basis. See `app/main/pages/authentication/login/login.component.ts`
 * constructor method to learn more about changing these options per component basis.
 */

export const fuseConfig: FuseConfig = {
    // Color themes can be defined in src/app/app.theme.scss
    colorTheme: "theme-default",
    customScrollbars: true,
    layout: {
        style: "horizontal-layout-2",
        width: "fullwidth",
        navbar: {
            primaryBackground: "#f2f2f2 !important",
            secondaryBackground: "#f2f2f2 !important",
            folded: true,
            hidden: false,
            position: "left",
            variant: "vertical-style-2",
        },
        toolbar: {
            customBackgroundColor: true,
            background: "fuse-white",
            hidden: false,
            position: "above",
        },
        footer: {
            customBackgroundColor: true,
            background: "light-blue-600",
            hidden: true,
            position: "above-fixed",
        },
        sidepanel: {
            hidden: false,
            position: "right",
        },
    },
};
