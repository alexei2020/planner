import { RBACItem } from 'app/components/shared/location-item';
import { Metadata } from './backendapi';
import { StringMap } from './backendapi.gen';

export interface IScope {
  apiGroup: string;
  locationName: string;
  permissions: string;
  privilege: string;
}
export interface ITenant {
  apiGroup: string;
  name: string;
}
export interface IScopeResponse {
  apiVersion?: string;
  kind?: string;
  metadata?: Metadata;
  spec?: {
    home?: 'string';
    scopes?: IScope[];
    userEmail?: string;
    userName?: string;
    tenants?: ITenant[];
  };
}
export interface ILocation {
  apiVersion?: string;
  kind?: string;
  metadata?: Metadata;
  spec: {
    base?: any;
    description: string;
    hr?: boolean;
    address: string;
  };
  status: {
    geo: {
      address: string;
      latitude: string;
      longitude: string;
      timeZoneID: string;
    };
  };
}
export interface ILocations {
  apiVersion: '';
  items: ILocation[];
  kind: '';
  metadata: {
    continue: '';
    resourceVersion: '';
    selfLink: '';
  };
}
export interface IBST {
  tenant: string;
  site?: string | null;
  building?: string | null;
}
export interface IFeature {
  type: string;
  properties: {
    namespace: string;
    description: string;
    address: string;
    timezone: string;
    location: string;
  };
}
export interface IGeoData {
  type: 'FeatureCollection';
  features: IFeature[];
}
export interface ICurrentLocationInfo {
  currentLocation?: string;
  locationName?: string;
  tenantName?: string;
  tenantAPIGroupName?: string;
  timezone?: string;
}
export interface IScopeTenantResponse {
  apiVersion?: string;
  kind?: string;
  metadata?: Metadata;
  spec?: {
    homeLocation: string;
    user: IUser;
    locations: StringMap<ILocationDataItem>;
  };
}

export interface ILocationDataItem {
  APIGroupName: string;
  APIGroupUID: string;
  RBAC: RBACItem[];
}
export interface IUser {
  email: string;
  name: string;
}
