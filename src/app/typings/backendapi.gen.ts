
// tslint:disable

import { Metadata, ObjectRef, WSMeta } from './backendapi';


export interface ILabelSpec{
	description: string,
	values: string[]
}
export interface ILabel {
	apiVersion: string;
	kind?: string;
	spec?: ILabelSpec;
	metadata?: Metadata;
}
export interface StringMap<T> {
  [key: string]: T;
}


export type MacAddress = string;
export type IPAddress = string;
export type HostName = string;
export type UserName = string;
export type ObjectName = string;
export type SerialNo = string;
export type OSVersion = string;
export type DeviceModel = string;
export type DeviceCategory = string;
export type DeviceType = string;
export type PortType = string;
export type AdmitStatus = string;
export type InterfaceAdminStatus = string;
export type DecomissionStatus = string;
export type InterfaceName = string;
export type InterfaceDescription = string;
export type VLANList = string;
export type ResourcePoolName = string;
export type ResourcePoolPurpose = string;
export type VlanPriority = number;
export type Priority = number;
export type DevID = number;
export type RouteID = string;
export type RouteName = string;
export type RouteStatus = string;
export type RouteType = string;
export type RouteMetric = string;
export type RouteMetricType = string;
export type RouteLinkState = string;
export type RouterString = string;
export type RouteDefaultMetric = number;
export type RouteExternalTag = number;
export type RouteSequenceNumber = number;
export type RouteTOS = number;
export type RouteChecksum = number;
export type RouteNumLinks = number;
export type RouteNumTOS = number;
export type NumVlans = number;
export type RouteAge = number;
export type RouteMaskLength = number;
export type RouteOspfAreaID = string;
export type RadioName = string;
export type RadioID = number;
export type RadioIDString = string;
export type RadioTransmitEIRP = number;
export type RadioChannel = number;
export type RadioScanningDwellTime = number;
export type RadioScanningDeferClients = number;
export type RadioScanningInterval = number;
export type IPAddressOrigin = string;
export type UUID = string;
export type ActivationCode = string;
export type OSClass = string;
export type AppClass = string;
export type AppVersion = string;
export type AppName = string;
export type AppStatus = string;
export type AgentPolicyPollTimer = number;
export type AgentMsgType = string;
export type TimeStamp = string;
export type AgentMsgSeqCnt = number;
export type RuleName = string;
export type Command = string;
export type Arguments = string;
export type SlackChannel = string;
export type AlertDuration = string;
export type AlertThreshold = number;
export type AlertStatusMessage = string;
export type AlertKind = string;
export type AlertQueryString = string;
export type AlertType = string;
export type AlertMessage = string;
export type AlertExplanation = string;
export type AlertRecommendedAction = string;
export type AuditResponseStatus = string;
export type AuditVersion = string;
export type AuditUserGroup = string;
export type ReferenceLink = string;
export type MsgID = string;
export type AlertCode = string;
export type VlanID = number;
export type VlanName = string;
export type IPSubnet = string;
export type IPPrefix = string;
export type IPPrefixLength = number;
export type L4Port = number;
export type TimeDiff32 = number;
export type TimeInterval32 = number;
export type QLength = number;
export type SubInterfaceIndex = number;
export type IfIndex = number;
export type TimeZoneID = string;
export type Email = string;
export type URL = string;
export type Description = string;
export type LastChange = string;
export type LagType = string;
export type MinLinks = number;
export type AuthProtocol = string;
export type Longitude = string;
export type Latitude = string;
export type Address = string;
export type APIGroup = string;
export type APIGroupName = string;
export type APIGroupUID = string;
export type MEKName = string;
export type Key = string;
export type SealedKey = string;
export type MaxEndPoints = number;
export type MaxUsers = number;
export type MaxMABEntries = number;
export type MaxSuites = number;
export type MaxRooms = number;
export type MaxSSIDs = number;
export type HomeLocation = string;
export type TenantName = string;
export type TokenID = string;
export type TokenHash = string;
export type TokenOtp = string;
export type OSName = string;
export type FileName = string;
export type Svc = string;
export type Module = string;
export type MD5Hash = string;
export type Args = string;
export type FileSize = number;
export type Dir = string;
export type DateString = string;
export type Crontab = string;
export type Count = number;
export type ProxyID = string;
export type ProxyKey = string;
export type ProxyResp = string;
export type SnmpCommString = string;
export type Vrf = string;
export type DevicePassword = string;
export type TunnelID = string;
export type MTU = string;
export type IntfMTU = number;
export type Version = string;
export type IPProtocol = number;
export type Proto = number;
export type SRCPort = number;
export type DSTPort = number;
export type EWDeny = string;
export type SSOIssuer = string;
export type SSOURL = string;
export type CAData = string;
export type GroupsAttr = string;
export type GroupsDelim = string;
export type NameIDPolicyFormat = string;
export type URI = string;
export type EntityIssuer = string;
export type Password = string;
export type UserID = string;
export type FirstName = string;
export type LastName = string;
export type RBACCacheValue = string;
export type BagInfo = string;
export type LocationName = string;
export type HomeName = string;
export type ServiceID = string;
export type ReportLayout = string;
export type DeviceName = string;
export type PingDestination = string;
export type PingRetries = number;
export type PingTimeTaken = string;
export type PingStatus = string;
export type WspingSource = string;
export type DHCPOptions = string;
export type DHCPClassIdentifier = string;
export type DHCPUserClassId = string;
export type DHCPVendorClass = string;
export type DHCPClientFQDN = string;
export type SSIDName = string;
export type AuthSessionID = string;
export type ServiceName = string;
export type NLPResolvedIntent = string;
export type NLPConfidence = number;
export type NLPField = string;
export type NLPKeyName = string;
export type DummyField = string;
export type OrganizationalUnit = string;
export type Organization = string;
export type PartnerSite = string;
export type Domain = string;
export type Building = string;
export type Locality = string;
export type State = string;
export type Country = string;
export type EmailAddress = string;
export type Unit = string;
export type ChartLabel = string;
export type DashboardLayout = string;
export type FloorPlan = string;
export type PlannerData = string;
export type UICatalogElementName = string;
export type ObjectVersion = string;
export type SamplingRate = number;
export type SamplingInterval = number;
export type EndpointAuxOS = string;
export type EndpointAuxImage = string;
export type EndpointAuxIcon = string;
export type EndpointAuxAccuracy = number;
export type InterfaceDHCP = string;
export type FPDebugString = string;
export type CacheSize = number;
export type ADGroupName = string;
export type ADGroupSID = string;
export type ADDomainName = string;
export type NACGenericString = string;
export type IdPName = string;
export type FavIcon = string;

	export interface AdmitDeviceList {
		items: AdmitDevice[];
	}
	export interface AdmitDevice {
		apiVersion?: string;
		spec?: AdmitDeviceSpec;
		kind?: string;
		status?: AdmitDeviceStatus;
		metadata?: ObjectMeta;
	}

	export interface AuditList {
		items: Audit[];
	}
	export interface Audit {
		apiVersion?: string;
		spec?: AuditSpec;
		kind?: string;
		status?: AuditStatus;
		metadata?: ObjectMeta;
	}

	export interface DeviceUIList {
		items: DeviceUI[];
	}
	export interface DeviceUI {
		apiVersion?: string;
		spec?: DeviceUISpec;
		kind?: string;
		status?: DeviceUIStatus;
		metadata?: ObjectMeta;
	}

	export interface EndPointAuxList {
		items: EndPointAux[];
	}
	export interface EndPointAux {
		apiVersion?: string;
		spec?: EndPointAuxSpec;
		kind?: string;
		status?: EndPointAuxStatus;
		metadata?: ObjectMeta;
	}

	export interface FileList {
		items: File[];
	}
	export interface File {
		apiVersion?: string;
		spec?: FileSpec;
		kind?: string;
		status?: FileStatus;
		metadata?: ObjectMeta;
	}

	export interface FloorSpaceList {
		items: FloorSpace[];
	}
	export interface FloorSpace {
		apiVersion?: string;
		spec?: FloorSpaceSpec;
		kind?: string;
		status?: FloorSpaceStatus;
		metadata?: ObjectMeta;
	}

	export interface FlowConfigList {
		items: FlowConfig[];
	}
	export interface FlowConfig {
		type?: string;
		samplingInterval?: SamplingInterval;
		samplingRate?: SamplingRate;
	}

	export interface IfInterfaceList {
		items: IfInterface[];
	}
	export interface IfInterface {
		apiVersion?: string;
		spec?: IfInterfaceSpec;
		kind?: string;
		status?: IfInterfaceStatus;
		metadata?: ObjectMeta;
	}

	export interface PlatformComponentList {
		items: PlatformComponent[];
	}
	export interface PlatformComponent {
		apiVersion?: string;
		spec?: PlatformComponentSpec;
		kind?: string;
		status?: PlatformComponentStatus;
		metadata?: ObjectMeta;
	}

	export interface RadioList {
		items: Radio[];
	}
	export interface Radio {
		apiVersion?: string;
		spec?: RadioSpec;
		kind?: string;
		status?: RadiosStatus;
		metadata?: ObjectMeta;
	}

	export interface RadioInfoList {
		items: RadioInfo[];
	}
	export interface RadioInfo {
		apiVersion?: string;
		spec?: RadioInfoSpec;
		kind?: string;
		status?: RadiosInfoStatus;
		metadata?: ObjectMeta;
	}

	export interface SSIDList {
		items: SSID[];
	}
	export interface SSID {
		apiVersion?: string;
		spec?: SSIDSpec;
		kind?: string;
		status?: SSIDStatus;
		metadata?: ObjectMeta;
	}

	export interface SnapshotTaskList {
		items: SnapshotTask[];
	}
	export interface SnapshotTask {
		apiVersion?: string;
		spec?: SnapshotTaskSpec;
		kind?: string;
		status?: SnapshotTaskStatus;
		metadata?: ObjectMeta;
	}

	export interface SubinterfaceList {
		items: Subinterface[];
	}
	export interface Subinterface {
		apiVersion?: string;
		spec?: SubinterfaceSpec;
		kind?: string;
		status?: SubinterfaceStatus;
		metadata?: ObjectMeta;
	}

	export interface TenantList {
		items: Tenant[];
	}
	export interface Tenant {
		apiVersion?: string;
		spec?: TenantSpec;
		kind?: string;
		status?: TenantStatus;
		metadata?: ObjectMeta;
	}

	export interface WallJackList {
		items: WallJack[];
	}
	export interface WallJack {
		apiVersion?: string;
		spec?: WallJackSpec;
		kind?: string;
		status?: WallJackStatus;
		metadata?: ObjectMeta;
	}

	export interface DeviceList {
		items: Device[];
	}
	export interface Device {
		apiVersion?: string;
		spec?: DeviceSpec;
		kind?: string;
		status?: DeviceStatus;
		metadata?: ObjectMeta;
	}

	export interface EndPointList {
		items: EndPoint[];
	}
	export interface EndPoint {
		apiVersion?: string;
		spec?: EndPointSpec;
		kind?: string;
		status?: EndPointStatus;
		metadata?: ObjectMeta;
	}

	export interface LocationList {
		items: Location[];
	}
	export interface Location {
		apiVersion?: string;
		spec?: LocationSpec;
		kind?: string;
		status?: LocationStatus;
		metadata?: ObjectMeta;
	}

	export interface NetworkTemplateList {
		items: NetworkTemplate[];
	}
	export interface NetworkTemplate {
		apiVersion?: string;
		spec?: NetworkTemplateSpec;
		kind?: string;
		status?: NetworkTemplateStatus;
		metadata?: ObjectMeta;
	}

	export interface AttributeSetList {
		items: AttributeSet[];
	}
	export interface AttributeSet {
		apiVersion?: string;
		spec?: AttributeSetSpec;
		kind?: string;
		status?: AttributeSetStatus;
		metadata?: ObjectMeta;
	}

	export interface DeviceAttributeSetList {
		items: DeviceAttributeSet[];
	}
	export interface DeviceAttributeSet {
		apiVersion?: string;
		spec?: DeviceAttributeSetSpec;
		kind?: string;
		status?: DeviceAttributeSetStatus;
		metadata?: ObjectMeta;
	}

	export interface EndPointSetList {
		items: EndPointSet[];
	}
	export interface EndPointSet {
		apiVersion?: string;
		spec?: EndPointSetSpec;
		kind?: string;
		status?: EndPointSetStatus;
		metadata?: ObjectMeta;
	}

	export interface SVIConfigList {
		items: SVIConfig[];
	}
	export interface SVIConfig {
		vlanName?: VlanName;
		vlanId?: VlanID;
		ipv4?: IPAddress;
		virtualIpv4?: IPAddress;
		v4PrefixLen?: IPPrefixLength;
		v6PrefixLen?: IPPrefixLength;
		operStatus?: string;
	}

export interface AOV {
	dict?: string;
	attr?: string;
	op?: string;
	val?: string;
	description?: string;
}

export interface AttributeSetSpec {
	apiVersion?: string;
	base?: WSSpec;
	description?: string;
	template?: WSTemplateSpec;
	conditions?: PolicyCondition[];
	kind?: string;
	metadata?: ObjectMeta;
}

export interface DeviceAttributeSetSpec {
	apiVersion?: string;
	base?: WSSpec;
	description?: string;
	template?: WSTemplateSpec;
	conditions?: PolicyCondition[];
	kind?: string;
	metadata?: ObjectMeta;
}

export interface FlowConfig {
	type?: string;
	samplingInterval?: SamplingInterval;
	samplingRate?: SamplingRate;
}

export interface GeoLocation {
	longitude?: Longitude;
	latitude?: Latitude;
	timeZoneID?: TimeZoneID;
	address?: Address;
}

export interface Secret {
	type?: string;
	name?: string;
	key?: string;
	override?: boolean;
	filled?: boolean;
	userName?: UserName;
	password?: string;
	opaquePassword?: string;
	opaqueSecret?: string;
	sshPrivateKey?: string;
	tlsCrt?: string;
	tlsKey?: string;
	data?: Map<string, string>;
}

export interface ZTPStatusInfo {
	ZTPStatus?: string;
	timeStamp?: Date;
}

export interface AttributeSetStatus {
	apiVersion?: string;
	base?: WSStatus;
	template?: WSTemplateStatus;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface AuditSpec {
	apiVersion?: string;
	base?: WSSpec;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface DeviceAttributeSetStatus {
	apiVersion?: string;
	base?: WSStatus;
	template?: WSTemplateStatus;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface ObjectMeta {
	name?: string;
	selfLink?: string;
	uid?: string;
	creationTimestamp?: Date;
	modificationTimestamp?: Date;
	namespace?: string;
	labels?: Map<string, string>;
	tenantuid?: string;
}

export interface PolicyCondition {
	aovList?: AOV[];
}

export interface RadiosInfoStatus {
	Id?: string;
	apiVersion?: string;
	base?: WSStatus;
	OperatingFrequency?: string;
	kind?: string;
	Enabled?: boolean;
	metadata?: ObjectMeta;
	TransmitPower?: string;
	TransmitEirp?: string;
	Channel?: string;
	ChannelWidth?: string;
	Dca?: boolean;
	AllowedChannels?: string;
	AntennaGain?: string;
	BaseRadioMac?: MacAddress;
	AllowedRegulatoryChannels?: string;
	SupportedChannels?: string;
	ChannelChangeReason?: string;
	TotalChannelUtilization?: string;
	RxDot11ChannelUtilization?: string;
	RxNoiseChannelUtilization?: string;
	TxDot11ChannelUtilization?: string;
	Neighbors?: APNeighborInfo[];
}

export interface APNeighborInfo {
	Bssid?: string;
	Ssid?: string;
	Rssi?: string;
	Channel?: string;
	PrimaryChannel?: string;
	LastSeen?: string;
	Opmode?: string;
}

export interface AdmitDeviceSpec {
	ipv4Net?: IPAddress;
	apiVersion?: string;
	base?: WSSpec;
	hostName?: HostName;
	kind?: string;
	admit?: boolean;
	metadata?: ObjectMeta;
	decomission?: boolean;
	macAddress?: MacAddress;
	serialNo?: SerialNo;
	osVersion?: OSVersion;
	vendor?: string;
	model?: DeviceModel;
	role?: string;
	loopbackIp?: IPAddress;
	tenant?: TenantName;
}

export interface AuditStatus {
	action?: string;
	apiVersion?: string;
	base?: WSStatus;
	kind?: string;
	request_uri?: URL;
	description?: Description;
	metadata?: ObjectMeta;
	user?: AuditUserInfo;
	source_ips?: IPAddress[];
	response_status?: AuditResponseStatus;
	affectedObject?: ReferenceLink;
	timeStamp?: Date;
	version_before?: AuditVersion;
	version_after?: AuditVersion;
}

export interface DHCPParams {
	options?: DHCPOptions;
	hostName?: HostName;
	clientIdentifier?: MacAddress;
	classIdentifier?: DHCPClassIdentifier;
	userClassId?: DHCPUserClassId;
	vendorClass?: DHCPVendorClass;
	clientFQDN?: DHCPClientFQDN;
}

export interface DeviceUISpec {
	apiVersion?: string;
	base?: WSSpec;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface EndPointAuxSpec {
	apiVersion?: string;
	base?: WSSpec;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface EndPointSetSpec {
	apiVersion?: string;
	base?: WSSpec;
	conditions?: PolicyCondition[];
	template?: WSTemplateSpec;
	icon?: string;
	kind?: string;
	image?: string;
	metadata?: ObjectMeta;
}

export interface FileSpec {
	apiVersion?: string;
	base?: WSSpec;
	commonAttrs?: CommonAttrs;
	kind?: string;
	tftpAttrs?: TftpAttrs;
	firmwareAttrs?: FirmwareAttrs;
	metadata?: ObjectMeta;
	reportAttrs?: ReportAttrs;
	snapshotAttrs?: SnapshotAttrs;
}

export interface FloorSpaceSpec {
	apiVersion?: string;
	base?: WSSpec;
	type?: string;
	floorPlan?: FloorPlan;
	kind?: string;
	metadata?: ObjectMeta;
	plannerData?: PlannerData;
}

export interface IfInterfaceSpec {
	apiVersion?: string;
	base?: WSSpec;
	type?: string;
	kind?: string;
	mtu?: number;
	loopbackMode?: boolean;
	metadata?: ObjectMeta;
	description?: Description;
	enabled?: boolean;
	ethernet?: EthernetSpec;
	aggregation?: AggregationSpec;
	routedVlan?: VlanID;
}

export interface PlatformComponentSpec {
	apiVersion?: string;
	base?: WSSpec;
	name?: string;
	kind?: string;
	properties?: PropertiesSpec;
	metadata?: ObjectMeta;
	subcomponents?: SubComponentsSpec;
	powerSupply?: PowerSupplySpec;
	linecard?: LinecardSpec;
}

export interface RadioSpec {
	apiVersion?: string;
	base?: WSSpec;
	name?: RadioName;
	template?: WSTemplateSpec;
	id?: RadioIDString;
	kind?: string;
	apBandSettings?: BandSettings;
	metadata?: ObjectMeta;
	radio2G?: Radio2GSpec;
	radio5G?: Radio5GSpec;
}

export interface SSIDSpec {
	apiVersion?: string;
	base?: WSSpec;
	template?: WSTemplateSpec;
	type?: string;
	kind?: string;
	purpose?: string;
	enabled?: boolean;
	metadata?: ObjectMeta;
	hidden?: boolean;
	operatingFrequency?: string;
	basicDataRates2g?: string;
	supportedDataRates2g?: string;
	basicDataRates5g?: string;
	supportedDataRates5g?: string;
	opmode?: string;
	wpa2Psk?: Secret;
	dot11k?: boolean;
	okc?: boolean;
	splash?: Splash;
	bandSteering?: BandSteering;
}

export interface SnapshotTaskSpec {
	apiVersion?: string;
	base?: WSSpec;
	schedule?: SchedulerSpec;
	kind?: string;
	operation?: string;
	snapshotname?: Description;
	metadata?: ObjectMeta;
}

export interface SubinterfaceSpec {
	apiVersion?: string;
	base?: WSSpec;
	index?: SubInterfaceIndex;
	description?: InterfaceDescription;
	kind?: string;
	enabled?: boolean;
	metadata?: ObjectMeta;
	vlan?: VlanID;
	ipv4?: IPv4Spec;
	parentInterface?: ReferenceLink;
	ospfV2?: RoutingOspfV2InterfaceSpec;
}

export interface TenantSpec {
	address?: Address;
	apiVersion?: string;
	base?: WSSpec;
	favIcon?: FavIcon;
	kind?: string;
	metadata?: ObjectMeta;
	website?: URL;
}

export interface WSSpec {
	parent_link?: ReferenceLink;
	real_name?: string;
	wstype?: string;
}

export interface WallJackSpec {
	apiVersion?: string;
	attachedToDevice?: IPAddress;
	base?: WSSpec;
	attachedToInterface?: InterfaceName;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface AdmitDeviceStatus {
	apiVersion?: string;
	attachedToPort?: InterfaceName;
	base?: WSStatus;
	attachedToPortType?: PortType;
	kind?: string;
	attachedToDevice?: IPAddress;
	metadata?: ObjectMeta;
	attachedToDeviceName?: HostName;
	admitStatus?: AdmitStatus;
	ports?: InterfaceName[];
	oldAttachedToPort?: InterfaceName;
	oldAttachedToPortType?: PortType;
	oldAttachedToDevice?: IPAddress;
}

export interface AuditUserInfo {
	user_name?: UserName;
	email?: Email;
	groups?: AuditUserGroup[];
}

export interface BandSettings {
	bandOperationMode?: string;
	bandSteeringEnabled?: boolean;
}

export interface CommonAttrs {
	rootDir?: Dir;
	type?: string;
	isDir?: boolean;
	size?: FileSize;
	noEncrypt?: boolean;
}

export interface DeviceSpec {
	ipv4Net?: IPAddress;
	apiVersion?: string;
	base?: WSSpec;
	hostName?: HostName;
	kind?: string;
	admit?: boolean;
	metadata?: ObjectMeta;
	decomission?: boolean;
	apMode?: string;
}

export interface DeviceUIStatus {
	apiVersion?: string;
	base?: WSStatus;
	role?: string;
	kind?: string;
	vendor?: string;
	metadata?: ObjectMeta;
	model?: DeviceModel;
	hostName?: HostName;
	ipv4Net?: IPAddress;
	type?: DeviceType;
	pltfmCatalog?: ReferenceLink;
	frontIntfs?: Map<InterfaceName, DeviceUIIntf>;
	rearIntfs?: Map<InterfaceName, DeviceUIIntf>;
	rearFans?: Map<InterfaceName, DeviceUIComp>;
	rearPsus?: Map<InterfaceName, DeviceUIComp>;
	linecards?: Map<InterfaceName, DeviceUIComp>;
}

export interface EndPointAuxStatus {
	apiVersion?: string;
	base?: WSStatus;
	deviceIPAddress?: IPAddress;
	kind?: string;
	userName?: UserName;
	localUserGroups?: string[];
	metadata?: ObjectMeta;
	IDPUserGroups?: string[];
	endPointSets?: ReferenceLink[];
	nmapResult?: NMAPResult;
	wallJack?: ReferenceLink;
	floorSpace?: ReferenceLink;
	icon?: EndpointAuxIcon;
	image?: EndpointAuxImage;
}

export interface EndPointSetStatus {
	apiVersion?: string;
	base?: WSStatus;
	template?: WSTemplateStatus;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface FloorSpaceStatus {
	apiVersion?: string;
	base?: WSStatus;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface IfInterfaceStatus {
	apiVersion?: string;
	base?: WSStatus;
	name?: InterfaceName;
	kind?: string;
	type?: string;
	metadata?: ObjectMeta;
	mtu?: number;
	loopbackMode?: boolean;
	description?: Description;
	enabled?: boolean;
	ifindex?: number;
	adminStatus?: string;
	operStatus?: string;
	lastChange?: LastChange;
	logical?: boolean;
	pcMember?: boolean;
	ethernet?: EthernetStatus;
	aggregation?: AggregationStatus;
	routedVlan?: VlanID;
	role?: string;
	deviceRole?: string;
	remoteDevice?: ReferenceLink;
	remoteIntf?: InterfaceName;
	poeEnabled?: boolean;
}

export interface LocationSpec {
	apiVersion?: string;
	base?: WSSpec;
	description?: Description;
	hq?: boolean;
	kind?: string;
	address?: Address;
	metadata?: ObjectMeta;
}

export interface PlatformComponentStatus {
	apiVersion?: string;
	base?: WSStatus;
	name?: string;
	kind?: string;
	type?: string;
	id?: string;
	metadata?: ObjectMeta;
	location?: string;
	description?: string;
	mfgName?: string;
	mfgDate?: string;
	hardwareVersion?: string;
	firmwareVersion?: string;
	softwareVersion?: string;
	serialNo?: string;
	partNo?: string;
	removable?: boolean;
	operStatus?: string;
	empty?: boolean;
	parent?: string;
	temperature?: TemperatureStatus;
	memory?: MemoryStatus;
	allocatedPower?: number;
	usedPower?: number;
	properties?: PropertiesStatus;
	subcomponents?: SubComponentsStatus;
	entityId?: number;
	powerSupply?: PowerSupplyStatus;
	fan?: FanStatus;
	cpu?: CpuStatus;
	linecard?: LinecardStatus;
}

export interface SnapshotTaskStatus {
	apiVersion?: string;
	base?: WSStatus;
	result?: SchedulerStatus;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface Splash {
	type?: string;
	captivePortalIP?: IPAddress;
	captivePortalURL?: URL;
}

export interface SubinterfaceStatus {
	apiVersion?: string;
	base?: WSStatus;
	index?: SubInterfaceIndex;
	description?: InterfaceDescription;
	kind?: string;
	enabled?: boolean;
	metadata?: ObjectMeta;
	name?: InterfaceName;
	ifindex?: IfIndex;
	adminStatus?: InterfaceAdminStatus;
	operStatus?: string;
	lastChange?: TimeStamp;
	logical?: boolean;
	vlan?: VlanID;
	ipv4?: IPv4Status;
	ospfV2?: RoutingOspfV2InterfaceStatus;
}

export interface TenantStatus {
	apiVersion?: string;
	base?: WSStatus;
	type?: string;
	APIGroupUID?: APIGroupUID;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface WallJackStatus {
	apiVersion?: string;
	base?: WSStatus;
	tenant?: TenantName;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface AggregationSpec {
	lagType?: string;
	minLinks?: MinLinks;
}

export interface BandSteering {
	bandSteering?: boolean;
	steeringRssi?: string;
}

export interface DeviceStatus {
	macAddress?: MacAddress;
	apiVersion?: string;
	base?: WSStatus;
	kind?: string;
	serialNo?: SerialNo;
	osVersion?: OSVersion;
	metadata?: ObjectMeta;
	vendor?: string;
	model?: DeviceModel;
	role?: string;
	loopbackIp?: IPAddress;
	inbandMgmtIp?: IPAddress;
	timeZoneID?: TimeZoneID;
	id?: DevID;
	sviConfigs?: SVIConfig[];
	ports?: InterfaceName[];
	aplinks?: InterfaceName[];
	uplinkConfigs?: UplinkConfig[];
	peerlinkConfigs?: PeerlinkConfig[];
	downlinkConfigs?: DownlinkConfig[];
	apLinkConfigs?: ApLinkConfig[];
	upgradeProgress?: boolean;
	connStatus?: string;
	tenant?: TenantName;
	apVirtualControllerIP?: IPAddress;
	powerSource?: string;
	processing?: string;
	downlinksDone?: boolean;
	deviceLink?: ReferenceLink;
	operStatus?: string;
	ZTPStatusInfo?: ZTPStatusInfo;
}

export interface DeviceUIIntf {
	name?: InterfaceName;
	type?: PortType;
	operStatus?: string;
	remotePort?: InterfaceName;
	remoteDevice?: DeviceName;
	uplink?: boolean;
}

export interface EndPointSpec {
	apiVersion?: string;
	base?: WSSpec;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface IPv4Spec {
	enabled?: boolean;
	mtu?: IntfMTU;
	dhcpClient?: boolean;
	addresses?: AddressSpec[];
	proxyArp?: string;
	neighbors?: NeighborSpec[];
	unnumbered?: UnnumberedSpec;
}

export interface LocationStatus {
	apiVersion?: string;
	base?: WSStatus;
	type?: string;
	geo?: GeoLocation;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface NMAPResult {
	OS?: OSName;
	accuracy?: EndpointAuxAccuracy;
}

export interface NetworkTemplateSpec {
	apiVersion?: string;
	base?: WSSpec;
	networkAccessControl?: boolean;
	template?: WSTemplateSpec;
	kind?: string;
	networkTrafficAnalytics?: boolean;
	metadata?: ObjectMeta;
	networking?: string;
	deviceSNMPV2ReadCommunityName?: SnmpCommString;
	autoAdmitDevices?: boolean;
	configMode?: string;
	domainName?: string;
	deviceUserName?: string;
	devicePassword?: Secret;
	controllerList?: ReferenceLink[];
	arubaAPMode?: string;
	devicePortalVendor?: string;
	devicePortalOrg?: Organization;
	devicePortalKey?: Secret;
	devicePortalVendor2?: string;
	devicePortalOrg2?: Organization;
	devicePortalKey2?: Secret;
	dhcpConfigIP?: IPAddress[];
	flowConfigs?: FlowConfig[];
	dnsServers?: IPAddress[];
}

export interface PropertiesSpec {
	property?: PropertySpec[];
}

export interface Radio2GSpec {
	id?: RadioID;
	minBitRate?: string;
	operatingFrequency?: string;
	enabled?: boolean;
	maxTxPower?: string;
	minTxPower?: string;
	transmitEirp?: RadioTransmitEIRP;
	channel?: string;
	channelWidth?: string;
	dca?: boolean;
	allowedChannels?: RadioChannel[];
	antennaGain?: string;
	scanning?: boolean;
	scanningInterval?: RadioScanningInterval;
	scanningDwellTime?: RadioScanningDwellTime;
	scanningDeferClients?: RadioScanningDeferClients;
	scanningDeferTraffic?: boolean;
}

export interface TftpAttrs {
	vendor?: string;
	version?: Version;
}

export interface WSStatus {
	faults?: ObjectRef[];
	nomodifynb?: boolean;
	nodeletenb?: boolean;
	audit?: WSAuditInfo;
}

export interface AggregationStatus {
	lagType?: string;
	minLinks?: MinLinks;
	lagSpeed?: string;
}

export interface DeviceUIComp {
	name?: DeviceName;
	operStatus?: string;
}

export interface EndPointStatus {
	macAddress?: MacAddress;
	apiVersion?: string;
	base?: WSStatus;
	kind?: string;
	state?: string;
	metadata?: ObjectMeta;
	userName?: UserName;
	connectedDevice?: IPAddress;
	connectedDeviceName?: HostName;
	connectedPort?: InterfaceName;
	connectedPortName?: InterfaceName;
	connectedPortType?: PortType;
	vlanName?: VlanName;
	vendor?: string;
	model?: DeviceModel;
	os?: OSVersion;
	category?: DeviceCategory;
	deviceName?: HostName;
	deviceIPAddress?: IPAddress;
	DHCPParams?: DHCPParams;
	description?: Description;
	ssid?: SSIDName;
	ssidType?: string;
	authProtocol?: AuthProtocol;
	authServer?: IPAddress;
	lastAuthTimeStamp?: Date;
	authStartTimeStamp?: Date;
	authTimeDiff?: TimeDiff32;
	lastDhcpTimeStamp?: Date;
	dhcpStartTimeStamp?: Date;
	dhcpTimeDiff?: TimeDiff32;
	dhcpServer?: IPAddress;
	authFailureReason?: string;
	authStatus?: string;
	timeStamp?: TimeStamp;
	connectivityType?: string;
	deviceAuthType?: string;
	deviceCategory?: DeviceCategory;
	networkType?: string;
	registeredDevice?: DeviceName;
	tenantName?: TenantName;
	inbandDevice?: boolean;
	guestAuthStatus?: string;
	guestAuthTimeStamp?: TimeStamp;
	wallJack?: ReferenceLink;
	floorSpace?: ReferenceLink;
	corporateDevice?: boolean;
	BYODRegistered?: boolean;
	auditSessionID?: AuthSessionID;
}

export interface IPv4Status {
	name?: string;
	enabled?: boolean;
	mtu?: IntfMTU;
	dhcpClient?: boolean;
	addresses?: AddressStatus[];
	proxyArp?: string;
	neighbors?: NeighborStatus[];
	unnumbered?: UnnumberedStatus;
}

export interface NetworkTemplateStatus {
	apiVersion?: string;
	base?: WSStatus;
	status?: string;
	template?: WSTemplateStatus;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface PropertySpec {
	name?: string;
	value?: string;
}

export interface Radio5GSpec {
	id?: RadioID;
	minBitRate?: string;
	operatingFrequency?: string;
	enabled?: boolean;
	maxTxPower?: string;
	minTxPower?: string;
	transmitEirp?: RadioTransmitEIRP;
	channel?: string;
	channelWidth?: string;
	dca?: boolean;
	allowedChannels?: RadioChannel[];
	antennaGain?: string;
	scanning?: boolean;
	scanningInterval?: RadioScanningInterval;
	scanningDwellTime?: RadioScanningDwellTime;
	scanningDeferClients?: RadioScanningDeferClients;
	scanningDeferTraffic?: boolean;
}

export interface RadioInfoSpec {
	apiVersion?: string;
	base?: WSSpec;
	item?: string;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface SSIDStatus {
	apiVersion?: string;
	base?: WSStatus;
	template?: WSTemplateStatus;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface SVIConfig {
	vlanName?: VlanName;
	vlanId?: VlanID;
	ipv4?: IPAddress;
	virtualIpv4?: IPAddress;
	v4PrefixLen?: IPPrefixLength;
	v6PrefixLen?: IPPrefixLength;
	operStatus?: string;
}

export interface SnapshotAttrs {
	args?: Args;
}

export interface AddressSpec {
	ip?: IPAddress;
	prefixLength?: IPPrefixLength;
	origin?: IPAddressOrigin;
}

export interface EthernetSpec {
	apiVersion?: string;
	base?: WSSpec;
	macAddress?: MacAddress;
	autoNegotiate?: boolean;
	kind?: string;
	duplexMode?: string;
	metadata?: ObjectMeta;
	portSpeed?: string;
	enableFlowControl?: boolean;
	switchedVlan?: SwitchedVlanEthernetSpec;
}

export interface PropertiesStatus {
	property?: PropertyStatus[];
}

export interface RadiosStatus {
	apiVersion?: string;
	base?: WSStatus;
	template?: WSTemplateStatus;
	kind?: string;
	metadata?: ObjectMeta;
}

export interface ReportAttrs {
	args?: Args;
}

export interface UplinkConfig {
	localPort?: InterfaceName;
	remotePort?: InterfaceName;
	remoteNodeName?: HostName;
	localNodeName?: HostName;
	remoteNodeId?: DevID;
	localNodeId?: DevID;
	vlanList?: VLANList;
	unconfigured?: boolean;
}

export interface WSTemplateSpec {
	enable?: string;
	Exlist?: ReferenceLink[];
	Inclist?: ReferenceLink[];
}

export interface AddressStatus {
	ip?: IPAddress;
	prefixLength?: IPPrefixLength;
}

export interface EthernetStatus {
	apiVersion?: string;
	base?: WSStatus;
	macAddress?: MacAddress;
	autoNegotiate?: boolean;
	kind?: string;
	duplexMode?: string;
	metadata?: ObjectMeta;
	portSpeed?: string;
	enableFlowControl?: boolean;
	hwMacAddress?: MacAddress;
	negotiatedDuplexMode?: string;
	negotiatedPortSpeed?: string;
	switchedVlan?: SwitchedVlanEthernetStatus;
	stp?: StpStatus;
}

export interface FirmwareAttrs {
	vendor?: string;
	version?: Version;
}

export interface PeerlinkConfig {
	localPort?: InterfaceName;
	remotePort?: InterfaceName;
	remoteNodeName?: HostName;
	localNodeName?: HostName;
	remoteNodeId?: DevID;
	localNodeId?: DevID;
	vlanList?: VLANList;
	unconfigured?: boolean;
}

export interface PropertyStatus {
	name?: string;
	value?: string;
	configurable?: boolean;
}

export interface DownlinkConfig {
	localPort?: InterfaceName;
	remotePort?: InterfaceName;
	remoteNodeName?: HostName;
	remoteNodeId?: DevID;
	localNodeId?: DevID;
	accessSwitchVlan?: VlanID;
	localNodeName?: HostName;
	unconfigured?: boolean;
}

export interface FileStatus {
	apiVersion?: string;
	base?: WSStatus;
	status?: Description;
	createdAt?: Date;
	kind?: string;
	md5Hash?: MD5Hash;
	metadata?: ObjectMeta;
}

export interface NeighborSpec {
	ip?: IPAddress;
	linkLayerAddress?: MacAddress;
}

export interface SubComponentsSpec {
	subcomponent?: SubComponentSpec[];
}

export interface SwitchedVlanEthernetSpec {
	apiVersion?: string;
	base?: WSSpec;
	interfaceMode?: string;
	kind?: string;
	nativeVlan?: VlanID;
	accessVlan?: VlanID;
	metadata?: ObjectMeta;
	trunkVlans?: string;
}

export interface WSTemplateStatus {
	template_link?: ReferenceLink;
	appliedto_locations?: ReferenceLink[];
}

export interface ApLinkConfig {
	apLink?: InterfaceName;
	apName?: HostName;
	unconfigured?: boolean;
}

export interface NeighborStatus {
	ip?: IPAddress;
	linkLayerAddress?: MacAddress;
	origin?: IPAddressOrigin;
}

export interface SubComponentSpec {
	name?: string;
}

export interface SwitchedVlanEthernetStatus {
	apiVersion?: string;
	base?: WSSpec;
	interfaceMode?: string;
	kind?: string;
	nativeVlan?: VlanID;
	accessVlan?: VlanID;
	metadata?: ObjectMeta;
	trunkVlans?: string;
}

export interface SchedulerSpec {
	now?: boolean;
	date?: DateString;
	crontab?: Crontab;
	maxFilesBeforeRotating?: Count;
}

export interface StpStatus {
	protocol?: string;
	protoState?: string;
}

export interface SubComponentsStatus {
	subcomponent?: SubComponentStatus[];
}

export interface UnnumberedSpec {
	enabled?: boolean;
}

export interface SchedulerStatus {
	ID?: ObjectName;
	lastTimeExecuted?: Date;
	operstatus?: string;
	message?: Description;
	numTimesExecuted?: Count;
	result?: ReferenceLink;
}

export interface SubComponentStatus {
	name?: string;
}

export interface UnnumberedStatus {
	enabled?: boolean;
}

export interface RoutingOspfV2InterfaceSpec {
	protocol?: ReferenceLink;
	areaId?: RouteOspfAreaID;
	networkType?: string;
	priority?: Priority;
	authType?: string;
	metric?: RouteMetric;
	passive?: boolean;
	hideNetwork?: boolean;
	timers?: RoutingOspfV2InterfaceTimersSpec;
}

export interface TemperatureStatus {
	instant?: number;
	avg?: number;
	min?: number;
	max?: number;
	interval?: number;
	minTime?: number;
	maxTime?: number;
	alarmStatus?: boolean;
	alarmThreshold?: number;
	alarmSeverity?: string;
}

export interface MemoryStatus {
	available?: number;
	utilized?: number;
}

export interface RoutingOspfV2InterfaceStatus {
	interface?: ReferenceLink;
	areaId?: RouteOspfAreaID;
	networkType?: string;
	priority?: Priority;
	authenticationType?: string;
	metric?: RouteMetric;
	passive?: boolean;
	hideNetwork?: boolean;
	timers?: RoutingOspfV2InterfaceTimersStatus;
	neighbors?: RoutingOspfV2InterfaceNeighborStatus[];
}

export interface PowerSupplySpec {
	enabled?: boolean;
}

export interface RoutingOspfV2InterfaceTimersSpec {
	deadInterval?: TimeInterval32;
	helloInterval?: TimeInterval32;
	retransmissionInterval?: TimeInterval32;
}

export interface WSAuditInfo {
	service?: ServiceName;
	userEmail?: Email;
	versionBefore?: ObjectVersion;
}

export interface PowerSupplyStatus {
	enabled?: boolean;
	capacity?: string;
	inputCurrent?: string;
	inputVoltage?: string;
	outputCurrent?: string;
	outputVoltage?: string;
	outputPower?: string;
}

export interface RoutingOspfV2InterfaceTimersStatus {
	deadInterval?: TimeInterval32;
	helloInterval?: TimeInterval32;
	retransmissionInterval?: TimeInterval32;
}

export interface FanStatus {
	speed?: string;
}

export interface RoutingOspfV2InterfaceNeighborStatus {
	routerId?: RouteID;
	metric?: RouteMetric;
	priority?: Priority;
	deadTime?: TimeStamp;
	designatedRouter?: RouterString;
	optionalCapabilities?: string;
	lastEstablishedTime?: TimeStamp;
	adjacencyState?: string;
	stateChanges?: TimeDiff32;
	retransmissionQueueLength?: QLength;
}

export interface CpuStatus {
	instant?: string;
	avg?: string;
	min?: string;
	max?: string;
	interval?: string;
	minTime?: string;
	maxTime?: string;
}

export interface LinecardSpec {
	powerAdminState?: string;
}

export interface LinecardStatus {
	powerAdminState?: string;
	slotId?: string;
}

// end of file
