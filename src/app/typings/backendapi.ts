export type PropertyMap<TSource, TResult> = { [TProperty in keyof TSource]: TResult };

export class Metadata {
    name?: string;
    uid?: string;
    creationTimestamp?: string;
    labels?: string;
    selfLink?: string;
    resourceVersion?: string;
}

export class ObjectRef {
    apiVersion?: string;
    uid?: string;
    name?: string;
    kind?: string;
    namespace?: string;
}

export class WSMeta {
    parent_uid?: string;
    faults: ObjectRef[];
}
