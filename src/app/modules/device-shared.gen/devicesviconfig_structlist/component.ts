
// tslint:disable

import { Component, ViewChild, Input, SimpleChanges, OnChanges, EventEmitter, OnInit, OnDestroy, Output, ElementRef, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { process, GroupDescriptor } from '@progress/kendo-data-query';
import { ActivatedRoute } from '@angular/router';
import { DialogService } from 'app/main/shared/dialog/dialog.service';
import { first, takeUntil, map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { DeviceDetailService } from '../../../resource/device.gen/services';
import { Device } from '../../../typings/backendapi.gen';
import { DropdownDict, DictionaryService } from '../../../services/dictionary.service';
import { v4 as uuid } from 'uuid';

import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { BreadcrumbService, EventType, FormValidationInfo } from 'app/main/shared/breadcrumb-bar.service';
import * as _ from 'lodash';

const createFormGroup = dataItem => new FormGroup({


    vlanName: new FormControl(dataItem.vlanName),
    vlanId: new FormControl(dataItem.vlanId),
});

@Component({
    selector: 'devicesviconfig-struct-list',
    templateUrl: './component.html',
    styleUrls: ['./component.scss'],
})

export class DevicesviconfigStructListComponent implements OnInit, OnChanges, OnDestroy {
    @Input() public resourceName: string;
    @Input() public namespace: string;
    @Input() public device: Device;
    @ViewChild('grid') private grid;
    @ViewChild('globalSearch', { static: false }) globalSearch: ElementRef;
    @Input() showMetrics = false;
    @Input() pageSize = 10;
    @Input() searchText;
    @Input() resizeble: boolean;

    @Output() setRows: any = new EventEmitter();

    public groups: GroupDescriptor[] = [];
    public formGroup: FormGroup;
    public gridView: GridDataResult;
    public mySelection: string[] = [];
    public clickedRowItem: any;
    public clickedRowIndex: number;
    public filterColumns = [];
    public skip = 0 as number;
    public shouldHideActionMenu = false as boolean;
    public groupable = false;
    public dataSource: any[] = [];
    public isLoading = false;
    public isEditing: boolean;
    public isAdding: boolean;
    _originalLabelData: any[] = [];
    readonly separatorKeysCodes: number[] = [ENTER, COMMA];
    public sort = [];
    private _location: string;
    private editedRowIndex: number;
    private sviConfigs: any;
    private initialised = false;
    private _unsubscribeAll = new Subject();
    previousSize: number;

    constructor(
        private readonly service: DeviceDetailService,
        private _dialogService: DialogService,
        private readonly activatedRoute: ActivatedRoute,
	private _breadcrumbService: BreadcrumbService,
        private _dict: DictionaryService,
    ) {

        this.gridView = {
            data: [],
            total: 0
        };
        this.filterColumns = [
            {
                name: 'Vlan Name', 
                field: 'vlanName'
            },
            {
                name: 'Vlan Id', 
                field: 'vlanId'
            },
        ];
    }

    ngOnInit(): void {
        this.initObjField(this.device, 'status.sviConfigs', []);
        this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(queryParams => {
            this.resourceName = this.activatedRoute.snapshot.params.resourceName;
            this._location = queryParams['loc'] || 'ws.io';
            this.load();
        });
	this._breadcrumbService.triggeredEvent.pipe(takeUntil(this._unsubscribeAll)).subscribe((event) => {
	    if (event.type === EventType.DEVICE_DETAIL_SAVE) {
  		if (this.isEditing || this.isAdding) {
		    this.saveHandler({
			sender: this.grid,
			rowIndex: this.editedRowIndex,
			formGroup: this.formGroup,
    		    });
  		}
	    } else if (event.type === EventType.DEVICE_DETAIL_CANCEL) {
		this.cancelHandler({
		    sender: this.grid,
		    rowIndex: this.clickedRowIndex,
		    dataItem: this.clickedRowItem,
		});
	    }
    	});
        this.formGroup = createFormGroup({
            vlanName: '',
            vlanId: 0,
        });
	this.initialised = true;
    }

    dataStateChange(event: DataStateChangeEvent) {
        this.skip = event.skip;
        this.pageSize = event.take;
        this.onFilter(this.searchText);
        this.setHeight();
    }

    onDropdownDictChange() {
    }

    setHeight() {
        setTimeout(() => {
            if (this.grid && this.grid.wrapper) {
                let size = 0;
				if (this.grid.wrapper) {
					const header = this.grid.wrapper.nativeElement.querySelector('.k-grouping-header'); 
					const grid = this.grid.wrapper.nativeElement.querySelector('.k-grid-aria-root');
					const pager = this.grid.wrapper.nativeElement.querySelector('.k-pager-wrap');
					if (header) {
						size += header.offsetHeight;
					}
					if (grid) {
						size += grid.offsetHeight;
					}
					if (pager) {
						size += pager.offsetHeight;
					}
				}

                if (size == 0) {
                    size = this.grid.wrapper.nativeElement.offsetHeight;
                }

                this.previousSize = size;

                this.setRows.emit(size);
            }
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        let update = false;
        let searchText = this.searchText;
        if ((changes.pageSize && changes.pageSize.currentValue) || (changes.resizeble && changes.resizeble.currentValue)) {
            update = true;
        }
        if (changes.searchText && this.dataSource) {
            searchText = changes.searchText.currentValue;
            update = true;
        }
        if (update) {
            this.setHeight();
            this.onFilter(searchText);
        }
	if (this.initialised) {
	    this.load();
	}
    }

    public intNormalizer =  (text$: Observable<string>): any => text$.pipe(
      map((userInput: string) => {
        return Number(userInput);
      })
    )

    save(): void {
	this.device.status.sviConfigs = [];
	this.device.status.sviConfigs = this.device.status.sviConfigs.concat(this.sviConfigs);
    }

    load(): void {
        this.dataSource = [];
	this.sviConfigs = [];

	
	if (this.device.status.sviConfigs) {
		this.device.status.sviConfigs?.map(item => {
		    var entry = new Object();
    		    entry = _.set(entry, 'vlanName', item.vlanName);
    		    entry = _.set(entry, 'vlanId', item.vlanId);
		    this.sviConfigs = this.sviConfigs.concat(entry);

		    this.dataSource.push({
			_id: uuid(),
			vlanName: item.vlanName,
			vlanId: item.vlanId,
		    })
		});
	}
        this.onFilter(this.searchText || null)
        this.setHeight();
	this.isLoading = false;
        this.gridView = {
            data: this.dataSource,
            total: this.dataSource.length,
        };
    }

    ngOnDestroy() {
	this.device = null;
        // Unsubscribe from all subscriptions
        this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
    }

    public onFilter(inputValue: string): void {
        if (!this.dataSource) {
            return;
        }

        const result: GridDataResult = process(this.dataSource, {
            filter: inputValue ? {
                logic: 'or',
                filters: this.filterColumns.map((col) => ({
                    field: col.field,
                    operator: 'contains',
                    value: inputValue
                })),
            } : null,
            group: this.groups,
        });

        this.gridView = {
            data: result.data,
            total: result.total
        };
    }

    public editSelectedItem(): void {
	if (!this.mySelection || this.mySelection.length == 0) {
	    this._dialogService.openNoticeDialog({
		title: 'Please select any row',
		msg: 'Action can be applied to selected row only.',
	    });
	    return;
	}

	if (this.mySelection && this.mySelection.length > 1) {
	    this._dialogService.openNoticeDialog({
		title: 'Please select only one row',
		msg: 'Action cannot be applied to multiple rows.',
	    });
	    return;
	}

	this.editHandler({
	    sender: this.grid,
	    rowIndex: this.clickedRowIndex,
	    dataItem: this.clickedRowItem,
	});
    }

    public editHandler({ sender, rowIndex, dataItem }) {
  	if (this.isEditing || this.isAdding) {
	    if (this.isAdding) rowIndex++;
	    this.saveHandler({
		sender: this.grid,
		rowIndex: this.editedRowIndex,
		formGroup: this.formGroup,
    	    });
  	}
        this.shouldHideActionMenu = true;
        this.closeEditor(sender);
        this.formGroup.setValue({
        
	vlanName: dataItem.vlanName,
	
        
	vlanId: dataItem.vlanId,
	
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
	this.isEditing = true;
	this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true}));
	this.onDropdownDictChange();
    }

    public editUsingApiHandler({ sender, rowIndex, dataItem }) {
        const selectedItem = this.clickedRowItem;
    }

    public cancelHandler({ sender, rowIndex, dataItem }) {
        this.shouldHideActionMenu = false;
        this.closeEditor(sender, rowIndex);
        this.setHeight();
	this.isEditing = false;
	this.isAdding = false;
	this.clickedRowIndex = undefined;
	this.clickedRowItem = undefined;
	this.mySelection = [];
	this.load();
    }

    public onAddHandler() {
	
  	if (this.isEditing || this.isAdding) {
	    this.saveHandler({
		sender: this.grid,
		rowIndex: this.editedRowIndex,
		formGroup: this.formGroup,
    	    });
  	}
        this.shouldHideActionMenu = true;
        this.closeEditor(this.grid);

	this.isAdding = true;
	this.editedRowIndex = 0;
	this.clickedRowIndex = 0;
        this.grid.addRow(this.formGroup);
        this.setHeight();
	this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true}));
    }
    insertAt(rowIndex) {
	if (!this.mySelection || this.mySelection.length == 0) {
	    this._dialogService.openNoticeDialog({
		title: 'Please select any row',
		msg: 'Action can be applied to selected row only.',
	    });
	    return;
	}

	if (this.mySelection && this.mySelection.length > 1) {
	    this._dialogService.openNoticeDialog({
		title: 'Please select only one row',
		msg: 'Action cannot be applied to multiple rows.',
	    });
	    return;
	}
        this.shouldHideActionMenu = true;
        this.closeEditor(this.grid);

	this.isAdding = true;

        const dataItem = {
            _id: uuid(),
            vlanName: '',
            vlanId: 0,
        };
        this.dataSource.splice(rowIndex, 0, dataItem);
        this.gridView = {
            data: this.dataSource,
            total: this.dataSource.length,
        };
        this.editedRowIndex = rowIndex;
        this.clickedRowIndex = rowIndex;
        this.clickedRowItem = dataItem;
        setTimeout(() => {
            this.grid.editRow(rowIndex, this.formGroup);
            this.setHeight();
        });
	this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true}));
    }

    onInsertBeforeHandler() {
  	if (this.isEditing || this.isAdding) {
            // If we are adding and trying to insert in another location; account for the
            // extra entry
            if (this.isAdding && this.clickedRowIndex > this.editedRowIndex) {
                this.clickedRowIndex++;
            }
	    this.saveHandler({
		sender: this.grid,
		rowIndex: this.editedRowIndex,
		formGroup: this.formGroup,
    	    });
            this.mySelection = [`{this.clickedRowIndex}`];
  	}
        this.insertAt(this.clickedRowIndex);
    }

    onInsertAfterHandler() {
  	if (this.isEditing || this.isAdding) {
            // If we are adding and trying to insert in another location; account for the
            // extra entry
            if (this.isAdding && this.clickedRowIndex > this.editedRowIndex) {
                this.clickedRowIndex++;
            }
	    this.saveHandler({
		sender: this.grid,
		rowIndex: this.editedRowIndex,
		formGroup: this.formGroup,
    	    });
            this.mySelection = [`${this.clickedRowIndex + 1}`];
  	}
        this.insertAt(this.clickedRowIndex+1);
    }

    public saveHandler({ sender, rowIndex, formGroup }): void {
	var entry = new Object();
        entry = _.set(entry, 'vlanName', formGroup.controls.vlanName.value);
        entry = _.set(entry, 'vlanId', +formGroup.controls.vlanId.value);
        this.shouldHideActionMenu = false;
	if (this.isAdding) {
           this.sviConfigs.splice(rowIndex, 0, entry);
        } else {
	   this.sviConfigs[rowIndex] = entry;
        }
        this.save();
	this.load();
	this.isAdding = false;
	this.isEditing = false;
	this.closeEditor(this.grid, rowIndex);
	this.mySelection = [];
    }

    public removeSelectedItem(): void {
	if (!this.mySelection || this.mySelection.length == 0) {
  	    this._dialogService.openNoticeDialog({
		title: 'Please select any row',
		msg: 'Action can be applied to selected row only.',
  	    });
	    return;
	}

	if (this.mySelection && this.mySelection.length > 1) {
	    this._dialogService.openNoticeDialog({
		title: 'Please select only one row',
		msg: 'Action cannot be applied to multiple rows.',
	    });
	    return;
	}

	this.removeHandler({ rowIndex: this.clickedRowIndex, dataItem: this.clickedRowItem });
    }

    public removeHandler({ rowIndex, dataItem }): void {

        this._dialogService
            .openConfirmDialog({ title: 'Remove devicesviconfig record?'})
            .afterClosed()
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((res) => {
                if (res) {
		    this.sviConfigs.splice(rowIndex, 1);
                    this.save();
		    this.load();
		    this.clickedRowIndex = undefined;
		    this.clickedRowItem = undefined;
		    this.mySelection = [];
		    this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true}));
                }
            });
        this.shouldHideActionMenu = false;
    }

    public onCellClick({ rowIndex, dataItem }) {
        this.clickedRowItem = dataItem;
	this.clickedRowIndex = rowIndex;
    }

    public onDblClick() {
    }

    public onExportPDFHandler() {
        this.grid.saveAsPDF();
    }

    public onExportExcelHandler() {
        this.grid.saveAsExcel();
    }

    public onGroupable() {
        this.groupable = !this.groupable;
        this.groups = [];
        this.load();
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        this.grid.closeRow();
	this.dataSource?.forEach((item, row) => this.grid.closeRow(row));
        this.editedRowIndex = undefined;
	this.formGroup.reset();
    }

    @HostListener('window:resize', ['$event'])
    onResize() {
        this.setHeight();
    }

    initObjField(obj, path, value): any {
	if (!_.has(obj, path)) {
	    return _.set(obj, path, value);
	}
	return obj;
    }

    renderList(obj: any): any[] {
	if (Array.isArray(obj)) {
	    return obj;
	} else {
	    return [obj];
	}
    }

    renderObject(obj: any): string {
	if (typeof(obj) === 'object') {
	    var str = JSON.stringify(obj, null, '\n');
            return str.replace(/"/g, "");
	}
	return obj;
    }


}

