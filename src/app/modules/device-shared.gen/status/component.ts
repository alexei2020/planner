

// tslint:disable


import { Component, OnDestroy, OnInit, Input, Output, ViewChild, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ReferenceLink } from '../../../typings/backendapi.gen';
import { first, takeUntil, map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { BreadcrumbService, EventType, Event, FormValidationInfo } from 'app/main/shared/breadcrumb-bar.service';
import { metaToUrl } from 'app/navigation/objrefnav';
import { DeviceDetailService } from '../../../resource/device.gen/services';
import * as datatypes from '../../../typings/backendapi.gen';

 import { HttpClient } from '@angular/common/http';
 import { environment } from '../../../../environments/environment';
 import { kindToEndpoint, kindToUrlClass } from 'app/navigation/objrefnav.gen';

import { FuseLocationService } from 'app/services/location.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from'@angular/material/chips';
import * as _ from 'lodash';
import { zonedTimeToUtc, format, utcToZonedTime } from 'date-fns-tz';
import { ShowHideService } from "ngx-show-hide-password";
import jstz from 'jstz';

@Component({
    selector: 'device-status',
    templateUrl: './component.html',
    styleUrls: ['./component.scss'],
})

export class DeviceStatusComponent implements OnInit, OnDestroy {
    @ViewChild('form') public form: NgForm;
    @Input() public resourceName: string;
    @Input() public namespace: string;
    @Input() public device: datatypes.Device;
    @Output() refreshGridster: any = new EventEmitter();
    public loaded: boolean;
	  private timeZoneID = '';


    public duplex_spec_ethernet_duplexmode_enum: Array<{text: string, value: string}> = [
        { text: "full", value: "full" },
        { text: "half", value: "half" },
        { text: "auto", value: "auto" },
    ];
    public speed_spec_ethernet_portspeed_enum: Array<{text: string, value: string}> = [
        { text: "10Mpbs", value: "10Mpbs" },
        { text: "100Mbps", value: "100Mbps" },
        { text: "1Gbps", value: "1Gbps" },
        { text: "10Gbps", value: "10Gbps" },
        { text: "40Gbps", value: "40Gbps" },
        { text: "100Gbps", value: "100Gbps" },
        { text: "auto", value: "auto" },
    ];




    private _location: string;
    
    
    

    didReset: boolean = false;
    isEdit = false;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly service: DeviceDetailService,
	      private readonly _breadcrumbService: BreadcrumbService,
	      public router: Router,
        private _locationService: FuseLocationService,
        private _showHideService: ShowHideService,
        private readonly _http: HttpClient,
    ) {}

    ngOnInit(): void {
	this._locationService.isBrowserTime.pipe(takeUntil(this._unsubscribeAll)).subscribe(res=>{
    if(this._locationService.isBrowserTime.getValue()){
      const timezone = jstz.determine();
      this.timeZoneID = timezone.name();
    } else {
      this.timeZoneID = this._locationService.locationDataAsKeyValue().timezone
    }
  });
	this.loaded = true;

	this.initPolicyCondition();
        this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe((queryParams)=> {
	    if (!queryParams['loc'] || queryParams['loc'] === this._location) {
		return;
	    }
            this.resourceName = this.activatedRoute.snapshot.params.resourceName;
            this._location = this._locationService.locationDataAsKeyValue().currentLocation;

        });
    }

    ngOnChanges(change: SimpleChanges) {
        if (change.device) {
            this.form?.form.markAsPristine();
            
        }
    }

    onValueChanged(form: NgForm): void {
	this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: form.valid}));
        this.refreshGridster.emit();
    }


    public statusAplinksStringNormalizer = (text$: Observable<string>): any => text$.pipe(
        map((userInput: string) => {
            if (this.device.status.aplinks?.find(item => userInput.toLowerCase() === item.toLowerCase())) {
                return null;
            }
            return userInput;
        })
    )

    save(form): void {
    }

    ngOnDestroy(): void {
	this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
    }







    objectRefToUrl(item: any): string {
	if (!item) return "";
        const reflink = item.metadata.selfLink;
	const location = reflink.split("/")[2];
        if (item.kind === 'File') {
	    const filePath = `${item.spec.commonAttrs.rootDir}/${item.spec.base.real_name}`;
            return "/file-manager/file" + "?filePath=" + filePath + "?loc=" + location;
        }
        return reflink.split("/").slice(-2).join("/") + "?loc=" + location;
    }
    loadRefLink(item: any) {
	if (!item) return;

        const reflink = item.metadata.selfLink;
	const loc = reflink.split("/")[2];
        var url, filePath: string;
        if (item.kind === 'File') {
	    url = "/file-manager/file";
	    filePath = `${item.spec.commonAttrs.rootDir}/${item.spec.base.real_name}`;
        } else {
	    url = "/" + reflink.split("/").slice(-2).join("/");
        }
        this.router.navigate([url], { relativeTo: this.activatedRoute,
                queryParams: { loc: loc, filePath: filePath }, queryParamsHandling: 'merge' });
	return false;
    }

    initPolicyCondition(): void {
	
	
	
    }
  
  
  

    renderPolicyCondition(p: datatypes.PolicyCondition[]): string[] {
        var opSymbols = {
            "Equals": " = ",
            "NotEquals": " != ",
            "Lesser": " < ",
            "LesserOrEquals": " <= ",
            "Greater" : " > ",
            "GreaterOrEquals": " >= ",
            "Contains": " contains ",
            "DoesNotContain": " !contains ",
            "RegEx": " RegEx "
        };
        var orStr = [];
        if (p) {
            p.map( (condition, index) => {
                var andStr = [];
                var andConditions = "";
                if (condition.aovList) {
                    andStr = condition.aovList.map( aov => 
                        (aov.attr.replace("AttrName", "") + opSymbols[aov.op.replace("AttrOp", "")] + aov.val)
                    );
                    andConditions = andStr.join(" AND ");
                }
                if (index != 0) {
                    orStr = orStr.concat("OR");
                }
                orStr = orStr.concat(andConditions);
            });
        }
        return orStr;
    }
 

}
