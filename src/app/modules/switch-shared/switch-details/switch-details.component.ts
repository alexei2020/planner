import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
    ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from "@angular/platform-browser";
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import sortBy from 'lodash/sortBy';

import { BreadcrumbService } from 'app/main/shared/breadcrumb-bar.service';
import { PageType } from 'app/main/shared/breadcrumb-bar.service.gen';

import { DataApiService } from './services/data-api.service';

import Device from './devices/base/Device';
import AccessPoint from './devices/AccessPoint';
import Switch from './devices/Switch';
import Nexus from './devices/Nexus';
import {FuseLocationService} from '../../../services/location.service';

interface LineCard {
    name: string,
    operStatus: string,
}

@Component({
    selector: 'kd-switch-details',
    templateUrl: './switch-details.component.html',
    styleUrls: ['./switch-details.component.scss'],
})
export class SwitchDetailsComponent implements OnInit, OnDestroy, AfterViewInit {
    @Input() rows = 17;
    @Output() rowsChange = new EventEmitter<number>();

    @ViewChild('tooltip', { static: false }) tooltip: ElementRef<HTMLDivElement>;
    @ViewChild('switcher', { static: false }) canvas: ElementRef<HTMLCanvasElement>;

    tooltipElement: HTMLDivElement;

    deviceName: string;
    deviceLabel: string;

    switchBox: Device;

    inftsData: any;
    fansData: any;
    powerSupplyData: any;

    frontData: Array<any> = [];
    rearData: Array<any> = [];

    dropdownList: Array<any> = [];

    selectedItems: Array<any> = [];

    private _unsubscribeAll = new Subject<any>();

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _breadcrumbService: BreadcrumbService,
        private _dataService: DataApiService,
        private matIconRegistry: MatIconRegistry,
        private domSanitizer: DomSanitizer,
        private _locationService: FuseLocationService,
    ) {
        this.matIconRegistry.addSvgIcon(
            "flip-panel",
            this.domSanitizer.bypassSecurityTrustResourceUrl('assets/images/switch/flip-panel.svg')
        );
    }

    async ngOnInit() {
        // console.time('ngOnInit');

        // console.time('preload');
        Device.preload();
        // console.timeEnd('preload');

        // console.time('_breadcrumbService');
        this._breadcrumbService.itemSelected.pipe(takeUntil(this._unsubscribeAll)).subscribe(item => this.onItemSelect(item));
        this._breadcrumbService.deItemSelected.pipe(takeUntil(this._unsubscribeAll)).subscribe(item => this.onItemDeSelect(item));
        this._breadcrumbService.allSelected.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => this.onSelectAll());
        this._breadcrumbService.deAllSelected.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => this.onDeSelectAll());
        this._breadcrumbService.currentPage.next(PageType.INVENTORY_DETAIL);
        this._breadcrumbService.dropdownList.next(this.dropdownList);
        this._breadcrumbService.selectedItems.next(this.selectedItems);
        // console.timeEnd('_breadcrumbService');

        // console.time('_dataService');
        this.deviceName = this._activatedRoute.snapshot.params.resourceName;

        // console.time('deviceUIData');
        const deviceUIData = await this._dataService.getDeviceUIData(this.deviceName, this._locationService.locationDataAsKeyValue().currentLocation).toPromise();
        // console.timeEnd('deviceUIData');

        // console.time('categoryData');
        const categoryData = await this._dataService.getUICatalogDataNew(deviceUIData.status.pltfmCatalog).toPromise();
        const frontTemplate = categoryData.spec.front;
        const rearTemplate = categoryData.spec.rear;
        // console.timeEnd('categoryData');
        // console.timeEnd('_dataService');

        // console.time('prepareDat');
        this.deviceLabel = `${deviceUIData.status.vendor.replace(/^\w/, c => c.toUpperCase())} ${deviceUIData.status.model.toUpperCase()}`;

        this.inftsData = [
            ...deviceUIData.status.frontIntfs ? Object.values(deviceUIData.status.frontIntfs) : [],
            ...deviceUIData.status.rearIntfs ? Object.values(deviceUIData.status.rearIntfs) : [],
        ];
        this.fansData = [
            ...deviceUIData.status.frontFans ? Object.values(deviceUIData.status.frontFans) : [],
            ...deviceUIData.status.rearFans ? Object.values(deviceUIData.status.rearFans) : [],
        ];
        this.powerSupplyData = [
            ...deviceUIData.status.frontPsus ? Object.values(deviceUIData.status.frontPsus) : [],
            ...deviceUIData.status.rearPsus ? Object.values(deviceUIData.status.rearPsus) : [],
        ];

        const lineCards = deviceUIData.status.linecards ? Object.values(deviceUIData.status.linecards) : [];
        // console.timeEnd('prepareDat');

        // console.time('initPaper');
        // define the appropriate image type
        let switchImage = deviceUIData.status.type;
        if (!switchImage) {
            // fallback to default switch image in case if no switch_image info was passed
            switchImage = 'type1';
        }
        if (switchImage === 'type1') {
            if (lineCards.length) {
                this.rowsChange.emit(30);
                this.switchBox = new Nexus(this.canvas.nativeElement, lineCards.map((lineCard: LineCard) => lineCard.operStatus === 'PRESENT'));
            } else {
                this.rowsChange.emit(17);
                this.switchBox = new Switch(this.canvas.nativeElement);
            }
        } else if (switchImage === 'type2') {
            this.rowsChange.emit(17);
            this.switchBox = new AccessPoint(this.canvas.nativeElement);
        }
        // console.timeEnd('initPaper');

        // console.time('prepareItem');
        const commonItems = [
            ...this.inftsData,
            ...this.fansData,
            ...this.powerSupplyData,
        ];
        const frontItems = this.formItems(commonItems, frontTemplate);
        const rearItems = this.formItems(commonItems, rearTemplate);
        // console.timeEnd('prepareItem');

        // console.log('switchImage:', switchImage);
        // console.log('commonItems:', commonItems);
        // console.log('frontItems:', frontItems, frontTemplate);
        // console.log('rearItems:', rearItems, rearTemplate);

        // console.time('initSwitch');

        //
        if (this.switchBox) {
            this.switchBox
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(({ type, payload }) => {
                    // console.log('switch box subscribe:', type, payload);

                    //
                    this.frontData = payload.slot <= 0 ? frontItems.slice() : frontItems.filter(item => item.slotNum === payload.slot);
                    this.rearData = payload.slot <= 0 ? rearItems.slice() : rearItems.filter(item => item.slotNum === payload.slot);

                    // console.log('frontData:', this.frontData);
                    // console.log('rearData:', this.rearData);

                    //
                    if (this.switchBox instanceof Switch
                        || this.switchBox instanceof AccessPoint
                        || this.switchBox instanceof Nexus) {
                        // this.switchBox.setupFrontLayer([...this.frontData, ...this.frontData].slice(0, 60));
                        this.switchBox.setupFrontLayer(this.frontData);
                        this.switchBox.setupRearLayer(this.rearData);
                    }

                    //
                    if (this.switchBox.isFrontSide) {
                        this.showFrontSide();
                    } else {
                        this.showRearSide();
                    }
                });

            this.switchBox
                .itemClick
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(([event, item]) => {
                    this.onObjectClick(item.fullName, item.isSelected, item.type, event);
                });

            this.switchBox
                .itemEnter
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe((label: string) => {
                    this.tooltip.nativeElement.style.display = 'block';
                    this.tooltip.nativeElement.children[0].innerHTML = label;
                });

            this.switchBox
                .itemLeave
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    this.tooltip.nativeElement.style.display = 'none';
                });

            this.switchBox
                .itemOver
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe((event: any) => {
                    //console.log(event);
                    this.tooltip.nativeElement.style.top = `${event.clientY}px`;
                    this.tooltip.nativeElement.style.left = `${event.clientX}px`;
                });
        }

        // console.timeEnd('initSwitch');

        // console.timeEnd('ngOnInit');
    }

    async ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._breadcrumbService.currentPage.next(PageType.DEFAULT);

        document.body.removeChild(this.tooltip.nativeElement);
    }

    async ngAfterViewInit() {
        document.body.appendChild(this.tooltip.nativeElement);
    }

    /* */

    goBack() {
        if (this.switchBox) {
            this.switchBox.reset();
        }
    }

    flipSide() {
        if (this.switchBox) {
            if (this.switchBox.isFrontSide) {
                this.showRearSide();
            } else {
                this.showFrontSide();
            }
        }
    }

    showFrontSide() {
        if (this.switchBox) {
            this.switchBox.isFrontSide = true;
            this.switchBox.draw();
        }

        this.dropdownList = this.frontData;
        this._breadcrumbService.dropdownList.next(this.dropdownList);

        this.selectedItems = [];
        this._breadcrumbService.selectedItems.next(this.selectedItems);

        this.dropdownList.forEach(item => {
            this.updateSelectionState([item.item_text], item.item_type, 2);
        });
    }

    showRearSide() {
        if (this.switchBox) {
            this.switchBox.isFrontSide = false;
            this.switchBox.draw();
        }

        this.dropdownList = this.rearData;
        this._breadcrumbService.dropdownList.next(this.dropdownList);

        this.selectedItems = [];
        this._breadcrumbService.selectedItems.next(this.selectedItems);

        this.dropdownList.forEach(item => {
            this.updateSelectionState([item.item_text], item.item_type, 2);
        });
    }

    /* */

    private formItems(items, template) {
        if (!template) {
            return [];
        }

        const results = items.reduce((items, item) => {
            let templateItem = null;
            template.some((tpl) => {
                const regex = new RegExp(tpl.name.replace('{$slotId}', '(\\d+)').replace('{$num}', '(\\d+)'), 'i');
                const match = item.name.match(regex);
                if (!match) {
                    return false;
                }

                templateItem = {
                    ...tpl,
                    num: +(match.length === 3 ? match[2] : match[1]),
                    slotId: +(match.length === 3 ? match[1] : 1),
                };
                return true;
            });
            if (!templateItem) {
                return items;
            }

            const newItem = {
                isSelected: false,
                item_id: `${item.name.replace(/\//g, '-')}-${this.deviceName}`.toLowerCase(),
                item_text: item.name,
                item_type: item.type,
                fullName: item.name,
                name: item.name,
                type: templateItem.type,
                orientation: templateItem.orientation,
                itemNum: templateItem.num,
                slotNum: templateItem.slotId,
                image: `${templateItem.type}.svg`,
            };

            const { objectImage, itemState } = this.getDeviceImage(newItem);

            return [
                ...items,
                {
                    ...newItem,
                    backgroundImgUrl: objectImage,
                    status: itemState,
                },
            ];
        }, []);

        return sortBy(results, ['type', 'itemNum']);
    }

    private getObjectType(objectName: string) {
        if (this.switchBox?.isFrontSide) {
            for (let index = 0; index < this.frontData.length; index++) {
                if (objectName == this.frontData[index].item_text) {
                    return this.frontData[index].item_type;
                }
            }
        } else {
            for (let index = 0; index < this.rearData.length; index++) {
                if (objectName == this.rearData[index].item_text) {
                    return this.rearData[index].item_type;
                }
            }
        }
        return '';
    }

    private getDeviceImage(el) {
        let objectImage: string;
        let itemState: string;

        const images = [
            {
                type: 'copperInterface',
                lookupVal_images: [
                    { image: 'infsUp.svg', lookupVal: 'up' },
                    { image: 'infsDown.svg', lookupVal: 'down' },
                ],
            },
            {
                type: 'fan',
                lookupVal_images: [
                    { image: 'fanok.svg', lookupVal: 'ok' },
                    { image: 'fanok.svg', lookupVal: 'present' },
                    { image: 'fannotpresent.svg', lookupVal: 'notpresent' },
                    { image: 'fannotpresent.svg', lookupVal: 'faulty' },
                    { image: 'fannotpresent.svg', lookupVal: 'unknown' },
                ],
            },
            {
                type: 'powersupply',
                lookupVal_images: [
                    { image: 'powersupplyok.svg', lookupVal: 'ok' },
                    { image: 'powersupplyok.svg', lookupVal: 'present' },
                    { image: 'powersupplynotpresent.svg', lookupVal: 'notpresent' },
                    { image: 'powersupplynotpresent.svg', lookupVal: 'faulty' },
                    { image: 'powersupplynotpresent.svg', lookupVal: 'unknown' },
                ],
            },
        ];

        if (el.type == 'copperInterface') {
            this.inftsData.some(element => {
                if (element.name == el.name) {
                    images.forEach(obj => {
                        if (obj.type == el.type) {
                            const opStatus = element.operStatus;
                            obj.lookupVal_images.some(img => {
                                if (opStatus == img.lookupVal) {
                                    objectImage = `assets/images/switch/${img.image}`;
                                    itemState = opStatus.replace(/\//g, '-');
                                    return true;
                                }
                                objectImage = `assets/images/switch/${el.image}`;
                                itemState = opStatus.replace(/\//g, '-');
                            });
                        }
                    });
                }
            });
        } else if (el.type == 'fan') {
            this.fansData.some(element => {
                if (element.name == el.name) {
                    images.forEach(obj => {
                        if (obj.type == el.type) {
                            const opStatus: string = element.operStatus;
                            obj.lookupVal_images.some((img: any) => {
                                if (opStatus.toLowerCase() == img.lookupVal.toLowerCase()) {
                                    objectImage = `assets/images/switch/${img.image}`;
                                    itemState = opStatus.replace(/\//g, '-');
                                    return true;
                                }
                                objectImage = `assets/images/switch/${el.image}`;
                                itemState = opStatus.replace(/\//g, '-');
                            });
                        }
                    });
                }
            });
        } else if (el.type == 'powersupply') {
            this.powerSupplyData.some(element => {
                if (element.name == el.name) {
                    images.forEach(obj => {
                        if (obj.type == el.type) {
                            const opStatus: string = element.operStatus;
                            obj.lookupVal_images.some((img: any) => {
                                if (opStatus.toLowerCase() == img.lookupVal.toLowerCase()) {
                                    objectImage = `assets/images/switch/${img.image}`;
                                    itemState = opStatus.replace(/\//g, '-');
                                    return true;
                                }
                                objectImage = `assets/images/switch/${el.image}`;
                                itemState = opStatus.replace(/\//g, '-');
                            });
                        }
                    });
                }
            });
        }

        return { objectImage, itemState };
    }

    private onItemSelect(item: any) {
        this.updateSelectionState([`${item.item_text}`], this.getObjectType(item.item_text), 0);
    }

    private onItemDeSelect(item: any) {
        this.updateSelectionState([`${item.item_text}`], this.getObjectType(item.item_text), 0);
    }

    private onSelectAll() {
        this.selectedItems = this.dropdownList.map(item => ({ ...item }));
        this.selectedItems.forEach(item => this.updateSelectionState([item.item_text], item.item_type, 1));
        this._breadcrumbService.selectedItems.next(this.selectedItems);
    }

    private onDeSelectAll() {
        this.selectedItems = [];
        this._breadcrumbService.selectedItems.next(this.selectedItems);
        this.dropdownList.forEach(item => {
            this.updateSelectionState([item.item_text], item.item_type, 2);
        });
    }

    private onObjectClick(name: string, isSelected: boolean, type: string, event) {
        let insertMode = 0;
        if (event.shiftKey || this.selectedItems.length == 0) {
            this.dropdownList.some(item => {
                if (item.item_text == name) {
                    if (!isSelected) {
                        this.selectedItems.push(item);
                    } else {
                        for (let index = 0; index < this.selectedItems.length; index++) {
                            if (this.selectedItems[index].item_text == name) {
                                this.selectedItems.splice(index, 1);
                                break;
                            }
                        }
                    }
                    this.selectedItems = this.selectedItems.map(item => ({
                        ...item,
                    }));
                }
            });

            insertMode = 0;
        } else {
            this.dropdownList.some(item => {
                if (item.item_text == name) {
                    this.selectedItems = [item];
                }
            });

            insertMode = 3;
        }

	switch (type) {
	case "copperInterface":
	    this._breadcrumbService.platformSelected.next({ name: "Interfaces", id: "Interfaces" });
	    break;
	case "fan":
	case "powersupply":
	    this._breadcrumbService.platformSelected.next({ name: "Platform", id: "Platform" });
	    break;
	}
        this._breadcrumbService.selectedItems.next(this.selectedItems);

        this.updateSelectionState([name], type, insertMode);
    }

    private updateSelectionState(ids: any, type: string, mode: number = 0) {
        let activeData = [];
        if (this.switchBox?.isFrontSide) {
            activeData = this.frontData;
        } else {
            activeData = this.rearData;
        }
        if (mode == 0) {
            ids.forEach((id: string) => {
                activeData.forEach(item => {
                    if (item.fullName == id) {
                        item.isSelected = !item.isSelected;
                    }
                });
            });
        } else if (mode == 1) {
            ids.forEach((id: string) => {
                activeData.forEach(item => {
                    item.isSelected = true;
                });
            });
        } else if (mode == 2) {
            ids.forEach((id: string) => {
                activeData.forEach(item => {
                    item.isSelected = false;
                });
            });
        } else {
            ids.forEach((id: string) => {
                activeData.forEach(item => {
                    item.isSelected = item.fullName == id;
                });
            });
        }

        this.switchBox?.draw();
    }
}
