import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})

export class DataApiService {
    constructor(private http: HttpClient) {
    }

    getDeviceData(deviceName: string, loc: string): Observable<any> {
        const url = `${environment.apiUrl}/apis/${loc}/v1/devices/${deviceName}`;
        return this.http.get(url).pipe(map(res => res));
    }

    getUICatalogDataNew(url: string): Observable<any> {
        return this.http.get(environment.apiUrl+url).pipe(map(res => res));
    }

    getUICatalogData(catalogName: string, loc: string): Observable<any> {
        const url = `${environment.apiUrl}/apis/${loc}/v1/namespaces/network/uicatalogs/${catalogName}`;
        return this.http.get(url).pipe(map(res => res));
    }

    getDeviceUIData(deviceName: string, loc: string): Observable<any> {
        const url = `${environment.apiUrl}/apis/${loc}/v1/deviceuis/${deviceName}`;
        return this.http.get(url).pipe(map(res => res));
    }

    /* enable this when porting back
      getInterfaceData(deviceUID: string, loc: string): Observable<any> {
          const url = `${environment.apiUrl}/apis/${loc}/v1/ifinterfaces?wsSelector=spec.base.parent_link=${deviceUID}`;
        return this.http.get(url).pipe(map(res => res));
      }
      getFans(deviceUID: string, loc: string):Observable<any>{
          const url = `${environment.apiUrl}/apis/${loc}/v1/platformcomponents?wsSelector=status.type=fan,spec.base.parent_link=${deviceUID}`;
        return this.http.get(url).pipe(map(res=>res));
      }
      getPowerSupplies(deviceUID: string, loc: string):Observable<any>{
          const url = `${environment.apiUrl}/apis/${loc}/v1/platformcomponents?wsSelector=status.type=psu,spec.base.parent_link=${deviceUID}`;
        return this.http.get(url).pipe(map(res=>res));
	}
    */

    getInterfaceData(deviceName: string, loc: string): Observable<any> {
        const url = `${environment.apiUrl}/apis/${loc}/v1/ifinterfaces?wsSelector=spec.base.parent_link=/apis/${loc}/v1/devices/${deviceName}`;
        return this.http.get(url).pipe(map(res => res));
    }

    getFans(deviceName: string, loc: string): Observable<any> {
        const url = `${environment.apiUrl}/apis/${loc}/v1/platformcomponents?wsSelector=status.type=fan,spec.base.parent_link=/apis/${loc}/v1/devices/${deviceName}`;
        return this.http.get(url).pipe(map(res => res));
    }

    getPowerSupplies(deviceName: string, loc: string): Observable<any> {
        const url = `${environment.apiUrl}/apis/${loc}/v1/platformcomponents?wsSelector=status.type=psu,spec.base.parent_link=/apis/${loc}/v1/devices/${deviceName}`;
        return this.http.get(url).pipe(map(res => res));
    }

    getLineCards(deviceName: string, loc: string): Observable<any> {
        const url = `${environment.apiUrl}/apis/${loc}/v1/platformcomponents?wsSelector=status.type=linecard,spec.base.parent_link=/apis/${loc}/v1/devices/${deviceName}`;
        return this.http.get(url).pipe(map(res => res));
    }
    getFanData(fanName: string, loc: string): Observable<any> {
        const url = `${environment.apiUrl}/apis/${loc}/v1/platformcomponents/${fanName}`;
        return this.http.get(url).pipe(map(res => res));
    }

    getPowerSupplyData(powerSupplyName: string, loc: string): Observable<any> {
        const url = `${environment.apiUrl}/apis/${loc}/v1/platformcomponents/${powerSupplyName}`;
        return this.http.get(url).pipe(map(res => res));
    }
}
