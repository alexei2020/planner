import paper, { Layer, Color, Path, Point } from 'paper';

import Device from './base/Device';

export default class AccessPoint extends Device {
    public ui: any = {
        width: 528,
        height: 128,
        scale: 0.5,
        dimensions: {
            width: 528,
            height: 128,
        },
        xStep: 26.4,
        yStep: 32,

        switch_front_back_color: '#DADADA',
        switch_top_color: '#878787',
        vendor_logo: 'cisco.svg',
        vendor_logo_color: '#EEE',
        device_image: 'access.svg',
    };

    setupFrontLayer(items = []) {
        this.setupFrontItemsLayer(items);

        this.frontLayer.addChild(this.drawBodyArea());
        this.frontLayer.addChild(this.drawDepth());
        this.frontLayer.addChild(this.drawAntenna(new Point(this.ui.xStep * 2, this.ui.yStep * 1.3)));
        this.frontLayer.addChild(this.drawAntenna(new Point(this.ui.width - (this.ui.xStep * 2) - 50, this.ui.yStep * 1.3)));

        this.frontItemsLayer.bringToFront();
    }

    setupRearLayer(items = []) {
        this.setupRearItemsLayer(items);

        this.rearLayer.addChild(this.drawBodyArea());
        this.rearLayer.addChild(this.drawDepth());
        this.rearLayer.addChild(this.drawAntenna(new Point(this.ui.xStep, this.ui.yStep * 1.9)));
        this.rearLayer.addChild(this.drawAntenna(new Point(this.ui.width - this.ui.xStep - 50, this.ui.yStep * 1.9)));

        this.rearItemsLayer.bringToFront();
    }

    setupFrontItemsLayer(items) {
        let marginX = 60;
        let marginY = 84;
        let offsetX = 0;
        let offsetY = 0;
        let lastX = 0;
        let lastY = 0;
        let lastO = '';
        let lastW = 0;
        let lastI = 0;

        items.forEach((item) => {
            let x = 0;
            let y = 0;
            let interval = 4;

            let width = 30;
            let height = 30;
            if (item.type === 'copperInterface') {
                width = 30;
                height = 30;
            } else if (item.type === 'powersupply') {
                width = 95;
                height = 63;
            } else if (item.type === 'fan') {
                width = 63;
                height = 63;
            }

            let orientation = 'up';
            if (item.orientation === '2rows,colfirst' || item.orientation === '2rows,rowfirst') {
                if (lastO === '2rows,colfirst' || lastO === '2rows,rowfirst') {
                    if (lastY === offsetY) {
                        x = offsetX;
                        y = offsetY + height + interval;
                        orientation = 'down';
                    } else {
                        x = offsetX + lastW + interval;
                        y = offsetY;
                    }
                } else {
                    x = offsetX + lastW + interval;
                    y = offsetY;
                }
            } else if (item.orientation === 'halfrow,top') {
                x = offsetX + lastW + interval;
                y = offsetY;
            } else if (item.orientation === 'onerow,centered') {
                x = offsetX + lastW + interval;
                y = offsetY;
            }

            lastX = x;
            lastY = y;
            lastO = item.orientation;
            lastW = width;
            lastI = interval;
            offsetX = x;

            if (item.type === 'copperInterface') {
                const intf = this.createInterface(item.backgroundImgUrl, item.name, item.itemNum, orientation);
                intf['userData'] = item;
                intf.position.x += marginX + x;
                intf.position.y += marginY + y;
                intf.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.frontItemsLayer.addChild(intf);
            } else if (item.type === 'powersupply') {
                const powerSupply = this.createPowerSupply(item.backgroundImgUrl, item.name);
                powerSupply['userData'] = item;
                powerSupply.position.x += marginX + x;
                powerSupply.position.y += marginY + y;
                powerSupply.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.frontItemsLayer.addChild(powerSupply);
            } else if (item.type === 'fan') {
                const fan = this.createFan(item.backgroundImgUrl, item.name);
                fan['userData'] = item;
                fan.position.x += marginX + x;
                fan.position.y += marginY + y;
                fan.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.frontItemsLayer.addChild(fan);
            }
        });
    }

    setupRearItemsLayer(items) {
        let marginX = 60;
        let marginY = 84;
        let offsetX = 0;
        let offsetY = 0;
        let lastX = 0;
        let lastY = 0;
        let lastO = '';
        let lastW = 0;
        let lastI = 0;

        items.forEach((item) => {
            let x = 0;
            let y = 0;
            let interval = 4;

            let width = 30;
            let height = 30;
            if (item.type === 'copperInterface') {
                width = 30;
                height = 30;
            } else if (item.type === 'powersupply') {
                width = 95;
                height = 63;
            } else if (item.type === 'fan') {
                width = 63;
                height = 63;
            }

            let orientation = 'up';
            if (item.orientation === '2rows,colfirst' || item.orientation === '2rows,rowfirst') {
                if (lastO === '2rows,colfirst' || lastO === '2rows,rowfirst') {
                    if (lastY === offsetY) {
                        x = offsetX;
                        y = offsetY + height + interval;
                        orientation = 'down';
                    } else {
                        x = offsetX + lastW + interval;
                        y = offsetY;
                    }
                } else {
                    x = offsetX + lastW + interval;
                    y = offsetY;
                }
            } else if (item.orientation === 'halfrow,top') {
                x = offsetX + lastW + interval;
                y = offsetY;
            } else if (item.orientation === 'onerow,centered') {
                x = offsetX + lastW + interval;
                y = offsetY;
            }

            lastX = x;
            lastY = y;
            lastO = item.orientation;
            lastW = width;
            lastI = interval;
            offsetX = x;

            if (item.type === 'copperInterface') {
                const intf = this.createInterface(item.backgroundImgUrl, item.name, item.itemNum, orientation);
                intf['userData'] = item;
                intf.position.x += marginX + x;
                intf.position.y += marginY + y;
                intf.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.rearItemsLayer.addChild(intf);
            } else if (item.type === 'powersupply') {
                const powerSupply = this.createPowerSupply(item.backgroundImgUrl, item.name);
                powerSupply['userData'] = item;
                powerSupply.position.x += marginX + x;
                powerSupply.position.y += marginY + y;
                powerSupply.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.rearItemsLayer.addChild(powerSupply);
            } else if (item.type === 'fan') {
                const fan = this.createFan(item.backgroundImgUrl, item.name);
                fan['userData'] = item;
                fan.position.x += marginX + x;
                fan.position.y += marginY + y;
                fan.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.rearItemsLayer.addChild(fan);
            }
        });
    }

    /* */

    drawBodyArea() {
        const topSection = new paper.Group();

        // spread ui properties
        const { xStep, yStep, width, height } = this.ui;

        // draw top section
        const topHat = new Path({
            closed: true,
            fillColor: new Color('#2d78d2'),
        });
        topHat.moveTo(new Point(0, yStep * 2.25));
        topHat.quadraticCurveTo(new Point(0, yStep * 2), new Point(xStep / 2, yStep * 2));
        topHat.lineTo(new Point(width - xStep / 2, yStep * 2));
        topHat.quadraticCurveTo(new Point(width, yStep * 2), new Point(width, yStep * 2.25));
        topSection.addChild(topHat);

        const bottom = new Path({
            closed: true,
            fillColor: new Color('#CFE8FF'),
        });
        bottom.moveTo(new Point(0, yStep * 2.25));
        bottom.lineTo(new Point(width, yStep * 2.25));
        bottom.quadraticCurveTo(new Point(width, height), new Point(width - xStep * 3, height));
        bottom.lineTo(new Point(xStep * 3, height));
        bottom.quadraticCurveTo(new Point(0, height), new Point(0, yStep * 2.25));
        topSection.addChild(bottom);

        return topSection;
    }

    drawDepth() {
        // spread ui properties
        const { xStep, yStep, width } = this.ui;

        // draw depth effect
        const depth = new Path({
            closed: true,
            fillColor: new Color('#5195E9'),
        });
        depth.moveTo(new Point(xStep / 3, yStep * 2));
        depth.lineTo(new Point(xStep * 2, yStep));
        depth.lineTo(new Point(width - xStep * 2, yStep));
        depth.lineTo(new Point(width - xStep / 3, yStep * 2));

        return depth;
    }

    drawAntenna(start, rearView = false) {
        const antenna = new paper.Group();
        const rearScale = 0.9;

        // draw base
        const base = new Path({
            closed: true,
            fillColor: new Color('#424242'),
        });
        if (rearView) {
            base.moveTo(start);
            base.lineTo(new Point(start.x + 50 * rearScale, start.y));
            base.lineTo(new Point(start.x + 40 * rearScale, start.y - 5 * rearScale));
            base.lineTo(new Point(start.x + 10 * rearScale, start.y - 5 * rearScale));
            antenna.addChild(base);
        } else {
            base.moveTo(start);
            base.lineTo(new Point(start.x + 50, start.y));
            base.lineTo(new Point(start.x + 40, start.y - 5));
            base.lineTo(new Point(start.x + 10, start.y - 5));
            antenna.addChild(base);
        }

        // draw top
        const top = new Path({
            closed: true,
            fillColor: new Color('#666565'),
        });
        top.moveTo(new Point(start.x + 10, start.y - 5));
        top.lineTo(new Point(start.x + 40, start.y - 5));
        top.lineTo(new Point(start.x + 40, start.y - 30));
        top.lineTo(new Point(start.x + 10, start.y - 30));
        antenna.addChild(top);

        return antenna;
    }

    draw() {
        this.frontLayer.visible = this.isFrontSide;
        this.rearLayer.visible = !this.isFrontSide;

        [...this.frontItemsLayer.children, ...this.rearItemsLayer.children].forEach((child) => {
            if (child.userData) {
                if (child.userData.isSelected) {
                    child.selectItem();
                } else {
                    child.deselectItem();
                }
            }
        });

        this.resize();
    }

    reset() {
        this.isFrontSide = true;

        this.draw();
    }
}
