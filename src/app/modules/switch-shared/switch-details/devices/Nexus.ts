import paper, { Point, Color } from 'paper';

import Device from './base/Device';

export default class Nexus extends Device {
    public isFrontSide = true;

    public ui: any = {
        width: 280,
        depthOffset: 45,
        xStep: 30,
        yStep: 32,
    };

    private rackLayer: paper.Layer;

    private panelStep = 26;

    constructor(canvas: HTMLCanvasElement, private panels: boolean[] = []) {
        super(canvas);
    }

    setup() {
        super.setup();

        this.rackLayer = new paper.Layer();
        this.rackLayer.name = 'RackLayer';
        this.backgroundLayer.addChild(this.rackLayer);
    }

    setupFrontLayer(items = []) {
        console.log('setupFrontLayer');

        this.rackLayer.removeChildren();
        this.rackLayer.addChild(this.drawFrontDepth());
        this.rackLayer.addChild(this.drawFrontTop());
        this.panels.forEach((panel, i) => {
            this.rackLayer.addChild(this.drawPanel((64 - this.panelStep) + (i + 1) * this.panelStep, !panel, this.panelStep, i + 1));
        });

        //

        this.frontLayer.removeChildren();

        this.setupFrontItemsLayer(items);

        this.frontLayer.addChild(this.drawPanelStandaloneFront());
        this.frontLayer.position.x += this.ui.width + 50;
        this.frontLayer.position.y += (this.rackLayer.bounds.height / 2) - (this.frontLayer.bounds.height / 2);

        this.frontItemsLayer.bringToFront();
    }

    setupRearLayer(items = []) {
        this.rearLayer.removeChildren();

        this.setupRearItemsLayer(items);

        this.rearLayer.addChild(this.drawPanelStandaloneRear());
        this.rearLayer.position.x += this.ui.width + 50;
        this.rearLayer.position.y += (this.rackLayer.bounds.height / 2) - (this.rearLayer.bounds.height / 2);

        this.rearItemsLayer.bringToFront();
    }

    /* */

    setupFrontItemsLayer(items) {
        this.frontItemsLayer.removeChildren();
        this.frontLayer.addChild(this.frontItemsLayer);

        let marginX = 80;
        let marginY = 58;
        let offsetX = 0;
        let offsetY = 0;
        let lastX = 0;
        let lastY = 0;
        let lastO = '';
        let lastW = 0;
        let lastI = 0;

        items.forEach((item) => {
            let x = 0;
            let y = 0;
            let interval = 4;

            let width = 30;
            let height = 30;
            if (item.type === 'copperInterface') {
                width = 30;
                height = 30;
            } else if (item.type === 'powersupply') {
                width = 95;
                height = 63;
            } else if (item.type === 'fan') {
                width = 63;
                height = 63;
            }

            let orientation = 'up';
            if (item.orientation === '2rows,colfirst' || item.orientation === '2rows,rowfirst') {
                if (lastO === '2rows,colfirst' || lastO === '2rows,rowfirst') {
                    if (lastY === offsetY) {
                        x = offsetX;
                        y = offsetY + height + interval;
                        orientation = 'down';
                    } else {
                        x = offsetX + lastW + interval;
                        y = offsetY;
                    }
                } else {
                    x = offsetX + lastW + interval;
                    y = offsetY;
                }
            } else if (item.orientation === 'halfrow,top') {
                x = offsetX + lastW + interval;
                y = offsetY;
            } else if (item.orientation === 'onerow,centered') {
                x = offsetX + lastW + interval;
                y = offsetY;
            }

            lastX = x;
            lastY = y;
            lastO = item.orientation;
            lastW = width;
            lastI = interval;
            offsetX = x;

            if (item.type === 'copperInterface') {
                const intf = this.createInterface(item.backgroundImgUrl, item.name, item.itemNum, orientation);
                intf['userData'] = item;
                intf.position.x += marginX + x;
                intf.position.y += marginY + y;
                intf.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.frontItemsLayer.addChild(intf);
            } else if (item.type === 'powersupply') {
                const powerSupply = this.createPowerSupply(item.backgroundImgUrl, item.name);
                powerSupply['userData'] = item;
                powerSupply.position.x += marginX + x;
                powerSupply.position.y += marginY + y;
                powerSupply.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.frontItemsLayer.addChild(powerSupply);
            } else if (item.type === 'fan') {
                const fan = this.createFan(item.backgroundImgUrl, item.name);
                fan['userData'] = item;
                fan.position.x += marginX + x;
                fan.position.y += marginY + y;
                fan.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.frontItemsLayer.addChild(fan);
            }
        });
    }

    setupRearItemsLayer(items) {
        this.rearItemsLayer.removeChildren();
        this.rearLayer.addChild(this.rearItemsLayer);

        let marginX = 90;
        let marginY = 90;
        let offsetX = 0;
        let offsetY = 0;
        let lastX = 0;
        let lastY = 0;
        let lastO = '';
        let lastW = 0;
        let lastI = 0;

        items.forEach((item) => {
            let x = 0;
            let y = 0;
            let interval = 4;

            let width = 30;
            let height = 30;
            if (item.type === 'copperInterface') {
                width = 30;
                height = 30;
            } else if (item.type === 'powersupply') {
                width = 95;
                height = 63;
            } else if (item.type === 'fan') {
                width = 63;
                height = 63;
            }

            let orientation = 'up';
            if (item.orientation === '2rows,colfirst' || item.orientation === '2rows,rowfirst') {
                if (lastO === '2rows,colfirst' || lastO === '2rows,rowfirst') {
                    if (lastY === offsetY) {
                        x = offsetX;
                        y = offsetY + height + interval;
                        orientation = 'down';
                    } else {
                        x = offsetX + lastW + interval;
                        y = offsetY;
                    }
                } else {
                    x = offsetX + lastW + interval;
                    y = offsetY;
                }
            } else if (item.orientation === 'halfrow,top') {
                x = offsetX + lastW + interval;
                y = offsetY;
            } else if (item.orientation === 'onerow,centered') {
                x = offsetX + lastW + interval;
                y = offsetY;
            }

            lastX = x;
            lastY = y;
            lastO = item.orientation;
            lastW = width;
            lastI = interval;
            offsetX = x;

            if (item.type === 'copperInterface') {
                const intf = this.createInterface(item.backgroundImgUrl, item.name, item.itemNum, orientation);
                intf['userData'] = item;
                intf.position.x += marginX + x;
                intf.position.y += marginY + y;
                intf.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.rearItemsLayer.addChild(intf);
            } else if (item.type === 'powersupply') {
                const powerSupply = this.createPowerSupply(item.backgroundImgUrl, item.name);
                powerSupply['userData'] = item;
                powerSupply.position.x += marginX + x;
                powerSupply.position.y += marginY + y;
                powerSupply.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.rearItemsLayer.addChild(powerSupply);
            } else if (item.type === 'fan') {
                const fan = this.createFan(item.backgroundImgUrl, item.name);
                fan['userData'] = item;
                fan.position.x += marginX + x;
                fan.position.y += marginY + y;
                fan.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.rearItemsLayer.addChild(fan);
            }
        });
    }

    /* */

    drawFrontTop() {
        const area = new paper.Group();

        // spread UI properties
        const { xStep, yStep, width, depthOffset } = this.ui;

        // draw "hat" with arcs
        const hat = new paper.Path({
            closed: true,
            fillColor: new Color('#5195E9'),
            style: {
                strokeColor: new Color('black'),
            },
        });
        hat.moveTo(new Point(depthOffset, yStep));
        hat.quadraticCurveTo(new Point(depthOffset, yStep / 2), new Point(xStep + depthOffset, yStep / 2));
        hat.lineTo(new Point(width - xStep, yStep / 2));
        hat.quadraticCurveTo(new Point(width, yStep / 2), new Point(width, yStep));
        hat.lineTo(new Point(width, yStep));
        hat.lineTo(new Point(depthOffset, yStep));
        area.addChild(hat);

        // top section notch
        const notch = new paper.Path({
            closed: true,
            fillColor: new Color('#5195E9'),
            style: {
                strokeColor: new Color('black'),
            },
        });
        notch.lineTo(new Point(depthOffset + xStep * 2, yStep));
        notch.lineTo(new Point(depthOffset + xStep * 2, yStep * 1.5));
        notch.lineTo(new Point(width - xStep * 2, yStep * 1.5));
        notch.lineTo(new Point(width - xStep * 2, yStep));
        notch.lineTo(new Point(depthOffset + xStep * 2, yStep));
        area.addChild(notch);

        // top section
        const topArea = new paper.Path({
            closed: true,
            fillColor: new Color('#2d78d2'),
            style: {
                strokeColor: new Color('black'),
            },
        });
        topArea.moveTo(new Point(depthOffset, yStep));
        topArea.lineTo(new Point(depthOffset + xStep * 2, yStep));
        topArea.lineTo(new Point(depthOffset + xStep * 2, yStep * 1.5));
        topArea.lineTo(new Point(width - xStep * 2, yStep * 1.5));
        topArea.lineTo(new Point(width - xStep * 2, yStep));
        topArea.lineTo(new Point(width, yStep));
        topArea.lineTo(new Point(width, yStep * 2));
        topArea.lineTo(new Point(depthOffset, yStep * 2));
        area.addChild(topArea);

        // add holes line
        const holesLine = this.drawHolesLine(xStep * 2 + depthOffset, width - (xStep * 2), yStep * 1.75);
        area.addChild(holesLine);

        return area;
    };

    drawPanel(offset, isStub: boolean = false, step = 64, slotId = null) {
        const { depthOffset, xStep, width } = this.ui;

        const panel = new paper.Group();
        if (!isStub) {
            panel.onMouseEnter = () => {
                bodyArea.fillColor = new Color('#accfef');
            };
            panel.onMouseLeave = () => {
                bodyArea.fillColor = new Color('#CFE8FF');
            };
            panel.onClick = () => {
                this.selectedSlot = slotId;

                this.next({
                    type: 'click',
                    payload: {
                        slot: slotId,
                    },
                });
            };
        }

        // panel side left
        const leftSide = new paper.Path({
            fillColor: new Color('#1d375c'),
            closed: true,
            style: {
                strokeColor: new Color('black'),
            },
        });
        leftSide.moveTo(new Point(depthOffset, offset));
        leftSide.lineTo(new Point(depthOffset + xStep / 2, offset));
        leftSide.lineTo(new Point(depthOffset + xStep / 2, offset + step));
        leftSide.lineTo(new Point(depthOffset, offset + step));
        panel.addChild(leftSide);

        // panel body area
        const bodyArea = new paper.Path({
            fillColor: new Color(isStub ? '#7c7a7a' : '#CFE8FF'),
            closed: true,
            style: {
                strokeColor: new Color('black'),
            },
        });
        bodyArea.moveTo(new Point(depthOffset + xStep / 2, offset));
        bodyArea.lineTo(new Point(width - xStep / 2, offset));
        bodyArea.lineTo(new Point(width - xStep / 2, offset + step));
        bodyArea.lineTo(new Point(depthOffset + xStep / 2, offset + step));
        panel.addChild(bodyArea);

        // panel side right
        const rightSide = new paper.Path({
            fillColor: new Color('#1d375c'),
            closed: true,
            style: {
                strokeColor: new Color('black'),
            },
        });
        rightSide.moveTo(new Point(width, offset));
        rightSide.lineTo(new Point(width, offset + step));
        rightSide.lineTo(new Point(width - xStep / 2, offset + step));
        rightSide.lineTo(new Point(width - xStep / 2, offset));
        panel.addChild(rightSide);

        return panel;
    };

    drawPanelStandaloneFront(width = 1056, height = 120) {
        const panel = new paper.Group();

        const depthHeight = height / 4;

        // panel side left
        const leftSide = new paper.Path({
            fillColor: new Color('#1d375c'),
            closed: true,
            style: {
                strokeColor: new Color('black'),
            },
        });
        leftSide.moveTo(new Point(0, depthHeight));
        leftSide.lineTo(new Point(height / 2, depthHeight));
        leftSide.lineTo(new Point(height / 2, height + depthHeight));
        leftSide.lineTo(new Point(0, height + depthHeight));
        panel.addChild(leftSide);

        // panel body area
        const bodyArea = new paper.Path({
            fillColor: new Color('#CFE8FF'),
            closed: true,
            style: {
                strokeColor: new Color('black'),
            },
        });
        bodyArea.moveTo(new Point(height / 2, depthHeight));
        bodyArea.lineTo(new Point(width - (height / 2), depthHeight));
        bodyArea.lineTo(new Point(width - (height / 2), height + depthHeight));
        bodyArea.lineTo(new Point(height / 2, height + depthHeight));
        panel.addChild(bodyArea);

        // panel side right
        const rightSide = new paper.Path({
            fillColor: new Color('#1d375c'),
            closed: true,
            style: {
                strokeColor: new Color('black'),
            },
        });
        rightSide.moveTo(new Point(width, depthHeight));
        rightSide.lineTo(new Point(width, height + depthHeight));
        rightSide.lineTo(new Point(width - (height / 2), height + depthHeight));
        rightSide.lineTo(new Point(width - (height / 2), depthHeight));
        panel.addChild(rightSide);

        // panel depths
        const depths = new paper.Path({
            fillColor: new Color('#7c7a7a'),
            closed: true,
            style: {
                strokeColor: new Color('black'),
            },
        });
        depths.moveTo(new Point(height / 2, depthHeight));
        depths.lineTo(new Point(width - (height / 2), depthHeight));
        depths.lineTo(new Point(width - (height * 1.5), 0));
        depths.lineTo(new Point(height * 1.5, 0));
        panel.addChild(depths);

        return panel;
    };

    drawPanelStandaloneRear(width = 1056, height = 120) {
        const panel = new paper.Group();

        const depthHeight = height / 2;

        // panel body area
        const bodyArea = new paper.Path({
            fillColor: new Color('#7c7a7a'),
            closed: true,
            style: {
                strokeColor: new Color('black'),
            },
        });
        bodyArea.moveTo(new Point(height / 2, depthHeight));
        bodyArea.lineTo(new Point(width - (height / 2), depthHeight));
        bodyArea.lineTo(new Point(width - (height / 2), height + depthHeight));
        bodyArea.lineTo(new Point(height / 2, height + depthHeight));
        panel.addChild(bodyArea);

        // panel depths
        const depths = new paper.Path({
            fillColor: new Color('#7c7a7a'),
            closed: true,
            style: {
                strokeColor: new Color('black'),
            },
        });
        depths.moveTo(new Point(height / 2, depthHeight));
        depths.lineTo(new Point(width - (height / 2), depthHeight));
        depths.lineTo(new Point(width - (height * 1.5), 0));
        depths.lineTo(new Point(height * 1.5, 0));
        panel.addChild(depths);

        return panel;
    };

    drawHolesLine(startX, endX, offsetY, holesNumber = 60) {
        const line = new paper.Group();
        const radius = (endX - startX) / (holesNumber * 6);
        const gap = radius * 6;
        for (let i = 0, g = 0, circle = null; i < holesNumber; i++, g += gap) {
            circle = new paper.Path.Circle(new Point(startX + g, offsetY), radius);
            circle.fillColor = new Color('black');
            line.addChild(circle);
        }
        return line;
    }

    drawFrontDepth() {
        const area = new paper.Group();

        // spread UI properties
        const { yStep, depthOffset } = this.ui;

        // draw general depth effect
        const generalDepth = new paper.Path({
            closed: true,
            fillColor: new Color('#7c7a7a'),
            style: {
                strokeColor: new Color('black'),
            },
        });

        const currentHeight = this.panelStep * this.panels.length + 64;
        generalDepth.moveTo(new Point(0, yStep * 3));
        generalDepth.lineTo(new Point(depthOffset, yStep * 1.5));
        generalDepth.lineTo(new Point(depthOffset, currentHeight));
        generalDepth.lineTo(new Point(0, currentHeight - yStep));
        area.addChild(generalDepth);

        // draw top section depths effect
        const topDepth = new paper.Path({
            closed: true,
            fillColor: new Color('#5195E9'),
            style: {
                strokeColor: new Color('black'),
            },
        });
        const lerpOffset = 0.7;
        const lerpX = depthOffset * lerpOffset;
        const lerpY = yStep * 3 * (1 - lerpOffset) + yStep * 1.5 * lerpOffset;

        topDepth.moveTo(new Point(depthOffset, yStep));
        topDepth.lineTo(new Point(depthOffset, yStep * 1.5));
        topDepth.lineTo(new Point(lerpX, lerpY));
        topDepth.lineTo(new Point(lerpX, lerpY - yStep * 0.5));
        area.addChild(topDepth);

        return area;
    }

    drawRearDepth() {
        // spread UI properties
        const { depthOffset, yStep } = this.ui;
        const area = new paper.Group();

        // draw general depth effect
        const generalDepth = new paper.Path({
            closed: true,
            fillColor: new Color('grey'),
            style: {
                strokeColor: new Color('black'),
            },
        });

        const currentHeight = this.panelStep * this.panels.length + 64;
        generalDepth.moveTo(new Point(0, yStep * 3));
        generalDepth.lineTo(new Point(depthOffset, yStep * 1.5));
        generalDepth.lineTo(new Point(depthOffset, currentHeight));
        generalDepth.lineTo(new Point(0, currentHeight - yStep));
        area.addChild(generalDepth);

        return area;
    }

    drawRearView() {
        // spread UI properties
        const { depthOffset, width, yStep } = this.ui;
        const area = new paper.Group();

        // draw general area
        const generalArea = new paper.Path({
            closed: true,
            fillColor: new Color('grey'),
            style: {
                strokeColor: new Color('black'),
            },
        });

        const currentHeight = this.panelStep * this.panels.length + 64;
        generalArea.moveTo(new Point(depthOffset, yStep * 1.5));
        generalArea.lineTo(new Point(width, yStep * 1.5));
        generalArea.lineTo(new Point(width, currentHeight));
        generalArea.lineTo(new Point(depthOffset, currentHeight));
        area.addChild(generalArea);

        return area;
    }

    /* */

    draw() {
        this.frontLayer.visible = this.isFrontSide;
        this.rearLayer.visible = !this.isFrontSide;

        [...this.frontItemsLayer.children, ...this.rearItemsLayer.children].forEach((child) => {
            if (child.userData) {
                if (child.userData.isSelected) {
                    child.selectItem();
                } else {
                    child.deselectItem();
                }
            }
        });

        this.resize();
    }

    reset() {
        this.isFrontSide = true;

        this.selectedSlot = 1;

        this.draw();
    }
}
