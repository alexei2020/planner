import paper, { Point, Path, Color, Rectangle, Size, GradientStop } from 'paper';

import Device from './base/Device';

export default class Switch extends Device {
    public ui: any = {
        dimensions: {
            width: 1056,
            height: 123,
        },
        scale: 1,
    };

    public frontLayerWidth = 1056;
    public frontLayerHeight = 162;

    public rearLayerWidth = 1056;

    setupFrontLayer(items = []) {
        this.setupFrontItemsLayer(items);

        //

        // const rect = new Path.Rectangle(this.frontItemsLayer.bounds);
        // rect.strokeColor = new Color('red');
        // this.frontItemsLayer.addChild(rect);

        const width = this.project.view.size.width - 60;
        const widthScale = width / this.frontItemsLayer.bounds.width;

        const height = this.project.view.size.height - 26;
        const heightScale = height / this.frontItemsLayer.bounds.height;

        const scale = Math.min(widthScale, heightScale);

        this.frontItemsLayer.position = this.project.view.center;
        this.frontItemsLayer.scale(scale);

        //

        this.setupPanel(this.frontLayer);

        this.frontLayer.position = this.project.view.center;

        this.frontItemsLayer.bringToFront();
    }

    setupRearLayer(items = []) {
        this.setupRearItemsLayer(items);

        const width = this.frontLayer.bounds.width - 60;
        const widthScale = width / this.rearItemsLayer.bounds.width;

        const height = this.frontLayer.bounds.height - 26;
        const heightScale = height / this.rearItemsLayer.bounds.height;

        const scale = Math.min(widthScale, heightScale);

        this.rearItemsLayer.position = this.project.view.center;
        this.rearItemsLayer.scale(scale);

        //

        this.rearItemsLayer.bringToFront();
    }

    setupPanel(layer: paper.Layer) {
        console.log(this.frontItemsLayer.bounds);

        const layerWidth = this.frontItemsLayer.bounds.width > 60
            ? this.frontItemsLayer.bounds.width + 60
            : 1118;
        const layerHeight = this.frontItemsLayer.bounds.height > 26
            ? this.frontItemsLayer.bounds.height + 26
            : 188;

        console.log(layerWidth, layerHeight);

        const rectangle = new Rectangle(0, 0, layerWidth, layerHeight);
        const cornerSize = new Size(5, 5);

        const bodyArea = new Path.Rectangle(rectangle, cornerSize);
        bodyArea.fillColor = new Color('#20253959');
        //bodyArea.fillColor = new Color('#039be566');
        layer.addChild(bodyArea);

        [
            [10, 10],
            [10, layerHeight - 26],
            [layerWidth - 26, 10],
            [layerWidth - 26, layerHeight - 26]
        ].forEach((point) => {
            const holeArea = new Path.Ellipse(new Rectangle(new Point(point[0], point[1]), new Size(13, 13)));
            holeArea.fillColor = new Color('#D3E8FD');
            holeArea.strokeColor = {
                // @ts-ignore
                gradient: {
                    stops: [new GradientStop(new Color('#588BA5')), new GradientStop(new Color('#BDE9FF'))],
                    radial: false,
                },
                origin: holeArea.bounds.topCenter,
                destination: holeArea.bounds.bottomCenter,
            };
            layer.addChild(holeArea);
        });
    }

    /* */

    setupFrontItemsLayer(items) {
        let marginX = 0;
        let marginY = 0;
        let offsetX = 0;
        let offsetY = 0;
        let lastX = 0;
        let lastY = 0;
        let lastO = '';
        let lastW = 0;
        let lastH = 0;
        let lastI = 0;
        let maxY = 0;

        items.forEach((item) => {
            let x = 0;
            let y = 0;
            let intervalY = 8;
            let intervalX = 16;

            let width = 30;
            let height = 30;
            if (item.type === 'copperInterface') {
                width = 54;
                height = 59;
            } else if (item.type === 'powersupply') {
                width = 95;
                height = 63;
            } else if (item.type === 'fan') {
                width = 63;
                height = 63;
            }

            let orientation = 'up';
            if (item.orientation === '2rows,colfirst' || item.orientation === '2rows,rowfirst') {
                if (lastO === '2rows,colfirst' || lastO === '2rows,rowfirst') {
                    if (lastY === offsetY) {
                        x = offsetX;
                        y = offsetY + height + intervalY;
                        orientation = 'down';
                    } else {
                        x = offsetX + lastW + intervalX;
                        y = offsetY;
                    }
                } else {
                    x = offsetX + lastW + intervalX;
                    y = offsetY;
                }
            } else if (item.orientation === 'halfrow,top') {
                x = offsetX + lastW + intervalX;
                y = offsetY;
            } else if (item.orientation === 'onerow,centered') {
                x = offsetX + lastW + intervalX;
                y = offsetY;
            }

            lastX = x;
            lastY = y;
            lastO = item.orientation;
            lastW = width;
            lastH = height;
            lastI = intervalX;
            offsetX = x;
            maxY = maxY >= y ? maxY : y;

            if (item.type === 'copperInterface') {
                const intf = this.createInterface(item.backgroundImgUrl, item.name, item.itemNum, orientation, width, height);
                intf['userData'] = item;
                intf.position.x += marginX + x;
                intf.position.y += marginY + y;
                intf.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.frontItemsLayer.addChild(intf);
            } else if (item.type === 'powersupply') {
                const powerSupply = this.createPowerSupply(item.backgroundImgUrl, item.name);
                powerSupply['userData'] = item;
                powerSupply.position.x += marginX + x;
                powerSupply.position.y += marginY + y;
                powerSupply.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.frontItemsLayer.addChild(powerSupply);
            } else if (item.type === 'fan') {
                const fan = this.createFan(item.backgroundImgUrl, item.name);
                fan['userData'] = item;
                fan.position.x += marginX + x;
                fan.position.y += marginY + y;
                fan.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.frontItemsLayer.addChild(fan);
            }
        });

        this.frontLayerWidth = offsetX + lastW + lastI;
        this.frontLayerHeight = maxY;
    }

    setupRearItemsLayer(items) {
        let marginX = 0;
        let marginY = 0;
        let offsetX = 0;
        let centerY = 162 / 2;
        let lastX = 0;
        let lastY = 0;
        let lastO = '';
        let lastType = '';
        let lastW = 0;
        let lastI = 0;

        items.forEach((item) => {
            let x = 0;
            let y = 0;
            let intervalXInternal = 20;
            let intervalXExternal = 20;

            let intervalY = 8;

            let width = 30;
            let height = 30;
            if (item.type === 'copperInterface') {
                width = 54 * 0.7;
                height = 59 * 0.7;
                intervalXExternal = 20;
            } else if (item.type === 'powersupply') {
                width = 151;
                height = 88;
                intervalXExternal = 169;
            } else if (item.type === 'fan') {
                width = 88;
                height = 88;
                intervalXExternal = 53;
            }

            let orientation = 'up';
            if (item.orientation === '2rows,colfirst' || item.orientation === '2rows,rowfirst') {
                if (lastO === '2rows,colfirst' || lastO === '2rows,rowfirst') {
                    if (lastY === 0) {
                        x = offsetX;
                        y = height + intervalY;
                        orientation = 'down';
                    } else {
                        x = offsetX + lastW + (item.type === lastType ? intervalXInternal : intervalXExternal);
                        y = 0;
                    }
                } else {
                    x = offsetX + lastW + (item.type === lastType ? intervalXInternal : intervalXExternal);
                    y = 0;
                }
            } else if (item.orientation === 'halfrow,top') {
                x = offsetX + lastW + (item.type === lastType ? intervalXInternal : intervalXExternal);
                y = 0;
            } else if (item.orientation === 'onerow,centered') {
                x = offsetX + lastW + (item.type === lastType ? intervalXInternal : intervalXExternal);
                y = centerY - marginY - height / 2;
            }

            lastX = x;
            lastY = y;
            lastO = item.orientation;
            lastW = width;
            lastI = (item.type === lastType ? intervalXInternal : intervalXExternal);
            lastType = item.type;
            offsetX = x;

            if (item.type === 'copperInterface') {
                const intf = this.createInterface(item.backgroundImgUrl, item.name, item.itemNum, orientation, width, height);
                intf['userData'] = item;
                intf.position.x += marginX + x;
                intf.position.y += marginY + y;
                intf.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.rearItemsLayer.addChild(intf);
            } else if (item.type === 'powersupply') {
                const powerSupply = this.createPowerSupply(item.backgroundImgUrl, item.name);
                powerSupply['userData'] = item;
                powerSupply.position.x += marginX + x;
                powerSupply.position.y += marginY;
                powerSupply.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.rearItemsLayer.addChild(powerSupply);
            } else if (item.type === 'fan') {
                const fan = this.createFan(item.backgroundImgUrl, item.name);
                fan['userData'] = item;
                fan.position.x += marginX + x;
                fan.position.y += marginY;
                fan.onClick = ({ event }) => this.itemClick.next([event, item]);
                this.rearItemsLayer.addChild(fan);
            }
        });

        this.rearLayerWidth = Math.max(300, offsetX + lastW + lastI + (marginX * 2));
    }

    /* */

    draw() {
        this.frontLayer.visible = true;
        this.rearLayer.visible = false;

        this.frontItemsLayer.visible = this.isFrontSide;
        this.rearItemsLayer.visible = !this.isFrontSide;

        [...this.frontItemsLayer.children, ...this.rearItemsLayer.children].forEach((child) => {
            if (child.userData) {
                if (child.userData.isSelected) {
                    child.selectItem();
                } else {
                    child.deselectItem();
                }
            }
        });

        this.resize();
    }

    reset() {
        this.isFrontSide = true;

        this.draw();
    }
}
