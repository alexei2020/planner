import { ReplaySubject, Subject } from 'rxjs';
import paper, { Group, Raster, PointText, Color, Point, Size, Path } from 'paper';

paper.settings.insertItems = false;

export interface DeviceEvent {
    type: string,
    payload?: any,
}

export default abstract class Device extends ReplaySubject<DeviceEvent> {
    public isFrontSide = true;

    public selectedSlot = 1;

    public ui: any = {
        dimensions: {
            width: 1024,
            height: 128,
        },
        scale: 1,
    };

    protected project: paper.Project;
    protected backgroundLayer: paper.Layer;
    protected frontLayer: paper.Layer;
    protected rearLayer: paper.Layer;

    protected frontItemsLayer;
    protected rearItemsLayer;

    public itemClick = new Subject();
    public itemEnter = new Subject();
    public itemLeave = new Subject();
    public itemOver = new Subject();

    public get backgroundLayerBoundingBox() {
        return this.backgroundLayer.bounds;
    }

    public constructor(protected canvas: HTMLCanvasElement) {
        super();

        this.next({
            type: 'init',
            payload: {
                slot: this.selectedSlot,
            },
        });

        this.setup();
    }

    public setup() {
        if (this.project) {
            this.project.clear();
            this.project.remove();
            this.project = null;
        }

        this.project = new paper.Project(this.canvas);
        this.project.view.on('resize', () => this.onResize());

        /* */

        this.backgroundLayer = new paper.Layer();
        this.backgroundLayer.name = 'BackgroundLayer';
        this.project.addLayer(this.backgroundLayer);

        /* */

        this.frontLayer = new paper.Layer();
        this.frontLayer.name = 'FrontLayer';
        this.backgroundLayer.addChild(this.frontLayer);

        this.frontItemsLayer = new paper.Layer();
        this.frontItemsLayer.name = 'FrontItemsLayer';
        this.backgroundLayer.addChild(this.frontItemsLayer);

        /* */

        this.rearLayer = new paper.Layer();
        this.rearLayer.name = 'RearLayer';
        this.backgroundLayer.addChild(this.rearLayer);

        this.rearItemsLayer = new paper.Layer();
        this.rearItemsLayer.name = 'RearItemsLayer';
        this.backgroundLayer.addChild(this.rearItemsLayer);
    }

    public resize() {
        // @ts-ignore
        this.project.view.setViewSize(this.project.view.getViewSize().add(1e-11));
    }

    /* */

    public abstract draw(force?: boolean);

    public abstract reset();

    /* */

    protected onResize() {
        const width = this.project.view.size.width;
        const widthScale = width / this.backgroundLayerBoundingBox.width;

        const height = this.project.view.size.height;
        const heightScale = height / this.backgroundLayerBoundingBox.height;

        const scale = Math.min(widthScale, heightScale) * (this.ui.scale || 1);

        this.backgroundLayer.position = this.project.view.center;
        this.backgroundLayer.scale(scale);
    }

    /* */

    protected createInterface(src, name, label, orientation = 'up', width = 30, height = 30) {
        const group = new Group();
        group.onMouseEnter = () => {
            this.itemEnter.next(name);

            this.project.view.element.style.setProperty('cursor', 'pointer');
        };
        group.onMouseMove = (event) => {
            this.itemOver.next(event.event);
        };
        group.onMouseLeave = () => {
            this.itemLeave.next();

            this.project.view.element.style.setProperty('cursor', null);
        };
        group['userData'] = {};
        group['selectItem'] = () => {
            raster.visible = false;
            rasterSelected.visible = true;
        };
        group['deselectItem'] = () => {
            raster.visible = true;
            rasterSelected.visible = false;
        };

        const raster = new Raster({
            image: Device.imagesCache[src],
            smoothing: 'mid',
        });
        raster.name = 'Raster';
        raster.translate(new Point(width / 2, height / 2));
        raster.rotate(orientation === 'down' ? 180 : 0, raster.bounds.center);
        raster.scale(width / raster.width, height / raster.height);
        group.addChild(raster);

        const rasterSelected = new Raster({
            image: Device.imagesCache[src.replace('.svg', '-selected.svg')],
            smoothing: 'mid',
        });
        rasterSelected.name = 'RasterSelected';
        rasterSelected.visible = false;
        rasterSelected.translate(new Point(width / 2, height / 2));
        rasterSelected.rotate(orientation === 'down' ? 180 : 0, rasterSelected.bounds.center);
        rasterSelected.scale(width / rasterSelected.width, height / rasterSelected.height);
        group.addChild(rasterSelected);

        const rect = new Path.Rectangle(new Point(0, 0), new Size(width, height));
        // rect.strokeColor = new Color('red');
        group.addChild(rect);

        const text = new PointText(new Point(0, 0));
        text.name = 'Text';
        text.fillColor = new Color('#4A5F7A');
        text.fontSize = 10;
        text.justification = 'center';
        text.content = label;
        text.position.x = width / 2;
        text.position.y = height / 2 + (orientation === 'up' ? 5 : -5);
        group.addChild(text);

        return group;
    }

    protected createPowerSupply(src, name, width = 151, height = 88) {
        const group = new Group();
        group.onMouseEnter = () => {
            this.itemEnter.next(name);

            this.project.view.element.style.setProperty('cursor', 'pointer');
        };
        group.onMouseMove = (event) => {
            this.itemOver.next(event.event);
        };
        group.onMouseLeave = () => {
            this.itemLeave.next();

            this.project.view.element.style.setProperty('cursor', null);
        };
        group['userData'] = {};
        group['selectItem'] = () => {
            raster.visible = false;
            rasterSelected.visible = true;
        };
        group['deselectItem'] = () => {
            raster.visible = true;
            rasterSelected.visible = false;
        };

        const rect = new Path.Rectangle(new Point(0, 0), new Size(width, height));
        // rect.strokeColor = new Color('red');
        group.addChild(rect);

        // const raster: any = {};
        const raster = new Raster({
            image: Device.imagesCache[src],
            smoothing: 'mid',
        });
        raster.name = 'Raster';
        raster.translate(new Point(width / 2, height / 2));
        raster.scale(width / raster.width, height / raster.height);
        group.addChild(raster);

        // const rasterSelected: any = { loaded: true };
        const rasterSelected = new Raster({
            image: Device.imagesCache[src.replace('.svg', '-selected.svg')],
            smoothing: 'mid',
        });
        rasterSelected.name = 'RasterSelected';
        rasterSelected.visible = false;
        rasterSelected.translate(new Point(width / 2, height / 2));
        rasterSelected.scale(width / rasterSelected.width, height / rasterSelected.height);
        group.addChild(rasterSelected);

        return group;
    }

    protected createFan(src, name, width = 88, height = 88) {
        const group = new Group();
        group.onMouseEnter = () => {
            this.itemEnter.next(name);

            this.project.view.element.style.setProperty('cursor', 'pointer');
        };
        group.onMouseMove = (event) => {
            this.itemOver.next(event.event);
        };
        group.onMouseLeave = () => {
            this.itemLeave.next();

            this.project.view.element.style.setProperty('cursor', null);
        };
        group['userData'] = {};
        group['selectItem'] = () => {
            raster.visible = false;
            rasterSelected.visible = true;
        };
        group['deselectItem'] = () => {
            raster.visible = true;
            rasterSelected.visible = false;
        };

        const rect = new Path.Rectangle(new Point(0, 0), new Size(width, height));
        // rect.strokeColor = new Color('red');
        group.addChild(rect);

        // const raster: any = {};
        const raster = new Raster({
            image: Device.imagesCache[src],
            smoothing: 'high',
        });
        raster.name = 'Raster';
        raster.translate(new Point(width / 2, height / 2));
        raster.scale(width / raster.width, height / raster.height);
        group.addChild(raster);

        // const rasterSelected: any = { loaded: true };
        const rasterSelected = new Raster({
            image: Device.imagesCache[src.replace('.svg', '-selected.svg')],
            smoothing: 'high',
        });
        rasterSelected.name = 'RasterSelected';
        rasterSelected.visible = false;
        rasterSelected.translate(new Point(width / 2, height / 2));
        rasterSelected.scale(width / rasterSelected.width, height / rasterSelected.height);
        group.addChild(rasterSelected);

        return group;
    }

    /* */

    static imagesCache = {};

    static preload() {
        Device.imagesCache = [
            'assets/images/switch/copperInterface.svg',
            'assets/images/switch/copperInterface-selected.svg',
            'assets/images/switch/fan.svg',
            'assets/images/switch/fan-selected.svg',
            'assets/images/switch/fannotpresent.svg',
            'assets/images/switch/fannotpresent-selected.svg',
            'assets/images/switch/fanok.svg',
            'assets/images/switch/fanok-selected.svg',
            'assets/images/switch/infsDown.svg',
            'assets/images/switch/infsDown-selected.svg',
            'assets/images/switch/infsUp.svg',
            'assets/images/switch/infsUp-selected.svg',
            'assets/images/switch/powersupply.svg',
            'assets/images/switch/powersupply-selected.svg',
            'assets/images/switch/powersupplynotpresent.svg',
            'assets/images/switch/powersupplynotpresent-selected.svg',
            'assets/images/switch/powersupplyok.svg',
            'assets/images/switch/powersupplyok-selected.svg',
        ].reduce((imagesCache, src) => {
            const image = new Image(108, 118);
            image.src = src;
            return { ...imagesCache, [src]: image };
        }, {});
    }
}
