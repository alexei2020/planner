import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { DataApiService } from '../../services/data-api.service';
import { State, process } from '@progress/kendo-data-query';
import { faBell } from '@fortawesome/free-solid-svg-icons';
import { HttpClient } from '@angular/common/http';

import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import * as _ from 'lodash';
import { AddEvent, EditEvent, GridComponent } from '@progress/kendo-angular-grid';
import { DeviceDetailService } from 'app/resource/device.gen/services';
export interface Label {
    key: string;
    value: string;
}
const createFormGroup = dataItem => new FormGroup({
    admit: new FormControl(dataItem.admit),
    hostName: new FormControl(dataItem.hostName),
    ipv4Net: new FormControl(dataItem.ipv4Net),
});
const matches = (el, selector) => (el.matches || el.msMatchesSelector).call(el, selector);
@Component({
    selector: 'interface-detail',
    templateUrl: './interface-detail.component.html',
    styleUrls: ['./interface-detail.component.scss']
})
export class InterfaceDetailComponent implements OnInit, OnDestroy {
    // Private
    private _unsubscribeAll: Subject<any>;
    private _filteredDeviceData: any;
    private _deviceData: any;
    private _originalDeviceData: any;
    private _OriginalLabels: any;
    private _labels: any;
    private _location: string
    columns: any[];
    private _grid: GridComponent;
    private _editedRowIndex: number;
    private docClickSubscription: any;

    public formGroup: FormGroup;
    isEditMode = false;
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    @Input() set deviceData(data: any) {
        if (data !== undefined && !_.isEmpty(data)) {
            this._originalDeviceData = Object.assign({}, data);
            this._deviceData = data;
            console.log(data);
            this._filteredDeviceData = [{
                vendor: data.status.vendor,
                model: data.status.model,
                serialNo: data.status.serialNo,
                role: data.status.role,
                osVersion: data.status.osVersion,
                hostName: data.spec.hostName,
                ipv4Net: data.spec.ipv4Net,
                admit: data.spec.admit,
                createdAt: data.metadata.creationTimestamp
            }];
        }
        else {
            this._deviceData = [];
            this._filteredDeviceData = [];
            this._originalDeviceData = {};
        }
    }
    get deviceData(): any {
        return this._filteredDeviceData
    }

    @Input() deviceName: string;
    @Input() set labels(labels: any) {
        this._OriginalLabels = Object.assign({}, labels);
        this._labels = labels;
    }
    get labels(): any {
        return this._labels;
    }
    //chip variables
    visible = true;
    selectable = true;
    removable = true;
    addOnBlur = true;
    canSave = false;
    canCancel = false;
    readonly separatorKeysCodes: number[] = [ENTER, COMMA];
    /**
     * Constructor
     */
    iconBell = faBell;
    constructor(
        private formBuilder: FormBuilder,
        private _dataService: DeviceDetailService,
        private _route: ActivatedRoute,
        private renderer: Renderer2
    ) {

        this._unsubscribeAll = new Subject<any>();
        this.columns = [
            {
                name: 'Hostname',
                field: 'hostName'
            },
            {
                name: 'IP4',
                field: 'ipv4Net'
            },
            {
                name: 'Vendor',
                field: 'vendor'
            },
            {
                name: 'Model',
                field: 'model'
            },
            {
                name: 'Serial Number',
                field: 'serialNo'
            },
            {
                name: 'OS Version',
                field: 'osVersion'
            },
            {
                name: 'Role',
                field: 'role'
            },
            {
                name: 'Created At',
                field: 'createdAt'
            }
        ]

    }
    onSave() {
        this.saveCurrent();
        this.canCancel = false;
        this.canSave = false;
        this._originalDeviceData.metadata = {
            ...this._originalDeviceData.metadata,
            labels: this._labels
        };
        this._originalDeviceData.spec = {
            ...this._originalDeviceData.spec,
            hostName: this._filteredDeviceData[0].hostName,
            ipv4Net: this._filteredDeviceData[0].ipv4Net,
            admit: this._filteredDeviceData[0].admit
        }

        this._dataService.update(this._originalDeviceData, 'ws.io').pipe(takeUntil(this._unsubscribeAll)).subscribe(res => {
            this._originalDeviceData = res;
            this.deviceName = res.metadata.name;
            this.deviceData = res;
            this._OriginalLabels = Object.assign({}, res.metadata?.labels)
            this.labels = res.metadata?.labels;

        })
    }
    onCancel() {
        console.log(this._OriginalLabels, this._originalDeviceData);
        this.deviceData = this._originalDeviceData;
        this.labels = this._OriginalLabels;
        this.canSave = false;
        this.canCancel = false;
    }
    add(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;
        const newKeyValue = value.split(":");
        if (newKeyValue.length < 2)
            return;
        if ((value || '').trim()) {
            let obj = {};
            obj[`${newKeyValue[0]}`] = newKeyValue[1];
            this._labels = Object.assign(this._labels, obj);
            this.canSave = true;
            this.canCancel = true;
        }

        // Reset the input value
        if (input) {
            input.value = '';
        }
    }

    remove(label: Label): void {
        console.log(this._labels);
        delete this._labels[label.key];
        this.canSave = true;
        this.canCancel = true;
    }
    public cellClickHandler({ sender, rowIndex, columnIndex, dataItem, isEdited }) {
        console.log("---col index---", rowIndex, isEdited);
        console.log(sender);
        this._grid = sender as GridComponent;
        // if (isEdited || (this.formGroup && !this.formGroup.valid)) {
        //     return;
        // }
        // if (!isEdited) {
        // sender.editCell(rowIndex, columnIndex, this.createFormGroup(dataItem));
        // this._grid.closeRow();
        this._editedRowIndex = rowIndex;
        this.formGroup = createFormGroup(dataItem);
        this.isEditMode = true;
        sender.editRow(rowIndex, this.formGroup);
        // }

        // this._grid.editRow(rowIndex, this.createFormGroup(dataItem))
    }
    private saveCurrent(): void {
        if (this._grid && this._grid instanceof GridComponent && this.isEditMode) {
            if (this.formGroup.dirty) {
                console.log("form edited", this.formGroup);
                this.canCancel = true;
                this.canSave = true;
                Object.assign(this._filteredDeviceData[0], {
                    admit: this.formGroup.controls.admit.value,
                    hostName: this.formGroup.controls.hostName.value,
                    ipv4Net: this.formGroup.controls.ipv4Net.value
                })
            }
            this.isEditMode = false;
            this._grid.closeRow(0);
        }
    }
    private onDocumentClick(e: any): void {
        console.log(this.formGroup);
        if (this.formGroup && this.formGroup.valid && !matches(e.target, '#device-detail-grid tbody *')) {
            this.saveCurrent();
        }
    }
    public cancelHandler({ sender, rowIndex }) {
        this.isEditMode = false;
        sender.closeRow(rowIndex);
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit() {
        this.docClickSubscription = this.renderer.listen('document', 'click', this.onDocumentClick.bind(this));
        this._route.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(res => {
            this._location = res.loc;
        })
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this.docClickSubscription();

        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
