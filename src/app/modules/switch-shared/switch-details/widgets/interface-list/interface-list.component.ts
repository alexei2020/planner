import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
import { DataApiService } from '../../services/data-api.service';
import { State, process, GroupDescriptor } from '@progress/kendo-data-query';

import { HttpClient } from '@angular/common/http';
@Component({
    selector: 'interface-list',
    templateUrl: './interface-list.component.html',
    styleUrls: ['./interface-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: fuseAnimations,
})
export class InterfaceListComponent implements OnInit, OnDestroy {
    // Private
    @ViewChild('grid') private grid;
    @Input() initialized = false;
    @Input() name;
    private intfDetails_: any[] = [];

    get intfDetails(): any[] {
        return this.intfDetails_ || [];
    }

    @Input()
    set intfDetails(val: any[]) {
        if (val === undefined) {
            this.initialized = false;
            this.intfDetails_ = [];
            return;
        } else {
            this.initialized = true;
            this.intfDetails_ = val;
            return;
        }
    }
    columns: any[] = [];
    public groupable = true;
    public groups: GroupDescriptor[] = [];
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    @Input() data: any[];
    /**
     * Constructor
     */
    constructor() {

        this.columns = [
            {
                name: 'Port Name',
                field: 'fpName'
            },
            {
                name: 'MAC Address',
                field: 'macAddress'
            },
            {
                name: 'Connected',
                field: 'connected'
            },
            {
                name: 'Line Protocal',
                field: 'lineProtocol'
            },
            {
                name: 'Operational Status',
                field: 'operationalStatus'
            },
            {
                name: 'Media Type',
                field: 'mediaType'
            },
        ]

    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit() {

    }
    getData(): any[] {
        const item = [
            {

            }
        ]
        return item;
    }

    public onGroupable() {
        this.groupable = !this.groupable;
        this.groups = [];
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
    }
}
