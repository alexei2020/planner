
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'labels-grid-item',
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class LabelsGridItemComponent implements OnInit {
    @Input() labels: any = [];

    constructor() { }

    ngOnInit(): void { }

    ngOnDestroy(): void { }
}
