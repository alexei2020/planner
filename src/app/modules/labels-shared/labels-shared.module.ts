import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';

import { GridModule, PDFModule, ExcelModule, BodyModule, SharedModule } from '@progress/kendo-angular-grid';
import { PopupModule } from '@progress/kendo-angular-popup';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { ChartsModule } from '@progress/kendo-angular-charts';

import { GridsterModule } from 'angular-gridster2';

import { ComponentsModule } from '../../components/module';
import { WidgetModule } from '../../main/widget/widget.module';
import { LabelsGridItemComponent } from './labels/component';

const KendoModules = [
  BodyModule, 
  ChartsModule,
  DropDownsModule,
  ExcelModule, 
  GridModule, 
  InputsModule,
  LayoutModule,
  PDFModule, 
  PopupModule,
  SharedModule,
];

@NgModule({
  declarations: [
    LabelsGridItemComponent,
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    KendoModules,
    MatSelectModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatChipsModule,
    FlexLayoutModule,
    GridsterModule,
    WidgetModule,
    ComponentsModule,
  ],
  exports: [
    LabelsGridItemComponent,
  ]
})
export class LabelsSharedModule { }
