
// tslint:disable

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { GridModule, PDFModule, ExcelModule, BodyModule, SharedModule } from '@progress/kendo-angular-grid';
import { PopupModule } from '@progress/kendo-angular-popup';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { InputsModule, TextBoxModule } from '@progress/kendo-angular-inputs';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { LabelModule } from '@progress/kendo-angular-label';
import { GridsterModule } from 'angular-gridster2';
import { ComponentsModule } from '../../components/module';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { PlatformcomponentSharedListComponent } from './list/component';
import { PlatformcomponentPlatform_Component_DetailsComponent } from './platform_component_details/component';
import { PlatformcomponentTemperature_DetailsComponent } from './temperature_details/component';
import { PlatformcomponentMemory_DetailsComponent } from './memory_details/component';
import { PlatformcomponentPower_DetailsComponent } from './power_details/component';
import { WidgetModule } from '../../main/widget/widget.module';
import {MatChipsModule} from '@angular/material/chips';
import { FuseSharedModule } from '@fuse/shared.module';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { IconsModule } from '@progress/kendo-angular-icons';
import { TooltipModule } from '@progress/kendo-angular-tooltip';

const KendoModules = [
  BodyModule, 
  ChartsModule,
  DropDownsModule,
  ExcelModule, 
  GridModule, 
  InputsModule,
  TextBoxModule,
  LayoutModule,
  LabelModule,
  PDFModule, 
  PopupModule,
  SharedModule,
  ButtonsModule,
  IconsModule,
  TooltipModule
];

@NgModule({
  declarations: [
    PlatformcomponentSharedListComponent,
    PlatformcomponentPlatform_Component_DetailsComponent,
    PlatformcomponentTemperature_DetailsComponent,
    PlatformcomponentMemory_DetailsComponent,
    PlatformcomponentPower_DetailsComponent,
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    KendoModules,
    MatSelectModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    FlexLayoutModule,
    GridsterModule,
    WidgetModule,
    ComponentsModule,
    MatChipsModule,
    ShowHidePasswordModule,
    FuseSharedModule
  ],
  exports: [
    CommonModule,
    PlatformcomponentSharedListComponent,
    PlatformcomponentPlatform_Component_DetailsComponent,
    PlatformcomponentTemperature_DetailsComponent,
    PlatformcomponentMemory_DetailsComponent,
    PlatformcomponentPower_DetailsComponent,
  ]
})
export class  PlatformcomponentSharedModule {}

