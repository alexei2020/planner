

// tslint:disable


import { Component, OnDestroy, OnInit, Input, Output, ViewChild, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ReferenceLink } from '../../../typings/backendapi.gen';
import { first, takeUntil, map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { BreadcrumbService, EventType, Event, FormValidationInfo } from 'app/main/shared/breadcrumb-bar.service';
import { metaToUrl } from 'app/navigation/objrefnav';
import { AdmitDeviceDetailService } from '../../../resource/admitdevice.gen/services';
import * as datatypes from '../../../typings/backendapi.gen';

import { FuseLocationService } from 'app/services/location.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from'@angular/material/chips';
import * as _ from 'lodash';
import { zonedTimeToUtc, format, utcToZonedTime } from 'date-fns-tz';
import { ShowHideService } from "ngx-show-hide-password";
import jstz from 'jstz';

@Component({
    selector: 'admitdevice-admit_device_details',
    templateUrl: './component.html',
    styleUrls: ['./component.scss'],
})

export class AdmitdeviceAdmit_Device_DetailsComponent implements OnInit, OnDestroy {
    @ViewChild('form') public form: NgForm;
    @Input() public resourceName: string;
    @Input() public namespace: string;
    @Input() public admitdevice: datatypes.AdmitDevice;
    @Output() refreshGridster: any = new EventEmitter();
    public loaded: boolean;
	  private timeZoneID = '';





    private _location: string;

    didReset: boolean = false;
    isEdit = false;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly service: AdmitDeviceDetailService,
	      private readonly _breadcrumbService: BreadcrumbService,
	      public router: Router,
        private _locationService: FuseLocationService,
        private _showHideService: ShowHideService,
    ) {}

    ngOnInit(): void {
	this._locationService.isBrowserTime.pipe(takeUntil(this._unsubscribeAll)).subscribe(res=>{
    if(this._locationService.isBrowserTime.getValue()){
      const timezone = jstz.determine();
      this.timeZoneID = timezone.name();
    } else {
      this.timeZoneID = this._locationService.locationDataAsKeyValue().timezone
    }
  });
	this.loaded = true;

	this.initPolicyCondition();
        this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe((queryParams)=> {
	    if (!queryParams['loc'] || queryParams['loc'] === this._location) {
		return;
	    }
            this.resourceName = this.activatedRoute.snapshot.params.resourceName;
            this._location = this._locationService.locationDataAsKeyValue().currentLocation;

        });
    }

    ngOnChanges(change: SimpleChanges) {
        if (change.admitdevice) {
            this.form?.form.markAsPristine();
            
        }
    }

    onValueChanged(form: NgForm): void {
	this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: form.valid}));
        this.refreshGridster.emit();
    }



    save(form): void {
    }

    ngOnDestroy(): void {
	this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
    }





    initPolicyCondition(): void {
    }

    renderPolicyCondition(p: datatypes.PolicyCondition[]): string[] {
        var opSymbols = {
            "Equals": " = ",
            "NotEquals": " != ",
            "Lesser": " < ",
            "LesserOrEquals": " <= ",
            "Greater" : " > ",
            "GreaterOrEquals": " >= ",
            "Contains": " contains ",
            "DoesNotContain": " !contains ",
            "RegEx": " RegEx "
        };
        var orStr = [];
        if (p) {
            p.map( (condition, index) => {
                var andStr = [];
                var andConditions = "";
                if (condition.aovList) {
                    andStr = condition.aovList.map( aov => 
                        (aov.attr.replace("AttrName", "") + opSymbols[aov.op.replace("AttrOp", "")] + aov.val)
                    );
                    andConditions = andStr.join(" AND ");
                }
                if (index != 0) {
                    orStr = orStr.concat("OR");
                }
                orStr = orStr.concat(andConditions);
            });
        }
        return orStr;
    }
 

}
