
// tslint:disable


import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, ViewChild, Input, SimpleChanges, OnChanges, EventEmitter, OnInit, OnDestroy, Output, ElementRef, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { first, takeUntil, map } from 'rxjs/operators';
import { Subject, Subscription, interval, Observable } from 'rxjs';

import { process, GroupDescriptor } from '@progress/kendo-data-query';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';

import * as datatypes from '../../../typings/backendapi.gen';
import { AttributeSetDetailService } from '../../../resource/attributeset.gen/services';
import { environment } from '../../../../environments/environment';
import { v4 as uuid } from 'uuid';
import { kindToEndpoint, kindToUrlClass } from 'app/navigation/objrefnav.gen';
import { KendoConfirmDialog } from "app/main/shared/dialogs/kendo-confirm-dialog/kendo-confirm-dialog.component";
import { KendoNoticeDialog } from "app/main/shared/dialogs/kendo-notice-dialog/kendo-notice-dialog.component";
import { KendoObjectDialog } from "app/main/shared/dialogs/kendo-object-dialog/kendo-object-dialog.component";
import {
  DialogCloseResult,
  DialogService,
  DialogResult,
} from "@progress/kendo-angular-dialog";

import { FuseLocationService } from 'app/services/location.service';
import { BreadcrumbService, EventType, FormValidationInfo } from 'app/main/shared/breadcrumb-bar.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import * as _ from 'lodash';
import { webSocket } from 'rxjs/webSocket';
import { ElasticSearchParserService, ESResponse } from 'app/services/esparser.service';
import { SortDescriptor } from '@progress/kendo-data-query';
import { FuseSearchBarService } from '@fuse/services/search-bar.service';

import { zonedTimeToUtc, format, utcToZonedTime } from 'date-fns-tz';
import jstz from 'jstz';

const createFormGroup = dataItem => new FormGroup({
    name: new FormControl(dataItem.name),
    apiVersion: new FormControl(dataItem.apiVersion),
    AttributeSetspecApiVersion: new FormControl(dataItem.AttributeSetspecApiVersion),
    AttributeSetspecDescription: new FormControl(dataItem.AttributeSetspecDescription),
    AttributeSetspecKind: new FormControl(dataItem.AttributeSetspecKind),
    age: new FormControl(dataItem.age),
});
@Component({
    selector: 'attributeset-list',
    templateUrl: './component.html',
    styleUrls: ['./component.scss'],
})

export class AttributesetSharedListComponent implements OnInit, OnChanges, OnDestroy {
    @ViewChild('grid') private grid;
    @ViewChild('globalSearch', { static: false }) globalSearch: ElementRef;
    @Input() showMetrics = false;
    @Input() pageSize = 20;
    @Input() searchText;
    @Input() resizeble: boolean;
    @Input() resourceName;
    @Input() url = `/attributesets`;
    @Input() wsFilter;

    @Output() setRows: any = new EventEmitter();

    public timer: any;
    public groups: GroupDescriptor[] = [];
    public isLoading = true;
    public formGroup: FormGroup;
    public gridView: GridDataResult;
    public latestCopyrightYear: number;
    public mySelection: string[] = [];
    public clickedRowItem: any;
    public clickedRowIndex: number;
    public filterColumns = [];
    public skip = 0 as number;
    public shouldHideActionMenu = false as boolean;
    public location: string;
    public fixedLocation =undefined;
    public tenantName: string;
    public locationName: string;
    public groupable = false;
    public isEditing: boolean;

    private editedRowIndex: number;
    // unsubscribe all
    private _unsubscribeAll: Subject<any>;
    private ws;
    intervalSub: Subscription;
    previousSize: number;

    public dataSource: any[] = [];
    private bodybuilder = require('bodybuilder');
    private body;
    public sort: SortDescriptor[] = [];
    public objectList = [];
    public metricList = [];

    public colFieldToESField = {
        'name' : 'object.metadata.name.keyword',
        'AttributeSetspecApiVersion' : 'object.spec.apiVersion.keyword',
        'AttributeSetspecDescription' : 'object.spec.description.keyword',
        'AttributeSetspecKind' : 'object.spec.kind.keyword',
    };
    public wildcardSearchFields = [
        'object.spec.apiVersion',
        'object.spec.description',
        'object.spec.kind',
    ];
    // TBD: to include only non-string fields and string fields that are encrypted
    public termSearchFields = [
        'object.spec.apiVersion.keyword',
        'object.spec.description.keyword',
        'object.spec.kind.keyword',
    ];
    public aggregatorStats = [
        {
            field: 'object.spec.apiVersion.keyword',
            name: 'Api Version',
            keys: []
        },
        {
            field: 'object.spec.description.keyword',
            name: 'Description',
            keys: []
        },
        {
            field: 'object.spec.kind.keyword',
            name: 'Kind',
            keys: []
        },
    ];

    public summaryCards = [
    ];
        
    public searchFilterExpand = false;

    constructor(
        public router: Router,
        private readonly service: AttributeSetDetailService,
        private readonly _http: HttpClient,
        private _dialogService: DialogService,
        private activatedRoute: ActivatedRoute,
        private _breadcrumbService: BreadcrumbService,
        private _locationService: FuseLocationService,
        private _esParser: ElasticSearchParserService,
        private _fuseSearchBarService: FuseSearchBarService
    ) {
        this._fuseSearchBarService.aggregatorStats.next(this.aggregatorStats);
        this._unsubscribeAll = new Subject();
        this.latestCopyrightYear = new Date().getFullYear();
        this._unsubscribeAll = new Subject();
    this.isEditing = false;
        this.gridView = {
            data: [],
            total: 0
        };
    }

    ngOnInit(): void {
        this._fuseSearchBarService.filterChange.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: boolean)=>{
            if(res) {
                this.updateFilters();
            }
        });
      this._locationService.isBrowserTime.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: boolean) =>{
        this.loadAttributeSets();
      });

        this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(queryParams => {
        if ((!queryParams['loc'] || queryParams['loc'] === this.location) && !queryParams['esFilter']) {
        return;
        }
            if (queryParams['esFilter']) {
                this.mergeFilters(JSON.parse(queryParams['esFilter']));
            }
      this.location = this._locationService.locationDataAsKeyValue().currentLocation;
      this.tenantName = this._locationService.locationDataAsKeyValue().tenantName;
      this.locationName = this._locationService.locationDataAsKeyValue().locationName;
        if (this.fixedLocation) {
          this.location = this.fixedLocation;
        }
            var wsUrl = `${environment.baseUrl}ws/list/${this.location}/v1/attributesets`.replace('http', 'ws') + "?wsSelector=includeSubtree=true";
            if (this.ws) {
                this.ws.complete();
            }
            this.ws = webSocket(wsUrl);
            this.ws.pipe(takeUntil(this._unsubscribeAll)).subscribe((rsp) => {
                if (!rsp) return;
                this.loadAttributeSets();
            });
            this.buildQuery();
            this.loadAttributeSets();
        });

    this._breadcrumbService.triggeredEvent.pipe(takeUntil(this._unsubscribeAll)).subscribe((event) => {
        if (event.type === EventType.DEVICE_DETAIL_SAVE) {
        if (this.isEditing) {
            this.saveHandler({
            sender: this.grid,
            rowIndex: this.clickedRowIndex,
            formGroup: this.formGroup,
            dataItem: this.clickedRowItem,
                });
        }
        } else if (event.type === EventType.DEVICE_DETAIL_CANCEL) {
        this.cancelHandler({
            sender: this.grid,
            rowIndex: this.clickedRowIndex,
            dataItem: this.clickedRowItem,
        });
        this.loadAttributeSets();
        }
        });
    }

    setHeight() {
        setTimeout(() => {
            if (this.grid && this.grid.wrapper) {
                let size = 0;
                if (this.grid.wrapper) {
                    const header = this.grid.wrapper.nativeElement.querySelector('.k-grouping-header'); 
                    const grid = this.grid.wrapper.nativeElement.querySelector('.k-grid-aria-root');
                    const pager = this.grid.wrapper.nativeElement.querySelector('.k-pager-wrap');
                    if (header) {
                        size += header.offsetHeight;
                    }
                    if (grid) {
                        size += grid.offsetHeight;
                    }
                    if (pager) {
                        size += pager.offsetHeight;
                    }
                }

                if (size == 0) {
                    size = this.grid.wrapper.nativeElement.offsetHeight;
                }

                this.previousSize = size;

                this.setRows.emit(size);
            }
        });
    }

    objectRefToUrl(reflink: datatypes.ReferenceLink): string {
    const location = reflink.split("/")[2];
        return reflink.split("/").slice(-2).join("/") + "?" + location;
    }

    loadRefLink(reflink: string) {
      const url = "/" + reflink.split("/").slice(-2).join("/");
      const loc = reflink.split("/")[2];
      const locationName = this._locationService.convertAPIGroup2Name(loc);
      this.router.navigate([url], {
        relativeTo: this.activatedRoute,
        queryParams: {
          loc: locationName,
          tenant: this._locationService.locationDataAsKeyValue().tenantName
        },
        replaceUrl: true
    });
        return false;
    }

    ngOnChanges(changes: SimpleChanges): void {
        let update = false;
        let searchText = this.searchText;
        if ((changes.pageSize && changes.pageSize.currentValue) || (changes.resizeble && changes.resizeble.currentValue)) {
            update = true;
        }
        if (changes.searchText && this.dataSource) {
            searchText = changes.searchText.currentValue;
            update = true;
        }
        if (update) {
            this.setHeight();
            this.onFilter(searchText);
            this.buildQuery();
            this.loadAttributeSets();
        }
    }

    loadAttributeSetsFailed(error: Error): void {
        let errorMessage;
        if (error instanceof HttpErrorResponse && error.error) {
            errorMessage = error.error.message;
            if (!errorMessage) {
                errorMessage = error.error.error;
            }
        } else {
            errorMessage = error.message;
        }

        const dialogRef = this._dialogService.open({
            title: "Failed to load AttributeSets",
            content: KendoNoticeDialog,
            width: "500px",
            height: "250px",
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.message = errorMessage;
        dialogInstance.dialog = dialogRef;
        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
    }

    private updateStats(res: ESResponse) {
        // Update aggregator stats from the response
        this.aggregatorStats.forEach( (item, index) => {
            const keys = this._esParser.aggrStats(res, item.field);
            const newStats = keys.map((key) => {
                const exists = this.aggregatorStats[index].keys.find(el => el.keyName === key.key);
                return {
                    name: key.key + "(" + key.doc_count + ")",
                    keyName: key.key,
                    count: key.doc_count,
                    checked: exists? exists.checked : false
                };
            });
            this.updateAggregatorStats(index, newStats);
        });
        // force a refresh on components reading aggregatorStats
        this.aggregatorStats = this.aggregatorStats.slice();
        this._fuseSearchBarService.aggregatorStats.next(this.aggregatorStats);

        // Read SummaryStats
        this.summaryCards.forEach((card, index) => {
            // Update only if we have asked for it
            const count = this._esParser.aggrStats(res, card.name);
            if (typeof count === 'number') {
                this.summaryCards[index].count = count;
            }
        });
        // force refresh on components reading summaryCards
        this.summaryCards = this.summaryCards.slice();
    }

    async loadAttributeSets() {
       
        if (!this.location) return;

        let timeZoneID = '';
        if(this._locationService.isBrowserTime.getValue()){
            const timezone = jstz.determine();
            timeZoneID = timezone.name();
        } else {
            timeZoneID = this._locationService.locationDataAsKeyValue().timezone
        }
        this.isLoading = true;

        // Are we sorting on metrics? If so, metrics result drives the rest of the columns. In all other
        // cases, the primary object drives the rest
        let body = this.body;
        let metricRes = [];
        let filterList = [];
        if (this.isSortOnMetrics()) {
            // Currently, we support only one metric
            this.metricList[0].applyFilters(this.summaryCards, this.aggregatorStats, this.sort, this.skip, this.pageSize);
            metricRes = await this.metricList[0].load(this.location).toPromise();
            filterList = metricRes.map(el => el.filterField);

            // Add filter, if we have something from metrics
            if (filterList.length) {
                body = this.bodybuilder();
                body = body.query('bool', (f) => {
                    filterList.forEach((item) => {
                        f = f.orFilter('wildcard', 'object.metadata.name.keyword', "*"+item);
                    });
                    return f;
                });
                body = body.build();
            }
        }

        let res: any;
        try {
            res = await this.service.getAttributeSets(this.location, body).toPromise();
        } catch(err) {
            this.setHeight();
            this.isLoading = false;
            this.loadAttributeSetsFailed(err);
            return;
        }
        this.dataSource = [];
        const data = this._esParser.items(res);
        data?.map(item => {
            this.dataSource.push({
                _id: uuid(),
                apiVersion: item.apiVersion,
                kind: item.kind,
                metadata: item.metadata,
                name: item.metadata.name,
                filterField: item.metadata.name,
                AttributeSetspecApiVersion: item.spec.apiVersion,
                AttributeSetspecDescription: item.spec.description,
                AttributeSetspecKind: item.spec.kind,
            });
        });

        // Now that we have the primary data, we can start rendering the table and collecting stats
        this.onFilter(this.searchText || null)
        this.setHeight();
        this.isLoading = false;
        this.gridView = {
            data: this.dataSource,
            total: this._esParser.total(res),
        };

        // If filtered by metrics, then we need to collect stats for the existing filters separately
        if (filterList.length) {
            // Fetch stats for the current filters in 'this.body'
            this.service.getAttributeSets(this.location, this.body)
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe((res: ESResponse) => {
                this.updateStats(res);
            });
        } else {
            this.updateStats(res);
        }

        // Stitch columns from secondary objects
        filterList = this.dataSource.map(el => el.filterField);
        for (let i = 0; i < this.objectList.length; i++) {
            this.objectList[i].applyPrimaryFilter(filterList);
            const secRes = await this.objectList[i].load(this.location, timeZoneID).toPromise();
            this.dataSource = this.dataSource.map(item => {
                let join = secRes.find(el => el.filterField === item.filterField);
                return {...item, ...join};
            });
        }

        // And from metrics; if sorting is done on metrics, then we already have the result in
        // metricRes
        if (!this.isSortOnMetrics()) {
            this.metricList[0].applyPrimaryFilter(filterList);
            metricRes = await this.metricList[0].load(this.location).toPromise();
            this.dataSource = this.dataSource.map(item => {
                let join = metricRes.find(el => el.filterField === item.metadata.name);
                return {...item, ...join};
            });
        } else if (metricRes) {
            this.dataSource = metricRes.map(item => {
                let join = this.dataSource.find(el => el.metadata.name === item.filterField);
                return {...join, ...item};
            });
        }

        this.gridView = {
            data: this.dataSource,
            total: this._esParser.total(res),
        };
    }

    ngOnDestroy() {
        this._fuseSearchBarService.aggregatorStats.next([]);
        // Unsubscribe from all subscriptions
        this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
        this.ws?.complete();
    }

    public onFilter(inputValue: string): void {
        if (!this.dataSource) {
            return;
        }

        const result: GridDataResult = process(this.dataSource, {
            filter: inputValue ? {
                logic: 'or',
                filters: this.filterColumns.map((col) => ({
                    field: col.field,
                    operator: 'contains',
                    value: inputValue
                })),
            } : null,
            group: this.groups,
        });

        this.gridView = {
            data: result.data,
            total: result.total,
        };
    }

    private isSortOnMetrics(): boolean {
        return false;
    }

    private buildQuery(updateSummary = true) {
        const bbSort = this.sort.map((item) => {
            var sortObj = {};
            if (item.dir && this.colFieldToESField[item.field]) {
                sortObj[this.colFieldToESField[item.field]] = item.dir;
            }
            return sortObj;
        });
        this.body = this.bodybuilder();

        // Add static filters
        if (this.wsFilter) {
            this.wsFilter.split(",").forEach( (filter) => {
                const rule = filter.split("=");
                this.body = this.body.query('match', rule[0], rule[1]);
            });
        }

        // Summary filters
        this.summaryCards.filter((c) => c.selected).forEach((card) => {
            card.fields.forEach((field) => {
                this.body = this.body.query('bool', (f) => {
                    field.values.forEach((value) => {
                        if (field.not) {
                            f = f.notFilter('wildcard', field.name, value);
                        } else {
                            f = f.orFilter('wildcard', field.name, value);
                        }
                    });
                    return f;
                });
            });
        });

        // User specified filters
        this.aggregatorStats.forEach((field) => {
            if (field.keys.some((key) => key.checked)) {
                this.body = this.body.query('bool', (f) => {
                    field.keys.filter((key) => key.checked).forEach((key) => {
                        f = f.orFilter('term', field.field, key.keyName);
                    });
                    return f;
                });
            }
        });

        // Finally, the search text
        if (this.searchText) {
            this.body = this.body.query('bool', (f) => {
                // Encrypted fields support only exact match. So, we split
                // the input text into individual words and feed them
                this.searchText.split(" ").forEach((searchText) => { 
                    // OR filter across all fields that are not encrypted
                    // Use wildcard only with strings
                    this.wildcardSearchFields.forEach((field) => {
                        f = f.orFilter('wildcard', field, `${searchText}*`);
                    });

                    // OR filter across all fields that are encrypted, or non-strings
                    this.termSearchFields.forEach((field) => {
                        f = f.orFilter('term', field, searchText);
                    });
                });
                return f;
            });
        }

        // Collect aggregators.
        this.aggregatorStats.forEach((item) => {
            this.body = this.body.aggregation('terms', item.field);
        });

        // Summary aggregators
        if (updateSummary) {
            this.summaryCards.forEach((card) => {
                card.fields.forEach((field) => {
                    this.body = this.body.aggregation('filter', card.name, (f) => {
                        field.values.forEach((value) => {
                            if (field.not) {
                                f = f.notFilter('wildcard', field.name, value);
                            } else {
                                f = f.orFilter('wildcard', field.name, value);
                            }
                        });
                        return f;
                    });
                });
            });
        }

        this.body = this.body
            .size(this.pageSize)
            .from(this.skip)
            .sort(bbSort)
            .build();
    }

    public dataStateChange(event: DataStateChangeEvent): void {
        this.skip = event.skip;
        this.pageSize = event.take;
        this.sort = event.sort;

        this.buildQuery(false);
        this.loadAttributeSets();
    }

    public updateFilters(): void {
        this.skip = 0;
        // If any filter is selected, we need to deselect summarycards Total
        const selected = this.aggregatorStats.some((item) => (
            item.keys.some((key) => key.checked === true)));
        if (selected && this.summaryCards.length) {
            this.summaryCards[0].selected = false;
        }

        this.buildQuery(false);
        this.loadAttributeSets();
    }

    public mergeFilters(filter): void {
        filter.forEach((item) => {
            var aggrStat = this.aggregatorStats.find(stat => item.field === stat.field);
            if (!aggrStat) {
                this.aggregatorStats.push({
                    field: item.field,
                    name: item.field.replace("object.", "").replace(".keyword", ""),
                    keys: item.keys.map(key => ({
                        name: key.keyName,
                        keyName: key.keyName,
                        count: 0,
                        checked: key.checked,
                    })),
                });
                return;
            }
            // aggrStat exists
            aggrStat.keys = item.keys.map(key => ({
                name: key.keyName,
                keyName: key.keyName,
                count: 0,
                checked: key.checked,
            }));
        });
    }

    public updateSummaryFilters(): void {
        var updateSummary = false;
        this.skip = 0;
        // If total is selected, then we clear all other filters
        if (this.summaryCards.length && this.summaryCards[0].selected) {
            this.aggregatorStats.forEach( (item) => {
                item.keys.forEach((key) => key.checked = false);
            });
            updateSummary = true;
        }
        this.buildQuery(updateSummary);
        this.loadAttributeSets();
    }

    public onSearchFilterState(expand: boolean): void {
        this.searchFilterExpand = expand;
    }

    private updateAggregatorStats(index:number, newStats:any[]) {
        // conditions to update:
        // 1. If none of the keys are 'checked'
        // 2. If any of the other fields have keys checked
        // 3. Always retain checked key after update even if response does not have it
        if (!this.aggregatorStats[index].keys.some((key) => key.checked)) {
            this.aggregatorStats[index].keys = newStats;
            return;
        }

        // This field has a 'checked' key
        const otherStats = this.aggregatorStats.filter((el, i) => i !== index);
        if (otherStats.some((item) => item.keys.some((key) => key.checked))) {
            const checkedKeys = this.aggregatorStats[index].keys.filter((key) => key.checked);
            // Add back checked keys, if not present
            checkedKeys.forEach((key) => {
                if (newStats.findIndex((entry) => entry.keyName === key.keyName) === -1) {
                    key.count = 0;
                    newStats.unshift(key);
                }
            });
            this.aggregatorStats[index].keys = newStats;
            return;
        }
    }

    public onCloneHandler(): void {
    if (!this.mySelection || this.mySelection.length == 0) {
        const dialogRef = this._dialogService.open({
        title: 'Please select any row',
            content: KendoNoticeDialog,
                width: "420px",
                height: "200px",
            });
            const dialogInstance = dialogRef.content.instance;
            dialogInstance.message = "Please select the row to be cloned";
            dialogInstance.dialog = dialogRef;
            dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
            return;
    }

    if (this.mySelection && this.mySelection.length > 1) {
         const dialogRef = this._dialogService.open({
               title: "Please select only one row",
               content: KendoNoticeDialog,
               width: "420px",
               height: "200px",
             });
             const dialogInstance = dialogRef.content.instance;
             dialogInstance.message = "Action cannot be applied to multiple rows.";
             dialogInstance.dialog = dialogRef;
             dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
             return;
    }
        const dialogRef = this._dialogService.open({
            content: KendoObjectDialog,
            width: "400px",
            height: "300px",
            title: "Clone Object",
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.dialog = dialogRef;
        dialogInstance.location = this.fixedLocation;
        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
            if (response instanceof DialogCloseResult || response.text == "Cancel") {
            } else {
                const item = dialogInstance.data as any;
                const clickedItem = this.clickedRowItem;
                const location = clickedItem.apiVersion.split("/", -1).slice(0, -1).join("/");
                this.service.load(clickedItem.name, clickedItem.metadata.namespace, location)
                    .pipe(first())
                    .subscribe((response) => {
                        var obj = response;
                        obj.metadata.name = item.name;
                        obj.apiVersion = item.location + "/v1";
                        this.onAddNewClosed(obj);
                    },
                    () => {
                        console.log('some error');
                    });
            }
        });
    }

    public onAddHandler() {
        const dialogRef = this._dialogService.open({
            content: KendoObjectDialog,
            width: "400px",
            height: "300px",
            title: "Add New Object",
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.dialog = dialogRef;
        dialogInstance.location = this.fixedLocation;
        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
            if (response instanceof DialogCloseResult || response.text == "Cancel") {
            } else {
                const item = dialogInstance.data as any;
                const obj = {
                    apiVersion: item.location + "/v1",
                    kind: 'AttributeSet',
                    metadata: {
                        name: item.name,
                    },
                };
                this.onAddNewClosed(obj);
            }
        });
    }

    private onAddNewClosed(obj: any): void {
    const location = obj.apiVersion.split("/")[0];
    const id = uuid();

    sessionStorage.setItem(id, JSON.stringify(obj));

    this.router.navigate([kindToEndpoint.get(obj.kind), obj.metadata.name], {
            queryParams: {
                loc: this._locationService.convertAPIGroup2Name(location),
                tenant: this._locationService.locationDataAsKeyValue().tenantName,
                backLocation: this._locationService.convertAPIGroup2Name(this.location),
                objectId: id,
            },
            replaceUrl: true
        });
    }

    public editSelectedItem(): void {
    if (!this.mySelection || this.mySelection.length == 0) {
        const dialogRef = this._dialogService.open({
        title: 'Please select any row',
            content: KendoNoticeDialog,
                width: "420px",
                height: "200px",
            });
            const dialogInstance = dialogRef.content.instance;
            dialogInstance.message = "Action can be applied to selected row only.";
            dialogInstance.dialog = dialogRef;
            dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
            return;
    }

    if (this.mySelection && this.mySelection.length > 1) {
         const dialogRef = this._dialogService.open({
               title: "Please select only one row",
               content: KendoNoticeDialog,
               width: "420px",
               height: "200px",
             });
             const dialogInstance = dialogRef.content.instance;
             dialogInstance.message = "Action cannot be applied to multiple rows.";
             dialogInstance.dialog = dialogRef;
             dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
             return;
    }

    this.editHandler({
        sender: this.grid,
        rowIndex: this.clickedRowIndex,
        dataItem: this.clickedRowItem,
    });
    }

    public editHandler({ sender, rowIndex, dataItem }) {
        this.shouldHideActionMenu = true;
        this.closeEditor(sender);
        this.formGroup = createFormGroup({
            name: dataItem.name,
            apiVersion: dataItem.apiVersion.split("/", -1).slice(0, -1).join("/"),
            AttributeSetspecApiVersion: dataItem.AttributeSetspecApiVersion,
            AttributeSetspecDescription: dataItem.AttributeSetspecDescription,
            AttributeSetspecKind: dataItem.AttributeSetspecKind,
            age: dataItem.age,
        });
        this.editedRowIndex = rowIndex;

        sender.editRow(rowIndex, this.formGroup);
    this.isEditing = true;
    this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true}));
    }

    public editUsingApiHandler({ sender, rowIndex, dataItem }) {
        const selectedItem = this.clickedRowItem;
    }

    public cancelHandler({ sender, rowIndex, dataItem}) {
        this._breadcrumbService.isDeviceDetailEditMode.next(null);
        this.shouldHideActionMenu = false;
        //roll back to the original data if editing canceled
        
    
    
    

        this.closeEditor(sender, rowIndex);
        this.setHeight();
    this.isEditing = false;
    this.clickedRowIndex = undefined;
    this.clickedRowItem = undefined;
    this.mySelection = [];
    }

    public saveHandler({ sender, rowIndex, formGroup, dataItem }): void {
        this.shouldHideActionMenu = false;
        var product: datatypes.AttributeSet = {
            apiVersion: dataItem.apiVersion || this.location + '/v1',
            kind: dataItem.kind || 'AttributeSet',
            metadata: {
                name: formGroup.value.name
            }
        } as datatypes.AttributeSet;

        // Fill in the values now
        product = _.set(product, 
            'spec.apiVersion', 
            formGroup.controls.AttributeSetspecApiVersion.value);
        product = _.set(product, 
            'spec.description', 
            formGroup.controls.AttributeSetspecDescription.value);
        product = _.set(product, 
            'spec.kind', 
            formGroup.controls.AttributeSetspecKind.value);

        const location = dataItem.apiVersion.split("/", -1).slice(0, -1).join("/");
        this.service.update(product, location).pipe(takeUntil(this._unsubscribeAll)).subscribe(
            () => {
                sender.closeRow(rowIndex);
                this.loadAttributeSets();
                this._breadcrumbService.isDeviceDetailEditMode.next(null);
    
                // save all secondary objects; on a best effort basis
                this.objectList.forEach(secondaryObj => {
                    secondaryObj.save(this, formGroup, dataItem)
                        .pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
                        this.loadAttributeSets();
                    });
                });
            },
            error => {
                sender.closeRow(rowIndex);
                let msg = "Failed to save: " + error.status;
                if (error.status == 500) {
                    msg += "\n" + error.error.message;
                }
                alert(msg)
            },
        );

        this.isEditing = false;
        this.clickedRowIndex = undefined;
        this.clickedRowItem = undefined;
        this.mySelection = [];
    }

    public removeSelectedItem(): void {
    if (!this.mySelection || this.mySelection.length == 0) {
        const dialogRef = this._dialogService.open({
            title: "Please select any row",
            content: KendoNoticeDialog,
            width: "420px",
            height: "200px",
            });
            const dialogInstance = dialogRef.content.instance;
            dialogInstance.message = "Action can be applied to selected row only.";
            dialogInstance.dialog = dialogRef;
            dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
            return;
    }

    if (this.mySelection && this.mySelection.length > 1) {
        const dialogRef = this._dialogService.open({
            title: "Please select only one row",
            content: KendoNoticeDialog,
            width: "420px",
            height: "200px",
            });
            const dialogInstance = dialogRef.content.instance;
            dialogInstance.message = "Action cannot be applied to multiple rows.";
            dialogInstance.dialog = dialogRef;
            dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
            return;
    }

    this.removeHandler({ dataItem: this.clickedRowItem });
    }

    public removeHandler({ dataItem }): void {
        const id = dataItem.metadata.name;
        const dialogRef = this._dialogService.open({
            title: "Remove AttributeSet record",
            content: KendoConfirmDialog,
            width: "420px",
            height: "200px",
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.message =
            "Are you sure you want to remove this attributeset?";
        dialogInstance.dialog = dialogRef;
        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
            if (response instanceof DialogCloseResult || response.text == "No") {
            } else {
                const location = dataItem.apiVersion.split("/", -1).slice(0, -1).join("/");
                this.service.delete(id, location).subscribe(() => {
                    this.loadAttributeSets();
                },
                error => alert('Failed to delete attributeset ' + id)
                );
                // remove all secondary objects; on a best effort basis
                this.objectList.forEach(secondaryObj => {
                    secondaryObj.delete(dataItem).pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
                        this.loadAttributeSets();
                    });
                });
            }
        });
        this.shouldHideActionMenu = false;
    }

    public onCellClick({ rowIndex, dataItem }) {
        this.clickedRowItem = dataItem;
    this.clickedRowIndex = rowIndex;
    }

    public onDblClick() {
        if (this.clickedRowItem != undefined) {
          const location = this.clickedRowItem.apiVersion.split("/", -1).slice(0, -1).join("/");
          const locationName = this.apiVersionToLocation(location);
//         let locationName = _.find(this._locationService.locationList.getValue(), ['value', location]) || {};
        const currentLocName = this._locationService.locationDataAsKeyValue().locationName;
        console.log("---locationame", locationName);
          const namespace = this.clickedRowItem.metadata.namespace;
            if (this.url) {
                this.router.navigate([this.url, this.clickedRowItem.name], {
                  relativeTo: this.activatedRoute,
                  queryParams: {
                    namespace: namespace,
                    tenant: this._locationService.locationDataAsKeyValue().tenantName,
                    loc: locationName,
                    backLocation: currentLocName
                  },
                });
            } else {
                this.router.navigate([this.clickedRowItem.name], {
                  relativeTo: this.activatedRoute,
                  queryParams: {
                    namespace: namespace,
                    tenant: this._locationService.locationDataAsKeyValue().tenantName,
                    loc: locationName,
                    backLocation: currentLocName
                  },
                });
            }
        }
    }

    public onExportPDFHandler() {
        this.grid.saveAsPDF();
    }

    public onExportExcelHandler() {
        this.grid.saveAsExcel();
    }

    public onGroupable() {
        this.groupable = !this.groupable;
        this.groups = [];
        this.loadAttributeSets();
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }

    public apiVersionToLocation(apiVersion: string): string {
        const apiGroup = apiVersion.split("/")[0];
        return this._locationService.convertAPIGroup2Name(apiGroup);
    }

    @HostListener('window:resize', ['$event'])
    onResize() {
        this.setHeight();
    }




    public dataValueAccessor(value: string, dataSource: any[]): string {
        const found = dataSource?.find( el => el.value === value );
        return found ? found.name : "";
    }

    iterableObj(obj: any): boolean {
        return Array.isArray(obj);
    }

    renderObject(obj: any, index?: number): string[] {
        if (!obj) return [];
        if (typeof(obj) === 'object') {
            var str: string[] = [];
            for (const prop in obj) {
                str.push(`${prop}: ${obj[prop]}`);
            }
            return str;
        }
        return [obj];
    }

    renderPolicyCondition(p: datatypes.PolicyCondition[]): string[] {
        var opSymbols = {
            "Equals": " = ",
            "NotEquals": " != ",
            "Lesser": " < ",
            "LesserOrEquals": " <= ",
            "Greater" : " > ",
            "GreaterOrEquals": " >= ",
            "Contains": " contains ",
            "DoesNotContain": " !contains ",
            "RegEx": " RegEx ",
            "In": " In ",
            "NotIn": " Not In "
        };
        if (p) {
            var andStr = [];
            p.map( (condition, index) => {
                var orStr = [];
                var orConditions = "";
                if (condition.aovList) {
                    orStr = condition.aovList.map( aov => 
                        (aov.attr.replace("AttrName", "") + opSymbols[aov.op.replace("AttrOp", "")] + aov.val)
                    );
                    orConditions = orStr.join(" AND ");
                }
                if (index != 0) {
                    andStr = andStr.concat("OR");
                }
                andStr = andStr.concat(orConditions);
            });
            return andStr;
        }

        return []
    }
}

// Secondary object specific class

// Class for metrics
class Metrics {
        
    private dataSource = [];
    private query = "";
    private from;
    private size;
    private sort: SortDescriptor[] = [];

    constructor(private readonly _http: HttpClient,
                private metrics: string[],
                private colFieldNoPrefix: string) {
    }

    // object.spec.vendor.keyword ===> spec.vendor
    private fieldNameToLabel(field): string {
        return field.replace(/^object\./g, "").replace(/\.keyword$/g, ""); 
    }

    // The list of objects to fetch; used in all cases where sorting is not on metrics column
    public applyPrimaryFilter(filterList: string[]) {
        this.from = 0;
        this.size = 0;
        this.sort = [];
        this.query = `objname=~"`;
        filterList.forEach((name, index) => {
            if (index !== 0) {
                this.query += "|";
            }
            this.query += name;
        });
        this.query += `"`;
    }

    // For a metric, we apply the same filters as what is currently applied for primary object
    public applyFilters(summaryCards, aggregatorStats, sort, from, size) {
        let firstField = true;
        this.query = "";
        summaryCards.filter((c, index) => c.selected && index !== 0).forEach((card) => {
            card.fields.forEach((field) => {
                if (!firstField) {
                    this.query += ",";
                }
                this.query += `${this.fieldNameToLabel(field.name)}=~"`;
                field.values.forEach((value, valueIndex) => {
                    if (valueIndex !== 0) {
                        this.query += "|";
                    }
                    this.query += value;
                    firstField = false;
                });
                this.query += `"`;
            });
        });

        aggregatorStats.forEach((field) => {
            if (field.keys.some((key) => key.checked)) {
                if (!firstField) {
                    this.query += ",";
                }
                this.query += `${this.fieldNameToLabel(field.field)}=~"`;
                field.keys.filter((key) => key.checked).forEach((key, keyIndex) => {
                    if (keyIndex !== 0) {
                        this.query += "|";
                    }
                    this.query += key.keyName;
                    firstField = false;
                });
                this.query += `"`;
            }
        });
        
        this.sort = sort;
        this.from = from;
        this.size = size;
    }

    public load(location): Observable<any[]> {
       
        let url = `${environment.promeUrl}metrics/${location}/v1/AttributeSets?includeSubtree=true`
        let sortStr = "";
        let sortStrEnd = "";
        const sortEl = this.sort.find(item => item.dir && item.field === this.colFieldNoPrefix);
        if (sortEl) {
            if (sortEl.dir === 'asc') {
                sortStr = "sort(";
            } else {
                sortStr = "sort_desc(";
            }
            sortStrEnd = ")";
        }
        url += `&query=${sortStr}round(`;
        this.metrics.forEach((metric, index) => {
            if (index !== 0) {
                url += "%2B";
            }
            url += `(increase(${metric}{${this.query}}[5m]))`
        });
        url += `)${sortStrEnd}`;

        if (this.from) {
            url += `&from=${this.from}`
        }
        if (this.size) {
            url += `&size=${this.size}`
        }
        return this._http.get(url).map((res: any) => {
            let metricRes = [];
            return metricRes;
        });
    }
}

