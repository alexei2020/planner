

// tslint:disable


import { Component, OnDestroy, OnInit, Input, Output, ViewChild, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ReferenceLink } from '../../../typings/backendapi.gen';
import { first, takeUntil, map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { BreadcrumbService, EventType, Event, FormValidationInfo } from 'app/main/shared/breadcrumb-bar.service';
import { metaToUrl } from 'app/navigation/objrefnav';
import { EndPointSetDetailService } from '../../../resource/endpointset.gen/services';
import * as datatypes from '../../../typings/backendapi.gen';

 import { HttpClient } from '@angular/common/http';
 import { environment } from '../../../../environments/environment';
 import { kindToEndpoint, kindToUrlClass } from 'app/navigation/objrefnav.gen';

import { FuseLocationService } from 'app/services/location.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from'@angular/material/chips';
import * as _ from 'lodash';
import { zonedTimeToUtc, format, utcToZonedTime } from 'date-fns-tz';
import { ShowHideService } from "ngx-show-hide-password";
import jstz from 'jstz';

@Component({
    selector: 'endpointset-template',
    templateUrl: './component.html',
    styleUrls: ['./component.scss'],
})

export class EndpointsettemplateComponent implements OnInit, OnDestroy {
    @ViewChild('form') public form: NgForm;
    @Input() public resourceName: string;
    @Input() public namespace: string;
    @Input() public endpointset: datatypes.EndPointSet;
    @Output() refreshGridster: any = new EventEmitter();
    public loaded: boolean;
	  private timeZoneID = '';


    public template_mode_spec_template_enable_enum: Array<{text: string, value: string}> = [
        { text: "Disabled", value: "TemplateDisabled" },
        { text: "Enable_ExList", value: "TemplateEnable_ExList" },
        { text: "Enable_IncList", value: "TemplateEnable_IncList" },
    ];


     public LocationDataSource: any;
     public LocationEndpoint = ""


    private _location: string;
    
    
    
    
    

    didReset: boolean = false;
    isEdit = false;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly service: EndPointSetDetailService,
	      private readonly _breadcrumbService: BreadcrumbService,
	      public router: Router,
        private _locationService: FuseLocationService,
        private _showHideService: ShowHideService,
        private readonly _http: HttpClient,
    ) {}

    ngOnInit(): void {
	this._locationService.isBrowserTime.pipe(takeUntil(this._unsubscribeAll)).subscribe(res=>{
    if(this._locationService.isBrowserTime.getValue()){
      const timezone = jstz.determine();
      this.timeZoneID = timezone.name();
    } else {
      this.timeZoneID = this._locationService.locationDataAsKeyValue().timezone
    }
  });
	this.loaded = true;

	this.initPolicyCondition();
        this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe((queryParams)=> {
	    if (!queryParams['loc'] || queryParams['loc'] === this._location) {
		return;
	    }
            this.resourceName = this.activatedRoute.snapshot.params.resourceName;
            this._location = this._locationService.locationDataAsKeyValue().currentLocation;

             this.LocationEndpoint =
                `${environment.apiUrl}/${kindToUrlClass.get('Location')}/${this._location}/v1/` + kindToEndpoint.get("Location") + `?wsSelector=includeSubtree=true`;
             this.loadLocationRefs();
        });
    }

    ngOnChanges(change: SimpleChanges) {
        if (change.endpointset) {
            this.form?.form.markAsPristine();
            
        }
    }

    onValueChanged(form: NgForm): void {
	this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: form.valid}));
        this.refreshGridster.emit();
    }


    get metadataNameRules(): any {
	return [
	    { name: 'First_Letter', type: 'pattern', value: `^([A-Za-z0-9].*)$` },
	    { name: 'allowed_characters', type: 'pattern', value: `^[a-zA-Z0-9]([a-zA-Z0-9+_.@\-]*[a-zA-Z0-9+_.@\-])?(\.[a-zA-Z0-9+_.@\-]([a-zA-Z0-9+_.@\-]*[a-zA-Z0-9+_.@\-])?)*$` },
	];
    }

    save(form): void {
    }

    ngOnDestroy(): void {
	this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
    }




    public isspecTemplateExlistSelected(value: string): boolean {
        return this.endpointset.spec.template.Exlist?.some(item => item === value);
    }

    public isspecTemplateInclistSelected(value: string): boolean {
        return this.endpointset.spec.template.Inclist?.some(item => item === value);
    }

    public isstatusTemplateAppliedtoLocationsSelected(value: string): boolean {
        return this.endpointset.status.template.appliedto_locations?.some(item => item === value);
    }



     loadLocationRefs(): void {
         this._http.get<datatypes.LocationList>(this.LocationEndpoint).pipe(first()).subscribe(
             Locations => {
                 if(!Locations || !Locations.items) return;
                 this.LocationDataSource = Locations.items.map((item) => (
                     {
                         name: this._locationService.convertAPIGroup2Name(item.metadata.selfLink.split("/")[2]) + '/' + (item.spec?.base?.real_name? item.spec.base.real_name : item.metadata.name),
                         value: item.metadata.selfLink,
                         item: item
                     }
                 ));
             }
         );
     }
    objectRefToUrl(item: any): string {
	if (!item) return "";
        const reflink = item.metadata.selfLink;
	const location = reflink.split("/")[2];
        if (item.kind === 'File') {
	    const filePath = `${item.spec.commonAttrs.rootDir}/${item.spec.base.real_name}`;
            return "/file-manager/file" + "?filePath=" + filePath + "?loc=" + location;
        }
        return reflink.split("/").slice(-2).join("/") + "?loc=" + location;
    }
    loadRefLink(item: any) {
	if (!item) return;

        const reflink = item.metadata.selfLink;
	const loc = reflink.split("/")[2];
        var url, filePath: string;
        if (item.kind === 'File') {
	    url = "/file-manager/file";
	    filePath = `${item.spec.commonAttrs.rootDir}/${item.spec.base.real_name}`;
        } else {
	    url = "/" + reflink.split("/").slice(-2).join("/");
        }
        this.router.navigate([url], { relativeTo: this.activatedRoute,
                queryParams: { loc: loc, filePath: filePath }, queryParamsHandling: 'merge' });
	return false;
    }

    initPolicyCondition(): void {
	
	
	
	
	
    }
  
  
  
  
  

    renderPolicyCondition(p: datatypes.PolicyCondition[]): string[] {
        var opSymbols = {
            "Equals": " = ",
            "NotEquals": " != ",
            "Lesser": " < ",
            "LesserOrEquals": " <= ",
            "Greater" : " > ",
            "GreaterOrEquals": " >= ",
            "Contains": " contains ",
            "DoesNotContain": " !contains ",
            "RegEx": " RegEx "
        };
        var orStr = [];
        if (p) {
            p.map( (condition, index) => {
                var andStr = [];
                var andConditions = "";
                if (condition.aovList) {
                    andStr = condition.aovList.map( aov => 
                        (aov.attr.replace("AttrName", "") + opSymbols[aov.op.replace("AttrOp", "")] + aov.val)
                    );
                    andConditions = andStr.join(" AND ");
                }
                if (index != 0) {
                    orStr = orStr.concat("OR");
                }
                orStr = orStr.concat(andConditions);
            });
        }
        return orStr;
    }
 

}
