import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs'
import { ActivatedRoute, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { FuseLocationService } from './location.service'
@Injectable({
    providedIn: 'root'
})

export class FuseLocationResolver implements Resolve<Observable<any>>{
    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _locationService: FuseLocationService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentLocation = this._locationService.locationDataAsKeyValue().currentLocation;
        if (route.queryParams["loc"] === undefined){
            return this._locationService.getScope();
        }
        else{
            return of(this._locationService.locationDataAsKeyValue());
        }
    }
}
