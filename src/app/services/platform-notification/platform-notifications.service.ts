import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Rx';
import {filter, first, switchMap, takeUntil} from 'rxjs/operators';

import {GenericObjCount, PlatformNotification} from './platform-notification.model';
import {environment} from 'environments/environment';
import {WebsocketService} from './../websocket.service';
import {FuseLocationService} from '../location.service';
import {BehaviorSubject} from 'rxjs';


const SERVER_URL = `${environment.baseUrl}ws/count/__LOC__/v1/alerts?wsSelector=includeSubtree=true`.replace('http', 'ws');
const LOCATION_URL = `${environment.baseUrl}ws/count/__LOC__/v1/locations?wsSelector=includeSubtree=true`.replace('http', 'ws');
@Injectable({
  providedIn: 'root'
})
export class PlatformNotificationsService implements OnDestroy {
  private _unsubscribeAll: Subject<any> = new Subject();
  public alerts = new BehaviorSubject<GenericObjCount>(new GenericObjCount());
  public locations = new BehaviorSubject<GenericObjCount>(new GenericObjCount());
  public loc = '';

  /**
   * Constructor
   */
  constructor(private wsService: WebsocketService, private readonly locationService: FuseLocationService) {
    locationService
      .getScope()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res) => {
        if (res.spec.tenants) {
          this.loc = res.spec.tenants[0].apiGroup;
        }
      },
        error => {
        console.log('Error: ', error);
        });
  }

  createAlertWs(): void {
    if (this.alerts) {
      this.alerts.unsubscribe();
    }

    this.alerts = this.wsService.createSocket(SERVER_URL.replace('__LOC__', this.loc)).map(
      (response: MessageEvent): GenericObjCount => {
        let data = JSON.parse(response.data);
        return data;
      }) as BehaviorSubject<GenericObjCount>;
  }

  createLocationWs(): void {
    this.locations = this.wsService.createSocket(LOCATION_URL.replace('__LOC__', this.loc)).map(
      (response: MessageEvent): GenericObjCount => {
        let data = JSON.parse(response.data);
        return data;
      }) as BehaviorSubject<GenericObjCount>;
  }

  /**
   * Get all notifications
   */
  loadList(apigroup: string) {
    // return this._httpClient.get<{
    //     items: {
    //         status: PlatformNotification,
    //         spec: { acknowledged: boolean }
    //     }[]
    // }>(`${environment.baseUrl}apis/${apigroup}/v1/alerts?wsSelector=includeSubtree=true`);
    // enable later with ws
    return false;
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
