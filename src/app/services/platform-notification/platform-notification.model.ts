export class PlatformNotification {
    public code: string;
    public description: string;
    public severity: string;
    public state: string;
    public timeCleared: Date;
    public timeCreated: Date;
    public timeUpdated: Date;
}

export class GenericObjCount {
  public Count: string;
}
