import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CanDeactivate } from '@angular/router';
import { LabelsMenuComponent } from '@fuse/components/menu/labels-menu/labels-menu.component';
import { DialogCloseResult, DialogService, DialogResult } from "@progress/kendo-angular-dialog";
import { KendoPageawayDialog } from "app/main/shared/dialogs/kendo-pageaway-dialog/kendo-pageaway-dialog.component";
import { BreadcrumbService, EventType } from 'app/main/shared/breadcrumb-bar.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDeactivateGuard implements CanDeactivate<LabelsMenuComponent> {

  public navigateAway: Subject<boolean>;
  private isEditing = false;

  constructor(public _breadcrumbService: BreadcrumbService,
              private router: Router,
              private _dialogService: DialogService) {
    this.navigateAway = new Subject<boolean>();

    this._breadcrumbService.isDeviceDetailEditMode
      .subscribe(info => {
        if (!info) {
          this.isEditing = false;
        } else {
          this.isEditing = true;
        }
      });
  }

  canDeactivate(target: any) {
    if (this.isEditing) {
      const dialogRef = this._dialogService.open({
        title: "Save Changes?",
        content: KendoPageawayDialog,
        width: "600px",
        height: "310px",
      });
      const dialogInstance = dialogRef.content.instance;
      dialogInstance.message = 'You have unsaved changes on this page.\n Would you like to save these changes?';
      dialogInstance.dialog = dialogRef;
      dialogRef.result.subscribe((response: DialogResult) => {
        if (response instanceof DialogCloseResult || response.text == "No") {
          this.navigateAway.next(false);
        } else if (response.text == "Save") {
          this._breadcrumbService.triggeredEvent.next({type: EventType.DEVICE_DETAIL_SAVE});
          this.navigateAway.next(false);
        } else {
          // Reset edit state when we navigate away
          this.isEditing = false;
          this.navigateAway.next(true);
        };
      });

      return this.navigateAway.asObservable();
    }

    return true;
  }

  editing(): boolean {
    return this.isEditing;
  }
}
