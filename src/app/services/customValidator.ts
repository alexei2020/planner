import {AbstractControl, ValidatorFn} from '@angular/forms';

export function customValidator(): ValidatorFn {  
    return (control: AbstractControl): { [key: string]: any } | null =>  
        control.value?.toLowerCase() ===  control.value 
            ? null : {wrongInput: 'Only Lowercase is supported now.'};
}
