import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import * as _ from 'lodash';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { ICurrentLocationInfo, ILocations, IScopeResponse, IScopeTenantResponse } from 'app/typings/location';
import { LocationDataItem, LocationListItem } from 'app/components/shared/location-item';
import {LocationWidgetComponent} from '../components/location/component';

@Injectable({
  providedIn: 'root',
})
export class FuseLocationService {
  public disabledLocationSelection = new BehaviorSubject(false);

  private _scopeAPI = `${environment.baseUrl}apis/scope.ws.io/v1/scopes/scope`;

  private _locationData$: BehaviorSubject<ICurrentLocationInfo>;
  private _locationList$: BehaviorSubject<LocationListItem[]>;
  private _locationHierarchy$: BehaviorSubject<LocationDataItem>;
  private _locationSubTreeData$: BehaviorSubject<LocationDataItem>;
  private _locationDropdownEnabled$: BehaviorSubject<boolean>;
  private _forceRefreshLocationList$: BehaviorSubject<boolean>;

  // TODO: too many references, need to update separetely
  public isBrowserTime: BehaviorSubject<boolean>;

  constructor(private http: HttpClient) {
    this._locationData$ = new BehaviorSubject<ICurrentLocationInfo>({});
    this._locationList$ = new BehaviorSubject([]);
    this._locationHierarchy$ = new BehaviorSubject<LocationDataItem>(null);
    this._locationSubTreeData$ = new BehaviorSubject<LocationDataItem>(null);
    this._locationDropdownEnabled$ = new BehaviorSubject<boolean>(true);
    this._forceRefreshLocationList$ = new BehaviorSubject<boolean>(false);
    this.isBrowserTime = new BehaviorSubject(false);
  }

  get locationData(): any | Observable<any> {
    return this._locationData$.asObservable();
  }

  get locationList(): Observable<LocationListItem[]> {
    return this._locationList$.asObservable();
  }

  get locationSubTreeData(): Observable<LocationDataItem> {
    return this._locationSubTreeData$.asObservable();
  }

  get locationHierarchy(): Observable<LocationDataItem> {
    return this._locationHierarchy$.asObservable();
  }

  get locationDropdownEnabled(): Observable<boolean> {
    return this._locationDropdownEnabled$.asObservable();
  }

  get forceRefreshLocationList(): Observable<boolean> {
    return this._forceRefreshLocationList$.asObservable();
  }

  locationDataAsKeyValue(): ICurrentLocationInfo {
    return this._locationData$.getValue() as ICurrentLocationInfo;
  }

  updateLocationData(value): void {
    // Get the value from the behavior subject
    let config = this._locationData$.getValue();

    // Merge the new config
    config = _.merge({}, config, value);

    // Notify the observers
    this._locationData$.next(config);
  }

  updateLocationListValue(value: LocationListItem[]): void {
    this._locationList$.next(value);
  }

  updateLocationHierarchyValue(value: LocationDataItem): void {
    this._locationHierarchy$.next(value);
  }

  updateLocationSubTreeDataValue(value: LocationDataItem): void {
    this._locationSubTreeData$.next(value);
  }

  updateLocationDropdownEnabledValue(value: boolean): void {
    this._locationDropdownEnabled$.next(value);
  }

  updateForceRefreshLocationListValue(value: boolean): void {
    this._forceRefreshLocationList$.next(value);
  }

  get<T = any>(url: string): Observable<T> {
    return this.http.get(`${url}`).pipe(map(this.handleSuccess)) as Observable<T>;
  }

  protected handleSuccess(res: any): any {
    return res;
  }

  getHierarchicalData(apiGroup: string): Observable<ILocations> {
    const endPoint = `${environment.baseUrl}apis/${apiGroup}/v1/locations`;
    return this.get(endPoint);
  }

  getAccessibleData(tenant: string): Observable<IScopeTenantResponse> {
    console.log('getAccessibleData');
    if (!tenant) {
      return of(null);
    }
    const endPoint = `${environment.baseUrl}apis/scope.ws.io/v1/scope/${tenant}`;
    return this.get(endPoint);
  }

  getScope(): Observable<IScopeResponse> {
    return this.get(`${this._scopeAPI}`);
  }

  getLocationsByURI(segment: string): Observable<ILocations> {
    return this.get(`${segment}`);
  }

  // convert apiGroupName to location name
  convertAPIGroup2Name(apiGroupName: string): string {
    const locationListValue = this._locationList$.getValue();
    const locationDataValue = this.locationDataAsKeyValue();

    if (!locationListValue.length) {
      return locationDataValue.tenantName;
    }

    const locationObj: LocationListItem = _.find(locationListValue, ['value', apiGroupName]) || null;
    const location = !_.isEmpty(locationObj) ? locationObj.name : locationDataValue.tenantName;
    return location;
  }

  disableLocationSelector(): void {
    this.disabledLocationSelection.next(true);
  }

  enableLocationSelector(): void {
    this.disabledLocationSelection.next(false);
  }
}
