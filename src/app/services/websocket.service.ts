import {Injectable} from '@angular/core';
import * as Rx from 'rxjs/Rx';

@Injectable()
export class WebsocketService {
  private sockets: Array<{url: string, obj: WebSocket}> = [];

  constructor() {
  }

  createSocket(url: string): Rx.Subject<MessageEvent> {
    if (this.sockets[url]) {
      this.sockets[url].close();
    }

    let ws = new WebSocket(url);
    this.sockets[url] = ws;

    let observable = Rx.Observable.create((obs: Rx.Observer<MessageEvent>) => {
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    });
    let observer = {
      next: (data: Object) => {
        if (ws.readyState === WebSocket.OPEN) {
          console.log('ws data', data);
        }
      }
    };
    return Rx.Subject.create(observer, observable);
  }
}
