import {HttpClient} from '@angular/common/http';
import { timer } from 'rxjs/observable/timer';
import {Injectable, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class SessionService implements OnInit {
  public sessionWarningTimer$ = new Subject();
  private logout: any;
  public interval: any;
  private sessionTimeoutMinutes = 60;
  private sessionCheckingMinutes = 1;
  private timeLeft = this.sessionTimeoutMinutes;
  constructor(
    private readonly httpClient: HttpClient,
    public cookieService: CookieService,
  ) {
  }

  ngOnInit(): void {
    this.setSessionTimeout();
  }

  setSessionTimeout(): void {
    console.log('Session expire in (min): ', this.sessionTimeoutMinutes);
    this.logout = timer(this.sessionTimeoutMinutes * 60 * 1000).subscribe(this.sessionWarningTimer$);
    this.interval = setInterval(() => {
      this.checkCookie();
    }, this.sessionCheckingMinutes * 60 * 1000);
  }

  checkCookie(): void {
    this.timeLeft -= 1;
    if (this.cookieService.check('_ws')) {
      this.timeLeft = this.sessionTimeoutMinutes - this.sessionCheckingMinutes;
      this.logout.unsubscribe();
      this.cookieService.delete('_ws');
      this.logout = timer((this.sessionTimeoutMinutes - this.sessionCheckingMinutes) * 60 * 1000).subscribe(this.sessionWarningTimer$);
    }
    if (this.timeLeft > 0) {
      console.log('Session expire in (min): ', this.timeLeft);
    }
  }
}



