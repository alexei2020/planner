import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { LocationDataItem, LocationListData, LocationListItem } from 'app/components/shared/location-item';
import { ILocation, ILocationDataItem, ILocations } from 'app/typings/location';
import { StringMap } from 'app/typings/backendapi.gen';

@Injectable({
  providedIn: 'root',
})
export class FuseLocationDataManagementService {
  constructor() {}

  getLocationRoot(data: LocationDataItem[]): LocationDataItem {
    const idMapping = _.reduce(
      data,
      (acc, el, i) => {
        acc[el.selfLink] = i;
        return acc;
      },
      {}
    );
    let root: LocationDataItem;
    _.forEach(data, (el) => {
      if (el.parentLink === '') {
        root = el;
        return;
      }
      const parentEl: LocationDataItem = data[idMapping[el.parentLink]];
      parentEl.children = [...(parentEl.children || []), el];
    });

    return root;
  }

  getLocationListData(locationList: ILocations, accessibleData: StringMap<ILocationDataItem>): LocationListData {
    const innerLocationList: LocationListItem[] = [];
    const locationData: LocationDataItem[] = [];

    _.forEach(locationList.items, (item: ILocation) => {
      const accessibleDataItem = accessibleData[item.metadata.name];      
      if (!accessibleDataItem) {
        return;
      }

      innerLocationList.push(
        new LocationListItem({
          label: item.metadata.name,
          name: item.metadata.name,
          value: accessibleDataItem.APIGroupName,
          geo: item.status.geo,
        })
      );
      const parentLink = item.spec.base.parent_link === undefined ? '' : item.spec.base.parent_link;

      locationData.push(new LocationDataItem({
        name: item.metadata.name,
        selfLink: item.metadata.selfLink,
        parentLink: parentLink,
        geo: item.status.geo,
        value: accessibleDataItem.APIGroupName,
        APIGroupName: accessibleDataItem.APIGroupName,
        APIGroupUID: accessibleDataItem.APIGroupUID,
        RBAC: accessibleDataItem.RBAC,
      }));
    });

    const locationListData = new LocationListData({
      locationList: innerLocationList,
      locationData: locationData,
    });

    return locationListData;
  }
}
