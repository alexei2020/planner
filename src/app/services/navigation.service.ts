import { EventEmitter, Injectable } from '@angular/core';
import { FuseMenuType } from '@fuse/types';

@Injectable({ providedIn: 'root' })
export class NavigationService {
    public readonly menuTypeChanged: EventEmitter<FuseMenuType>;
    public readonly menuStateToggled: EventEmitter<FuseMenuType>;

    private fuseMenuType = FuseMenuType.IconOnly;
    private fuseMenuState;

    get menuType(): FuseMenuType {
        return this.fuseMenuType;
    }

    get menuState(): FuseMenuType {
        return this.fuseMenuState;
    }

    constructor() {
        this.menuStateToggled = new EventEmitter<FuseMenuType>();
        this.menuTypeChanged = new EventEmitter<FuseMenuType>();

        this.initFuseMenuState();
    }

    toggleMenu(): void {
        this.fuseMenuState = this.toggleFuseMenuState(this.fuseMenuState);

        this.menuStateToggled.emit(this.fuseMenuState);
    }

    changeMenuType(fuseMenu: FuseMenuType): void {
        this.fuseMenuType = fuseMenu;

        this.menuTypeChanged.emit(this.fuseMenuType);
    }

    private initFuseMenuState(): void {
        if (this.fuseMenuType === FuseMenuType.IconOnly || this.fuseMenuType === FuseMenuType.IconWithText) {
            this.fuseMenuState = this.fuseMenuType;
        } else {
            this.fuseMenuState = FuseMenuType.None;
        }
    }

    private toggleFuseMenuState(fuseMenuType: FuseMenuType): FuseMenuType {
        switch (fuseMenuType) {
            case FuseMenuType.IconOnly:
                return FuseMenuType.IconWithText;
            
            case FuseMenuType.IconWithText:
                return FuseMenuType.IconOnly;

            case FuseMenuType.None:
                return FuseMenuType.Overlapping;

            case FuseMenuType.Overlapping:
                return FuseMenuType.None;

            default:
                return FuseMenuType.None;
        }
    }
}
