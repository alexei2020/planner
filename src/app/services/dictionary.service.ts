import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { FuseLocationService } from 'app/services/location.service';
import { environment } from 'environments/environment';
import { kindToEndpoint } from 'app/navigation/objrefnav.gen';
import { DeviceList } from 'app/typings/backendapi.gen';


export interface DropdownDictNext {
    name: string,
    type: string,
}

export interface DropdownDict {
    name: string,
    type: string,
    value: string,
    next: DropdownDictNext,
};
    
@Injectable({
    providedIn: 'root'
})

export class DictionaryService {
  public dropdownDictionary: Map<string, DropdownDict[]>

  constructor(private http_: HttpClient,
              private _locationService: FuseLocationService) {
  }

  getDictionary(root: string): Observable<Map<string, DropdownDict[]>> {
    return this.http_.get<Map<string, DropdownDict[]>>(`assets/dictionary/${root}.json`);
  }

  getDictionaryTable(name: string, type: string): Observable<DropdownDict[]> {
    switch(type) {
      case 'dict':
        return this.http_.get(`assets/dictionary/dictionary.gen.json`).map( res => {
          return res[name];
        });
        break;
      case 'kind':
        const location = this._locationService.locationDataAsKeyValue().currentLocation;
        const url = `${environment.apiUrl}/history/${location}/v1/` + kindToEndpoint.get(name) + `?wsSelector=includeSubtree=true`;
        return this.http_.get<DeviceList>(url).map( res => {
          if(!res || !res.items) return;
          return res.items.map((item) => ({
            name: item.metadata.name,
            type: "object",
            value: item.metadata.selfLink,
            next: { name: "", type: 'dict' },
          }));
        });
        break;
    }

    return Observable.of([]);
  }
}
