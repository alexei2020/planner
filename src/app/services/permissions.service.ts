import { Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { FuseLocationService } from './location.service';
import { kindToPrivilege, kindToEndpoint } from '../navigation/objrefnav.gen';
import { navigationRules } from '../navigation/navservice.gen';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { ActivatedRoute } from '@angular/router';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

export interface Permissions {
  kind: string;
  apiGroup: string;
  location: string;
  url: string;
  permissions: string;
  hideByRule: boolean;
  hideMenu: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class PermissionsService {
  private permissions: Map<string, Map<string, Permissions>>;
  private _unsubscribeAll: Subject<any>;
  private currentLocation: string;
  private currentTenant: string;

  constructor(private _locationService: FuseLocationService,
              private activatedRoute: ActivatedRoute,
              private readonly _http: HttpClient,
              private _navigationService: FuseNavigationService) {
    this._unsubscribeAll = new Subject();
    this.init();
  }

  init(): void {
    this._locationService.getScope().pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
      this.permissions = new Map();
      res.spec.scopes?.map(item => {
        if (!this.permissions.has(item.apiGroup)) {
          this.permissions.set(item.apiGroup, new Map());
        }
        let kindPermissions = this.permissions.get(item.apiGroup);
        for (let [kind, privilege] of kindToPrivilege) {
          if (!kindPermissions.has(kind)) {
            kindPermissions.set(kind, {
              kind: kind,
              apiGroup: item.apiGroup,
              location: item.locationName,
              url: kindToEndpoint.get(kind),
              permissions: "",
              hideByRule: false,
              hideMenu: true,
            });
          }
          if (privilege === item.privilege) {
            var permissions = kindPermissions.get(kind);
            permissions.permissions = item.permissions;
            // If there is some permission, then open up the menu
            if (permissions.permissions) {
              permissions.hideMenu = permissions.hideByRule;
            }
          }
        }
      });
    });

    this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
      const loc = this._locationService.locationDataAsKeyValue().currentLocation;
      const tenant = this._locationService.locationDataAsKeyValue().tenantName;

      // If location has changed, refresh the new permissions based menu
      if (loc && this.currentLocation !== loc) {
        this.currentLocation = loc;
        console.log("LOC...", loc, this.permissions.get(loc));
        this._navigationService.setNavigationMenuByPermissions(this.permissions.get(loc));
      }

      // If tenant has changed, apply new rules based permissions
      if (tenant && tenant !== this.currentTenant) {
        console.log("Tenant...", tenant);
        this.currentTenant = tenant;
        navigationRules.forEach((rule) => {
          this.updateRulesBasedPermissions(rule);
        });
      }

    });
  }

  updateRulesBasedPermissions(rule) {
    const url = `${environment.apiUrl}/apis/${this.currentTenant}.ws.io/v1/` + kindToEndpoint.get(rule.kind);
    this._http.get<any>(url).pipe(first()).subscribe(objects => {
      if (objects.items?.length) {
        // We expect the rules to be only on singleton objects; so, we need to check the only
        // object
        this.updateRuleBasedMenu(rule, objects.items[0]);

        // Register for object changes
        const wsUrl = (`${environment.apiUrl}/ws/obj/${this.currentTenant}.ws.io/v1/` + kindToEndpoint.get(rule.kind) + `/${objects.items[0].metadata.name}`).replace('http', 'ws');
        if (rule.ws) {
          rule.ws.complete();
        }
        rule.ws = webSocket(wsUrl);
        rule.ws.pipe(takeUntil(this._unsubscribeAll)).subscribe((rsp) => {
          if (!rsp) return;
          let response = JSON.parse(rsp.Object);
          this.updateRuleBasedMenu(rule, response);
        });
      } else {
        // No object - update with default value
        this.updateRuleBasedMenu(rule);
      }
    });
  }

  // NOTE: object is used in eval()
  updateRuleBasedMenu(navRule, object?) {
    // disable all the kinds listed across all locations
    navRule.rules.forEach(rule => {
      rule.disable.forEach(kind => {
        for (let [loc, kindPerm] of this.permissions) {
          var permission = kindPerm.get(kind)
          permission.hideByRule = object ? eval(`${rule.eval}`.replace(navRule.kind, "object")) : false;
          permission.hideMenu = (permission.hideByRule || !permission.permissions);
        }
      });
    });
    console.log("updateRuleBasedMenu...", this.permissions.get(this.currentLocation));
    this._navigationService.setNavigationMenuByPermissions(this.permissions.get(this.currentLocation));
  }

  ngOnDestroy() {
    // Unsubscribe from all subscriptions
    console.log("DESTROYED!!!");
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
