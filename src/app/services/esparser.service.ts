import {Injectable} from '@angular/core';

export interface ESResponse {
  took: number;
  timed_out: boolean;
  _scroll_id?: string;
  _shards: any;
  hits: {
    total: {
      value: number;
      relation: string;
    };
    max_score: number;
    hits: Array<{
        _index: string;
        _type: string;
        _id: string;
        _score: number;
        _source: {
          object: any;
          _type: string;
        };
        _version?: number;
        _explanation?: any;
        fields?: any;
        highlight?: any;
        inner_hits?: any;
        matched_queries?: string[];
        sort?: string[];
    }>;
  };
  aggregations?: any;
}

@Injectable({
    providedIn: 'root'
})
export class ElasticSearchParserService {
  constructor() {
  }

  public items(res: ESResponse): any[] {
    if (!res) return [];

    return res.hits.hits.map( (item) => ( item._source.object ) );
  }

  public total(res: ESResponse): number {
    if (!res) return 0;

    if (res.aggregations?.total) {
        return res.aggregations.total.value;
    }

    return res.hits.total.value;
  }

  public aggrStats(res: ESResponse, value: string): any {
    if (!res || !res.aggregations) return [];
    var key = "agg_terms_" + value;

    if (res.aggregations[key]) {
      return res.aggregations[key].buckets;
    }

    key = "agg_filter_" + value;
    if (res.aggregations[key]) {
       return res.aggregations[key].doc_count;
    }
    return [];
  }
}
