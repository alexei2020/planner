export default [
    {
        type: 'FloorPlan',
        name: 'Floor Plan',
        preview: 'assets/roomplanner/catalog/floorplan/floorplan.png',
    },

    {
        type: 'Room',
        name: 'Draw Room',
        preview: 'assets/roomplanner/catalog/room/room.png',
        textures: {
            floor: [
                {
                    label: 'Transparent',
                    icon: 'format_color_reset',
                },
                {
                    label: 'Light Laminate',
                    icon: 'assets/roomplanner/catalog/room/textures/laminate_2.jpg',
                    url: 'assets/roomplanner/catalog/room/textures/laminate_2.jpg',
                    default: true,
                },
                {
                    label: 'Dark Laminate',
                    icon: 'assets/roomplanner/catalog/room/textures/laminate_1.jpg',
                    url: 'assets/roomplanner/catalog/room/textures/laminate_1.jpg',
                },
            ],
            walls: [
                {
                    label: 'White Solid',
                    icon: 'assets/roomplanner/catalog/room/wall/white-solid-wall.png',
                },
                {
                    label: 'White Block',
                    icon: 'assets/roomplanner/catalog/room/wall/white-block-wall.png',
                    url: 'assets/roomplanner/catalog/room/wall/textures/white-block-facade.png',
                },
                {
                    label: 'Brown Block',
                    icon: 'assets/roomplanner/catalog/room/wall/brown-block-wall.png',
                    url: 'assets/roomplanner/catalog/room/wall/textures/brown-block-facade.png',
                    default: true,
                },
            ],
        },
    },

    {
        type: 'RoomLabel',
        name: 'Room Label',
        preview: 'assets/roomplanner/catalog/room-label/room.png',
        snap: {
            to: ['Wall'],
            height: 0.8,
            hole: false,
        },
    },

    {
        type: 'Door',
        name: 'Door',
        preview: 'assets/roomplanner/catalog/door/single-window-door.png',
        models: [ // variants
            {
                label: 'Single Door',
                icon: 'assets/roomplanner/catalog/door/single-door.png',
                url: 'assets/roomplanner/catalog/door/single-door.fbx',
                scale: [0.0001583, 0.0001583, 0.0001583],
            },
            {
                label: 'Double Door',
                icon: 'assets/roomplanner/catalog/door/double-door.png',
                url: 'assets/roomplanner/catalog/door/double-door.fbx',
                scale: [0.0003567, 0.0003567, 0.0003567],
            },
            {
                label: 'Single Window Door',
                icon: 'assets/roomplanner/catalog/door/single-window-door.png',
                url: 'assets/roomplanner/catalog/door/single-window-door.glb',
                scale: [0.34, 0.34, 0.34],
                default: true,
            },
            {
                label: 'Double Window Door',
                icon: 'assets/roomplanner/catalog/door/double-window-door.png',
                url: 'assets/roomplanner/catalog/door/double-window-door.glb',
                scale: [0.34, 0.34, 0.34],
            },
            {
                label: 'Triple Window Door',
                icon: 'assets/roomplanner/catalog/door/triple-window-door.png',
                url: 'assets/roomplanner/catalog/door/triple-window-door.glb',
                scale: [0.34, 0.34, 0.34],
            },
        ],
        snap: {
            to: ['Wall'],
            hole: true,
            height: 0,
        },
    },

    {
        type: 'Window',
        name: 'Window',
        preview: 'assets/roomplanner/catalog/window/single-window.png',
        models: [
            {
                label: 'Single Window',
                icon: 'assets/roomplanner/catalog/window/single-window.png',
                url: 'assets/roomplanner/catalog/window/single-window.glb',
                scale: [0.34, 0.34, 0.34],
                default: true,
            },
            {
                label: 'Double Window',
                icon: 'assets/roomplanner/catalog/window/double-window.png',
                url: 'assets/roomplanner/catalog/window/double-window.glb',
                scale: [0.34, 0.34, 0.34],
            },
            {
                label: 'Square Window',
                icon: 'assets/roomplanner/catalog/window/square-window.png',
                url: 'assets/roomplanner/catalog/window/square-window.glb',
                scale: [0.34, 0.34, 0.34],
            },
        ],
        snap: {
            to: ['Wall'],
            hole: true,
            height: 0.258,
        },
    },

    {
        type: 'EthernetWallJack',
        name: 'Ethernet Wall Jack',
        preview: 'assets/roomplanner/catalog/ethernet-wall-jack/ethernet-wall-jack.png',
        model: 'assets/roomplanner/catalog/ethernet-wall-jack/ethernet-wall-jack.glb',
        offset: [0.5, 0, 0],
        scale: [10, 10, 0.5],
        snap: {
            to: ['Wall'],
            hole: false,
            height: 0.1,
        },
    },

    {
        type: 'WirelessAccessPoint',
        name: 'Wireless Access Point',
        preview: 'assets/roomplanner/catalog/wireless-access-point/wireless-access-point.png',
        model: 'assets/roomplanner/catalog/wireless-access-point/wireless-access-point.glb',
        scale: [1.25, 1.25, 1.25],
        snap: {
            to: ['Floor', 'Table', 'Ceiling'],
            closest: true,
            hole: false,
            height: 0.1,
        },
    },

    {
        type: 'Furniture',
        name: 'TV',
        preview: 'assets/roomplanner/catalog/tv/tv.png',
        model: 'assets/roomplanner/catalog/tv/tv.glb',
        snap: {
            to: ['Floor', 'Wall'],
            hole: false,
        },
    },

    {
        type: 'Furniture',
        name: 'Microphone',
        preview: 'assets/roomplanner/catalog/microphone/sennheiser_mat_133-s/scene.png',
        model: 'assets/roomplanner/catalog/microphone/sennheiser_mat_133-s/scene.gltf',
        rotate: [0, -Math.PI / 2, 0],
        scale: [0.01, 0.01, 0.01],
        snap: {
            to: ['Floor', 'Table'],
            hole: false,
        },
    },

    {
        type: 'Furniture',
        name: 'Telephone',
        preview: 'assets/roomplanner/catalog/telephone/office_phone/scene.png',
        model: 'assets/roomplanner/catalog/telephone/office_phone/scene.gltf',
        rotate: [0, Math.PI, 0],
        scale: [0.0001, 0.0001, 0.0001],
        snap: {
            to: ['Floor', 'Table'],
            hole: false,
        },
    },

    {
        type: 'Furniture',
        name: 'Laptop',
        preview: 'assets/roomplanner/catalog/laptop/laptop_free/scene.png',
        model: 'assets/roomplanner/catalog/laptop/laptop_free/scene.gltf',
        rotate: [0, 0, 0],
        scale: [0.002, 0.002, 0.002],
        snap: {
            to: ['Floor', 'Table'],
            hole: false,
        },
    },

    {
        type: 'Furniture',
        name: 'Printer',
        preview: 'assets/roomplanner/catalog/printer/printer-_low_poly/scene.png',
        model: 'assets/roomplanner/catalog/printer/printer-_low_poly/scene.gltf',
        rotate: [0, 0, 0],
        scale: [0.015, 0.015, 0.015],
        snap: {
            to: ['Floor', 'Table'],
            hole: false,
        },
    },

    {
        type: 'Table',
        name: 'Table',
        preview: 'assets/roomplanner/catalog/table/default-table.png',
        model: 'assets/roomplanner/catalog/table/default-table.glb',
        scale: [0.3, 0.3, 0.3],
        snap: {
            to: ['Floor'],
            hole: false,
        },
    },
];
