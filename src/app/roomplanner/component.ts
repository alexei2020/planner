import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';

import { BreadcrumbService } from '../main/shared/breadcrumb-bar.service';
import { PageType } from '../main/shared/breadcrumb-bar.service.gen';
import { RoomPlannerService } from './services/room-planner.service';
import { FloorSpaceService } from './services/floor-space.service';

import Editor from './editor/Editor';

import catalog from './catalog';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
@Component({
    selector: 'roomplanner',
    templateUrl: './component.html',
    styleUrls: ['./component.scss'],
})
export class RoomPlannerComponent implements OnInit, OnDestroy, AfterViewInit {
    catalog = catalog;
    editor: Editor;

    measureSystem = 'imperial';
    measureUnit1 = 1;
    measureUnit2 = 0;

    @ViewChild('editorContainer', { static: false }) editorContainer;

    constructor(
        private _breadcrumbService: BreadcrumbService,
        private _roomPlannerService: RoomPlannerService,
        private _floorSpaceService: FloorSpaceService,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer
    ) {
        iconRegistry.addSvgIcon(
            'roomplanner-zoom-in',
            sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/zoom/zoom-in.svg')
        );
        iconRegistry.addSvgIcon(
            'roomplanner-zoom-out',
            sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/zoom/zoom-out.svg')
        );
    }

    ngOnInit() {
        this.editor = new Editor(catalog);
        this.editor.interfaceManager.floorSpaceService = this._floorSpaceService;
        this.editor.interfaceManager.roomPlannerService = this._roomPlannerService;
        this.editor.addEventListener('restarted', () => {
            this.measureSystem = 'imperial';
            this.measureUnit1 = 1;
            this.measureUnit2 = 0;
        });

        this._breadcrumbService.currentPage.next(PageType.ROOM_PLANNER);

        this._roomPlannerService.editor = this.editor;
    }

    ngOnDestroy() {
        this._breadcrumbService.currentPage.next(PageType.DEFAULT);

        this.editor.dispose();
    }

    ngAfterViewInit() {
        this.editor.init(this.editorContainer.nativeElement);
    }
}
