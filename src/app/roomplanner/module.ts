import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { TreeListModule } from '@progress/kendo-angular-treelist';
import {DialogsModule} from '@progress/kendo-angular-dialog';

import { FloorSpaceDialogComponent } from './components/floor-space-dialog.component';
import { RoomPlannerDialogComponent } from './components/room-planner-dialog.component';

import { RoomPlannerViewerComponent } from './components/viewer.component';

import { RoomPlannerComponent } from './component';

@NgModule({
    declarations: [
        FloorSpaceDialogComponent,
        RoomPlannerDialogComponent,

        RoomPlannerViewerComponent,

        RoomPlannerComponent,
    ],
    imports: [
        MatIconModule,
        MatButtonModule,
        MatTooltipModule,
        MatListModule,
        MatMenuModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatSelectModule,
        MatDialogModule,
        MatInputModule,
        MatExpansionModule,

        TreeListModule,
        DialogsModule,

        CommonModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
        FloorSpaceDialogComponent,
        RoomPlannerDialogComponent,

        RoomPlannerViewerComponent,

        RoomPlannerComponent,
    ],
})
export class RoomPlannerModule {
}
