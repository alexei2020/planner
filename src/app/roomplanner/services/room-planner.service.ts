import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { ReplaySubject } from 'rxjs';

import { RoomPlannerDialogComponent } from '../components/room-planner-dialog.component';

import Editor from '../editor/Editor';

import { FloorSpaceService } from './floor-space.service';
import { WallJackService } from './wall-jack.service';

export interface RoomPlannerEvent {
    type: string,
    payload?: any
}

@Injectable({
    providedIn: 'root',
})
export class RoomPlannerService extends ReplaySubject<RoomPlannerEvent> {
    editor: Editor | null = null;

    constructor(
        private readonly http: HttpClient,
        private readonly dialog: MatDialog,
        private readonly floorSpaceService: FloorSpaceService,
        private readonly wallJackService: WallJackService,
    ) {
        super();
    }

    /* */

    selectDialog(items: any = [], options: any = {}) {
        return this.dialog.open(RoomPlannerDialogComponent, {
            panelClass: 'dialog-panel',
            disableClose: false,
            autoFocus: true,
            width: '400px',
            data: {
                id: items[0].id,
                items,
                options,
            },
        })
            .afterClosed();
    }

    /* */

    async load(data) {
        if (!data || !data.plannerData) {
            this.editor.fromJSON(null);
            return;
        }

        const name = data.name1;
        const plannerData = data.plannerData;

        // const entities = plannerData.entities;
        //
        // const items = await this.floorSpaceService.loadAuxs(name).toPromise();
        // items.forEach((item) => {
        //     const wallJack = entities.find(entity => StringUtils.normalize(entity.name) === item.status.wallJack);
        //     if (wallJack) {
        //         const obj = catalog.find(obj => obj.name === item.status.image);
        //         if (obj) {
        //             const id = v4();
        //
        //             const attachedTo = entities.find(entity => entity.id === wallJack.attachedTo);
        //             if (attachedTo) {
        //                 attachedTo.entities.push(id);
        //             }
        //
        //             entities.push({
        //                 id: id,
        //                 type: obj.type,
        //                 name: obj.name,
        //                 position: wallJack.position,
        //                 rotation: wallJack.rotation,
        //                 scale: [1, 1, 1],
        //                 visible: wallJack.visible,
        //                 attachedTo: attachedTo?.id,
        //             });
        //         }
        //     }
        // });

        this.editor.fromJSON(plannerData);
    }

    async save(layers) {
        const plannerData = this.editor.toJSON();
        if (!plannerData.entities.length) {
            alert('Scene data is empty, please try again');
            return;
        }

        let isNew = false;
        let item = layers[0]?.root || layers[0];
        if (item) {
            isNew = item.isPreview || false;
            item.plannerData = plannerData;
        } else {
            isNew = true;
            item = {
                parentId: null,
                type: 'Floor',
                name: '',
                floorPlan: '',
                plannerData,
            };
        }

        const hierarchyItem = plannerData.hierarchy.find(item => !item.parentId && (item.type === 'Floor' || item.type === 'Suite'));
        if (hierarchyItem) {
            item.type = hierarchyItem.type;
            item.name = hierarchyItem.name;
        } else {
            const response = await this.floorSpaceService.confirmDialog('', item).toPromise();
            if (!response) {
                return;
            }
            item.type = response.type;
            item.name = response.name;
        }

        const checkName = async () => {
            if (!this.floorSpaceService.items.some(v => !v.id.startsWith(item.id) && !v.parentId && v.name.toLowerCase() === item.name.toLowerCase())) {
                return true;
            }

            const response = await this.floorSpaceService.confirmDialog(`"${item.name}" already exists, please input new unique name:`, item).toPromise();
            if (!response) {
                return false;
            }
            if (!response.name || response.name.length < 1) {
                alert('Invalid name, please try again');
                return false;
            }

            this.editor.entityManager.entities.forEach((entity) => {
                if (entity.type === 'Room' && entity.name === item.name) {
                    entity['setLabel'](response.name);
                    entity.meta.type = response.type;
                    entity.meta.name = response.name;
                }
            })

            item.type = response.type;
            item.name = response.name;
            item.plannerData = this.editor.toJSON();

            return await checkName();
        };
        if (!await checkName()) {
            return;
        }

        const response = await this.floorSpaceService.save(item, isNew).toPromise<any>();

        alert('Project successfully saved');

        // const entities = data.entities;
        // const jacks = entities.filter(entity => entity.type === 'EthernetWallJack');
        // const items = this.wallJackService.items.filter(i => i.spec.base.parent_link === item.id1);
        //
        // // TODO update instead of re-create
        // Promise.all(items.map(item => this.wallJackService.remove({ id: item.metadata.name }).toPromise()))
        //     .then(() => Promise.all(jacks.map(jack => this.wallJackService.save(item, { name: jack.name }, true).toPromise())))
        //     .then(() => this.wallJackService.load())
        //     .then(() => {
        //         alert('Project successfully saved');
        //     });
        return response.metadata.name;
    }
}
