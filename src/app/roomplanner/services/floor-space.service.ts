import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { Observable, ReplaySubject, throwError } from 'rxjs';
import { tap, take, reduce, catchError } from 'rxjs/operators';
import cloneDeep from 'lodash/cloneDeep';

import { FloorSpaceDialogComponent } from '../components/floor-space-dialog.component';

import StringUtils from '../editor/utils/StringUtils';
import {environment} from '../../../environments/environment';

function parse(str) {
    try {
        return JSON.parse(str);
    } catch (err) {
        return null;
    }
}

function stringify(obj) {
    try {
        return obj ? JSON.stringify(obj) : '';
    } catch (err) {
        return '';
    }
}

export interface FloorSpaceData {
    kind: string;
    version: string,
    uid: string;
    id: string;
    parentId: string;
    name1: string;
    name: string;
    type: string;
    floorPlan: string;
    plannerData: any;
}

@Injectable({
    providedIn: 'root',
})
export class FloorSpaceService extends ReplaySubject<any> {
    public location = '';
    public loading = true;
    public preItems: any[] = [];
    public orgItems: any[] = [];
    public items: any[] = [];
    apiUrl: string = environment.apiUrl;

    public isConfirmDialogOpened = false;

    constructor(
        private readonly http: HttpClient,
        private readonly dialog: MatDialog,
    ) {
        super();
    }

    /* */

    confirmDialog(title?: string, data: any = {}, options: any = {}) {
        this.isConfirmDialogOpened = true;

        return this.dialog.open(FloorSpaceDialogComponent, {
            panelClass: 'dialog-panel',
            disableClose: true,
            autoFocus: true,
            width: '400px',
            data: {
                title,
                type: data.type || 'Floor',
                name: data.name || '',
                options,
            },
        })
            .afterClosed()
            .pipe(tap(() => {
                this.isConfirmDialogOpened = false;
            }));
    }

    /* */

    load() {
        this.loading = true;
        this.next({
            loading: this.loading,
            preItems: this.preItems,
            orgItems: this.orgItems,
            items: this.items,
        });

        this.http.get<any>(`${this.apiUrl}/apis/${this.location}/v1/floorspaces`)
            .pipe(take(1))
            .pipe(catchError((err) => this.errorHandler(err)))
            .subscribe((response) => {
                this.loading = false;
                if(!response.items) {
                    this.preItems = []
                } else {
                    this.preItems = response.items.map((item) => ({
                        root: null,
                        kind: item.kind,
                        version: item.metadata.resourceVersion,
                        uid: item.metadata.uid,
                        id: item.metadata.uid,
                        id1: item.metadata.selfLink,
                        name1: item.metadata.name,
                        parentId: null, // item.spec.base.parent_link || null,
                        name: item.spec.base.real_name || 'untitled',
                        type: item.spec.type,
                        floorPlan: item.spec.floorPlan,
                        plannerData: parse(item.spec.plannerData),
                    }));
                }
                this.orgItems = cloneDeep(this.preItems);
                this.items = this.computeItems(this.preItems);
                this.next({
                    loading: this.loading,
                    preItems: this.preItems,
                    orgItems: this.orgItems,
                    items: this.items,
                });
            });
    }

    loadAuxs(floorSpace) {
        return this.http.get(`${this.apiUrl}/apis/${this.location}/v1/endpointauxs?WSFilter=status.floorSpace=${floorSpace}`)
            .pipe(take(1))
            .pipe(catchError((err) => this.errorHandler(err)))
            .pipe(reduce((items, response: any) => ([
                ...items,
                ...response.items.filter(item => item.status.floorSpace === floorSpace),
            ]), []));
    }

    create(item: any): Observable<FloorSpaceData> {
        this.loading = true;
        this.next({
            loading: this.loading,
            preItems: this.preItems,
            orgItems: this.orgItems,
            items: this.items,
        });

        return this.http.post<FloorSpaceData>(`${this.apiUrl}/apis/${this.location}/v1/floorspaces`, {
            apiVersion: `${this.location}/v1`,
            kind: 'FloorSpace',
            metadata: {
                name: StringUtils.normalize(item.name),
            },
            spec: {
                base: {
                    parent_link: item.parentId || '',
                    real_name: item.name,
                },
                type: item.type || 'Floor',
                floorPlan: item.floorPlan || '',
                plannerData: stringify(item.plannerData),
            },
        })
            .pipe(catchError((err) => this.errorHandler(err)))
            .pipe(tap(() => this.load()));
    }

    update(item: any): Observable<FloorSpaceData> {
        this.loading = true;
        this.next({
            loading: this.loading,
            preItems: this.preItems,
            orgItems: this.orgItems,
            items: this.items,
        });

        return this.http.put<FloorSpaceData>(`${this.apiUrl}/apis/${this.location}/v1/floorspaces/${item.name}`, {
            apiVersion: `${this.location}/v1`,
            kind: 'FloorSpace',
            metadata: {
                resourceVersion: item.version,
                name: item.name1,
            },
            spec: {
                base: {
                    parent_link: item.parentId || '',
                    real_name: item.name,
                },
                type: item.type || 'Floor',
                floorPlan: item.floorPlan || '',
                plannerData: stringify(item.plannerData),
            },
        })
            .pipe(catchError((err) => this.errorHandler(err)))
            .pipe(tap(() => this.load()));
    }

    remove(item: any): Observable<FloorSpaceData> {
        this.loading = true;
        this.next({
            loading: this.loading,
            preItems: this.preItems,
            orgItems: this.orgItems,
            items: this.items,
        });

        return this.http.delete<FloorSpaceData>(`${this.apiUrl}/apis/${this.location}/v1/floorspaces/${item.name}`)
            .pipe(catchError((err) => this.errorHandler(err)))
            .pipe(tap(() => this.load()));
    }

    save(item: any, isNew: boolean): Observable<FloorSpaceData> {
        return isNew ? this.create(item) : this.update(item);
    }

    /* */

    restore() {
        this.loading = false;
        this.preItems = cloneDeep(this.orgItems);
        this.orgItems = cloneDeep(this.orgItems);
        this.items = this.computeItems(this.preItems);
        this.next({
            loading: this.loading,
            preItems: this.preItems,
            orgItems: this.orgItems,
            items: this.items,
        });
    }

    /* */

    computeItems(items) {
        return items.reduce((items, rootItem) => {
            const plannerData = rootItem.plannerData || {};
            const hierarchyList = plannerData.hierarchy || [];
            const hierarchyItem = hierarchyList.find(item => !item.parentId && (item.type === 'Floor' || item.type === 'Suite'));

            const hierarchyItems = hierarchyList.map((item) => ({
                isPreview: rootItem.isPreview,
                isVirtual: true,

                root: rootItem,
                kind: rootItem.kind,
                version: rootItem.version,
                uid: rootItem.uid,
                id: `${rootItem.id}--${item.id}`,
                id1: rootItem.id1,
                name1: `${rootItem.name}--${item.name}`,
                parentId: hierarchyItem
                    ? (item.id === hierarchyItem.id ? null : `${rootItem.id}--${item.parentId || hierarchyItem.id}`)
                    : rootItem.id,
                parentName1: hierarchyItem
                ? (item.name1 === hierarchyItem.name1 ? null : `${rootItem.name1}--${item.name1 || hierarchyItem.name1}`)
                : rootItem.name1,
                name: item.name || 'Untitled',
                type: item.type || 'Other',
                floorPlan: rootItem.floorPlan,
                plannerData: rootItem.plannerData,
            }));
            if (!hierarchyItem) {
                hierarchyItems.unshift(rootItem);
            }

            return [
                ...items,
                ...hierarchyItems,
            ];
        }, []);
    }

    /* */

    private errorHandler(err) {
        this.loading = false;
        this.preItems = [];
        this.orgItems = [];
        this.items = [];

       /*  this.next({
            loading: this.loading,
            preItems: this.preItems,
            orgItems: this.orgItems,
            items: this.items,
        }); */
        // alert('Error While doing action')
        console.log('Undefined error occurred while floor space fetching');

        return throwError(err);
    }
}
