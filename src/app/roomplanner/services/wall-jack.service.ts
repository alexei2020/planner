import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';

import StringUtils from '../editor/utils/StringUtils';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class WallJackService extends ReplaySubject<{ loading: boolean, items: any[] }> {
    public location = '';
    public loading = true;
    public items = [];
    apiUrl: string = environment.apiUrl;

    constructor(private readonly http: HttpClient) {
        super();
    }

    load() {
        this.loading = true;
        this.next({
            loading: this.loading,
            items: this.items,
        });

        this.http.get<any>(`${this.apiUrl}/apis/${this.location}/v1/walljacks`)
            .pipe(take(1))
            .subscribe((response) => {
                this.loading = false;
                this.items = response.items;
                this.next({
                    loading: this.loading,
                    items: this.items,
                });
            });
    }

    create(floorSpace, item): Observable<any> {
        this.loading = true;
        this.next({
            loading: this.loading,
            items: this.items,
        });

        return this.http.post<any>(`${this.apiUrl}/apis/${this.location}/v1/walljacks`, {
            apiVersion: `${this.location}/v1`,
            kind: 'WallJack',
            metadata: {
                name: StringUtils.normalize(item.name),
            },
            spec: {
                base: {
                    parent_link: floorSpace?.id || '',
                    real_name: item.name,
                },
            },
        });
    }

    update(floorSpace, item): Observable<any> {
        this.loading = true;
        this.next({
            loading: this.loading,
            items: this.items,
        });

        return this.http.put<any>(`${this.apiUrl}/apis/${this.location}/v1/walljacks/${item.id}`, {
            apiVersion: `${this.location}/v1`,
            kind: 'WallJack',
            metadata: {
                resourceVersion: item.version,
                name: item.id,
            },
            spec: {
                base: {
                    parent_link: item.parentId || '',
                    real_name: item.name,
                },
            },
        });
    }

    remove(item): Observable<any> {
        this.loading = true;
        this.next({
            loading: this.loading,
            items: this.items,
        });

        return this.http.delete<any>(`${this.apiUrl}/apis/${this.location}/v1/walljacks/${item.id}`);
    }

    save(floorSpace, item, isNew: boolean): Observable<any> {
        return isNew ? this.create(floorSpace, item) : this.update(floorSpace, item);
    }
}
