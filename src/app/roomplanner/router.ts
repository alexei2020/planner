import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoomPlannerComponent } from './component';
import { RoomPlannerModule } from './module';

const routes: Routes = [
    {
        path: '**',
        component: RoomPlannerComponent,
    },
];

@NgModule({
    imports: [
        RoomPlannerModule,
        RouterModule.forChild(routes),
    ],
})
export class RoomPlannerRouter {
}
