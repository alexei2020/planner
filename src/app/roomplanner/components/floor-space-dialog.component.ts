import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {customValidator} from '../../services/customValidator';


@Component({
    selector: 'floor-space-dialog',
    templateUrl: './floor-space-dialog.component.html',
    styleUrls: ['./floor-space-dialog.component.scss'],
})
export class FloorSpaceDialogComponent {
    dialogForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<FloorSpaceDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public dialogData: any,
    ) {
        this.dialogForm = new FormGroup({
            type: new FormControl(this.dialogData.type || 'Floor', [
                Validators.required,
            ]),
            name: new FormControl(this.dialogData.name || '', [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(32),
                customValidator()
            ]),
        }, {updateOn: 'change'});
    }

    cancelHandler(): void {
        this.dialogRef.close(null);
    }

    okHandler(): void {
        if (this.dialogForm.valid === false) {
            this.dialogForm.markAllAsTouched();
            return;
        }

        this.dialogRef.close(this.dialogForm.value);
    }
}
