import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'room-planner-dialog',
    templateUrl: './room-planner-dialog.component.html',
    styleUrls: ['./room-planner-dialog.component.scss'],
})
export class RoomPlannerDialogComponent {
    dialogForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<RoomPlannerDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public dialogData: any,
    ) {
        this.dialogForm = new FormGroup({
            id: new FormControl(this.dialogData.id, [
                Validators.required,
            ]),
        });
    }

    cancelHandler(): void {
        this.dialogRef.close(null);
    }

    okHandler(): void {
        if (this.dialogForm.valid === false) {
            this.dialogForm.markAllAsTouched();
            return;
        }

        this.dialogRef.close(this.dialogForm.value);
    }
}
