import {
  Component,
  Input,
  ViewChild,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  SimpleChange,
  ElementRef,
  HostListener
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import Editor from '../editor/Editor';
import { EditorMode, TransformMode, ViewMode } from '../editor/managers/ViewManager';

import { RoomPlannerService } from '../services/room-planner.service';
import { FloorSpaceService } from '../services/floor-space.service';
import { WallJackService } from '../services/wall-jack.service';
import { FuseLocationService } from '../../../app/services/location.service';
import catalog from '../catalog';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { PropertyMap } from 'app/typings/backendapi';
import _ from 'lodash';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import {
  TreeListComponent,
} from "@progress/kendo-angular-treelist";

@Component({
  selector: 'roomplanner-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss'],
})
export class RoomPlannerViewerComponent implements OnChanges, OnDestroy, AfterViewInit {
  @Input() location = 'ws.io';
  @Input() layers = [];

  @Input()
  locationApiGroup: string;

  @Input()
  tenantName: string;

  @Input()
  locationName: string;

  @ViewChild('container', { static: false }) container;
  @ViewChild("overlay") overlay: ElementRef;
  @ViewChild("tree", { static: false }) tree: TreeListComponent;


  editor: Editor;
  private apigroup: any;

  loadTree = true;
  dataTree = [];

  selectedOrder = ['Floor', 'Suite', 'Room', 'Other'];
  selectedItems = [];
  selectedLayers = [];
  showTree = false;
  selectedTitle = "";

  private _unsubscribeAll: Subject<any> = new Subject();

  constructor(
    private _roomPlannerService: RoomPlannerService,
    private _floorSpaceService: FloorSpaceService,
    private _wallJackService: WallJackService,
    private _locationService: FuseLocationService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
      'edit',
      sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/edit.svg')
    );

    this.editor = new Editor(catalog);
    this.editor.viewManager.changeEditorMode(EditorMode.View);
    this.editor.viewManager.changeTransformMode(TransformMode.Select);
    this.editor.viewManager.changeViewMode(ViewMode.Iso);
    this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
    this._floorSpaceService.location = this.apigroup;
    this._floorSpaceService.load();

    this._activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
      this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
      this._floorSpaceService.location = this.apigroup;
      this._floorSpaceService.load();
    });

    this._floorSpaceService.pipe(takeUntil(this._unsubscribeAll)).subscribe(({ loading, items }) => {
      this.loadTree = loading;
      this.dataTree = [];

      if (this.loadTree === false && this.layers.length) {
        const match = /^(.+)--/g.exec(this.layers[0]);
        this.dataTree = items;
        // this.dataTree = items.filter(item => item.uid.startsWith(match ? match[1] : this.layers[0]));
        
        this.selectedItems = this.layers.map((value) => ({ itemKey: value }));
        this.selectedLayers = this.selectedItems
          .map((item) => this.dataTree.find((i) => i.id === item.itemKey))
          .filter(Boolean)
          .sort((a, b) => this.selectedOrder.indexOf(a.type) - this.selectedOrder.indexOf(b.type));

        if (this.selectedLayers.length === 0 && this.dataTree && this.dataTree.length > 0) {
          this.selectFirst(this.dataTree);
        }
        if (this.selectedLayers.length && this.selectedLayers[0].plannerData) {
          this.selectedLayers[0].plannerData.entities.forEach((entity) => {
            if (entity.type === 'Room') {
              entity.visible = this.selectedLayers.some((item) => item.name === entity.name);
            }
          });

          this.editor.fromJSON(this.selectedLayers[0].plannerData || null);
        } else {
          this.editor.fromJSON(null);
        }
      }
    });
  }

  @HostListener("document:click", ["$event"])
  public onDocumentClick(event: any): void {
    if (this.overlay?.nativeElement.contains(event.target)) {
      this.showTree = false;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    const componentChanges = changes as PropertyMap<RoomPlannerViewerComponent, SimpleChange>;
    const locationApiGroupChange = componentChanges.locationApiGroup;
    const isValidChanges =
      locationApiGroupChange &&
      locationApiGroupChange.currentValue &&
      !_.isEqual(locationApiGroupChange.previousValue, locationApiGroupChange.currentValue);
    if (isValidChanges) {
      this.apigroup = locationApiGroupChange.currentValue;
      this._floorSpaceService.location = this.apigroup;
      this._floorSpaceService.load();
      return;
    }

    this.apigroup = this._locationService.locationDataAsKeyValue().currentLocation;
    this._floorSpaceService.location = this.apigroup;
    this._floorSpaceService.load();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();

    this.editor.dispose();
  }

  ngAfterViewInit(): void {
    this.editor.init(this.container.nativeElement);
  }

  /* */
  openTree() {
    //this.computeTree();
    this.showTree = !this.showTree;
  }

  closeTree() {
    this.showTree = false;
  }

  selectHandler(selectedItems): void {
    const otherLayers = selectedItems.filter((item) => {
      const first = selectedItems[0];
      const match = /^(.+)--/g.exec(first.itemKey);
      return !item.itemKey.startsWith(match ? match[1] : first.itemKey);
    });

    this.selectedItems = otherLayers.length ? otherLayers : selectedItems;
    this.selectedLayers = this.selectedItems
      .map((item) => this.dataTree.find((i) => i.id === item.itemKey))
      .filter(Boolean)
      .sort((a, b) => this.selectedOrder.indexOf(a.type) - this.selectedOrder.indexOf(b.type));

    if (this.selectedLayers.length && this.selectedLayers[0].plannerData) {
      this.selectedLayers[0].plannerData.entities.forEach((entity) => {
        if (entity.type === 'Room') {
          entity.visible = this.selectedLayers.some((item) => item.name === entity.name);
        }
      });

      this.editor.fromJSON(this.selectedLayers[0].plannerData || null);
    } else {
      this.editor.fromJSON(null);
    }
  }

  goTo3dView(): void {
    const firstSelectedLayer = _.first(this.selectedLayers);
    const floorName = firstSelectedLayer?.name || '';

    this._router.navigate(['/3d-view'], {
      queryParams: {
          loc: this.locationName,
          tenant: this.tenantName,
          name: floorName
      }
    });
  }

  private selectFirst(items: any[]): void {
    if (!items || items.length === 0) {
      return;
    }
    const firstId = items[0].id;
    const childrenItems = items.filter(item => item.parentId === firstId).map(item => {
      return {
        itemKey: item.id
      };
    });

    this.selectedItems = [{
      itemKey: items[0].id,
    }, ...childrenItems];

    this.selectedLayers = this.selectedItems
      .map((item) => items.find((i) => i.id === item.itemKey))
      .filter(Boolean)
      .sort((a, b) => this.selectedOrder.indexOf(a.type) - this.selectedOrder.indexOf(b.type));

    if (this.selectedLayers.length && this.selectedLayers[0].plannerData) {
      this.selectedLayers[0].plannerData.entities.forEach((entity) => {
        if (entity.type === 'Room') {
          entity.visible = this.selectedLayers.some((item) => item.name === entity.name);
        }
      });

      this.editor.fromJSON(this.selectedLayers[0].plannerData || null);
    } else {
      this.editor.fromJSON(null);
    }
  }
}
