import * as THREE from 'three';

import Dispatcher from './Dispatcher';

export default class Mouse extends Dispatcher {
    private _onDocumentMouseDown: (event) => void;
    private _onDocumentMouseMove: (event) => void;
    private _onDocumentMouseUp: (event) => void;
    private _onDocumentMouseCancel: (event) => void;
    private _onDocumentTouchStart: (event) => void;
    private _onDocumentTouchMove: (event) => void;
    private _onDocumentTouchEnd: (event) => void;

    /* */

    enabled = true;

    constructor(private domElement: HTMLCanvasElement) {
        super();

        this._onDocumentMouseDown = this.onDocumentMouseDown.bind(this);
        this._onDocumentMouseMove = this.onDocumentMouseMove.bind(this);
        this._onDocumentMouseUp = this.onDocumentMouseUp.bind(this);
        this._onDocumentMouseCancel = this.onDocumentMouseCancel.bind(this);
        this._onDocumentTouchStart = this.onDocumentTouchStart.bind(this);
        this._onDocumentTouchMove = this.onDocumentTouchMove.bind(this);
        this._onDocumentTouchEnd = this.onDocumentTouchEnd.bind(this);

        this.activate();
    }

    activate() {
        this.domElement.addEventListener('mousedown', this._onDocumentMouseDown, false);
        this.domElement.addEventListener('mousemove', this._onDocumentMouseMove, false);
        this.domElement.addEventListener('mouseup', this._onDocumentMouseUp, false);
        this.domElement.addEventListener('mouseleave', this._onDocumentMouseCancel, false);
        this.domElement.addEventListener('touchstart', this._onDocumentTouchStart, false);
        this.domElement.addEventListener('touchmove', this._onDocumentTouchMove, false);
        this.domElement.addEventListener('touchend', this._onDocumentTouchEnd, false);
    }

    deactivate() {
        this.domElement.removeEventListener('mousedown', this._onDocumentMouseDown, false);
        this.domElement.removeEventListener('mousemove', this._onDocumentMouseMove, false);
        this.domElement.removeEventListener('mouseup', this._onDocumentMouseUp, false);
        this.domElement.removeEventListener('mouseleave', this._onDocumentMouseCancel, false);
        this.domElement.removeEventListener('touchstart', this._onDocumentTouchStart, false);
        this.domElement.removeEventListener('touchmove', this._onDocumentTouchMove, false);
        this.domElement.removeEventListener('touchend', this._onDocumentTouchEnd, false);
    }

    dispose() {
        this.deactivate();
    }

    /* */

    private isPointerDown = false;
    private isPointerMove = false;

    private pointerDownMouse = new THREE.Vector2();
    private pointerMoveMouse = new THREE.Vector2();

    private clickCount = 0;
    private clickTimer = 0;
    private clickDelay = 300;

    private handlePointerDown(event) {
        if (!this.enabled) {
            return;
        }

        const rect = this.domElement.getBoundingClientRect();
        const mouse = new THREE.Vector2(
            ((event.clientX - rect.left) / rect.width) * 2 - 1,
            -((event.clientY - rect.top) / rect.height) * 2 + 1,
        );

        this.pointerDownMouse = mouse.clone();
        this.pointerMoveMouse = mouse.clone();

        this.dispatchEvent('pointerdown', {
            originalEvent: event,
            isPointerDown: this.isPointerDown,
            isPointerMove: this.isPointerMove,
            downMouse: this.pointerDownMouse,
            moveMouse: this.pointerMoveMouse,
            mouse,
        });

        this.isPointerDown = true;
        this.isPointerMove = false;
    }

    private handlePointerMove(event) {
        if (!this.enabled) {
            return;
        }

        const rect = this.domElement.getBoundingClientRect();
        const mouse = new THREE.Vector2(
            ((event.clientX - rect.left) / rect.width) * 2 - 1,
            -((event.clientY - rect.top) / rect.height) * 2 + 1,
        );

        this.pointerMoveMouse = mouse.clone();

        this.dispatchEvent('pointermove', {
            originalEvent: event,
            isPointerDown: this.isPointerDown,
            isPointerMove: this.isPointerMove,
            downMouse: this.pointerDownMouse,
            moveMouse: this.pointerMoveMouse,
            mouse,
        });

        this.isPointerMove = this.pointerDownMouse.clone().sub(this.pointerMoveMouse).length() > 0.001;

        if (this.isPointerDown) {
            this.dispatchEvent('pointerdrag', {
                originalEvent: event,
                isPointerDown: this.isPointerDown,
                isPointerMove: this.isPointerMove,
                downMouse: this.pointerDownMouse,
                moveMouse: this.pointerMoveMouse,
                mouse,
            });
        }
    }

    private handlePointerUp(event) {
        if (!this.enabled) {
            return;
        }

        const rect = this.domElement.getBoundingClientRect();
        const mouse = new THREE.Vector2(
            ((event.clientX - rect.left) / rect.width) * 2 - 1,
            -((event.clientY - rect.top) / rect.height) * 2 + 1,
        );

        this.dispatchEvent('pointerup', {
            originalEvent: event,
            isPointerDown: this.isPointerDown,
            isPointerMove: this.isPointerMove,
            downMouse: this.pointerDownMouse,
            moveMouse: this.pointerMoveMouse,
            mouse,
        });

        if (this.isPointerDown && !this.isPointerMove) {
            this.clickCount++;

            if (this.clickCount === 1) {
                this.clickTimer = window.setTimeout(() => { this.clickCount = 0; }, this.clickDelay);
                this.dispatchEvent('click', {
                    originalEvent: event,
                    isPointerDown: this.isPointerDown,
                    isPointerMove: this.isPointerMove,
                    downMouse: this.pointerDownMouse,
                    moveMouse: this.pointerMoveMouse,
                    mouse,
                });
            } else if (this.clickCount === 2) {
                clearTimeout(this.clickTimer);
                this.clickCount = 0;
                this.dispatchEvent('dblclick', {
                    originalEvent: event,
                    isPointerDown: this.isPointerDown,
                    isPointerMove: this.isPointerMove,
                    downMouse: this.pointerDownMouse,
                    moveMouse: this.pointerMoveMouse,
                    mouse,
                });
            }
        }

        this.isPointerDown = false;
        this.isPointerMove = false;
    }

    private handlePointerCancel(event) {
        this.isPointerDown = false;
        this.isPointerMove = false;
    }

    /* */

    private onDocumentMouseDown(event) {
        event.preventDefault();

        this.handlePointerDown(event);
    }

    private onDocumentMouseMove(event) {
        event.preventDefault();

        this.handlePointerMove(event);
    }

    private onDocumentMouseUp(event) {
        event.preventDefault();

        this.handlePointerUp(event);
    }

    private onDocumentMouseCancel(event) {
        event.preventDefault();

        this.handlePointerCancel(event);
    }

    private onDocumentTouchStart(event) {
        event.preventDefault();
        event = event.changedTouches[0];

        this.handlePointerDown(event);
    }

    private onDocumentTouchMove(event) {
        event.preventDefault();
        event = event.changedTouches[0];

        this.handlePointerMove(event);
    }

    private onDocumentTouchEnd(event) {
        event.preventDefault();
        event = event.changedTouches[0];

        this.handlePointerUp(event);
    }
}
