import { ReplaySubject } from 'rxjs';

export default class Dispatcher extends ReplaySubject<any> {
    private listeners = {};

    constructor() {
        super();
    }

    addEventListener(type, listener) {
        const listeners = this.listeners;

        if (listeners[type] === undefined) {
            listeners[type] = [];
        }

        if (listeners[type].indexOf(listener) === -1) {
            listeners[type].push(listener);
        }
    }

    hasEventListener(type, listener) {
        const listeners = this.listeners;
        return listeners[type] !== undefined && listeners[type].indexOf(listener) !== -1;
    }

    removeEventListener(type, listener) {
        const listeners = this.listeners;
        const listenerArray = listeners[type];

        if (listenerArray !== undefined) {
            const index = listenerArray.indexOf(listener);

            if (index !== -1) {
                listenerArray.splice(index, 1);
            }
        }
    }

    removeAllEventListener(type?) {
        if (type) {
            delete this.listeners[type];
        } else {
            this.listeners = {};
        }
    }

    dispatchEvent(type, event: any = {}) {
        const listeners = this.listeners;
        const listenerArray = listeners[type];

        if (listenerArray !== undefined) {
            event.type = type;
            event.target = this;

            // Make a copy, in case listeners are removed while iterating.
            const array = listenerArray.slice(0);

            for (let i = 0, l = array.length; i < l; i++) {
                array[i].call(this, event);
            }
        }
    }
}
