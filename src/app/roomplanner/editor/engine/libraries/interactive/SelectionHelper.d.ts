import { Vector2 } from 'three';

import { SelectionBox } from 'three/examples/jsm/interactive/SelectionBox';

export class SelectionHelper {
    constructor(selectionBox: SelectionBox, domElement: HTMLElement, cssClassName?: string);

    element: HTMLElement;
    isDown: boolean;
    pointBottomRight: Vector2;
    pointTopLeft: Vector2;
    domElement: HTMLElement;
    startPoint: Vector2;

    onSelectStart(event: Event): void;

    onSelectMove(event: Event): void;

    onSelectOver(event: Event): void;
}
