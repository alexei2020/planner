/* eslint-disable */

/**
 * @author HypnosNova / https://www.threejs.org.cn/gallery
 */

import { Vector2 } from 'three';

function SelectionHelper(selectionBox, domElement, cssClassName) {

    this.element = document.createElement('div');
    if (cssClassName) {
        this.element.classList.add(cssClassName);
    }
    this.element.style.position = 'absolute';
    this.element.style.backgroundColor = 'rgba(64, 136, 210, 0.2)';
    this.element.style.border = '1px solid rgba(64, 136, 210, 0.6)';
    this.element.style.pointerEvents = 'none';

    this.domElement = domElement;

    this.startPoint = new Vector2();
    this.pointTopLeft = new Vector2();
    this.pointBottomRight = new Vector2();

    this.isDown = false;

    this.domElement.addEventListener('mousedown', function (event) {

        if (!event.shiftKey) {

            this.isDown = false;
            this.onSelectOver(event);

            return;
        }

        this.isDown = true;
        this.onSelectStart(event);

    }.bind(this), false);

    this.domElement.addEventListener('mousemove', function (event) {

        if (!event.shiftKey) {

            this.isDown = false;
            this.onSelectOver(event);

            return;
        }

        if (this.isDown) {

            this.onSelectMove(event);

        }

    }.bind(this), false);

    this.domElement.addEventListener('mouseup', function (event) {

        this.isDown = false;
        this.onSelectOver(event);

    }.bind(this), false);

}

SelectionHelper.prototype.onSelectStart = function (event) {

    this.domElement.parentElement.appendChild(this.element);

    var rect = this.domElement.getBoundingClientRect();
    var mouse = new Vector2(
        event.clientX - rect.left,
        event.clientY - rect.top,
    );

    this.element.style.left = mouse.x + 'px';
    this.element.style.top = mouse.y + 'px';
    this.element.style.width = '0px';
    this.element.style.height = '0px';

    this.startPoint.x = mouse.x;
    this.startPoint.y = mouse.y;

};

SelectionHelper.prototype.onSelectMove = function (event) {

    var rect = this.domElement.getBoundingClientRect();
    var mouse = new Vector2(
        event.clientX - rect.left,
        event.clientY - rect.top,
    );

    this.pointBottomRight.x = Math.max(this.startPoint.x, mouse.x);
    this.pointBottomRight.y = Math.max(this.startPoint.y, mouse.y);
    this.pointTopLeft.x = Math.min(this.startPoint.x, mouse.x);
    this.pointTopLeft.y = Math.min(this.startPoint.y, mouse.y);

    this.element.style.left = this.pointTopLeft.x + 'px';
    this.element.style.top = this.pointTopLeft.y + 'px';
    this.element.style.width = (this.pointBottomRight.x - this.pointTopLeft.x) + 'px';
    this.element.style.height = (this.pointBottomRight.y - this.pointTopLeft.y) + 'px';

};

SelectionHelper.prototype.onSelectOver = function () {

    if (this.element.parentElement) {

        this.element.parentElement.removeChild(this.element);

    }

};

export { SelectionHelper };
