import * as THREE from 'three';

export default class Raycaster {
    public plane: THREE.Mesh;

    private camera;
    private domElement;

    private raycaster;

    constructor(camera, domElement) {
        this.camera = camera;
        this.domElement = domElement;

        this.raycaster = new THREE.Raycaster();

        this.plane = new THREE.Mesh(
            new THREE.PlaneBufferGeometry(10000, 10000, 1, 1),
            new THREE.MeshBasicMaterial({
                visible: false,
            }),
        );
        this.plane.rotation.x = -Math.PI / 2;
    }

    hit(event, object, recursive = true): THREE.Intersection[] {
        const pointer = event.changedTouches ? event.changedTouches[0] : event;

        const rect = this.domElement.getBoundingClientRect();

        const mouse = new THREE.Vector2(
            ((pointer.clientX - rect.left) / rect.width) * 2 - 1,
            -((pointer.clientY - rect.top) / rect.height) * 2 + 1,
        );

        const origin = new THREE.Vector3();
        const direction = new THREE.Vector3();

        let intersects = [];

        this.camera.updateMatrixWorld();

        if (this.camera instanceof THREE.PerspectiveCamera || this.camera.inPerspectiveMode) {
            origin.setFromMatrixPosition(this.camera.matrixWorld);
            direction.set(mouse.x, mouse.y, 0.5).unproject(this.camera).sub(origin).normalize();
        } else if (this.camera instanceof THREE.OrthographicCamera || this.camera.inOrthographicMode) {
            origin
                .set(mouse.x, mouse.y, (this.camera.near + this.camera.far) / (this.camera.near - this.camera.far))
                .unproject(this.camera);
            direction.set(0, 0, -1).transformDirection(this.camera.matrixWorld);
        } else {
            console.warn('Unsupported camera');
        }

        this.raycaster.set(origin, direction);

        if (Array.isArray(object)) {
            intersects = this.raycaster.intersectObjects(object, recursive);
        } else {
            intersects = this.raycaster.intersectObject(object, recursive);
        }

        return intersects;
    }
}
