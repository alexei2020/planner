import * as THREE from 'three';

import { OrbitControls } from './libraries/controls/OrbitControls';
import { TransformControls } from './libraries/controls/TransformControls';

import Config from './Config';
import Dispatcher from './Dispatcher';
import Mouse from './Mouse';

export default class Renderer extends Dispatcher {
    public static Anisotropy = Config.get('anisotropy');

    private container: HTMLDivElement | undefined;

    private width = 640;
    private height = 480;

    private isRendering = false;

    public renderer: THREE.WebGLRenderer;
    public pmremGenerator: THREE.PMREMGenerator;

    public scene: THREE.Scene;
    public camera: THREE.PerspectiveCamera;

    public orbit: OrbitControls;
    public transform: TransformControls;

    public light: any = {};

    public mouse: Mouse;

    public constructor() {
        super();

        this.scene = new THREE.Scene();
        this.scene.name = 'Scene';

        this.renderer = new THREE.WebGLRenderer({
            antialias: true,
        });
        this.renderer.sortObjects = true;
        this.renderer.physicallyCorrectLights = true;
        this.renderer.outputEncoding = THREE.sRGBEncoding;
        this.renderer.setClearColor(0xf3f2ef, 1);
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);

        this.pmremGenerator = new THREE.PMREMGenerator(this.renderer);
        this.pmremGenerator.compileEquirectangularShader();

        this.camera = new THREE.PerspectiveCamera(60, this.width / this.height, 0.1, 5000);
        this.camera.name = 'Camera';
        this.camera.position.set(5, 5, 5);
        this.camera.lookAt(this.scene.position);
        this.scene.add(this.camera);

        this.orbit = new OrbitControls(this.camera, this.renderer.domElement);
        this.orbit.maxPolarAngle = (Math.PI / 2) - 0.025;
        this.orbit.target.copy(this.scene.position);
        this.orbit.addEventListener('change', () => this.render());
        this.orbit.update();

        this.transform = new TransformControls(this.camera, this.renderer.domElement);
        this.transform.name = 'Transform';
        this.transform.setSpace('local');
        this.transform.addEventListener('dragging-changed', (event) => {
            this.orbit.enabled = !event.value;
            this.mouse.enabled = !event.value;
        });
        this.transform.addEventListener('change', () => this.render());
        this.scene.add(this.transform);

        this.light.ambient = new THREE.AmbientLight(0x404040, 0.6);
        this.light.ambient.name = 'AmbientLight';
        this.scene.add(this.light.ambient);
        this.light.hemisphere = new THREE.HemisphereLight(0xffffff, 0x444444);
        this.light.hemisphere.position.set(0, 5, 0);
        this.scene.add(this.light.hemisphere);
        this.light.directional = new THREE.DirectionalLight(0xffffff, 0.2);
        this.light.directional.name = 'DirectionalLight';
        this.light.directional.position.set(0.5, 1, 0.866).normalize().multiplyScalar(10);
        this.scene.add(this.light.directional);

        this.mouse = new Mouse(this.renderer.domElement);

        new THREE.TextureLoader().load('assets/roomplanner/editor/envs/env_1.jpg', (texture) => {
            this.scene.environment = this.pmremGenerator.fromEquirectangular(texture).texture;

            this.pmremGenerator.dispose();

            this.render();
        });

        window.addEventListener('resize', () => this.resize());

        //

        Renderer.Anisotropy = this.renderer.capabilities.getMaxAnisotropy();
    }

    public get domElement() {
        return this.renderer.domElement;
    }

    public init(container: HTMLDivElement) {
        this.container = container;
        this.container.appendChild(this.renderer.domElement);

        this.resize();
    }

    public resize(width = 0, height = 0) {
        if (!this.container) {
            return;
        }

        this.width = width || this.container.offsetWidth || window.innerWidth || 640;
        this.height = height || this.container.offsetHeight || window.innerHeight || 480;

        this.camera.aspect = this.width / this.height;
        this.camera.updateProjectionMatrix();

        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);

        this.dispatchEvent('resize');
        this.render();
    }

    public render(force = false) {
        if (!this.container) {
            return;
        }

        if (force) {
            this.dispatchEvent('update');

            this.renderer.render(this.scene, this.camera);
            this.isRendering = false;
            return;
        }

        if (!this.isRendering) {
            requestAnimationFrame(this.render.bind(this, true));
        }

        this.isRendering = true;
    }

    public dispose() {
        this.removeAllEventListener();

        this.orbit.dispose();
        this.mouse.dispose();
        this.transform.dispose();

        this.scene.traverse((child) => {
            // eslint-disable-next-line @typescript-eslint/dot-notation
            child['geometry']?.dispose();

            // eslint-disable-next-line @typescript-eslint/dot-notation
            (Array.isArray(child['material']) ? child['material'] : [child['material']]).filter(Boolean).forEach((material) => {
                Object.keys(material).forEach(key => key.toLowerCase().endsWith('map') && material[key]?.dispose());
                material.dispose();
            });
        });
        for (let i = this.scene.children.length - 1; i >= 0; i--) {
            this.scene.remove(this.scene.children[i]);
        }
        this.scene.dispose();

        this.renderer.dispose();

        this.container = null;

        this.isRendering = false;
    }
}
