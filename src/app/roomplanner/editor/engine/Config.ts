import * as THREE from 'three';
import get from 'lodash/get';

export class Config {
    private storage: any = {
        cache: {
            enabled: true,
        },
        anisotropy: 1,
    };

    constructor() {
        THREE.Cache.enabled = this.get('cache.enabled');
    }

    get(key) {
        return get(this.storage, key);
    }
}

export default new Config();
