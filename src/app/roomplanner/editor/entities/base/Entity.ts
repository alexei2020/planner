import * as THREE from 'three';

import Dispatcher from '../../engine/Dispatcher';

import MeasureManager from '../../managers/MeasureManager';
import EntityManager from '../../managers/EntityManager';
import InterfaceManager from '../../managers/InterfaceManager';

export default class Entity extends Dispatcher {
    public isEntity = true;
    public isInteractive = true;

    public id = THREE.MathUtils.generateUUID();
    public type: string;
    public name: string;
    public meta: any = {};
    public data: any = {};

    public object: THREE.Object3D = new THREE.Object3D();
    public userData: any = {};

    public snapTo = 'None';
    public measureManager: MeasureManager | null = null;
    public entityManager: EntityManager | null = null;
    public interfaceManager: InterfaceManager | null = null;

    public attachedTo: Entity | null = null;

    /* */

    private _isSelected = false;

    public get isSelected() {
        return this._isSelected;
    }

    public set isSelected(value) {
        this.setSelected(value);
    }

    public setSelected(value) {
        this._isSelected = value;

        this.object.traverse((child) => {
            (Array.isArray(child['material']) ? child['material'] : [child['material']]).filter(Boolean).forEach((material) => {
                if (material.emissive) {
                    material.emissive.setHex(this._isSelected ? 0xe51c1c : 0x000000);
                    material.emissiveMap = null;
                    material.emissiveIntensity = this._isSelected ? (this.type === 'EthernetWallJack' ? 0.75 : 0.25) : 1;

                    material.needsUpdate = true;
                }
            });
        });

        this.dispatchEvent('change');
    }

    /* */

    private _isHovered = false;

    public get isHovered() {
        return this._isHovered;
    }

    public set isHovered(value) {
        this.setHovered(value);
    }

    public setHovered(value) {
        this._isHovered = value;

        this.object.traverse((child) => {
            (Array.isArray(child['material']) ? child['material'] : [child['material']]).filter(Boolean).forEach((material) => {
                if (material.emissive) {
                    if (material.emissive.getHex() === 0xe51c1c) {
                        return;
                    }
                    material.emissive.setHex(this._isHovered ? 0x2ecc71 : 0x000000);
                    material.emissiveMap = null;
                    material.emissiveIntensity = this._isHovered ? 0.25 : 1;

                    material.needsUpdate = true;
                }
            });
        });

        if (this['_label']) {
            this['_label'].material.visible = this.isHovered;

            if (this.type !== 'Room') {
                const objectSize = this.boundingBox.getSize(new THREE.Vector3());

                this['_label'].center.set(0.5, 0);
                this['_label'].position.y = objectSize.y / 2;
                this['_label'].position.z = (-objectSize.z / 2) - 0.05;
            }
        }

        this.dispatchEvent('change');
    }

    /* */

    private _isVisible = true;

    public get visible(): boolean {
        return this._isVisible;
    }

    public set visible(value: boolean) {
        this.setVisible(value);
    }

    public setVisible(value: boolean) {
        this._isVisible = value;

        this.object.visible = this._isVisible;

        this.dispatchEvent('change');
    }

    /* */

    isSelectable = true;
    isRotatable = true;
    isScalable = true;
    isMeasured = true;

    params: any[] | null = null;

    /* */

    public get boundingBox() {
        /* eslint-disable */
        let boundingBox: THREE.Box3;
        if (this.object['geometry']) {
            if (this.object['geometry'].boundingBox == null) {
                this.object['geometry'].computeBoundingBox();
            }
            boundingBox = this.object['geometry'].boundingBox.clone();
        } else {
            boundingBox = new THREE.Box3().setFromObject(this.object);
        }
        /* eslint-enable */
        return boundingBox;
    }

    /* */

    public get position(): THREE.Vector3 {
        return this.object.position;
    }

    public set position(position: THREE.Vector3) {
        this.object.position.copy(position);

        this.dispatchEvent('change');
    }

    /* */

    setEntityManager(entityManager: EntityManager) {
        this.entityManager = entityManager;
    }

    /* */

    renderOnTop() {
        this.object.traverse((child) => {
            if (child instanceof THREE.Mesh) {
                (Array.isArray(child['material']) ? child['material'] : [child['material']]).filter(Boolean).forEach((material) => {
                    material.depthTest = false;
                    material.depthWrite = false;
                    material.originalTransparent = material.originalTransparent !== undefined ? material.originalTransparent : material.transparent;
                    material.transparent = true;
                });
                child.renderOrder = 100;
            }
        });

        this.dispatchEvent('change');
    }

    renderReset() {
        this.object.traverse((child) => {
            if (child instanceof THREE.Mesh) {
                (Array.isArray(child['material']) ? child['material'] : [child['material']]).filter(Boolean).forEach((material) => {
                    material.depthTest = true;
                    material.depthWrite = true;
                    material.transparent = material.originalTransparent || false;
                });
                child.renderOrder = 0;
            }
        });

        this.dispatchEvent('change');
    }

    /* */

    public update(camera): void {
    }

    /* */

    public addToScene(scene: THREE.Object3D) {
        scene.add(this.object);

        this.dispatchEvent('change');
    }

    public removeFromScene() {
        if (this.object.parent) {
            this.object.parent.remove(this.object);

            this.dispatchEvent('change');

            return true;
        }

        return false;
    }

    /* */

    public fromJSON(data) {
        this.data = data;

        this.id = data.id;
        this.type = data.type;
        this.name = data.name;
        this.meta = data.meta;
        this.visible = data.type === 'Corner' ? false : (data.type === 'Room' ? data.visible : true);

        if (data.position) {
            this.position = new THREE.Vector3().fromArray(data.position);
        }
        if (data.rotation) {
            this.object.quaternion.fromArray(data.rotation);
        }
        if (data.scale) {
            this.object.scale.fromArray(data.scale);
        }
    }

    public toJSON(): any {
        return {
            id: this.id,
            type: this.type,
            name: this.name,
            meta: this.meta,
            userData: {
                name: this.userData.name,
                type: this.userData.type,
            },
            attachedTo: this.attachedTo?.id,
            position: (this.attachedTo ? this.attachedTo.object.localToWorld(this.position.clone()) : this.position).toArray(),
            rotation: this.object.getWorldQuaternion(new THREE.Quaternion()).toArray(),
            scale: this.object.getWorldScale(new THREE.Vector3()).toArray(),
            visible: this.visible,
        };
    }

    public dispose() {
        this.removeFromScene();
        this.removeAllEventListener();
    }
}
