import Entity from './base/Entity';

export default class Furniture extends Entity {
    type = 'Furniture';
    name = 'Furniture';

    constructor() {
        super();

        this.object.name = this.name;
        this.object.userData = this;
    }
}
