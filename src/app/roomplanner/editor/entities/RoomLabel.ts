import * as THREE from 'three';

import Renderer from '../engine/Renderer';

import Entity from './base/Entity';

export default class RoomLabel extends Entity {
    type = 'RoomLabel';
    name = 'RoomLabel';

    isRotatable = false;
    isMeasured = false;

    object: THREE.Mesh;

    constructor() {
        super();

        // create canvas
        const width = 256;
        const height = 64;
        const aspectRatio = width / height;
        const pixelRatio = window.devicePixelRatio;

        const drawingCanvas = document.createElement('canvas');
        drawingCanvas.width = width * pixelRatio;
        drawingCanvas.height = height * pixelRatio;
        drawingCanvas.style.width = `${width}px`;
        drawingCanvas.style.height = `${height}px`;

        const drawingContext = drawingCanvas.getContext('2d') as CanvasRenderingContext2D;
        drawingContext.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);

        const drawingTexture = new THREE.CanvasTexture(drawingCanvas);
        drawingTexture.anisotropy = Renderer.Anisotropy;
        drawingTexture.needsUpdate = true;
        drawingTexture['aspectRatio'] = aspectRatio;
        drawingTexture['setText'] = (text) => {
            drawingTexture['text'] = text;

            drawingContext.clearRect(0, 0, width, height);

            drawingContext.fillStyle = '#585858';
            drawingContext.fillRect(0, 0, width, height);

            drawingContext.font = '42px Arial';
            drawingContext.textBaseline = 'middle';
            drawingContext.textAlign = 'center';
            drawingContext.fillStyle = '#ffffff';
            drawingContext.fillText(text, width / 2, height / 2 + 4);

            drawingTexture.needsUpdate = true;
        };
        drawingTexture['setText']('');

        const mat = new THREE.MeshStandardMaterial({ map: drawingTexture });
        const baseMaterial = new THREE.MeshStandardMaterial({ color: '#2d2d2d' });

        this.object = new THREE.Mesh<THREE.BoxGeometry, THREE.MeshStandardMaterial[]>(
            new THREE.BoxGeometry(1, 1, 0.01),
            [
                baseMaterial,
                baseMaterial,
                baseMaterial,
                baseMaterial,
                mat,
                baseMaterial,
            ],
        );
        this.object.geometry.translate(0, 0.5, 0);
        this.object.geometry.scale(0.11 * aspectRatio, 0.11, 1.0);
        this.object.name = this.name;
        this.object.userData = this;
    }
}
