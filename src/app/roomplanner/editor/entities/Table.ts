import Entity from './base/Entity';

export default class Table extends Entity {
    type = 'Table';
    name = 'Table';

    constructor() {
        super();

        this.object.name = this.name;
        this.object.userData = this;
    }

    public static intersectWithTable(intersect, target, hole = false) {
        const point = intersect.point.clone();
            // .sub(intersect.position.clone()
            //     .sub(intersect.offset.clone().setY(0).applyMatrix4(intersect.matrix))
            //     .negate());
        const normal = intersect.face.normal.clone();

        if (target.userData.userData?.snap?.to?.includes('Table')) {
            if (Math.abs(normal.y) === 1) {
                target.position.copy(point);
            }
        }
    }
}
