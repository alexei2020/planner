import * as THREE from 'three';

import Entity from './base/Entity';

export default class Corner extends Entity {
    type = 'Corner';
    name = 'Corner';

    isRotatable = false;
    isScalable = false;

    /* */

    public get position() {
        return this.object.position;
    }

    public set position(position: THREE.Vector3) {
        this.object.position.copy(position);

        this.dispatchEvent('positionChanged');
        this.dispatchEvent('change');
    }

    public constructor(position = new THREE.Vector3()) {
        super();

        this.object = new THREE.Mesh(
            new THREE.SphereBufferGeometry(0.1, 12, 12, 0, Math.PI),
            new THREE.MeshStandardMaterial({
                color: 0xff0000,
                depthTest: false,
                depthWrite: false,
            }),
        );
        this.object.name = this.name;
        this.object.rotation.x = -Math.PI / 2;
        this.object.renderOrder = 10;
        this.object.userData = this;

        this.position = position;
    }

    public fromJSON(data) {
        data.visible = false;

        super.fromJSON(data);
    }
}
