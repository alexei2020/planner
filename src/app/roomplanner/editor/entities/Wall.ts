import * as THREE from 'three';

import Renderer from '../engine/Renderer';

import Entity from './base/Entity';
import Corner from './Corner';
import Door from './Door';
import Room from './Room';

import ShapeUtils from '../utils/ShapeUtils';
import LoaderUtils from '../utils/LoaderUtils';

export default class Wall extends Entity {
    type = 'Wall';
    name = 'Wall';

    public entities: any[] = [];
    private holes: THREE.Vector2[][] = [];

    private _width = 0.1;
    private _height = 1;

    public object: THREE.Mesh;

    public room: Room | null = null;

    public render = this.onRender.bind(this);

    public flip: boolean = false;
    public texture: string = '';

    public constructor(private _startCorner: Corner, private _endCorner: Corner) {
        super();

        this.object = new THREE.Mesh(
            new THREE.Geometry(),
            [
                new THREE.MeshStandardMaterial({
                    color: 0xcccccc,
                    polygonOffset: true,
                    polygonOffsetFactor: -0.25,
                    polygonOffsetUnits: -1.0,
                }),
                new THREE.MeshStandardMaterial({
                    color: 0xcccccc,
                    polygonOffset: true,
                    polygonOffsetFactor: 0.25,
                    polygonOffsetUnits: 1.0,
                }),
                new THREE.MeshStandardMaterial({
                    color: 0xcccccc,
                }),
                new THREE.MeshStandardMaterial({
                    color: 0x262e39,
                    polygonOffset: true,
                    polygonOffsetFactor: -1.0,
                    polygonOffsetUnits: -4.0,
                }),
            ],
        );
        this.object.name = this.name;
        this.object.userData = this;

        this.startCorner = this._startCorner;
        this.endCorner = this._endCorner;
        this.render();
    }

    public get startPoint() {
        return this._startCorner.position;
    }

    public set startPoint(startPoint) {
        this._startCorner.position = startPoint;
        this.render();
    }

    public get endPoint() {
        return this._endCorner.position;
    }

    public set endPoint(endPoint) {
        this._endCorner.position = endPoint;
        this.render();
    }

    public get startCorner() {
        return this._startCorner;
    }

    public set startCorner(startCorner) {
        this._startCorner?.removeEventListener('positionChanged', this.render);
        this._startCorner = startCorner;
        this._startCorner.addEventListener('positionChanged', this.render);
        this.render();
    }

    public get endCorner() {
        return this._endCorner;
    }

    public set endCorner(endCorner) {
        this._endCorner?.removeEventListener('positionChanged', this.render);
        this._endCorner = endCorner;
        this._endCorner.addEventListener('positionChanged', this.render);
        this.render();
    }

    public get height() {
        return this._height;
    }

    public set height(height) {
        this._height = height;
        this.render();
    }

    public get width() {
        return this._width;
    }

    public set width(width) {
        this._width = width;
        this.render();
    }

    public get length() {
        return Math.max(this._startCorner.position.distanceTo(this._endCorner.position), 0.1);
    }

    /* */

    isRotatable = false;
    isScalable = false;

    private _userData: any = {};

    get userData() {
        return this._userData;
    }

    set userData(value) {
        this._userData = value;

        this.params = [
            {
                label: 'Flip texture',
                icon: 'flip',
                onClick: () => {
                    this.flip = !this.flip;
                    this.render();
                },
            },
            ...(this._userData.textures?.walls || []).map((texture) => {
                const onClick = () => {
                    if (this.texture === texture.label) {
                        return;
                    }

                    this.texture = texture.label;

                    if (texture.url) {
                        LoaderUtils.loadTexture(texture.url).then((texture) => {
                            texture.anisotropy = Renderer.Anisotropy;
                            texture.wrapS = THREE.RepeatWrapping;
                            texture.wrapT = THREE.RepeatWrapping;
                            texture.repeat.set(3, 3);

                            this.object.material[0].color.setHex(0xffffff);
                            this.object.material[0].map = texture.clone();
                            this.object.material[0].map.needsUpdate = true;
                            this.object.material[0].needsUpdate = true;

                            this.object.material[1].color.setHex(0xffffff);
                            this.object.material[1].map = texture.clone();
                            this.object.material[1].map.offset.set(0.02, -0.04);
                            this.object.material[1].map.needsUpdate = true;
                            this.object.material[1].needsUpdate = true;

                            this.dispatchEvent('change');
                        });
                    } else {
                        this.object.material[0].color.setHex(0xcccccc);
                        this.object.material[0].map = null;
                        this.object.material[0].needsUpdate = true;

                        this.object.material[1].color.setHex(0xcccccc);
                        this.object.material[1].map = null;
                        this.object.material[1].needsUpdate = true;

                        this.dispatchEvent('change');
                    }
                };

                if (texture.default) {
                    onClick();
                }

                return {
                    label: texture.label,
                    icon: texture.icon,
                    onClick,
                };
            }),
        ];
    }

    /* */

    private _showMeasure = false;

    public get showMeasure() {
        return this._showMeasure;
    }

    public set showMeasure(value) {
        this._showMeasure = value;

        this.measureManager.measureText(this.id, this._showMeasure ? this.length : null, this.startPoint.clone().lerp(this.endPoint, 0.5));
    }

    /* */

    private _position: THREE.Vector3 = new THREE.Vector3();

    public get position(): THREE.Vector3 {
        return this._position;
    }

    public set position(position: THREE.Vector3) {
        this._startCorner.position = this._startCorner.position.clone().sub(this._position.clone().sub(position));
        this._endCorner.position = this._endCorner.position.clone().sub(this._position.clone().sub(position));
        this._position.copy(position);

        this.render();
    }

    /* */

    private createGeometry(holes: THREE.Vector2[][]) {
        const shape = new THREE.Shape([
            new THREE.Vector2(0, 0),
            new THREE.Vector2(this.length, 0),
            new THREE.Vector2(this.length, this._height),
            new THREE.Vector2(0, this._height),
        ]);
        shape.holes = holes.map(hole => new THREE.Path(hole));

        const geometry = new THREE.ExtrudeGeometry(shape, {
            steps: 1,
            depth: 0.1,
            bevelEnabled: false,
        });
        geometry.faces.forEach((face) => {
            const v1 = geometry.vertices[face.a];
            const v2 = geometry.vertices[face.b];
            const v3 = geometry.vertices[face.c];

            const height = this._height - 0.01;

            if (v1.y >= height && v2.y >= height && v3.y >= height) {
                face.materialIndex = 3;
            } else if (this.flip) {
                if ((v1.z === 0 && v2.z === 0 && v3.z === 0) || (v1.y === 1 && v2.y === 1 && v3.y === 1)) {
                    face.materialIndex = 2;
                } else if (v1.z > 0 && v2.z > 0 && v3.z > 0) {
                    face.materialIndex = 0;
                } else {
                    face.materialIndex = 1;
                }
            } else if (v1.z === 0 && v2.z === 0 && v3.z === 0) {
                face.materialIndex = 0;
            } else if ((v1.z > 0 && v2.z > 0 && v3.z > 0) || (v1.y === 1 && v2.y === 1 && v3.y === 1)) {
                face.materialIndex = 2;
            } else {
                face.materialIndex = 1;
            }
        });
        geometry.translate(0, 0, -this._width / 2);
        geometry.rotateY(-Math.PI / 2);

        return geometry;
    }

    public addEntity(entity: any) {
        /* eslint-disable */
        const isUpdate = this.entities.some((e) => {
            if (e['uuid'] === entity['uuid']) {
                Object.assign(e, entity);
                return true;
            }
            return false;
        });
        if (!isUpdate) {
            this.entities.push(entity);
        }
        if (entity['target'].userData.type === 'RoomLabel') {
            entity['target'].userData.object.material[4].map.setText(this.room.name);
        }
        entity['target'].originalParent = entity['target'].originalParent || entity['target'].parent;
        entity['target'].userData.attachedTo = this;
        entity['target'].attachedTo = this;
        this.object.attach(entity['target']);
        this.render();
        /* eslint-enable */

        return entity;
    }

    public removeEntity(entity: any) {
        /* eslint-disable */
        if (entity['target'].userData.type === 'RoomLabel') {
            entity['target'].userData.object.material[4].map.setText('');
        }
        if (entity['target'].originalParent) {
            entity['target'].originalParent.attach(entity['target']);
            entity['target'].userData.lastAttachedTo = this;
            entity['target'].userData.attachedTo = null;
            entity['target'].lastAttachedTo = this;
            entity['target'].attachedTo = null;
        }

        this.entities = this.entities.filter(e => e['uuid'] !== entity['uuid']);
        this.render();
        /* eslint-enable */
    }

    public removeAllEntities() {
        if (!this.entities.length) {
            return;
        }

        this.entities.forEach(entity => this.removeEntity(entity));
        this.entities = [];
        this.render();
    }

    private onRender() {
        /* eslint-disable */
        const holes = this.entities.reduce((holes: THREE.Vector2[][], entity) => {
            if (entity['target'].userData.type === 'RoomLabel') {
                entity['target'].userData.object.material[4].map.setText(this.room.name);
            }

            if (!entity['hole']) {
                return holes;
            }

            entity['target'].updateMatrixWorld(false, false);
            entity['target'].geometry.computeBoundingBox();
            const boundingBox = entity['target'].geometry.boundingBox.clone().applyMatrix4(entity['target'].matrixWorld);
            const center = this.object.worldToLocal(boundingBox.getCenter(new THREE.Vector3()));

            // if (Math.abs(center.x) > 0.06) {
            //     return holes;
            // }

            const hole = [
                new THREE.Vector2((center.z - (0.0)) - (entity['width'] / 2), (center.y - (0.0)) - (entity['height'] / 2)),
                new THREE.Vector2((center.z - (0.0)) - (entity['width'] / 2), (center.y - (0.0)) + (entity['height'] / 2)),
                new THREE.Vector2((center.z - (0.0)) + (entity['width'] / 2), (center.y - (0.0)) + (entity['height'] / 2)),
                new THREE.Vector2((center.z - (0.0)) + (entity['width'] / 2), (center.y - (0.0)) - (entity['height'] / 2)),
            ];
            if (THREE.ShapeUtils.isClockWise(hole)) {
                hole.reverse();
            }

            if (
                !ShapeUtils.rectangleFit(hole, [
                    new THREE.Vector2(0, 0),
                    new THREE.Vector2(this.length, 0),
                    new THREE.Vector2(this.length, this._height),
                    new THREE.Vector2(0, this._height),
                ])
                || holes.some(h => ShapeUtils.rectangleIntersect(hole, h) || ShapeUtils.rectangleIntersect(h, hole))
            ) {
                return holes;
            }

            return [...holes, hole];
        }, []);

        this.object.geometry.dispose();
        this.object.geometry = this.createGeometry(holes);

        this.object.position.copy(this._startCorner.position);
        this.object.lookAt(this._endCorner.position);

        this.dispatchEvent('geometryChanged');
        this.dispatchEvent('change');
        /* eslint-enable */
    }

    public static intersectWithWall(intersect, target, hole = false) {
        const wall = intersect.object.userData;

        const point = intersect.point.clone()
            .sub(intersect.position.clone()
                .sub(intersect.offset.clone().setZ(0).applyMatrix4(intersect.matrix))
                .negate());
        const normal = intersect.face.normal.clone();

        if (target.userData.userData?.snap?.to?.includes('Wall')) {
            let snapEntity = false;

            if (Math.abs(normal.y) === 1) {
                point.setY(target.userData.userData?.snap?.height || 0);
                snapEntity = true;
            } else if (Math.abs(normal.y) === 0) {
                if (target.userData instanceof Door) {
                    point.setY(0);
                }
                snapEntity = true;
            }

            if (snapEntity) {
                if (point.y < 0.0001) {
                    point.y = 0.0001;
                }

                const product = normal.clone()
                    .transformDirection(intersect.object.matrixWorld)
                    .multiplyScalar(10)
                    .add(point);

                target.position.copy(point);

                if (Math.abs(normal.y) === 1) {
                    target.rotation.copy(wall.object.rotation);
                    target.rotation.y += Math.PI / 2;
                } else if (Math.abs(normal.y) === 0) {
                    target.lookAt(product);
                }

                if (hole) {
                    target.geometry.computeBoundingBox();

                    const geometryBbox = target.geometry.boundingBox;
                    const geometrySize = geometryBbox.getSize(new THREE.Vector3());

                    wall?.addEntity({
                        uuid: target.uuid,
                        target,
                        width: geometrySize.x * target.scale.x,
                        height: geometrySize.y * target.scale.y,
                        hole: target.userData.userData?.snap?.hole,
                    });
                }
            }
        }
    }

    dispose() {
        this.showMeasure = false;

        super.dispose();
    }

    fromJSON(data) {
        data.visible = true;

        super.fromJSON(data);

        this.width = data.width;
        this.height = data.height;
        this.flip = data.flip;
        this.texture = data.texture;
        this.showMeasure = data.showMeasure;

        this.params.find(param => param.label === this.texture)?.onClick();

        this.render();
    }

    toJSON() {
        return {
            ...super.toJSON(),

            position: null,
            rotation: null,
            scale: null,

            width: this.width,
            height: this.height,
            flip: this.flip,
            texture: this.texture,
            room: this.room.id,
            showMeasure: this.showMeasure,
            startCorner: this.startCorner.id,
            endCorner: this.endCorner.id,
            entities: this.entities.map(entity => entity.target.userData.id),
        };
    }
}
