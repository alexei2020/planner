import * as THREE from 'three';

import Entity from './base/Entity';

import LoaderUtils from '../utils/LoaderUtils';

export default class FloorPlan extends Entity {
    type = 'FloorPlan';
    name = 'FloorPlan';

    isLoaded = false;

    image: string = '';
    object: THREE.Mesh<THREE.PlaneGeometry, THREE.MeshBasicMaterial>;

    isSelectable = false;
    isRotatable = false;
    isMeasured = false;

    params = [
        {
            label: 'Remove from scene',
            icon: 'delete',
            onClick: () => {
                this.entityManager?.removeEntity(this);
            },
        },
    ];

    constructor() {
        super();

        this.object = new THREE.Mesh<THREE.PlaneGeometry, THREE.MeshBasicMaterial>(
            new THREE.PlaneGeometry(12, 12, 1, 1),
            new THREE.MeshBasicMaterial({
                color: 0xffffff,
            }),
        );
        this.object.name = this.name;
        this.object.position.y = -0.00175;
        this.object.rotation.x = -Math.PI / 2;
        this.object.renderOrder = -15;
        this.object.userData = this;
    }

    async loadImage(file: any) {
        if (!file) {
            return;
        }

        await this.setImage(await new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result as string);
            reader.onerror = err => reject(err);
        }));
    }

    async setImage(image) {
        this.reset();

        this.image = image;

        const texture = await LoaderUtils.loadTexture(this.image, false);

        this.object.material.map = texture;
        this.object.material.needsUpdate = true;
        this.object.scale.set(texture.image.width / texture.image.height, 1, 1);

        this.isLoaded = true;

        this.dispatchEvent('change');
    }

    fromJSON(data) {
        super.fromJSON(data);

        this.setImage(data.image);
    }

    toJSON() {
        return {
            ...super.toJSON(),

            image: this.image,
        };
    }

    reset() {
        this.isLoaded = false;

        this.image = '';

        this.object.material.map?.dispose();
        this.object.material.map = null;
        this.object.material.needsUpdate = true;
        this.object.scale.set(1, 1, 1);

        this.dispatchEvent('change');
    }
}
