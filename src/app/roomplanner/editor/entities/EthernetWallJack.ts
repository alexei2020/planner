import * as THREE from 'three';

import Entity from './base/Entity';

import IconUtils from '../utils/IconUtils';

export default class EthernetWallJack extends Entity {
    type = 'EthernetWallJack';
    name = 'EthernetWallJack';

    isRotatable = false;

    params = [
        {
            label: 'Edit label',
            icon: 'create',
            onClick: () => {
                const text = prompt('Enter wall jack name:', this._label.material.map.text || '');
                if (text && text.length >= 1 && text.length <= 12) {
                    this.setLabel(text);
                }

                this.dispatchEvent('change');
            },
        },
        {
            label: 'Show label',
            icon: 'visibility',
            onClick: () => {
                this._label.visible = !this._label.visible;

                this.isMeasured = !this._label.visible;

                setTimeout(() => {
                    this.params[1].label = this._label.visible ? 'Hide label' : 'Show label';
                    this.params[1].icon = this._label.visible ? 'visibility_off' : 'visibility';
                }, 250);

                this.dispatchEvent('change');
            },
        },
    ];

    /* */

    private _label;
    private _object: THREE.Object3D = new THREE.Object3D();

    get object() {
        return this._object;
    }

    set object(value) {
        this._object?.remove(this._label);

        this._object = value;
        this._object.userData = this;

        this._label = IconUtils.createDynamicIcon(() => this.dispatchEvent('change'));
        this._label.position.y += 0.01;
        this._label.visible = false;
        this.object.add(this._label);
    }

    /* */

    constructor() {
        super();

        this.object.name = this.name;
        this.object.userData = this;
    }

    setLabel(text) {
        this.name = text;

        this._label.material.map.setText(text);
        this._label.material.visible = this.isHovered;
        this._label.visible = !!text;

        this.isMeasured = !this._label.visible;

        setTimeout(() => {
            this.params[1].label = this._label.visible ? 'Hide Label' : 'Show Label';
            this.params[1].icon = this._label.visible ? 'visibility_off' : 'visibility';
        }, 250);

        this.dispatchEvent('change');
    }

    public update(camera) {
        super.update(camera);

        if (!this._label || !this._label.visible) {
            return;
        }

        const scaleX = 1 / this._object.scale.x;
        const scaleY = 1 / this._object.scale.y;
        const scaleZ = 1 / this._object.scale.z;

        if (camera.fov === 1) {
            this._label.scale.set(
                scaleX * (0.000525 * this._label.material.map.aspectRatio),
                scaleY * 0.000525,
                scaleY,
            );
        } else {
            this._label.scale.set(
                scaleX * (0.035 * this._label.material.map.aspectRatio),
                scaleY * 0.035,
                scaleZ,
            );
        }
    }

    fromJSON(data) {
        super.fromJSON(data);

        this.setLabel(this.name);
    }
}
