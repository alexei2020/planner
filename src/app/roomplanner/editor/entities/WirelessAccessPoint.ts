import * as THREE from 'three';

import Entity from './base/Entity';
import EthernetWallJack from './EthernetWallJack';

import EntityManager from '../managers/EntityManager';

import IconUtils from '../utils/IconUtils';

export default class WirelessAccessPoint extends Entity {
    type = 'WirelessAccessPoint';
    name = 'WirelessAccessPoint';

    jacks = [];

    params = [
        {
            id: 'attach_to_jack',
            label: 'Attach it to WallJack',
            icon: 'settings_ethernet',
            children: [],
            visible: false,
        },
        {
            label: 'Snap to Ceiling',
            icon: 'vertical_align_top',
            onClick: () => {
                this.snapTo = 'None';
            },
        },
        {
            label: 'Snap to Closest',
            icon: 'vertical_align_bottom',
            onClick: () => {
                this.snapTo = 'Closest';
            },
        },
    ];

    /* */

    setEntityManager(entityManager: EntityManager) {
        super.setEntityManager(entityManager);

        this.entityManager.addEventListener('entitiesChanged', () => {
            const param = this.params.find(param => param.id === 'attach_to_jack');
            if (param) {
                const jacks = this.entityManager.entities.filter(entity => entity instanceof EthernetWallJack) as EthernetWallJack[];

                this.jacks = this.jacks.filter(j => jacks.find(jack => jack.id === j.id));

                param.children = jacks.map(jack => ({
                    label: jack.name,
                    onClick: (event) => {
                        if (event.checked) {
                            if (this.jacks.length >= 2) {
                                event.source.toggle();
                                return;
                            }
                            this.jacks.push(jack);
                        } else {
                            this.jacks = this.jacks.filter(j => j.id !== jack.id);
                        }
                    },
                    checked: this.jacks.find(j => j.id === jack.id),
                }));
                param.visible = !!jacks.length;
            }
        });
    }

    /* */

    private _label;
    private _object: THREE.Object3D = new THREE.Object3D();

    get object() {
        return this._object;
    }

    set object(value) {
        this._object?.remove(this._label);

        this._object = value;
        this._object.userData = this;

        this._label = IconUtils.createDynamicIcon(() => this.dispatchEvent('change'));
        this._label.position.y += 0.01;
        this._label.visible = false;
        this.object.add(this._label);
    }

    /* */

    constructor() {
        super();

        this.object.name = this.name;
        this.object.userData = this;
    }

    public update(camera) {
        super.update(camera);

        if (!this._label || !this._label.visible) {
            return;
        }

        const scaleX = 1 / this._object.scale.x;
        const scaleY = 1 / this._object.scale.y;
        const scaleZ = 1 / this._object.scale.z;

        if (camera.fov === 1) {
            this._label.scale.set(
                scaleX * (0.000525 * this._label.material.map.aspectRatio),
                scaleY * 0.000525,
                scaleY,
            );
        } else {
            this._label.scale.set(
                scaleX * (0.035 * this._label.material.map.aspectRatio),
                scaleY * 0.035,
                scaleZ,
            );
        }
    }

    toJSON(): any {
        return {
            ...super.toJSON(),

            jacks: this.jacks,
        }
    }

    fromJSON(data) {
        super.fromJSON(data);

        this.jacks = data.jacks || [];
    }
}
