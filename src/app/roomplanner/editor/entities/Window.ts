import * as THREE from 'three';

import Entity from './base/Entity';

import LoaderUtils from '../utils/LoaderUtils';

export default class Window extends Entity {
    type = 'Window';
    name = 'Window';

    isRotatable = false;
    params = [];

    variant: string;

    private _userData: any = {};

    get userData() {
        return this._userData;
    }

    set userData(value) {
        this._userData = value;

        this.params = [
            ...(this._userData.models || []).map((model) => {
                const onClick = async () => {
                    this.variant = model.label;

                    if (!this.object.parent) {
                        return;
                    }

                    const wall = this.object['attachedTo'];
                    if (wall) {
                        wall.removeEntity({
                            uuid: this.object.uuid,
                            target: this.object,
                        });
                    }

                    const parent = this.object.parent.remove(this.object);
                    const position = this.object.position.clone();
                    const rotation = this.object.rotation.clone();
                    const scale = this.object.scale.clone();

                    this.object = await LoaderUtils.loadAuto(model.url, {
                        rotate: model.rotate,
                        offset: model.offset,
                        scale: model.scale,
                    });
                    this.object.position.copy(position);
                    this.object.rotation.copy(rotation);
                    this.object.scale.copy(scale);
                    this.object.userData = this;
                    parent.add(this.object);

                    if (wall) {
                        const target = this.object as THREE.Mesh<THREE.Geometry, THREE.Material>;

                        target.geometry.computeBoundingBox();

                        const geometryBbox = target.geometry.boundingBox;
                        const geometrySize = geometryBbox.getSize(new THREE.Vector3());

                        wall.addEntity({
                            uuid: target.uuid,
                            target,
                            width: geometrySize.x * target.scale.x,
                            height: geometrySize.y * target.scale.y,
                            hole: target.userData.userData?.snap?.hole,
                        });
                    }

                    this.dispatchEvent('change');
                };

                if (model.default) {
                    onClick();
                }

                return {
                    label: model.label,
                    icon: model.icon,
                    onClick,
                };
            }),
        ];
    }

    constructor() {
        super();

        this.object.name = this.name;
        this.object.userData = this;
    }

    public fromJSON(data) {
        super.fromJSON(data);

        this.variant = data.variant;

        // this.params.find(param => param.label === this.variant)?.onClick();
    }

    public toJSON() {
        return {
            ...super.toJSON(),

            variant: this.variant,
        };
    }
}
