import * as THREE from 'three';
import linePoint from 'intersects/line-point';
import polygonPoint from 'intersects/polygon-point';

import Renderer from '../engine/Renderer';

import Entity from './base/Entity';
import Corner from './Corner';
import Wall from './Wall';

import LoaderUtils from '../utils/LoaderUtils';
import ShapeUtils from '../utils/ShapeUtils';
import GeometryUtils from '../utils/GeometryUtils';
import IconUtils from '../utils/IconUtils';

export default class Room extends Entity {
    type = 'Room';
    name = 'Room';

    // Define ceiling and floor mesh
    public ceiling: THREE.Mesh<THREE.Geometry, THREE.MeshStandardMaterial>;
    public floor: THREE.Mesh<THREE.Geometry, THREE.MeshStandardMaterial>;

    // Define walls container
    public walls: Wall[];
    public texture: string = '';

    public isUpdateCornersGeometry = true;

    public updateCornersGeometry = this.onUpdateCornersGeometry.bind(this);
    public render = this.onRender.bind(this);

    private _label;

    static counter = 1;

    public constructor() {
        super();

        Room.counter += 1;

        this.object.name = this.name;
        this.object.userData = this;

        // Create ceiling mesh
        this.ceiling = new THREE.Mesh(
            new THREE.Geometry(),
            new THREE.MeshStandardMaterial({
                color: 0xffffff,
                transparent: true,
                opacity: 0.8,
                side: THREE.DoubleSide,
            }),
        );
        this.ceiling.name = 'Ceiling';
        this.ceiling.position.y = 0.999;
        this.ceiling.visible = false;
        this.ceiling.userData = this;
        this.object.add(this.ceiling);

        // Floor ceiling mesh
        this.floor = new THREE.Mesh(
            new THREE.Geometry(),
            new THREE.MeshStandardMaterial({
                color: 0xb8b8b8,
                transparent: true,
                opacity: 0.6,
                side: THREE.BackSide,
                polygonOffset: true,
                polygonOffsetFactor: -0.25,
                polygonOffsetUnits: -Room.counter,
            }),
        );
        this.floor.name = 'Floor';
        this.floor.position.y = -0.001;
        this.floor.renderOrder = -1;
        this.floor.userData = this;
        this.object.add(this.floor);

        // Create label
        this._label = IconUtils.createDynamicIcon(() => this.dispatchEvent('change'));
        this._label.visible = false;
        this.object.add(this._label);

        // Walls container
        this.walls = [];
    }

    /* */

    setLabel(text) {
        this.name = text;

        this._label.material.map.setText(text);
        this._label.material.visible = this.isHovered;
        this._label.visible = !!text;

        this.walls.forEach(wall => wall.render());

        setTimeout(() => {
            this.params[1].label = this._label.visible ? 'Hide Label' : 'Show Label';
            this.params[1].icon = this._label.visible ? 'visibility_off' : 'visibility';
        }, 250);

        this.dispatchEvent('change');
    }

    setSelected(value) {
        super.setSelected(value);

        if (this.entityManager) {
            this.entityManager.walls.forEach((wall) => {
                if (this._corners.includes(wall.startCorner) && this._corners.includes(wall.endCorner)) {
                    wall.isSelected = value;
                }
            });
        }
    }

    setHovered(value) {
        super.setHovered(value);

        if (this.entityManager) {
            this.entityManager.walls.forEach((wall) => {
                if (this._corners.includes(wall.startCorner) && this._corners.includes(wall.endCorner)) {
                    wall.isHovered = value;
                }
            });
        }
    }

    /* */

    isRotatable = false;

    private _userData: any = {};

    get userData() {
        return this._userData;
    }

    set userData(value) {
        this._userData = value;

        this.params = [
            {
                label: 'Edit label',
                icon: 'create',
                onClick: () => {
                    this.interfaceManager.floorSpaceService
                        .confirmDialog('Input room type and name:', this.meta)
                        .subscribe((response) => {
                            if (!response) {
                                return;
                            }

                            this.setLabel(response.name);
                            this.meta.type = response.type;
                            this.meta.name = response.name;
                        });
                },
            },
            {
                label: 'Show label',
                icon: 'visibility',
                onClick: () => {
                    this._label.visible = !this._label.visible;

                    setTimeout(() => {
                        this.params[1].label = this._label.visible ? 'Hide label' : 'Show label';
                        this.params[1].icon = this._label.visible ? 'visibility_off' : 'visibility';
                    }, 250);

                    this.dispatchEvent('change');
                },
            },
            {
                label: 'Flip texture',
                icon: 'flip',
                onClick: () => {
                    this.walls.forEach((wall) => {
                        wall.flip = !wall.flip;
                        wall.render();
                    });

                    this.dispatchEvent('change');
                },
            },
            ...(this._userData.textures?.walls || []).map((texture) => {
                const onClick = () => {
                    if (this.texture === texture.label) {
                        return;
                    }

                    this.texture = texture.label;

                    this.walls.forEach((wall) => {
                        wall.texture = texture.label;
                    });

                    if (texture.url) {
                        LoaderUtils.loadTexture(texture.url).then((texture) => {
                            texture.anisotropy = Renderer.Anisotropy;
                            texture.wrapS = THREE.RepeatWrapping;
                            texture.wrapT = THREE.RepeatWrapping;
                            texture.repeat.set(3, 3);

                            this.walls.forEach((wall) => {
                                wall.object.material[0].color.setHex(0xffffff);
                                wall.object.material[0].map = texture.clone();
                                wall.object.material[0].map.needsUpdate = true;
                                wall.object.material[0].needsUpdate = true;

                                wall.object.material[1].color.setHex(0xffffff);
                                wall.object.material[1].map = texture.clone();
                                wall.object.material[1].map.offset.set(0.02, -0.04);
                                wall.object.material[1].map.needsUpdate = true;
                                wall.object.material[1].needsUpdate = true;
                            });

                            this.dispatchEvent('change');
                        });
                    } else {
                        this.walls.forEach((wall) => {
                            wall.object.material[0].color.setHex(0xcccccc);
                            wall.object.material[0].map = null;
                            wall.object.material[0].needsUpdate = true;

                            wall.object.material[1].color.setHex(0xcccccc);
                            wall.object.material[1].map = null;
                            wall.object.material[1].needsUpdate = true;
                        });

                        this.dispatchEvent('change');
                    }
                };

                if (texture.default) {
                    onClick();
                }

                return {
                    label: texture.label,
                    icon: texture.icon,
                    onClick,
                };
            }),
            ...(this._userData.textures?.floor || []).map((texture) => {
                const onClick = () => {
                    this.texture = texture.label;

                    if (texture.url) {
                        LoaderUtils.loadTexture(texture.url).then((texture) => {
                            texture.anisotropy = Renderer.Anisotropy;
                            texture.wrapS = THREE.RepeatWrapping;
                            texture.wrapT = THREE.RepeatWrapping;
                            texture.needsUpdate = true;

                            this.floor.material.color.setHex(0xffffff);
                            this.floor.material.map = texture;
                            this.floor.material.visible = true;
                            this.floor.material.needsUpdate = true;

                            this.dispatchEvent('change');
                        });
                    } else if (texture.color) {
                        this.floor.material.color.setHex(texture.color);
                        this.floor.material.map = null;
                        this.floor.material.visible = true;
                        this.floor.material.needsUpdate = true;
                    } else {
                        this.floor.material.visible = false;
                    }

                    this.dispatchEvent('change');
                };

                if (texture.default) {
                    onClick();
                }

                return {
                    label: texture.label,
                    icon: texture.icon,
                    onClick,
                };
            }),
        ];
    }

    /* */

    private _height = 1;

    public get height() {
        return this._height;
    }

    public set height(height) {
        this._height = height;

        this.ceiling.position.y = height - 0.001;

        this.walls.forEach((wall) => {
            wall.height = height;
        });

        this.render();
    }

    /* */

    private _corners: Corner[] = [];

    public get corners(): Corner[] {
        return this._corners;
    }

    public set corners(corners: Corner[]) {
        this.walls.forEach(wall => wall.dispose());
        this.walls = [];

        this._corners.forEach(corner => corner.removeEventListener('positionChanged', this.render));
        this._corners = corners;
        this._corners.forEach(corner => corner.addEventListener('positionChanged', this.render));

        this.render();
    }

    /* */

    private _position: THREE.Vector3 = new THREE.Vector3();

    public get position(): THREE.Vector3 {
        return this._position;
    }

    public set position(position: THREE.Vector3) {
        this._corners.forEach((corner) => {
            corner.position = corner.position.clone().sub(this._position.clone().sub(position));
        });
        this._position.copy(position);

        this.render();
    }

    /* */

    public get center() {
        return new THREE.Box3().setFromObject(this.object).getCenter(new THREE.Vector3()).setY(0);
    }

    /* */

    private _showMeasure = false;

    public get showMeasure() {
        return this._showMeasure;
    }

    public set showMeasure(value) {
        this._showMeasure = this.visible && value;

        this.walls.forEach((wall, i) => {
            wall.showMeasure = this._showMeasure;
        });
    }

    private onUpdateCornersGeometry(force = true) {
        if (!this.isUpdateCornersGeometry) {
            return;
        }

        this.walls.forEach((wall, i) => {
            const next = this.walls[i + 1] || this.walls[0];

            if (wall.visible && wall.room?.visible && next.visible && next.room?.visible) {
                GeometryUtils.mergeWalls(wall, next);
            }
        });

        this.dispatchEvent('change');

        if (force) {
            setTimeout(() => this.onUpdateCornersGeometry(false), 16);
        }
    }

    private onRender() {
        // Create wall from corners
        this.corners.forEach((corner, i, corners) => {
            const next = corners[i + 1] || (corners.length >= 3 && corners[0]);
            if (!next) {
                return;
            }

            if (this.walls[i]) {
                if (this.walls[i].startCorner !== corner) {
                    this.walls[i].startCorner = corner;
                } else if (this.walls[i].endCorner !== next) {
                    this.walls[i].endCorner = next;
                }
            } else {
                const wall = new Wall(corner, next);
                wall.room = this;
                wall.measureManager = this.measureManager;
                wall.interfaceManager = this.interfaceManager;
                wall.setEntityManager(this.entityManager);
                wall.showMeasure = this.showMeasure;
                wall.userData = this.userData;
                wall.addEventListener('geometryChanged', () => this.updateCornersGeometry());
                wall.addEventListener('change', () => this.dispatchEvent('change'));
                wall.addToScene(this.object);
                this.walls.push(wall);

                this.updateCornersGeometry();
            }
        });

        // Create geometry from corners
        const geometry = ShapeUtils.createGeometry(this._corners.map(corner => corner.position));

        // Apply geometry to ceiling
        this.ceiling.geometry.dispose();
        this.ceiling.geometry = geometry.clone();

        // Apply geometry to floor
        this.floor.geometry.dispose();
        this.floor.geometry = geometry.clone();

        this._label.position.copy(this.center);

        this.dispatchEvent('change');
    }

    /* */

    public update(camera) {
        super.update(camera);

        if (!this._label || !this._label.visible) {
            return;
        }

        const scaleX = 1; // / this._object.scale.x;
        const scaleY = 1; // / this._object.scale.y;
        const scaleZ = 1; // / this._object.scale.z;

        if (camera.fov === 1) {
            this._label.scale.set(
                scaleX * (0.000525 * this._label.material.map.aspectRatio),
                scaleY * 0.000525,
                scaleY,
            );
        } else {
            this._label.scale.set(
                scaleX * (0.035 * this._label.material.map.aspectRatio),
                scaleY * 0.035,
                scaleZ,
            );
        }
    }

    public dispose() {
        this.setSelected(false);
        this.setHovered(false);

        this.walls.forEach(wall => wall.dispose());
        // this.corners.forEach(corner => corner.dispose());

        super.dispose();
    }

    public fromJSON(data) {
        super.fromJSON(data);

        this.showMeasure = data.showMeasure;
        this.height = data.height;
        this.texture = data.texture;

        this.params.find(param => param.label === this.texture)?.onClick();

        this.setLabel(this.name);

        this.render();
    }

    public toJSON() {
        return {
            ...super.toJSON(),

            position: null,
            rotation: null,
            scale: null,

            showMeasure: this.showMeasure,
            height: this.height,
            texture: this.texture,

            corners: this.corners.map(corner => corner.id),
            walls: this.walls.map(wall => wall.id),
        };
    }

    /* */

    public static mergeRooms(entityManager, force = true) {
        const tolerance = 0.01;

        entityManager.rooms.forEach((room) => {
            const roomCorners = room.corners.slice();
            const roomWalls = room.walls.slice();

            roomWalls.forEach((wall) => {
                const roomCornerIndex = roomCorners.findIndex(corner => corner === wall.startCorner) + 1;

                entityManager.corners
                    .filter((corner) => {
                        const isIntersect = linePoint(
                            wall.startCorner.position.x, wall.startCorner.position.z,
                            wall.endCorner.position.x, wall.endCorner.position.z,
                            corner.position.x, corner.position.z,
                            tolerance,
                        );
                        return isIntersect && (wall.startCorner.position.distanceTo(corner.position) > tolerance
                            && wall.endCorner.position.distanceTo(corner.position) > tolerance);
                    })
                    .sort((a, b) => a.position.distanceTo(wall.startCorner.position) - b.position.distanceTo(wall.startCorner.position))
                    .forEach((corner, i) => {
                        roomCorners.splice(roomCornerIndex + i, 0, corner);
                        room.corners = roomCorners;
                    });
            });
        });

        entityManager.walls.forEach((wall, i, walls) => {
            wall.visible = true;

            const wallIntersects = walls.filter(w =>
                (w.startCorner.position.distanceTo(wall.startCorner.position) < tolerance
                    || w.startCorner.position.distanceTo(wall.endCorner.position) < tolerance)
                && (w.endCorner.position.distanceTo(wall.startCorner.position) < tolerance
                    || w.endCorner.position.distanceTo(wall.endCorner.position) < tolerance),
            );
            if (wallIntersects.length >= 2) {
                const wall = wallIntersects.find(wall => wall.visible && wall.room?.visible) || wallIntersects[0];
                const entities = wallIntersects
                    .reduce((entities, w) => {
                        const acc = [...entities, ...w.entities];

                        if (w !== wall) {
                            w.removeAllEntities();
                            w.visible = false;
                        }

                        return acc;
                    }, [])
                    .map(entity => wall.addEntity(entity));

                wall.params.find(param => param.label === wall.texture)?.onClick();
                wall.visible = true;
                wall.render();
            }
        });

        entityManager.entities.forEach((entity) => {
            if (['Corner', 'Wall', 'Room'].includes(entity.type)) {
                return;
            }

            entity.visible = false;
        });

        entityManager.rooms.forEach((room) => {
            if (room.visible) {
                entityManager.entities.forEach((entity) => {
                    if (['Corner', 'Wall', 'Room'].includes(entity.type)) {
                        return;
                    }

                    if (entity.object.parent) {
                        entity.object.parent.updateMatrixWorld();
                    }
                    entity.object.updateMatrixWorld();

                    const entityPosition = new THREE.Vector3().setFromMatrixPosition(entity.object.matrixWorld);

                    const isInside = polygonPoint(
                        room.corners.reduce((points, corner) => [...points, corner.position.x, corner.position.z], []),
                        entityPosition.x, entityPosition.z,
                        0.1,
                    );
                    if (isInside) {
                        entity.visible = isInside;
                    }
                });
            }
        });

        if (force) {
            setTimeout(() => Room.mergeRooms(entityManager, false), 250);
        }
    }
}
