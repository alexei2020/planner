import * as THREE from 'three';
import Polygon from 'polygon';

export default class ShapeUtils {
    private static defineRectBoundaries(rectangle: THREE.Vector2[], paddingX = 0, paddingY = 0) {
        // define X and Y boundaries
        const boundaries = rectangle.reduce((acc, point, iteration) => {
            if (iteration === 0) {
                acc.minX = point.x;
                acc.maxX = point.x;
                acc.minY = point.y;
                acc.maxY = point.y;
            } else {
                if (point.x > acc.maxX) {
                    acc.maxX = point.x;
                }
                if (point.x < acc.minX) {
                    acc.minX = point.x;
                }
                if (point.y > acc.maxY) {
                    acc.maxY = point.y;
                }
                if (point.y < acc.minY) {
                    acc.minY = point.y;
                }
            }
            return acc;
        }, {
            minX: 0,
            maxX: 0,
            minY: 0,
            maxY: 0,
        });

        // apply padding to boundaries
        boundaries.minX += paddingX;
        boundaries.maxX -= paddingX;
        boundaries.minY += paddingY;
        boundaries.maxY -= paddingY;

        return boundaries;
    }

    public static rectangleFit(rect1: THREE.Vector2[], rect2: THREE.Vector2[], paddingX = 0, paddingY = 0): boolean {
        const boundaries = ShapeUtils.defineRectBoundaries(rect2, paddingX, paddingY);

        // check the boundaries and return result
        // returns true if all points of rect1 are inside of rect2 boundaries
        return !rect1.some(point => (
            point.x > boundaries.maxX
            || point.x < boundaries.minX
            || point.y > boundaries.maxY
            || point.y < boundaries.minY
        ));
    }

    public static rectangleIntersect(rect1: THREE.Vector2[], rect2: THREE.Vector2[], paddingX = 0, paddingY = 0): boolean {
        const boundaries = ShapeUtils.defineRectBoundaries(rect2, paddingX, paddingY);

        // check the boundaries and return result
        // returns true if at least one point of rect1 is inside of rect2 boundaries
        return rect1.some(point => (
            point.x >= boundaries.minX
            && point.x <= boundaries.maxX
            && point.y >= boundaries.minY
            && point.y <= boundaries.maxY
        ));
    }

    public static createGeometry(vertices: THREE.Vector3[]): THREE.Geometry {
        const polygon = new Polygon(vertices.map(vertex => new THREE.Vector2(vertex.x, vertex.z))).clean();
        const intersect = polygon.selfIntersections();
        if (intersect.length) {
            return new THREE.Geometry();
        }

        const contour = polygon.points.map(point => new THREE.Vector2(point.x, point.y));
        if (!THREE.ShapeUtils.isClockWise(contour)) {
            contour.reverse();
        }

        const faces = THREE.ShapeUtils.triangulateShape(contour, []);
        if (!faces.length || faces.length < Math.floor(contour.length / 2)) {
            return new THREE.Geometry();
        }

        const geometry = new THREE.Geometry();
        geometry.vertices = contour.map(point => new THREE.Vector3(point.x, 0, point.y));
        geometry.faces = faces.map(face => new THREE.Face3(face[0], face[1], face[2]));
        geometry.faceVertexUvs = [
            geometry.faces.map(face => [
                new THREE.Vector2(geometry.vertices[face.a].x, geometry.vertices[face.a].z),
                new THREE.Vector2(geometry.vertices[face.b].x, geometry.vertices[face.b].z),
                new THREE.Vector2(geometry.vertices[face.c].x, geometry.vertices[face.c].z),
            ]),
        ];
        geometry.computeFaceNormals();
        return geometry;
    }

    public static computeCentroid(vertices) {
        const polygon = new Polygon(vertices.map(vertex => new THREE.Vector2(vertex.x, vertex.z))).clean();
        const centroid = polygon.center();
        return new THREE.Vector3(centroid.x, 0, centroid.y);
    }
}
