import * as THREE from 'three';

import Renderer from '../engine/Renderer';

export default class IconUtils {
    static createDynamicIcon(render) {
        const width = 256;
        const height = 64;
        const aspectRatio = width / height;
        const pixelRatio = window.devicePixelRatio;

        const drawingCanvas = document.createElement('canvas');
        drawingCanvas.width = width * pixelRatio;
        drawingCanvas.height = height * pixelRatio;
        drawingCanvas.style.width = `${width}px`;
        drawingCanvas.style.height = `${height}px`;

        const drawingContext = drawingCanvas.getContext('2d') as CanvasRenderingContext2D;
        drawingContext.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);

        const drawingTexture = new THREE.CanvasTexture(drawingCanvas);
        // drawingTexture.minFilter = THREE.NearestFilter;
        // drawingTexture.magFilter = THREE.NearestFilter;
        drawingTexture.anisotropy = Renderer.Anisotropy;
        drawingTexture.needsUpdate = true;
        /* eslint-disable */
        drawingTexture['aspectRatio'] = aspectRatio;
        drawingTexture['setText'] = (text) => {
            drawingTexture['text'] = text;

            drawingContext.clearRect(0, 0, width, height);

            drawingContext.font = '36px Arial';
            drawingContext.textBaseline = 'middle';
            drawingContext.textAlign = 'center';

            const textWidth = Math.min(drawingContext.measureText(text).width, width);

            const rectX = ((width - textWidth) / 2) - 8;
            const rectY = 0;
            const rectWidth = textWidth + 16;
            const rectHeight = height;
            const rectRadius = 9;
            drawingContext.lineJoin = 'round';
            drawingContext.lineWidth = 9;
            drawingContext.strokeStyle = '#e74c3c';
            drawingContext.fillStyle = '#e74c3c';
            drawingContext.strokeRect(
                rectX + (rectRadius / 2),
                rectY + (rectRadius / 2),
                rectWidth - rectRadius,
                rectHeight - rectRadius,
            );
            drawingContext.fillRect(
                rectX + (rectRadius / 2),
                rectY + (rectRadius / 2),
                rectWidth - rectRadius,
                rectHeight - rectRadius,
            );

            drawingContext.fillStyle = '#ffffff';
            drawingContext.fillText(text, width / 2, height / 2 + 2);

            drawingTexture.needsUpdate = true;

            render();
        };
        /* eslint-enable */

        const icon = new THREE.Sprite(
            new THREE.SpriteMaterial({
                map: drawingTexture,
                side: THREE.DoubleSide,
                sizeAttenuation: false,
                depthTest: false,
                depthWrite: false,
                transparent: true,
                fog: false,
            }),
        );
        icon.renderOrder = 1001;
        icon.name = 'scaleInfo';
        icon.scale.set(0.035 * aspectRatio, 0.035, 1.0);
        return icon;
    }
}
