import * as THREE from 'three';

export default class VectorUtils {
    public static equalVectors(v1: THREE.Vector3, v2: THREE.Vector3, tolerance: number = 1e-4) {
        return (Math.abs(v1.clone().x - v2.clone().x) <= tolerance)
            && (Math.abs(v1.clone().y - v2.clone().y) <= tolerance)
            && (Math.abs(v1.clone().z - v2.clone().z) <= tolerance);
    }

    public static alignPoints(target: THREE.Vector3, base: THREE.Vector3): THREE.Vector3 {
        const wallSnapPos1 = base.clone().setX(target.x);
        const wallSnapPos2 = base.clone().setZ(target.z);
        const wallSnapDist1 = target.distanceTo(wallSnapPos1);
        const wallSnapDist2 = target.distanceTo(wallSnapPos2);

        if (wallSnapDist1 < wallSnapDist2) {
            return target.clone().setZ(wallSnapPos1.z);
        } else {
            return target.clone().setX(wallSnapPos2.x);
        }
    }
}
