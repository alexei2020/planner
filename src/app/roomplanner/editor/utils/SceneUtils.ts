import * as THREE from 'three';

export default class SceneUtils {
    static traverse(object: THREE.Object3D, iterator: (child: THREE.Object3D) => void) {
        object.traverse(iterator);
    }

    static traverseMap(object: THREE.Object3D, iterator: (child: THREE.Object3D) => any[]) {
        const map: any[] = [];
        object.traverse((child) => {
            map.push(iterator(child));
        });
        return map;
    }

    static traverseReduce(object: THREE.Object3D, iterator: (state: any, child: THREE.Object3D) => any, initialState: any) {
        let state = initialState;
        object.traverse((child) => {
            state = iterator(state, child);
        });
        return state;
    }

    static traverseFilter(object: THREE.Object3D, iterator: (child: THREE.Object3D) => boolean) {
        const map: THREE.Object3D[] = [];
        object.traverse((child) => {
            if (iterator(child)) {
                map.push(child);
            }
        });
        return map;
    }

    static toScreenPosition(obj, camera, renderer) {
        const widthHalf = 0.5 * renderer.getContext().canvas.width;
        const heightHalf = 0.5 * renderer.getContext().canvas.height;

        obj.updateMatrixWorld();

        const vector = new THREE.Vector3().setFromMatrixPosition(obj.matrixWorld).project(camera);
        vector.x = Math.round(((vector.x + 1) * widthHalf) / window.devicePixelRatio);
        vector.y = Math.round(((-vector.y + 1) * heightHalf) / window.devicePixelRatio);

        // Behind camera
        if (vector.z > 1) {
            vector.x = -1000;
            vector.y = -1000;
        }

        return new THREE.Vector2(vector.x, vector.y);
    }
}
