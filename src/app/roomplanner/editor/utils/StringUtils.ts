export default class StringUtils {
    static normalize(text) {
        return text.slice(0, 63)
            .toLowerCase()
            .replace(/[^a-z0-9-]+/g, '-')
            .replace(/-+/g, '-')
            .replace(/-$/g, '')
            .replace(/^-/g, '');
    }
}
