import * as THREE from 'three';

import Wall from '../entities/Wall';

import VectorUtils from './VectorUtils';
import MathUtils from './MathUtils';

interface EdgesMap {
    leftStart: null | THREE.Vector3;
    leftEnd: null | THREE.Vector3;
    rightStart: null | THREE.Vector3;
    rightEnd: null | THREE.Vector3;
}

interface IntersectionPoint {
    x: null | number;
    y: null | number;
    onLine1: boolean;
    onLine2: boolean;
}

export default class GeometryUtils {
    public static mergeWalls(wall1: Wall, wall2: Wall, snapThreshold: number = 0.25) {
        // check if both walls were passed, otherwise exit
        if (!wall1 || !wall2) return;

        // check if walls geometry is instance of THREE.Geometry, thus able to return vertices
        if (!(wall1.object.geometry instanceof THREE.Geometry) || !(wall2.object.geometry instanceof THREE.Geometry)) return;

        // define edges boundaries for both walls
        wall1.object.updateWorldMatrix(true, true);
        wall2.object.updateWorldMatrix(true, true);

        // define walls edges
        const wall1Edges: EdgesMap = GeometryUtils.defineWallEdges(wall1);
        const wall2Edges: EdgesMap = GeometryUtils.defineWallEdges(wall2);

        // check if all wall1 edges values are in place for calculation
        if (!wall1Edges.leftEnd || !wall1Edges.leftStart || !wall1Edges.rightEnd || !wall1Edges.rightStart) return;

        // check if all wall2 edges values are in place for calculation
        if (!wall2Edges.leftEnd || !wall2Edges.leftStart || !wall2Edges.rightEnd || !wall2Edges.rightStart) return;

        // define shortcuts and placeholders
        let globalVertex: THREE.Vector3;
        const wall1Vertices: THREE.Vector3[] = wall1.object.geometry.vertices;
        const wall2Vertices: THREE.Vector3[] = wall2.object.geometry.vertices;
        let intersectionLeft: undefined | THREE.Vector3;
        let intersectionRight: undefined | THREE.Vector3;

        // snap point end-to-start case
        if (VectorUtils.equalVectors(wall1.endPoint, wall2.startPoint)) {
            // check if points are on same line
            // further flow is to be skipped in this case
            if (GeometryUtils.threePointsLieOnLine(wall1.startPoint, wall1.endPoint, wall2.endPoint)) {
                return;
            }

            // find left intersection
            const intersectionLeft2D = GeometryUtils.checkLineIntersection(
                wall1Edges.leftStart.x,
                wall1Edges.leftStart.z,
                wall1Edges.leftEnd.x,
                wall1Edges.leftEnd.z,
                wall2Edges.leftEnd.x,
                wall2Edges.leftEnd.z,
                wall2Edges.leftStart.x,
                wall2Edges.leftStart.z,
            );

            // find right intersection
            const intersectionRight2D = GeometryUtils.checkLineIntersection(
                wall1Edges.rightStart.x,
                wall1Edges.rightStart.z,
                wall1Edges.rightEnd.x,
                wall1Edges.rightEnd.z,
                wall2Edges.rightEnd.x,
                wall2Edges.rightEnd.z,
                wall2Edges.rightStart.x,
                wall2Edges.rightStart.z,
            );

            // check if all required intersection points were found
            if (intersectionLeft2D.x === null
                || intersectionLeft2D.y === null
                || intersectionRight2D.x === null
                || intersectionRight2D.y === null) {
                return;
            }

            // translate intersections to proper global coordinates
            intersectionLeft = new THREE.Vector3(intersectionLeft2D.x, 0, intersectionLeft2D.y);
            intersectionRight = new THREE.Vector3(intersectionRight2D.x, 0, intersectionRight2D.y);

            // check if snap point is not too far
            if (intersectionLeft.distanceTo(wall1.endPoint) > wall1.length * snapThreshold
                && intersectionLeft.distanceTo(wall2.startPoint) > wall2.length * snapThreshold) {
                return;
            }

            // deform wall1
            for (let i = 0, l = wall1Vertices.length; i < l; i++) {
                globalVertex = wall1.object.localToWorld(wall1Vertices[i].clone());

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.leftEnd)) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionLeft.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.leftEnd.clone().setY(wall1.height))) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionLeft.clone().setY(wall1.height)));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.rightEnd)) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionRight.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.rightEnd.clone().setY(wall1.height))) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionRight.clone().setY(wall1.height)));
                }
            }

            // deform wall2
            for (let i = 0, l = wall2Vertices.length; i < l; i++) {
                globalVertex = wall2.object.localToWorld(wall2Vertices[i].clone());

                // noinspection DuplicatedCode
                if (VectorUtils.equalVectors(globalVertex, wall2Edges.leftStart)) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionLeft.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.leftStart.clone().setY(wall2.height))) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionLeft.clone().setY(wall2.height)));
                }

                // noinspection DuplicatedCode
                if (VectorUtils.equalVectors(globalVertex, wall2Edges.rightStart)) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionRight.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.rightStart.clone().setY(wall2.height))) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionRight.clone().setY(wall2.height)));
                }
            }
        }

        // snap point start-to-end case
        if (VectorUtils.equalVectors(wall1.startPoint, wall2.endPoint)) {
            // check if points are on same line
            // further flow is to be skipped in this case
            if (GeometryUtils.threePointsLieOnLine(wall2.startPoint, wall1.startPoint, wall1.endPoint)) {
                return;
            }

            // find left intersection
            const intersectionLeft2D = GeometryUtils.checkLineIntersection(
                wall1Edges.rightEnd.x,
                wall1Edges.rightEnd.z,
                wall1Edges.rightStart.x,
                wall1Edges.rightStart.z,
                wall2Edges.rightStart.x,
                wall2Edges.rightStart.z,
                wall2Edges.rightEnd.x,
                wall2Edges.rightEnd.z,
            );

            // find right intersection
            const intersectionRight2D = GeometryUtils.checkLineIntersection(
                wall1Edges.leftEnd.x,
                wall1Edges.leftEnd.z,
                wall1Edges.leftStart.x,
                wall1Edges.leftStart.z,
                wall2Edges.leftStart.x,
                wall2Edges.leftStart.z,
                wall2Edges.leftEnd.x,
                wall2Edges.leftEnd.z,
            );

            // check if all required intersection points were found
            if (intersectionLeft2D.x === null
                || intersectionLeft2D.y === null
                || intersectionRight2D.x === null
                || intersectionRight2D.y === null) {
                return;
            }

            // translate intersections to proper global coordinates
            intersectionLeft = new THREE.Vector3(intersectionLeft2D.x, 0, intersectionLeft2D.y);
            intersectionRight = new THREE.Vector3(intersectionRight2D.x, 0, intersectionRight2D.y);

            // check if snap point is not too far
            if (intersectionLeft.distanceTo(wall1.startPoint) > wall1.length * snapThreshold
                && intersectionLeft.distanceTo(wall2.endPoint) > wall2.length * snapThreshold) {
                return;
            }

            // deform wall1
            for (let i = 0, l = wall1Vertices.length; i < l; i++) {
                globalVertex = wall1.object.localToWorld(wall1Vertices[i].clone());

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.rightStart)) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionLeft.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.rightStart.clone().setY(wall1.height))) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionLeft.clone().setY(wall1.height)));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.leftStart)) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionRight.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.leftStart.clone().setY(wall1.height))) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionRight.clone().setY(wall1.height)));
                }
            }

            // deform wall2
            for (let i = 0, l = wall2Vertices.length; i < l; i++) {
                globalVertex = wall2.object.localToWorld(wall2Vertices[i].clone());

                // noinspection DuplicatedCode
                if (VectorUtils.equalVectors(globalVertex, wall2Edges.rightEnd)) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionLeft.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.rightEnd.clone().setY(wall2.height))) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionLeft.clone().setY(wall2.height)));
                }

                // noinspection DuplicatedCode
                if (VectorUtils.equalVectors(globalVertex, wall2Edges.leftEnd)) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionRight.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.leftEnd.clone().setY(wall2.height))) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionRight.clone().setY(wall2.height)));
                }
            }
        }

        // snap point end-to-end case
        if (VectorUtils.equalVectors(wall1.endPoint, wall2.endPoint)) {
            // check if points are on same line
            // further flow is to be skipped in this case
            if (GeometryUtils.threePointsLieOnLine(wall1.startPoint, wall1.endPoint, wall2.startPoint)) {
                return;
            }

            const intersectionLeft2D = GeometryUtils.checkLineIntersection(
                wall1Edges.leftStart.x,
                wall1Edges.leftStart.z,
                wall1Edges.leftEnd.x,
                wall1Edges.leftEnd.z,
                wall2Edges.rightStart.x,
                wall2Edges.rightStart.z,
                wall2Edges.rightEnd.x,
                wall2Edges.rightEnd.z,
            );

            const intersectionRight2D = GeometryUtils.checkLineIntersection(
                wall1Edges.rightStart.x,
                wall1Edges.rightStart.z,
                wall1Edges.rightEnd.x,
                wall1Edges.rightEnd.z,
                wall2Edges.leftStart.x,
                wall2Edges.leftStart.z,
                wall2Edges.leftEnd.x,
                wall2Edges.leftEnd.z,
            );

            // check if all required intersection points were found
            if (intersectionLeft2D.x === null
                || intersectionLeft2D.y === null
                || intersectionRight2D.x === null
                || intersectionRight2D.y === null) {
                return;
            }

            // translate intersections to proper global coordinates
            intersectionLeft = new THREE.Vector3(intersectionLeft2D.x, 0, intersectionLeft2D.y);
            intersectionRight = new THREE.Vector3(intersectionRight2D.x, 0, intersectionRight2D.y);

            // check if snap point is not too far
            if (intersectionLeft.distanceTo(wall1.endPoint) > wall1.length * snapThreshold
                && intersectionLeft.distanceTo(wall2.endPoint) > wall2.length * snapThreshold) {
                return;
            }

            // deform wall1
            for (let i = 0, l = wall1Vertices.length; i < l; i++) {
                globalVertex = wall1.object.localToWorld(wall1Vertices[i].clone());

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.leftEnd)) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionLeft.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.leftEnd.clone().setY(wall1.height))) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionLeft.clone().setY(wall1.height)));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.rightEnd.clone())) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionRight.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.rightEnd.clone().setY(wall1.height))) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionRight.clone().setY(wall1.height)));
                }
            }

            // deform wall2
            for (let i = 0, l = wall2Vertices.length; i < l; i++) {
                globalVertex = wall2.object.localToWorld(wall2Vertices[i].clone());

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.rightEnd)) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionLeft.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.rightEnd.clone().setY(wall2.height))) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionLeft.clone().setY(wall2.height)));
                }

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.leftEnd.clone())) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionRight.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.leftEnd.clone().setY(wall2.height))) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionRight.clone().setY(wall2.height)));
                }
            }
        }

        // snap point start-to-start case
        if (VectorUtils.equalVectors(wall1.startPoint, wall2.startPoint)) {
            // check if points are on same line
            // further flow is to be skipped in this case
            if (GeometryUtils.threePointsLieOnLine(wall1.endPoint, wall1.startPoint, wall2.endPoint)) {
                return;
            }

            const intersectionLeft2D = GeometryUtils.checkLineIntersection(
                wall1Edges.leftEnd.x,
                wall1Edges.leftEnd.z,
                wall1Edges.leftStart.x,
                wall1Edges.leftStart.z,
                wall2Edges.rightEnd.x,
                wall2Edges.rightEnd.z,
                wall2Edges.rightStart.x,
                wall2Edges.rightStart.z,
            );

            const intersectionRight2D = GeometryUtils.checkLineIntersection(
                wall1Edges.rightEnd.x,
                wall1Edges.rightEnd.z,
                wall1Edges.rightStart.x,
                wall1Edges.rightStart.z,
                wall2Edges.leftEnd.x,
                wall2Edges.leftEnd.z,
                wall2Edges.leftStart.x,
                wall2Edges.leftStart.z,
            );

            // check if all required intersection points were found
            if (intersectionLeft2D.x === null
                || intersectionLeft2D.y === null
                || intersectionRight2D.x === null
                || intersectionRight2D.y === null) {
                return;
            }

            // translate intersections to proper global coordinates
            intersectionLeft = new THREE.Vector3(intersectionLeft2D.x, 0, intersectionLeft2D.y);
            intersectionRight = new THREE.Vector3(intersectionRight2D.x, 0, intersectionRight2D.y);

            // check if snap point is not too far
            if (intersectionLeft.distanceTo(wall1.startPoint) > wall1.length * snapThreshold
                && intersectionLeft.distanceTo(wall2.startPoint) > wall2.length * snapThreshold) {
                return;
            }

            // deform wall1
            for (let i = 0, l = wall1Vertices.length; i < l; i++) {
                globalVertex = wall1.object.localToWorld(wall1Vertices[i].clone());

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.rightStart)) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionRight.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.rightStart.clone().setY(wall1.height))) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionRight.clone().setY(wall1.height)));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.leftStart)) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionLeft.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall1Edges.leftStart.clone().setY(wall1.height))) {
                    wall1Vertices[i].copy(wall1.object.worldToLocal(intersectionLeft.clone().setY(wall1.height)));
                }
            }

            // deform wall2
            for (let i = 0, l = wall2Vertices.length; i < l; i++) {
                globalVertex = wall2.object.localToWorld(wall2Vertices[i].clone());

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.leftStart)) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionRight.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.leftStart.clone().setY(wall2.height))) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionRight.clone().setY(wall2.height)));
                }

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.rightStart)) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionLeft.clone()));
                }

                if (VectorUtils.equalVectors(globalVertex, wall2Edges.rightStart.clone().setY(wall2.height))) {
                    wall2Vertices[i].copy(wall2.object.worldToLocal(intersectionLeft.clone().setY(wall2.height)));
                }
            }
        }

        // Update uv 1
        const geometry1 = wall1.object.geometry;
        geometry1.faceVertexUvs[0] = geometry1.faces.map((face) => {
            const components = ['z', 'y', 'x'].sort((a, b) => (Math.abs(face.normal[a]) > Math.abs(face.normal[b]) ? 1 : 0));

            const v1 = geometry1.vertices[face.a];
            const v2 = geometry1.vertices[face.b];
            const v3 = geometry1.vertices[face.c];

            return [
                new THREE.Vector2(v1[components[0]], v1[components[1]]),
                new THREE.Vector2(v2[components[0]], v2[components[1]]),
                new THREE.Vector2(v3[components[0]], v3[components[1]]),
            ];
        });
        geometry1.elementsNeedUpdate = true;
        geometry1.verticesNeedUpdate = true;
        geometry1.normalsNeedUpdate = true;
        geometry1.uvsNeedUpdate = true;

        // Update uv 2
        const geometry2 = wall2.object.geometry;
        geometry2.faceVertexUvs[0] = geometry2.faces.map((face) => {
            const components = ['z', 'y', 'x'].sort((a, b) => (Math.abs(face.normal[a]) > Math.abs(face.normal[b]) ? 1 : 0));

            const v1 = geometry2.vertices[face.a];
            const v2 = geometry2.vertices[face.b];
            const v3 = geometry2.vertices[face.c];

            return [
                new THREE.Vector2(v1[components[0]], v1[components[1]]),
                new THREE.Vector2(v2[components[0]], v2[components[1]]),
                new THREE.Vector2(v3[components[0]], v3[components[1]]),
            ];
        });
        geometry2.elementsNeedUpdate = true;
        geometry2.verticesNeedUpdate = true;
        geometry2.normalsNeedUpdate = true;
        geometry2.uvsNeedUpdate = true;
    }

    private static defineWallEdges(wall) {
        // ensure that object world matrix is updated
        wall.object.updateMatrixWorld();

        // clone geometries into working copies
        const geo = wall.object.geometry.clone();

        // define wall local edges
        const leftStart = wall.object.worldToLocal(wall.startPoint.clone())
            .add(new THREE.Vector3(wall.width / 2, 0, 0));
        const leftEnd = wall.object.worldToLocal(wall.endPoint.clone())
            .add(new THREE.Vector3(wall.width / 2, 0, 0));
        const rightStart = wall.object.worldToLocal(wall.startPoint.clone())
            .add(new THREE.Vector3(-wall.width / 2, 0, 0));
        const rightEnd = wall.object.worldToLocal(wall.endPoint.clone())
            .add(new THREE.Vector3(-wall.width / 2, 0, 0));
        // map edges
        const edges: EdgesMap = {
            leftStart: null,
            leftEnd: null,
            rightStart: null,
            rightEnd: null,
        };

        // define vertices shortcut
        const vertices = geo.vertices;

        // iterate thru all vertices
        for (let i = 0, l = vertices.length, vertex; i < l; i++) {
            vertex = vertices[i];
            // check the left and right edge vertices
            // main criteria is that start vertex will always be closer to default start etc.
            if (vertex.y === 0 && MathUtils.equalNumbers(vertex.x, wall.width / 2)) {
                if (vertex.distanceTo(leftStart) < vertex.distanceTo(leftEnd)) {
                    edges.leftStart = wall.object.localToWorld(vertex.clone());
                } else {
                    edges.leftEnd = wall.object.localToWorld(vertex.clone());
                }
            } else if (vertex.y === 0 && MathUtils.equalNumbers(vertex.x, -wall.width / 2)) {
                if (vertex.distanceTo(rightStart) < vertex.distanceTo(rightEnd)) {
                    edges.rightStart = wall.object.localToWorld(vertex.clone());
                } else {
                    edges.rightEnd = wall.object.localToWorld(vertex.clone());
                }
            }
        }

        return edges;
    }

    private static checkLineIntersection(
        line1StartX, line1StartY,
        line1EndX, line1EndY,
        line2StartX, line2StartY,
        line2EndX, line2EndY,
    ) {
        // if the lines intersect, the result contains the x and y of the intersection (treating the lines as infinite) and booleans for whether line segment 1 or line segment 2 contain the point
        const result: IntersectionPoint = {
            x: null,
            y: null,
            onLine1: false,
            onLine2: false,
        };
        const denominator = ((line2EndY - line2StartY) * (line1EndX - line1StartX))
            - ((line2EndX - line2StartX) * (line1EndY - line1StartY));
        if (denominator === 0) {
            return result;
        }
        let a = line1StartY - line2StartY;
        let b = line1StartX - line2StartX;
        const numerator1 = ((line2EndX - line2StartX) * a) - ((line2EndY - line2StartY) * b);
        const numerator2 = ((line1EndX - line1StartX) * a) - ((line1EndY - line1StartY) * b);
        a = numerator1 / denominator;
        b = numerator2 / denominator;

        // if we cast these lines infinitely in both directions, they intersect here:
        result.x = line1StartX + (a * (line1EndX - line1StartX));
        result.y = line1StartY + (a * (line1EndY - line1StartY));

        // if line1 is a segment and line2 is infinite, they intersect if:
        if (a > 0 && a < 1) {
            result.onLine1 = true;
        }
        // if line2 is a segment and line1 is infinite, they intersect if:
        if (b > 0 && b < 1) {
            result.onLine2 = true;
        }
        // if line1 and line2 are segments, they intersect if both of the above are true
        return result;
    }

    private static threePointsLieOnLine(point1: THREE.Vector3, point2: THREE.Vector3, point3: THREE.Vector3) {
        const triangle = new THREE.Triangle(point1, point2, point3);
        return triangle.getArea() === 0;
    }
}
