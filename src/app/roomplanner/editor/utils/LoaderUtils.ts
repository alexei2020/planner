import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';

export default class LoaderUtils {
    static textureLoader = new THREE.TextureLoader().setCrossOrigin('');
    static gltfLoader = new GLTFLoader().setCrossOrigin('');
    static fbxLoader = new FBXLoader().setCrossOrigin('');

    static assets = {};

    static clone(asset) {
        if (asset instanceof THREE.Texture) {
            const texture = asset.clone();
            texture.needsUpdate = true;
            return texture;
        }

        if (asset instanceof THREE.Object3D) {
            const object3D = asset.clone(true);
            object3D.traverse((child) => {
                if (child['material']) {
                    if (Array.isArray(child['material'])) {
                        child['material'] = child['material'].map(material => material.clone());
                    } else {
                        child['material'] = child['material'].clone();
                    }
                }
            });
            return object3D;
        }

        return null;
    }

    static async loadTexture(url: string, cache = true) {
        if (cache && LoaderUtils.assets[url]) {
            return LoaderUtils.clone(await LoaderUtils.assets[url]) as THREE.Texture;
        }

        return LoaderUtils.assets[url] = new Promise<THREE.Texture>((resolve) => {
            LoaderUtils.textureLoader.load(url, (texture) => {
                texture.encoding = THREE.sRGBEncoding;

                resolve(LoaderUtils.clone(texture) as THREE.Texture);
            });
        });
    }

    static async loadGLTF(url: string, cache = true) {
        if (cache && LoaderUtils.assets[url]) {
            return LoaderUtils.clone(await LoaderUtils.assets[url]) as THREE.Object3D;
        }

        return LoaderUtils.assets[url] = new Promise<THREE.Object3D>((resolve) => {
            LoaderUtils.gltfLoader.load(url, (gltf) => {
                if (gltf.animations.length) {
                    const animationMixer = new THREE.AnimationMixer(gltf.scene);

                    gltf.animations.forEach((clip) => {
                        const action = animationMixer.clipAction(clip);
                        action.time = 0; // clip.duration;
                        action.play();
                    });

                    animationMixer.update(0);
                }

                resolve(LoaderUtils.clone(gltf.scene) as THREE.Object3D);
            });
        });
    }

    static async loadFBX(url: string, cache = true) {
        if (cache && LoaderUtils.assets[url]) {
            return LoaderUtils.clone(await LoaderUtils.assets[url]) as THREE.Object3D;
        }

        return LoaderUtils.assets[url] = new Promise<THREE.Object3D>((resolve) => {
            LoaderUtils.fbxLoader.load(url, (group) => {
                resolve(LoaderUtils.clone(group) as THREE.Object3D);
            });
        });
    }

    static async loadAuto(url: string, options: any = {}) {
        const padding = new THREE.Vector3(0.001, 0.001, 0.001);
        const offset = new THREE.Vector3().fromArray(options.offset || [0.5, 0, 0.5]);
        const rotate = new THREE.Euler().fromArray(options.rotate || [0, 0, 0]);
        const scale = new THREE.Vector3().fromArray(options.scale || [1, 1, 1]);

        const holder = new THREE.Mesh<THREE.BoxGeometry, THREE.MeshBasicMaterial>(
            new THREE.BoxGeometry(1, 1, 1),
            new THREE.MeshBasicMaterial({
                color: 0xff0000,
                transparent: true,
                opacity: 0.6,
            }),
        );

        let model;
        if (url.toLowerCase().endsWith('.glb') || url.toLowerCase().endsWith('.gltf')) {
            model = await LoaderUtils.loadGLTF(url);
        } else if (url.toLowerCase().endsWith('.fbx')) {
            model = await LoaderUtils.loadFBX(url);
        }

        if (model) {
            model.rotation.copy(rotate);
            model.scale.copy(scale);

            const bbox = new THREE.Box3().setFromObject(model);
            const size = bbox.getSize(new THREE.Vector3());

            model.position.x -= bbox.min.x;
            model.position.y -= bbox.min.y;
            model.position.z -= bbox.min.z;

            model.position.x -= size.x * offset.x;
            model.position.y -= size.y * offset.y;
            model.position.z -= size.z * offset.z;

            model.position.x -= (padding.x / 2) * ((offset.x - 0.5) * 2);
            model.position.y -= (padding.y / 2) * ((offset.y - 0.5) * 2);
            model.position.z -= (padding.z / 2) * ((offset.z - 0.5) * 2);

            holder.geometry.dispose();
            holder.geometry = new THREE.BoxGeometry(size.x + padding.x, size.y + padding.y, size.z + padding.z);
            holder.geometry.translate(holder.geometry.parameters.width / 2, holder.geometry.parameters.height / 2, holder.geometry.parameters.depth / 2);
            holder.geometry.translate(-holder.geometry.parameters.width * offset.x, -holder.geometry.parameters.height * offset.y, -holder.geometry.parameters.depth * offset.z);
            holder.material.visible = false;
            holder.add(model);
        } else {
            holder.geometry.translate(0, 0.5, 0);
        }

        return holder;
    }
}
