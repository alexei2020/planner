import * as THREE from 'three';

import Dispatcher from '../engine/Dispatcher';

export default class Grid extends Dispatcher {
    public object: THREE.Group;

    public get visible(): boolean {
        return this.object.children[1].visible && this.object.children[2].visible;
    }

    public set visible(visible: boolean) {
        this.object.children[1].visible = visible;
        this.object.children[2].visible = visible;
        this.dispatchEvent('changed');
    }

    public constructor() {
        super();

        this.object = new THREE.Group();
        this.object.position.y = -0.001;
        this.object.userData = this;

        const background = new THREE.Mesh(
            new THREE.PlaneGeometry(12, 12, 1, 1),
            new THREE.MeshBasicMaterial({
                color: 0xffffff,
            }),
        );
        background.position.y = -0.002;
        background.rotation.x = -Math.PI / 2;
        background.renderOrder = -20;
        this.object.add(background);

        const backgroundGrid = new THREE.GridHelper(12, 24, 0xff0000, 0xb6b6b6);
        backgroundGrid.name = 'BackgroundGrid';
        backgroundGrid.renderOrder = -10;
        this.object.add(backgroundGrid);

        const foregroundGrid = new THREE.GridHelper(12, 12, 0xff0000, 0x000000);
        foregroundGrid.name = 'ForegroundGrid';
        foregroundGrid.renderOrder = -10;
        this.object.add(foregroundGrid);

        this.dispatchEvent('changed');
    }

    fromJSON(data) {
        this.visible = data.visible;
    }

    toJSON() {
        return {
            visible: this.visible,
        };
    }

    reset() {
        this.visible = true;
    }
}
