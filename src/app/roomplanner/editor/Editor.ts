import * as THREE from 'three';
import cloneDeep from 'lodash/cloneDeep';

import Dispatcher from './engine/Dispatcher';
import Renderer from './engine/Renderer';

import Room from './entities/Room';

import Grid from './helpers/Grid';

import InterfaceManager from './managers/InterfaceManager';
import HistoryManager from './managers/HistoryManager';
import MeasureManager from './managers/MeasureManager';
import EntityManager from './managers/EntityManager';
import SelectionManager from './managers/SelectionManager';
import ViewManager from './managers/ViewManager';
import EventManager from './managers/EventManager';

export default class Editor extends Dispatcher {
    private readonly catalog = [];
    private readonly renderer: Renderer;

    grid: Grid;

    interfaceManager: InterfaceManager;
    historyManager: HistoryManager;
    measureManager: MeasureManager;
    entityManager: EntityManager;
    selectionManager: SelectionManager;
    viewManager: ViewManager;
    eventManager: EventManager;

    constructor(catalog) {
        super();

        this.catalog = catalog;

        // Setup Renderer
        this.renderer = new Renderer();

        // Setup grid helper
        this.grid = new Grid();
        this.grid.addEventListener('changed', () => this.renderer.render());
        this.renderer.scene.add(this.grid.object);

        // Setup managers
        this.interfaceManager = new InterfaceManager();
        this.historyManager = new HistoryManager(this.renderer);
        this.measureManager = new MeasureManager(this.renderer);
        this.entityManager = new EntityManager(this.renderer, this.interfaceManager, this.historyManager, this.measureManager);
        this.viewManager = new ViewManager(this.renderer, this.grid, this.entityManager);
        this.selectionManager = new SelectionManager(this.renderer, this.entityManager, this.viewManager);
        this.eventManager = new EventManager(
            this.renderer,
            this.interfaceManager,
            this.historyManager,
            this.measureManager,
            this.entityManager,
            this.selectionManager,
            this.viewManager,
        );
    }

    init(container: HTMLDivElement) {
        setTimeout(() => {
            this.renderer.init(container);

            this.dispatchEvent('started');
        }, 16);
    }

    restart(): void {
        // Reset managers
        this.interfaceManager.reset();
        this.historyManager.reset();
        this.measureManager.reset();
        this.entityManager.reset();
        this.selectionManager.reset();
        this.viewManager.reset();
        this.eventManager.reset();

        // Reset helpers
        this.grid.reset();

        // Reset renderer
        this.renderer.transform.detach();
        this.renderer.render();

        this.dispatchEvent('restarted');
    }

    async fromJSON(data) {
        console.time('load');
        console.time('init');

        /* */

        // Reset managers
        this.interfaceManager.reset();
        this.historyManager.reset();
        this.measureManager.reset();
        this.entityManager.reset();
        this.selectionManager.reset();
        //this.viewManager.reset();
        this.eventManager.reset();

        // Reset helpers
        this.grid.reset();

        // Reset renderer
        this.renderer.transform.detach();
        this.renderer.render();

        this.dispatchEvent('restarted');

        /* */

        if (!data || data.version !== '0.0.1') {
            return;
        }

        console.timeEnd('init');

        console.time('helpers');

        this.measureManager.fromJSON(data.measure);

        this.grid.fromJSON(data.helpers.grid);

        console.timeEnd('helpers');

        console.time('loader');

        await Promise.all(
            data.entities
                .filter(entityData => entityData.type !== 'Wall')
                .map(entityData => this.entityManager.addEntity(
                    entityData.type === 'Corner'
                        ? { type: 'Corner' }
                        : this.catalog.find(item => item.name === entityData.userData.name),
                    false,
                    entityData,
                )),
        );

        console.timeEnd('loader');

        console.time('rooms');

        this.entityManager.rooms.forEach((room) => {
            room.corners = room.data.corners.map(cornerId => this.entityManager.corners.find(corner => corner.id === cornerId));
            room.walls.forEach((wall, i) => {
                const wallData = data.entities
                    .filter(roomData => roomData.type === 'Wall' && roomData.room === room.id)
                    .find(wallData => wallData.startCorner === wall.startCorner.id && wallData.endCorner === wall.endCorner.id);
                wall.fromJSON(wallData);
            });
            room.render();
        });

        console.timeEnd('rooms');

        console.time('entities');

        this.entityManager.entities.forEach((entity) => {
            if (entity.data.attachedTo) {
                this.entityManager.walls
                    .filter(wall => wall.id === entity.data.attachedTo)
                    .forEach((wall) => {
                        // @ts-ignore
                        entity.object.geometry.computeBoundingBox();

                        // @ts-ignore
                        const geometryBbox = entity.object.geometry.boundingBox;
                        const geometrySize = geometryBbox.getSize(new THREE.Vector3());

                        wall.addEntity({
                            uuid: entity.object.uuid,
                            target: entity.object,
                            width: geometrySize.x * entity.object.scale.x,
                            height: geometrySize.y * entity.object.scale.y,
                            hole: entity.object.userData.userData?.snap?.hole,
                        });
                    });
            }

            // @ts-ignore
            entity.params?.find(param => param.label === entity.variant)?.onClick();
        });

        console.timeEnd('entities');

        console.time('merge');

        Room.mergeRooms(this.entityManager);

        console.timeEnd('merge');

        console.timeEnd('load');
    }

    toJSON() {
        const entities = this.entityManager.entities.map(entity => entity.toJSON());

        const hierarchy = cloneDeep(entities.filter(entity => entity.type === 'Room'));
        const buffer = hierarchy.slice();

        const order = ['Floor', 'Suite', 'Room', 'Other'];

        this.entityManager.computeHierarchy(entities, buffer, 'Room', ['Other']);
        this.entityManager.computeHierarchy(entities, buffer, 'Suite', ['Room', 'Other']);
        this.entityManager.computeHierarchy(entities, buffer, 'Floor', ['Suite', 'Room', 'Other']);
        this.entityManager.computeHierarchy(entities, buffer, 'Other', ['Floor', 'Suite', 'Room', 'Other']);

        return {
            version: '0.0.1',
            measure: this.measureManager.toJSON(),
            helpers: {
                grid: this.grid.toJSON(),
            },
            entities: entities,
            hierarchy: hierarchy
                .map(item => ({
                    id: item.id,
                    parentId: item.parentId || null,
                    name: item.name || 'Untitled',
                    type: item.meta.type || 'Other',
                }))
                .sort((a, b) => order.indexOf(a.type) - order.indexOf(b.type)),
        };
    }

    dispose(): void {
        this.restart();
        this.removeAllEventListener();

        this.renderer.dispose();
    }
}
