import * as THREE from 'three';
import { SelectionBox } from 'three/examples/jsm/interactive/SelectionBox';

import { SelectionHelper } from '../engine/libraries/interactive/SelectionHelper';

import Renderer from '../engine/Renderer';

import Entity from '../entities/base/Entity';
import Room from '../entities/Room';
import Wall from '../entities/Wall';
import Corner from '../entities/Corner';

import Manager from './base/Manager';
import EntityManager from './EntityManager';
import ViewManager, { EditorMode } from './ViewManager';

export default class SelectionManager extends Manager {
    private readonly selectionBox: SelectionBox;
    private readonly selectionHelper: SelectionHelper;

    public selected: Entity | null = null;
    public hovered: Entity | null = null;

    public constructor(private renderer: Renderer, private entityManager: EntityManager, private viewManager: ViewManager) {
        super();

        this.selectionBox = new SelectionBox(this.renderer.camera, this.renderer.scene);
        this.selectionHelper = new SelectionHelper(this.selectionBox, this.renderer.domElement);

        this.entityManager.addEventListener('entityRemoved', ({ entity }) => {
            if (this.selected === entity) {
                this.selectEntity(null);
            }
        });

        this.viewManager.addEventListener('editorModeChanged', ({ editorMode }) => {
            if (editorMode === EditorMode.View) {
                this.selectEntity(null);
            }
        });
    }

    public selectEntitiesByArea(startPoint: THREE.Vector2, endPoint: THREE.Vector2): void {
        if (this.viewManager.editorMode !== EditorMode.Edit) {
            return;
        }

        this.entityManager.entities.forEach((entity) => {
            entity.isSelected = false;
        });

        this.selectionBox.collection = [];
        this.selectionBox.startPoint.set(startPoint.x, startPoint.y, 0.5);
        this.selectionBox.endPoint.set(endPoint.x, endPoint.y, 0.5);
        this.selectionBox.select()
            .filter(mesh => mesh.userData instanceof Entity && mesh.userData.isInteractive)
            .map(mesh => mesh.userData)
            .filter((entity, i, entities) => entities.indexOf(entity) === i)
            .forEach((entity) => {
                entity.isHovered = true;
                entity.isSelected = true;
            });

        this.renderer.render();
    }

    public selectEntity(entity: Entity | null): void {
        this.renderer.transform.detach();

        this.entityManager.entities.forEach((entity) => {
            entity.isHovered = false;
            entity.isSelected = false;
        });

        // TODO move to entities
        if (this.selected instanceof Corner) {
            this.selected.visible = false;
        } else if (this.selected instanceof Wall) {
            this.selected.startCorner.visible = false;
            this.selected.endCorner.visible = false;
        } else if (this.selected instanceof Room) {
            this.selected.corners.forEach((corner) => {
                corner.visible = false;
            });
        }

        this.selected = this.viewManager.editorMode === EditorMode.Edit ? entity : null;
        if (this.selected) {
            this.selected.isHovered = true;
            this.selected.isSelected = true;

            // TODO move to entities
            if (this.selected instanceof Corner) {
                this.selected.visible = true;
            } else if (this.selected instanceof Wall) {
                this.selected.startCorner.visible = true;
                this.selected.endCorner.visible = true;
            } else if (this.selected instanceof Room) {
                this.selected.corners.forEach((corner) => {
                    corner.visible = true;
                });
            }

            if (this.selected.object.parent) {
                this.renderer.transform.attach(this.selected.object);
            }
        }

        this.renderer.render();

        this.dispatchEvent('selected', {
            selected: this.selected,
        });
    }

    public reset(): void {
        super.reset();

        this.selectionBox.collection = [];

        this.selectEntity(null);
    }
}
