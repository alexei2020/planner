import polygonPoint from 'intersects/polygon-point';

import Renderer from '../engine/Renderer';

import Entity from '../entities/base/Entity';
import Corner from '../entities/Corner';
import Wall from '../entities/Wall';
import Room from '../entities/Room';
import RoomLabel from '../entities/RoomLabel';
import Door from '../entities/Door';
import Window from '../entities/Window';
import Table from '../entities/Table';
import EthernetWallJack from '../entities/EthernetWallJack';
import WirelessAccessPoint from '../entities/WirelessAccessPoint';
import Furniture from '../entities/Furniture';
import FloorPlan from '../entities/FloorPlan';

import Manager from './base/Manager';
import InterfaceManager from './InterfaceManager';
import HistoryManager from './HistoryManager';
import MeasureManager from './MeasureManager';

import LoaderUtils from '../utils/LoaderUtils';
import SceneUtils from '../utils/SceneUtils';

const Entities = {
    Corner,
    Wall,
    Room,
    RoomLabel,
    Door,
    Window,
    Table,
    EthernetWallJack,
    WirelessAccessPoint,
    Furniture,
    FloorPlan,
};

export default class EntityManager extends Manager {
    get entities(): Entity[] {
        return SceneUtils.traverseReduce(this.renderer.scene, (entities, child) => {
            if (child.userData instanceof Entity && entities.indexOf(child.userData) === -1) {
                entities.push(child.userData);
            }
            return entities;
        }, []);
    }

    get rooms(): Room[] {
        return this.entities.filter(entity => entity instanceof Room) as Room[];
    }

    get walls(): Wall[] {
        return this.entities.filter(entity => entity instanceof Wall) as Wall[];
    }

    get corners(): Corner[] {
        return this.entities.filter(entity => entity instanceof Corner) as Corner[];
    }

    constructor(private renderer: Renderer, private interfaceManager: InterfaceManager, private historyManager: HistoryManager, private measureManager: MeasureManager) {
        super();
    }

    async addEntity(entityData, fromUI = true, jsonData = null): Promise<Entity> {
        let entity: Entity;

        if (entityData.type === 'FloorPlan') {
            this.entities.filter(entity => entity instanceof FloorPlan).forEach(entity => this.removeEntity(entity));
        }

        if (Entities[entityData.type]) {
            entity = new Entities[entityData.type]();
        } else {
            entity = new Furniture();
        }

        if (entityData.model) {
            entity.object = await LoaderUtils.loadAuto(entityData.model, {
                rotate: entityData.rotate,
                offset: entityData.offset,
                scale: entityData.scale,
            });
            entity.object.userData = entity;
        } else if (entityData.models) {
            const modelData = entityData.models.find(model => model.default) || entityData.models[0];
            entity.object = await LoaderUtils.loadAuto(modelData.url, {
                rotate: modelData.rotate,
                offset: modelData.offset,
                scale: modelData.scale,
            });
            entity.object.userData = entity;
        }

        if (this.renderer.camera.fov === 1) {
            if (entityData.snap?.to?.includes('Wall')) {
                entity.renderOnTop();
            }
        }

        entity.isSelected = fromUI;
        entity.visible = !fromUI;
        entity.userData = entityData;
        entity.measureManager = this.measureManager;
        entity.interfaceManager = this.interfaceManager;
        entity.setEntityManager(this);
        if (entity instanceof FloorPlan) {
            if (fromUI) {
                const input = document.createElement('input');
                input.type = 'file';
                input.accept = 'image/*';
                input.onchange = event => (entity as FloorPlan).loadImage(event.target['files'][0]);
                input.click();
            }
            entity.visible = true;
        } else if (entity instanceof Room) {
            entity.showMeasure = true;
        }
        entity.addEventListener('change', () => this.renderer.render());
        entity.addToScene(this.renderer.scene);

        this.historyManager.addToHistory(
            () => {
                this.removeEntity(entity);
            },
            () => {
            },
        );

        this.dispatchEvent('entityAdded', { entity, fromUI });
        this.dispatchEvent('entitiesChanged');

        if (jsonData) {
            entity.fromJSON(jsonData);
        }

        return entity;
    }

    async removeEntity(entity: Entity): Promise<void> {
        this.walls.forEach((wall) => {
            wall.removeEntity({
                uuid: entity.object.uuid,
                target: entity.object,
            });
        });

        entity.dispose();

        this.corners.forEach((corner) => {
            if (!this.walls.some(wall => wall.startCorner === corner || wall.endCorner === corner)) {
                corner.dispose();
            }
        });

        Room.mergeRooms(this);

        this.dispatchEvent('entityRemoved', { entity });
        this.dispatchEvent('entitiesChanged');
    }

    computeHierarchy(entities, buffer, type, groups = []) {
        return buffer.filter(entity => entity.meta.type === type).forEach((entity) => {
            const points = entity.corners
                .map(corner => entities.find(entity => entity.id === corner))
                .map(corner => ({
                    x: corner.position[0],
                    y: corner.position[2],
                }));

            buffer.filter(child => groups.includes(child.meta.type)).filter((child) => {
                const subPoints = child.corners
                    .map(corner => entities.find(entity => entity.id === corner))
                    .map(corner => ({
                        x: corner.position[0],
                        y: corner.position[2],
                    }));

                const isInside = subPoints.every(point => polygonPoint(
                    points.reduce((points, point) => [...points, point.x, point.y], []),
                    point.x, point.y,
                    0.01,
                ));

                if (isInside) {
                    child.parentId = child.parentId || entity.id;

                    buffer = buffer.filter(space => space !== child);
                }

                return isInside;
            });
        });
    }

    reset(): void {
        super.reset();

        this.entities.forEach(entity => entity.dispose());
    }
}
