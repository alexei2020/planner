import { v4 } from 'uuid';
import * as THREE from 'three';
import orderBy from 'lodash/orderBy';

import Renderer from '../engine/Renderer';

import Entity from '../entities/base/Entity';
import Corner from '../entities/Corner';
import Wall from '../entities/Wall';
import Room from '../entities/Room';
import Table from '../entities/Table';
import EthernetWallJack from '../entities/EthernetWallJack';

import Manager from './base/Manager';
import InterfaceManager from './InterfaceManager';
import HistoryManager from './HistoryManager';
import MeasureManager from './MeasureManager';
import EntityManager from './EntityManager';
import SelectionManager from './SelectionManager';
import ViewManager, { EditorMode, TransformMode } from './ViewManager';

import VectorUtils from '../utils/VectorUtils';
import SceneUtils from '../utils/SceneUtils';

export default class EventManager extends Manager {
    public isAdding = false;
    public isDrawing = false;
    public isCutting = false;
    public measureDrawing = -1;

    public currentRoom: Room | null = null;
    public currentCorner: Corner | null = null;

    private raycaster = new THREE.Raycaster();
    private plane = new THREE.Plane(new THREE.Vector3(0, 1, 0), 0);

    public selected: Entity | null = null;
    private hovered: Entity | null = null;
    private buffered: Entity | null = null;

    private offset = new THREE.Vector3();
    private tolerance = 0.01;

    private affectedCorners: Corner[] = [];
    private affectedWalls: Wall[] = [];

    private inverseMatrix = new THREE.Matrix4();

    private intersectTargetPosition = new THREE.Vector3();
    private intersectTargetOffset = new THREE.Vector3();
    private intersectTargetMatrix = new THREE.Matrix4();

    private lastMoveEvent: any = null;
    private lastDragEvent: any = null;

    public constructor(
        private renderer: Renderer,
        private interfaceManager: InterfaceManager,
        private historyManager: HistoryManager,
        private measureManager: MeasureManager,
        private entityManager: EntityManager,
        private selectionManager: SelectionManager,
        private viewManager: ViewManager,
    ) {
        super();

        this.measureManager.addEventListener('setScale', () => {
            this.measureDrawing = 0;
        });

        this.entityManager.addEventListener('entityAdded', ({ entity, fromUI }) => {
            this.selectionManager.selectEntity(null);
            this.reset();

            if (!fromUI) {
                return;
            }

            if (entity instanceof Room) {
                this.entityManager.corners.forEach((corner) => {
                    corner.visible = true;
                });

                this.isAdding = false;
                this.isDrawing = true;

                this.currentCorner = new Corner();
                this.currentCorner.visible = false;
                this.currentCorner.addEventListener('change', () => this.renderer.render());
                this.currentCorner.addToScene(this.renderer.scene);
                this.currentRoom = entity;
                this.currentRoom.isUpdateCornersGeometry = false;
                this.currentRoom.corners = [this.currentCorner];

                this.selected = null;
            } else {
                this.isAdding = true;
                this.isDrawing = false;

                this.currentCorner = null;
                this.currentRoom = null;

                this.selected = entity;
            }
        });

        this.renderer.addEventListener('update', () => {
            this.entityManager.entities.forEach(entity => entity.update(this.renderer.camera));
        });

        this.renderer.transform.addEventListener('dragging-changed', (event) => {
            if (event.value) {
                if (event.target.object.userData.userData.snap?.to?.includes('Wall')) {
                    this.entityManager.walls.forEach((wall) => {
                        wall.entities.forEach((entity) => {
                            entity.prevHole = entity.prevHole || entity.hole;
                            if (entity.target.userData === event.target.object.userData) {
                                entity.hole = false;
                            }
                        });
                        wall.render();
                    });
                }

                this.historyManager.prevData = {
                    entity: event.target.object.userData,
                    height: event.target.object.userData.height,
                    position: event.target.object.userData.position.clone(),
                    rotation: event.target.object.rotation.clone(),
                    scale: event.target.object.scale.clone(),
                };
            } else {
                if (event.target.object.userData.userData.snap?.to?.includes('Wall')) {
                    this.entityManager.walls.forEach((wall) => {
                        wall.entities.forEach((entity) => {
                            entity.target.geometry.computeBoundingBox();

                            const geometryBbox = entity.target.geometry.boundingBox;
                            const geometrySize = geometryBbox.getSize(new THREE.Vector3());

                            entity.width = geometrySize.x * entity.target.scale.x;
                            entity.height = geometrySize.y * entity.target.scale.y;
                            entity.hole = entity.prevHole;

                            delete entity.prevHole;
                        });
                        wall.render();
                    });
                }
                this.measureManager.measureText('scale', null);

                this.historyManager.nextData = {
                    entity: event.target.object.userData,
                    height: event.target.object.userData.height,
                    position: event.target.object.userData.position.clone(),
                    rotation: event.target.object.rotation.clone(),
                    scale: event.target.object.scale.clone(),
                };

                const prevData = this.historyManager.prevData;
                const nextData = this.historyManager.nextData;
                if (prevData && nextData) {
                    this.historyManager.addToHistory(
                        () => {
                            prevData.entity.position = prevData.position.clone();
                            prevData.entity.object.rotation.copy(prevData.rotation.clone());
                            prevData.entity.object.scale.copy(prevData.scale.clone());
                            if (prevData.entity.height) {
                                prevData.entity.height = prevData.height;
                            }
                        },
                        () => {
                            nextData.entity.position = nextData.position.clone();
                            nextData.entity.object.rotation.copy(nextData.rotation.clone());
                            nextData.entity.object.scale.copy(nextData.scale.clone());
                            if (nextData.entity.height) {
                                nextData.entity.height = nextData.height;
                            }
                        },
                    );
                }
            }
        });
        this.renderer.transform.addEventListener('objectChange', (event) => {
            if (event.target.mode === 'scale') {
                event.target.object.scale.min(new THREE.Vector3(2, 2, 2));
                event.target.object.scale.max(new THREE.Vector3(0.5, 0.5, 0.5));
                event.target.object.vScale = event.target.object.scale.clone();

                if (event.target.object.userData instanceof Room) {
                    event.target.object.scale.set(1, 1, 1);
                    event.target.object.vScale.y = Math.max(event.target.object.vScale.y, 1);
                    event.target.object.userData.height = event.target.object.vScale.y;
                }

                if (event.target.object.userData.isMeasured) {
                    this.measureManager.measureText(
                        'scale',
                        (event.target.object.vScale || event.target.object.scale)[event.target.axis.toLowerCase()],
                        event.target.object.userData.center || event.target.object.userData.position,
                    );
                }
            }
        });

        this.renderer.mouse.addEventListener('pointerdown', event => this.handlePointerDown(event));
        this.renderer.mouse.addEventListener('pointermove', event => this.handlePointerMove(event));
        this.renderer.mouse.addEventListener('pointerdrag', event => this.handlePointerDrag(event));
        this.renderer.mouse.addEventListener('pointerup', event => this.handlePointerUp(event));
        this.renderer.mouse.addEventListener('click', event => this.handlePointerClick(event));
        this.renderer.mouse.addEventListener('dblclick', event => this.handlePointerDblClick(event));

        window.addEventListener('keydown', event => this.handleKeyDown(event));
        window.addEventListener('keyup', event => this.handleKeyUp(event));
    }

    private getIntersections(event) {
        this.raycaster.setFromCamera(event.mouse, this.renderer.camera);

        const order = [
            'Corner',
        ];

        const objects = SceneUtils.traverseReduce(this.renderer.scene, (objects, child) =>
            child.userData instanceof Entity && child.userData.isInteractive && child.userData.visible ? [...objects, child] : objects, []);

        const intersection = this.raycaster.ray.intersectPlane(this.plane, new THREE.Vector3()) || new THREE.Vector3();
        const intersects = orderBy(
            this.raycaster.intersectObjects(objects, false),
            [
                (intersect) => {
                    const index = order.indexOf(intersect.object.userData.type);
                    return index === -1 ? Infinity : index;
                },
                'distance',
            ],
            ['asc', 'asc'],
        );

        return {
            intersection,
            intersects: intersects.filter(intersect => intersect.object.parent.visible && intersect.object.visible),
            all: intersects,
        };
    }

    private async getClosestEntity(intersects, ask = false): Promise<Entity> {
        let closestIntersect = intersects[0];

        if (ask) {
            const closestIntersects = intersects.filter(intersect => (intersect.distance - intersects[0].distance) <= 0.01);
            if (closestIntersects.length > 1) {
                const items = closestIntersects.map(intersect => {
                    let name = intersect.object.userData.name || 'Untitled';
                    let type = intersect.object.userData.userData.name || intersect.object.userData.type || 'Undefined';

                    if (['Corner', 'Wall', 'Room'].includes(type)) {
                        type = 'Room'
                    }

                    return {
                        id: intersect.object.userData.id,
                        name: `${name} - ${type}`,
                    };
                });
                const item = await this.interfaceManager.roomPlannerService.selectDialog(items).toPromise();

                closestIntersect = item ? intersects.find(intersect => intersect.object.userData.id === item.id) : null;
            }
        }

        return closestIntersect ? closestIntersect.object.userData : null;
    }

    /* Mouse Events */

    private async handlePointerDown(event) {
        if (this.viewManager.editorMode === EditorMode.Edit) {
            const { intersection, intersects } = this.getIntersections(event);

            if (this.isAdding) {
                if (this.selected) {
                    this.handleEntityDragStart(event, this.selected, intersection, intersects);
                }
            } else if (this.isDrawing) {
                // skip
            } else if (this.measureDrawing !== -1) {
                if (!event.originalEvent.metaKey) {
                    intersection.x = Math.round(intersection.x / 0.1) * 0.1;
                    intersection.y = Math.round(intersection.y / 0.1) * 0.1;
                    intersection.z = Math.round(intersection.z / 0.1) * 0.1;
                }

                if (this.measureDrawing === 0) {
                    this.measureManager.startMarker = intersection;
                } else if (this.measureDrawing === 1) {
                    this.measureManager.endMarker = intersection;
                }
            } else if (event.originalEvent.shiftKey) {
                this.selectionManager.selectEntitiesByArea(event.downMouse, event.mouse);

                this.renderer.orbit.enabled = false;
            } else if (this.viewManager.transformMode === TransformMode.Translate) {
                if (intersects.some(intersect => intersect.object.userData === this.selectionManager.selected)) {
                    this.handleEntityDragStart(event, this.selectionManager.selected, intersection, intersects);
                }
            }

            this.renderer.render();
        }
    }

    private async handlePointerMove(event) {
        this.lastMoveEvent = event;

        if (this.viewManager.editorMode === EditorMode.Edit) {
            const { intersection, intersects } = this.getIntersections(event);

            if (this.isAdding) {
                if (this.selected) {
                    this.handleEntityDragMove(event, this.selected, intersection, intersects);
                }
            } else if (this.isDrawing) {
                if (this.currentCorner) {
                    if (!event.originalEvent.metaKey) {
                        intersection.x = Math.round(intersection.x / 0.1) * 0.1;
                        intersection.y = Math.round(intersection.y / 0.1) * 0.1;
                        intersection.z = Math.round(intersection.z / 0.1) * 0.1;

                        const lastCorner = this.currentRoom.corners[this.currentRoom.corners.length - 2];
                        if (lastCorner) {
                            intersection.copy(VectorUtils.alignPoints(intersection, lastCorner.position));
                        }
                    }

                    this.currentCorner.position = intersection;
                    this.currentCorner.visible = true;
                }

                this.currentRoom.showMeasure = true;
            } else if (this.measureDrawing !== -1) {
                if (!event.originalEvent.metaKey) {
                    intersection.x = Math.round(intersection.x / 0.1) * 0.1;
                    intersection.y = Math.round(intersection.y / 0.1) * 0.1;
                    intersection.z = Math.round(intersection.z / 0.1) * 0.1;
                }

                if (this.measureDrawing === 0) {
                    this.measureManager.startMarker = intersection;
                } else if (this.measureDrawing === 1) {
                    this.measureManager.endMarker = intersection;
                }
            } else {
                if ((!this.selectionManager.selected || this.selectionManager.selected.type === 'FloorPlan')
                    && (!this.selected || this.selected.type === 'FloorPlan')) {
                    if (intersects.length > 0 && intersects[0].object.userData instanceof Entity) {
                        if (this.selectionManager.hovered !== intersects[0].object.userData) {
                            if (this.selectionManager.hovered) {
                                this.selectionManager.hovered.isHovered = false;
                            }
                            this.selectionManager.hovered = intersects[0].object.userData;
                            this.selectionManager.hovered.isHovered = !this.selectionManager.hovered.isSelected && true;

                            this.renderer.domElement.style.cursor = 'pointer';
                        }
                    } else if (this.selectionManager.hovered !== null) {
                        this.selectionManager.hovered.isHovered = false;
                        this.selectionManager.hovered = null;

                        this.renderer.domElement.style.cursor = 'auto';
                    }
                } else if (this.selectionManager.hovered) {
                    this.selectionManager.hovered.isHovered = false;
                    this.selectionManager.hovered = null;
                }
            }

            this.renderer.render();
        }
    }

    private async handlePointerDrag(event) {
        this.lastDragEvent = event;

        if (this.viewManager.editorMode === EditorMode.Edit) {
            const { intersection, intersects } = this.getIntersections(event);

            if (this.isDrawing) {
                // skip
            } else if (this.measureDrawing !== -1) {
                // skip
            } else if (this.viewManager.transformMode === TransformMode.Translate) {
                if (this.selected) {
                    this.handleEntityDragMove(event, this.selected, intersection, intersects);
                }
            }

            this.renderer.render();
        }
    }

    private async handlePointerUp(event) {
        if (this.viewManager.editorMode === EditorMode.Edit) {
            const { intersection, intersects } = this.getIntersections(event);

            if (this.isAdding) {
                this.isAdding = false;

                if (this.selected) {
                    if (this.selected instanceof EthernetWallJack) {
                        const text = prompt('Enter name:', '');
                        if (text && text.length >= 1 && text.length <= 12) {
                            this.selected.setLabel(text);
                        }
                    }

                    this.handleEntityDragEnd(event, this.selected, intersection, intersects);
                }
            } else if (this.isDrawing) {
                // skip
            } else if (this.measureDrawing !== -1) {
                if (!event.originalEvent.metaKey) {
                    intersection.x = Math.round(intersection.x / 0.1) * 0.1;
                    intersection.y = Math.round(intersection.y / 0.1) * 0.1;
                    intersection.z = Math.round(intersection.z / 0.1) * 0.1;
                }

                if (this.measureDrawing === 0) {
                    this.measureManager.startMarker = intersection;
                } else if (this.measureDrawing === 1) {
                    this.measureManager.endMarker = intersection;
                }

                if (this.measureDrawing === 1) {
                    this.measureDrawing = -1;
                    this.measureManager.confirmDrawing();
                } else {
                    this.measureDrawing++;
                }
            } else if (event.originalEvent.shiftKey) {
                this.selectionManager.selectEntitiesByArea(event.downMouse, event.mouse);
            } else if (this.viewManager.transformMode === TransformMode.Translate) {
                if (this.selected) {
                    this.handleEntityDragEnd(event, this.selected, intersection, intersects);
                }
            }

            this.renderer.orbit.enabled = true;
            this.renderer.render();
        }
    }

    private async handlePointerClick(event) {
        if (this.viewManager.editorMode === EditorMode.Edit) {
            this.viewManager.changeTransformMode(TransformMode.Select);

            const { intersection, intersects } = this.getIntersections(event);

            if (this.isDrawing) {
                const intersectCorner = intersects
                    .map(intersect => intersect.object.userData)
                    .find(entity => entity instanceof Corner && entity !== this.currentCorner && !this.currentRoom.corners.some(corner => corner === entity));
                if (intersectCorner) {
                    this.currentRoom.corners = this.currentRoom.corners.filter(corner => corner !== this.currentCorner);
                    this.currentRoom.corners = [...this.currentRoom.corners, intersectCorner as Corner];

                    this.currentCorner.dispose();
                    this.currentCorner = null;
                }

                this.currentCorner = new Corner(intersection);
                this.currentCorner.addEventListener('change', () => this.renderer.render());
                this.currentCorner.addToScene(this.renderer.scene);

                this.currentRoom.corners = [...this.currentRoom.corners, this.currentCorner];
                this.currentRoom.visible = true;
            } else {
                let entity = await this.getClosestEntity(intersects, true);
                if (entity) {
                    if (event.originalEvent.metaKey && entity instanceof Wall && entity.room) {
                        entity = entity.room;
                    }
                }

                this.selectionManager.selectEntity(entity);
            }
        }
    }

    private async handlePointerDblClick(event) {
        if (this.viewManager.editorMode === EditorMode.Edit) {
            // const { intersection } = this.getIntersections(event);

            if (this.isDrawing) {
                this.isDrawing = false;

                if (this.currentCorner) {
                    this.currentRoom.corners = this.currentRoom.corners.filter(corner => corner !== this.currentCorner);

                    this.currentCorner.dispose();
                    this.currentCorner = null;
                }

                const corner1 = this.currentRoom.corners[0];
                const corner2 = this.currentRoom.corners[this.currentRoom.corners.length - 1];
                if (corner1 && corner2 && corner1.position.distanceTo(corner2.position) < 0.1) {
                    this.currentRoom.corners = this.currentRoom.corners.filter(corner => corner !== corner2);

                    corner2.dispose();
                }

                this.currentRoom.isUpdateCornersGeometry = true;
                this.currentRoom.walls.forEach(wall => wall.render());
                this.currentRoom.showMeasure = false;
                this.currentRoom.render();

                this.interfaceManager.floorSpaceService
                    .confirmDialog('Input room type and name', { type: 'Room' })
                    .subscribe((response) => {
                        if (!response) {
                            return;
                        }

                        this.currentRoom.setLabel(response.name);
                        this.currentRoom.meta.type = response.type;
                        this.currentRoom.meta.name = response.name;

                        this.entityManager.dispatchEvent('entitiesChanged');
                    });

                Room.mergeRooms(this.entityManager);

                this.entityManager.corners.forEach((corner) => {
                    corner.visible = false;
                });
            }
        }
    }

    /* Keyboard Events */

    private async handleKeyDown(event) {
        if (this.viewManager.editorMode === EditorMode.Edit) {
            if (event.metaKey) {
                this.renderer.transform.setRotationSnap(0);
                this.renderer.render();
            }

            if (['Escape'].includes(event.key)) {
                this.handleEscape();
            }

            if (['Delete', 'Backspace'].includes(event.key)) {
                this.handleDelete();
            }
        }
    }

    private async handleKeyUp(event) {
        if (this.viewManager.editorMode === EditorMode.Edit) {
            this.renderer.transform.setRotationSnap(Math.PI / 4);
            this.renderer.render();
        }
    }

    /* Entity Events */

    private handleEntityDragStart(event, entity: Entity, intersection, intersects) {
        this.reset();

        this.selected = entity;

        if (event.originalEvent.metaKey) {
            if (this.selected instanceof Wall) {
                if (this.selected.room) {
                    this.selected = this.selected.room;
                }
            }
        }

        this.historyManager.prevData = {
            entity: this.selected,
            position: this.selected.position.clone(),
            attachedTo: this.selected.object['attachedTo'],
        };

        if (this.selected) {
            this.entityManager.walls.forEach((wall) => {
                wall.removeEntity({
                    uuid: this.selected.object.uuid,
                    target: this.selected.object,
                });
            });
        }

        if (this.selected instanceof Corner) {
            this.affectedCorners = [];
            this.renderer.scene.traverse((child) => {
                if (this.selected instanceof Corner && child.userData instanceof Corner
                    && child.userData.position.distanceTo(this.selected.position) < this.tolerance) {
                    this.affectedCorners.push(child.userData);
                }
            });
        } else if (this.selected instanceof Wall) {
            this.affectedWalls = [];
            this.renderer.scene.traverse((child) => {
                if (this.selected instanceof Wall && child.userData instanceof Wall
                    && (child.userData.startCorner.position.distanceTo(this.selected.startCorner.position) < this.tolerance
                        || child.userData.startCorner.position.distanceTo(this.selected.endCorner.position) < this.tolerance)
                    && (child.userData.endCorner.position.distanceTo(this.selected.startCorner.position) < this.tolerance
                        || child.userData.endCorner.position.distanceTo(this.selected.endCorner.position) < this.tolerance)) {
                    this.affectedWalls.push(child.userData);
                }
            });
        }

        // Handle intersects
        const intersections = this.getIntersections(event);
        const otherIntersects = intersections.intersects.filter(intersect => intersect.object.userData !== this.selected);

        if (otherIntersects[0]) {
            this.selected.object.updateWorldMatrix(true, true);
            this.selected.object.updateMatrixWorld(true);

            this.intersectTargetPosition.copy(this.selected.object.position);
            this.intersectTargetMatrix.copy(this.selected.object.matrixWorld);
            this.intersectTargetOffset.copy(this.selected.object.worldToLocal(otherIntersects[0].point.clone()));
        }

        if (this.selected.userData.snap?.to?.includes('Wall')) {
            if (this.selected.object.parent) {
                this.selected.object.parent.updateMatrixWorld();
                this.selected.object.updateMatrixWorld();
                this.inverseMatrix.getInverse(this.selected.object.parent.matrixWorld);
                this.offset.copy(intersection).sub(new THREE.Vector3().setFromMatrixPosition(this.selected.object.matrixWorld));
            } else {
                this.offset.copy(intersection).sub(this.selected.object.position);
            }
        } else {
            this.offset.copy(intersection).sub(this.selected.position);
        }

        if (this.selected.isSelectable) {
            this.renderer.orbit.enabled = false;
            this.renderer.domElement.style.cursor = 'move';
        }

        //this.selectionManager.selectEntity(this.selected);

        this.renderer.render();
    }

    private handleEntityDragMove(event, entity: Entity, intersection, intersects) {
        this.selected = entity;

        if (!this.selected.isSelectable) {
            return;
        }

        // Handle intersects
        const intersections = this.getIntersections(event);
        const otherIntersects = intersections.intersects.filter(intersect => intersect.object.userData !== this.selected);
        const roomIntersect = intersections.all.find(intersect => intersect.object.userData !== this.selected && intersect.object.userData instanceof Room);
        const wallIntersect = otherIntersects.find(intersect => intersect.object.userData instanceof Wall);
        const tableIntersect = otherIntersects.find(intersect => intersect.object.userData instanceof Table);

        // Calculate position offsets
        if (this.selected.snapTo === 'None' && this.selected.userData.snap?.to?.includes('Ceiling') && roomIntersect?.object.name === 'Ceiling') {
            this.offset.set(0, 0, 0);
            this.plane.constant = -roomIntersect.object.position.y;
        } else {
            if (this.selected.userData.snap?.to?.includes('Ceiling')) {
                if (this.plane.constant !== 0) {
                    this.offset.y = 0;
                    this.plane.constant = 0;
                }
            }
        }

        // Apply position
        const position = intersection.clone().sub(this.offset).applyMatrix4(this.inverseMatrix);
        if (this.selected instanceof Corner || this.selected instanceof Wall || this.selected instanceof Room) {
            if (!event.originalEvent.metaKey) {
                position.x = Math.round(position.x / 0.1) * 0.1;
                position.y = Math.round(position.y / 0.1) * 0.1;
                position.z = Math.round(position.z / 0.1) * 0.1;
            }
        }

        if (this.selected instanceof Corner) {
            this.affectedCorners.forEach((corner) => {
                corner.position = position.clone();
            });
        } else if (this.selected instanceof Wall) {
            this.affectedWalls.forEach((wall) => {
                wall.position = position.clone();
            });
        } else {
            this.selected.position = position;
        }
        this.selected.visible = true;

        // Handle snap
        if (this.selected.snapTo === 'None' && this.selected.userData.snap?.to?.includes('Ceiling') && roomIntersect?.object.name === 'Ceiling') {
            this.entityManager.rooms.forEach((room) => {
                room.ceiling.visible = true;
            });
        } else {
            if (this.selected.userData.snap?.to?.includes('Ceiling')) {
                this.entityManager.rooms.forEach((room) => {
                    room.ceiling.visible = false;
                });
            }

            if (this.selected.userData.snap?.to?.includes('Wall') && wallIntersect) {
                Wall.intersectWithWall(
                    {
                        ...otherIntersects[0],
                        position: this.intersectTargetPosition.clone(),
                        offset: this.intersectTargetOffset.clone(),
                        matrix: this.intersectTargetMatrix.clone(),
                    },
                    this.selected.object,
                    false,
                );
            } else if (this.selected.userData.snap?.to?.includes('Table') && tableIntersect) {
                Table.intersectWithTable(
                    {
                        ...otherIntersects[0],
                        position: this.intersectTargetPosition.clone(),
                        offset: this.intersectTargetOffset.clone(),
                        matrix: this.intersectTargetMatrix.clone(),
                    },
                    this.selected.object,
                    false,
                );
            }

            if (otherIntersects[0]) {
                this.selected.object.updateWorldMatrix(true, true);
                this.selected.object.updateMatrixWorld(true);

                this.intersectTargetPosition.copy(this.selected.object.position);
                this.intersectTargetMatrix.copy(this.selected.object.matrixWorld);
                this.intersectTargetOffset.copy(this.selected.object.worldToLocal(otherIntersects[0].point.clone()));
            }
        }

        // Handle measure
        const walls = this.entityManager.walls.map(wall => wall.object);
        this.affectedCorners.forEach((affectedCorner) => {
            this.entityManager.walls.forEach((wall) => {
                if (wall.startCorner === affectedCorner || wall.endCorner === affectedCorner) {
                    wall.showMeasure = true;
                }
            });
        });
        this.affectedWalls.forEach((affectedWall) => {
            this.entityManager.walls.forEach((wall) => {
                if (wall.startCorner === affectedWall.startCorner
                    || wall.startCorner === affectedWall.endCorner
                    || wall.endCorner === affectedWall.startCorner
                    || wall.endCorner === affectedWall.endCorner
                ) {
                    wall.showMeasure = true;
                }
            });
        });
        if (otherIntersects[0]?.object.userData instanceof Wall && this.selected.userData?.snap?.to?.includes('Wall')) {
            this.measureManager.computeWallMeasure(this.selected, otherIntersects, walls);
            this.measureManager.clearFloorMeasure();
        } else if (this.selected.userData?.snap?.to?.includes('Floor')) {
            this.measureManager.computeFloorMeasure(this.selected, walls);
            this.measureManager.clearWallMeasure();
        } else {
            this.measureManager.clearWallMeasure();
            this.measureManager.clearFloorMeasure();
        }

        this.renderer.render();
    }

    private handleEntityDragEnd(event, entity: Entity, intersection, intersects) {
        this.selected = entity;

        this.entityManager.rooms.forEach((room) => {
            room.ceiling.visible = false;
        });

        // Handle measure
        this.affectedCorners.forEach((affectedCorner) => {
            this.entityManager.walls.forEach((wall) => {
                if (wall.startCorner === affectedCorner || wall.endCorner === affectedCorner) {
                    wall.showMeasure = false;
                }
            });
        });
        this.affectedWalls.forEach((affectedWall) => {
            this.entityManager.walls.forEach((wall) => {
                if (wall.startCorner === affectedWall.startCorner
                    || wall.startCorner === affectedWall.endCorner
                    || wall.endCorner === affectedWall.startCorner
                    || wall.endCorner === affectedWall.endCorner
                ) {
                    wall.showMeasure = false;
                }
            });
        });
        this.measureManager.clearWallMeasure();
        this.measureManager.clearFloorMeasure();

        // Handle intersects
        const otherIntersects = intersects.filter(intersect => intersect.object.userData !== this.selected);
        const wallIntersect = otherIntersects.find(intersect => intersect.object.userData instanceof Wall);
        const tableIntersect = otherIntersects.find(intersect => intersect.object.userData instanceof Table);

        if (otherIntersects[0]) {
            this.selected.object.updateWorldMatrix(true, true);
            this.selected.object.updateMatrixWorld(true);

            this.intersectTargetPosition.copy(this.selected.object.position);
            this.intersectTargetMatrix.copy(this.selected.object.matrixWorld);
            this.intersectTargetOffset.copy(this.selected.object.worldToLocal(otherIntersects[0].point.clone()));
        }

        // Handle snap
        if (this.selected.userData.snap?.to?.includes('Wall') && wallIntersect) {
            Wall.intersectWithWall(
                {
                    ...otherIntersects[0],
                    position: this.intersectTargetPosition.clone(),
                    offset: this.intersectTargetOffset.clone(),
                    matrix: this.intersectTargetMatrix.clone(),
                },
                this.selected.object,
                true,
            );
        } else if (this.selected.userData.snap?.to?.includes('Table') && tableIntersect) {
            Table.intersectWithTable(
                {
                    ...otherIntersects[0],
                    position: this.intersectTargetPosition.clone(),
                    offset: this.intersectTargetOffset.clone(),
                    matrix: this.intersectTargetMatrix.clone(),
                },
                this.selected.object,
                false,
            );
        }

        // Handle room merge
        if (this.selected instanceof Corner || this.selected instanceof Wall || this.selected instanceof Room) {
            Room.mergeRooms(this.entityManager);
        }

        this.historyManager.nextData = {
            entity: this.selected,
            position: this.selected.position.clone(),
            attachedTo: this.selected.object['attachedTo'],
        };

        const prevData = this.historyManager.prevData;
        const nextData = this.historyManager.nextData;
        if (prevData && nextData && prevData.position.distanceTo(nextData.position) > 0.01
            && !(this.selected.userData.snap?.to?.includes('Wall') && this.selected.userData.snap?.hole)) {
            this.historyManager.addToHistory(
                () => {
                    prevData.entity.position = prevData.position.clone();
                },
                () => {
                    nextData.entity.position = nextData.position.clone();
                },
            );
        }

        this.reset();
    }

    /* */

    public reset() {
        super.reset();

        this.plane.constant = 0;

        this.offset = new THREE.Vector3();
        this.inverseMatrix = new THREE.Matrix4();

        this.intersectTargetPosition = new THREE.Vector3();
        this.intersectTargetOffset = new THREE.Vector3();
        this.intersectTargetMatrix = new THREE.Matrix4();

        this.selected = null;
        this.affectedCorners = [];
        this.affectedWalls = [];

        this.renderer.orbit.enabled = true;
        this.renderer.domElement.style.cursor = 'auto';
        this.renderer.render();
    }

    /* */

    handleEscape() {
        if (this.viewManager.editorMode === EditorMode.Edit) {
            if (this.interfaceManager?.floorSpaceService.isConfirmDialogOpened) {
                return;
            }

            this.selectionManager.selectEntity(null);
            this.reset();
        }
    }

    handleDelete() {
        if (this.viewManager.editorMode === EditorMode.Edit) {
            if (this.interfaceManager?.floorSpaceService.isConfirmDialogOpened) {
                return;
            }

            this.entityManager.entities.forEach((entity) => {
                if (entity instanceof Corner) {
                    return;
                }

                if (entity instanceof Wall) {
                    if (entity.isSelected && entity.room && entity.room.walls.length < 2) {
                        this.selectionManager.selectEntity(null);
                        this.reset();

                        this.entityManager.removeEntity(entity.room);
                    }
                    return;
                }

                if (entity.isSelected) {
                    this.selectionManager.selectEntity(null);
                    this.reset();

                    this.entityManager.removeEntity(entity);
                }
            });
        }
    }

    handleCut() {
        if (this.viewManager.editorMode !== EditorMode.Edit || !this.selectionManager.selected || this.buffered) {
            return;
        }

        this.isCutting = true;
        this.buffered = this.selectionManager.selected;
    }

    handleCopy() {
        if (this.viewManager.editorMode !== EditorMode.Edit || !this.selectionManager.selected || this.buffered) {
            return;
        }

        this.isCutting = false;
        this.buffered = this.selectionManager.selected;
    }

    handlePaste() {
        if (this.viewManager.editorMode !== EditorMode.Edit || !this.buffered) {
            return;
        }

        if (this.buffered instanceof Corner || this.buffered instanceof Wall || this.buffered instanceof Room) {
            return;
        }

        if (this.isCutting) {
            const { intersection, intersects } = this.getIntersections(this.lastMoveEvent);
            this.handleEntityDragMove(this.lastMoveEvent, this.buffered, intersection, intersects);
            this.handleEntityDragEnd(this.lastMoveEvent, this.buffered, intersection, intersects);
        } else {
            this.entityManager.addEntity(this.buffered.userData, true, { ...this.buffered.toJSON(), id: v4(), visible: true })
                .then((entity) => {
                    this.handlePointerMove(this.lastMoveEvent);
                });
        }

        this.isCutting = false;
        this.buffered = null;
    }
}
