import * as THREE from 'three';
import { Line2 } from 'three/examples/jsm/lines/Line2';
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry';
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';

import Renderer from '../engine/Renderer';

import Wall from '../entities/Wall';

import Manager from './base/Manager';

import MathUtils from '../utils/MathUtils';
import IconUtils from '../utils/IconUtils';

export default class MeasureManager extends Manager {
    isSetted = false;
    isSelected = false;
    isConfirmed = false;

    systemOfUnits = 'metric';
    units = 1;

    private _marker = new THREE.Mesh(
        new THREE.SphereGeometry(0.05, 16, 16),
        new THREE.MeshBasicMaterial({
            color: 0xe74c3c,
            depthTest: false,
            depthWrite: false,
            transparent: true,
        }),
    );

    private _startMarker: THREE.Mesh;
    private _endMarker: THREE.Mesh;

    private _distance = 0;

    set startMarker(position: THREE.Vector3) {
        position = position.clone();
        position.x = Math.round(position.x / 0.1) * 0.1;
        position.y = Math.round(position.y / 0.1) * 0.1;
        position.z = Math.round(position.z / 0.1) * 0.1;

        this._startMarker.position.copy(position);
        this._startMarker.visible = true;

        this._distance = this._startMarker.position.distanceTo(this._endMarker.position);

        this.measureBetween(
            'units',
            this._startMarker.visible ? this._startMarker.position : null,
            this._endMarker.visible ? this._endMarker.position : null,
        );
    }

    set endMarker(position: THREE.Vector3) {
        position = position.clone();
        position.x = Math.round(position.x / 0.1) * 0.1;
        position.y = Math.round(position.y / 0.1) * 0.1;
        position.z = Math.round(position.z / 0.1) * 0.1;

        this._endMarker.position.copy(position);
        this._endMarker.visible = true;

        this._distance = this._startMarker.position.distanceTo(this._endMarker.position);

        this.measureBetween(
            'units',
            this._startMarker.visible ? this._startMarker.position : null,
            this._endMarker.visible ? this._endMarker.position : null,
        );
    }

    constructor(private renderer: Renderer) {
        super();

        this._startMarker = this._marker.clone(true);
        this._startMarker.visible = false;
        this._startMarker.renderOrder = 1000;
        this.renderer.scene.add(this._startMarker);

        this._endMarker = this._marker.clone(true);
        this._endMarker.visible = false;
        this._endMarker.renderOrder = 1000;
        this.renderer.scene.add(this._endMarker);

        this.renderer.orbit.addEventListener('change', () => {
            this.scaleMeasureStick();
        });
    }

    scaleMeasureStick() {
        if (!this._startMarker.visible || !this._endMarker.visible) {
            return;
        }

        this.renderer.camera.updateMatrixWorld();
        this.renderer.camera.updateProjectionMatrix();

        const cameraAngle = this.renderer.camera.quaternion.angleTo(this.renderer.scene.quaternion);

        const startMarkerInScreenSpace = new THREE.Vector3(0, 0, 0).project(this.renderer.camera);
        const endMarkerInScreenSpace = new THREE.Vector3(this._distance, 0, 0).project(this.renderer.camera);

        let markerDistance = startMarkerInScreenSpace.distanceTo(endMarkerInScreenSpace);
        if (MathUtils.equalNumbers(2.09439, cameraAngle)) {
            markerDistance -= markerDistance * 0.24;
        }

        const startPosition = new THREE.Vector3(0 - markerDistance / 2, 0.93, 0.985).unproject(this.renderer.camera);
        const endPosition = new THREE.Vector3(markerDistance / 2, 0.93, 0.985).unproject(this.renderer.camera);

        this._startMarker.position.copy(startPosition);
        this._endMarker.position.copy(endPosition);

        this.measureBetween(
            'units',
            this._startMarker.visible ? this._startMarker.position : null,
            this._endMarker.visible ? this._endMarker.position : null,
            this._distance,
        );
    }

    setScale(): void {
        this.reset();

        this.isSetted = true;

        this.dispatchEvent('setScale');
    }

    confirmScale(systemOfUnits: string, units: number): void {
        this.isConfirmed = true;
        this.systemOfUnits = systemOfUnits;
        this.units = units;

        this.scaleMeasureStick();
    }

    confirmDrawing() {
        this.isSelected = true;

        this.scaleMeasureStick();
    }

    /* */

    measureBetween(name: string, fromPoint: THREE.Vector3 | null, toPoint: THREE.Vector3 | null, value?) {
        if (!this.isSetted) {
            return;
        }

        name = `${name}MeasureBetween`;

        if (fromPoint == null || toPoint == null || fromPoint.distanceTo(toPoint) < 1e-2) {
            if (this[name]) {
                this[name].visible = false;
            }
            return;
        }

        const d = this._distance;
        const c = d / this.units;

        const distance = value !== undefined ? value : fromPoint.distanceTo(toPoint);
        const scale = +(distance / c).toFixed(2);

        if (this[name] == null) {
            this[name] = new Line2(
                new LineGeometry(),
                new LineMaterial({
                    color: 0xff0000,
                    linewidth: 1,
                    resolution: new THREE.Vector2(640, 480),
                    depthTest: false,
                    depthWrite: false,
                    transparent: true,
                }),
            );
            this[name].renderOrder = 1000;
            this.renderer.scene.add(this[name]);
        }
        this[name].geometry.setPositions([
            ...fromPoint.clone().toArray(),
            ...toPoint.clone().toArray(),
        ]);
        this[name].computeLineDistances();
        this[name].visible = true;
        this.renderer.render();

        if (!this.isConfirmed) {
            if (this[name].text) {
                this[name].text.visible = false;
            }
            return;
        }

        if (this[name].text == null) {
            this[name].text = IconUtils.createDynamicIcon(() => this.renderer.render());
            this[name].add(this[name].text);
        }
        if (this.systemOfUnits === 'metric') {
            const meters = Math.floor(scale / 100);
            const cms = Math.floor(scale % 100);
            this[name].text.material.map.setText(`${meters}m ${cms}cm`);
        } else {
            const feet = Math.floor(scale / 12);
            const inches = Math.floor(scale % 12);
            this[name].text.material.map.setText(`${feet}′ ${inches}″`);
        }
        this[name].text.position.copy(fromPoint.clone().lerp(toPoint, 0.5));
        if (this.renderer.camera.fov === 1) {
            this[name].text.scale.set(0.000525 * this[name].text.material.map.aspectRatio, 0.000525, 1.0);
        } else {
            this[name].text.scale.set(0.035 * this[name].text.material.map.aspectRatio, 0.035, 1.0);
        }
        this[name].text.visible = true;
        this.renderer.render();
    }

    measureText(name: string, value: number | null, position = new THREE.Vector3()) {
        if (!this.isConfirmed) {
            return;
        }

        name = `${name}MeasureText`;

        if (value == null) {
            if (this[name]) {
                this[name].visible = false;
            }
            return;
        }

        const d = this._distance;
        const c = d / this.units;

        const distance = value;
        const scale = +(distance / c).toFixed(2);

        if (this[name] == null) {
            this[name] = IconUtils.createDynamicIcon(() => this.renderer.render());
            this.renderer.scene.add(this[name]);
        }
        this[name].visible = true;

        if (this.systemOfUnits === 'metric') {
            const meters = Math.floor(scale / 100);
            const cms = Math.floor(scale % 100);
            this[name].material.map.setText(`${meters}m ${cms}cm`);
        } else {
            const feet = Math.floor(scale / 12);
            const inches = Math.floor(scale % 12);
            this[name].material.map.setText(`${feet}′ ${inches}″`);
        }

        this[name].position.copy(position);

        if (this.renderer.camera.fov === 1) {
            this[name].scale.set(0.000525 * this[name].material.map.aspectRatio, 0.000525, 1.0);
        } else {
            this[name].scale.set(0.035 * this[name].material.map.aspectRatio, 0.035, 1.0);
        }

        this.renderer.render();
    }

    /* */

    computeFloorMeasure(entity, intersects) {
        const raycaster = new THREE.Raycaster();

        // input data
        entity.object.updateMatrixWorld();

        // get X intersections
        raycaster.set(entity.position.clone().setY(0.1), new THREE.Vector3(1, 0, 0));
        const intersectionsXPositive = raycaster.intersectObjects(intersects);
        raycaster.set(entity.position.clone().setY(0.1), new THREE.Vector3(-1, 0, 0));
        const intersectionsXNegative = raycaster.intersectObjects(intersects);

        // define minimum distance intersect
        let minXIntersection;
        if (intersectionsXPositive.length && intersectionsXNegative.length) {
            minXIntersection = intersectionsXPositive[0].distance <= intersectionsXNegative[0].distance
                ? intersectionsXPositive[0]
                : intersectionsXNegative[0];
        } else if (intersectionsXPositive.length) {
            minXIntersection = intersectionsXPositive[0];
        } else if (intersectionsXNegative.length) {
            minXIntersection = intersectionsXNegative[0];
        }
        this.measureBetween('lineX', entity.position.clone(), minXIntersection ? minXIntersection.point.clone().setY(entity.position.y) : null);

        // get Z intersections
        raycaster.set(entity.position.clone().setY(0.1), new THREE.Vector3(0, 0, 1));
        const intersectionsZPositive = raycaster.intersectObjects(intersects);

        raycaster.set(entity.position.clone().setY(0.1), new THREE.Vector3(0, 0, -1));
        const intersectionsZNegative = raycaster.intersectObjects(intersects);

        // define minimum distance intersect
        let minZIntersection;
        if (intersectionsZPositive.length && intersectionsZNegative.length) {
            minZIntersection = intersectionsZPositive[0].distance <= intersectionsZNegative[0].distance
                ? intersectionsZPositive[0]
                : intersectionsZNegative[0];
        } else if (intersectionsZPositive.length) {
            minZIntersection = intersectionsZPositive[0];
        } else if (intersectionsZNegative.length) {
            minZIntersection = intersectionsZNegative[0];
        }
        this.measureBetween('lineZ', entity.position.clone(), minZIntersection ? minZIntersection.point.clone().setY(entity.position.y) : null);
    }

    clearFloorMeasure() {
        this.measureBetween('lineX', null, null);
        this.measureBetween('lineZ', null, null);
    }

    /* */

    computeWallMeasure(entity, intersects, walls) {
        const raycaster = new THREE.Raycaster();

        if (intersects.length && intersects[0].object.userData instanceof Wall) {
            const intersect = intersects[0];

            // input data
            entity.object.updateMatrixWorld();
            entity.object.geometry.computeBoundingBox();

            const boundingBox = entity.object.geometry.boundingBox?.clone()
                .applyMatrix4(entity.object.matrixWorld);
            const center = intersect.object.worldToLocal(boundingBox?.getCenter(new THREE.Vector3()))
                || new THREE.Vector3();
            const size = entity.object.geometry.boundingBox?.getSize(new THREE.Vector3())
                || new THREE.Vector3();

            const wallLength = intersect.object.userData.length;
            const wallHeight = intersect.object.userData.height;

            const targetWidth = size.x * entity.object.scale.x;
            const targetHeight = size.y * entity.object.scale.y;

            // calc points
            const upHinge = intersect.object.localToWorld(center.clone()
                .add(new THREE.Vector3(0, targetHeight / 2, 0)));
            const upEndpoint = intersect.object.localToWorld(new THREE.Vector3(center.x, wallHeight, center.z));
            this.measureBetween('lineUp', upHinge.clone(), upEndpoint.clone());

            const downHinge = intersect.object.localToWorld(center.clone()
                .add(new THREE.Vector3(0, -targetHeight / 2, 0)));
            const downEndpoint = intersect.object.localToWorld(new THREE.Vector3(center.x, 0, center.z));
            this.measureBetween('lineDown', downHinge.clone(), downEndpoint.clone());

            const leftHinge = intersect.object.localToWorld(center.clone()
                .add(new THREE.Vector3(0, 0, -targetWidth / 2)));
            const leftEndpoint = intersect.object.localToWorld(new THREE.Vector3(center.x, center.y, 0));
            raycaster.far = leftHinge.distanceTo(leftEndpoint);
            raycaster.set(leftHinge, leftEndpoint.clone().sub(leftHinge).normalize());
            const leftIntersects = raycaster.intersectObjects(walls
                .filter(object => object.uuid !== intersect.object.uuid));
            this.measureBetween('lineLeft', leftHinge.clone(), leftIntersects.length ? leftIntersects[0].point.clone() : leftEndpoint.clone());

            const rightHinge = intersect.object.localToWorld(center.clone()
                .add(new THREE.Vector3(0, 0, targetWidth / 2)));
            const rightEndpoint = intersect.object.localToWorld(new THREE.Vector3(center.x, center.y, wallLength));
            raycaster.far = rightHinge.distanceTo(rightEndpoint);
            raycaster.set(rightHinge, rightEndpoint.clone().sub(rightHinge).normalize());
            const rightIntersects = raycaster.intersectObjects(walls
                .filter(object => object.uuid !== intersect.object.uuid));
            this.measureBetween('lineRight', rightHinge.clone(), rightIntersects.length ? rightIntersects[0].point.clone() : rightEndpoint.clone());
        } else {
            this.clearWallMeasure();
        }
    }

    clearWallMeasure() {
        this.measureBetween('lineUp', null, null);
        this.measureBetween('lineDown', null, null);
        this.measureBetween('lineLeft', null, null);
        this.measureBetween('lineRight', null, null);
    }

    /* */

    fromJSON(data) {
        if (data.isConfirmed) {
            this.isSetted = true;
            this.isSelected = true;

            this.startMarker = new THREE.Vector3(0, 0, 0);
            this.endMarker = new THREE.Vector3(data.unitsDistance, 0, 0);

            this.confirmScale(data.systemOfUnits, data.unitsPerDistance);
        }
    }

    toJSON() {
        return {
            isConfirmed: this.isConfirmed,
            systemOfUnits: this.systemOfUnits,
            unitsPerDistance: this.units,
            unitsCoefficient: this.units / this._distance,
            unitsDistance: this._distance,
        }
    }

    reset() {
        super.reset();

        this.measureBetween('units', null, null);

        this.clearFloorMeasure();
        this.clearWallMeasure();

        this.isSetted = false;
        this.isSelected = false;
        this.isConfirmed = false;

        this._startMarker.visible = false;
        this._endMarker.visible = false;

        this.renderer.render();
    }
}
