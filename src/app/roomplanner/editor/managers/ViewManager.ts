import * as THREE from 'three';

import Room from '../entities/Room';
import FloorPlan from '../entities/FloorPlan';

import Grid from '../helpers/Grid';

import Manager from './base/Manager';
import EntityManager from './EntityManager';

export enum EditorMode {
    Edit,
    View
}

export enum ViewMode {
    Top,
    Iso
}

export enum TransformMode {
    Select = 'select',
    Translate = 'translate',
    Rotate = 'rotate',
    Scale = 'scale',
}

export default class ViewManager extends Manager {
    editorMode = EditorMode.View;
    viewMode = ViewMode.Iso;
    transformMode: TransformMode | string = TransformMode.Select;

    constructor(private renderer, private grid: Grid, private entityManager: EntityManager) {
        super();

        this.reset();
    }

    zoomIn() {
        this.renderer.orbit.setRadius(this.renderer.orbit.getRadius() - (this.viewMode === ViewMode.Top ? 30 : 0.5));
        this.renderer.orbit.update();
    }

    zoomOut() {
        this.renderer.orbit.setRadius(this.renderer.orbit.getRadius() + (this.viewMode === ViewMode.Top ? 30 : 0.5));
        this.renderer.orbit.update();
    }

    rotateLeft() {
        const angle = Math.PI / 2;
        this.renderer.orbit.angle += THREE.MathUtils.radToDeg(angle);
        if (this.renderer.orbit.angle >= 360) {
            this.renderer.orbit.angle = 0;
        }
        this.renderer.orbit.rotateLeft(angle);
        this.renderer.orbit.update();
    }

    rotateRight() {
        const angle = -Math.PI / 2;
        this.renderer.orbit.angle += THREE.MathUtils.radToDeg(angle);
        if (this.renderer.orbit.angle <= -360) {
            this.renderer.orbit.angle = 0;
        }
        this.renderer.orbit.rotateLeft(angle);
        this.renderer.orbit.update();
    }

    changeGrid(visible?: boolean) {
        this.grid.visible = visible === undefined ? !this.grid.visible : visible;
    }

    changeEditorMode(editorMode?: EditorMode) {
        if (editorMode === undefined) {
            if (this.editorMode === EditorMode.View) {
                this.editorMode = EditorMode.Edit;
            } else if (this.editorMode === EditorMode.Edit) {
                this.editorMode = EditorMode.View;
            }
        } else {
            this.editorMode = editorMode;
        }

        this.next({
            type: 'editor-mode/changed',
            payload: this.editorMode,
        });
    }

    changeViewMode(viewMode?: ViewMode) {
        if (viewMode === undefined) {
            if (this.viewMode === ViewMode.Iso) {
                this.viewMode = ViewMode.Top;
            } else if (this.viewMode === ViewMode.Top) {
                this.viewMode = ViewMode.Iso;
            }
        } else {
            this.viewMode = viewMode;
        }

        if (this.viewMode === ViewMode.Iso) {
            this.renderer.camera.near = 0.1;
            this.renderer.camera.fov = 60;
            this.renderer.camera.position.set(-5, 5, 5);
            this.renderer.camera.rotation.set(-Math.PI / 4, Math.PI / 5, Math.PI / 6);
            this.renderer.camera.updateProjectionMatrix();

            this.renderer.orbit.enableRotate = true;
            this.renderer.orbit.minDistance = 0.25;
            this.renderer.orbit.maxDistance = 50;
            this.renderer.orbit.target.set(0, 0, 0);
            this.renderer.orbit.update();

            this.renderer.transform.setRotationSnap(Math.PI / 4);
            this.renderer.transform.setRotationSpeed(20);
            this.renderer.transform.setScaleSpeed(0.5);
            this.renderer.transform.setSize(1);

            this.entityManager.entities.forEach((entity) => {
                if (entity.userData.snap?.to?.includes('Wall')) {
                    entity.renderReset();
                }
            });
        } else if (this.viewMode === ViewMode.Top) {
            this.renderer.camera.near = 5;
            this.renderer.camera.fov = 1;
            this.renderer.camera.position.set(0, 500, 0);
            this.renderer.camera.rotation.set(-Math.PI / 2, 0, 0);
            this.renderer.camera.updateProjectionMatrix();

            this.renderer.orbit.enableRotate = false;
            this.renderer.orbit.minDistance = 25;
            this.renderer.orbit.maxDistance = 5000;
            this.renderer.orbit.target.set(0, 0, 0);
            this.renderer.orbit.update();

            this.renderer.transform.setRotationSnap(Math.PI / 4);
            this.renderer.transform.setRotationSpeed(2000);
            this.renderer.transform.setScaleSpeed(0.1);
            this.renderer.transform.setSize(0.7);

            this.entityManager.entities.forEach((entity) => {
                if (entity.userData.snap?.to?.includes('Wall')) {
                    entity.renderOnTop();
                }
            });
        }

        this.renderer.resize();

        this.next({
            type: 'view-mode/changed',
            payload: this.viewMode,
        });
    }

    changeTransformMode(transformMode: TransformMode | string) {
        this.transformMode = transformMode;

        this.renderer.transform.enabled = true;
        this.renderer.transform.showX = true;
        this.renderer.transform.showY = true;
        this.renderer.transform.showZ = true;

        if (this.transformMode === 'select' || this.transformMode === 'translate') {
            this.renderer.transform.enabled = false;
            this.renderer.transform.showX = false;
            this.renderer.transform.showY = false;
            this.renderer.transform.showZ = false;
        } else if (this.transformMode === 'scale') {
            if (this.renderer.transform.object) {
                if (this.renderer.transform.object.userData instanceof FloorPlan) {
                    this.renderer.transform.showZ = false;
                } else if (this.renderer.transform.object.userData instanceof Room) {
                    this.renderer.transform.showX = false;
                    this.renderer.transform.showZ = false;
                }
            }

            this.renderer.transform.setMode('scale');
        } else {
            if (this.transformMode === 'rotate') {
                this.renderer.transform.showX = false;
                this.renderer.transform.showZ = false;
            }

            this.renderer.transform.setMode('rotate');
        }
    }

    reset() {
        this.changeViewMode(ViewMode.Iso);
        this.changeEditorMode(EditorMode.View);
        this.changeTransformMode(TransformMode.Select);
    }
}
