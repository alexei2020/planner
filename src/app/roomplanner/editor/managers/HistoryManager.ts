import UndoManager from 'undo-manager';

import Manager from './base/Manager';

export default class HistoryManager extends Manager {
    private undoManager: UndoManager;

    prevData: any;
    nextData: any;

    constructor(private renderer) {
        super();

        this.undoManager = new UndoManager();
        this.undoManager.setCallback(() => this.renderer.render());
    }

    hasUndo() {
        return this.undoManager.hasUndo();
    }

    hasRedo() {
        return this.undoManager.hasRedo();
    }

    undo() {
        this.undoManager.undo();
    }

    redo() {
        this.undoManager.redo();
    }

    addToHistory(undo, redo) {
        this.undoManager.add({
            undo: () => undo(),
            redo: () => redo(),
        });
    }

    reset() {
        super.reset();
        this.undoManager.clear();
    }
}
