import Manager from './base/Manager';

export default class InterfaceManager extends Manager {
    floorSpaceService = null;
    roomPlannerService = null;

    confirmDialog = null;
    selectDialog = null;
}
