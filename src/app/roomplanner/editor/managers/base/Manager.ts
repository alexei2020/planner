import Dispatcher from '../../engine/Dispatcher';

export default abstract class Manager extends Dispatcher {
    public reset(): void { }
}
