

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { SnapshotTask } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onSnapshotTaskDetailLoadCallback = (instance?: SnapshotTask) => void;
type onSnapshotTaskDetailLoadFailCallback = (instance?: SnapshotTask) => void;
@Injectable({
    providedIn: 'root'
})
export class SnapshotTaskDetailService {
    public onSnapshotTaskUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: SnapshotTask = {} as SnapshotTask;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('SnapshotTask');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/snapshottasks?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getSnapshotTasks(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/snapshottasks?${filterString}`, body, httpOptions);
     }

    getSnapshotTaskByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/snapshottasks/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/snapshottasks/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<SnapshotTask>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/snapshottasks/${resourceName}`);
	 } else {
         	return this.http_.get<SnapshotTask>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/snapshottasks/${resourceName}`);
	 }
     }

//     load(onLoad?: onSnapshotTaskDetailLoadCallback, onFail?: onSnapshotTaskDetailLoadFailCallback): void {
//         this.http_.get<SnapshotTask>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onSnapshotTaskUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onSnapshotTaskUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(snapshottask: SnapshotTask, _location: string): Observable<SnapshotTask> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<SnapshotTask>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/snapshottasks/`+snapshottask.metadata.name, snapshottask, httpOptions);
      }



    add(snapshottask: SnapshotTask, _location: string): Observable<SnapshotTask> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<SnapshotTask>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/snapshottasks/`, snapshottask, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/snapshottasks/${id}`);
    }

    getInstance(): SnapshotTask {
        return this.instance_;
    }
}
