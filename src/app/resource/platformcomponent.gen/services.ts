

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { PlatformComponent } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onPlatformComponentDetailLoadCallback = (instance?: PlatformComponent) => void;
type onPlatformComponentDetailLoadFailCallback = (instance?: PlatformComponent) => void;
@Injectable({
    providedIn: 'root'
})
export class PlatformComponentDetailService {
    public onPlatformComponentUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: PlatformComponent = {} as PlatformComponent;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('PlatformComponent');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/platformcomponents?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getPlatformComponents(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/platformcomponents?${filterString}`, body, httpOptions);
     }

    getPlatformComponentByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/platformcomponents/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/platformcomponents/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<PlatformComponent>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/platformcomponents/${resourceName}`);
	 } else {
         	return this.http_.get<PlatformComponent>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/platformcomponents/${resourceName}`);
	 }
     }

//     load(onLoad?: onPlatformComponentDetailLoadCallback, onFail?: onPlatformComponentDetailLoadFailCallback): void {
//         this.http_.get<PlatformComponent>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onPlatformComponentUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onPlatformComponentUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(platformcomponent: PlatformComponent, _location: string): Observable<PlatformComponent> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<PlatformComponent>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/platformcomponents/`+platformcomponent.metadata.name, platformcomponent, httpOptions);
      }



    add(platformcomponent: PlatformComponent, _location: string): Observable<PlatformComponent> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<PlatformComponent>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/platformcomponents/`, platformcomponent, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/platformcomponents/${id}`);
    }

    getInstance(): PlatformComponent {
        return this.instance_;
    }
}
