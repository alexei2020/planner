
// tslint:disable
import {NgModule} from '@angular/core';
import {Route, RouterModule, Routes} from '@angular/router';
import { LocationResolver } from 'app/components/location/location.resolver';
import { ConfirmDeactivateGuard } from 'app/services/confirm-deactivation.service';
import { PlatformcomponentDetailPageComponent } from '../../pages/platformcomponent.gen/detail/component';
import { PlatformcomponentListPageComponent } from '../../pages/platformcomponent.gen/list/component';
const LIST_ROUTE: Route = {
  path: '',
  component:  PlatformcomponentListPageComponent,
  resolve: {resolveData: LocationResolver},
  data: {
    breadcrumb: '',
    link: ['', 'platformcomponents'],
  },
  canDeactivate: [ConfirmDeactivateGuard],
};

const CHART_PREVIEW_ROUTE: Route = {
  path: 'runtime-chart',
  resolve: {resolveData: LocationResolver},
  loadChildren: () => import('../runtime-chart-view-shared/runtime-chart-view-shared.module').then(m => m.RuntimeChartViewSharedModule),
  data: {
    breadcrumb: 'Chart Details',
    parent: LIST_ROUTE,
  },
};
const DETAIL_ROUTE: Route = {
  path: ':resourceName',
  resolve: {resolveData: LocationResolver},
  component: PlatformcomponentDetailPageComponent,
  data: {
    breadcrumb: ':resourceName',
    parent: LIST_ROUTE,
  },
  canDeactivate: [ConfirmDeactivateGuard],
};

@NgModule({
  imports: [RouterModule.forChild([LIST_ROUTE, CHART_PREVIEW_ROUTE, DETAIL_ROUTE])],
  exports: [RouterModule],
})
export class  PlatformcomponentRoutingModule {}


