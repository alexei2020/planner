

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { EndPointAux } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onEndPointAuxDetailLoadCallback = (instance?: EndPointAux) => void;
type onEndPointAuxDetailLoadFailCallback = (instance?: EndPointAux) => void;
@Injectable({
    providedIn: 'root'
})
export class EndPointAuxDetailService {
    public onEndPointAuxUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: EndPointAux = {} as EndPointAux;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('EndPointAux');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/endpointauxs?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getEndPointAuxs(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/endpointauxs?${filterString}`, body, httpOptions);
     }

    getEndPointAuxByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/endpointauxs/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/endpointauxs/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<EndPointAux>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/endpointauxs/${resourceName}`);
	 } else {
         	return this.http_.get<EndPointAux>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/endpointauxs/${resourceName}`);
	 }
     }

//     load(onLoad?: onEndPointAuxDetailLoadCallback, onFail?: onEndPointAuxDetailLoadFailCallback): void {
//         this.http_.get<EndPointAux>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onEndPointAuxUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onEndPointAuxUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(endpointaux: EndPointAux, _location: string): Observable<EndPointAux> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<EndPointAux>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/endpointauxs/`+endpointaux.metadata.name, endpointaux, httpOptions);
      }



    add(endpointaux: EndPointAux, _location: string): Observable<EndPointAux> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<EndPointAux>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/endpointauxs/`, endpointaux, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/endpointauxs/${id}`);
    }

    getInstance(): EndPointAux {
        return this.instance_;
    }
}
