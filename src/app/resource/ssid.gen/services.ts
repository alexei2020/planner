

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { SSID } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onSSIDDetailLoadCallback = (instance?: SSID) => void;
type onSSIDDetailLoadFailCallback = (instance?: SSID) => void;
@Injectable({
    providedIn: 'root'
})
export class SSIDDetailService {
    public onSSIDUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: SSID = {} as SSID;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('SSID');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/ssids?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getSSIDs(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/ssids?${filterString}`, body, httpOptions);
     }

    getSSIDByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/ssids/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/ssids/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<SSID>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/ssids/${resourceName}`);
	 } else {
         	return this.http_.get<SSID>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/ssids/${resourceName}`);
	 }
     }

//     load(onLoad?: onSSIDDetailLoadCallback, onFail?: onSSIDDetailLoadFailCallback): void {
//         this.http_.get<SSID>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onSSIDUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onSSIDUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(ssid: SSID, _location: string): Observable<SSID> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<SSID>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/ssids/`+ssid.metadata.name, ssid, httpOptions);
      }



    add(ssid: SSID, _location: string): Observable<SSID> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<SSID>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/ssids/`, ssid, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/ssids/${id}`);
    }

    getInstance(): SSID {
        return this.instance_;
    }
}
