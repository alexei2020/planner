import { Component, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { GridsterItem, GridsterConfig, GridType, CompactType } from 'angular-gridster2';
import { BreadcrumbService } from 'app/main/shared/breadcrumb-bar.service';
import { PageType } from 'app/main/shared/breadcrumb-bar.service.gen';

@Component({
  selector: 'app-file-manager-page',
  templateUrl: './file-manager-page.component.html',
  styleUrls: ['./file-manager-page.component.scss'],
})
export class FileManagerPageComponent implements OnInit, OnDestroy {

  resizeEvent: EventEmitter<any> = new EventEmitter<any>();
  item1: GridsterItem;
  options: GridsterConfig;

  constructor(private breadcrumbService: BreadcrumbService) {
    this.options = {
      gridType: GridType.Fit,
      compactType: CompactType.None,
      margin: 5,
      outerMarginBottom:0,
      outerMarginTop:0,
      outerMarginLeft: 30,
      outerMarginRight: 30,
      minCols: 96,
      maxCols: 96,
      minRows: 96,
      maxRows: 1000,
      maxItemCols: 96,
      minItemCols: 1,
      maxItemRows: 1000,
      minItemRows: 1,
      maxItemArea: 500000,
      minItemArea: 1,
      itemResizeCallback: (item) => {
          this.resizeEvent.emit(item);
      },
      pushItems: true,
      draggable: {
          enabled: false,
      },
      resizable: {
          enabled: false,
      },
    };

    this.item1 = { cols: 96, rows: 96, y: 0, x: 0 };
  }

  ngOnInit(): void {
    this.breadcrumbService.currentPage.next(PageType.FILE_MANAGER);
  }

  ngOnDestroy(): void {    
    this.breadcrumbService.currentPage.next(PageType.DEFAULT);
  }
}
