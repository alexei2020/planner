import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FileManagerApiService } from '../file-manager-api.service';
import { FileExtension } from '../shared/file-type';
import { FileManagerService } from '../file-manager.service';
import { BreadcrumbService } from 'app/main/shared/breadcrumb-bar.service';
import { PageType } from 'app/main/shared/breadcrumb-bar.service.gen';
import { IBreadCrumb } from '@fuse/components/breadcrumbs/IBreadCrumb';
import { DiffTableFormat, DiffResults } from 'ngx-text-diff/lib/ngx-text-diff.model';
import {FuseLocationService} from '../../../services/location.service';

@Component({
  selector: 'app-file-manager-compare',
  templateUrl: './file-manager-compare.component.html',
  styleUrls: ['./file-manager-compare.component.scss'],
})
export class FileManagerCompareComponent implements OnInit {
  left = '';
  right = '';
  diffTableFormat: DiffTableFormat = 'LineByLine';
  hideMatchingLines = false;
  diffsCount = 0;

  state: {
    isLoading: boolean;
    isFailed: boolean;
    hasNotSupportedFile: boolean;
  };

  constructor(
    private route: ActivatedRoute,
    private fileManagerApiService: FileManagerApiService,
    private fileManagerService: FileManagerService,
    private breadcrumbService: BreadcrumbService,
    private _locationService: FuseLocationService,
  ) {
    this.state = {
      isLoading: false,
      isFailed: false,
      hasNotSupportedFile: false,
    };
  }

  async ngOnInit(): Promise<void> {
    this.breadcrumbService.currentPage.next(PageType.FILE_MANAGER_COMPARE);
    try {
      this.state.isLoading = true;
      const filePath1 = this.route.snapshot.queryParams['filePath1'];
      const filePath2 = this.route.snapshot.queryParams['filePath2'];
      const currentLocation = this._locationService.locationDataAsKeyValue().currentLocation;

      this.breadcrumbService.addBreadcrumb({
        label: `${filePath1} / ${filePath2}`
      } as IBreadCrumb);

      const fileType1 = this.fileManagerService.getFileExtension(filePath1);
      const fileType2 = this.fileManagerService.getFileExtension(filePath2);
      if (fileType1 !== FileExtension.Plain || fileType2 !== FileExtension.Plain) {
        this.state.hasNotSupportedFile = true;
      } else {
        const downloadFile1Promise = this.fileManagerApiService.downloadFile(currentLocation, filePath1);
        const downloadFile2Promise = this.fileManagerApiService.downloadFile(currentLocation, filePath2);

        const [blob1, blob2] = await Promise.all([downloadFile1Promise, downloadFile2Promise]);

        this.left = await (blob1 as any).text();
        this.right = await (blob2 as any).text();
      }
    } catch (ex) {
      this.state.isFailed = true;
    } finally {
      this.state.isLoading = false;
    }
  }

  onCompareResults(diff: DiffResults): void {
    this.diffsCount = diff.diffsCount;
  }
}
