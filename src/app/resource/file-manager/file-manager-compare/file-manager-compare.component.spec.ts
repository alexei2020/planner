import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerCompareComponent } from './file-manager-compare.component';

describe('FileManagerCompareComponent', () => {
  let component: FileManagerCompareComponent;
  let fixture: ComponentFixture<FileManagerCompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileManagerCompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
