import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FileManagerApiService } from '../file-manager-api.service';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { DialogService } from '@progress/kendo-angular-dialog';
import { FuseLocationService } from 'app/services/location.service';
import { FileManagerDeleteConfirmationComponent } from '../file-manager-delete-confirmation/file-manager-delete-confirmation.component';
import { FileManagerUploadDialogComponent } from '../file-manager-upload-dialog/file-manager-upload-dialog.component';
import { map, isEmpty, forEach, some, filter, includes, split } from 'lodash';
import { TreeItem } from '@progress/kendo-angular-treeview';
import _ from 'lodash';
import { FileManagerNewFolderDialogComponent } from '../file-manager-new-folder-dialog/file-manager-new-folder-dialog.component';
import { FileDownloadStatus, FileItem } from '../shared/file-item';
import { FileExtension } from '../shared/file-type';
import { FileManagerService, ToolbarAction } from '../file-manager.service';
import { BreadcrumbService } from 'app/main/shared/breadcrumb-bar.service';
import * as fileSaver from 'file-saver';
import { ICurrentLocationInfo } from 'app/typings/location';

@Component({
  selector: 'app-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.scss'],
})
export class FileManagerComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<any>;

  allGridData: FileItem[];

  selectedFolder: FileItem;
  copyCutStore: {
    selectedFiles: string[];
    directory: string;
    isCopy: boolean;
  };

  expandedKeys: string[] = [];
  selectedKeys: any[] = [];
  selectedGridItems: FileItem[];
  breadcrumbs: FileItem[] = [];
  rootFolder: FileItem[] = [];

  toolbarState: {
    canCopyOrCut: boolean;
    canPaste: boolean;
    canDelete: boolean;
    canDownload: boolean;
    canDiff: boolean;
  };

  state: {
    isGridLoading: boolean;
    isLoadFailed: boolean;
  };

  tenantName: string;
  currentLocation: string;

  constructor(
    private _fileManagerApiService: FileManagerApiService,
    private _fileManagerService: FileManagerService,
    private _dialogService: DialogService,
    private _router: Router,
    private _locationService: FuseLocationService,
    private _breadcrumbService: BreadcrumbService
  ) {
    this._unsubscribeAll = new Subject();

    this.state = {
      isGridLoading: false,
      isLoadFailed: false,
    };
    this.selectedKeys = ['0'];

    this.toolbarState = {
      canCopyOrCut: false,
      canPaste: false,
      canDelete: false,
      canDownload: false,
      canDiff: false,
    };
    this.copyCutStore = {
      selectedFiles: [],
      directory: null,
      isCopy: false,
    };
    this.selectedGridItems = [];
    this._fileManagerService.selectedGridItems.next([]);
  }

  ngOnInit(): void {
    this._locationService.locationData
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(async (res: ICurrentLocationInfo) => {
        this.tenantName = res.tenantName;
        this.currentLocation = res.currentLocation;
        this.rootFolder = await this._fileManagerApiService.fetchTreeViewDataInRoot(this.currentLocation);
        this.selectedFolder = this.rootFolder[0];
        this.updateBreadcrumbs([this.selectedFolder]);
        this.loadGridData();
        this.updateToolbarState();
      });

    this._breadcrumbService.fileManagerBreadcrumbsFired
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((folder: FileItem) => this.onBreadcrumbClick(folder));

    this._fileManagerService.downloadingFilesStore
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((files: FileItem[]) => {
        this.updateFilesDownloadStatus(files);
      });

    this._fileManagerService.toolbarAction.pipe(takeUntil(this._unsubscribeAll)).subscribe((action: ToolbarAction) => {
      switch (action.type) {
        case 'CreateNewFolder':
          this.onCreateNewFolder();
          break;
        case 'UploadFile':
          this.onUploadFile();
          break;
        case 'Refresh':
          this.onRefresh();
          break;
        case 'Cut':
          this.onCut();
          break;
        case 'Copy':
          this.onCopy();
          break;
        case 'Paste':
          this.onPaste();
          break;
        case 'Delete':
          this.onDeleteItems();
          break;
        case 'DownloadFile':
          this.onDownloadFile();
          break;
        case 'Diff':
          this.onDiff();
          break;
        default:
          break;
      }
    });
    this._fileManagerService.selectedGridItems.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: FileItem[]) => {
      this.selectedGridItems = data;
    });
  }

  ngOnDestroy(): void {
    this._fileManagerService.toolbarAction.next({ type: null, payload: null } as ToolbarAction);
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  onSortByItemClick(item: string): void {}

  hasChildren = (item: any) => {
    return true;
  };

  isLevelMenuNumber(index: string, levelNumber: number): boolean {
    const chunks = split(index, '_');

    return chunks.length === levelNumber;
  }

  async handleTreeViewSelection({ dataItem, index }: any): Promise<void> {
    const ids = _.split(index, '_');

    this.selectedFolder = dataItem as FileItem;
    const firstBreadcrumb = this.rootFolder[+ids[0]];
    const newBreadcrumbs = ids.length > 1 ? [firstBreadcrumb, dataItem] : [dataItem];
    this.updateBreadcrumbs(newBreadcrumbs);
    this.loadGridData();
  }

  onGridItemsSelected(files: FileItem[]): void {
    this.selectedGridItems = files;
    this._fileManagerService.selectedGridItems.next(files);
    this.updateToolbarState();
  }

  fetchTreeViewChildren = (item: FileItem) =>
    this._fileManagerApiService.fetchFoldersInFolder(this.currentLocation, item);

  onRefresh(): void {
    this.loadGridData();
  }

  onCut(): void {
    // this.copyCutStore.selectedFiles = this._fileManagerService.getFilesNameWithRoot(this.selectedGridItems);
    // this.copyCutStore.directory = ''; // TODO
    // this.copyCutStore.isCopy = false;
    // this.updateToolbarState();
  }

  onCopy(): void {
    // this.copyCutStore.selectedFiles = this._fileManagerApiService.getFilesNameWithRoot(this.selectedGridItems);
    // this.copyCutStore.directory = ''; // TODO
    // this.copyCutStore.isCopy = true;
    // this.updateToolbarState();
  }

  onPaste(): void {
    this.copyCutStore.selectedFiles = [];
    this.copyCutStore.directory = '';
    this.selectedGridItems = [];
    this._fileManagerService.selectedGridItems.next([]);

    this.updateToolbarState();
  }

  onDeleteItems(): void {
    const dialogRef = this._dialogService.open({
      content: FileManagerDeleteConfirmationComponent,
    });

    const filesNameWithRoot = this._fileManagerService.getFilesNameWithRoot(this.selectedGridItems);
    const filesName = this._fileManagerService.getFilesName(this.selectedGridItems);

    const deleteConfirmation = dialogRef.content.instance as FileManagerDeleteConfirmationComponent;
    deleteConfirmation.names = filesName;

    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((r: { text: string; primary: boolean }) => {
      if (r.text === 'Yes') {
        this.deleteFilesConfirmed(filesNameWithRoot);
      }
    });
  }

  onDownloadFile(): void {
    forEach(this.selectedGridItems, (file) => this.downloadFile(file));
  }

  onDiff(): void {
    this._router.navigate(['file-manager', 'compare'], {
      queryParams: {
        filePath1: this.selectedGridItems[0].fullPath,
        filePath2: this.selectedGridItems[1].fullPath,
        tenant: this.tenantName,
        loc: this.currentLocation,
      },
    });
  }

  onCreateNewFolder(): void {
    const dialogRef = this._dialogService.open({
      content: FileManagerNewFolderDialogComponent,
      width: 400,
    });

    const uploadDlg = dialogRef.content.instance as FileManagerNewFolderDialogComponent;
    uploadDlg.existingFolderNames = [];
    uploadDlg.root = this.selectedFolder.fullPath;
    uploadDlg.location = this.currentLocation;
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((r: { text: string; primary: boolean }) => {
      setTimeout(() => this.loadGridData(), 10);
    });
  }

  onUploadFile(): void {
    const rootDir = this.selectedFolder.fullPath;
    const dialogRef = this._dialogService.open({
      content: FileManagerUploadDialogComponent,
    });

    const uploadDlg = dialogRef.content.instance as FileManagerUploadDialogComponent;
    uploadDlg.prefix = rootDir;
    uploadDlg.location = this.currentLocation;
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((r: { text: string; primary: boolean }) => {
      this.loadGridData();
    });
  }

  async onBreadcrumbClick(folder: FileItem): Promise<void> {
    const breadcrumbIndex = _.findIndex(this.breadcrumbs, (breadcrumb: FileItem) => breadcrumb === folder);
    this.selectedFolder = folder;
    const newBreadcrumbs = this.breadcrumbs.slice(0, breadcrumbIndex + 1);
    this.updateBreadcrumbs(newBreadcrumbs);
    this.loadGridData();
  }

  async onFolderOpening(folder: FileItem): Promise<void> {
    if (!folder.isDir) {
      return;
    }

    this.selectedGridItems = filter(this.selectedGridItems, (item: FileItem) => item.uid !== folder.uid);
    this._fileManagerService.selectedGridItems.next(this.selectedGridItems);
    this.updateToolbarState();

    this.selectedFolder = folder;
    const newBreadcrumbs = [...this.breadcrumbs, this.selectedFolder];
    this.updateBreadcrumbs(newBreadcrumbs);
    this.loadGridData();
  }

  onFileOpening(file: FileItem): void {
    if (file.isDir) {
      return;
    }

    this._router.navigate(['file-manager', 'file'], {
      queryParams: {
        filePath: file.fullPath,
        tenant: this.tenantName,
        loc: this.currentLocation,
      },
    });
  }

  onResetSelectedFiles(resetItems: FileItem[]): void {
    this.selectedGridItems = filter(this.selectedGridItems, (item: FileItem) => !includes(resetItems, item));
    this._fileManagerService.selectedGridItems.next(this.selectedGridItems);
    this.updateToolbarState();
  }

  handleCollapse(node: TreeItem): void {
    this.expandedKeys = this.expandedKeys.filter((k) => k !== node.index);
  }

  handleExpand(node: TreeItem): void {
    this.expandedKeys = this.expandedKeys.concat(node.index);
  }

  treeViewItemDbclick(): void {
    // if (this.isExpanded(this.selectedTreeViewItem, this.selectedTreeViewItem.index)) {
    //   this.handleCollapse(this.selectedFolder);
    // } else {
    //   this.handleExpand(this.selectedFolder);
    // }
  }

  isExpanded = (dataItem: any, index: string) => {
    return this.expandedKeys.indexOf(index) > -1;
  };

  private async loadGridData(): Promise<void> {
    this.state.isGridLoading = true;
    this.state.isLoadFailed = false;
    try {
      this.allGridData = await this._fileManagerApiService.fetchDataInFolder(this.currentLocation, this.selectedFolder);
      this.updateCurrentFilesDownloadStatus();
    } catch {
      this.state.isLoadFailed = true;
    } finally {
      this.state.isGridLoading = false;
    }
  }

  private updateBreadcrumbs(items: FileItem[]): void {
    this.breadcrumbs = items;
    this._breadcrumbService.updateFileManagerBreadcrumbs(items);
  }

  private updateFileDownloadStatus(file: FileItem, downloadStatus: FileDownloadStatus): void {
    _.forEach(this.allGridData, (item) => {
      if (item.uid === file.uid) {
        item.downloadStatus = downloadStatus;
        return false;
      }
    });
    const downloadingFiles = _.filter(
      this.allGridData,
      (item) => item.downloadStatus === FileDownloadStatus.Downloading
    );
    this._fileManagerService.updateDownloadingFilesStoreValue(downloadingFiles);
  }

  private async updateCurrentFilesDownloadStatus(): Promise<void> {
    const files = await this._fileManagerService.downloadingFilesStore.pipe(take(1)).toPromise();
    this.updateFilesDownloadStatus(files);
  }

  private updateFilesDownloadStatus(files: FileItem[]): void {
    _.forEach(this.allGridData, (item) => {
      const downloadingFiles = _.filter(files, (downloadingFile) => downloadingFile.uid === item.uid);
      if (!_.isEmpty(downloadingFiles)) {
        item.downloadStatus = FileDownloadStatus.Downloading;
      }
    });
  }

  private async downloadFile(file: FileItem): Promise<void> {
    this.updateFileDownloadStatus(file, FileDownloadStatus.Downloading);

    try {
      const blob = await this._fileManagerApiService.downloadFile(this.currentLocation, file.fullPath);
      fileSaver.saveAs(blob, file.name);

      this.updateFileDownloadStatus(file, FileDownloadStatus.None);
    } catch (ex) {
      console.log(ex);
      this.updateFileDownloadStatus(file, FileDownloadStatus.DownloadFailed);
    }
  }

  private updateToolbarState(): void {
    const hasSelectedItems = !isEmpty(this.selectedGridItems);
    const hasSelectedFolder = hasSelectedItems && some(this.selectedGridItems, (file) => file.isDir);

    let canDiff = hasSelectedItems && !hasSelectedFolder && this.selectedGridItems.length === 2;
    if (canDiff) {
      const fileType1 = this._fileManagerService.getFileExtension(this.selectedGridItems[0].path);
      const fileType2 = this._fileManagerService.getFileExtension(this.selectedGridItems[1].path);
      canDiff = fileType1 === FileExtension.Plain && fileType2 === FileExtension.Plain;
    }

    this.toolbarState = {
      canCopyOrCut: hasSelectedItems,
      canPaste: !isEmpty(this.copyCutStore.selectedFiles),
      canDelete: hasSelectedItems,
      canDownload: hasSelectedItems && !hasSelectedFolder,
      canDiff: canDiff,
    };
    this._fileManagerService.toolbarState.next(this.toolbarState);
  }

  private async deleteFilesConfirmed(filesNameWithRoot: string[]): Promise<void> {
    const fileDeletePromises = map(filesNameWithRoot, (fileNameWithRoot: string) =>
      this._fileManagerApiService.deleteItem(this.currentLocation, fileNameWithRoot)
    );

    await Promise.all(fileDeletePromises);
    this.selectedGridItems = [];
    this._fileManagerService.selectedGridItems.next([]);
    this.loadGridData();
  }
}
