
export const fileManagerConfig = {
  isUIDEnabled: true,
  createFolder: {
    validation: {
      name: {
        maxLength: 64,
        allowedCharacters: ['-', '_', '.']
      }
    }
  },
  gridSettings: {
    dateTimeFormat: 'dd-MMM-yyyy hh:mm'
  }
};
