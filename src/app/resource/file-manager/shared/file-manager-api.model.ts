export interface Metadata {
  name?: string;
  namespace?: string;
  creationTimestamp?: string;
  uid?: string;
  selfLink?: string;
  resourceVersion?: string;
}
export interface File {
  apiVersion?: string;
  kind?: string;
  metadata?: Metadata;
  spec?: {
    base?: {
      parent_link?: string;
      real_name?: string;
    };
    commonAttrs?: {
      isDir?: boolean;
      rootDir?: string;
      size?: number;
      type?: string;
    };
    firmwareAttrs?: { vendor?: string; version?: string };
    reportAttrs?: { args?: string };
    snapshotAttrs?: { args?: string };
    tftpAttrs?: { vendor?: string; version?: string };
  };
  status?: {
    base?: {};
    createdAt?: string;
    md5Hash?: string;
    status?: string;
  };
}

export interface FileList {
  apiVersion: string;
  items: File[];
  kind: 'FileList';
  metadata: {
    continue?: string;
    resourceVersion?: string;
    selfLink: string;
  };
}

export type FileType = 'all' | 'file' | 'directory';

export enum Type {
  TFTP = 'tftp',
  SNAPSHOT = 'snapshot',
  FIRMWARE = 'firmware',
  REPORT = 'report',
}
