import { FileExtension, FileItemRootType } from './file-type';

export enum FileDownloadStatus {
  None,
  Downloading,
  DownloadFailed
}

export class FileItem {
  readonly uid: string;
  readonly name: string;
  readonly path: string;
  readonly fullPath: string;
  readonly extension: FileExtension;
  readonly isDir: boolean;
  readonly dateModified: Date;
  readonly size: number;
  readonly isTopLevel: boolean;
  readonly location: string;
  readonly rootType: FileItemRootType;

  readonly attributes: FileItemAttributes;
  downloadStatus: FileDownloadStatus;

  constructor({
    uid,
    name,
    path,
    fullPath,
    extension,
    isDir,
    dateModified,
    size,
    isTopLevel,
    location,
    rootType,
    attributes,
    downloadStatus,
  }: Partial<FileItem> = {}) {
    this.uid = uid || '';
    this.name = name || '';
    this.path = path || '';
    this.fullPath = fullPath || '';
    this.extension = extension || FileExtension.Unknown;
    this.isDir = isDir || false;
    this.dateModified = dateModified || new Date();
    this.size = size || 0;
    this.isTopLevel = isTopLevel || false;
    this.location = location || '';
    this.rootType = rootType;
    this.attributes = attributes || new FileItemAttributes();
    this.downloadStatus = downloadStatus || FileDownloadStatus.None;
  }
}

export class FileItemAttributes {
  readonly vendor: string;
  readonly version: string;
  readonly args: string;

  constructor({ vendor, version, args }: Partial<FileItemAttributes> = {}) {
    this.vendor = vendor || null;
    this.version = version || null;
    this.args = args || null;
  }
}
