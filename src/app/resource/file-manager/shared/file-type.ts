export enum FileExtension {
  Plain,
  Image,
  Pdf,
  Video,
  Audio,
  Unknown
}

export enum FileItemRootType {
  tftp,
  firmware,
  report,
  snapshot
}
