// tslint:disable
/*
Copyright (c) 2020 WaveSurf Systems Inc. All rights reserved.
*/
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { FileManagerViewerComponent } from './file-manager-viewer/file-manager-viewer.component';
import { FileManagerPageComponent } from './file-manager-page/file-manager-page.component';
import { FileManagerCompareComponent } from './file-manager-compare/file-manager-compare.component';

const MAIN_ROUTE: Route = {
  path: '',
  component: FileManagerPageComponent,
  data: {
    breadcrumb: '',
    link: ['', 'file-manager'],
  },
};

const VIEW_ROUTE: Route = {
  path: 'file',
  component: FileManagerViewerComponent,
  data: {
    breadcrumb: ' ',
    parent: MAIN_ROUTE,
  },
};

const COMPARE_ROUTE: Route = {
  path: 'compare',
  component: FileManagerCompareComponent,
  data: {
    breadcrumb: ' ',
    parent: MAIN_ROUTE,
  },
};

@NgModule({
  imports: [RouterModule.forChild([VIEW_ROUTE, COMPARE_ROUTE, MAIN_ROUTE])],
  exports: [RouterModule],
})
export class FileManagerRoutingModule {}
