import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerToolbarSelectedRowsComponent } from './file-manager-toolbar-selected-rows.component';

describe('FileManagerToolbarSelectedRowsComponent', () => {
  let component: FileManagerToolbarSelectedRowsComponent;
  let fixture: ComponentFixture<FileManagerToolbarSelectedRowsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileManagerToolbarSelectedRowsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerToolbarSelectedRowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
