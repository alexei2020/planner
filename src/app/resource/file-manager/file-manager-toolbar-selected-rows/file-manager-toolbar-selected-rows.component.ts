import { Component, OnInit, forwardRef, Input, ViewChild, TemplateRef, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ToolBarToolComponent } from '@progress/kendo-angular-toolbar';
import { FileManagerViewSelectedComponent } from '../file-manager-view-selected/file-manager-view-selected.component';
import { DialogService } from '@progress/kendo-angular-dialog';
import { FileItem } from '../shared/file-item';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-file-manager-toolbar-selected-rows',
  templateUrl: './file-manager-toolbar-selected-rows.component.html',
  styleUrls: ['./file-manager-toolbar-selected-rows.component.scss'],
  providers: [
    { provide: ToolBarToolComponent, useExisting: forwardRef(() => FileManagerToolbarSelectedRowsComponent) },
  ],
})
export class FileManagerToolbarSelectedRowsComponent extends ToolBarToolComponent implements OnDestroy {
  @Input()
  selectedFiles: FileItem[];

  @Output()
  resetSelected: EventEmitter<FileItem[]>;

  @ViewChild('toolbarTemplate', { static: true })
  toolbarTemplate: TemplateRef<any>;

  private _unsubscribeAll = new Subject();

  constructor( private _dialogService: DialogService) {
    super();

    this.resetSelected = new EventEmitter();
  }

  onButtonClick(): void {
    const dialogRef = this._dialogService.open({
      content: FileManagerViewSelectedComponent,
      width: 400
    });

    const viewDlg = dialogRef.content.instance as FileManagerViewSelectedComponent;
    viewDlg.fileItems = this.selectedFiles;
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((r: { items: FileItem[] }) => {
      if (r.items && r.items.length > 0) {
        this.resetSelected.next(r.items);
      }
    });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll?.next();
    this._unsubscribeAll?.complete();
  }
}
