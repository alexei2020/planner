import { Component, Input } from '@angular/core';
import { DialogRef, DialogContentBase } from '@progress/kendo-angular-dialog';
import { fileManagerConfig } from '../file-manager.config';
import { FileManagerApiService } from '../file-manager-api.service';

@Component({
  selector: 'app-file-manager-new-folder-dialog',
  templateUrl: './file-manager-new-folder-dialog.component.html',
  styleUrls: ['./file-manager-new-folder-dialog.component.scss'],
})
export class FileManagerNewFolderDialogComponent extends DialogContentBase {
  @Input()
  existingFolderNames: string[];

  @Input()
  location: string;

  @Input()
  root: string;

  name: string;

  state: {
    isLoading: boolean;
  };

  get folderNameAllowedCharacters(): string[] {
    return fileManagerConfig.createFolder.validation.name.allowedCharacters;
  }

  get folderNameMaxLength(): number {
    return fileManagerConfig.createFolder.validation.name.maxLength;
  }

  constructor(public dialog: DialogRef, private fileManagerService: FileManagerApiService) {
    super(dialog);

    this.state = {
      isLoading: false,
    };
  }

  onCancelAction(): void {
    this.dialog.close({ text: 'Cancel' });
  }

  async onCreateFolder(): Promise<void> {
    await this.fileManagerService.createNewFolder(this.location, `${this.root}/${this.name.toLowerCase()}`);

    this.dialog.close({ text: 'Submit', primary: true });
  }
}
