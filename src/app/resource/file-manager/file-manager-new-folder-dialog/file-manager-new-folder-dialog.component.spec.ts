import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerNewFolderDialogComponent } from './file-manager-new-folder-dialog.component';

describe('FileManagerNewFolderDialogComponent', () => {
  let component: FileManagerNewFolderDialogComponent;
  let fixture: ComponentFixture<FileManagerNewFolderDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileManagerNewFolderDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerNewFolderDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
