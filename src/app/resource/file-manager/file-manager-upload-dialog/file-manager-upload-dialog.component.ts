import { Component, Input } from '@angular/core';
import { DialogRef, DialogContentBase } from '@progress/kendo-angular-dialog';
import { FileRestrictions } from '@progress/kendo-angular-upload';
import { UploadEvent } from '@progress/kendo-angular-upload';

@Component({
  selector: 'app-file-manager-upload-dialog',
  templateUrl: './file-manager-upload-dialog.component.html',
  styleUrls: ['./file-manager-upload-dialog.component.scss'],
})
export class FileManagerUploadDialogComponent extends DialogContentBase {
  @Input() 
  prefix: string;

  @Input() 
  location: string;

  fileRestrictions: FileRestrictions = {
    allowedExtensions: ['.txt', '.html', '.pdf', '.xls|.xlsx', '.json', '.yaml|.yml', '.doc|.docx', '.xml'],
  };

  get uploadSaveUrl(): string {
    return `files/${this.location}/v1/namespaces/network/files`;
  }

  constructor(public dialog: DialogRef) {
    super(dialog);
  }

  public onCancelAction(): void {
    this.dialog.close({ text: 'Cancel' });
  }

  public onConfirmAction(): void {
    this.dialog.close({ text: 'Submit', primary: true });
  }

  uploadEventHandler(e: UploadEvent): void {
    const regex = /\//gi;
    const metaName: string = this.prefix === '' ? e.files[0].name : this.prefix + '/' + e.files[0].name;

    //metaName now should be like: tftp/sample10/img1.png or snapshot/sample11/img2,png, tftp/img2.png
    const name = metaName.replace(regex, '-');
    let contentType = e.files[0].rawFile.type;

    e.data = {
      filepath: metaName.toLowerCase(),
      isdir: 'false',
    };
  }
}
