import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerUploadDialogComponent } from './file-manager-upload-dialog.component';

describe('FileManagerUploadDialogComponent', () => {
  let component: FileManagerUploadDialogComponent;
  let fixture: ComponentFixture<FileManagerUploadDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileManagerUploadDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerUploadDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
ß
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
