import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { File, FileList } from './shared/file-manager-api.model';
import * as _ from 'lodash';
import { map } from 'rxjs/operators';
import moment from 'moment';
import { filter, includes, split } from 'lodash';
import { FileExtension, FileItemRootType } from './shared/file-type';
import { FileItem, FileItemAttributes } from './shared/file-item';
import { fileManagerConfig } from './file-manager.config';
export interface ToolbarState {
  canCopyOrCut: boolean;
  canPaste: boolean;
  canDelete: boolean;
  canDownload: boolean;
  canDiff: boolean;
}
export interface CopyCutStore {
  selectedFiles: string[];
  directory: string;
  isCopy: boolean;
}
export type ToolbarActionType =
  | "CreateNewFolder"
  | "UploadFile"
  | "Refresh"
  | "Cut"
  | "Copy"
  | "Paste"
  | "Delete"
  | "DownloadFile"
  | "Diff"
  | null;
export interface ToolbarAction {
  type: ToolbarActionType;
  payload?: any;
}

@Injectable({
  providedIn: 'root',
})
export class FileManagerService {
  constructor() {}
  
  toolbarState: BehaviorSubject<ToolbarState> = new BehaviorSubject<
    ToolbarState
  >({
    canCopyOrCut: false,
    canPaste: false,
    canDelete: false,
    canDownload: false,
    canDiff: false,
  });
  toolbarAction: BehaviorSubject<ToolbarAction> = new BehaviorSubject<
    ToolbarAction
  >({ type: null, payload: null });

  selectedGridItems: BehaviorSubject<FileItem[]> = new BehaviorSubject<
    FileItem[]
  >([]);
  copyCutStore: BehaviorSubject<CopyCutStore> = new BehaviorSubject<
    CopyCutStore
  >({
    selectedFiles: [],
    directory: null,
    isCopy: false,
  });
  
  private _downloadingFilesStore$: BehaviorSubject<FileItem[]> = new BehaviorSubject<FileItem[]>([]);
  
  get downloadingFilesStore(): Observable<FileItem[]> {
    return this._downloadingFilesStore$.asObservable();
  }

  getFileNameWithRoot(file: File): string {
    const filePath = `${file.spec.base.real_name}`;

    return filePath;
  }

  getFilesNameWithRoot(files: FileItem[]): string[] {
    const names = _.map(files, (file: FileItem) => file.path);

    return names;
  }

  getFilesName(files: FileItem[]): string[] {
    const names = _.map(files, (file: FileItem) => file.name);

    return names;
  }

  getFileExtension(fileName: string): FileExtension {
    const fileChunks = _.split(fileName, '/');
    const shortFileName = fileChunks.length === 1 ? fileChunks[0] : fileChunks.slice(-1).pop();

    const shortFileChunks = _.split(shortFileName, '.');
    const extension =
      shortFileChunks.length === 1 ? shortFileChunks[0].toLowerCase() : shortFileChunks.slice(-1).pop().toLowerCase();

    if (includes(['png', 'jpg', 'jpeg', 'jpe', 'jif', 'jfif', 'jfi', 'gif', 'tiff', 'tif', 'bmp'], extension)) {
      return FileExtension.Image;
    }
    if (extension === 'pdf') {
      return FileExtension.Pdf;
    }
    if (includes(['txt', 'json', 'yaml', 'yml', 'log', 'html', 'htm'], extension)) {
      return FileExtension.Plain;
    }

    return FileExtension.Plain;
  }

  mapFile(file: File, isTopLevel: boolean = false): FileItem {
    const fileChunks = _.split(file.spec.base.real_name, '/');
    const fileName = fileChunks.length === 1 ? fileChunks[0] : fileChunks.slice(-1).pop();
    const filePath = this.getFileNameWithRoot(file);
    const fileExtension = this.getFileExtension(fileName);
    const location = this.getLocation(file.apiVersion);
    const rootType = this.getFileRootType(file.spec.commonAttrs.type);
    const attributes = this.getFileAttributes(file, rootType);

    const newFile = new FileItem({
      uid: file.metadata.uid,
      name: fileName,
      path: file.spec.base.real_name,
      fullPath: filePath,
      extension: fileExtension,
      dateModified: moment(file.status.createdAt).toDate(),
      size: +file.spec.commonAttrs.size,
      isDir: file.spec.commonAttrs.isDir,
      isTopLevel: isTopLevel,
      location: location,
      rootType: rootType,
      attributes: attributes,
    });

    return newFile;
  }
  
  updateDownloadingFilesStoreValue(value: FileItem[]): void {
    this._downloadingFilesStore$.next(value);
  }

  private getLocation(apiVersion: string): string {
    if (!apiVersion) {
      return '';
    }

    const parts = split(apiVersion, '/');

    return parts[0];
  }

  private getFileRootType(fileType: string): FileItemRootType {
    switch (fileType) {
      case 'tftp':
        return FileItemRootType.tftp;
      case 'firmware':
        return FileItemRootType.firmware;
      case 'report':
        return FileItemRootType.report;
      case 'snapshot':
        return FileItemRootType.snapshot;
      default:
        return null;
    }
  }

  private getFileAttributes(file: File, rootType: FileItemRootType): FileItemAttributes {
    switch (rootType) {
      case FileItemRootType.tftp:
        return new FileItemAttributes({
          vendor: file.spec.tftpAttrs.vendor,
          version: file.spec.tftpAttrs.version,
        });
      case FileItemRootType.firmware:
        return new FileItemAttributes({
          vendor: file.spec.firmwareAttrs.vendor,
          version: file.spec.firmwareAttrs.version,
        });
      case FileItemRootType.report:
        return new FileItemAttributes({
          args: file.spec.reportAttrs.args,
        });
      case FileItemRootType.snapshot:
        return new FileItemAttributes({
          args: file.spec.snapshotAttrs.args,
        });
      default:
        return new FileItemAttributes();
    }
  }
}
