import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerDeleteConfirmationComponent } from './file-manager-delete-confirmation.component';

describe('FileManagerDeleteConfirmationComponent', () => {
  let component: FileManagerDeleteConfirmationComponent;
  let fixture: ComponentFixture<FileManagerDeleteConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileManagerDeleteConfirmationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerDeleteConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
