import { Component, Input } from '@angular/core';
import { DialogRef, DialogContentBase } from '@progress/kendo-angular-dialog';

@Component({
  selector: 'app-file-manager-delete-confirmation',
  templateUrl: './file-manager-delete-confirmation.component.html',
  styleUrls: ['./file-manager-delete-confirmation.component.scss'],
})
export class FileManagerDeleteConfirmationComponent extends DialogContentBase {
  @Input()
  names: string[];

  constructor(public dialog: DialogRef) {
    super(dialog);
  }

  public onCancelAction(): void {
    this.dialog.close({ text: 'No' });
  }

  public onConfirmAction(): void {
    this.dialog.close({ text: 'Yes', primary: true });
  }
}
