import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerViewSelectedComponent } from './file-manager-view-selected.component';

describe('FileManagerViewSelectedComponent', () => {
  let component: FileManagerViewSelectedComponent;
  let fixture: ComponentFixture<FileManagerViewSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileManagerViewSelectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerViewSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
