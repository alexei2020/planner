import { Component, OnInit, Input } from '@angular/core';
import { DialogContentBase, DialogRef } from '@progress/kendo-angular-dialog';
import { FileItem } from '../shared/file-item';
import { filter } from 'lodash';

@Component({
  selector: 'app-file-manager-view-selected',
  templateUrl: './file-manager-view-selected.component.html',
  styleUrls: ['./file-manager-view-selected.component.scss'],
})
export class FileManagerViewSelectedComponent extends DialogContentBase {
  @Input()
  fileItems: FileItem[];

  selectedItems: FileItem[] = [];

  constructor(public dialog: DialogRef) {
    super(dialog);
  }

  getFilePathBradcrumb(filePath: string): string {
    return filePath.replace('/', ' > ');
  }

  onItemSelected(event: any, item: FileItem): void{
    if (event.currentTarget.checked) {
      this.selectedItems = [...this.selectedItems, item];
    } else {
      this.selectedItems = filter(this.selectedItems, (selectedItem: FileItem) => selectedItem !== item);
    }
  }

  onCancelAction(): void {
    this.dialog.close({ text: 'Cancel' });
  }

  onDeselect(): void {
    this.dialog.close({ items: this.selectedItems });
  }

  onDeselectAll(): void {
    this.dialog.close({ items: this.fileItems });
  }
}
