import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import {
  SelectableSettings,
  GridDataResult,
  SelectionEvent,
  RowArgs,
  PagerSettings,
  SortSettings,
  PageChangeEvent,
} from '@progress/kendo-angular-grid';
import { map, filter, some, split, isEmpty, orderBy, includes } from 'lodash';
import { FileDownloadStatus, FileItem } from '../shared/file-item';
import { SortDescriptor } from '@progress/kendo-data-query';
import { fileManagerConfig } from '../file-manager.config';
import { FileItemRootType } from '../shared/file-type';

@Component({
  selector: 'app-file-manager-grid',
  templateUrl: './file-manager-grid.component.html',
  styleUrls: ['./file-manager-grid.component.scss'],
})
export class FileManagerGridComponent implements OnInit, OnChanges {
  @Input()
  title: string;

  @Input()
  items: FileItem[];

  @Input()
  isLoading: boolean;

  @Input()
  selectedFiles: FileItem[];

  @Output()
  fileOpening: EventEmitter<FileItem>;

  @Output()
  folderOpening: EventEmitter<FileItem>;

  @Output()
  itemsSelected: EventEmitter<FileItem[]>;

  selectableSettings: SelectableSettings = {
    checkboxOnly: true,
    mode: 'multiple',
  };
  pageableSettings: PagerSettings = {
    type: 'numeric',
    pageSizes: false,
  };
  sortableSettings: SortSettings = {
    allowUnsort: true,
    mode: 'single',
  };
  sort: SortDescriptor[] = [];

  gridView: GridDataResult;
  mySelection: string[] = [];
  skip = 0;
  pageSize = 5;
  filter = '';
  gridViewSettings: {
    showArgsColumn: boolean;
    showVendorColumn: boolean;
  };

  get FileDownloadStatus(): typeof FileDownloadStatus {
    return FileDownloadStatus;
  }

  get dataTimeFormat(): string {
    return fileManagerConfig.gridSettings.dateTimeFormat;
  }

  constructor() {
    this.gridViewSettings = {
      showArgsColumn: false,
      showVendorColumn: false,
    };

    this.fileOpening = new EventEmitter<FileItem>();
    this.folderOpening = new EventEmitter<FileItem>();
    this.itemsSelected = new EventEmitter<FileItem[]>();
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    const itemsChanges = changes['items'];
    if (itemsChanges && itemsChanges.currentValue !== itemsChanges.previousValue) {
      this.refreshGridView();
    }

    const selectedFilesChanges = changes['selectedFiles'];
    if (selectedFilesChanges && selectedFilesChanges.currentValue !== selectedFilesChanges.previousValue) {
      this.mySelection = map(this.selectedFiles, (file: FileItem) => file.name);
      this.refreshGridView();
    }
  }

  getIconClass(file: FileItem): string {
    if (file.isDir) {
      return 'k-i-folder';
    }

    const fileChunks = split(file.path, '.');
    const fileExtention = fileChunks.length === 1 ? '' : fileChunks.slice(-1).pop();
    switch (fileExtention) {
      case 'pdf':
        return 'k-i-file-pdf';
      case 'json':
        return 'k-i-paste-plain-text';
      case 'xls':
      case 'xlsx':
        return 'k-i-file-excel';

      default:
        return 'k-i-file';
    }
  }

  onPageSizeChanged(pageSize: number): void {
    this.skip = 0;
    this.pageSize = pageSize;
    this.refreshGridView();
  }

  onPageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.pageSize = event.take;
    this.refreshGridView();
  }

  onRowSelected(event: SelectionEvent): void {
    const isSingleSelect = !event.shiftKey && !event.ctrlKey;
    const newSelectedFiles = map(event.selectedRows, (selectdRow: RowArgs) => selectdRow.dataItem as FileItem);
    if (isSingleSelect) {
      this.itemsSelected.emit(newSelectedFiles);
      return;
    }

    const newDeselectedFiles = map(event.deselectedRows, (selectdRow: RowArgs) => selectdRow.dataItem as FileItem);
    const selectedFilesWithoutExcluded = filter(
      this.selectedFiles,
      (file: FileItem) => !some(newDeselectedFiles, (deselectedFile: FileItem) => deselectedFile.uid === file.uid)
    );

    const updatedSelectedFiles = [...selectedFilesWithoutExcluded, ...newSelectedFiles];

    this.itemsSelected.emit(updatedSelectedFiles);
  }

  onSortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.refreshGridView();
  }

  onItemDbclick(event): void {
    let rowIndex: number;
    if (event.path) {
      // works on chrome and all browsers supporting path property in mouseevent
      rowIndex = event.path[2].rowIndex;
    } else {
      // should work on all browsers
      const parent = event.target.parentElement;
      rowIndex = (parent.rowIndex !== undefined) ? parent.rowIndex : parent.parentElement.rowIndex;
    }

    if (rowIndex === undefined) {
      return;
    }
    const selectedFile = this.gridView.data[rowIndex] as FileItem;
    if (selectedFile.isDir) {
      this.folderOpening.next(selectedFile);
    } else {
      this.fileOpening.next(selectedFile);
    }
  }

  onFilterChanged(filter: string): void {
    this.filter = filter;
    this.refreshGridView();
  }

  private refreshGridView(): void {
    if (isEmpty(this.items)) {
      this.gridView = {
        data: [],
        total: 0,
      };
      this.gridViewSettings = {
        showArgsColumn: false,
        showVendorColumn: false,
      };
    } else {
      const showArgsColumn = some(
        this.items,
        (item: FileItem) => item.rootType === FileItemRootType.report || item.rootType === FileItemRootType.snapshot
      );
      const showVendorColumn = some(
        this.items,
        (item: FileItem) => item.rootType === FileItemRootType.tftp || item.rootType === FileItemRootType.firmware
      );

      const orderByFields = map(this.sort, (s) => s.field);
      const directions = map(this.sort, (s) => s.dir);

      const filteredItems = filter(this.items, (item: FileItem) => includes(item.name, this.filter));
      const sortedItems = orderBy(filteredItems, orderByFields, directions);

      this.gridView = {
        data: sortedItems.slice(this.skip, this.skip + this.pageSize),
        total: sortedItems.length,
      };
      this.gridViewSettings = {
        showArgsColumn: showArgsColumn,
        showVendorColumn: showVendorColumn,
      };
    }
  }
}
