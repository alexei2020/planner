import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerGridComponent } from './file-manager-grid.component';

describe('FileManagerGridComponent', () => {
  let component: FileManagerGridComponent;
  let fixture: ComponentFixture<FileManagerGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileManagerGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
