// tslint:disable
/*
    Copyright (c) 2020 WaveSurf Systems Inc. All rights reserved.
    */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ToolBarModule } from '@progress/kendo-angular-toolbar';
import { TreeListModule } from '@progress/kendo-angular-treelist';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { UploadsModule } from '@progress/kendo-angular-upload';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LabelModule } from '@progress/kendo-angular-label';

import { FileManagerViewerComponent } from './file-manager-viewer/file-manager-viewer.component';
import { FileManagerRoutingModule } from './file-manager.routing';
import 'hammerjs';
import { FileManagerPageComponent } from './file-manager-page/file-manager-page.component';
import { FileManagerDeleteConfirmationComponent } from './file-manager-delete-confirmation/file-manager-delete-confirmation.component';
import { FileManagerNewFolderDialogComponent } from './file-manager-new-folder-dialog/file-manager-new-folder-dialog.component';
import { FileManagerUploadDialogComponent } from './file-manager-upload-dialog/file-manager-upload-dialog.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { GridModule } from '@progress/kendo-angular-grid';
import { FileManagerGridComponent } from './file-manager-grid/file-manager-grid.component';
import { FileManagerCompareComponent } from './file-manager-compare/file-manager-compare.component';
import { FileManagerViewSelectedComponent } from './file-manager-view-selected/file-manager-view-selected.component';
import { WidgetModule } from 'app/main/widget/widget.module';
import { GridsterModule } from 'angular-gridster2';
import { FileManagerComponent } from './file-manager/file-manager.component';
import { ComponentsModule } from 'app/components/module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from 'app/main/shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { ContentDiffModule } from 'app/main/shared/modules/content-diff/content-diff.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

const KENDO_COMPONENTS = [
  ToolBarModule,
  TreeListModule,
  TreeViewModule,
  LayoutModule,
  UploadsModule,
  DialogModule,
  ButtonsModule,
  InputsModule,
  GridModule,
  LabelModule,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    KENDO_COMPONENTS,
    FileManagerRoutingModule,
    FuseSharedModule,
    WidgetModule,
    GridsterModule,
    ComponentsModule,
    FlexLayoutModule,
    // ButtonsModule,
    // SharedModule
    ContentDiffModule,
    MatProgressSpinnerModule,
  ],
  declarations: [
    FileManagerViewerComponent,
    FileManagerPageComponent,
    FileManagerDeleteConfirmationComponent,
    FileManagerNewFolderDialogComponent,
    FileManagerUploadDialogComponent,
    FileManagerGridComponent,
    FileManagerCompareComponent,
    FileManagerViewSelectedComponent,
    FileManagerComponent,
  ],
  entryComponents: [
    FileManagerDeleteConfirmationComponent,
    FileManagerNewFolderDialogComponent,
    FileManagerUploadDialogComponent,
    FileManagerViewSelectedComponent,
  ],
  exports: [FileManagerPageComponent, FileManagerViewerComponent],
})
export class FileManagerModule {}
