import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FileManagerApiService } from '../file-manager-api.service';
import { FileExtension } from '../shared/file-type';
import { FileManagerService } from '../file-manager.service';
import { BreadcrumbService } from 'app/main/shared/breadcrumb-bar.service';
import { PageType } from 'app/main/shared/breadcrumb-bar.service.gen';
import { IBreadCrumb } from '@fuse/components/breadcrumbs/IBreadCrumb';
import {FuseLocationService} from '../../../services/location.service';

@Component({
  selector: 'app-file-manager-viewer',
  templateUrl: './file-manager-viewer.component.html',
  styleUrls: ['./file-manager-viewer.component.scss'],
})
export class FileManagerViewerComponent implements OnInit, OnDestroy {
  data: string | object;
  fileUrl: string;
  fileType: FileExtension;
  
  state: {
    isLoading: boolean;
    isFailed: boolean;
  };

  get FileExtension(): typeof FileExtension {
    return FileExtension;
  }

  constructor(
    private route: ActivatedRoute,
    private fileManagerApiService: FileManagerApiService,
    private fileManagerService: FileManagerService,
    private breadcrumbService: BreadcrumbService,
    private _locationService: FuseLocationService,
  ) {
    this.data = null;

    this.state = {
      isLoading: false,
      isFailed: false,
    };
  }

  async ngOnInit(): Promise<void> {
    this.breadcrumbService.currentPage.next(PageType.FILE_MANAGER_PREVIEW);
    
    try {
      this.state.isLoading = true;
      const filePath = this.route.snapshot.queryParams['filePath'];
      const apigroup = this._locationService.locationDataAsKeyValue().currentLocation;

      this.breadcrumbService.addBreadcrumb({
        label: filePath
      } as IBreadCrumb);

      const fileBaseUrl = this.fileManagerApiService.getFileBaseUrl(apigroup);
      this.fileUrl = `${fileBaseUrl}/${filePath}`;
      this.fileType = this.fileManagerService.getFileExtension(filePath);
      if (this.fileType === FileExtension.Image) {
        return;
      }

      const blob = await this.fileManagerApiService.downloadFile(apigroup, filePath);
      const text = await (blob as any).text();
      this.data = text;
    } catch (ex) {
      this.state.isFailed = true;
    } finally {
      this.state.isLoading = false;
    }
  }
  ngOnDestroy() {
    this.breadcrumbService.currentPage.next(PageType.DEFAULT);
  }
}
