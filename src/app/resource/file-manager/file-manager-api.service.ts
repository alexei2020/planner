import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { File, FileList } from './shared/file-manager-api.model';
import * as _ from 'lodash';
import { map } from 'rxjs/operators';
import { filter } from 'lodash';
import { FileItem } from './shared/file-item';
import { fileManagerConfig } from './file-manager.config';
import { FileManagerService } from './file-manager.service';

@Injectable({
  providedIn: 'root',
})
export class FileManagerApiService {
  constructor(private http: HttpClient, private fileManagerService: FileManagerService) {}

  private getRootApiUrl(location: string, rootName: string): string {
    const apiUrl = `apis/${location}/v1/namespaces/network/files?wsSelector=metadata.name=${rootName}`;

    return apiUrl;
  }

  private getApiUrl(location: string, folder: FileItem): string {
    let apiUrl: string;
    if (fileManagerConfig.isUIDEnabled && folder.uid) {
      apiUrl = `apis/${location}/v1/namespaces/network/files?wsSelector=spec.base.parent_link=${folder.uid}`;
    } else {
      apiUrl = `apis/${location}/v1/namespaces/network/files?wsSelector=spec.base.parent_link=/apis/${location}/v1/namespaces/network/files/${folder.path}`;
    }

    return apiUrl;
  }

  getFileBaseUrl(location: string): string {
    const apiBaseUrl = `files/${location}/v1/namespaces/network/files`;

    return apiBaseUrl;
  }

  async fetchTreeViewDataInRoot(location: string): Promise<FileItem[]> {
    if (fileManagerConfig.isUIDEnabled) {
      const rootNames = ['tftp', 'snapshot', 'firmware', 'report'];

      const rootTreePromises = _.chain(rootNames)
        .map((root: string) => this.getRootApiUrl(location, root))
        .map((apiUrl: string) => this.http.get<FileList>(apiUrl).toPromise())
        .value();

      const rootTreeResponses = await Promise.all(rootTreePromises);

      const rootTreeResults = _.chain(rootTreeResponses)
        .map((response: FileList) => filter(response.items, (file: File) => file.spec.commonAttrs.isDir))
        .map((items) => _.map(items, (file: File) => this.fileManagerService.mapFile(file, true)))
        .flatMap()
        .value();

      return rootTreeResults;
    } else {
      const rootTree = [
        new FileItem({
          name: 'tftp',
          path: 'tftp',
          fullPath: 'tftp',
          isDir: true,
          isTopLevel: true,
        }),
        new FileItem({
          name: 'snapshot',
          path: 'snapshot',
          fullPath: 'snapshot',
          isDir: true,
          isTopLevel: true,
        }),
        new FileItem({
          name: 'firmware',
          path: 'firmware',
          fullPath: 'firmware',
          isDir: true,
          isTopLevel: true,
        }),
        new FileItem({
          name: 'report',
          path: 'report',
          fullPath: 'report',
          isDir: true,
          isTopLevel: true,
        }),
      ];

      return rootTree;
    }
  }

  async fetchDataInFolder(location: string, folder: FileItem): Promise<FileItem[]> {
    const apiUrl = this.getApiUrl(location, folder);

    let files: FileItem[] = [];
    try {
      const fileList = await this.http.get<FileList>(apiUrl).toPromise();
      files = _.map(fileList.items, (file) => this.fileManagerService.mapFile(file));
    } catch (ex) {
      console.log(ex);
      throw ex;
    }

    return files;
  }

  fetchFoldersInFolder(location: string, folder: FileItem): Observable<FileItem[]> {
    const apiUrl = this.getApiUrl(location, folder);

    return this.http.get<FileList>(apiUrl).pipe(
      map((response: FileList) => filter(response.items, (file: File) => file.spec.commonAttrs.isDir)),
      map((items) => _.map(items, (file: File) => this.fileManagerService.mapFile(file)))
    );
  }

  async createNewFolder(location: string, folderName: string): Promise<void> {
    const fileBaseUrl = this.getFileBaseUrl(location);
    const input = new FormData();
    input.append('filepath', folderName);
    input.append('isdir', 'true');

    try {
      await this.http.post(fileBaseUrl, input).toPromise();
    } catch (ex) {
      console.log(ex);
    }
  }

  async downloadFile(location: string, filePath: string): Promise<Blob> {
    const fileBaseUrl = this.getFileBaseUrl(location);
    const url = `${fileBaseUrl}/${filePath}`;

    let response: Blob;
    try {
      response = await this.http.get(url, { responseType: 'blob' }).toPromise();
    } catch (ex) {
      console.log(ex);
    }

    return response;
  }

  async deleteItem(location: string, filePathWithRoot: string): Promise<void> {
    const fileBaseUrl = this.getFileBaseUrl(location);
    const url = `${fileBaseUrl}/${filePathWithRoot}`;

    try {
      await this.http.delete(url).toPromise();
    } catch (ex) {
      console.log(ex);
    }
  }
}
