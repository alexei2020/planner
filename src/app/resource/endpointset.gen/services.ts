

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { EndPointSet } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onEndPointSetDetailLoadCallback = (instance?: EndPointSet) => void;
type onEndPointSetDetailLoadFailCallback = (instance?: EndPointSet) => void;
@Injectable({
    providedIn: 'root'
})
export class EndPointSetDetailService {
    public onEndPointSetUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: EndPointSet = {} as EndPointSet;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('EndPointSet');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/endpointsets?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getEndPointSets(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/endpointsets?${filterString}`, body, httpOptions);
     }

    getEndPointSetByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/endpointsets/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/endpointsets/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<EndPointSet>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/endpointsets/${resourceName}`);
	 } else {
         	return this.http_.get<EndPointSet>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/endpointsets/${resourceName}`);
	 }
     }

//     load(onLoad?: onEndPointSetDetailLoadCallback, onFail?: onEndPointSetDetailLoadFailCallback): void {
//         this.http_.get<EndPointSet>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onEndPointSetUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onEndPointSetUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(endpointset: EndPointSet, _location: string): Observable<EndPointSet> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<EndPointSet>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/endpointsets/`+endpointset.metadata.name, endpointset, httpOptions);
      }



    add(endpointset: EndPointSet, _location: string): Observable<EndPointSet> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<EndPointSet>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/endpointsets/`, endpointset, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/endpointsets/${id}`);
    }

    getInstance(): EndPointSet {
        return this.instance_;
    }
}
