

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { Audit } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onAuditDetailLoadCallback = (instance?: Audit) => void;
type onAuditDetailLoadFailCallback = (instance?: Audit) => void;
@Injectable({
    providedIn: 'root'
})
export class AuditDetailService {
    public onAuditUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: Audit = {} as Audit;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('Audit');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/audits?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getAudits(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/audits?${filterString}`, body, httpOptions);
     }

    getAuditByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/audits/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/audits/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<Audit>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/audits/${resourceName}`);
	 } else {
         	return this.http_.get<Audit>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/audits/${resourceName}`);
	 }
     }

//     load(onLoad?: onAuditDetailLoadCallback, onFail?: onAuditDetailLoadFailCallback): void {
//         this.http_.get<Audit>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onAuditUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onAuditUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(audit: Audit, _location: string): Observable<Audit> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<Audit>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/audits/`+audit.metadata.name, audit, httpOptions);
      }



    add(audit: Audit, _location: string): Observable<Audit> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<Audit>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/audits/`, audit, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/audits/${id}`);
    }

    getInstance(): Audit {
        return this.instance_;
    }
}
