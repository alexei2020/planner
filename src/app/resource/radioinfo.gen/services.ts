

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { RadioInfo } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onRadioInfoDetailLoadCallback = (instance?: RadioInfo) => void;
type onRadioInfoDetailLoadFailCallback = (instance?: RadioInfo) => void;
@Injectable({
    providedIn: 'root'
})
export class RadioInfoDetailService {
    public onRadioInfoUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: RadioInfo = {} as RadioInfo;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('RadioInfo');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/radioinfos?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getRadioInfos(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/radioinfos?${filterString}`, body, httpOptions);
     }

    getRadioInfoByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/radioinfos/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/radioinfos/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<RadioInfo>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/radioinfos/${resourceName}`);
	 } else {
         	return this.http_.get<RadioInfo>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/radioinfos/${resourceName}`);
	 }
     }

//     load(onLoad?: onRadioInfoDetailLoadCallback, onFail?: onRadioInfoDetailLoadFailCallback): void {
//         this.http_.get<RadioInfo>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onRadioInfoUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onRadioInfoUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(radioinfo: RadioInfo, _location: string): Observable<RadioInfo> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<RadioInfo>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/radioinfos/`+radioinfo.metadata.name, radioinfo, httpOptions);
      }



    add(radioinfo: RadioInfo, _location: string): Observable<RadioInfo> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<RadioInfo>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/radioinfos/`, radioinfo, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/radioinfos/${id}`);
    }

    getInstance(): RadioInfo {
        return this.instance_;
    }
}
