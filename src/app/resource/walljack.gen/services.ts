

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { WallJack } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onWallJackDetailLoadCallback = (instance?: WallJack) => void;
type onWallJackDetailLoadFailCallback = (instance?: WallJack) => void;
@Injectable({
    providedIn: 'root'
})
export class WallJackDetailService {
    public onWallJackUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: WallJack = {} as WallJack;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('WallJack');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/walljacks?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getWallJacks(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/walljacks?${filterString}`, body, httpOptions);
     }

    getWallJackByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/walljacks/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/walljacks/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<WallJack>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/walljacks/${resourceName}`);
	 } else {
         	return this.http_.get<WallJack>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/walljacks/${resourceName}`);
	 }
     }

//     load(onLoad?: onWallJackDetailLoadCallback, onFail?: onWallJackDetailLoadFailCallback): void {
//         this.http_.get<WallJack>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onWallJackUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onWallJackUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(walljack: WallJack, _location: string): Observable<WallJack> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<WallJack>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/walljacks/`+walljack.metadata.name, walljack, httpOptions);
      }



    add(walljack: WallJack, _location: string): Observable<WallJack> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<WallJack>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/walljacks/`, walljack, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/walljacks/${id}`);
    }

    getInstance(): WallJack {
        return this.instance_;
    }
}
