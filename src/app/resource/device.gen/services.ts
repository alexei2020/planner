

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { Device } from '../../typings/backendapi.gen';
import { IfInterface } from '../../typings/backendapi.gen';
import { PlatformComponent } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onDeviceDetailLoadCallback = (instance?: Device) => void;
type onDeviceDetailLoadFailCallback = (instance?: Device) => void;
type onIfInterfaceDetailLoadCallback = (instance?: IfInterface) => void;
type onIfInterfaceDetailLoadFailCallback = (instance?: IfInterface) => void;
type onPlatformComponentDetailLoadCallback = (instance?: PlatformComponent) => void;
type onPlatformComponentDetailLoadFailCallback = (instance?: PlatformComponent) => void;
@Injectable({
    providedIn: 'root'
})
export class DeviceDetailService {
    public onDeviceUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: Device = {} as Device;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('Device');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/devices?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getDevices(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/devices?${filterString}`, body, httpOptions);
     }

    getDeviceByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/devices/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/devices/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<Device>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/devices/${resourceName}`);
	 } else {
         	return this.http_.get<Device>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/devices/${resourceName}`);
	 }
     }

//     load(onLoad?: onDeviceDetailLoadCallback, onFail?: onDeviceDetailLoadFailCallback): void {
//         this.http_.get<Device>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onDeviceUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onDeviceUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(device: Device, _location: string): Observable<Device> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<Device>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/devices/`+device.metadata.name, device, httpOptions);
      }



    add(device: Device, _location: string): Observable<Device> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<Device>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/devices/`, device, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/devices/${id}`);
    }

    getInstance(): Device {
        return this.instance_;
    }
}
export class IfInterfaceDetailService {
    public onIfInterfaceUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: IfInterface = {} as IfInterface;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('IfInterface');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/ifinterface?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getIfInterfaces(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/ifinterface?${filterString}`, body, httpOptions);
     }

    getIfInterfaceByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/ifinterface/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/ifinterface/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<IfInterface>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/devices/${resourceName}`);
	 } else {
         	return this.http_.get<IfInterface>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/devices/${resourceName}`);
	 }
     }

//     load(onLoad?: onIfInterfaceDetailLoadCallback, onFail?: onIfInterfaceDetailLoadFailCallback): void {
//         this.http_.get<IfInterface>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onIfInterfaceUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onIfInterfaceUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(device: IfInterface, _location: string): Observable<IfInterface> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<IfInterface>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/devices/`+device.metadata.name, device, httpOptions);
      }



    add(device: IfInterface, _location: string): Observable<IfInterface> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<IfInterface>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/devices/`, device, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/devices/${id}`);
    }

    getInstance(): IfInterface {
        return this.instance_;
    }
}
export class PlatformComponentDetailService {
    public onPlatformComponentUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: PlatformComponent = {} as PlatformComponent;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('PlatformComponent');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/platformcomponent?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getPlatformComponents(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/platformcomponent?${filterString}`, body, httpOptions);
     }

    getPlatformComponentByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/platformcomponent/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/platformcomponent/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<PlatformComponent>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/devices/${resourceName}`);
	 } else {
         	return this.http_.get<PlatformComponent>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/devices/${resourceName}`);
	 }
     }

//     load(onLoad?: onPlatformComponentDetailLoadCallback, onFail?: onPlatformComponentDetailLoadFailCallback): void {
//         this.http_.get<PlatformComponent>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onPlatformComponentUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onPlatformComponentUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(device: PlatformComponent, _location: string): Observable<PlatformComponent> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<PlatformComponent>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/devices/`+device.metadata.name, device, httpOptions);
      }



    add(device: PlatformComponent, _location: string): Observable<PlatformComponent> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<PlatformComponent>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/devices/`, device, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/devices/${id}`);
    }

    getInstance(): PlatformComponent {
        return this.instance_;
    }
}
