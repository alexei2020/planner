
// tslint:disable

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { GridModule, PDFModule, ExcelModule, BodyModule, SharedModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule, MatCheckbox } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';

import { GridsterModule } from 'angular-gridster2';

import 'hammerjs';

import { ComponentsModule } from '../../components/module';
import { DeviceattributesetDetailPageComponent } from '../../pages/deviceattributeset.gen/detail/component';
import { DeviceattributesetRoutingModule} from './routing';
import { DeviceattributesetListPageComponent } from '../../pages/deviceattributeset.gen/list/component';
import { DeviceattributesetSharedModule} from '../../modules/deviceattributeset-shared.gen/deviceattributeset-shared.module';
import { WidgetModule } from '../../main/widget/widget.module';
import { SwitchModule } from '../../modules/switch-shared/switch-shared-module';
import { LabelsSharedModule } from '../../modules/labels-shared/labels-shared.module';
import { FuseDirectivesModule } from '@fuse/directives/directives';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { SharedModule as WSSharedModule } from 'app/main/shared/shared.module';

@NgModule({
  imports: [
    BodyModule,
    ChartsModule,
    CommonModule,
    ComponentsModule,
    DeviceattributesetRoutingModule,
    DeviceattributesetSharedModule,
    DropDownsModule,
    ExcelModule,
    FlexLayoutModule,
    FormsModule,
    GridModule,
    GridsterModule,
    InputsModule,
    LayoutModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatSelectModule,
    PDFModule,
    PopupModule,
    ReactiveFormsModule,
    SharedModule,
    WidgetModule,
    SwitchModule,
    LabelsSharedModule,
    ButtonsModule,
    DialogModule,
    WSSharedModule,
    FuseDirectivesModule
  ],
  declarations: [
    DeviceattributesetListPageComponent,
    DeviceattributesetDetailPageComponent,
  ],
  exports: [
    DeviceattributesetListPageComponent,
    DeviceattributesetDetailPageComponent,
  ]
})
export class DeviceattributesetModule { }


