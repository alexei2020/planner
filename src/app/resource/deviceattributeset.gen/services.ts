

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { DeviceAttributeSet } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onDeviceAttributeSetDetailLoadCallback = (instance?: DeviceAttributeSet) => void;
type onDeviceAttributeSetDetailLoadFailCallback = (instance?: DeviceAttributeSet) => void;
@Injectable({
    providedIn: 'root'
})
export class DeviceAttributeSetDetailService {
    public onDeviceAttributeSetUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: DeviceAttributeSet = {} as DeviceAttributeSet;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('DeviceAttributeSet');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/deviceattributesets?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getDeviceAttributeSets(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/deviceattributesets?${filterString}`, body, httpOptions);
     }

    getDeviceAttributeSetByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/deviceattributesets/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/deviceattributesets/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<DeviceAttributeSet>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/deviceattributesets/${resourceName}`);
	 } else {
         	return this.http_.get<DeviceAttributeSet>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/deviceattributesets/${resourceName}`);
	 }
     }

//     load(onLoad?: onDeviceAttributeSetDetailLoadCallback, onFail?: onDeviceAttributeSetDetailLoadFailCallback): void {
//         this.http_.get<DeviceAttributeSet>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onDeviceAttributeSetUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onDeviceAttributeSetUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(deviceattributeset: DeviceAttributeSet, _location: string): Observable<DeviceAttributeSet> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<DeviceAttributeSet>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/deviceattributesets/`+deviceattributeset.metadata.name, deviceattributeset, httpOptions);
      }



    add(deviceattributeset: DeviceAttributeSet, _location: string): Observable<DeviceAttributeSet> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<DeviceAttributeSet>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/deviceattributesets/`, deviceattributeset, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/deviceattributesets/${id}`);
    }

    getInstance(): DeviceAttributeSet {
        return this.instance_;
    }
}
