import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import {
  DialogService,
  DialogCloseResult,
} from "@progress/kendo-angular-dialog";
import { ScheduleCustomOptionsComponent } from "../schedule-custom-options/schedule-custom-options.component";
import { timezoneNames, ZonedDate } from "@progress/kendo-date-math";
import { format } from "date-fns";
import { Item } from "../shared/model";
import "@progress/kendo-date-math/tz/all";
import { ScheduleConfig } from "../schedule.config";
import { SchedulerSpec } from "../../../typings/backendapi.gen";
import { BreadcrumbService, FormValidationInfo } from 'app/main/shared/breadcrumb-bar.service';
import { takeUntil } from 'rxjs/operators';
import { Subject, combineLatest } from 'rxjs';
import { FuseLocationService } from 'app/services/location.service';
import { format as tzFormat, utcToZonedTime, zonedTimeToUtc } from "date-fns-tz";
import jstz from 'jstz';
const filterUncommon = (list) => list.filter((l) => l.indexOf("/") > -1);
const timezones = filterUncommon(timezoneNames()).sort((a, b) =>
  a.localeCompare(b)
);

@Component({
  selector: "app-schedule",
  templateUrl: "./schedule-settings.component.html",
  styleUrls: ["./schedule-settings.component.scss"],
})
export class ScheduleSettingsComponent implements OnInit, OnDestroy {
  private _isTimezoneEnabled: boolean;
  @Input() public scheduler: SchedulerSpec;
  @Input() set isTimezoneEnabled(value: boolean) {
    this._isTimezoneEnabled = value;
  }
  get isTimezoneEnabled(): boolean {
    return this._isTimezoneEnabled;
  }
  public summary: string;
  public isNow: boolean;
  public allTimezones: string[] = timezones;
  public timezone: string = "America/Chicago";

  public hasCustomValue: boolean;
  public selectedDate: Date;
  public format: string = "MM/dd/yyyy hh:mm a";
  public repeatListItems: Array<Item>;
  public selectedValue: Item;

  public selectedTimeZone: string;
  private _unsubscribeAll: Subject<any> = new Subject();
  constructor(
    private _dialogService: DialogService,
    private _breadcrumbService: BreadcrumbService,
    private _locationService: FuseLocationService,
  ) {
  }

  ngOnInit(): void {
    combineLatest([this._locationService.isBrowserTime, this._locationService.locationData])
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(([isBrowser, locationInfo])=>{
        console.log("is browser", isBrowser);
        console.log("is locationInfo", locationInfo);
        if(isBrowser)
          this.selectedTimeZone = jstz.determine().name();
        else
          this.selectedTimeZone = (locationInfo as any).timezone;
        
        this.resetTimeByTimeZone();
    })
    this._locationService.isBrowserTime.pipe(takeUntil(this._unsubscribeAll)).subscribe(res=>{
      console.log("schedule setting, broswer subscriber");

    });

    this.hasCustomValue = false;
    // console.log("000000 location->", this._locationService.locationDataAsKeyValue());
    // console.log("000000 selected time->", this._locationService.isBrowserTime.getValue());

    this.isNow = this.scheduler.now;
    this.resetItems();
    // this.isTimezoneEnabled = ScheduleConfig.isTimezoneEnabled;
  }

  resetTimeByTimeZone(){
    if (this.scheduler && this.scheduler.date) {
      var dateonly = this.scheduler.date.split(",", 1);
      this.selectedDate = utcToZonedTime(new Date(dateonly[0]), this.selectedTimeZone);
    } else {
      this.selectedDate = utcToZonedTime(new Date(), this.selectedTimeZone);
      this.scheduler.date = format(this.selectedDate, this.format);
    }
    console.log("selcted timezone in schedule is--->", this.selectedTimeZone);
    this.resetItems();
  }
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll?.next();
    this._unsubscribeAll?.complete();
}

  resetItems() {
    this.repeatListItems = [];
    var offset = new Date().getTimezoneOffset();
    console.log("offset-->", offset);
    let selectedDateUTC = new Date(this.selectedDate.getTime() + offset * 60 * 1000);
    console.log("UTC time-->", selectedDateUTC);
    let item: Item = {
      text: "Does not repeat",
      value:
        format(selectedDateUTC, "m") +
        " " +
        format(selectedDateUTC, "H") +
        " " +
        selectedDateUTC.getDate() +
        " " +
        format(selectedDateUTC, "M") +
        " " +
        selectedDateUTC.getDay() +
        " " +
        "*",
        //backend needs cron year as *
        //selectedDateUTC.getFullYear(),
    };
    this.repeatListItems.push(item);
    item = {
      text: "Daily",
      value:
        format(selectedDateUTC, "m") +
        " " +
        format(selectedDateUTC, "H") +
        ` * * * *`,
    };
    this.repeatListItems.push(item);
    item = {
      text: "Weekly on " + format(selectedDateUTC, "iiii"),
      value:
        format(selectedDateUTC, "m") +
        " " +
        format(selectedDateUTC, "H") +
        " * * " +
        selectedDateUTC.getDay() +
        " *",
    };
    this.repeatListItems.push(item);
    item = {
      text: "Monthly on the last " + format(selectedDateUTC, "iiii"),
      value:
        format(selectedDateUTC, "m") +
        " " +
        format(selectedDateUTC, "H") +
        " * * " +
        selectedDateUTC.getDay() +
        "L" +
        " *",
    };
    this.repeatListItems.push(item);
    item = {
      text:
        "Annually on " +
        format(selectedDateUTC, "MMMM") +
        " " +
        selectedDateUTC.getDate(),
      value:
        format(selectedDateUTC, "m") +
        " " +
        format(selectedDateUTC, "H") +
        " " +
        selectedDateUTC.getDate() +
        " " +
        format(selectedDateUTC, "M") +
        " *" +
        " *",
    };
    this.repeatListItems.push(item);
    item = {
      text: "Every weekday(Monday to Friday)",
      value:
        format(selectedDateUTC, "m") +
        " " +
        format(selectedDateUTC, "H") +
        " * * 1-5" +
        " *",
    };
    this.repeatListItems.push(item);
    item = {
      text: "Custom...",
      value: "custom",
    };
    this.repeatListItems.push(item);
    console.log("item---->", this.repeatListItems);
    this.selectedValue = this.repeatListItems[0];
    if (this.selectedValue.text !== "Does not repeat") {
      this.summary =
        format(this.selectedDate, this.format) + ", " + this.selectedValue.text;
    } else {
      this.summary = format(this.selectedDate, this.format);
    }
    this.selectedValue = this.repeatListItems.filter(
      (item: Item) => item.value === this.scheduler.crontab
    )[0];
    if (!this.selectedValue) {
	this.selectedValue = { text: "", value: "" };
    }
  }
  onSaveClicked() {
    alert(`now: ${this.isNow}, date: ${this.selectedDate}, cron expression: ${this.selectedValue.value}, summary: ${this.summary}`);
  }
  onChangeDate(date: Date) {
    this.selectedDate = date;
    this.scheduler.date = format(date, this.format);
    this.selectedValue.value = format(date, this.format);
    this.resetItems();
    console.log("changed---->", this.selectedValue);
    if (this.selectedValue.text !== "Does not repeat") {
      this.summary =
        format(this.selectedDate, this.format) + ", " + this.selectedValue.text;
    } else {
      this.summary = format(this.selectedDate, this.format);
    }
    this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true}));
  }
  onChangeNowButton(value: boolean) {
    console.log(this.isNow);
    if (this.isNow) this.summary = "Now";
    else {
      if (this.selectedValue.text !== "Does not repeat")
        this.summary =
          format(this.selectedDate, this.format) +
          ", " +
          this.selectedValue.text;
      else this.summary = format(this.selectedDate, this.format);
    }
    this.scheduler.now = this.isNow;
    this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true}));
  }
  onChange(value: Item) {
    console.log(value);
    this.selectedValue = value;
    if (this.selectedValue.text !== "Does not repeat") {
      this.summary =
        format(this.selectedDate, this.format) + ", " + this.selectedValue.text;
    } else {
      this.summary = format(this.selectedDate, this.format);
    }

    this.scheduler.crontab = this.selectedValue.value;

    if (value.value === "custom") {
      console.log("open dialog...");
      const dialogRef = this._dialogService.open({
        content: ScheduleCustomOptionsComponent,
      });
      dialogRef.content.instance.selectedDate = this.selectedDate;
      dialogRef.content.instance.customRepeatExpression = this.repeatListItems[6];

      dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
        if (result instanceof DialogCloseResult) {
          console.log("close");
          this.selectedValue = this.repeatListItems[0];
        } else {
          console.log("action", result);
          if (result.text === "Cancel") {
            this.selectedValue = this.repeatListItems[0];
          } else {
            console.log("-------------->", result.text);
            let item: Item;
            if (this.hasCustomValue) {
              this.repeatListItems.pop();
              this.repeatListItems.pop();
              item = {
                text: result.text.split(":")[0],
                value: result.text.split(":")[1],
              };
              this.repeatListItems.push(item);
              item = {
                text: "Custom...",
                value: "custom",
              };
              this.repeatListItems.push(item);
            } else {
              this.repeatListItems.pop();
              item = {
                text: result.text.split(":")[0],
                value: result.text.split(":")[1],
              };
              this.repeatListItems.push(item);
              item = {
                text: "Custom...",
                value: "custom",
              };
              this.repeatListItems.push(item);
              this.hasCustomValue = true;
            }
            this.selectedValue = {
              text: result.text.split(":")[0],
              value: result.text.split(":")[1],
            };
          }

          if (this.selectedValue.text !== "Does not repeat") {
            this.summary =
              format(this.selectedDate, this.format) +
              ", " +
              this.selectedValue.text;
          } else {
            this.summary = format(this.selectedDate, this.format);
          }
        }
    	this.scheduler.crontab = this.selectedValue.value;
      });
    }

    this.scheduler.date = this.summary;
    console.log("date-->", this.scheduler.date);
    this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true}));
  }
}
