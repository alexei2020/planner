import { Component, OnInit, Input } from "@angular/core";
import { DialogRef, DialogContentBase } from "@progress/kendo-angular-dialog";
import { WeekDay, RepeatType, Item } from "../shared/model";
import { format, getWeeksInMonth, getWeekOfMonth } from "date-fns";
@Component({
  selector: "app-schedule-custom-options",
  templateUrl: "./schedule-custom-options.component.html",
  styleUrls: ["./schedule-custom-options.component.scss"],
})
export class ScheduleCustomOptionsComponent extends DialogContentBase {
  @Input() selectedDate: Date;
  @Input() customRepeatExpression: Item;
  public date: Date = new Date();
  public autoCorrect: boolean = false;
  public repeatNumber: number = 2;
  public repeat: number = 2;
  public endOption: "never" | "on" | "after";

  public selectedRepeatType: RepeatType;
  public repeatTypeList: Array<RepeatType>;

  public selectedMonthlyRepeatValue: Item;
  public monthlyRepeatList: Array<Item>;

  public weekDays: Array<WeekDay>;

  public didRepeatDisabled: boolean;

  public nthWeekDayLabels = [
    {
      text: "first",
      value: 1,
    },
    {
      text: "second",
      value: 2,
    },
    {
      text: "third",
      value: 3,
    },
    {
      text: "fourth",
      value: 4,
    },
    {
      text: "fifth",
      value: 5,
    },
  ];
  constructor(public dialog: DialogRef) {
    super(dialog);

    this.endOption = "never";

    this.repeatTypeList = ["days", "weeks", "months", "years"];
    this.weekDays = [
      {
        text: "S",
        value: 0,
        isSelected: false,
        fullText: "Sunday",
      },
      {
        text: "M",
        value: 1,
        isSelected: false,
        fullText: "Monday",
      },
      {
        text: "T",
        value: 2,
        isSelected: false,
        fullText: "Tuesday",
      },
      {
        text: "W",
        value: 3,
        isSelected: false,
        fullText: "Wendesday",
      },
      {
        text: "T",
        value: 4,
        isSelected: false,
        fullText: "Thurdsday",
      },
      {
        text: "F",
        value: 5,
        isSelected: false,
        fullText: "Friday",
      },
      {
        text: "S",
        value: 6,
        isSelected: false,
        fullText: "Saturday",
      },
    ];
    this.monthlyRepeatList = [];
  }

  initUI() {
    console.log("custom expression--->", this.customRepeatExpression);
    if (this.customRepeatExpression.value === "custom") {
      this.selectedRepeatType = "weeks";
      this.didRepeatDisabled = true;
    } else {
      if (this.customRepeatExpression.value.split(" ")[5].indexOf("/") !== -1) {
        this.selectedRepeatType = "years";
        this.didRepeatDisabled = false;
        this.repeatNumber = +this.customRepeatExpression.value
          .split(" ")[5]
          .split("/")[1];
      } else if (
        this.customRepeatExpression.value.split(" ")[3].indexOf("/") !== -1
      ) {
        this.selectedRepeatType = "months";
        this.didRepeatDisabled = false;
        this.repeatNumber = +this.customRepeatExpression.value
          .split(" ")[3]
          .split("/")[1];
        if (this.customRepeatExpression.value.split(" ")[4] === "*")
          this.selectedMonthlyRepeatValue = this.monthlyRepeatList[0];
        else this.selectedMonthlyRepeatValue = this.monthlyRepeatList[1];
      } else if (this.customRepeatExpression.value.split(" ")[4] !== "*") {
        this.selectedRepeatType = "weeks";
        this.didRepeatDisabled = true;
        this.customRepeatExpression.value
          .split(" ")[4]
          .split(",")
          .map((item) => {
            this.weekDays[+item].isSelected = true;
          });
      } else if (
        this.customRepeatExpression.value.split(" ")[2].indexOf("/") !== -1
      ) {
        this.selectedRepeatType = "days";
        this.didRepeatDisabled = false;
        this.repeatNumber = +this.customRepeatExpression.value
          .split(" ")[2]
          .split("/")[1];
      }
    }
  }
  ngOnInit() {
    this.weekDays[this.selectedDate.getDay()].isSelected = true;
    let item: Item = {
      text: "Monthly on day " + this.selectedDate.getDate(),
      value: "1",
    };
    this.monthlyRepeatList.push(item);
    const nthWeekDay = getWeekOfMonth(this.selectedDate.getDate() / 7);
    console.log("this.nthWeekDayLabels", this.nthWeekDayLabels, nthWeekDay)
    const label = this.nthWeekDayLabels.filter(
      (item) => item.value === nthWeekDay
    )[0].text;
    item = {
      text: `Monthly on the ${label} ${format(this.selectedDate, "iiii")}`,
      value: "2",
    };
    this.monthlyRepeatList.push(item);
    this.selectedMonthlyRepeatValue = this.monthlyRepeatList[0];

    this.initUI();
  }
  onMonthlyRepeatChange(data: Item) {}
  onRepeatTypeChanage(value: RepeatType) {
    this.selectedRepeatType = value;
    console.log("current repeat type---->", value);
    switch (value) {
      case "days":
      case "months":
      case "years":
        this.didRepeatDisabled = false;
        break;
      case "weeks":
        this.didRepeatDisabled = true;
        break;

      default:
        this.didRepeatDisabled = false;
        break;
    }
  }
  public onCancelAction(): void {
    this.dialog.close({ text: "Cancel" });
  }

  public onConfirmAction(): void {
    let summary = "";
    switch (this.selectedRepeatType) {
      case "days":
        if (this.repeatNumber === 1) {
          let txt =
            "Daily:" +
            format(this.selectedDate, "m") +
            " " +
            format(this.selectedDate, "H") +
            ` * * * *`;
          summary = txt;
        } else {
          summary =
            `Every ${this.repeatNumber} days:` +
            format(this.selectedDate, "m") +
            " " +
            format(this.selectedDate, "H") +
            ` */${this.repeatNumber} * * *`;
        }
        break;
      case "weeks":
        let txt = "";
        if (this.repeatNumber === 1) {
          txt = `Weely on `;
        } else {
          txt = `Every ${this.repeatNumber} weeks on `;
        }
        summary = txt;
        let labels = "";
        let values = "";
        this.weekDays
          .filter((item) => item.isSelected === true)
          .map((item) => {
            labels += item.fullText + ",";
            values += item.value + ",";
          });
        summary += labels.slice(0, -1);
        summary +=
          ":" +
          format(this.selectedDate, "m") +
          " " +
          format(this.selectedDate, "H") +
          ` * * ${values.slice(0, -1)} *`;
        break;
      case "months":
        const nthWeekDay = Math.ceil(this.selectedDate.getDate() / 7);
        const label = this.nthWeekDayLabels.filter(
          (item) => item.value === nthWeekDay
        )[0].text;
        if (this.repeatNumber === 1) {
          if (this.selectedMonthlyRepeatValue.value === "1") {
            summary = "Monthly on day " + this.selectedDate.getDate();
            summary +=
              ":" +
              format(this.selectedDate, "m") +
              " " +
              format(this.selectedDate, "H") +
              ` ${this.selectedDate.getDate()} */${this.repeatNumber} ` +
              "* *";
          } else {
            summary = `Monthly on the ${label} ${format(
              this.selectedDate,
              "iiii"
            )}`;
            summary +=
              ":" +
              format(this.selectedDate, "m") +
              " " +
              format(this.selectedDate, "H") +
              ` * */${this.repeatNumber} ` +
              `${this.selectedDate.getDay()}#${nthWeekDay} *`;
          }
        } else {
          if (this.selectedMonthlyRepeatValue.value === "1") {
            summary = `Every ${
              this.repeatNumber
            } months on day ${this.selectedDate.getDate()}`;
            summary +=
              ":" +
              format(this.selectedDate, "m") +
              " " +
              format(this.selectedDate, "H") +
              ` ${this.selectedDate.getDate()} */${this.repeatNumber} ` +
              "* *";
          } else {
            summary = `Every ${
              this.repeatNumber
            } months on the ${label} ${format(this.selectedDate, "iiii")}`;
            summary +=
              ":" +
              format(this.selectedDate, "m") +
              " " +
              format(this.selectedDate, "H") +
              ` * */${this.repeatNumber} ` +
              `${this.selectedDate.getDay()}#${nthWeekDay} *`;
          }
        }
        break;
      case "years":
        if (this.repeatNumber == 1) {
          summary = `Annually on ${format(
            this.selectedDate,
            "MMMM"
          )} ${this.selectedDate.getDate()}`;
          summary +=
            ":" +
            format(this.selectedDate, "m") +
            " " +
            format(this.selectedDate, "H") +
            ` ${this.selectedDate.getDate()} ${this.selectedDate.getMonth()} ` +
            `* */${this.repeatNumber}`;
        } else {
          summary = `Every ${this.repeatNumber} years on ${format(
            this.selectedDate,
            "MMMM"
          )} ${this.selectedDate.getDate()}`;
          summary +=
            ":" +
            format(this.selectedDate, "m") +
            " " +
            format(this.selectedDate, "H") +
            ` ${this.selectedDate.getDate()} ${this.selectedDate.getMonth()} ` +
            `* */${this.repeatNumber}`;
        }
        break;
      default:
        break;
    }

    this.dialog.close({ text: summary });
  }
  public onWeekDayChange(weekday: WeekDay) {
    for (let idx = 0; idx < this.weekDays.length; idx++) {
      if (this.weekDays[idx].value === weekday.value) {
        this.weekDays[idx] = weekday;
        break;
      }
    }
    const selectedNumber = this.weekDays.filter(
      (item) => item.isSelected === true
    ).length;
    if (selectedNumber === 0) {
      this.weekDays[this.selectedDate.getDay()].isSelected = true;
    }
  }
}
