import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { WindowModule } from '@progress/kendo-angular-dialog';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { LabelModule } from '@progress/kendo-angular-label';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';

import { ScheduleSettingsComponent } from './schedule-settings/schedule-settings.component';
import { ScheduleCustomOptionsComponent } from './schedule-custom-options/schedule-custom-options.component';
import { SchedulePageComponent } from './schedule-page/schedule-page.component';
import { WeekDayItemComponent } from './week-day-item/week-day-item.component';
import { GridsterModule } from 'angular-gridster2';

@NgModule({
  declarations: [
    ScheduleCustomOptionsComponent,
    ScheduleSettingsComponent,
    SchedulePageComponent,
    WeekDayItemComponent,
  ],
  entryComponents: [ScheduleSettingsComponent, ScheduleCustomOptionsComponent],
  exports: [SchedulePageComponent, ScheduleSettingsComponent, WeekDayItemComponent],
  imports: [
    FormsModule,
    GridsterModule,
    CommonModule,
    ButtonsModule,
    DialogModule,
    WindowModule,
    DropDownsModule,
    LabelModule,
    InputsModule,
    DateInputsModule,
  ],
})
export class ScheduleModule {}
