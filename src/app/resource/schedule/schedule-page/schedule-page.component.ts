import { Component, OnInit, EventEmitter } from '@angular/core';
import { DialogService, DialogRef, DialogCloseResult, WindowService } from '@progress/kendo-angular-dialog';
import { ScheduleSettingsComponent } from '../schedule-settings/schedule-settings.component';
import { GridsterItem, GridsterConfig, GridType, CompactType } from 'angular-gridster2';
import { SchedulerSpec } from "../../../typings/backendapi.gen";
import { BreadcrumbService } from 'app/main/shared/breadcrumb-bar.service';
import { PageType } from 'app/main/shared/breadcrumb-bar.service.gen';
@Component({
  selector: 'app-schedule-page',
  templateUrl: './schedule-page.component.html',
  styleUrls: ['./schedule-page.component.scss'],
})
export class SchedulePageComponent implements OnInit {
  resizeEvent: EventEmitter<any> = new EventEmitter<any>();
  item1: GridsterItem;
  options: GridsterConfig;
  scheduleObj: SchedulerSpec;
  constructor(private windowService: WindowService, private _breadcrumbService: BreadcrumbService) {
    this.options = {
      gridType: GridType.Fit,
      compactType: CompactType.None,
      margin: 20,
      minCols: 96,
      maxCols: 96,
      minRows: 96,
      maxRows: 1000,
      maxItemCols: 96,
      minItemCols: 1,
      maxItemRows: 1000,
      minItemRows: 1,
      maxItemArea: 500000,
      minItemArea: 1,
      itemResizeCallback: (item) => {
        this.resizeEvent.emit(item);
      },
      pushItems: true,
      draggable: {
        enabled: false,
      },
      resizable: {
        enabled: false,
      },
    };

    this.item1 = { cols: 96, rows: 96, y: 0, x: 0 };
    this.scheduleObj = {
      now: false,
      date: '2020-10-30T21:59:49.885574999Z'
    }
  }

  ngOnInit(): void {    
    this._breadcrumbService.currentPage.next(PageType.DEFAULT);
  }

  public showWindow() {
    const windowRef = this.windowService.open({
      title: '',
      content: ScheduleSettingsComponent,
      resizable: false,
    });
    windowRef.content.instance.isTimezoneEnabled = false;
    windowRef.content.instance.scheduler = this.scheduleObj;
  }
}
