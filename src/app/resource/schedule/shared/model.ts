export interface WeekDay {
  text: string;
  value: number;
  isSelected: boolean;
  fullText?: string;
}
export type RepeatType = 'days' | 'weeks' | 'months' | 'years';

export interface Item {
  text: string;
  value: string;
}
