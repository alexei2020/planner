import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WeekDay } from '../shared/model';
@Component({
  selector: 'week-day-item',
  templateUrl: './week-day-item.component.html',
  styleUrls: ['./week-day-item.component.scss'],
})
export class WeekDayItemComponent implements OnInit {
  @Input() isSelected: boolean;
  @Input() value: number;
  @Input() text: string;
  @Input() fullText: string;
  @Output() onChange = new EventEmitter<WeekDay>();
  constructor() {}

  ngOnInit(): void {}
  onClicked() {
    this.isSelected = !this.isSelected;
    this.onChange.emit({
      text: this.text,
      value: this.value,
      isSelected: this.isSelected,
      fullText: this.fullText,
    });
  }
}
