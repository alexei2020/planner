// tslint:disable
/*
Copyright (c) 2020 WaveSurf Systems Inc. All rights reserved.
*/
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { SchedulePageComponent } from './schedule-page/schedule-page.component';
const MAIN_ROUTE: Route = {
  path: '',
  component: SchedulePageComponent,
  data: {
    breadcrumb: '',
    link: ['', 'schedule'],
  },
};

@NgModule({
  imports: [RouterModule.forChild([MAIN_ROUTE])],
  exports: [RouterModule],
})
export class ScheduleRoutingModule {}
