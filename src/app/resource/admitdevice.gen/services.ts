

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { AdmitDevice } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onAdmitDeviceDetailLoadCallback = (instance?: AdmitDevice) => void;
type onAdmitDeviceDetailLoadFailCallback = (instance?: AdmitDevice) => void;
@Injectable({
    providedIn: 'root'
})
export class AdmitDeviceDetailService {
    public onAdmitDeviceUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: AdmitDevice = {} as AdmitDevice;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('AdmitDevice');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/admitdevices?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getAdmitDevices(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/admitdevices?${filterString}`, body, httpOptions);
     }

    getAdmitDeviceByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/admitdevices/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/admitdevices/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<AdmitDevice>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/admitdevices/${resourceName}`);
	 } else {
         	return this.http_.get<AdmitDevice>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/admitdevices/${resourceName}`);
	 }
     }

//     load(onLoad?: onAdmitDeviceDetailLoadCallback, onFail?: onAdmitDeviceDetailLoadFailCallback): void {
//         this.http_.get<AdmitDevice>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onAdmitDeviceUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onAdmitDeviceUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(admitdevice: AdmitDevice, _location: string): Observable<AdmitDevice> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<AdmitDevice>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/admitdevices/`+admitdevice.metadata.name, admitdevice, httpOptions);
      }



    add(admitdevice: AdmitDevice, _location: string): Observable<AdmitDevice> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<AdmitDevice>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/admitdevices/`, admitdevice, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/admitdevices/${id}`);
    }

    getInstance(): AdmitDevice {
        return this.instance_;
    }
}
