import { Component, ElementRef, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-rule-parse',
  template: `
            <div id="rulesParsedContainer">
              <kendo-multiselect *ngIf="parsedData" class="multiselect-readonly"
              name="parsedData"
              [readonly]="true"
              [(ngModel)]="parsedData">
            </kendo-multiselect>
            </div>`,
  styleUrls: ['./rule-parse.component.scss']
})
export class RuleParseComponent implements OnInit, OnChanges {

  @Input()
  parsedData: string[];
  @Output() setRows: any = new EventEmitter();
  public previousSize: number;

  constructor(private nativeElement: ElementRef) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.setHeight();
  }

  ngOnInit(): void {
    this.setHeight();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
      this.setHeight();
  }

  setHeight() {
    setTimeout(() => {
      if (this.nativeElement && this.nativeElement.nativeElement) {
        const size = this.nativeElement.nativeElement.offsetHeight;
        this.previousSize = size;
        this.setRows.emit(size);
      }
    });
  }
}
