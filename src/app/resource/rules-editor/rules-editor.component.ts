import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { ChipRemoveEvent } from "@progress/kendo-angular-buttons";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { RuleCard, RuleRow } from "./rule-row";
import { RulesEditorService } from "./rules-editor.service";
import { EndPointSet, DeviceAttributeSet, AttributeSet } from 'app/typings/backendapi.gen';
import { BreadcrumbService, FormValidationInfo } from 'app/main/shared/breadcrumb-bar.service';
import { MathUtils } from "three";
import generateUUID = MathUtils.generateUUID;
import * as _ from 'lodash';

import AnimEvent from '../../../../node_modules/anim-event/anim-event.esm.js';
import Timeout = NodeJS.Timeout;
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { process, GroupDescriptor } from '@progress/kendo-data-query';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogCloseResult, DialogResult, DialogService } from '@progress/kendo-angular-dialog';
import { KendoConfirmDialog } from "app/main/shared/dialogs/kendo-confirm-dialog/kendo-confirm-dialog.component";
import { DictionaryService } from 'app/services/dictionary.service';

const createFormGroup = dataItem => new FormGroup({
  id: new FormControl(dataItem.id),
  uuid: new FormControl(dataItem.uuid),
  dict: new FormControl(dataItem.dict),
  field: new FormControl(dataItem.field),
  criteria: new FormControl(dataItem.criteria),
  values: new FormControl(dataItem.values),
  description: new FormControl(dataItem.description),
});

@Component({
  selector: 'app-rules-editor',
  templateUrl: './rules-editor.component.html',
  styleUrls: ['./rules-editor.component.scss']
})
export class RulesEditorComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild('grid') private grid;

  @Input() pageSize: number;

  @Input()
  endpointset: EndPointSet | DeviceAttributeSet | AttributeSet;
  @Output() setRows: any = new EventEmitter();
  isScrolling: Timeout;

  dictRoot: string;
  ruleDicts = [];
  ruleDictsTable = [];
  ruleFields = [];
  ruleFieldsTable = [];
  ruleValues = [];

  ruleCriteria = [];

  private criteriaToSign = {
    "Contains": "contains",
    "DoesNotContain": "Does not contain",
    "Equals": "=",
    "NotEquals": "!=",
    "Greater": ">",
    "GreaterOrEquals": ">=",
    "Lesser": "<",
    "LesserOrEquals": "<=",
    "RegEx": "RegEx",
    "In": "In",
    "NotIn": "Not In",
    "StartsWith": "Starts with",
    "EndsWith": "Ends with",
  }

  public dataTable = [];
  public formGroup: FormGroup;
  public groups: GroupDescriptor[] = [{ field: 'id' }];
  public skip = 0 as number;
  public gridView: GridDataResult;
  public mySelection: string[] = [];
  public groupable = true;
  public previousSize: number;
  public isEditing: boolean = false;
  public clickedRowIndex: string;
  public clickedRowItem: string;

  dataSource: any[] = [];
  @Input() parsedData: string[];
  @Output() parsedDataChange = new EventEmitter();

  private editedRowIndex: number;

  private _unsubscribeAll: Subject<any> = new Subject<any>();

  constructor(
    private _reService: RulesEditorService,
    private _breadcrumbService: BreadcrumbService,
    private _dialogService: DialogService,
    private _dict: DictionaryService,
  ) {
    this.gridView = {
      data: [],
      total: 0
    };
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.setHeight();
  }

  setHeight() {
    setTimeout(() => {
      if (this.grid && this.grid.wrapper) {
        let size = 0;
        if (this.grid.wrapper) {
          const header = this.grid.wrapper.nativeElement.querySelector('.k-grouping-header');
          const grid = this.grid.wrapper.nativeElement.querySelector('.k-grid-aria-root');
          const pager = this.grid.wrapper.nativeElement.querySelector('.k-pager-wrap');
          if (header) {
            size += header.offsetHeight;
          }
          if (grid) {
            size += grid.offsetHeight;
          }
          if (pager) {
            size += pager.offsetHeight;
          }
        }

        if (size == 0) {
          size = this.grid.wrapper.nativeElement.offsetHeight;
        }

        this.previousSize = size;

        this.setRows.emit(size);
      }
    });
  }

  public editHandler(dataItem, rowIndex) {
    this.closeEditor(this.grid);
    this.formGroup = createFormGroup({
      id: dataItem.id,
      uuid: dataItem.uuid,
      dict: dataItem.dict,
      field: dataItem.field,
      criteria: dataItem.criteria,
      values: dataItem.values,
      description: dataItem.description,
    });

    this.grid.editRow(rowIndex, this.formGroup);
    this.isEditing = true;
    this.editedRowIndex = rowIndex;
    this.loadDropdowns()
  }

  changeHandler(formValue: any, rowIndex: any) {
    const dataItem = {
      ...formValue.value,
      dict: this.formGroup.controls.dict.value,
      field: this.formGroup.controls.field.value,
      criteria: this.formGroup.controls.criteria.value,
      values: this.formGroup.controls.values.value,
      description: this.formGroup.controls.description.value,
    }
    this._reService.editRow(dataItem, dataItem.id);
    this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({ isValid: true }));
    this.loadDropdowns()
  }

  onCellClick({ rowIndex, dataItem }) {
    this.closeEditor(this.grid);
    this.clickedRowItem = dataItem;
    this.clickedRowIndex = rowIndex;
    this.editHandler(this.clickedRowItem, this.clickedRowIndex)
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    if (rowIndex !== undefined) {
      grid.closeRow(rowIndex);
    }
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
    this.isEditing = false;
    this.clickedRowIndex = undefined;
    this.clickedRowItem = undefined;
  }

  loadDropdowns() {
    this._dict.getDictionaryTable(this.dictRoot, "dict").pipe(takeUntil(this._unsubscribeAll)).subscribe((dict) => {
      if (!dict) {
        this.ruleDicts = [];
        return;
      }
      this.ruleDicts = dict.map(elem => elem.name);
      this.ruleDictsTable = dict;
    });
    
    var table = this.ruleDictsTable.find(el => el.name === this.formGroup.controls.dict.value);
    if (!table) {
      this.ruleFields = [];
      return;
    }
    this._dict.getDictionaryTable(table.next.name, table.next.type).pipe(takeUntil(this._unsubscribeAll)).subscribe((dict) => {
      if (!dict) {
        this.ruleFields = [];
        return;
      }
      this.ruleFields = dict.map(elem => elem.name);
      this.ruleFieldsTable = dict;
    });

    var table = this.ruleFieldsTable.find(el => el.name === this.formGroup.controls.field.value);
    if (!table) {
      this.ruleValues = [];
      return;
    }
    this._dict.getDictionaryTable(table.next.name, table.next.type).pipe(takeUntil(this._unsubscribeAll)).subscribe((dict) => {
      if (!dict) {
        this.ruleValues = [];
        return;
      }
      this.ruleValues = dict.map(elem => ({text: elem.name, value: elem.value}));
    });
  }

  setContext(kind: string) {
    switch(kind) {
    case 'EndPointSet':
      this.dictRoot = 'AOV:root';

      this.ruleCriteria = [ 'Equals', 'NotEquals', 'Lesser', 'LesserOrEquals', 'Greater', 'GreaterOrEquals', 'Contains', 'DoesNotContain', 'RegEx', 'In', 'NotIn', 'StartsWith', 'EndsWith'];

      if (this.mabCard()) {
        this.ruleFields = ['MACAddress'];
        this.ruleCriteria = ['Equals', 'In', 'Not In'];
      }
      break;
    case 'DeviceAttributeSet':
      this.dictRoot = 'DevAttrSet-AOV:root';
      this.ruleCriteria = [ 'Equals'];
      break;

    case 'AttributeSet':
      this.dictRoot = 'AttrSet-AOV:root';

      this.ruleCriteria = [ 'Equals', 'NotEquals', 'Lesser', 'LesserOrEquals', 'Greater', 'GreaterOrEquals', 'Contains', 'DoesNotContain', 'RegEx', 'In', 'NotIn', 'StartsWith', 'EndsWith'];

      if (this.mabCard()) {
        this.ruleFields = ['MACAddress'];
        this.ruleCriteria = ['Equals', 'In', 'Not In'];
      }
      break;
    }
  }

  ngOnInit(): void {
    if (!_.has(this.endpointset, 'spec.conditions')) {
      _.set(this.endpointset, 'spec.conditions', []);
    }
    this.setContext(this.endpointset.kind);
    this._reService.sharedData.pipe(takeUntil(this._unsubscribeAll)).subscribe((data) => {

      this.dataTable = [];
      data.forEach(item => {
        item.rows.forEach((rowItem) => {
          this.dataTable.push({ id: item.uuid, ...rowItem })
        })
      });
      this.dataSource = [];
      this.dataSource = data;
      // this.gridView = process(this.dataTable, { group: this.groups });
      const result: GridDataResult = process(this.dataTable, {
        group: this.groups,
      });
      this.gridView = {
        data: result.data,
        total: result.total,
      };
      console.log(this.gridView);
      this.parseData();
      this.saveData();
      this.setHeight();
    });
    this.load();
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.endpointset) {
      this._breadcrumbService.isDeviceDetailEditMode.next(null);
      this.closeEditor(this.grid);
      this.load();
    }
  }

  load(): void {
    var rows = 0;
    this._reService.reset();
    if (this.endpointset.spec.conditions.length === 0) {
      this.addNewCard();
      return;
    }
    this.endpointset.spec.conditions.forEach((card) => {
      rows++;
      var uuidCard = generateUUID();
      this._reService.addCard(uuidCard);
      if (card.aovList) {
        card.aovList.forEach((row) => {
          var uuidRow = generateUUID();
          rows++;
          this._reService.addRow(uuidRow, uuidCard);
          var ruleRow: RuleRow = {
            uuid: uuidRow,
            isSelected: false,
            dict: row.dict,
            field: row.attr,
            criteria: row.op,
            values: row.val,
            description: row.description,
          };
          this._reService.editRow(ruleRow, uuidCard);
        });
      }
    });
    this.setHeight();
  }

  addNewRow(dataItem: any, rowIndex: number, isNew?: any): void {
    let id: string;
    if(dataItem.value) {
      id = dataItem.value.id
    } else {
      id = dataItem.id
    }
    const uuid = generateUUID();
    this._reService.addRow(uuid, id);
    this.closeEditor(this.grid);
    this.formGroup = createFormGroup({
      id:id,
      uuid: uuid,
      dict: '',
      field: '',
      criteria: '',
      values: '',
      description: '',
    });

    this.grid.editRow(++rowIndex, this.formGroup);
    this.editedRowIndex = rowIndex;
    this.setHeight();
    this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({ isValid: true }));
  }

  public removeHandler(dataItem: any, rowIndex: any, isRow: boolean): void {
    this.closeEditor(this.grid);
    if (isRow) {
      this._reService.removeRow(dataItem.uuid, dataItem.id);
      this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({ isValid: true }));
      // If no more cards left, add back an empty one to create new entries
      if (this.dataSource.length === 0) {
        this.addNewCard();
      }
    } else {
      const dialogRef = this._dialogService.open({
        title: `Remove Card`,
        content: KendoConfirmDialog,
        width: "420px",
        height: "200px",
      });
      const dialogInstance = dialogRef.content.instance;
      dialogInstance.message =
        "Are you sure you want to remove this OR?";
      dialogInstance.dialog = dialogRef;
      dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
        if (response instanceof DialogCloseResult || response.text == "No") {
        } else {
          this._reService.removeCard(dataItem.id);
          this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({ isValid: true }));
          // If no more cards left, add back an empty one to create new entries
          if (this.dataSource.length === 0) {
            this.addNewCard();
          }
        }
      });
    }
    this.setHeight();
  }

  addNewCard(): void {
    this.closeEditor(this.grid);
    const uuidCard = generateUUID();
    const uuidRow = generateUUID();
    const index = ++this.dataTable.length;
    this._reService.addCard(uuidCard);
    this._reService.addRow(uuidRow, uuidCard);
    this.formGroup = createFormGroup({
      id: uuidCard,
      uuid: uuidRow,
      dict: '',
      field: '',
      criteria: '',
      values: '',
      description: '',
    });
    this.setHeight();
  }

  parseData(): void {
    let parsed = [];
    this.dataSource.forEach((card, cardIndex, cardArray) => {
      let rowString = '';
      card.rows.forEach((row, rowIndex, rowArray) => {
        let valuesString = row.values;
        let criteriaSign: string;

        criteriaSign = this.criteriaToSign[row.criteria];
        if (!criteriaSign) criteriaSign = "";

        // At least one field should be valid to display the row
        if (!row.field && !criteriaSign && !valuesString) {
          return;
        }

        if (!valuesString) {
            rowString += row.field + ' ' + criteriaSign;
        } else {
            rowString += row.field + ' ' + criteriaSign + ' \'' + valuesString + '\'';
        }
        if (rowIndex < rowArray.length - 1) {
          rowString += ' AND ';
        }
      });
      if (rowString) {
        parsed = parsed.concat(rowString);
      }
      if (cardIndex < cardArray.length - 1) {
        parsed = parsed.concat(' OR ');
      }
    });
    this.parsedData = parsed;
    this.parsedDataChange.emit(parsed);
  }

  saveData(): void {
    var rows = 0;
    this.endpointset.spec.conditions = [];
    this.dataSource.forEach((card, cardIndex) => {
      rows++;
      card.rows.forEach((row, rowIndex) => {
        rows++;
        var value = row.values;
        _.set(this.endpointset.spec.conditions, `[${cardIndex}].aovList[${rowIndex}].dict`, row.dict);
        _.set(this.endpointset.spec.conditions, `[${cardIndex}].aovList[${rowIndex}].attr`, row.field);
        _.set(this.endpointset.spec.conditions, `[${cardIndex}].aovList[${rowIndex}].op`, row.criteria);
        _.set(this.endpointset.spec.conditions, `[${cardIndex}].aovList[${rowIndex}].val`, value);
        _.set(this.endpointset.spec.conditions, `[${cardIndex}].aovList[${rowIndex}].description`, row.description);
      });
    });
  }

  mabCard(): boolean {
    return this.endpointset.metadata.name.includes("mab") ||
      this.endpointset.metadata.name.includes("byod-registered") ||
      this.endpointset.metadata.name.includes("corporate");
  }

  ngOnDestroy(): void {
    this._reService.reset();
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
