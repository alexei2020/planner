
// tslint:disable

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { GridModule, PDFModule, ExcelModule, BodyModule, SharedModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule, MatCheckbox } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';

import { GridsterModule } from 'angular-gridster2';

import 'hammerjs';

import { ComponentsModule } from '../../components/module';
import { WidgetModule } from '../../main/widget/widget.module';
import { FuseDirectivesModule } from '@fuse/directives/directives';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { RulesEditorComponent } from './rules-editor.component';
import { RuleParseComponent } from './rule-parse/rule-parse.component';
import { SharedModule as WSSharedModule } from 'app/main/shared/shared.module';

@NgModule({
  imports: [
    BodyModule,
    ChartsModule,
    CommonModule,
    ComponentsModule,
    DropDownsModule,
    ExcelModule,
    FlexLayoutModule,
    FormsModule,
    GridModule,
    GridsterModule,
    InputsModule,
    LayoutModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatSelectModule,
    PDFModule,
    PopupModule,
    ReactiveFormsModule,
    SharedModule,
    WidgetModule,
    ButtonsModule,
    DialogModule,
    WSSharedModule,
    FuseDirectivesModule
  ],
  declarations: [
    RulesEditorComponent,
    RuleParseComponent,
  ],
  exports: [
    RulesEditorComponent,
    RuleParseComponent,
  ]
})
export class RulesEditorModule { }


