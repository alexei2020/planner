import { Injectable } from '@angular/core';
import { RuleCard, RuleRow } from "./rule-row";
import { MathUtils } from "three";
import { Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RulesEditorService {

  private dataSource = new Subject<RuleCard[]>();
  sharedData = this.dataSource.asObservable();

  private data: RuleCard[] = [

  ];

  constructor() { }

  addCard(uuid: string): void {
    this.data.push({
      uuid,
      rows: []
    });
    this.dataSource.next(this.data);
  }

  removeCard(uuid: string): void {
    const removeIndex = this.data.findIndex((rc) => {
      return rc.uuid === uuid;
    });
    this.data.splice(removeIndex, 1);
    this.dataSource.next(this.data);
  }

  addRow(rowUUID: string, cardUUID: string): void {
    const card = this.data.find((rc) => {
      return rc.uuid === cardUUID;
    });
    const row: RuleRow = {
      uuid: rowUUID,
      isSelected: false,
      dict: '',
      field: '',
      criteria: '',
      values: '',
      description: "",
    };
    card.rows.push(row);
    this.dataSource.next(this.data);
  }

  removeRow(rowUUID: string, cardUUID: string): void {
    let data = this.data.filter(() => true);
    const card = data.find((rc) => rc.uuid === cardUUID);

    if (card.rows.length === 1) {
      data = data.filter(item => item.uuid !== cardUUID);
    } else {
      data = data.map(item => ({
        ...item,
        rows: item.rows.filter(record => record.uuid !== rowUUID)
      }));
    }

    this.data = data;

    this.dataSource.next(data);
  }

  editRow(row: RuleRow, cardUUID: string): void {
    const card = this.data.find((c) => {
      return c.uuid === cardUUID;
    });
    let targetRowIndex = card.rows.findIndex((r) => {
      return r.uuid === row.uuid;
    });
    card.rows[targetRowIndex] = row;
    this.dataSource.next(this.data);
  }

  reset(): void {
    this.data = [];
  }
}
