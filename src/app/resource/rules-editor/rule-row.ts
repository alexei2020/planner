export interface RuleCard {
    uuid: string;
    rows: RuleRow[];
}
export interface RuleRow {
    id?: string;
    uuid: string;
    isSelected: boolean;
    dict: string;
    field: string;
    criteria: string;
    values: string;
    description: string;
}
