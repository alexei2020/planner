

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { Subinterface } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onSubinterfaceDetailLoadCallback = (instance?: Subinterface) => void;
type onSubinterfaceDetailLoadFailCallback = (instance?: Subinterface) => void;
@Injectable({
    providedIn: 'root'
})
export class SubinterfaceDetailService {
    public onSubinterfaceUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: Subinterface = {} as Subinterface;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('Subinterface');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/subinterfaces?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getSubinterfaces(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/subinterfaces?${filterString}`, body, httpOptions);
     }

    getSubinterfaceByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/subinterfaces/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/subinterfaces/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<Subinterface>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/subinterfaces/${resourceName}`);
	 } else {
         	return this.http_.get<Subinterface>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/subinterfaces/${resourceName}`);
	 }
     }

//     load(onLoad?: onSubinterfaceDetailLoadCallback, onFail?: onSubinterfaceDetailLoadFailCallback): void {
//         this.http_.get<Subinterface>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onSubinterfaceUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onSubinterfaceUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(subinterface: Subinterface, _location: string): Observable<Subinterface> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<Subinterface>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/subinterfaces/`+subinterface.metadata.name, subinterface, httpOptions);
      }



    add(subinterface: Subinterface, _location: string): Observable<Subinterface> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<Subinterface>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/subinterfaces/`, subinterface, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/subinterfaces/${id}`);
    }

    getInstance(): Subinterface {
        return this.instance_;
    }
}
