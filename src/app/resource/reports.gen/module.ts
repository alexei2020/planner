

// tslint:disable

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { GridModule, PDFModule, ExcelModule, BodyModule, SharedModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { LayoutModule } from '@progress/kendo-angular-layout';

import 'hammerjs';

import { GridsterModule } from 'angular-gridster2';

import { FlexLayoutModule } from '@angular/flex-layout';

import { ReportsPageComponent } from '../../pages/reports.gen/list/component';
import { ReportsRoutingModule } from './routing';
import { ComponentsModule } from '../../components/module';
import { ReportsDetailPageComponent } from 'app/pages/reports.gen/detail/component';
import { WidgetModule } from 'app/main/widget/widget.module';
import { ReportChartViewComponent } from 'app/pages/reports.gen/chart/component';
import { FuseDirectivesModule } from '@fuse/directives/directives';

// generated
import { AuditSharedModule } from '../../modules/audit-shared.gen'
import { EndpointSharedModule } from '../../modules/endpoint-shared.gen'
import { EndpointsetSharedModule } from '../../modules/endpointset-shared.gen'
import { NetworktemplateSharedModule } from '../../modules/networktemplate-shared.gen'
import { PlatformcomponentSharedModule } from '../../modules/platformcomponent-shared.gen'
import { AdmitdeviceSharedModule } from '../../modules/admitdevice-shared.gen'
import { AttributesetSharedModule } from '../../modules/attributeset-shared.gen'
import { DeviceSharedModule } from '../../modules/device-shared.gen'
import { DeviceattributesetSharedModule } from '../../modules/deviceattributeset-shared.gen'
import { DeviceuiSharedModule } from '../../modules/deviceui-shared.gen'
import { EndpointauxSharedModule } from '../../modules/endpointaux-shared.gen'
import { FileSharedModule } from '../../modules/file-shared.gen'
import { FloorspaceSharedModule } from '../../modules/floorspace-shared.gen'
import { IfinterfaceSharedModule } from '../../modules/ifinterface-shared.gen'
import { LocationSharedModule } from '../../modules/location-shared.gen'
import { RadioSharedModule } from '../../modules/radio-shared.gen'
import { RadioinfoSharedModule } from '../../modules/radioinfo-shared.gen'
import { SnapshottaskSharedModule } from '../../modules/snapshottask-shared.gen'
import { SsidSharedModule } from '../../modules/ssid-shared.gen'
import { SubinterfaceSharedModule } from '../../modules/subinterface-shared.gen'
import { TenantSharedModule } from '../../modules/tenant-shared.gen'
import { WalljackSharedModule } from '../../modules/walljack-shared.gen'

const SharedVisualModules = [
    AuditSharedModule,
    EndpointSharedModule,
    EndpointsetSharedModule,
    NetworktemplateSharedModule,
    PlatformcomponentSharedModule,
    AdmitdeviceSharedModule,
    AttributesetSharedModule,
    DeviceSharedModule,
    DeviceattributesetSharedModule,
    DeviceuiSharedModule,
    EndpointauxSharedModule,
    FileSharedModule,
    FloorspaceSharedModule,
    IfinterfaceSharedModule,
    LocationSharedModule,
    RadioSharedModule,
    RadioinfoSharedModule,
    SnapshottaskSharedModule,
    SsidSharedModule,
    SubinterfaceSharedModule,
    TenantSharedModule,
    WalljackSharedModule,
];

@NgModule({
  imports: [
    ReactiveFormsModule, 
    ReportsRoutingModule,
    PDFModule, 
    ExcelModule, 
    InputsModule,
    GridModule, 
    PopupModule, 
    LayoutModule, 
    ChartsModule,
    MatMenuModule,
    BodyModule,
    MatButtonModule,
    MatIconModule,
    SharedModule,
    CommonModule, 
    GridsterModule, 
    ComponentsModule, 
    MatCardModule,
    MatFormFieldModule, 
    MatInputModule, 
    FlexLayoutModule, 
    FormsModule, 
    MatCheckboxModule,
    HttpClientModule,
    WidgetModule,
    SharedVisualModules,
    FuseDirectivesModule,
  ],
  declarations: [
    ReportsPageComponent,
    ReportsDetailPageComponent,
    ReportChartViewComponent,
  ],
})
export class ReportsModule { }

