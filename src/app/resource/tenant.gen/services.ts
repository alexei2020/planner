

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { Tenant } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onTenantDetailLoadCallback = (instance?: Tenant) => void;
type onTenantDetailLoadFailCallback = (instance?: Tenant) => void;
@Injectable({
    providedIn: 'root'
})
export class TenantDetailService {
    public onTenantUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: Tenant = {} as Tenant;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('Tenant');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/tenants?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getTenants(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/tenants?${filterString}`, body, httpOptions);
     }

    getTenantByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/tenants/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/tenants/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<Tenant>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/tenants/${resourceName}`);
	 } else {
         	return this.http_.get<Tenant>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/tenants/${resourceName}`);
	 }
     }

//     load(onLoad?: onTenantDetailLoadCallback, onFail?: onTenantDetailLoadFailCallback): void {
//         this.http_.get<Tenant>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onTenantUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onTenantUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(tenant: Tenant, _location: string): Observable<Tenant> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<Tenant>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/tenants/`+tenant.metadata.name, tenant, httpOptions);
      }



    add(tenant: Tenant, _location: string): Observable<Tenant> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<Tenant>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/tenants/`, tenant, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/tenants/${id}`);
    }

    getInstance(): Tenant {
        return this.instance_;
    }
}
