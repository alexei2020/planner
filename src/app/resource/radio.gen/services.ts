

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { Radio } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onRadioDetailLoadCallback = (instance?: Radio) => void;
type onRadioDetailLoadFailCallback = (instance?: Radio) => void;
@Injectable({
    providedIn: 'root'
})
export class RadioDetailService {
    public onRadioUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: Radio = {} as Radio;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('Radio');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/radios?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getRadios(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/radios?${filterString}`, body, httpOptions);
     }

    getRadioByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/radios/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/radios/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<Radio>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/radios/${resourceName}`);
	 } else {
         	return this.http_.get<Radio>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/radios/${resourceName}`);
	 }
     }

//     load(onLoad?: onRadioDetailLoadCallback, onFail?: onRadioDetailLoadFailCallback): void {
//         this.http_.get<Radio>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onRadioUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onRadioUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(radio: Radio, _location: string): Observable<Radio> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<Radio>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/radios/`+radio.metadata.name, radio, httpOptions);
      }



    add(radio: Radio, _location: string): Observable<Radio> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<Radio>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/radios/`, radio, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/radios/${id}`);
    }

    getInstance(): Radio {
        return this.instance_;
    }
}
