
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LabelService } from '../services'
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { process } from '@progress/kendo-data-query';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

const createFormGroup = dataItem => new FormGroup({
    name: new FormControl(dataItem.name),
    location: new FormControl(dataItem.location),
    description: new FormControl(dataItem.description),
    values: new FormControl(dataItem.values),
});

@Component({
    selector: 'label-list',
    templateUrl: './template.html',
    styleUrls: ['./style.scss']
})
export class LabelListComponent implements OnInit {
    location: string;
    public gridView: GridDataResult;
    labelData: any[] = [];
    _originalLabelData: any[] = [];
    editedRowIndex: number;
    formGroup: FormGroup;
    shouldHideActionMenu = false;
    @ViewChild('globalSearch', { static: false }) globalSearch: ElementRef;
    clickedRowItem: any;

    displayedColumns = [];


    readonly separatorKeysCodes: number[] = [ENTER, COMMA];

    private _unsubscribeAll: Subject<any> = new Subject();

    constructor(
        private _labelService: LabelService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
        this.gridView = {
            data: [],
            total: 0
        };
        this.displayedColumns = [
            {
                name: 'Name',
                field: 'name'
            },
            {
                name: 'Location',
                field: 'location'
            },
            {
                name: 'Description',
                field: 'description'
            },
            {
                name: 'Values',
                field: 'values'
            }
        ];
    }
    loadLabelData() {
        this._labelService.getLabels(this.location).pipe(first()).subscribe((data: any) => {
            this.labelData = [];
            data.items.map(item => {
                let values = item.spec.values.map((label: string, index: number) => (
                    {
                        id: index,
                        label: label
                    }
                ))
                this.labelData.push({
                    apiVersion: item.apiVersion,
                    kind: item.kind,
                    metadata: item.metadata,
                    name: item.metadata.name,
                    location: this.location,
                    description: item.spec.description,
                    values: values
                })
            })
            this._originalLabelData = _.cloneDeep(this.labelData);
            this.gridView = {
                data: this.labelData,
                total: this.labelData.length,
            };
            if (this.globalSearch.nativeElement.value && this.globalSearch.nativeElement.value.length > 0) {
                this.onFilter(this.globalSearch.nativeElement.value);
            }
        })
    }
    ngOnInit(): void {
        this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(param => {
            this.location = param["loc"];
            this.loadLabelData();
        });

        this.activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
            const itemId = params['id'];
        });
    }
    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }
    public onFilter(inputValue: string): void {
        const result: GridDataResult = process(this.labelData, {
            filter: {
                logic: 'or',
                filters: this.displayedColumns.map((col) => ({
                    field: col.field,
                    operator: 'contains',
                    value: inputValue
                })),
            }
        });

        this.gridView = {
            data: result.data,
            total: result.total
        };
    }
    onStateChange(event) {
        console.log("data change", event);
    }
    editHandler({ sender, rowIndex, dataItem }) {
        this.shouldHideActionMenu = true;
        this.closeEditor(sender);
        this.editedRowIndex = rowIndex;

        this.formGroup = createFormGroup({
            name: dataItem.name,
            location: dataItem.location,
            description: dataItem.description,
            values: dataItem.values
        });
        sender.editRow(rowIndex, this.formGroup);
    }
    addHandler({ sender }) {
        this.shouldHideActionMenu = true;
        this.closeEditor(sender);
        console.log("add...");
        this.formGroup = createFormGroup({
            name: '',
            location: this.location,
            description: '',
            values: []
        });
        sender.addRow(this.formGroup);

    }
    cancelHandler({ sender, rowIndex, dataItem, isNew }) {
        this.shouldHideActionMenu = false;
        if (!isNew) {
            //roll back to the original data if editing canceled
            this.labelData[this.editedRowIndex].values = _.cloneDeep(this._originalLabelData[this.editedRowIndex].values);
        }
        this.closeEditor(sender, rowIndex);
    }
    saveHandler({ sender, rowIndex, formGroup, dataItem, isNew }): void {
        console.log(formGroup);
        console.log(dataItem);
        this.shouldHideActionMenu = false;
        if (isNew) {
            const label: any = {
                apiVersion: formGroup.value.location + "/v1",
                kind: "WSLabel",
                metadata: {
                    name: formGroup.value.name
                },
                spec: {
                    description: formGroup.value.description,
                    values: formGroup.value.values.map(item => item.label)
                }
            };
            this._labelService.add(label, this.location).pipe(takeUntil(this._unsubscribeAll)).subscribe(
                () => {
                    sender.closeRow(rowIndex);
                    this.loadLabelData();
                },
                error => {
                    sender.closeRow(rowIndex);
                    alert('Failed to save.');

                },
            );
        }
        else {
            const label: any = {
                apiVersion: dataItem.apiVersion,
                kind: dataItem.kind,
                metadata: {
                    ...dataItem.metadata,
                    name: formGroup.value.name
                },
                spec: {
                    description: formGroup.value.description,
                    values: formGroup.value.values.map(item => item.label)
                }
            };
            this._labelService.update(label, this.location).pipe(takeUntil(this._unsubscribeAll)).subscribe(
                () => {
                    sender.closeRow(rowIndex);
                    this.loadLabelData();
                },
                error => {
                    sender.closeRow(rowIndex);
                    alert('Failed to save.');
                }
            );
        }
        sender.closeRow(rowIndex);

    }
    removeHandler({ dataItem }): void {
        this.shouldHideActionMenu = false;
        const id = dataItem.name;
        this._labelService.delete(id, this.location).pipe(takeUntil(this._unsubscribeAll)).subscribe(
            (data: any) => console.log('Deleted label ', id),
            error => alert('Failed to delete label ' + id)
        );
        this.loadLabelData();
    }
    remove(label: any, index: number): void {
        if (label !== undefined && index !== undefined && this.formGroup !== undefined) {
            console.log("removing...");
            this.labelData[this.editedRowIndex].values.splice(index, 1);
        }
    }
    add(event: MatChipInputEvent): void {
        console.log("add label is", event);
        const input = event.input;
        const value = event.value;

        // Add label
        if ((value || '').trim()) {
            let id = this.formGroup.value.values.length;
            this.formGroup.value.values.push({
                id: id,
                label: value.trim()
            })
        }

        // Reset the input value
        if (input) {
            input.value = '';
        }
    }
    onCellClick(event) {
        this.clickedRowItem = event.dataItem;
    }
    onDblClick() {
        if (this.clickedRowItem != undefined) {
            this.router.navigate(['/labels', this.clickedRowItem.metadata.name]);
        }
    }
}
