import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, ReplaySubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ILabel } from '../../typings/backendapi.gen';
type onDetailLoadCallback = (label?: ILabel) => void;
type onDetailLoadFailCallback = (label?: ILabel) => void;
@Injectable({
    providedIn: 'root',
})
export class LabelService {
    constructor(private http_: HttpClient) { }

    get<T = any>(location: string) {
        return this.http_.get(`apis/${location}/v1/wslabels`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    update(label: any, location: string): Observable<any> {
        const httpOptions = {
            method: 'PUT',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };

        return this.http_.put<any>(`apis/${location}/v1/wslabels/${label.metadata.name}`, label, httpOptions);
    }
    delete(id: number, location: string): Observable<{}> {
        return this.http_.delete(`apis/${location}/v1/wslabels/${id}`)
            .pipe();
    }
    add(label: any, location: string): Observable<any> {
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post<any>(`apis/${location}/v1/wslabels`, label, httpOptions);
    }
    getLabels(location: string) {
        return this.get(location);
    }
    getLabelByName(name: string, location: string){
        return this.http_.get(`apis/${location}/v1/wslabels/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }
}
