import {
  Component,
  Inject,
  ViewChild,
  Renderer2,
  OnInit,
  Input,
} from "@angular/core";
// import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {
  CompactType,
  GridsterConfig,
  GridType,
  GridsterPush,
  GridsterItem,
  GridsterItemComponent,
} from "angular-gridster2";
import { ItemType } from "app/main/shared/services/report-menu.enum.gen";
import { DialogContentBase, DialogRef } from "@progress/kendo-angular-dialog";
/**
 * @title Injecting data when opening a dialog
 */
@Component({
  selector: "label-table-dialog",
  templateUrl: "template.html",
  styleUrls: ["style.scss"],
})
export class LabelTableDialogComponent
  extends DialogContentBase
  implements OnInit {
  @Input() public data: any;
  public options: GridsterConfig;
  labelData: any[] = [];
  public ItemType = ItemType;
  public dialog: DialogRef;
  public listForPush = new Array<GridsterItemComponent>(1);
  public gridsterStyleHeight = 300;
  public items: GridsterItem[] = [
    {
      reportType: ItemType.LABELS,
      cols: 96,
      rows: 20,
      y: 0,
      x: 0,
      resizeEnabled: false,
      dragEnabled: false,
      initCallback: (value1, value2: GridsterItemComponent) =>
        this.initItem(value1, value2, 0),
    },
  ];
  constructor(private dlg: DialogRef, private _renderer: Renderer2) {
    super(dlg);

    this.options = {
      gridType: GridType.VerticalFixed,
      compactType: CompactType.None,
      margin: 5,
      minCols: 96,
      maxCols: 96,
      minRows: 96,
      maxRows: 1300,
      maxItemCols: 96,
      minItemCols: 1,
      maxItemRows: 1000,
      minItemRows: 1,
      maxItemArea: 500000,
      fixedRowHeight: 10,
      minItemArea: 1,
      mobileBreakpoint: 300,
      keepFixedHeightInMobile: false,
      scrollVertical: true,
      compactUp: false,
      draggable: {
        delayStart: 0,
        enabled: false,
        ignoreContentClass: "gridster-item-content",
        ignoreContent: true,
        dragHandleClass: "drag-handler",
        stop: ($event) => {},
      },
      resizable: {
        enabled: false,
      },
      swap: false,
      pushItems: true,
    };
  }
  ngOnInit() {}
  changeData(result: any) {
    console.log(result);
    this.labelData = result.data;
  }

  updateOptions() {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
  }

  initItem(item: GridsterItem, itemComponent: GridsterItemComponent, index) {
    this.listForPush[index] = itemComponent;
  }

  setHeightGridster(index, targetItem, tableHeight) {
    const previousRow: number = targetItem.rows;
    const rows = Math.ceil((tableHeight + 50) / 15);

    if (tableHeight > 10) {
      this.gridsterStyleHeight = tableHeight + 70;
      console.log(this.gridsterStyleHeight);
    }
    if (this.listForPush[index]) {
      this.pushItem(this.listForPush[index], previousRow, rows);
    }
  }

  pushItem(itemToPush, previousRow, currentRow) {
    const push = new GridsterPush(itemToPush); // init the service
    itemToPush.$item.rows = currentRow; // move/resize your item

    const listOfEnabledItems = this.items
      .filter((item) => item.dragEnabled)
      .map((item) => item.id);
    this.items.forEach((item) => {
      item.dragEnabled = true;
      item.resizeEnabled = true;
    });
    this.updateOptions();

    itemToPush.$item.rows = currentRow;

    if (push.pushItems(push.fromNorth) || push.pushItems(push.fromSouth)) {
      // push items from a direction
      push.checkPushBack(); // check for items can restore to original position
      push.setPushedItems(); // save the items pushed
      itemToPush.setSize();
      itemToPush.checkItemChanges(itemToPush.$item, itemToPush.item);
    } else {
      itemToPush.$item.rows = previousRow;
      push.restoreItems(); // restore to initial state the pushed items
    }
    this.items.forEach((record) => {
      const enabled = listOfEnabledItems.some((id) => record.id === id);
      record.dragEnabled = enabled;
      record.resizeEnabled = enabled;
    });
    this.updateOptions();

    push.destroy(); // destroy push instance
  }

  changeGridsterState(widget: any, general = true) {
    if (general) {
      widget.resizeEnabled = !widget.resizeEnabled;
      widget.dragEnabled = !widget.dragEnabled;
    }
    this.updateOptions();
  }
  onSaveBtnClicked() {
    this.dialog.close({ text: "Save" });
  }
  onCancelBtnClicked() {
    this.dialog.close({ text: "Cancel" });
  }
}
