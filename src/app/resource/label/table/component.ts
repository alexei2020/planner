
import { Component, OnInit, ViewChild, ElementRef, Input, Output,  EventEmitter, SimpleChanges, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LabelService } from '../services'
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { process, GroupDescriptor } from '@progress/kendo-data-query';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { Observable, BehaviorSubject, from, Subject } from 'rxjs';
import { delay, switchMap, map, tap, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'label-table',
    templateUrl: './template.html',
    styleUrls: ['./style.scss']
})
export class LabelTableComponent implements OnInit {
    @ViewChild('grid') private grid;
    @Input() pageSize = 10;
    @Input() searchText;
    @Input() resizeble: boolean;

    @Output() setRows: any = new EventEmitter();

    location: string;
    public gridView: GridDataResult;
    public groups: GroupDescriptor[] = [];
    public groupable = false;
    public skip = 0 as number;
    labelData: any[] = [];
    editedRowIndex: number;
    formGroup: FormGroup;
    labelCtrl = new FormControl();
    shouldHideActionMenu = false;
    previousSize: number;
    // @ViewChild('globalSearch', { static: false }) globalSearch: ElementRef;

    private _selectedLabel: any[];
    get selectedLabel(): any[]{
        return this._selectedLabel;
    }
    @Input('labels') set selectedLabel(data: any[]){
        this._selectedLabel = data;
    }
    @Output() dataChanged = new EventEmitter<any>();
    @ViewChild("list") list;

    clickedRowItem: any;

    displayedColumns = [];

    readonly separatorKeysCodes: number[] = [ENTER, COMMA];

    private _unsubscribeAll: Subject<any> = new Subject();

    constructor(
        private _labelService: LabelService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
        this.gridView = {
            data: [],
            total: 0
        };
        this.displayedColumns = [
            {
                name: 'Name',
                field: 'name'
            },
            {
                name: 'Values',
                field: 'values'
            }
        ];
    }
    loadLabelData() {
        this._labelService.getLabels(this.location).pipe(takeUntil(this._unsubscribeAll)).subscribe((data: any) => {
            this.labelData = [];
            data.items.map(item => {
                let values = item.spec.values.map((label: string, index: number) => (label));
                let selectedValues = this.selectedLabel.filter(label=>label.name === item.metadata.name)[0];
                this.labelData.push({
                    apiVersion: item.apiVersion,
                    kind: item.kind,
                    metadata: item.metadata,
                    name: item.metadata.name,
                    location: this.location,
                    description: item.spec.description,
                    values: values,
                    selectedValues: selectedValues ? selectedValues.values : [],
                    filteredValues: _.cloneDeep(values)
                })
                this.setHeight();
            })
            this.dataChanged.emit({type: "dataChange",data: this.labelData});
            this.gridView = {
                data: this.labelData,
                total: this.labelData.length,
            };
        })
    }
    
    ngOnChanges(changes: SimpleChanges): void {
        let update = false;
        let searchText = this.searchText;
        if ((changes.pageSize && changes.pageSize.currentValue) || (changes.resizeble && changes.resizeble.currentValue)) {
            update = true;
        }
        if (changes.searchText && this.labelData) {
            searchText = changes.searchText.currentValue;
            update = true;
        }
        if (update) {
            this.setHeight();
            this.onFilter(searchText);
        }
    }

    ngOnInit(): void {
        this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(param => {
            this.location = param["loc"];
            this.loadLabelData();
        })
        this.activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
            const itemId = params['id'];
        });
    }

    setHeight() {
        setTimeout(() => {
            if (this.grid && this.grid.wrapper) {
                let size = 0;
				if (this.grid.wrapper) {
					const header = this.grid.wrapper.nativeElement.querySelector('.k-grouping-header'); 
					const grid = this.grid.wrapper.nativeElement.querySelector('.k-grid-aria-root');
					const pager = this.grid.wrapper.nativeElement.querySelector('.k-pager-wrap');
					if (header) {
						size += header.offsetHeight;
					}
					if (grid) {
						size += grid.offsetHeight;
					}
					if (pager) {
						size += pager.offsetHeight;
					}
				}

                if (size == 0) {
                    size = this.grid.wrapper.nativeElement.offsetHeight;
                }

                this.previousSize = size;

                this.setRows.emit(size);
            }
        });
    }

    public onExportPDFHandler() {
        this.grid.saveAsPDF();
    }

    public onExportExcelHandler() {
        this.grid.saveAsExcel();
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    public onGroupable() {
        this.groupable = !this.groupable;
        this.groups = [];
        this.loadLabelData();
    }

    public onFilter(inputValue: string): void {
        if (!this.labelData) {
            return;
        }
        const result: GridDataResult = process(this.labelData, {
            filter: inputValue ? {
                logic: 'or',
                filters: this.displayedColumns.map((col) => ({
                    field: col.field,
                    operator: 'contains',
                    value: inputValue
                })),
            } : null,
            group: this.groups,
        });

        this.gridView = {
            data: result.data,
            total: result.total
        };
    }

    public pageChange(event: PageChangeEvent): void {
        this.skip = event.skip;
        this.pageSize = event.take;
        this.onFilter(this.searchText);
        this.setHeight();
    }

    public onCellClick(event: any) {
        this.clickedRowItem = event.dataItem;
    }

    onStateChange(event) {
        console.log("data change", event);
    }
   
    valueChange(value: any): void {
        this.dataChanged.emit({type: "dataChange",data: this.labelData});
    }

    private _filter(filter: any, rowIndex: number) {
        this.labelData[rowIndex].filteredValues = this.labelData[rowIndex].values.filter((s) => s.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
    }

    @HostListener('window:resize', ['$event'])
    onResize() {
        this.setHeight();
    }
}
