
import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';
import { LabelListPageComponent } from '../../pages/label/list/component'
import { LabelDetailPageComponent } from '../../pages/label/detail/component'
const LABEL_List_ROUTE: Route = {
    path: '',
    component: LabelListPageComponent
};
const LABEL_DETAIL_ROUTE: Route = {
    path: ':resourceName',
    component: LabelDetailPageComponent,
    data: {
      breadcrumb: '{{ resourceName }}',
      parent: LABEL_List_ROUTE,
    },
  };
@NgModule({
    imports: [RouterModule.forChild([LABEL_List_ROUTE, LABEL_DETAIL_ROUTE])],
    exports: [RouterModule],
})
export class LabelRoutingModule { }


