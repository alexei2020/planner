

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { EndPoint } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onEndPointDetailLoadCallback = (instance?: EndPoint) => void;
type onEndPointDetailLoadFailCallback = (instance?: EndPoint) => void;
@Injectable({
    providedIn: 'root'
})
export class EndPointDetailService {
    public onEndPointUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: EndPoint = {} as EndPoint;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('EndPoint');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/endpoints?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getEndPoints(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/endpoints?${filterString}`, body, httpOptions);
     }

    getEndPointByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/endpoints/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/endpoints/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<EndPoint>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/endpoints/${resourceName}`);
	 } else {
         	return this.http_.get<EndPoint>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/endpoints/${resourceName}`);
	 }
     }

//     load(onLoad?: onEndPointDetailLoadCallback, onFail?: onEndPointDetailLoadFailCallback): void {
//         this.http_.get<EndPoint>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onEndPointUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onEndPointUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(endpoint: EndPoint, _location: string): Observable<EndPoint> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<EndPoint>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/endpoints/`+endpoint.metadata.name, endpoint, httpOptions);
      }



    add(endpoint: EndPoint, _location: string): Observable<EndPoint> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<EndPoint>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/endpoints/`, endpoint, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/endpoints/${id}`);
    }

    getInstance(): EndPoint {
        return this.instance_;
    }
}
