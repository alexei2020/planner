

// tslint:disable
/*
Copyright (c) 2020 WaveSurf Systems Inc. All rights reserved.
*/
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { ReportsPageComponent } from '../../pages/reports/list/component';
import { ReportsDetailPageComponent } from 'app/pages/reports/detail/component';
import { ReportChartViewComponent } from 'app/pages/reports/chart/component';

const LIST_ROUTE: Route = {
  path: '',
  component: ReportsPageComponent,
  data: {
    breadcrumb: '',
    link: ['', 'reports'],
  },
};


const PREVIEW_CHART = {
  path: 'chart/:dashboard/:resourceName',
  component: ReportChartViewComponent,
  data: {
      breadcrumb: 'Details :resourceName',
      parent: LIST_ROUTE,
  },
}

const DETAIL_ROUTE: Route = {
  path: ':pageName/:resourceName',
  component: ReportsDetailPageComponent,
  data: {
    breadcrumb: 'Details :resourceName',
    parent: LIST_ROUTE,
  },
};

const CHART_PREVIEW_ROUTE: Route = {
  path: 'runtime-chart',
  loadChildren: () => import('../runtime-chart-view-shared/runtime-chart-view-shared.module').then(m => m.RuntimeChartViewSharedModule),
  data: {
    breadcrumb: 'Chart Details',
    parent: LIST_ROUTE,
  },
};

@NgModule({
  imports: [RouterModule.forChild([LIST_ROUTE, PREVIEW_CHART, CHART_PREVIEW_ROUTE, DETAIL_ROUTE])],
  exports: [RouterModule],
})
export class ReportsRoutingModule { }


