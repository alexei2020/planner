

// tslint:disable
/*
Copyright (c) 2020 WaveSurf Systems Inc. All rights reserved.
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { GridModule, PDFModule, ExcelModule, BodyModule, SharedModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { EditorModule } from '@progress/kendo-angular-editor';
import { UploadModule } from '@progress/kendo-angular-upload';
import { DialogsModule } from '@progress/kendo-angular-dialog';

import 'hammerjs';

import { GridsterModule } from 'angular-gridster2';

import { FlexLayoutModule } from '@angular/flex-layout';


import { ReportsPageComponent } from '../../pages/reports/list/component';
import { InitEditorDirective } from '../../pages/reports/list/kendo-editor-upload-files/initEditor.directive';
import { SafeHtmlPipe } from '../../pages/reports/list/safe-html.pipe';
import { CardImageComponent } from '../../pages/reports/list/card-image.component';
import { DialogComponent } from '../../pages/reports/list/kendo-editor-upload-files/upload-dialog.component';
import { DialogLineComponent } from '../../pages/reports/list/report-line/report-line.component';
import { UploadComponent } from '../../pages/reports/list/kendo-editor-upload-files/upload.component';
import { ReportsRoutingModule } from './routing';
import { ComponentsModule } from '../../components/module';
import { ReportsDetailPageComponent } from 'app/pages/reports/detail/component';
import { WidgetModule } from 'app/main/widget/widget.module';
import { ReportChartViewComponent } from 'app/pages/reports/chart/component';
import { OverlayModule } from '@angular/cdk/overlay';
// import { AdSharedModule } from '../../modules/ad-shared'
// import { AdmitdeviceSharedModule } from '../../modules/admitdevice-shared'
// import { CatalogitemSharedModule } from '../../modules/catalogitem-shared'
// import { ChartquerySharedModule } from '../../modules/chartquery-shared'
// import { CredentialSharedModule } from '../../modules/credential-shared'
// import { DashboardSharedModule } from '../../modules/dashboard-shared'
// import { DashboardquerySharedModule } from '../../modules/dashboardquery-shared'
import { FuseDirectivesModule } from '@fuse/directives/directives';
// import { DevicecredentialSharedModule } from '../../modules/devicecredential-shared'
// import { DeviceuserSharedModule } from '../../modules/deviceuser-shared'
// import { DeviceuserroleSharedModule } from '../../modules/deviceuserrole-shared'
// import { DiscovereddeviceSharedModule } from '../../modules/discovereddevice-shared'
// import { DiscoveredlinkSharedModule } from '../../modules/discoveredlink-shared'
// import { EndpointsetSharedModule } from '../../modules/endpointset-shared'
// import { EthernetSharedModule } from '../../modules/ethernet-shared'
// import { FailureinjectSharedModule } from '../../modules/failureinject-shared'
// import { FanSharedModule } from '../../modules/fan-shared'
// import { FloorspaceSharedModule } from '../../modules/floorspace-shared'
// import { FwimgSharedModule } from '../../modules/fwimg-shared'
// import { FwmaintgrpSharedModule } from '../../modules/fwmaintgrp-shared'
// import { GeoSharedModule } from '../../modules/geo-shared'
// import { HealthSharedModule } from '../../modules/health-shared'
// import { HeartbeatSharedModule } from '../../modules/heartbeat-shared'
// import { IfinterfaceSharedModule } from '../../modules/ifinterface-shared'
// import { InfrapoolSharedModule } from '../../modules/infrapool-shared'
// import { Internalroutev4SharedModule } from '../../modules/internalroutev4-shared'
// import { Internalroutev6SharedModule } from '../../modules/internalroutev6-shared'
// import { IntfSharedModule } from '../../modules/intf-shared'
// import { Ipv4SharedModule } from '../../modules/ipv4-shared'
// import { Ipv6SharedModule } from '../../modules/ipv6-shared'
// import { L2accesssubnetSharedModule } from '../../modules/l2accesssubnet-shared'
// import { L2distsubnetSharedModule } from '../../modules/l2distsubnet-shared'
// import { LdapSharedModule } from '../../modules/ldap-shared'
// import { LinkSharedModule } from '../../modules/link-shared'
// import { MemlogSharedModule } from '../../modules/memlog-shared'
// import { MgmtintfSharedModule } from '../../modules/mgmtintf-shared'
// import { NacprofileSharedModule } from '../../modules/nacprofile-shared'
// import { NetworkSharedModule } from '../../modules/network-shared'
// import { NetworkprofileSharedModule } from '../../modules/networkprofile-shared'
// import { NetworkresolutioncacheSharedModule } from '../../modules/networkresolutioncache-shared'
// import { PlatformcomponentSharedModule } from '../../modules/platformcomponent-shared'
// import { PlatformcpuSharedModule } from '../../modules/platformcpu-shared'
// import { PlatformfanSharedModule } from '../../modules/platformfan-shared'
// import { PlatformlinecardSharedModule } from '../../modules/platformlinecard-shared'
// import { PlatformpsuSharedModule } from '../../modules/platformpsu-shared'
// import { PolicySharedModule } from '../../modules/policy-shared'
// import { PolicyresolvedSharedModule } from '../../modules/policyresolved-shared'
// import { PolicyruleSharedModule } from '../../modules/policyrule-shared'
// import { PolicysyncdstateSharedModule } from '../../modules/policysyncdstate-shared'
// import { PortalroleSharedModule } from '../../modules/portalrole-shared'
// import { PortalrolebindingSharedModule } from '../../modules/portalrolebinding-shared'
// import { PortaltenantbindingSharedModule } from '../../modules/portaltenantbinding-shared'
// import { PortaltenantbindingcacheSharedModule } from '../../modules/portaltenantbindingcache-shared'
// import { PortaluserSharedModule } from '../../modules/portaluser-shared'
// import { PortalusersignupSharedModule } from '../../modules/portalusersignup-shared'
// import { PowersupplySharedModule } from '../../modules/powersupply-shared'
// import { ProxyconfigSharedModule } from '../../modules/proxyconfig-shared'
// import { RadioSharedModule } from '../../modules/radio-shared'
// import { RadiusclientSharedModule } from '../../modules/radiusclient-shared'
// import { RegistryinfoSharedModule } from '../../modules/registryinfo-shared'
// import { ReportSharedModule } from '../../modules/report-shared'
// import { SchedSharedModule } from '../../modules/sched-shared'
// import { ServiceidentitySharedModule } from '../../modules/serviceidentity-shared'
// import { ServiceinfoSharedModule } from '../../modules/serviceinfo-shared'
// import { SsidSharedModule } from '../../modules/ssid-shared'
// import { StaticrouteSharedModule } from '../../modules/staticroute-shared'
// import { SubintfSharedModule } from '../../modules/subintf-shared'
// import { SubnetSharedModule } from '../../modules/subnet-shared'
// import { SubtenantSharedModule } from '../../modules/subtenant-shared'
// import { SwitchedvlanSharedModule } from '../../modules/switchedvlan-shared'
// import { SystemSharedModule } from '../../modules/system-shared'
// import { TacacsdstateSharedModule } from '../../modules/tacacsdstate-shared'
// import { TaskSharedModule } from '../../modules/task-shared'
// import { TemplateSharedModule } from '../../modules/template-shared'
// import { TenantSharedModule } from '../../modules/tenant-shared'
// import { TenantconfigSharedModule } from '../../modules/tenantconfig-shared'
// import { TenantprofileSharedModule } from '../../modules/tenantprofile-shared'
// import { TenantsignupSharedModule } from '../../modules/tenantsignup-shared'
// import { TunnelconfigSharedModule } from '../../modules/tunnelconfig-shared'
// import { UicatalogSharedModule } from '../../modules/uicatalog-shared'
// import { UsersettingSharedModule } from '../../modules/usersetting-shared'
// import { WalljackSharedModule } from '../../modules/walljack-shared'
// import { WsauditSharedModule } from '../../modules/wsaudit-shared'
// import { WsdeviceidSharedModule } from '../../modules/wsdeviceid-shared'
// import { EndpointauxSharedModule } from '../../modules/endpointaux-shared'
// import { WseventSharedModule } from '../../modules/wsevent-shared'
// import { WsfaultSharedModule } from '../../modules/wsfault-shared'
// import { fileSharedModule } from '../../modules/file-shared'
// import { WsippoolSharedModule } from '../../modules/wsippool-shared'
// import { WsquerySharedModule } from '../../modules/wsquery-shared'
// import { WssecretSharedModule } from '../../modules/wssecret-shared'
// import { UserSharedModule } from '../../modules/user-shared'
// import { UsergroupSharedModule } from '../../modules/usergroup-shared'
// import { WsvlanpoolSharedModule } from '../../modules/wsvlanpool-shared'

const SharedVisualModules = [
    // CandidatedeviceSharedModule,
    // ClusterSharedModule,
    // DeviceUserRuleSharedModule,
    // EndpointSharedModule,
    // WslabelSharedModule,
    // AdSharedModule,
    // AdmitdeviceSharedModule,
    // CatalogitemSharedModule,
    // ChartquerySharedModule,
    // CredentialSharedModule,
    // DashboardSharedModule,
    // DashboardquerySharedModule,
    // DeviceSharedModule,
    // DevicecredentialSharedModule,
    // DeviceuserSharedModule,
    // DeviceuserroleSharedModule,
    // DiscovereddeviceSharedModule,
    // DiscoveredlinkSharedModule,
    // EndpointsetSharedModule,
    // EthernetSharedModule,
    // FailureinjectSharedModule,
    // FanSharedModule,
    // FloorspaceSharedModule,
    // FwimgSharedModule,
    // FwmaintgrpSharedModule,
    // GeoSharedModule,
    // HealthSharedModule,
    // HeartbeatSharedModule,
    // IfinterfaceSharedModule,
    // InfrapoolSharedModule,
    // Internalroutev4SharedModule,
    // Internalroutev6SharedModule,
    // IntfSharedModule,
    // Ipv4SharedModule,
    // Ipv6SharedModule,
    // L2accesssubnetSharedModule,
    // L2distsubnetSharedModule,
    // LdapSharedModule,
    // LinkSharedModule,
    // MemlogSharedModule,
    // MgmtintfSharedModule,
    // NacprofileSharedModule,
    // NetworkSharedModule,
    // NetworkprofileSharedModule,
    // NetworkresolutioncacheSharedModule,
    // PlatformcomponentSharedModule,
    // PlatformcpuSharedModule,
    // PlatformfanSharedModule,
    // PlatformlinecardSharedModule,
    // PlatformpsuSharedModule,
    // PolicySharedModule,
    // PolicyresolvedSharedModule,
    // PolicyruleSharedModule,
    // PolicysyncdstateSharedModule,
    // PortalroleSharedModule,
    // PortalrolebindingSharedModule,
    // PortaltenantbindingSharedModule,
    // PortaltenantbindingcacheSharedModule,
    // PortaluserSharedModule,
    // PortalusersignupSharedModule,
    // PowersupplySharedModule,
    // ProxyconfigSharedModule,
    // RadioSharedModule,
    // RadiusclientSharedModule,
    // RegistryinfoSharedModule,
    // ReportSharedModule,
    // SchedSharedModule,
    // ServiceidentitySharedModule,
    // ServiceinfoSharedModule,
    // SsidSharedModule,
    // StaticrouteSharedModule,
    // SubintfSharedModule,
    // SubnetSharedModule,
    // SubtenantSharedModule,
    // SwitchedvlanSharedModule,
    // SystemSharedModule,
    // TacacsdstateSharedModule,
    // TaskSharedModule,
    // TemplateSharedModule,
    // TenantSharedModule,
    // TenantconfigSharedModule,
    // TenantprofileSharedModule,
    // TenantsignupSharedModule,
    // TunnelconfigSharedModule,
    // UicatalogSharedModule,
    // UsersettingSharedModule,
    // WalljackSharedModule,
    // WsauditSharedModule,
    // WsdeviceidSharedModule,
    // EndpointauxSharedModule,
    // WseventSharedModule,
    // WsfaultSharedModule,
    // fileSharedModule,
    // WsippoolSharedModule,
    // WsquerySharedModule,
    // WssecretSharedModule,
    // UserSharedModule,
    // UsergroupSharedModule,
    // WsvlanpoolSharedModule,
];

@NgModule({
  imports: [
    ReactiveFormsModule,
    ReportsRoutingModule,
    PDFModule,
    ExcelModule,
    InputsModule,
    GridModule,
    PopupModule,
    LayoutModule,
    ChartsModule,
    MatMenuModule,
    BodyModule,
    MatButtonModule,
    MatIconModule,
    SharedModule,
    CommonModule,
    GridsterModule,
    ComponentsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    FlexLayoutModule,
    FormsModule,
    MatCheckboxModule,
    HttpClientModule,
    WidgetModule,
    SharedVisualModules,
    FuseDirectivesModule,
    EditorModule,
    UploadModule,
    DialogsModule,
    OverlayModule,
  ],
  declarations: [
    ReportsPageComponent,
    CardImageComponent,
    ReportsDetailPageComponent,
    ReportChartViewComponent,
    DialogComponent,
    UploadComponent,
    DialogLineComponent,
    InitEditorDirective,
    SafeHtmlPipe,
  ],
  entryComponents: [ReportsPageComponent],
  bootstrap: [ReportsPageComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DialogComponent,
      multi: true
    },
  ],
})
export class ReportsModule { }

