

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { DeviceUI } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onDeviceUIDetailLoadCallback = (instance?: DeviceUI) => void;
type onDeviceUIDetailLoadFailCallback = (instance?: DeviceUI) => void;
@Injectable({
    providedIn: 'root'
})
export class DeviceUIDetailService {
    public onDeviceUIUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: DeviceUI = {} as DeviceUI;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('DeviceUI');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/deviceuis?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getDeviceUIs(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/deviceuis?${filterString}`, body, httpOptions);
     }

    getDeviceUIByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/deviceuis/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/deviceuis/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<DeviceUI>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/deviceuis/${resourceName}`);
	 } else {
         	return this.http_.get<DeviceUI>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/deviceuis/${resourceName}`);
	 }
     }

//     load(onLoad?: onDeviceUIDetailLoadCallback, onFail?: onDeviceUIDetailLoadFailCallback): void {
//         this.http_.get<DeviceUI>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onDeviceUIUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onDeviceUIUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(deviceui: DeviceUI, _location: string): Observable<DeviceUI> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<DeviceUI>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/deviceuis/`+deviceui.metadata.name, deviceui, httpOptions);
      }



    add(deviceui: DeviceUI, _location: string): Observable<DeviceUI> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<DeviceUI>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/deviceuis/`, deviceui, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/deviceuis/${id}`);
    }

    getInstance(): DeviceUI {
        return this.instance_;
    }
}
