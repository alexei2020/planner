
// tslint:disable
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { RuntimeChartViewSharedComponent } from './components/runtime-chart-view-shared.component';

const GLOBAL_ROUTE: Route = {
  path: '',
  component: RuntimeChartViewSharedComponent,
};
const CUSTOM_VIEW_ROUTE: Route = {
  path: 'custom',
  component: RuntimeChartViewSharedComponent,
};

const DASHBOARD_VIEW_ROUTE: Route = {
  path: ':dashboard/:chartId',
  component: RuntimeChartViewSharedComponent,
};

@NgModule({
  imports: [RouterModule.forChild([GLOBAL_ROUTE, CUSTOM_VIEW_ROUTE, DASHBOARD_VIEW_ROUTE])],
  exports: [RouterModule],
})
export class RuntimeChartViewSharedRoutingModule { }


