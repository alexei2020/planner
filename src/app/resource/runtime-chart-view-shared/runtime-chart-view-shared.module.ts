
// tslint:disable

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { GridModule, PDFModule, ExcelModule, BodyModule, SharedModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { LayoutModule } from '@progress/kendo-angular-layout';

import 'hammerjs';

import { GridsterModule } from 'angular-gridster2';

import { FlexLayoutModule } from '@angular/flex-layout';

import { RuntimeChartViewSharedRoutingModule } from './routing';
import { WidgetModule } from 'app/main/widget/widget.module';
import { RuntimeChartViewSharedComponent } from './components/runtime-chart-view-shared.component';
import { FuseDirectivesModule } from '@fuse/directives/directives';


@NgModule({
  imports: [
    ReactiveFormsModule, 
    RuntimeChartViewSharedRoutingModule,
    PDFModule, 
    ExcelModule, 
    InputsModule,
    GridModule, 
    PopupModule, 
    LayoutModule, 
    ChartsModule,
    MatMenuModule,
    BodyModule,
    MatButtonModule,
    MatIconModule,
    SharedModule,
    CommonModule, 
    GridsterModule, 
    MatCardModule,
    MatFormFieldModule, 
    MatInputModule, 
    FlexLayoutModule, 
    FormsModule, 
    MatCheckboxModule,
    HttpClientModule,
    WidgetModule,
    FuseDirectivesModule
  ],
  declarations: [
    RuntimeChartViewSharedComponent
  ],
})
export class RuntimeChartViewSharedModule { }

