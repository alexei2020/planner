import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { Subject, Observable, combineLatest } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';

import { GridsterConfig, GridType, CompactType, GridsterItem } from 'angular-gridster2';

import { WidgetDialogComponent } from '../../../main/shared/widget-dialog/widget-dialog.component';
import { LayoutService } from '../../../main/shared/services/layout.service';
import { BreadcrumbService } from '../../../main/shared/breadcrumb-bar.service';
import { PageType } from '../../../main/shared/breadcrumb-bar.service.gen';
import { TimerService } from '../../../main/shared/services';
import { Widget } from '../../../main/widget';
import { HttpClient } from '@angular/common/http';
import {JsonDialogComponent} from '../../../main/shared/dialogs/json-dialog/json-dialog.component';
import {DialogCloseResult, DialogService} from '@progress/kendo-angular-dialog';

@Component({
    selector: 'runtime-chart-view-shared',
    templateUrl: './runtime-chart-view-shared.component.html',
    styleUrls: ['./runtime-chart-view-shared.component.scss']
})
export class RuntimeChartViewSharedComponent implements OnInit, OnDestroy {
    public filter: string;
    public resetDataSource: any;
    public timer$: Observable<any>;
    public refreshTimer$: Observable<any>;
    public interval$: Observable<any>;
    public options: GridsterConfig;
    public item1: GridsterItem;
    public item2: GridsterItem;

    private _unsubscribeAll: Subject<any> = new Subject();
    timer: any;

    listOfCharts: any[];
    private objName: string;
    private namespace: string;

    constructor(
        private location: Location,
        private layoutService: LayoutService,
        private activatedRoute: ActivatedRoute,
        private httpClient: HttpClient,
        private _dialogService: DialogService,
        private _breadcrumbService: BreadcrumbService,
    ) {
        this.options = {
            gridType: GridType.VerticalFixed,
            compactType: CompactType.None,
            margin: 5,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1000,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            minItemArea: 1,
            fixedRowHeight: 10,
            resizable: {
                enabled: false
            },
            draggable: {
                enabled: false
            }
        };

        this.item1 = { cols: 96, rows: 20, y: 0, x: 0 };
        this.item2 = { cols: 96, rows: 46, y: 0, x: 0 };

        this.activatedRoute
            .queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((params) => {
                if (params && params['chartLink']) {
                    this.loadRuntimeChart(params['chartLink']);
                }
            });
        this._breadcrumbService.currentPage.next(PageType.DEFAULT);
        this._breadcrumbService.currentPageInfo
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((cpi) => {
            this.objName = cpi?.name;
            this.namespace = cpi?.namespace;
          });
    }

    ngOnInit(): void { }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    public onFilter(filter: string): void {
        this.filter = filter;
    }

    public jsonWidget(widget: Widget): void {
        this.layoutService.setSelectedChartObs(widget.id);
        // const dialogRef = this._dialog.open(JsonDialogComponent, {
        //     width: '750px',
        //     data: options,
        //     panelClass: 'json-panel'
        // });
        // dialogRef.afterClosed().subscribe((res) => {
        //     if (res) {
        //         widget['chartOptions'] = res;
        //         this.layoutService.editItem(widget);
        //     }
        // });
        const dialogRef = this._dialogService.open({
            title: 'Chart JSON',

            // Show component
            content: JsonDialogComponent,
            width: '80%',
            height: '400px',
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.dialog = dialogRef;
        dialogInstance.widget = widget;

        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
            console.log(result);
            if (result instanceof DialogCloseResult) {

            } else {
                // create widget object and emit it
                const widget = result as unknown as Widget;
                this.layoutService.editItem(widget);
            }
        });
    }
    public viewWidget(): void {
        this.location.back();
        // this.router.navigate([ 'dashboard' ]);
    }

    public filterChanged(event):void  {
        console.log(event);
        this.filter = event;
    }

    public updateOptions(): void {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    changeGridsterState(widget: any, general = true): void {
        if (general) {
            widget.resizeEnabled = !widget.resizeEnabled;
            widget.dragEnabled = !widget.dragEnabled;
        }
        this.updateOptions();
    }
    
    setHeightGridster(tableHeight, targetItem, tableHeaderHeight, otherItems): void {
        const previousRow = targetItem.rows;
        targetItem.rows = Math.ceil((tableHeight + tableHeaderHeight) / 15);
        this.updateOptions();
        if (otherItems) {
            otherItems = otherItems.map((item) => {
                if (previousRow < targetItem.rows) {
                    if (item.y > targetItem.y) {
                        item.y = item.y + (targetItem.rows - previousRow);
                    }
                } else {
                    if (item.y > targetItem.y) {
                        item.y = item.y - (previousRow - targetItem.rows);
                    }
                }
                return item;
            });
        }
        this.updateOptions();
    }

    private loadRuntimeChart(link): void {
        this.httpClient.get<any>(link).pipe(
            map(res => ({
                ...res, ...{ id: uuid(), dragEnabled: false, resizeEnabled: false, x: 0, y: 0, rows: 20, cols: 48 }
            })),
            takeUntil(this._unsubscribeAll)).subscribe((response: any[]) => {
                this.listOfCharts = [{ ...response, cols: 96, rows: 25, y: 0, x: 0 }];
            });
    }
}
