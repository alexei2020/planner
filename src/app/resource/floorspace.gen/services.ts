

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { FloorSpace } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onFloorSpaceDetailLoadCallback = (instance?: FloorSpace) => void;
type onFloorSpaceDetailLoadFailCallback = (instance?: FloorSpace) => void;
@Injectable({
    providedIn: 'root'
})
export class FloorSpaceDetailService {
    public onFloorSpaceUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: FloorSpace = {} as FloorSpace;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('FloorSpace');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/floorspaces?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getFloorSpaces(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/floorspaces?${filterString}`, body, httpOptions);
     }

    getFloorSpaceByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/floorspaces/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/floorspaces/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<FloorSpace>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/floorspaces/${resourceName}`);
	 } else {
         	return this.http_.get<FloorSpace>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/floorspaces/${resourceName}`);
	 }
     }

//     load(onLoad?: onFloorSpaceDetailLoadCallback, onFail?: onFloorSpaceDetailLoadFailCallback): void {
//         this.http_.get<FloorSpace>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onFloorSpaceUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onFloorSpaceUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(floorspace: FloorSpace, _location: string): Observable<FloorSpace> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<FloorSpace>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/floorspaces/`+floorspace.metadata.name, floorspace, httpOptions);
      }



    add(floorspace: FloorSpace, _location: string): Observable<FloorSpace> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<FloorSpace>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/floorspaces/`, floorspace, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/floorspaces/${id}`);
    }

    getInstance(): FloorSpace {
        return this.instance_;
    }
}
