

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { IfInterface } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onIfInterfaceDetailLoadCallback = (instance?: IfInterface) => void;
type onIfInterfaceDetailLoadFailCallback = (instance?: IfInterface) => void;
@Injectable({
    providedIn: 'root'
})
export class IfInterfaceDetailService {
    public onIfInterfaceUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: IfInterface = {} as IfInterface;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('IfInterface');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/ifinterfaces?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getIfInterfaces(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/ifinterfaces?${filterString}`, body, httpOptions);
     }

    getIfInterfaceByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/ifinterfaces/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/ifinterfaces/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<IfInterface>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/ifinterfaces/${resourceName}`);
	 } else {
         	return this.http_.get<IfInterface>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/ifinterfaces/${resourceName}`);
	 }
     }

//     load(onLoad?: onIfInterfaceDetailLoadCallback, onFail?: onIfInterfaceDetailLoadFailCallback): void {
//         this.http_.get<IfInterface>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onIfInterfaceUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onIfInterfaceUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(ifinterface: IfInterface, _location: string): Observable<IfInterface> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<IfInterface>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/ifinterfaces/`+ifinterface.metadata.name, ifinterface, httpOptions);
      }



    add(ifinterface: IfInterface, _location: string): Observable<IfInterface> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<IfInterface>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/ifinterfaces/`, ifinterface, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/ifinterfaces/${id}`);
    }

    getInstance(): IfInterface {
        return this.instance_;
    }
}
