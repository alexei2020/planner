

// tslint:disable


import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Observable, ReplaySubject, throwError} from 'rxjs';
import { environment } from '../../../environments/environment';
import { kindToUrlClass } from '../../navigation/objrefnav.gen';
import { Location } from '../../typings/backendapi.gen';
import { map, catchError } from 'rxjs/operators';
type onLocationDetailLoadCallback = (instance?: Location) => void;
type onLocationDetailLoadFailCallback = (instance?: Location) => void;
@Injectable({
    providedIn: 'root'
})
export class LocationDetailService {
    public onLocationUpdate = new ReplaySubject<void>();
    public _location = 'ws.io';
    private endpoint_ = '';
    private instance_: Location = {} as Location;
    private isInitialized_ = false;
    private urlClass = kindToUrlClass.get('Location');

    location: string;
    constructor(
        private readonly http_: HttpClient,
    ) {}

    get<T = any>(location: string) {
	var filterString = "wsSelector=includeSubtree=true";
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/locations?${filterString}`).pipe(
            map(this.handleSuccess),
        ) as Observable<T>;
    }

    protected handleSuccess(res: any) {
        return res;
    }

    protected handleError(error: any) {
        return throwError(error);
    }

    getLocations(location: string, body: any) {
	var filterString = "wsSelector=includeSubtree=true,rawResponse=true";
        const httpOptions = {
            method: 'POST',
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };
        return this.http_.post(`${environment.apiUrl}/history/${location}/v1/locations?${filterString}`, body, httpOptions);
     }

    getLocationByName(name: string, location: string){
        return this.http_.get(`${environment.apiUrl}/${this.urlClass}/${location}/v1/locations/${name}`).pipe(
            map(this.handleSuccess),
        ) as Observable<any>;
    }

//     init(resourceName: any, loc?: string): void {
//         // remove once fixed
//         loc = "ws.io";
//         // end of "remove once fixed"
//
//         this.location = loc;
//         this.endpoint_ = `${environment.apiUrl}/${this.urlClass}/${this.location}/v1/locations/` + resourceName;
//         this.load();
//     }

    isInitialized(): boolean {
        return this.isInitialized_;
    }

    load(resourceName, namespace: string, _location: string) {
         if (_location) {
             this._location = _location;
         }
	 if (namespace) {
         	return this.http_.get<Location>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/namespaces/${namespace}/locations/${resourceName}`);
	 } else {
         	return this.http_.get<Location>(`${environment.apiUrl}/${this.urlClass}/${this._location}/v1/locations/${resourceName}`);
	 }
     }

//     load(onLoad?: onLocationDetailLoadCallback, onFail?: onLocationDetailLoadFailCallback): void {
//         this.http_.get<Location>(this.endpoint_)
//             .toPromise()
//             .then(
//                 instance => {
//                     this.instance_ = instance;
//                     this.isInitialized_ = true;
//                     this.onLocationUpdate.next();
//                     if (onLoad) onLoad(instance);
//                 },
//                 err => {
//                     this.isInitialized_ = false;
//                     this.onLocationUpdate.next();
//                     if (onFail) onFail(err);
//                 },
//             );
//     }

    update(location: Location, _location: string): Observable<Location> {
          const httpOptions = {
              method: 'PATCH',
              headers: new HttpHeaders({
                  'Content-Type': 'application/merge-patch+json',
              }),
          };
          return this.http_.patch<Location>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/locations/`+location.metadata.name, location, httpOptions);
      }



    add(location: Location, _location: string): Observable<Location> {
         const httpOptions = {
             method: 'POST',
             headers: new HttpHeaders({
                 'Content-Type': 'application/json',
             }),
         };
         return this.http_.post<Location>(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/locations/`, location, httpOptions);
     }

    delete(id: string, _location: string): Observable<{}> {
             return this.http_.delete(`${environment.apiUrl}/${this.urlClass}/${_location ? _location : "ws.io"}/v1/locations/${id}`);
    }

    getInstance(): Location {
        return this.instance_;
    }
}
