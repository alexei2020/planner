import {defaultVer, strValOrEmpty, verConstFactory} from './objrefnav';

class Zoo {
    val: string;
}
class Boo {
    zoo: Zoo;
}
class Foo {
    boo: Boo;
}

export function test_strValOrEmpty(): void {
    const foo = new Foo();
    foo.boo = new Boo();
    foo.boo.zoo = new Zoo();
    foo.boo.zoo.val = 'My Val';

    console.log(chainedValueAccessor(foo, 'boo.zoo.val', defaultVer()));
    console.log(chainedValueAccessor(foo, 'boo.invalid', verConstFactory('INVALID!!!')));
}
