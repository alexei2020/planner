import {FuseNavigationItem} from '@fuse/types';

export const navigation: FuseNavigationItem[] = [
    {
        id: 'Dashboard',
        text: 'Dashboard',
        type: 'collapsable',
        icon: 'dashboard',
        items: [
            {
               id: 'Location',
               text: 'Location',
               type: 'item',
               url: '/location',
            },
            {
               id: 'Inventory',
               text: 'Inventory',
               type: 'item',
               url: '/dashboard',
            },
            {
                id: 'Top10',
                text: 'Top10',
                type: 'item',
                url: '/dashboard?layout=top10',
            },
        ],

    },
    {
        id: 'locations',
        text: 'Location',
        type: 'collapsable',
        icon: '3d_rotation',
        items: [
            {
                id: '3d-view',
                text: '3D View',
                type: 'item',
                url: '/3d-view',
            },
        ]
    },
    {
        id: 'endpoints',
        text: 'EndPoint',
        type: 'collapsable',
        icon: 'devices',
        items: [
            {
                id: 'endpoint',
                text: 'Inventory',
                type: 'item',
                url: '/endpoints',
            },
            {
                id: 'endpointsets',
                text: 'EndPoint Set',
                type: 'item',
                url: '/endpointsets',
            }
        ]
    },
    {
        id: 'network_devices',
        text: 'Networking',
        type: 'collapsable',
        icon: 'device_hub',
        items: [
            {
                id: 'topology',
                text: 'Topology',
                type: 'item',
                url: '/topology',
            },
            {
                id: 'devices',
                text: 'Device',
                type: 'item',
                url: '/devices',
            },
            {
                id: 'Portal',
                text: 'Portal',
                type: 'item',
                url: '/audits',
            },
            {
                id: 'feature',
                text: 'feature',
                type: 'item',
                url: '/networktemplates',
            },
        ]
    },
    {
        id: 'filemanager',
        text: 'File Manager',
        type: 'item',
        icon: 'folder_open',
        url: '/file-manager'
    },
    {
        id: 'schedule',
        text: 'Schedule',
        type: 'collapsable',
        icon: 'schedule',
        items: [
            {
                id: 'summary',
                text: 'Summary',
                type: 'item',
                url: '/schedule',
            },
            {
                id: 'configbackup',
                text: 'Config Backup',
                type: 'item',
                url: '/snapshottasks',
            },
            {
                id: 'firmware',
                text: 'Firmware',
                type: 'item',
                url: '/fwmaintgrps',
            },
            {
                id: 'reports',
                text: 'Report',
                type: 'item',
                url: '/reports',
            }
        ]
    },
    {
        id: 'system',
        text: 'System',
        type: 'collapsable',
        icon: 'settings',
        items: [
            {
                id: 'Configure',
                text: 'Tenant',
                type: 'item',
                url: '/tenants',
            },

            {
                id: 'wslabels',
                text: 'Label',
                type: 'item',
                url: '/wslabels',
            },
            {
                id: 'settings',
                text: 'Setting',
                type: 'item',
                url: '/settings',
            },
        ]
    },
];
