export const locale = {
    lang: 'en',
    data: {
        NAV: {
            APPLICATIONS: 'Applications',
            DASHBOARDS: 'Dashboards',
            INVENTORY: 'Inventory',
            SAMPLE: {
                TITLE: 'Sample',
                BADGE: '25',
            },
        },
    },
};
