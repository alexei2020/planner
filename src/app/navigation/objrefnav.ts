import { kindToEndpoint } from './objrefnav.gen';
import { ObjectRef} from '../typings/backendapi';

export const kindToDefaultBaseUrl: Record<string, (name, namespace, apigroup) => string> = {
    Device: (name, namespace, apigroup) => 'devices/' + name,
    EndPoint: (name, namespace, apigroup) => 'endpoints/' + name + '?' + (namespace ? 'namespace=' + namespace : ''),
};

// apiVersion at the time of writing looks like this: ws.io/v1
// location would be the first part of apiVersion
export function metaToUrl(objectRef: ObjectRef): string {
    if (!objectRef || !objectRef.name) {
        return '';
    }

    let location = '';
    if (objectRef.apiVersion) {
        const tokens = objectRef.apiVersion.split('/');
        if (tokens.length !== 2) {
            console.log('Expected format of apiVersion is loc/version, such as ws.io/v1. Got ' + objectRef.apiVersion + ' instead');
        } else {
            location = tokens[0];
        }
    }
    const result = kindToEndpoint.get(objectRef.kind) + '/' + objectRef.name + '?' +
        (location ? '&loc=' + location : '') + (objectRef.namespace ? '&namespace=' + objectRef.namespace : '');
    return result;
}

// defaultValueOnErrorReporter
export function blankVer(
    startingVal: any,
    startingPropsChain: string[],
    erroredVal: any,
    erroredPropsChain: string[]): any {
    return '';
}

// valOnErrorReporterConstFactory
export function verConstFactory(val: any): (startingVal: any,
                                            startingPropsChain: string[],
                                            erroredVal: any,
                                            erroredPropsChain: string[]) => any {
    return (startingVal: any,
            startingPropsChain: string[],
            erroredVal: any,
            erroredPropsChain: string[]): any => {
        return val;
    };
}

// access chained props, such as foo.boo.zoo.xyz by calling this method as
// chainedValueAccessor(foo, 'boo.zoo.xyz', valOnErrorReporter)
export function chainedValueAccessor(
    val: any, propsChain: string,
    valOnErrorReporter: (startingVal: any,
                         startingPropsChain: string[],
                         erroredVal: any,
                         erroredPropsChain: string[]) => any): any {
    const startingPropsChain = (propsChain + '').split('.', -1);
    const inner = (innerVal: any, props: string[]): string => {
        if (innerVal === undefined || innerVal == null) {
            return valOnErrorReporter(val, startingPropsChain, innerVal, props);
        }
        if (props.length === 0) {
            return innerVal.toString();
        }
        return inner(innerVal[props[0]], props.slice(1, props.length));
    };
    return inner(val, startingPropsChain);
}
