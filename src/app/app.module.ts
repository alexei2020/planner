import 'hammerjs';

import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { AppComponent } from 'app/app.component';
import { fuseConfig } from 'app/fuse-config';
import { LayoutModule } from 'app/layout/layout.module';
import { SampleModule } from 'app/main/sample/sample.module';
import { GridModule } from '@progress/kendo-angular-grid';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import { LabelModule } from '@progress/kendo-angular-label';
import { MenuModule } from '@progress/kendo-angular-menu';
import { WebsocketService } from './services/websocket.service';
import {CookieService} from 'ngx-cookie-service';
import { LocationResolver } from './components/location/location.resolver';
const appRoutes: Routes = [
    {
        path: 'sample',
        data: {
            breadcrumb: 'Sample'
        },
        redirectTo: 'sample'
    },
    {
        path: '',
        data: {
            breadcrumb: ''
        },
        resolve: {resolveData: LocationResolver},
        loadChildren: () =>
            import('./main/apps/apps.module').then(m => m.AppsModule)
    },
    {
        path: '**',
        redirectTo: 'devices'
    },
];

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes,
            { useHash: true }
        ),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatDialogModule,
        MatSnackBarModule,
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        SampleModule,
        GridModule,
        ChartsModule,
        InputsModule,

        ScrollingModule,
        
        NgMultiSelectDropDownModule.forRoot(),

        // Kendo modules
        ButtonsModule,
        DialogsModule,
        LabelModule,
        MenuModule,
    ],
    providers: [
      WebsocketService,
      CookieService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
