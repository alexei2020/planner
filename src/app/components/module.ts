import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { FuseWidgetModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { WidgetComponent } from './widget/component';
import { FormWidgetComponent } from './form-widget/component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LocationWidgetComponent } from './location/component';
import { TimeZoneWidgetComponent } from './timezone/component';

import { GoogleMapsComponent } from './google-map/gmap.component';
import { AgmCoreModule } from '@agm/core';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { AngularResizedEventModule } from 'angular-resize-event';
import { LabelSelectorComponent } from './label-selector/label-selector.component'

import { ScrollingModule } from '@angular/cdk/scrolling';
import { AutofocusDirective } from './label-selector/autofocus.directive';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { PopupModule } from '@progress/kendo-angular-popup';
import { TabStripModule } from '@progress/kendo-angular-layout';

@NgModule({
    declarations: [WidgetComponent, FormWidgetComponent, LocationWidgetComponent, GoogleMapsComponent, LabelSelectorComponent, AutofocusDirective, TimeZoneWidgetComponent],
    imports: [
        CommonModule,
        RouterModule,
        FuseSharedModule,
        FuseWidgetModule,
        MatCardModule,
        MatIconModule,
        MatTooltipModule,
        MatButtonModule,
        DropDownsModule,
        MatProgressSpinnerModule,
        AngularResizedEventModule,
        ScrollingModule,
        TabStripModule,
	TreeViewModule,
        PopupModule,
        ButtonsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAfPO_C_Wr_VMu7b7tG54VG-wiPGPFVqQw',
            libraries: ["places"]
        }),
    ],
    exports: [WidgetComponent, FormWidgetComponent, LocationWidgetComponent, GoogleMapsComponent, LabelSelectorComponent, TimeZoneWidgetComponent],
})
export class ComponentsModule { }
