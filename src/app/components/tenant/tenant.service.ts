import { Inject, Injectable, InjectionToken } from "@angular/core";
import { ResolveEnd, Router } from "@angular/router";
import { Platform } from "@angular/cdk/platform";
import { BehaviorSubject, Observable } from "rxjs";
import { filter } from "rxjs/operators";
import * as _ from "lodash";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "environments/environment";

export interface Metadata {
  name?: string;
  uid?: string;
  creationTimestamp?: string;
  labels?: string;
  selfLink?: string;
  resourceVersion?: string;
}
export interface Tenant {
  APIGroupName: string;
  APIGroupUID?: string;
  state?: string;
  location?: string;
}
export interface Tenants {
  [key: string]: Tenant;
}
export interface User {
  [key: string]: string;
}
export interface Spec {
  base: any;
  defaultTenant: string;
  tenants: Tenants;
  user: User;
}
export interface GlobalTenants {
  apiVersion?: string;
  metadata?: Metadata;
  spec?: Spec;
  status?: {
    base: any;
  };
}
export interface DropdownListItem {
    name: string;
    APIGroupName: string;
    APIGroupUID?: string;
    state?: string;
    location?: string;
  }
@Injectable({
  providedIn: "root",
})
export class FuseTenantService {
  private _apiURI = `${environment.baseUrl}apis/scope.ws.io/v1/global`;
  public selectedTenant: BehaviorSubject<DropdownListItem>;
  public tenants: BehaviorSubject<GlobalTenants> = new BehaviorSubject<GlobalTenants>({});

  constructor(private http: HttpClient) {
    this.selectedTenant = new BehaviorSubject({
      name: "",
      APIGroupName: "",
      APIGroupUID: "",
      state: "",
    });
  }
  selectedTenantAsKeyValue(): Tenant {
    return this.selectedTenant.getValue();
  }

  get<T = any>(url: string) {
    return this.http.get(`${url}`).pipe(map(this.handleSuccess)) as Observable<
      T
    >;
  }

  protected handleSuccess(res: any) {
    return res;
  }

  getTenants(): Observable<GlobalTenants> {
    return this.get(`${this._apiURI}`);
  }

}
