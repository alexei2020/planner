import { Component, Input, Output, OnInit, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { TreeItemLookup } from '@progress/kendo-angular-treeview';

@Component({
    selector: 'search-filter',
    templateUrl: './component.html',
    styleUrls: ['./component.scss'],
})
export class SearchFilterComponent implements OnInit, OnChanges {
    @Input() data;
    @Output() filterChange: any = new EventEmitter();
    public checkedKeys: any[] = [];
    public expandedKeys: any[] = [];

    constructor(){

    }
    ngOnInit() {
        this.loadCheckedKeys();
        this.data.forEach((field, fieldIndex) => {
            this.expandedKeys.push(`${fieldIndex}`);
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.data) {
            this.loadCheckedKeys();
        }
    }

    private loadCheckedKeys(): void {
	this.checkedKeys = [];
        this.data.forEach((field, fieldIndex) => {
            field.keys.forEach((key, keyIndex) => {
                if (key.checked) {
                    this.checkedKeys.push(`${fieldIndex}_${keyIndex}`);
                }
            });
        });
    }

    public fetchChildren(node: any): Observable<any[]> {
        // Return the parent node's items collection as children
        return of(node.keys);
    }

    public hasChildren(node: any): boolean {
        // Check if the parent node has children
        return node.keys && node.keys.length > 0;
    }

    public handleChecking(itemLookup: TreeItemLookup): void {
        itemLookup.item.dataItem.checked = !itemLookup.item.dataItem.checked;
        this.filterChange.emit();
    }
}
