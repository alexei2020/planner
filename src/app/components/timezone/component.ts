import {
    Component,
    OnInit,
    OnDestroy,
    HostListener,
    ViewChild,
    ElementRef
  } from "@angular/core";
  import { ActivatedRoute, Router, NavigationEnd } from "@angular/router";
  
  import { Subject, timer } from "rxjs";
  import { takeUntil, map } from "rxjs/operators";
  
  import {
    FuseLocationService,
  } from "../../services/location.service";
  
  import jstz from 'jstz';
  import * as _ from "lodash";

  import { zonedTimeToUtc, format, utcToZonedTime } from 'date-fns-tz';
  export interface TimeZoneItem{
    label: string,
    value: string,
    isBrowserTime: boolean
  }
  @Component({
    selector: "fuse-timezone-widget",
    templateUrl: "./component.html",
    styleUrls: ["./component.scss"],
  })
  export class TimeZoneWidgetComponent implements OnInit, OnDestroy {
    @ViewChild("anchor") public anchor: ElementRef;
    @ViewChild("popup", { read: ElementRef }) public popup: ElementRef;
    public timeZoneList: Array<TimeZoneItem>;
    public selectedTimeZone: TimeZoneItem;
    public show: boolean = false;
    public currentTime: any;
    public isBowserTime: boolean;
    private _unsubscribeAll: Subject<any>;
    constructor(
        private _locationService: FuseLocationService
    ){
        this._unsubscribeAll = new Subject();
        this.isBowserTime = false;
    }
    ngOnInit(){
        this.timeZoneList  = [];
        const timezone = jstz.determine();
        
        this._locationService.locationData.pipe(takeUntil(this._unsubscribeAll)).subscribe(res=>{
            const localTimeZone = {
                label: 'Browser Time',
                value: timezone.name(),
                isBrowserTime: true
            }
            if(res.timezone){
                this.timeZoneList = [localTimeZone, { label: "Location Time", value: res.timezone, isBrowserTime: false}];
                if(this.isBowserTime)
                    this.selectedTimeZone = this.timeZoneList[0];
                else
                    this.selectedTimeZone = this.timeZoneList[1];
            } else {
                this.timeZoneList = [localTimeZone];
                this.selectedTimeZone =  this.timeZoneList[0];
            }
            this.currentTime = timer(0, 1000).pipe(map(() => format(utcToZonedTime(new Date(), this.selectedTimeZone.value), 'EEE, hh:mm bbb zzz', { timeZone: this.selectedTimeZone.value })));
        })
    }
    ngOnDestroy(){
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    onTimeZoneChange(timezone: TimeZoneItem){
        console.log("timezone changed", timezone);
        this.close();
        if(this.selectedTimeZone.value === timezone.value)
            return;
        this.isBowserTime = timezone.isBrowserTime;
        this._locationService.isBrowserTime.next(timezone.isBrowserTime);
        this.currentTime = timer(0, 1000).pipe(map(() => format(utcToZonedTime(new Date(), timezone.value), 'EEE, hh:mm bbb zzz', {timeZone: timezone.value})));
        this.selectedTimeZone = timezone;
    }
    public onToggle(): void {
        this.show = !this.show;
    }
    public close(): void {
        this.show = false;
    }
    @HostListener("document:click", ["$event"])
    public documentClick(event: any): void {
        if (!this.isPopup(event.target)) {
            this.close();
        }
    }
    private isPopup(target: any): boolean {
        return (
            this.anchor.nativeElement.contains(target) ||
            (this.popup ? this.popup.nativeElement.contains(target) : false)
        );
        
    }
    onBlur(){
        console.log("blue")
    }
  }
