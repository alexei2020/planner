import { Component, OnInit, OnDestroy, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';
import { FuseLocationService } from '../../services/location.service';
import { FuseTenantService, DropdownListItem, GlobalTenants } from '../tenant/tenant.service';
import * as _ from 'lodash';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { LocationDataItem, LocationListItem } from '../shared/location-item';
import { ILocationDataItem, ILocations } from 'app/typings/location';
import { StringMap } from 'app/typings/backendapi.gen';
import { FuseLocationDataManagementService } from 'app/services/location-data-management.service';
import { TabStripComponent } from '@progress/kendo-angular-layout';

@Component({
  selector: 'fuse-location-widget',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
})
export class LocationWidgetComponent implements OnInit, OnDestroy {
  // hierarchical dropdonwlist
  @ViewChild('anchor')
  public anchor: ElementRef;

  @ViewChild('popup', { read: ElementRef })
  public popup: ElementRef;

  @ViewChild('tenantLocationTabstrip')
  public tenantLocationTabstrip: TabStripComponent;

  static updatedLocation = false;
  static updatedTenant = false;
  static tenantName = '';

  public searchTerm = '';
  public locationDataSource: LocationDataItem[];
  public filteredLocationData: any[];
  public show: boolean;
  public selectedKeys: Array<string>;
  public isLocationDropdownEnabled: boolean;

  public tenantListSource: DropdownListItem[];
  public tenantListData: DropdownListItem[];
  public selectedTenant: DropdownListItem;
  public isTenantTabSelected: boolean;
  public showLocationTreeOnly: boolean;
  public state: {
    isLoading: boolean;
    isFailed: boolean;
  };

  private _unsubscribeAll: Subject<any>;
  private locationList: LocationListItem[];
  private relativeURI: string;
  private locationTabIndex: number;

  constructor(
    private _locationService: FuseLocationService,
    private _route: ActivatedRoute,
    private _router: Router,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    private _fuseTenantService: FuseTenantService,
    private locationDataManagementService: FuseLocationDataManagementService
  ) {
    iconRegistry.addSvgIcon(
      'toolbar-pin',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/toolbar/pin.svg')
    );
    iconRegistry.addSvgIcon(
      'edit',
      sanitizer.bypassSecurityTrustResourceUrl('assets/roomplanner/menu/edit.svg')
    );
    this.show = false;

    // Set the private defaults
    this._unsubscribeAll = new Subject();
    this.locationList = [];
    this.isLocationDropdownEnabled = true;
    this.locationTabIndex = 1;
    this.isTenantTabSelected = true;
    this.showLocationTreeOnly = false;
    this.state = {
      isLoading: false,
      isFailed: false,
    };
  }

  async ngOnInit(): Promise<void> {
    this.state.isLoading = true;
    LocationWidgetComponent.updatedTenant = false;
    LocationWidgetComponent.updatedLocation = false;
    this._fuseTenantService.tenants.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: GlobalTenants) => {
      if (!res.spec) {
        return;
      }
      this.tenantListSource = _.map(res.spec.tenants, (value, key) => {
        return {
          name: key,
          APIGroupName: value.APIGroupName,
          APIGroupUID: value.APIGroupUID,
          state: value.state,
        };
      });
      const currentTenant = this._fuseTenantService.selectedTenant.getValue();
      const currentTenantName = currentTenant.name ? currentTenant.name : res.spec.defaultTenant;
      this.selectedTenant = _.find(this.tenantListSource, (item: DropdownListItem) => {
        return item.name === currentTenantName;
      });
      this.tenantListData = this.tenantListSource.slice();
    });

    this._fuseTenantService.selectedTenant
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(async (tenant: DropdownListItem) => {
        if (!tenant || !tenant.name || tenant.name.length <= 0) {
          return;
        }
        this.state.isLoading = true;
        await this.updateLocationList(tenant);
      });

    if (this.selectedTenant) {
      this._locationService.forceRefreshLocationList
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(async (forceRefresh: boolean) => {
          if (!forceRefresh) {
            return;
          }
          this.selectedTenant.location = _.first(this.selectedKeys);
          await this.updateLocationList(this.selectedTenant);
        });
    }

    this._route.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
      this.relativeURI = this._router.url.split('?')[0];
    });

    this._router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        takeUntil(this._unsubscribeAll)
      )
      .subscribe(() => {
        this.navigateToWithLocationParams();
      });

    this._locationService.locationDropdownEnabled
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((dropdownEnabled: boolean) => {
        this.isLocationDropdownEnabled = dropdownEnabled;
      });
    this.state.isLoading = false;
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  async updateLocationList(tenant: DropdownListItem): Promise<void> {
    this.state.isLoading = true;
    const accessibleData = await this._locationService
      .getAccessibleData(tenant.name)
      .pipe(takeUntil(this._unsubscribeAll))
      .map((data) => data.spec.locations)
      .toPromise();
    this._locationService
      .getHierarchicalData(tenant.APIGroupName)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (locationList: ILocations) => {
          this.updateCurrentLocationData(locationList, accessibleData, tenant);
          this.state.isLoading = false;
          this.state.isFailed = false;
        },
        (err) => {
          this.state.isLoading = false;
          this.state.isFailed = true;
        }
      );
    LocationWidgetComponent.updatedLocation = false;
  }

  updateLocation({ dataItem }: any): void {
    this.show = false;
    this.selectedKeys = [dataItem.name];
    const locationData = {
      currentLocation: dataItem.APIGroupName,
      locationName: dataItem.name,
      timezone: dataItem.geo.timeZoneID,
    };
    this._locationService.updateLocationData(locationData);
    this._locationService.updateLocationSubTreeDataValue(dataItem);

    this._router.navigate([this.relativeURI], {
      queryParams: {
        loc: dataItem.name,
      },
      replaceUrl: true,
      queryParamsHandling: 'merge',
    });
  }

  onTenantItemClicked(item: DropdownListItem, event: MouseEvent): void {
    event.stopPropagation();
    this.relativeURI = this._router.url.split('?')[0];
    this.searchTerm = '';
    if (this.selectedTenant.name !== item.name) {
      console.log('selection changed--->', item);
      this.selectedTenant = item;
      const locationData = {
        currentLocation: item.APIGroupName,
        locationName: item.name,
        tenantName: item.name,
        tenantAPIGroupName: item.APIGroupName,
      };
      this._locationService.updateLocationData(locationData);
      this._fuseTenantService.selectedTenant.next(item);
      this._router.navigate([this.relativeURI], {
        queryParams: {
          loc: item.name,
          tenant: item.name,
        },
        replaceUrl: true,
        queryParamsHandling: 'merge',
      });
      this.locationDataSource = [];
      this.filteredLocationData = [];
    }
    this.tenantLocationTabstrip.selectTab(this.locationTabIndex);
    this.isTenantTabSelected = false;
  }

  onKeyUp(value: string): void {
    this.filteredLocationData = this.hierarchicalSearch(this.locationList, value);
    if (value === undefined || value === '') {
      this.tenantListData = this.tenantListSource;
    } else {
      this.tenantListData = _.filter(
        this.tenantListSource,
        (s) => s.name.toLowerCase().indexOf(value.toLowerCase()) !== -1
      );
    }
  }

  contains(text: string, term: string): boolean {
    return text.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  }

  hierarchicalSearch(items: LocationListItem[], term: string): LocationListItem[] {
    const hierarchicalItems = _.reduce(
      items,
      (acc: LocationListItem[], item) => {
        if (this.contains(item.label, term)) {
          acc.push(item);
        } else if (item.children && item.children.length > 0) {
          const newItems = this.search();

          if (newItems.length > 0) {
            acc.push({ ...item, name: item.name, children: newItems });
          }
        }
        return acc;
      },
      []
    );

    return hierarchicalItems;
  }

  search(): any[] {
    return [];
  }

  close(): void {
    this.show = false;
  }

  @HostListener('document:click', ['$event'])
  documentClick(event: any): void {
    if (!this.isPopup(event.target)) {
      this.close();
    }
  }

  onToggle(): void {
    this.show = !this.show;
    if (this.show) {
      if (LocationWidgetComponent.updatedTenant) {
        LocationWidgetComponent.updatedTenant = false;
        this.ngOnInit();
      }
      if (LocationWidgetComponent.updatedLocation) {
        LocationWidgetComponent.updatedLocation = false;
        // if (!this.selectedTenant) {
        //   this.selectedTenant.name = LocationWidgetComponent.tenantName;
        // }
        // this.updateLocationList(this.selectedTenant);
        this.ngOnInit();
      }
      this.showLocationTreeOnly = this.tenantListSource.length === 1;
      this.filteredLocationData = this.locationDataSource;
      this.tenantListData = this.tenantListSource;
      this.searchTerm = '';
      if (this.tenantListData.length === 1) {
        this.isTenantTabSelected = false;
      }
    }
  }

  goToLocationPage(): void {
    this._router.navigate(['location'], {
      queryParams: {
        loc: this._locationService.locationDataAsKeyValue().locationName,
        tenant: this._locationService.locationDataAsKeyValue().tenantName,
      },
    });
    this.close();
  }

  private isPopup(target: any): boolean {
    return (
      this.anchor.nativeElement.contains(target) || (this.popup ? this.popup.nativeElement.contains(target) : false)
    );
  }

  private getCurrentLocationSubTree(currentLocation: string, tree: LocationDataItem[]): LocationDataItem {
    let currentLocationSubTree: LocationDataItem;
    _.forEach(tree, (item) => {
      if (item.name === currentLocation) {
        currentLocationSubTree = item;
        return false;
      }
      if (!_.isEmpty(item.children)) {
        currentLocationSubTree = this.getCurrentLocationSubTree(currentLocation, item.children);
        if (!_.isEmpty(currentLocationSubTree)) {
          return false;
        }
      }
    });
    return currentLocationSubTree;
  }

  private changeCurrentLocation(location: string): void {
    const subTree = this.getCurrentLocationSubTree(location, this.filteredLocationData);

    if (!_.isEmpty(subTree)) {
      const locationData = {
        currentLocation: subTree.APIGroupName,
        locationName: subTree.name,
      };
      this._locationService.updateLocationData(locationData);
      this.selectedKeys = [subTree.name];
      this._locationService.updateLocationSubTreeDataValue(subTree);
    }
  }

  private navigateToWithLocationParams(): void {
    console.log('Location component: ' + window.location.href);
    this.relativeURI = this._router.url.split('?')[0];
    const tenantInQueryParam = this._route.snapshot.queryParams.tenant;
    const locationInQueryParam = this._route.snapshot.queryParams.loc;
    const currentLocation = this._locationService.locationDataAsKeyValue().locationName;
    const currentTenant = this._locationService.locationDataAsKeyValue().tenantName;
    if (
      (tenantInQueryParam && tenantInQueryParam !== currentTenant) ||
      (locationInQueryParam && locationInQueryParam !== currentLocation)
    ) {
      this._router.navigate([this.relativeURI], {
        queryParams: {
          loc: locationInQueryParam || currentLocation,
          tenant: tenantInQueryParam || currentTenant,
        },
        queryParamsHandling: 'merge',
      });
      this.changeCurrentLocation(locationInQueryParam);
    } else if (currentLocation && currentTenant) {
      this._router.navigate([], {
        queryParams: {
          loc: currentLocation,
          tenant: currentTenant,
        },
        queryParamsHandling: 'merge',
      });
    }
    console.log('----Navigation End----');
  }

  private updateCurrentLocationData(
    locationList: ILocations,
    accessibleData: StringMap<ILocationDataItem>,
    tenant: DropdownListItem
  ): void {
    this.locationList = [];

    try {
      const locationListData = this.locationDataManagementService.getLocationListData(locationList, accessibleData);
      this.locationList = locationListData.locationList;

      const root = this.locationDataManagementService.getLocationRoot(locationListData.locationData);
      const selectedLoc = tenant.location ? tenant.location : root.name;
      this.selectedKeys = [selectedLoc];
      this.locationDataSource = [root];
      this.filteredLocationData = this.locationDataSource;
      this._locationService.updateLocationListValue(this.locationList);
      this._locationService.updateLocationHierarchyValue(root);

      const currentLocationName = this._locationService.locationDataAsKeyValue().locationName;
      const subTree = this.getCurrentLocationSubTree(currentLocationName, this.filteredLocationData);

      this._locationService.updateLocationSubTreeDataValue(subTree);
      const currentLocObj = _.find(this.locationList, ['name', selectedLoc]);
      if (currentLocObj) {
        const locationData = {
          timezone: currentLocObj.geo.timeZoneID,
        };
        this._locationService.updateLocationData(locationData);
      }
      this.relativeURI = this._router.url.split('?')[0];
    } catch (error) {
      this.state.isFailed = true;
    }
  }
}
