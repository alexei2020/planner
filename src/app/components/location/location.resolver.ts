import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { FuseTenantService, GlobalTenants } from 'app/components/tenant/tenant.service';
import { FuseLocationService } from 'app/services/location.service';
import * as _ from 'lodash';
import { ILocationDataItem, ILocations } from 'app/typings/location';
import { FuseLocationDataManagementService } from 'app/services/location-data-management.service';
import { StringMap } from 'app/typings/backendapi.gen';
import { LocationParamsData } from '../shared/location-item';

@Injectable({
  providedIn: 'root',
})
export class LocationResolver implements Resolve<Observable<any>> {
  constructor(
    private tenantService: FuseTenantService,
    private locationService: FuseLocationService,
    private locationDataManagementService: FuseLocationDataManagementService
  ) {}

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
    const loc = route.queryParams.loc;
    const tenant = route.queryParams.tenant;
    const tenants: GlobalTenants = await this.tenantService.getTenants().toPromise();
    try {
      if (!tenant) {
        const defaultTenantName = tenants.spec.defaultTenant;
        let defaultAPIGroupName = 'scope.ws.io';
        if (tenants.spec.tenants) {
          defaultAPIGroupName = tenants.spec.tenants[defaultTenantName].APIGroupName;
        }

        await this.updateLocationData(defaultTenantName, defaultTenantName, defaultAPIGroupName, defaultAPIGroupName);
      } else {
        let tenantName = tenant;

        // check if the tenant name parsed from queryParam exists in data from API. If not, sets default value
        if (tenants.spec.tenants[tenantName]) {
          const tenantAPIGroupName = tenants.spec.tenants[tenantName].APIGroupName;
          await this.updateLocationData(tenantName, '', tenantAPIGroupName, '', true, loc);
        } else {
          tenantName = tenants.spec.defaultTenant;
          const tenantAPIGroupName = tenants.spec.tenants[tenantName].APIGroupName;

          await this.updateLocationData(tenantName, tenantName, tenantAPIGroupName, tenantAPIGroupName);
        }
      }
      this.tenantService.tenants.next(tenants);
      return null;
    } catch (e) {
      console.log('exception in resolver---->', e);
      return null;
    }
  }

  resolveData(accessibleData: StringMap<ILocationDataItem>, hierarchicalData: ILocations): void {
    const locationListData = this.locationDataManagementService.getLocationListData(hierarchicalData, accessibleData);
    const root = this.locationDataManagementService.getLocationRoot(locationListData.locationData);

    this.locationService.updateLocationListValue(locationListData.locationList);
    this.locationService.updateLocationHierarchyValue(root);
  }

  private async updateLocationData(
    tenantName: string,
    currentLocationName: string,
    tenantAPIGroupName: string,
    currentAPIGroupName: string,
    isTenantNameExistInData: boolean = false,
    location: string = ''
  ): Promise<void> {
    const accessibleData = await this.locationService
      .getAccessibleData(tenantName)
      .map((data) => data?.spec?.locations)
      .toPromise();
    const hierarchicalData = await this.locationService.getHierarchicalData(tenantAPIGroupName).toPromise();

    let timezone = '';
    if (!isTenantNameExistInData) {
      const locationObj = _.filter(hierarchicalData.items, (item) => item.metadata.name === tenantName)[0];
      timezone = locationObj?.status.geo.timeZoneID;
    } else {
      const locationParamsData = this.getLocationParams(
        hierarchicalData,
        tenantName,
        accessibleData,
        location,
        tenantAPIGroupName
      );
      timezone = locationParamsData.timezone;
      currentLocationName = locationParamsData.currentLocationName;
      currentAPIGroupName = locationParamsData.currentAPIGroupName;
    }

    this.resolveData(accessibleData, hierarchicalData);
    const locationData = {
      currentLocation: currentAPIGroupName,
      locationName: currentLocationName,
      tenantName: tenantName,
      tenantAPIGroupName: tenantAPIGroupName,
      timezone: timezone,
    };
    this.locationService.updateLocationData(locationData);
    this.tenantService.selectedTenant.next({
      name: tenantName,
      APIGroupName: tenantAPIGroupName,
      location: currentLocationName,
    } as any);
  }

  private getLocationParams(
    hierarchicalData: ILocations,
    tenantName: string,
    accessibleData: StringMap<ILocationDataItem>,
    location: string,
    tenantAPIGroupName: string
  ): LocationParamsData {
    const locationObj = accessibleData[location];

    // check if location name exists
    if (locationObj) {
      const currentLocObj = _.filter(hierarchicalData.items, (item) => item.metadata.name === location)[0];

      const locationTimezone = currentLocObj ? currentLocObj.status.geo.timeZoneID : '';
      const locationData = new LocationParamsData({
        currentLocationName: location,
        currentAPIGroupName: locationObj.APIGroupName,
        timezone: locationTimezone,
      });
      return locationData;
    }

    const currentLocationObj = _.filter(hierarchicalData.items, (item) => item.metadata.name === tenantName)[0];
    const timezone = currentLocationObj ? currentLocationObj.status.geo.timeZoneID : '';
    const locationParamsData = new LocationParamsData({
      currentLocationName: tenantName,
      currentAPIGroupName: tenantAPIGroupName,
      timezone: timezone,
    });
    return locationParamsData;
  }
}
