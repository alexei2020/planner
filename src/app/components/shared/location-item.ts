export class RBACItem {
  readonly permissions: string;
  readonly privilege: string;

  constructor({ permissions, privilege }: Partial<RBACItem> = {}) {
    this.permissions = permissions;
    this.privilege = privilege;
  }
}

export class Metadata {
  readonly name: string;
  readonly apiVersion: string;

  constructor({ name, apiVersion }: Partial<Metadata> = {}) {
    this.name = name;
    this.apiVersion = apiVersion;
  }
}

export class GeoItem {
  readonly address: string;
  readonly latitude: string;
  readonly longitude: string;
  readonly timeZoneID: string;

  constructor({ address, latitude, longitude, timeZoneID }: Partial<GeoItem> = {}) {
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;
    this.timeZoneID = timeZoneID;
  }
}

export class LocationDataItem {
  readonly name: string;
  readonly value: string;
  readonly selfLink: string;
  readonly parentLink: string;
  readonly geo: GeoItem;
  readonly APIGroupName: string;
  readonly APIGroupUID: string;
  readonly RBAC: RBACItem[];
  children: LocationDataItem[];

  constructor({
    name,
    value,
    selfLink,
    parentLink,
    geo,
    APIGroupName,
    APIGroupUID,
    RBAC,
    children,
  }: Partial<LocationDataItem> = {}) {
    this.name = name;
    this.value = value;
    this.selfLink = selfLink;
    this.parentLink = parentLink;
    this.geo = geo;
    this.APIGroupName = APIGroupName;
    this.APIGroupUID = APIGroupUID;
    this.RBAC = RBAC;
    this.children = children;
  }
}

export class LocationItem {
  readonly name: string;
  readonly apiVersion: string;
  readonly metadata: Metadata;

  constructor({
    name,
    apiVersion,
    metadata
  }: Partial<LocationItem> = {}) {
    this.name = name;
    this.apiVersion = apiVersion;
    this.metadata = metadata;
  }
}

export class LocationListItem {
  readonly label: string;
  readonly name: string;
  readonly value: string;
  readonly geo: GeoItem;
  children: LocationListItem[];

  constructor({ label, name, value, geo, children }: Partial<LocationListItem> = {}) {
    this.label = label;
    this.name = name;
    this.value = value;
    this.geo = geo;
    this.children = children;
  }
}

export class LocationListData {
  readonly locationList: LocationListItem[];
  readonly locationData: LocationDataItem[];

  constructor({ locationList, locationData }: Partial<LocationListData> = {}) {
    this.locationList = locationList;
    this.locationData = locationData;
  }
}

export class LocationParamsData {
  readonly currentLocationName: string;
  readonly currentAPIGroupName: string;
  readonly timezone: string;

  constructor({ currentLocationName, currentAPIGroupName, timezone }: Partial<LocationParamsData> = {}) {
    this.currentLocationName = currentLocationName;
    this.currentAPIGroupName = currentAPIGroupName;
    this.timezone = timezone;
  }
}
