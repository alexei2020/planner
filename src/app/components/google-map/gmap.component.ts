import { Component, Input, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import * as _ from 'lodash';
import { MapsAPILoader, LatLngBounds, LatLng, LatLngBoundsLiteral, AgmMap, AgmGeocoder, GeocoderRequest } from '@agm/core'
import { FuseLocationService } from '../../services/location.service';
declare var google: any;
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';
interface Marker {
    lat: number;
    lng: number;
}

interface Location {
    latitude: number;
    longitude: number;
    mapType?: string;
    zoom?: number;
    markers: Array<Marker>;
}
@Component({
    selector: 'fuse-google-maps',
    templateUrl: './gmap.component.html',
    styleUrls: ['./gmap.component.scss']
})
export class GoogleMapsComponent implements AfterViewInit, OnInit {
    @ViewChild('AgmMap') agmMap: AgmMap;
    location: Location;
    private _geoData: any;
    private _center: LatLng;
    private _markers: any[] = [];
    dbClickedTarget: any;
    currentLocation: string;
    private _unsubscribeAll: Subject<any>;

    set markers(data: any[]) {
        this._markers = data;
    }
    get markers(): any[] {
        return this._markers;
    }
    @Input('geoData') set geoData(data: any) {
        this._geoData = _.clone(data);
        console.log(this._geoData);

        console.log(this._geoData);
    }
    ngAfterViewInit() {

    }
    get geoData(): any {
        if (this._geoData === undefined || this._geoData.features.length === 0)
            return null;

        return this._geoData;
    }
    /**
     * Constructor
     */
    constructor(
        private _agmGeocoder: AgmGeocoder,
        private _locationService: FuseLocationService,
        private _router: Router
    ) {
        this._unsubscribeAll = new Subject();
        // Set the defaults
        this.location = {
            latitude: 38.629151,
            longitude: -90.260637,
            mapType: "roadmap",
            zoom: 5,
            markers: [
                {
                    lat: 38.629151,
                    lng: -90.260637
                }
            ]
        }
    }
    protected mapReady(map) {
        if (!_.isEmpty(this._geoData)) {
            let markers: any[] = [];
            this._geoData.features.map(feature => {
                const geocodeRequest: GeocoderRequest = {
                    address: feature.properties.address
                };
                let geocoder = new google.maps.Geocoder();
                geocoder.geocode(geocodeRequest, (results, status) => {
                    markers.push({
                        address: feature.properties.address,
                        description: feature.properties.description,
                        namespace: feature.properties.namespace,
                        timezone: feature.properties.timezone,
                        location: feature.properties.location,
                        lat: results[0].geometry.location.lat(),
                        lng: results[0].geometry.location.lng()
                    });
                })
            });
            this.markers = markers;
        }
    }
    onMarkerClick(event) {
        console.log("marker clicked", event);
        
        this.dbClickedTarget = {
            lat: event.latitude,
            lng: event.longitude
        }
    }
    ngOnInit() {
        // this.map.fitBounds
        this._locationService.locationData.pipe(takeUntil(this._unsubscribeAll)).subscribe(res=>{
            this.currentLocation = res.currentLocation;
        })
    }
    onDbClick(event) {
        const marker = this._markers.filter(marker=>marker.lat===this.dbClickedTarget.lat && marker.lng===this.dbClickedTarget.lng)[0];
        console.log(marker);
        const namespace = marker.namespace;
        const locationData = {
            currentLocation: marker.location
        };
        this._locationService.updateLocationData(locationData);
        this._router.navigate(['/topology'],{
            queryParams: {
                loc: marker.location
            }
        })
    }

    onMouseOverMaker(infoWindow, gm) {
        console.log("mouse over", infoWindow, event);
        if (gm.lastOpen && gm.lastOpen.isOpen) {
            gm.lastOpen.close();
        }

        gm.lastOpen = infoWindow;

        infoWindow.open();
    }

    onMouseOutMarker(infoWindow, gm) {
        console.log("mouse out", event);
        if (gm.lastOpen && gm.lastOpen.isOpen) {
            gm.lastOpen.close();
        }

        gm.lastOpen = infoWindow;

        infoWindow.close();
    }
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
