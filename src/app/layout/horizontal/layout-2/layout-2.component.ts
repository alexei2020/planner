import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';

@Component({
    selector: 'horizontal-layout-2',
    templateUrl: './layout-2.component.html',
    styleUrls: ['./layout-2.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class HorizontalLayout2Component implements OnInit, OnDestroy {
    fuseConfig: any;
    folded$: Observable<any> = new Observable<any>();
    opened = false;
    folded: any;
    
    hideToolbar:boolean=false;

    @ViewChild('contentContainer', {static: true})
    private contentContainer: ElementRef;
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseSidebarService} _fuseSidebarService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private router: Router
        ) {
        // Set the defaults

        // Set the private defaults
        this._unsubscribeAll = new Subject();

        this.router.events.subscribe((ev) => {
            this.hideToolbar = false;
            // if (ev instanceof NavigationEnd) { 
            //     alert('yes');
            // }
          });
    }

 
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    scroll:boolean=false;
    /**
     * On init
     */
    ngOnInit(): void {
        this.contentContainer.nativeElement.addEventListener('scroll', this.scrolling, true);

        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.fuseConfig = config;
            });

        this.folded$ = this._fuseSidebarService.navState;
                        
                       
        this._fuseSidebarService.navState
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((folded) => {
                this.folded = folded;   
            });
    }

    setStateFolded(event): void {
        // console.log(event);
        this._fuseSidebarService.foldedState.next(event);
    }

    scrolling = (s) => {  
        const isNotValidForScrollSrcRoomElement = s.srcElement && s.srcElement.offsetParent && 
            s.srcElement.offsetParent.offsetParent && s.srcElement.offsetParent.offsetParent.id === 'room-planner-select-floorspace';
        const isNotValidForScrollSrcElement = s.srcElement.classList.contains('tenant-list') ||
            s.srcElement.classList.contains('location-list') || isNotValidForScrollSrcRoomElement;
        if (isNotValidForScrollSrcElement) {
            return;
        }
        if (s.target.scrollTop > 0) {
            this.hideToolbar = true;
        } else {
            this.hideToolbar = false; 
        }
        // if (s.target.offsetHeight + s.target.scrollTop >= s.target.scrollHeight) {
        //     console.log("End");
        //   }

        // let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        // let max = document.documentElement.scrollHeight;
        // // pos/max will give you the distance between scroll bottom and and bottom of screen in percentage.
        // console.log(pos);
        // console.log(max);
         
        // if(pos == max )   {
        //  //Do your action here
        //  }else{
        //     alert('scroll'); 
        //  }
      }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(true);
        this._unsubscribeAll.unsubscribe();
    }
}
