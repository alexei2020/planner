import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class LayoutResolver implements Resolve<Observable<any>> {
    constructor(
        private _router: Router
    ) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Observable<any>> | Promise<Observable<any>> | Observable<any> {
        console.log(route);
        const splitUrl = route.url[0].path.split('=')[1].split('&');
        const layoutParam = splitUrl[0];
        console.log(splitUrl);
        console.log(layoutParam);
        this._router.navigate(['dashboard'], {
            queryParams: {
                layout: layoutParam
            },
            queryParamsHandling: 'merge'
        });
        return undefined;
    }
}
