import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FuseNavigationModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { NavbarHorizontalStyle2Component } from 'app/layout/components/navbar/horizontal/style-2/style-2.component';

@NgModule({
    declarations: [
        NavbarHorizontalStyle2Component
    ],
    imports     : [
        MatButtonModule,
        MatIconModule,

        FuseSharedModule,
        FuseNavigationModule
    ],
    exports     : [
        NavbarHorizontalStyle2Component
    ]
})
export class NavbarHorizontalStyle2Module
{
}
