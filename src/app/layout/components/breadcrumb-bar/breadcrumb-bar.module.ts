import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FuseBreadcrumbsModule, FuseShortcuts2Module } from '@fuse/components';
import { FuseMenu2Module } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { BreadcrumbBarComponent } from './breadcrumb-bar.component';



@NgModule({
    declarations: [BreadcrumbBarComponent],
    imports: [
        MatToolbarModule,
        MatButtonModule,
        MatIconModule,

        CommonModule,
        FuseSharedModule,
        FuseBreadcrumbsModule,
        FuseShortcuts2Module,
        FuseMenu2Module,
    ],
    exports: [BreadcrumbBarComponent],
})
export class BreadcrumbBarModule {}
