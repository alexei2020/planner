import { Component, OnDestroy, OnInit } from '@angular/core';

import { FuseConfigService } from '@fuse/services/config.service';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { BreadcrumbService } from '../../../main/shared/breadcrumb-bar.service';
import { PageType } from '../../../main/shared/breadcrumb-bar.service.gen';

@Component({
    selector: 'breadcrumb-bar',
    templateUrl: './breadcrumb-bar.component.html',
    styleUrls: ['./breadcrumb-bar.component.scss'],
})
export class BreadcrumbBarComponent implements OnInit, OnDestroy {
    fuseConfig: any;
    currentPage: PageType;
    canShowTopologyMenu = false;
    iconStyle: string;
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Creates an instance of BreadcrumbBarComponent.
     * @param {FuseConfigService} _fuseConfigService
     * @memberof BreadcrumbBarComponent
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _breadcrumbService: BreadcrumbService,
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();

    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._breadcrumbService.iconStyle.pipe(takeUntil(this._unsubscribeAll)).subscribe(res => {
            this.iconStyle = res;
        })
        this._breadcrumbService.canShowTopologyMenu.pipe(takeUntil(this._unsubscribeAll)).subscribe((res: boolean) => {
            this.canShowTopologyMenu = res;
        })
        // Subscribe to the dashboard changes
        this._breadcrumbService.currentPage.pipe(takeUntil(this._unsubscribeAll)).subscribe(page => {
            setTimeout(() => {
                this.currentPage = page;
            }, 10);
        })
        // this._breadcrumbService.dashboardLayouts.subscribe((layouts: UserSetting[]) => {

        // })
        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.fuseConfig = config;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
