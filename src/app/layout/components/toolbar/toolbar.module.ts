import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatBadgeModule } from '@angular/material/badge';

import { FuseSearchBarModule, FuseShortcutsModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { DialogModule } from '@progress/kendo-angular-dialog'
import { ToolbarComponent } from 'app/layout/components/toolbar/toolbar.component';
import { InputsModule } from '@progress/kendo-angular-inputs';

import 'hammerjs';
import { ComponentsModule } from 'app/components/module';

@NgModule({
    declarations: [
        ToolbarComponent
    ],
    imports: [
        DialogModule,
        RouterModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatBadgeModule,
        MatToolbarModule,
        MatTooltipModule,

        FuseSharedModule,
        FuseSearchBarModule,
        FuseShortcutsModule,

        InputsModule,
        
        ComponentsModule
    ],
    exports: [
        ToolbarComponent
    ]
})
export class ToolbarModule {
}
