import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, TemplateRef} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FuseConfigService} from '@fuse/services/config.service';
import {FuseSearchBarService} from '@fuse/services/search-bar.service';

import {TranslateService} from '@ngx-translate/core';

import * as _ from 'lodash';

import {Subject, Observable, interval, timer} from 'rxjs';
import {takeUntil, map, first, filter} from 'rxjs/operators';

import {FuseMenuType} from '@fuse/types';

import {format} from 'date-fns';
import {navigation} from 'app/navigation/navigation';
import {FuseLocationService} from '../../../services/location.service';
import {NavigationService} from '../../../services/navigation.service';
import {PlatformNotificationsService} from 'app/services/platform-notification/platform-notifications.service';
import {GenericObjCount, PlatformNotification} from 'app/services/platform-notification/platform-notification.model';
import {ActivatedRoute, Router} from '@angular/router';
import {SessionService} from '../../../services/session.service';
import { IScopeResponse } from 'app/typings/location';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ToolbarComponent implements OnInit, OnDestroy {
  // Remove it and move to template ng template part
  @ViewChild('notificationList') notificationList: TemplateRef<any>;

  displayLogo: boolean;
  horizontalNavbar: boolean;
  rightNavbar: boolean;
  hiddenNavbar: boolean;
  languages: any;
  navigation: any;
  selectedLanguage: any;
  userStatusOptions: any[];
  isFolded = false;
  isTemporarily = false;
  iconStyle = 'nav-fix';
  public openTimer$: Observable<number>;
  // location
  userName: string;
  userEmail: string;
  countPlatformNotificationsCount = 0;
  didSearchBarExpand: boolean;
  currentUserDate$;
  logoutOpened = false;

  notificationOverlayIsOpen = false;
  platformNotifications: PlatformNotification[] = [];
  private apigroup = '';

  private destroyed$ = new Subject();
  sessionOpened = false;

  isSearching: boolean;

  get countPlatformNotifications() {
    return this.platformNotifications.length > 99 ? 99 : this.platformNotifications.length;
  }

  // Private
  private _unsubscribeAll: Subject<any>;
  public _unsubscribeNotify: any;

  // TODO: improve general icons list
  private icons = [
    {name: 'nav-fix', path: 'assets/images/icons/nav-fix.svg'},
    {name: 'notification-critical', path: 'assets/icons/notifications/critical.svg'},
    {name: 'notification-debug', path: 'assets/icons/notifications/debug.svg'},
    {name: 'notification-emergency', path: 'assets/icons/notifications/emergency.svg'},
    {name: 'notification-error', path: 'assets/icons/notifications/error.svg'},
    {name: 'notification-info', path: 'assets/icons/notifications/info.svg'},
    {name: 'notification-notice', path: 'assets/icons/notifications/notice.svg'},
    {name: 'notification-warning', path: 'assets/icons/notifications/warning.svg'},
  ];

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FuseSidebarService} _fuseSidebarService
   * @param {NavigationService} _navService
   * @param {TranslateService} _translateService
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _fuseSidebarService: FuseSidebarService,
    private _navService: NavigationService,
    private _translateService: TranslateService,
    private _searchBarService: FuseSearchBarService,
    private _locationService: FuseLocationService,
    private _platformNotificationsService: PlatformNotificationsService,
    iconRegistry: MatIconRegistry,
    private activatedRoute: ActivatedRoute,
    sanitizer: DomSanitizer,
    private router: Router,
    private contextService: SessionService,
  ) {
    this.contextService.setSessionTimeout();
    this.icons.forEach(icon => {
      iconRegistry.addSvgIcon(icon.name, sanitizer.bypassSecurityTrustResourceUrl(icon.path));
    });

    iconRegistry.addSvgIcon('toolbar-user', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/toolbar/user.svg'));
    iconRegistry.addSvgIcon('toolbar-notification', sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/toolbar/notification.svg'));


    // Set the defaults
    this.userStatusOptions = [
      {
        title: 'Online',
        icon: 'icon-checkbox-marked-circle',
        color: '#4CAF50',
      },
      {
        title: 'Away',
        icon: 'icon-clock',
        color: '#FFC107',
      },
      {
        title: 'Do not Disturb',
        icon: 'icon-minus-circle',
        color: '#F44336',
      },
      {
        title: 'Invisible',
        icon: 'icon-checkbox-blank-circle-outline',
        color: '#BDBDBD',
      },
      {
        title: 'Offline',
        icon: 'icon-checkbox-blank-circle-outline',
        color: '#616161',
      },
    ];

    this.languages = [
      {
        id: 'en',
        title: 'English',
        flag: 'us',
      },
      {
        id: 'tr',
        title: 'Turkish',
        flag: 'tr',
      },
    ];

    this.navigation = navigation;

    // Set the private defaults
    this._unsubscribeAll = new Subject();


    this.currentUserDate$ = timer(0, 1000).pipe(map(() => format(new Date(), 'EEE, hh:mm bbb')));

    // this.loadNotificationList();
    // @ts-ignore
    //
    timer(2000).subscribe(x => {
      this._platformNotificationsService.createLocationWs();
      this._platformNotificationsService.locations.subscribe((locCountObj) => {
        this._platformNotificationsService.createAlertWs();
        this._platformNotificationsService.alerts.subscribe((alertCountObj: GenericObjCount) => {
          // @ts-ignore
          this.platformNotifications.push(alertCountObj);
          // tslint:disable-next-line:radix
          this.countPlatformNotificationsCount = parseInt(alertCountObj.Count);
        });
      });
    });
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this._locationService
      .getScope()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((res) => {
        this.userEmail = res?.spec?.userEmail;
        this.userName = res?.spec?.userName;
      });

    this._fuseSidebarService.foldedState.pipe(takeUntil(this._unsubscribeAll)).subscribe((state) => {
      this.isTemporarily = state;
    });

    // register sidebar

    // Subscribe to the search bar state changes
    this._searchBarService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((settings: any) => {
        this.didSearchBarExpand = settings.didExpand;
      });
    // Subscribe to the config changes
    this._fuseConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((settings: any) => {
        this.horizontalNavbar =
          settings.layout.navbar.position === 'top';
        this.displayLogo =
          settings.layout.navbar.position === 'top' ||
          settings.layout.style === 'horizontal-layout-2';
        this.rightNavbar = settings.layout.navbar.position === 'right';
        this.hiddenNavbar = settings.layout.navbar.hidden === true;
      });

    // Set the selected language from default languages
    this.selectedLanguage = _.find(this.languages, {
      id: this._translateService.currentLang,
    });

    // Watcher for toggling between menu views
    this._navService.menuTypeChanged.subscribe((menuType: FuseMenuType) => {
      this.foldedToggle();
    });

    this.contextService
      .sessionWarningTimer$
      .takeUntil(this.destroyed$)
      .subscribe(() => this.displaySessionWarning());
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    //this._unsubscribeNotify.Unsubscribe();
    this._unsubscribeAll.next(true);
    this._unsubscribeAll.complete();
    this.destroyed$.next();
  }

  displaySessionWarning(): void {
    this.sessionOpened = true;
  }

  onLogout(): void {
    this.sessionOpened = false;
    console.log('Logging out...');
    window.location.assign('/oauth2/sign_out');
    window.location.assign('/oauth2/start');
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  openMenu(): void {
    this._navService.toggleMenu();
  }


  sidebarOpenToggle(): void {
    if (!this._fuseSidebarService.getSidebar('navbar').folded) {
      this._fuseSidebarService.getSidebar('navbar').opened = !this._fuseSidebarService.getSidebar('navbar').opened;
    }
  }

  foldedToggle(): void {
    // this._fuseSidebarService.getSidebar('navbar').folded = !this._fuseSidebarService.getSidebar('navbar').folded;
    // this._fuseSidebarService.foldedState.next(this._fuseSidebarService.getSidebar('navbar').folded);
    this._navService.toggleMenu();
  }

  /**
   * Search
   *
   * @param value
   */
  search(value): void {
    // Do your search here...
  }

  /**
   * Set the language
   *
   * @param lang
   */
  setLanguage(lang): void {
    // Set the selected language for the toolbar
    this.selectedLanguage = lang;

    // Use the selected language for translations
    this._translateService.use(lang.id);
  }

  openNotificationOverlay() {
    this.notificationOverlayIsOpen = !this.notificationOverlayIsOpen;
  }

  closeNotificationOverlay() {
    this.notificationOverlayIsOpen = false;
  }

  removeNotification(notification) {
    // console.log('remove notification', notification);
  }

  private loadNotificationList() {
    timer(0, 3000)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(() => {
        this.activatedRoute.queryParams.subscribe(param => {
          this.apigroup = param['loc'];
        });
        // this._platformNotificationsService.loadList(this.apigroup).pipe(
        //     first(),
        //     map(response => response.items.filter(item => !item.spec.acknowledged)),
        //     map(item => item.map(item => item.status)),
        // ).subscribe(items => this.platformNotifications = items);
      });
  }

  public display_logout(): void {
    this.logoutOpened = true;
  }
  public onClose(): void {
    this.logoutOpened = false;
  }

  public onAccept(): void {
    this.logoutOpened = false;
    window.location.assign('/oauth2/sign_out');
    window.location.assign('/oauth2/start');
  }
  onSearch(evt){
    this.isSearching = evt;
  }
}
