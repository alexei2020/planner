import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatButtonModule } from '@angular/material/button';
import { FloatingNotificationComponent } from './floating-notification.component';



@NgModule({
    declarations: [FloatingNotificationComponent],
    imports: [
        MatCardModule,
        MatToolbarModule,
        MatIconModule,
        MatGridListModule,
        OverlayModule,
        MatButtonModule,

        CommonModule,
    ],
    exports: [FloatingNotificationComponent],
})
export class FloatingNotificationModule { }
