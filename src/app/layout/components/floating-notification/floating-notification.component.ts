import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FloatingNotificationModel } from '../../../main/shared/floating-notification.model';
import { FloatingNotificationService } from '../../../main/shared/floating-notification.service';

@Component({
    selector: 'floating-notification',
    templateUrl: './floating-notification.component.html',
    styleUrls: ['./floating-notification.component.scss'],
})
export class FloatingNotificationComponent implements OnDestroy {
    messages: FloatingNotificationModel[] = [];
    subscription: Subscription;
    isOpen: boolean = true;

    constructor(private floatingNotificationService: FloatingNotificationService) {
        setTimeout(function () {
            let menuButton = document.querySelector('.hamburger-button');
            if (menuButton) {
                menuButton.addEventListener("click", function () {
                    let arr: any = document.querySelector('.hamburger-button').classList;
                    if (arr.contains('opened')) {
                        let toggle: NodeListOf<Element> = document.querySelectorAll('.toggle-padding');
                        for (let i = 0; i < toggle.length; i++) {
                            toggle[i].classList.remove('padding-10')
                        }
                    }
                    else {
                        let toggle: NodeListOf<Element> = document.querySelectorAll('.toggle-padding');
                        for (let i = 0; i < toggle.length; i++) {
                            toggle[i].classList.add('padding-10')
                        }
                    }
                });
            }
        }, 1000);
        this.subscription = this.floatingNotificationService.getMessage().subscribe(message => {
            if (message) {
                this.messages.push(message);
            } else {
                // clear messages when empty message received
                this.messages = [];
            }
        });
    }

    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    }

    clearMessages(selNotify: FloatingNotificationModel): void {
        // clear message
        this.messages = this.messages.filter(item => item != selNotify);
    }
}
