// tslint:disable
/*
    Copyright (c) 2020 WaveSurf Systems Inc. All rights reserved.
    */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FileManagerModule } from '../../resource/file-manager/file-manager.module';
import { FileManagerPageRoutingModule } from './routing';
import { FileManagerPageComponent } from './component';
import { TreeListModule } from '@progress/kendo-angular-treelist';
import { TreeViewModule } from '@progress/kendo-angular-treeview';

@NgModule({
  imports: [CommonModule, FileManagerModule, FileManagerPageRoutingModule, TreeListModule, TreeViewModule],
  declarations: [FileManagerPageComponent],
  exports: [],
})
export class FileManagerPageModule {}
