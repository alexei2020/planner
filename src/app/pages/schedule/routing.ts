// tslint:disable
/*
Copyright (c) 2020 WaveSurf Systems Inc. All rights reserved.
*/
import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";
import { SchedulePageComponent } from "./component";
const LIST_ROUTE: Route = {
    path: "",
    component: SchedulePageComponent
};


@NgModule({
    imports: [
        RouterModule.forChild([LIST_ROUTE]),
    ],
    exports: [RouterModule],
})
export class SchedulePageRoutingModule {}
