// tslint:disable
/*
    Copyright (c) 2020 WaveSurf Systems Inc. All rights reserved.
    */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScheduleModule } from '../../resource/schedule/schedule.module';
import { SchedulePageRoutingModule } from './routing';
import { SchedulePageComponent } from './component';
import { TreeListModule } from '@progress/kendo-angular-treelist';
import { TreeViewModule } from '@progress/kendo-angular-treeview';

@NgModule({
  imports: [CommonModule, ScheduleModule, SchedulePageRoutingModule, TreeListModule, TreeViewModule],
  declarations: [SchedulePageComponent],
  exports: [],
})
export class SchedulePageModule {}
