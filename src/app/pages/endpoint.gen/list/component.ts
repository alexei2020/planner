
import { Component, EventEmitter, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import { CompactType, GridsterConfig, GridsterItem, GridsterItemComponent, GridsterPush, GridType, GridsterSwap } from 'angular-gridster2';

import { combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { v4 as uuid } from 'uuid';

import { BreadcrumbService, FormValidationInfo } from 'app/main/shared/breadcrumb-bar.service';
import { ReportMenuService } from '../../../main/shared/services/report-menu.service';
import { PageType } from 'app/main/shared/breadcrumb-bar.service.gen';
import { ItemType } from 'app/main/shared/services/report-menu.enum.gen';
import { MatDialog } from '@angular/material/dialog';
import { KendoReportItemDialog } from "app/main/shared/dialogs/kendo-report-item-dialog/kendo-report-item-dialog.component";
import { KendoDashboardItemDialog } from "app/main/shared/dialogs/kendo-dashboard-item-dialog/kendo-dashboard-item-dialog.component";
import { KendoObjectDialog } from "app/main/shared/dialogs/kendo-object-dialog/kendo-object-dialog.component";
import {
  DialogCloseResult,
  DialogService,
  DialogResult,
} from "@progress/kendo-angular-dialog";
import { kindToEndpoint } from 'app/navigation/objrefnav.gen';
import { FuseLocationService } from 'app/services/location.service';
import { EndpointSharedListComponent } from "../../../modules/endpoint-shared.gen/list/component"

@Component({
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class EndpointListPageComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild(EndpointSharedListComponent) endpointList: EndpointSharedListComponent;
    public options: GridsterConfig;
    public resizeEvent: EventEmitter<any> = new EventEmitter<any>();
    public queryParams: any;
    public resizeble: boolean;
    public prevTableHeight = 0;
    public wsFilter: string;

    public ItemType = ItemType;

    public tableList = [
        { id: uuid(), reportType: ItemType.ENDPOINT_LIST, x: 0, y: 0, rows: 38, cols: 96, compactEnabled: true, dragEnabled: false, resizeEnabled: false },
        { id: uuid(), reportType: "ITEM_ROW_FILLER", rows: 1.7, cols: 96, compactEnabled: true, dragEnabled: false, resizeEnabled: false },
    ];
    public list = [];

    public resizeTimer: any;
    public pageSize = { label: '5', value: 5 };
    public pageSizes = [{ label: '5', value: 5 }, { label: '10', value: 10 }, { label: '20', value: 20 }];

    public widget: any;
    public listOfCharts = [
        {
            id: uuid(),
            reportType: ItemType.CHART,
	    compactEnabled: true,
            dragEnabled: false,
            resizeEnabled: true,
            position: 'top',
            x: 0,
            y: 0,
            rows: 15,
            cols: 96,
            chartLink: 'assets/json/wsendpoint_list_chart1.json',
        },  
    ];
    public range = { label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 };
    public data: any;
    public isSelectedForReport: boolean;
    public location: string;
    public fixedLocation =null

    private _unsubscribeAll: Subject<any> = new Subject();
    private itemToPush: GridsterItemComponent;

    ngAfterViewInit() {
        setTimeout(() => this.loadSummaryCards());
    }

    loadSummaryCards() {
        if (!this.endpointList) {
            // We don't have the component loaded yet; lets try later
            setTimeout(() => this.loadSummaryCards());
            return;
        }
        if (this.endpointList.summaryCards.length) {
            this.tableList = [{ id: uuid(), reportType: "ITEM_SUMMARY_CARDS", rows: 8, cols: 96, compactEnabled: true, dragEnabled: false, resizeEnabled: false }, ...this.tableList];
            // If list already loaded, then we need to reload the list of gridster items
            if (this.list.length > 0) {
                this.list = [];
                setTimeout(() => this.loadChart());
            }
        }
    }

    constructor(
        private _breadcrumbService: BreadcrumbService,
        private _dialog: MatDialog,
        private _httpClient: HttpClient,
        private _reportMenuService: ReportMenuService,
        private _router: Router,
        private _dialogService: DialogService,
        private activatedRoute: ActivatedRoute,
        private _locationService: FuseLocationService
    ) {

        this._reportMenuService.getSelectedItemObs().pipe(takeUntil(this._unsubscribeAll)).subscribe(items => {
            this.isSelectedForReport = this._reportMenuService.itemIsChecked(ItemType.ENDPOINT_LIST);
        });

        this._breadcrumbService.currentPage.next(PageType.DEVICE_LIST);
        this._breadcrumbService.currentPageInfo.next({ detailsPage: false });

        this.options = {
            gridType: GridType.VerticalFixed,
            compactType: 'compactUp',
            margin: 5,
	    outerMarginTop: 0,
	    outerMarginBottom: 0,
            outerMarginLeft: 30,
            outerMarginRight: 18,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 6144,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            fixedRowHeight: 10,
            minItemArea: 1,
            mobileBreakpoint: 300,
            keepFixedHeightInMobile: false,
            scrollVertical: true,
            compactUp: false,
            draggable: {
                delayStart: 0,
                enabled: true,
                ignoreContentClass: 'gridster-item-content',
                ignoreContent: true,
                dragHandleClass: 'drag-handler',
                stop: ($event) => { }
            },
            resizable: {
                enabled: true,
            },
            swap: false,
            pushItems: true,
        };
    }

    ngOnInit(): void {
        this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(queryParams => {
	    if (!queryParams['loc'] || queryParams['loc'] === this.location) {
		  return;
	    }
      this.location = this._locationService.locationDataAsKeyValue().currentLocation;
	    if (this.fixedLocation) {
		  this.location = this.fixedLocation;
	    }
	    if (queryParams['wsFilter']) {
		this.wsFilter = queryParams['wsFilter'];
	    }
	});
        this.loadChart();
    }


    setHeightGridster(index, targetItem, tableHeight) {
        const previousRow: number = targetItem.rows;
	const emptyTableHeight = 120;
	tableHeight = Math.max(emptyTableHeight, tableHeight);
        const rows = Math.ceil((tableHeight + 50) / 15);
	if (this.options && this.options.api && this.options.api.getItemComponent) {
                const item = this.options.api.getItemComponent(targetItem);
                this.pushItem(item, previousRow, rows);
        }
    }

    pushItem(itemToPush, previousRow, currentRow) {
        const push = new GridsterPush(itemToPush); // init the service
        itemToPush.$item.rows = currentRow; // move/resize your item

        const listOfEnabledItems = this.list.filter(item => item.dragEnabled).map(item => item.id);
        this.list.forEach(item => {
            item.dragEnabled = true;
            item.resizeEnabled = true;
        });
        this.updateOptions();

        itemToPush.$item.rows = currentRow;

        if (push.pushItems(push.fromNorth) || push.pushItems(push.fromSouth)) { // push items from a direction
            push.checkPushBack(); // check for items can restore to original position
            push.setPushedItems(); // save the items pushed
            itemToPush.setSize();
            itemToPush.checkItemChanges(itemToPush.$item, itemToPush.item);
        } else {
            itemToPush.$item.rows = previousRow;
            push.restoreItems(); // restore to initial state the pushed items
        }
        this.list.forEach(record => {
            const enabled = listOfEnabledItems.some(id => record.id === id);
            record.dragEnabled = enabled;
            record.resizeEnabled = enabled;
        });
        this.updateOptions();

        push.destroy(); // destroy push instance
    }

    updateOptions() {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    changeGridsterState(widget: any, general = true) {
        if (general) {
            widget.resizeEnabled = !widget.resizeEnabled;
            widget.dragEnabled = !widget.dragEnabled;
        }
        this.updateOptions();
    }

    ngOnDestroy(): void {
        this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
    }

    public setForReport() {
        this._reportMenuService.setItemObs(ItemType.ENDPOINT_LIST);
    }

    onAddToReport(item: any) {
      const dialogRef = this._dialogService.open({
      title: "Show in reports",
      content: KendoReportItemDialog,
      width: "300px",
      height: "200px",
    });
    const dialogInstance = dialogRef.content.instance;
    dialogInstance.dialog = dialogRef;
    dialogInstance.data = {
      title: "Show in reports",
      item,
    };
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
  }

    onAddToDashboard(item: any) {
      const dialogRef = this._dialogService.open({
      title: "Show in dashboards",
      content: KendoDashboardItemDialog,
      width: "300px",
      height: "200px",
    });
    const dialogInstance = dialogRef.content.instance;
    dialogInstance.dialog = dialogRef;
    dialogInstance.data = {
      title: "Show in dashboards",
      item,
    };
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
  }

    customViewWidget(widget) {
        this._router.navigate(['endpoints', 'runtime-chart'], {
            queryParams: {
                tenant: this._locationService.locationDataAsKeyValue().tenantName,
                loc: this._locationService.locationDataAsKeyValue().locationName,
                chartLink: widget.chartLink 
            }
        });
    }

    private loadChart() {
        if (!this.listOfCharts.length) {
            this.list = this.tableList.slice(0);
            return;
        }

        combineLatest(
            this.listOfCharts.map((chart) => this._httpClient.get<any>(chart.chartLink).pipe(map((res) => ({
                ...res,
                ...chart,
            }))))
        ).pipe(takeUntil(this._unsubscribeAll)).subscribe((response: any[]) => {
            var colsTop = 0, colsBottom = 0;
            this.list = this.tableList.slice(0);
            response.forEach(chart => {
                if (chart.position === 'top') {
                    colsTop += chart.cols;
                    if (colsTop >= 96) {
                        colsTop = 0;
                        this.list.unshift({ id: uuid(), reportType: "ITEM_ROW_FILLER", 
                                rows: 1.7, cols: 96, compactEnabled: true, dragEnabled: false,
                                resizeEnabled: false });
                        this.list.unshift(chart);
                    } else {
                        this.list.unshift({id: uuid(), reportType: "ITEM_COL_FILLER", rows:chart.rows,
                                cols:2});
                        this.list.unshift(chart);
                    }
                } else {
                    colsBottom += chart.cols;
                    if (colsBottom >= 96) {
                        colsBottom = 0;
                        this.list.push(chart);
                        this.list.push({ id: uuid(), reportType: "ITEM_ROW_FILLER", 
                                rows: 1.7, cols: 96, compactEnabled: true, dragEnabled: false,
                                resizeEnabled: false });
                    } else {
                        this.list.push(chart);
                        this.list.push({id: uuid(), reportType: "ITEM_COL_FILLER", rows:chart.rows,
                                cols:2});
                    }
                }
            });
        });
    }
}
