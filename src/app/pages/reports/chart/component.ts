
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { Subject, Observable } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';

import { GridsterConfig, GridType, CompactType, GridsterItem } from 'angular-gridster2';

import { LayoutService } from '../../../main/shared/services';
import { Widget } from '../../../main/widget';
import { ReportMenuService } from 'app/main/shared/services/report-menu.service';
import { BreadcrumbService } from '../../../main/shared/breadcrumb-bar.service';
import { PageType } from '../../../main/shared/breadcrumb-bar.service.gen';
import {DialogCloseResult, DialogService} from '@progress/kendo-angular-dialog';
import {JsonDialogComponent} from '../../../main/shared/dialogs/json-dialog/json-dialog.component';

@Component({
    selector: 'report-chart-view',
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class ReportChartViewComponent implements OnInit, OnDestroy {
    public filter: string;
    public resetDataSource: any;
    public timer$: Observable<any>;
    public refreshTimer$: Observable<any>;
    public interval$: Observable<any>;
    public options: GridsterConfig;
    public item1: GridsterItem;
    public item2: GridsterItem;

    private _unsubscribeAll: Subject<any> = new Subject();
    timer: any;

    listOfCharts: any[];


    constructor(
        private location: Location,
        private layoutService: LayoutService,
        private activatedRoute: ActivatedRoute,
        private httpClient: HttpClient,
        private _breadcrumbService: BreadcrumbService,
        private _reportMenuService: ReportMenuService,
        private _dialogService: DialogService,
    ) {
        this.options = {
            gridType: GridType.ScrollVertical,
            compactType: CompactType.None,
            margin: 8,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1000,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            minItemArea: 1,
            resizable: {
                enabled: true
            },
            draggable: {
                enabled: true
            }
        };

        this.item1 = { cols: 96, rows: 35, y: 0, x: 0 };
        this.item2 = { cols: 96, rows: 46, y: 0, x: 0 };

        this.activatedRoute
            .queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((params) => {
                if (params && params['chartLink']) {
                    this.loadRuntimeChart(params['chartLink']);
                } else {
                    const chartId = this.activatedRoute.snapshot.params.resourceName;
                    const layoutName = this.activatedRoute.snapshot.params.dashboard;
                    if (chartId && layoutName) {
                        this._reportMenuService.getReports(params['loc']).pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
                            const layout = res.items.find((item: any) => item.metadata.name === layoutName).spec.layout;
                            const targetChart = JSON.parse(layout).find((item: any) => item.id ? item.id === chartId : null);
                            if (layout && targetChart) {
                                this.listOfCharts = [];
                                this.listOfCharts.push(targetChart);
                            }
                        });
                    }
                }
            });
        this._breadcrumbService.currentPage.next(PageType.DEFAULT);
    }

    ngOnInit(): void { }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    public onFilter(filter: string): void {
        this.filter = filter;
    }

    public jsonWidget(widget: Widget): void {
        this.layoutService.setSelectedChartObs(widget.id);
        // const options = this.layoutService.chartOptions;
        // const dialogRef = this._dialog.open(JsonDialogComponent, {
        //     width: '750px',
        //     data: options,
        //     panelClass: 'json-panel'
        // });
        // dialogRef.afterClosed().subscribe((res) => {
        //     if (res) {
        //         widget['chartOptions'] = res;
        //         this.layoutService.editItem(widget);
        //     }
        // });
        const dialogRef = this._dialogService.open({
            title: 'Chart JSON',

            // Show component
            content: JsonDialogComponent,
            width: '80%',
            height: '400px',
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.dialog = dialogRef;
        dialogInstance.widget = widget;

        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((result) => {
            console.log(result);
            if (result instanceof DialogCloseResult) {

            } else {
                // create widget object and emit it
                const widget = result as unknown as Widget;
                this.layoutService.editItem(widget);
            }
        });
    }
    public viewWidget(): void {
        this.location.back();
    }

    public filterChanged(event): void {
        this.filter = event;
    }

    public changeGridsterState(): void {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    private loadRuntimeChart(link): void {
        this.httpClient.get<any>(link).pipe(
            map(res => ({
                ...res, ...{ id: uuid(), dragEnabled: false, resizeEnabled: false, x: 0, y: 0, rows: 20, cols: 48 }
            })),
            takeUntil(this._unsubscribeAll)).subscribe((response: any[]) => {
                this.listOfCharts = [{ ...response, cols: 96, rows: 38, y: 0, x: 0 }];
            });
    }
}
