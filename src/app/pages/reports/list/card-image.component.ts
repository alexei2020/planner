import { Component, Output, EventEmitter, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { SelectEvent, FileInfo } from '@progress/kendo-angular-upload';

export interface ImageInfo {
    src: string;
    width: number;
    height: number;
}

@Component({
    selector: 'card-image',
    template: `
    <img style="max-height:100%" [style.width.px]="item.value?.width" [style.height.px]="item.value?.height" [src]="item.value?.src">
`
})
export class CardImageComponent implements OnInit, OnChanges {
    @Input() item: any;
    @Output() setRows: any = new EventEmitter();
    constructor() { }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.item?.currentValue) {
            this.setRows.emit(this.item.value.height);
        }
    }

    ngOnInit(): void { }
}
