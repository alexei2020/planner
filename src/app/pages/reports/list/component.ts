import { Component, EventEmitter, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { Subject, BehaviorSubject } from 'rxjs';
import { takeUntil, first } from 'rxjs/operators';

import { CompactType, GridsterItem, GridType, GridsterConfig, GridsterItemComponent, GridsterPush, GridsterComponent } from 'angular-gridster2';

import { v4 as uuid } from 'uuid';

import { format } from 'date-fns';

import jspdf from 'jspdf';
import html2canvas from 'html2canvas';

import { NotificationService } from '../../../main/shared/services';
import { Widget } from '../../../main/widget';
import { ReportMenuService } from 'app/main/shared/services/report-menu.service';
import { ItemType } from 'app/main/shared/services/report-menu.enum';
import { BreadcrumbService, EventType } from '../../../main/shared/breadcrumb-bar.service';
import { PageType } from '../../../main/shared/breadcrumb-bar.service.gen';
import { DialogComponent } from "./kendo-editor-upload-files/upload-dialog.component";
import { DialogLineComponent } from "./report-line/report-line.component";
import { KendoObjectDialog } from "app/main/shared/dialogs/kendo-object-dialog/kendo-object-dialog.component";
import { KendoConfirmDialog } from "app/main/shared/dialogs/kendo-confirm-dialog/kendo-confirm-dialog.component";
import { KendoReportItemDialog } from "app/main/shared/dialogs/kendo-report-item-dialog/kendo-report-item-dialog.component";
import { KendoDashboardItemDialog } from "app/main/shared/dialogs/kendo-dashboard-item-dialog/kendo-dashboard-item-dialog.component";
import { KendoDashboardDialog } from "app/main/shared/dialogs/kendo-dashboard-dialog/kendo-dashboard-dialog.component";
import {
  DialogCloseResult,
  DialogService,
  DialogResult,
} from "@progress/kendo-angular-dialog";
import {FuseLocationService} from '../../../services/location.service';

@Component({
    selector: 'report-page',
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class ReportsPageComponent implements OnInit, OnDestroy {
    @ViewChild('upload') public dialogUpload: DialogComponent;
    @ViewChild('line') public dialogLine: DialogLineComponent;
    @ViewChild('report') report: GridsterComponent;

    public hideGridster;
    public editMode = false;
    public editor$ = new BehaviorSubject(null);
    public line$ = new BehaviorSubject(null);
    public image$ = new BehaviorSubject(false);
    public indexItem: number;
    public editors = [];
    public listForPush = new Array<GridsterItemComponent>(1);
    public prevSelectedLayout: any[];
    public prevLocation: string;
    public ItemType = ItemType;
    public queryParams: any;
    public reportLayouts: any[];
    public reports: any[];
    public selectedLayout: any;
    public currentItem: any;
    public currentItemHeight: number;

    public options: GridsterConfig = {
        gridType: GridType.VerticalFixed,
        compactType: CompactType.None,
        minCols: 96,
        maxCols: 96,
        minRows: 100,
        maxRows: 3000,
        maxItemCols: 96,
        minItemCols: 1,
        minItemRows: 1,
        setGridSize: true,
        minItemArea: 1,
        fixedRowHeight: 1,
        disableScrollHorizontal: true,
        itemResizeCallback: () => setTimeout(() => this.gridsterStateChanged()),
        draggable: {
            delayStart: 0,
            enabled: false,
            ignoreContentClass: 'gridster-item-content',
            ignoreContent: true,
            dragHandleClass: 'drag-handler',
            stop: ($event) => { }
        },
        resizable: {
            enabled: true,
        },
        swap: false,
        pushItems: true,
    };

    public resizeEvent: EventEmitter<any> = new EventEmitter<any>();
    public data: any;
    public reportConfigurations: GridsterItem[] = [];

    private _unsubscribeAll: Subject<any> = new Subject();

    constructor(
        private _dialog: MatDialog,
        private _router: Router,
        private _route: ActivatedRoute,
        private _reportMenuService: ReportMenuService,
        private _breadcrumbService: BreadcrumbService,
        private _noticeService: NotificationService,
        private _dialogService: DialogService,
        private _ref: ElementRef,
        private _locationService: FuseLocationService,
    ) { }

    get layout(): Widget[] {
        return this.reportConfigurations;
    }

    ngOnInit(): void {
        this._breadcrumbService.editorEvent.pipe(takeUntil(this._unsubscribeAll)).subscribe((event) => {
            const editor = this.editors.find(item => item.id === this.currentItem.id);
            if (editor) {
                editor.editor?.exec(event);
            }
        });

        this._breadcrumbService.triggeredEvent.pipe(takeUntil(this._unsubscribeAll)).subscribe((event) => {
            switch (event.type) {
                case EventType.REPORT_ADD:
                    this.addItem();
                    break;

                case EventType.REPORT_ADD_TEXT_CONTROL:
                    this.addTextBox();
                    break;

                case EventType.REPORT_TO_PDF:
                    this.downloadPDF();
                    break;

                case EventType.REPORT_ADD_IMAGE_CONTROL:
                    this.openImageDialog(null, true, null);
                    break;

                case EventType.REPORT_ADD_LINE:
                    this.openLineDialog();
                    break;

                case EventType.REPORT_SAVE:
                    this.saveItem();
                    break;

                case EventType.REPORT_EDIT_ITEMS:
                    this.editMode = !this.editMode;
                    this.currentItem = null;
                    this.currentItemHeight = null;
                    this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ITEM_UNSAVE });
                    break;

                case EventType.REPORT_ADD_TIMER_CONTROL:
                    this.addTime();
                    break;


                case EventType.REPORT_CLONE:
                    this.cloneItem();
                    break;

                case EventType.REPORT_DELETE:
                    this.deleteItem();
                    break;

                case EventType.REPORT_ITEM_UNLOCK:
                    this.changeGridsterState(this.currentItem);
                    break;

                case EventType.REPORT_ITEM_REMOVE:
                    this.removeReport(this.currentItem, this.reportConfigurations.indexOf(this.currentItem));
                    break;

                case EventType.REPORT_ITEM_EDIT:
                    switch (this.currentItem.reportType) {
                        case ItemType.IMAGE_BOX:
                            this.openImageDialog(null, true, this.reportConfigurations.indexOf(this.currentItem))
                            break;

                        case ItemType.TEXT_BOX:
                            this.changeGridsterEditTexboxState(this.currentItem);
                            break;

                        case ItemType.LINE_BOX:
                            this.openLineDialog(this.reportConfigurations.indexOf(this.currentItem))
                            break;

                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
        });

        this._route.params.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
            this._breadcrumbService.currentPage.next(PageType.REPORT);
        });

        this._route.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
            this.queryParams = params;
            //get reports
            if (this.queryParams && this.queryParams.loc && this.prevLocation !== this.queryParams.loc) {
                this.prevLocation = this.queryParams.loc;
                this._reportMenuService.getReports(this.queryParams.loc).pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
                    this.reportLayouts = [];
                    setTimeout(() => {
                        const report = res.items;
                        this.reports = report;
                        this._breadcrumbService.item.next(this.reports);
                        report.map((item) => {
                            this.reportLayouts.push({
                                type: item.metadata.name,
                                name: item.metadata.name,
                                uid: item.metadata.uid,
                                layout: item.spec.layout
                            });
                        });
                        if (
                            this.queryParams === undefined ||
                            this.queryParams.layout === undefined
                        ) {
                            if (this.reportLayouts.length > 0) {
                                this._router.navigate([], {
                                    queryParams: { layout: this.reportLayouts[0].name }
                                });
                                this._breadcrumbService.queryParams.next({
                                    layout: this.reportLayouts[0].name
                                });
                                if (this.reportConfigurations.length === 0) {
                                    this.reportConfigurations = JSON.parse(this.reportLayouts[0].layout);
                                }
                            }

                        } else {
                            this.selectedLayout = this.reportLayouts.filter(
                                (item) => item.name === this.queryParams.layout
                            );
                            if (this.selectedLayout && this.selectedLayout.length > 0 && this.reportConfigurations.length === 0)
                                this.reportConfigurations = this.selectedLayout[0].layout ? JSON.parse(this.selectedLayout[0].layout) : [];
                            else {
                                if (this.reportConfigurations.length === 0) {
                                    this.reportConfigurations = this.reportLayouts[0].layout ? JSON.parse(this.reportLayouts[0].layout) : [];
                                }
                                this._router.navigate([], {
                                    queryParams: {
                                        layout: this.reportLayouts[0].name
                                    }
                                });
                                this._breadcrumbService.queryParams.next({
                                    layout: this.reportLayouts[0].name
                                });
                            }
                        }
                        this.reportConfigurations.forEach((widget, index) => {
                            widget.resizeEnabled = false;
                            widget.dragEnabled = false;
                        });
                        this._breadcrumbService.layouts.next(this.reportLayouts);
                    }, 10);
                }
                    ,
                    () => {
                        this.reportLayouts = [];
                        this._breadcrumbService.layouts.next(this.reportLayouts);
                        this.reports = [];
                        this._breadcrumbService.item.next(this.reports);
                        this.reportConfigurations = [];
                        this._noticeService.openSnackBar('Error loading report', '');
                    });
            }

            if (
                (this.queryParams === undefined ||
                    this.queryParams.layout === undefined) &&
                (this.reportLayouts !== undefined && this.reportLayouts.length > 0)
            ) {
                this._router.navigate([], {
                    queryParams: { layout: this.reportLayouts[0].name }
                });
                this._breadcrumbService.queryParams.next({
                    layout: this.reportLayouts[0].name
                });
                this.reportConfigurations = JSON.parse(this.reportLayouts[0].layout);
            } else {
                if (this.reportLayouts && this.reportLayouts.length > 0) {
                    this.selectedLayout = this.reportLayouts.filter(
                        (item) => item.name === this.queryParams.layout
                    );
                    if (this.prevSelectedLayout) {
                        if (this.prevSelectedLayout[0].uid !== this.selectedLayout[0].uid) {
                            this.reportConfigurations = this.selectedLayout[0].layout ? JSON.parse(this.selectedLayout[0].layout) : [];
                        }
                    } else {
                        this.reportConfigurations = this.selectedLayout[0].layout ? JSON.parse(this.selectedLayout[0].layout) : [];
                    }
                    this.prevSelectedLayout = this.selectedLayout;
                }
                setTimeout(() => {
                    this._breadcrumbService.queryParams.next(params);
                }, 10);
            }
        });
    }

    setItemMenu(item: any, event?: any, itemHeight?: any) {
        if (this.editMode) {
            if (item) {
                if (itemHeight) this.currentItemHeight = item.dragEnabled ? itemHeight - 41 : itemHeight;
                this.currentItem = item;
                this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ITEM_INIT });
                this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ITEM_INIT });
                if (item.reportType === ItemType.LINE_BOX || item.reportType === ItemType.IMAGE_BOX || item.reportType === ItemType.TEXT_BOX) {
                    this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ITEM_IS_EDIT });
                    this._breadcrumbService.isEditStateItem.next(item.editable ? true : false);
                } else { this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ITEM_NO_EDIT }); }
            } else {
                this.currentItem = null;
                this.currentItemHeight = null;
                this._breadcrumbService.triggeredEvent.next({ type: EventType.REPORT_ITEM_UNSAVE });
            }
        }
        if (event) {
            event.stopPropagation();
        }
    }

    openImageDialog(editor?: any, image?: any, index?: number) {
        editor ? this.editor$.next(editor) : this.editor$.next(null);
        image ? this.image$.next(true) : this.image$.next(false);
        this.indexItem = !!index ? index : null;
        this.dialogUpload.open();
    }

    openLineDialog(index?: number) {
        this.indexItem = !!index ? index : null;
        this.line$.next(this.reportConfigurations[index]);
        this.dialogLine.open();
    }

    saveEditor(editor: any) {
        this.editors.push(editor);
    }

    changeGridsterChartState() {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    changeGridsterState(widget?: any) {
        if (widget) {
            widget.resizeEnabled = !widget.resizeEnabled;
            widget.dragEnabled = !widget.dragEnabled;
            if ((widget.reportType === ItemType.TIME_BOX ||
                widget.reportType === ItemType.TEXT_BOX ||
                widget.reportType === ItemType.LINE_BOX ||
                widget.reportType === ItemType.IMAGE_BOX) && !widget.editable) {
                const heightItem = widget.dragEnabled ? this.currentItemHeight + 41 : this.currentItemHeight;
                this.setHeightGridster(this.reportConfigurations.indexOf(widget), widget, heightItem)
            }
        }
        this.gridsterStateChanged();
    }
    public gridsterStateChanged() {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    changeGridsterEditTexboxState(widget?: any) {
        if (widget) {
            widget.editable = !widget.editable;
            this._breadcrumbService.isEditStateItem.next(widget.editable ? true : false);
            const heightItem = widget.editable ? this.currentItemHeight + 70 : this.currentItemHeight;
            this.setHeightGridster(this.reportConfigurations.indexOf(widget), widget, heightItem)
        }
        this.gridsterStateChanged();
    }

    setImage(index: number, img: any) {
        if (index) {
            this.reportConfigurations[index].value = img;
        } else this.addImageBox(img);
    }

    setLine(index: number, line: any) {
        if (index) {
            this.reportConfigurations[index] = {
                ...this.reportConfigurations[index],
                cols: line.type === 'horizontal' ? 96 : 2,
                rows: line.type === 'vertical' ? 96 : 2,
                value: line,
            };
            this.gridsterStateChanged();
        } else this.addLineBox(line);

    }

    setHeightGridster(index, targetItem, tableHeight) {
        const previousRow: number = targetItem.rows;
        const rows = Math.ceil((tableHeight + tableHeight * 0.4) / 15);
        if (this.options && this.options.api && this.options.api.getItemComponent) {
            const item = this.options.api.getItemComponent(targetItem);
            this.pushItem(item, previousRow, rows);
        }
    }

    pushItem(itemToPush, previousRow, currentRow) {
        const push = new GridsterPush(itemToPush); // init the service
        itemToPush.$item.rows = currentRow; // move/resize your item

        const listOfEnabledItems = this.reportConfigurations.filter(item => item.dragEnabled).map(item => item.id);
        this.reportConfigurations.forEach(item => {
            item.dragEnabled = true;
            item.resizeEnabled = true;
        });
        this.gridsterStateChanged();

        itemToPush.$item.rows = currentRow;

        if (push.pushItems(push.fromNorth) || push.pushItems(push.fromSouth)) { // push items from a direction
            push.checkPushBack(); // check for items can restore to original position
            push.setPushedItems(); // save the items pushed
            itemToPush.setSize();
            itemToPush.checkItemChanges(itemToPush.$item, itemToPush.item);
        } else {
            itemToPush.$item.rows = previousRow;
            push.restoreItems(); // restore to initial state the pushed items
        }
        this.reportConfigurations.forEach(record => {
            const enabled = listOfEnabledItems.some(id => record.id === id);
            record.dragEnabled = enabled;
            record.resizeEnabled = enabled;
        });
        this.gridsterStateChanged();

        push.destroy(); // destroy push instance
    }

    updateLayout(layout) {
        this._router.navigate([], { queryParams: { layout: layout.name } });
    }

  removeReport(item: any, index) {
    const dialogRef = this._dialogService.open({
      title: "Remove Widget",
      content: KendoConfirmDialog,
      width: "420px",
      height: "200px",
    });
    const dialogInstance = dialogRef.content.instance;
    dialogInstance.message = "Are you sure you want to remove widget?";
    dialogInstance.dialog = dialogRef;
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
      if (response instanceof DialogCloseResult || response.text == "No") {
      } else {
        if (item.reportType === ItemType.TEXT_BOX) {
          this.editors = this.editors.filter((el) => el.id !== item.id);
        }
        this.reportConfigurations.splice(index, 1);
      }
    });
  }


  private addItem() {
    const dialogRef = this._dialogService.open({
      title: "Report Name",
      content: KendoDashboardDialog,
      width: "300px",
      height: "200px",
    });
    const dialogInstance = dialogRef.content.instance;
    dialogInstance.dialog = dialogRef;
    dialogInstance.name = "";

    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});

    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
      if (response instanceof DialogCloseResult || response.text == "No") {
      } else {
        let newItem = {
          apiVersion: `${this._locationService.locationDataAsKeyValue().currentLocation}/v1`,
          kind: "Report",
          metadata: {
            name: dialogInstance.form.value.name,
          },
          spec: { layout: `[]` },
        };
        this._reportMenuService
          .addReports(newItem, this.queryParams.loc)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(
            (report: any) => {
              this._noticeService.openSnackBar("Report add successfully", "");
              const layout = {
                type: report.metadata.name,
                name: report.metadata.name,
                layout: report.spec.layout,
                uid: report.metadata.uid,
              };
              this.reports.push(report);
              this.reportLayouts.push(layout);
              this._breadcrumbService.layouts.next(this.reportLayouts);
              this._breadcrumbService.item.next(this.reports);
              this.updateLayout(layout);
            },
            () => {
              this._noticeService.openSnackBar(`Failed to added report`, ``);
            }
          );
      }
    });
  }

    private addTextBox() {
        const textBox = {
            id: uuid(),
            reportType: ItemType.TEXT_BOX,
            cols: 96, rows: 20, y: 0, x: 0,
            value: '',
            editable: true,
            compactEnabled: true, dragEnabled: false, resizeEnabled: false,
        };
        this.reportConfigurations.push(textBox);
        this.setItemMenu(textBox);
    }

    private addImageBox(img: any) {
        const imageBox = {
            id: uuid(),
            reportType: ItemType.IMAGE_BOX,
            cols: 96, rows: 20, y: 0, x: 0,
            value: img,
            editable: false,
            compactEnabled: true, dragEnabled: false, resizeEnabled: false,
        };
        this.reportConfigurations.push(imageBox);
    }

    private addLineBox(line: any) {
        const lineBox = {
            id: uuid(),
            reportType: ItemType.LINE_BOX,
            cols: line.type === 'horizontal' ? 96 : 2, rows: line.type === 'vertical' ? 96 : 2, y: 0, x: 0,
            value: line,
            editable: false,
            compactEnabled: true, dragEnabled: false, resizeEnabled: false,
        };
        this.reportConfigurations.push(lineBox);
    }

    private addTime() {
        const timeBox = {
            id: uuid(),
            reportType: ItemType.TIME_BOX,
            cols: 24, rows: 4, y: 0, x: 0,
            value: format(new Date(), 'd.m.yy, hh:mm bbb'),
            editable: false,
            compactEnabled: true, dragEnabled: false, resizeEnabled: false,
        };
        this.reportConfigurations.push(timeBox);
    }

    private saveItem() {
        const layout = this.reportConfigurations;
        const selectedUserItem = this.reports.filter(
            (item) => item.metadata.name === this.selectedLayout[0].type
        )[0];
        selectedUserItem.spec.layout = JSON.stringify(layout);
        this._reportMenuService.saveReport(selectedUserItem.metadata.name, selectedUserItem, this.queryParams['loc'])
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                (res: any) => {
                    let objIndex = this.reports.findIndex((item) => item.metadata.name === this.selectedLayout[0].type);
                    this.reports[objIndex] = res;
                    this.reportLayouts[objIndex] = {
                        type: res.metadata.name,
                        name: res.metadata.name,
                        layout: res.spec.layout
                    };
                    this._breadcrumbService.layouts.next(this.reportLayouts);
                    this._breadcrumbService.item.next(this.reports);
                    this.updateLayout(this.selectedLayout[0]);
                    this._noticeService.openSnackBar('Report saved successfully', '');
                },
                () => {
                    this._noticeService.openSnackBar('Failed to saved report', '');
                }
            );
    }

    private cloneItem() {
      const dialogRef = this._dialogService.open({
        title: "Clone",
        content: KendoDashboardDialog,
        width: "300px",
        height: "200px",
      });
      const dialogInstance = dialogRef.content.instance;
      dialogInstance.dialog = dialogRef;
      dialogInstance.name = "Copy of " + this.selectedLayout[0].name;
      dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
      if (response instanceof DialogCloseResult || response.text == "Cancel") {
      } else {
                let selectedUserDashboard = this.reports.find((item) => item.metadata.name === this.selectedLayout[0].type);

                if (selectedUserDashboard) {
                    selectedUserDashboard = {
                        ...selectedUserDashboard,
                        metadata: {
                            name: response,
                        },
                        spec: {
                            layout: JSON.stringify(this.reportConfigurations)
                        }
                    }

                    this._reportMenuService.cloneReport(selectedUserDashboard, this.queryParams['loc'])
                        .pipe(takeUntil(this._unsubscribeAll))
                        .subscribe(
                            (report: any) => {
                                // Add regexp
                                /// [a-z0-9]([-a-z0-9][a-z0-9])?(.[a-z0-9]([-a-z0-9][a-z0-9])?)*
                                const layout = {
                                    type: report.metadata.name,
                                    name: report.metadata.name,
                                    layout: report.spec.layout,
                                    uid: report.metadata.uid
                                }
                                this.reportLayouts.push(layout);
                                this.reports.push(report);
                                this._noticeService.openSnackBar('Report saved successfully', '');
                                this._breadcrumbService.layouts.next(this.reportLayouts);
                                this._breadcrumbService.item.next(this.reports);
                                this.updateLayout(layout);
                            },
                            () => {
                                this._noticeService.openSnackBar('Failed to saved report', '');
                            }
                        );
                }
            }
        });
    }

    private deleteItem() {
	const dialogRef = this._dialogService.open({
          title: "Delete",
          content: KendoConfirmDialog,
          width: "420px",
          height: "200px",
        });
        const dialogInstance = dialogRef.content.instance;
        dialogInstance.message = "Are you sure to delete the current report?";
        dialogInstance.dialog = dialogRef;

        dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
        if (response instanceof DialogCloseResult || response.text == "No") {
        } else {
                this._reportMenuService.deleteReport(this.selectedLayout[0].name, this.queryParams['loc'])
                    .pipe(first())
                    .subscribe(
                        () => {
                            this.reportConfigurations = [];
                            this._noticeService.openSnackBar('Report removed successfully', '');
                            this.reports = this.reports.filter(item => item.metadata.uid !== this.selectedLayout[0].uid)
                            this._breadcrumbService.item.next(this.reports);
                            this.reportLayouts = this.reportLayouts.filter(item => item.uid !== this.selectedLayout[0].uid)
                            this._breadcrumbService.layouts.next(this.reportLayouts);
                            this._router.navigate([], { queryParams: {} });
                        },
                        () => {
                            this._noticeService.openSnackBar('Failed to removed report', '');
                        }
                    );
            }
        });
    }

    public viewChartWidget(record) {
        this._router.navigate(['reports', 'chart', this.selectedLayout[0].name, record.id], { queryParamsHandling: 'preserve' });
    }

    public downloadPDF(): void {
        html2canvas(this._ref.nativeElement.getElementsByTagName('gridster')[0]).then(canvas => {
            let imgData = canvas.toDataURL('image/png');

            let imgWidth = 210,
                pageHeight = 295,
                imgHeight = canvas.height * imgWidth / canvas.width,
                heightLeft = imgHeight,
                doc = new jspdf('p', 'mm'),
                position = 0;

            doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
            heightLeft -= pageHeight;

            while (heightLeft >= 0) {
                position = heightLeft - imgHeight;
                doc.addPage();
                doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;
            }
            doc.save('report.pdf');

            this.hideGridster = true;

            setTimeout(() => {
                this.hideGridster = false;
            }, 500);
        });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
