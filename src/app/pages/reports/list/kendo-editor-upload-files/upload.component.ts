import { Component, Output, EventEmitter } from '@angular/core';
import { SelectEvent, FileInfo } from '@progress/kendo-angular-upload';

export interface ImageInfo {
    src: string;
    width: number;
    height: number;
}

@Component({
    selector: 'my-upload',
    template: `
        <kendo-upload
            [saveUrl]="uploadSaveUrl"
            [removeUrl]="uploadRemoveUrl"
            (select)="onSelect($event)"
            (remove)="onRemove()"
            (success)="completeEventHandler($event)"
            [multiple]="false">
            <kendo-upload-messages select="Select image">
            </kendo-upload-messages>
        </kendo-upload>
`
})
export class UploadComponent {
    public uploadSaveUrl = 'saveUrl'; // Has to represent an actual API endpoint.
    public uploadRemoveUrl = 'removeUrl'; // Has to represent an actual API endpoint.
    public img = new Image();

    @Output() public valueChange: EventEmitter<ImageInfo> = new EventEmitter<ImageInfo>();

    public onSelect(ev: SelectEvent): void {
        ev.files.forEach((file: FileInfo) => {
            if (file.rawFile) {
                const reader = new FileReader();

                reader.onloadend = () => {
                    this.img.src = <string>reader.result;
                    this.img.onload = () => {
                        this.valueChange.emit({
                            src: this.img.src,
                            height: this.img.height,
                            width: this.img.width
                        });
                    };
                };

                reader.readAsDataURL(file.rawFile);
            }
        });
    }

    public completeEventHandler(event) {
        this.valueChange.emit({
            src: event.response.body.imageUrl,
            height: this.img.height,
            width: this.img.height
        });
    }

    public onRemove(): void {
        this.valueChange.emit(null);
    }
}
