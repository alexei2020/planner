import { Directive, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[initEditor]'
})
export class InitEditorDirective implements OnChanges {
@Input('initEditor') editor: any;
@Input('id') id: any;
@Output() setEditor: EventEmitter<any> = new EventEmitter();
  constructor( ) { }
    ngOnChanges(changes: SimpleChanges): void {
        console.log(changes)
        if(changes.editor?.currentValue && changes.id?.currentValue) {
            this.setEditor.emit({id: changes.id?.currentValue, editor: changes.editor?.currentValue});
        }
    }
}
