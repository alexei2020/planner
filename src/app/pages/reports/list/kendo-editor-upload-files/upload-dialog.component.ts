import { Component, Input, Injectable, Output, EventEmitter } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpProgressEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable, of, concat } from 'rxjs';
import { EditorComponent } from '@progress/kendo-angular-editor';
import { ImageInfo } from './upload.component';
import { delay } from 'rxjs/operators';

@Component({
    selector: 'upload-dialog',
    styles: [`
        my-upload {
            min-width: 175px;
            max-width: 275px;
        }

        label {
            width: 100px;
        }
    `],
    template: `
    <kendo-dialog (keydown)="onKeyDown($event)" title="Insert Image" *ngIf="opened" (close)="close()" [minWidth]="250" [width]="450">
        <div class="row example-wrapper">
            <div class="col-xs-8 col-sm-12 example-col">
                <div class="card">
                    <div class="card-block">
                        <form class="k-form-inline">
                            <div class="k-form-field">
                                <label>Image</label>
                                <my-upload (valueChange)="setImageInfo($event)">
                                </my-upload>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <kendo-dialog-actions>
            <button kendoButton (click)="close()">Cancel</button>
            <button kendoButton [disabled]="canInsert" (click)="uploadImage()" primary="true">Insert</button>
        </kendo-dialog-actions>
    </kendo-dialog>
`
})

@Injectable()
export class DialogComponent implements HttpInterceptor {
    @Input() public editor: EditorComponent;
    @Input() public emitImage: boolean;

    @Output() public getImage = new EventEmitter();

    public opened = false;
    public src: string;
    public height: number;
    public width: number;

    public get canInsert(): boolean {
        return !this.src;
    }

    public uploadImage(): void {
        if (this.editor) {
            this.editor.exec('insertImage', this.imageInfo);
        }
        if (this.emitImage) {
            this.getImage.emit(this.imageInfo);
        }
        this.close();
    }

    onKeyDown(pressedKey) {
        if (pressedKey.key ==="Enter" && !this.canInsert) {
            this.uploadImage();
          }
    }

    public get imageInfo(): ImageInfo {
        return {
            src: this.src,
            height: this.height,
            width: this.width
        };
    }

    public setImageInfo(value: ImageInfo) {
        if (value) {
            this.src = value.src;
            this.height = value.height;
            this.width = value.width;
        } else {
            this.resetData();
        }
    }

    public open(): void {
        this.opened = true;
    }

    public close(): void {
        this.opened = false;
        this.resetData();
    }

    public resetData(): void {
        this.src = null;
        this.width = null;
        this.height = null;
    }

    //    fake backend api
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url === 'saveUrl') {
            const events: Observable<HttpEvent<any>>[] = [0, 30, 60, 100].map((x) => of(<HttpProgressEvent>{
                type: HttpEventType.UploadProgress,
                loaded: x,
                total: 100
            }).pipe(delay(1000)));

            const success = of(new HttpResponse({ status: 200, body: { imageUrl: 'assets/images/logos/fuse.svg' } })).pipe(delay(1000));
            events.push(success);
            return concat(...events);
        }

        if (req.url === 'removeUrl') {
            return of(new HttpResponse({ status: 200 }));
        }

        return next.handle(req);
    }
}
