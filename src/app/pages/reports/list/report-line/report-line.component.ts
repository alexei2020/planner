import { Component, Input, Injectable, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

interface OptionModel {
    type: string;
    color: string;
}

@Component({
    selector: 'report-line',
    styles: [`
        report-line {
            min-width: 175px;
            max-width: 275px;
        }

        label {
            width: 100px;
            cursor: pointer;
        }
    `],
    template: `
    <kendo-dialog (keydown)="onKeyDown($event)" title="Select line type" *ngIf="opened" (close)="close()" [minWidth]="250" [width]="450">
        <div class="row example-wrapper">
            <div class="col-xs-8 col-sm-12 example-col">
                <div class="card">
                    <div class="card-block">
                        <form class="k-form-inline" [formGroup]="lineForm">
                            <div class="k-form-field">
                                <label class="k-label" [for]="lhorizontal">Horizontal</label>
                                <input class="mr-16" #lhorizontal type="radio" name="type" value="horizontal" formControlName="type" kendoRadioButton />
                                <label class="k-label" [for]="lvertical">Vertical</label>
                                <input #lvertical  type="radio" name="type" value="vertical" formControlName="type" kendoRadioButton />
                            </div>
                            <div class="k-form-field">
                                <kendo-colorpicker
                                    [formControl]="color"
                                    [view]="'gradient'"
                                >
                                </kendo-colorpicker>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <kendo-dialog-actions>
            <button kendoButton (click)="close()">Cancel</button>
            <button kendoButton [disabled]="!lineForm?.valid" (click)="setLine()" primary="true">Insert</button>
        </kendo-dialog-actions>
    </kendo-dialog>
`
})

@Injectable()
export class DialogLineComponent implements OnChanges {
    @Input() public line: OptionModel;

    @Output() public emitLine = new EventEmitter();

    public lineForm: FormGroup;
    public color: FormControl;

    public opened = false;

    constructor(formBuilder: FormBuilder) {
        this.color = formBuilder.control('', [Validators.required]);
        this.lineForm = formBuilder.group({
            type: new FormControl('', Validators.required),
            color: this.color
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.line?.currentValue) {
            this.lineForm.patchValue(changes.line?.currentValue.value, { emitEvent: false });
        }
    }

    setLine(): void {
        this.emitLine.emit(this.lineForm.value);
        this.close();
    }

    open(): void {
        this.opened = true;
    }

    onKeyDown(pressedKey) {
        if (pressedKey.key ==="Enter" && this.lineForm?.valid) {
            this.setLine();
          }
    }

    close(): void {
        this.opened = false;
    }
}
