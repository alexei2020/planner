


import { Component, EventEmitter, OnInit } from '@angular/core';
import { CompactType, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { ActivatedRoute } from '@angular/router';
import { ReportMenuService } from 'app/main/shared/services/report-menu.service';
import { ItemType } from 'app/main/shared/services/report-menu.enum';
import { combineLatest, Subject } from 'rxjs';
import { NotificationService } from 'app/main/shared/services';
import { takeUntil } from 'rxjs/operators';
import { KendoObjectDialog } from "app/main/shared/dialogs/kendo-object-dialog/kendo-object-dialog.component";
import { KendoConfirmDialog } from "app/main/shared/dialogs/kendo-confirm-dialog/kendo-confirm-dialog.component";
import { KendoReportItemDialog } from "app/main/shared/dialogs/kendo-report-item-dialog/kendo-report-item-dialog.component";
import { KendoDashboardItemDialog } from "app/main/shared/dialogs/kendo-dashboard-item-dialog/kendo-dashboard-item-dialog.component";
import {
  DialogCloseResult,
  DialogService,
  DialogResult,
} from "@progress/kendo-angular-dialog";

@Component({
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class ReportsDetailPageComponent implements OnInit {
    public pageName: string;
    public resourceName: string;
    public options: GridsterConfig;
    public resizeEvent: EventEmitter<any> = new EventEmitter<any>();
    public data: any;
    public item1: GridsterItem;
    public item2: GridsterItem;
    public item3: GridsterItem;
    public item4: GridsterItem;
    public item5: GridsterItem;
    public item6: GridsterItem;
    public item7: GridsterItem;
    public item8: GridsterItem;
    public item9: GridsterItem;
    public item10: GridsterItem;
    public item11: GridsterItem;
    public item12: GridsterItem;
    public item13: GridsterItem;
    public item14: GridsterItem;
    public item15: GridsterItem;
    public item16: GridsterItem;
    public item17: GridsterItem;
    public item18: GridsterItem;
    public item19: GridsterItem;
    public item20: GridsterItem;
    public item21: GridsterItem;
    public item22: GridsterItem;
    public item23: GridsterItem;
    public item24: GridsterItem;
    public item25: GridsterItem;
    public item26: GridsterItem;
    public item27: GridsterItem;
    public item28: GridsterItem;
    public item29: GridsterItem;
    public item30: GridsterItem;
    public item31: GridsterItem;
    public item32: GridsterItem;
    public item33: GridsterItem;
    public item34: GridsterItem;
    public item35: GridsterItem;
    public item36: GridsterItem;
    public item37: GridsterItem;
    public item38: GridsterItem;
    public item39: GridsterItem;
    public item40: GridsterItem;
    public item41: GridsterItem;
    public item42: GridsterItem;
    public item43: GridsterItem;
    public item44: GridsterItem;
    public item45: GridsterItem;
    public item46: GridsterItem;
    public item47: GridsterItem;
    public item48: GridsterItem;
    public item49: GridsterItem;
    public item50: GridsterItem;
    public item51: GridsterItem;
    public item52: GridsterItem;
    public item53: GridsterItem;
    public item54: GridsterItem;
    public item55: GridsterItem;
    public item56: GridsterItem;
    public item57: GridsterItem;
    public item58: GridsterItem;
    public item59: GridsterItem;
    public item60: GridsterItem;
    public item61: GridsterItem;
    public item62: GridsterItem;
    public item63: GridsterItem;
    public item64: GridsterItem;
    public item65: GridsterItem;
    public item66: GridsterItem;
    public item67: GridsterItem;
    public item68: GridsterItem;
    public item69: GridsterItem;
    public item70: GridsterItem;
    public item71: GridsterItem;
    public item72: GridsterItem;
    public item73: GridsterItem;
    public item74: GridsterItem;
    public item75: GridsterItem;
    public item76: GridsterItem;
    public item77: GridsterItem;
    public item78: GridsterItem;
    public item79: GridsterItem;
    public item80: GridsterItem;
    public item81: GridsterItem;
    public item82: GridsterItem;
    public item83: GridsterItem;
    public item84: GridsterItem;
    public item85: GridsterItem;
    public item86: GridsterItem;
    public item87: GridsterItem;
    public item88: GridsterItem;
    public item89: GridsterItem;
    public item90: GridsterItem;
    public item91: GridsterItem;
    public item92: GridsterItem;
    public item93: GridsterItem;
    public item94: GridsterItem;
    public item95: GridsterItem;
    public item96: GridsterItem;
    public item97: GridsterItem;
    public item98: GridsterItem;
    public item99: GridsterItem;
    public item100: GridsterItem;
    public item101: GridsterItem;
    public item102: GridsterItem;
    public item103: GridsterItem;
    public item104: GridsterItem;
    public item105: GridsterItem;
    public item106: GridsterItem;
    public item107: GridsterItem;
    public item108: GridsterItem;
    public item109: GridsterItem;
    public item110: GridsterItem;
    public item111: GridsterItem;
    public item112: GridsterItem;
    public item113: GridsterItem;
    public item114: GridsterItem;
    public item115: GridsterItem;
    public item116: GridsterItem;
    public item117: GridsterItem;
    public item118: GridsterItem;
    public item119: GridsterItem;
    public item120: GridsterItem;
    public item121: GridsterItem;
    public item122: GridsterItem;
    public item123: GridsterItem;
    public item124: GridsterItem;
    public item125: GridsterItem;
    public item126: GridsterItem;

    private _unsubscribeAll: Subject<any> = new Subject();

    public reportConfigurations: GridsterItem[] = [];
    
    constructor(
        private activatedRoute: ActivatedRoute,
	private _dialogService: DialogService
    ) {
        this.options = {
            swapping: false,
            itemResizeCallback: (item) => {
                this.resizeEvent.emit(item);
            },
            gridType: GridType.ScrollVertical,
            compactType: CompactType.None,
            margin: 8,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1300,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            minItemArea: 1,
            resizable: {
                enabled: false
            },
            draggable: {
                delayStart: 0,
		        enabled: false,
		        ignoreContentClass: 'gridster-item-content',
		        ignoreContent: true,
		        dragHandleClass: 'drag-handler',
		        stop: ($event) => { }
            },
        };
    }

    ngOnInit(): void {
        this.activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
            this.resourceName = params['resourceName'];
            this.pageName = params['pageName'];
        });
        this.item1 = { reportType: ItemType.AD_AD_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item2 = { reportType: ItemType.ADMITDEVICE_ADMIT_DEVICE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item3 = { reportType: ItemType.CANDIDATEDEVICE_GENERAL_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item4 = { reportType: ItemType.CATALOGITEM_CATALOG_ITEM_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item5 = { reportType: ItemType.CHARTQUERY_CHART_QUERY_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item6 = { reportType: ItemType.CLUSTER_INDEX_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 0 };
        this.item7 = { reportType: ItemType.CLUSTER_DISCOVERY_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 32 };
        this.item8 = { reportType: ItemType.CLUSTER_ADMISSION_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 64 };
        this.item9 = { reportType: ItemType.CREDENTIAL_CREDENTIAL_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item10 = { reportType: ItemType.DASHBOARD_DASHBOARD_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item11 = { reportType: ItemType.DASHBOARDQUERY_DASHBOARD_QUERY_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item12 = { reportType: ItemType.PLATFORMCOMPONENT_DETAILCARD, id: this.resourceName, cols: 96, rows: 36, y: 0, x: 0 };
        this.item13 = { reportType: ItemType.DEVICE_SWITCHDETAIL_DETAILCARD, id: this.resourceName, cols: 80, rows: 17, y: 0, x: 0 };
        this.item14 = { reportType: ItemType.SYSTEM_DETAILCARD, id: this.resourceName, cols: 96, rows: 36, y: 0, x: 0 };
        this.item15 = { reportType: ItemType.DEVICE_IP_DETAILCARD, id: this.resourceName, cols: 16, rows: 17, y: 0, x: 81 };
        this.item16 = { reportType: ItemType.DEVICE_GENERAL_DETAILCARD, id: this.resourceName, cols: 32, rows: 36, y: 0, x: 0 };
        this.item17 = { reportType: ItemType.DEVICE_STATUS_DETAILCARD, id: this.resourceName, cols: 32, rows: 36, y: 0, x: 0 };
        this.item18 = { reportType: ItemType.DEVICESVICONFIG_DETAILCARD, id: this.resourceName, cols: 32, rows: 36, y: 0, x: 0 };
        this.item19 = { reportType: ItemType.DEVICECREDENTIAL_DEVICE_CREDENTIAL_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item20 = { reportType: ItemType.DEVICEUSER_DEVICE_USERS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item21 = { reportType: ItemType.DEVICEUSERROLE_DEVICE_USER_ROLE_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item22 = { reportType: ItemType.DEVICEUSERRULE_DEVICE_USER_RULE_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item23 = { reportType: ItemType.DISCOVEREDDEVICE_DISCOVERED_DEVICE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item24 = { reportType: ItemType.DISCOVEREDLINK_DISCOVERED_LINK_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item25 = { reportType: ItemType.ENDPOINTSET_ENDPOINT_SET_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item26 = { reportType: ItemType.ETHERNET_ETHERNET_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item27 = { reportType: ItemType.FAILUREINJECT_FAILURE_INJECT_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item28 = { reportType: ItemType.FAN_FAN_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item29 = { reportType: ItemType.FLOORSPACE_FLOOR_SPACE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item30 = { reportType: ItemType.FWIMG_FW_IMAGE_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item31 = { reportType: ItemType.FWMAINTGRP_GENERAL_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item32 = { reportType: ItemType.GEO_GEO_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item33 = { reportType: ItemType.HEALTH_HEALTH_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item34 = { reportType: ItemType.HEARTBEAT_HEART_BEAT_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item35 = { reportType: ItemType.IFINTERFACE_INTERFACE_DETAILS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 0 };
        this.item36 = { reportType: ItemType.SUBINTERFACESSPEC_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 32 };
        this.item37 = { reportType: ItemType.SUBINTERFACESSTATUS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 64 };
        this.item38 = { reportType: ItemType.INFRAPOOL_INFRA_POOL_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item39 = { reportType: ItemType.INTERNALROUTEV4_INTERNAL_ROUTE_V4_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item40 = { reportType: ItemType.INTERNALROUTEV6_INTERNAL_ROUTE_V6_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item41 = { reportType: ItemType.INTF_INTF_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item42 = { reportType: ItemType.IPV4_I_PV4_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item43 = { reportType: ItemType.IPV6_I_PV6_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item44 = { reportType: ItemType.L2ACCESSSUBNET_L2_ACCESS_SUB_NET_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item45 = { reportType: ItemType.L2DISTSUBNET_L2_DIST_SUB_NET_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item46 = { reportType: ItemType.LDAP_GENERAL_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item47 = { reportType: ItemType.LINK_LINK_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item48 = { reportType: ItemType.MEMLOG_MEM_LOG_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item49 = { reportType: ItemType.MGMTINTF_MGMT_INTF_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item50 = { reportType: ItemType.NACPROFILE_NAC_PROFILE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item51 = { reportType: ItemType.NETWORK_SPEC_DETAILCARD, id: this.resourceName, cols: 48, rows: 42, y: 0, x: 0 };
        this.item52 = { reportType: ItemType.NETWORK_STATUS_DETAILCARD, id: this.resourceName, cols: 48, rows: 42, y: 0, x: 48 };
        this.item53 = { reportType: ItemType.NETWORKPROFILE_TEMPLATE_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 0 };
        this.item54 = { reportType: ItemType.NETWORKPROFILE_FEATURES_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 32 };
        this.item55 = { reportType: ItemType.NETWORKPROFILE_NETWORKING_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 64 };
        this.item56 = { reportType: ItemType.NETWORKPROFILE_DEVICECREDENTIALS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 42, x: 0 };
        this.item57 = { reportType: ItemType.NETWORKRESOLUTIONCACHE_NETWORK_RESOLUTION_CACHE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item58 = { reportType: ItemType.PLATFORMCOMPONENT_PLATFORM_COMPONENT_DETAILS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 0 };
        this.item59 = { reportType: ItemType.PLATFORMCOMPONENT_TEMPERATURE_DETAILS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 32 };
        this.item60 = { reportType: ItemType.PLATFORMCOMPONENT_MEMORY_DETAILS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 64 };
        this.item61 = { reportType: ItemType.PLATFORMCOMPONENT_POWER_DETAILS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 42, x: 0 };
        this.item62 = { reportType: ItemType.PLATFORMCPU_PLATFORM_CPU_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item63 = { reportType: ItemType.PLATFORMFAN_PLATFORM_FAN_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item64 = { reportType: ItemType.PLATFORMLINECARD_PLATFORM_LINECARD_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item65 = { reportType: ItemType.PLATFORMPSU_PLATFORM_PSU_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item66 = { reportType: ItemType.POLICY_POLICY_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item67 = { reportType: ItemType.POLICYRESOLVED_POLICY_RESOLVED_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item68 = { reportType: ItemType.POLICYRULE_POLICY_RULE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item69 = { reportType: ItemType.POLICYSYNCDSTATE_POLICY_SYNCD_STATE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item70 = { reportType: ItemType.PORTALROLE_PORTAL_ROLES_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item71 = { reportType: ItemType.PORTALROLEBINDING_PORTAL_ROLE_BINDING_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item72 = { reportType: ItemType.PORTALTENANTBINDING_PORTAL_TENANT_BINDING_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item73 = { reportType: ItemType.PORTALTENANTBINDINGCACHE_PORTAL_TENANT_BINDING_CACHE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item74 = { reportType: ItemType.PORTALUSER_PORTAL_USERS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item75 = { reportType: ItemType.PORTALUSERSIGNUP_PORTAL_USER_SIGNUP_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item76 = { reportType: ItemType.POWERSUPPLY_POWERSUPPLY_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item77 = { reportType: ItemType.PROXYCONFIG_PROXY_CONFIG_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item78 = { reportType: ItemType.RADIO_RADIO_SPEC_DETAILCARD, id: this.resourceName, cols: 48, rows: 42, y: 0, x: 0 };
        this.item79 = { reportType: ItemType.RADIO_RADIO_STATUS_DETAILCARD, id: this.resourceName, cols: 48, rows: 42, y: 0, x: 48 };
        this.item80 = { reportType: ItemType.RADIUSCLIENT_RADIUS_CLIENT_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item81 = { reportType: ItemType.REGISTRYINFO_REGISTRY_INFO_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item82 = { reportType: ItemType.REPORT_REPORT_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item83 = { reportType: ItemType.SCHED_SCHED_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item84 = { reportType: ItemType.SERVICEIDENTITY_SERVICE_IDENTITY_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item85 = { reportType: ItemType.SERVICEINFO_SERVICE_INFO_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item86 = { reportType: ItemType.SSID_SSID_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item87 = { reportType: ItemType.STATICROUTE_STATIC_ROUTE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item88 = { reportType: ItemType.SUBINTF_SUB_INTF_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item89 = { reportType: ItemType.SUBNET_SUB_NET_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item90 = { reportType: ItemType.SUBTENANT_SUB_TENANT_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item91 = { reportType: ItemType.SWITCHEDVLAN_SWITCHED_VLAN_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item92 = { reportType: ItemType.SYSTEM_SYSTEM_DETAILCARD, id: this.resourceName, cols: 48, rows: 42, y: 0, x: 0 };
        this.item93 = { reportType: ItemType.SYSTEMCPUSTATUS_DETAILCARD, id: this.resourceName, cols: 48, rows: 42, y: 0, x: 48 };
        this.item94 = { reportType: ItemType.TACACSDSTATE_TACACSD_STATE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item95 = { reportType: ItemType.TASK_TASK_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item96 = { reportType: ItemType.TEMPLATE_TEMPLATE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item97 = { reportType: ItemType.TENANT_TENANT_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item98 = { reportType: ItemType.TENANTCONFIG_TENANT_CONFIG_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item99 = { reportType: ItemType.TENANTPROFILE_TENANT_PROFILE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item100 = { reportType: ItemType.TENANTSIGNUP_TENANT_SIGNUP_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item101 = { reportType: ItemType.TUNNELCONFIG_TUNNEL_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item102 = { reportType: ItemType.UICATALOG_UI_CATALOG_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item103 = { reportType: ItemType.USERSETTING_USER_SETTING_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item104 = { reportType: ItemType.WALLJACK_WALL_JACK_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item105 = { reportType: ItemType.WSAUDIT_WS_AUDIT_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item106 = { reportType: ItemType.WSDEVICEID_WS_DEVICE_ID_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item107 = { reportType: ItemType.ENDPOINT_INDEX_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 0, x: 0 };
        this.item108 = { reportType: ItemType.ENDPOINT_ENDPOINT_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 0, x: 24 };
        this.item109 = { reportType: ItemType.ENDPOINT_CONNECTION_TYPE_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 0, x: 48 };
        this.item110 = { reportType: ItemType.ENDPOINT_ROOM_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 0, x: 72 };
        this.item111 = { reportType: ItemType.ENDPOINT_USER_TYPE_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 44, x: 0 };
        this.item112 = { reportType: ItemType.ENDPOINT_CONNECTED_TO_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 44, x: 24 };
        this.item113 = { reportType: ItemType.ENDPOINT_AUTHENTICATION_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 44, x: 48 };
        this.item114 = { reportType: ItemType.ENDPOINT_DHCP_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 44, x: 72 };
        this.item115 = { reportType: ItemType.ENDPOINTAUX_ENDPOINT_SETS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item116 = { reportType: ItemType.WSEVENT_GENERAL_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item117 = { reportType: ItemType.WSFAULT_GENERAL_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item118 = { reportType: ItemType.FILE_WS_FILE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item119 = { reportType: ItemType.WSIPPOOL_WSIP_POOL_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item120 = { reportType: ItemType.WSLABEL_DETAIL_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item121 = { reportType: ItemType.WSQUERY_WS_QUERY_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item122 = { reportType: ItemType.WSSECRET_WS_SECRET_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item123 = { reportType: ItemType.USER_WS_USER_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item124 = { reportType: ItemType.USERGROUP_WS_USER_GROUP_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item125 = { reportType: ItemType.WSVLANPOOL_WS_VLAN_POOL_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item126 = { cols: 96, rows: 38, y: 0, x: 0 };
    }

    changeGridsterState(widget: any) {
        widget.resizeEnabled = !widget.resizeEnabled;
        widget.dragEnabled = !widget.dragEnabled;
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

  onAddToReport(item: any) {
    const dialogRef = this._dialogService.open({
      title: "Show in reports",
      content: KendoReportItemDialog,
      width: "300px",
      height: "200px",
    });
    const dialogInstance = dialogRef.content.instance;
    dialogInstance.dialog = dialogRef;
    dialogInstance.data = {
      title: "Show in reports",
      item,
    };
    dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
  }
    
    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}

