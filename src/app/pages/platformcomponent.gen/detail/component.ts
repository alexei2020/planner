
    import { Component, EventEmitter, OnInit, OnDestroy } from '@angular/core';
    import { HttpClient, HttpErrorResponse } from '@angular/common/http';
    
    import { combineLatest } from 'rxjs';
    import { map, first } from 'rxjs/operators';
    
    import { v4 as uuid } from 'uuid';
    
    import { CompactType, GridsterConfig, GridsterItem, GridsterPush, GridType } from 'angular-gridster2';
    import { DashboardService, TimerService } from '../../../main/shared/services';
    import { BreadcrumbService, EventType, Event, FormValidationInfo } from 'app/main/shared/breadcrumb-bar.service';
    import { ActivatedRoute, Router } from '@angular/router';
    import { ReportMenuService } from 'app/main/shared/services/report-menu.service';
    import { ItemType } from 'app/main/shared/services/report-menu.enum.gen';
    import { Subject } from 'rxjs';
    import { takeUntil } from 'rxjs/operators';
    import { KendoReportItemDialog } from "app/main/shared/dialogs/kendo-report-item-dialog/kendo-report-item-dialog.component";
    import { KendoDashboardItemDialog } from "app/main/shared/dialogs/kendo-dashboard-item-dialog/kendo-dashboard-item-dialog.component";
    import { KendoConfirmDialog } from "app/main/shared/dialogs/kendo-confirm-dialog/kendo-confirm-dialog.component";
    import { KendoObjectDialog } from "app/main/shared/dialogs/kendo-object-dialog/kendo-object-dialog.component";
    import {
       DialogCloseResult,
       DialogService,
       DialogResult,
    } from "@progress/kendo-angular-dialog";
    import { FloatingNotificationModel } from '../../../main/shared/floating-notification.model';
    import { FloatingNotificationService } from '../../../main/shared/floating-notification.service';
    import { FuseLocationService } from 'app/services/location.service';
    import { PageType } from 'app/main/shared/breadcrumb-bar.service.gen';
    import { LabelTableDialogComponent } from 'app/resource/label/dialog/component';
    import { PlatformComponentDetailService } from 'app/resource/platformcomponent.gen/services';
    import * as datatypes from '../../../typings/backendapi.gen';
    import { kindToEndpoint } from 'app/navigation/objrefnav.gen';
    import * as _ from 'lodash';
    import { webSocket } from 'rxjs/webSocket';
    import { environment } from 'environments/environment';
    import { KendoNoticeDialog } from '../../../main/shared/dialogs/kendo-notice-dialog/kendo-notice-dialog.component';

    @Component({
        templateUrl: './component.html',
        styleUrls: ['./component.scss']
    })
    export class PlatformcomponentDetailPageComponent implements OnInit, OnDestroy {
        public resourceName: string;
        public options: GridsterConfig;
        public itemType = ItemType;
        public itemLabel = { cols: 96, rows: 3, y: 0, x: 0 };
        public resizeEvent: EventEmitter<any> = new EventEmitter<any>();

	public activeSelectors = [];
        public selectors = [
        ];

	public activeDashboards = [];
	public staticDashboards = [
            { id: uuid(), reportType: ItemType.LABELS, cols: 96, rows: 0, y: 0, x: 0, pageSize:0, resizeEnabled: false, dragEnabled: false, enable: () => true },
	        // Auto calculation of rows and cols based on fields and columns
            { id: uuid(), reportType: ItemType.PLATFORMCOMPONENT_PLATFORM_COMPONENT_DETAILS_DETAILCARD, x: 0, y: 0, rows: 21 , cols: 48, compactEnabled: true, dragEnabled: false, resizeEnabled: false, pageSize: 10, enable: () => !((false)) && ((true)) },
	        // Auto calculation of rows and cols based on fields and columns
            { id: uuid(), reportType: ItemType.PLATFORMCOMPONENT_TEMPERATURE_DETAILS_DETAILCARD, x: 0, y: 0, rows: 13 , cols: 48, compactEnabled: true, dragEnabled: false, resizeEnabled: false, pageSize: 10, enable: () => !((false)) && ((true)) },
	        // Auto calculation of rows and cols based on fields and columns
            { id: uuid(), reportType: ItemType.PLATFORMCOMPONENT_MEMORY_DETAILS_DETAILCARD, x: 0, y: 0, rows: 5 , cols: 48, compactEnabled: true, dragEnabled: false, resizeEnabled: false, pageSize: 10, enable: () => !((false)) && ((true)) },
	        // Auto calculation of rows and cols based on fields and columns
            { id: uuid(), reportType: ItemType.PLATFORMCOMPONENT_POWER_DETAILS_DETAILCARD, x: 0, y: 0, rows: 5 , cols: 48, compactEnabled: true, dragEnabled: false, resizeEnabled: false, pageSize: 10, enable: () => !((false)) && ((true)) },
	];
        public defaultDashboards = [
        ];
	// list of object fields that this page operates on
	public fieldsUsed = [ 'apiVersion', 'kind', 'metadata.name', 'spec.base.parent_link',
	'spec.name',
	'spec.powerSupply.enabled',
	'spec.linecard.powerAdminState',
	'status.type',
	'status.id',
	'status.location',
	'status.description',
	'status.mfgName',
	'status.mfgDate',
	'status.hardwareVersion',
	'status.softwareVersion',
	'status.firmwareVersion',
	'status.serialNo',
	'status.partNo',
	'status.removable',
	'status.operStatus',
	'status.empty',
	'status.parent',
	'status.temperature.instant',
	'status.temperature.avg',
	'status.temperature.min',
	'status.temperature.max',
	'status.temperature.interval',
	'status.temperature.minTime',
	'status.temperature.maxTime',
	'status.temperature.alarmStatus',
	'status.temperature.alarmThreshold',
	'status.temperature.alarmSeverity',
	'status.memory.available',
	'status.memory.utilized',
	'status.allocatedPower',
	'status.usedPower',
	];
        item5: GridsterItem;
        public items: GridsterItem[];
        labels: any;
        public data: any;
        public platformcomponent: datatypes.PlatformComponent;
        currentLocation: any;
        currentTenant: string;
        currentLocationName: string
    	public fixedLocation =undefined
	public namespace: string;
        itemSwitch: GridsterItem;
        public linksObj = [
        ]
        public listOfCharts = [];
        public isSelectedForReportPlatformcomponentPlatform_Component_Details: boolean;
        public isSelectedForReportPlatformcomponentTemperature_Details: boolean;
        public isSelectedForReportPlatformcomponentMemory_Details: boolean;
        public isSelectedForReportPlatformcomponentPower_Details: boolean;
	public objectId: string;
    
        private _unsubscribeAll: Subject<any> = new Subject();
        private ws;
	private _prevPath = "";
    	private _backLocation: string;
	private selectedItemName: string;
	private selectedPlatform: string;
        private addChartTimeout;
    
        constructor(
            private dashboardSvc: DashboardService,
            private _breadcrumbService: BreadcrumbService,
            private _timer: TimerService,
            private httpClient: HttpClient,
            private activatedRoute: ActivatedRoute,
            private _reportMenuService: ReportMenuService,
            private _router: Router,
            private _locationService : FuseLocationService,
            private _platformcomponentDetailService: PlatformComponentDetailService,
	    private _dialogService: DialogService,
            private _notificationService: FloatingNotificationService,
        ) {
            this._locationService.disableLocationSelector();
            this.options = {
                swapping: false,
                itemResizeCallback: (item) => {
                    this.resizeEvent.emit(item);
                },
                gridType: GridType.VerticalFixed,
                compactType: 'compactUp',
                margin: 33,
		outerMarginTop: 0,
		outerMarginBottom: 0,
		outerMarginLeft: 30,
		outerMarginRight: 18,
                minCols: 96,
                maxCols: 96,
                minRows: 96,
                maxRows: 1300,
                maxItemCols: 96,
                minItemCols: 1,
                maxItemRows: 1000,
                minItemRows: 1,
                maxItemArea: 500000,
                minItemArea: 1,
                fixedRowHeight: 10,
                resizable: {
                    enabled: false
                },
                draggable: {
                    delayStart: 0,
                    enabled: false,
                    ignoreContentClass: 'gridster-item-content',
                    ignoreContent: true,
                    dragHandleClass: 'drag-handler',
                    stop: ($event) => { }
                },
                pushItems: true,
            };
            this._reportMenuService.getSelectedItemObs().pipe(takeUntil(this._unsubscribeAll)).subscribe(items => {
                this.isSelectedForReportPlatformcomponentPlatform_Component_Details = this._reportMenuService.itemIsChecked(ItemType.PLATFORMCOMPONENT_PLATFORM_COMPONENT_DETAILS_DETAILCARD, this.resourceName);
                this.isSelectedForReportPlatformcomponentTemperature_Details = this._reportMenuService.itemIsChecked(ItemType.PLATFORMCOMPONENT_TEMPERATURE_DETAILS_DETAILCARD, this.resourceName);
                this.isSelectedForReportPlatformcomponentMemory_Details = this._reportMenuService.itemIsChecked(ItemType.PLATFORMCOMPONENT_MEMORY_DETAILS_DETAILCARD, this.resourceName);
                this.isSelectedForReportPlatformcomponentPower_Details = this._reportMenuService.itemIsChecked(ItemType.PLATFORMCOMPONENT_POWER_DETAILS_DETAILCARD, this.resourceName);
            });
            
            this._breadcrumbService.currentPage.next(PageType.DEVICE_DETAIL);
            this._breadcrumbService.platformSelected.next(null);
        }
    
        ngOnInit(): void {
            this._locationService.updateLocationDropdownEnabledValue(false);
            this.activatedRoute.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
                this.resourceName = this.activatedRoute.snapshot.params['resourceName'];
		this.objectId = this.activatedRoute.snapshot.queryParams.objectId;
		if (this.objectId) {
        	    this._breadcrumbService.currentPage.next(PageType.DEVICE_DETAIL_ADD);
                    setTimeout(() => this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true})), 100);
		}
	    	if (this.activatedRoute.snapshot.queryParams.backLocation) {
	    		this._backLocation = this.activatedRoute.snapshot.queryParams.backLocation;
			this._prevPath = this._router.url.split("?")[0].replace("/"+this.resourceName, "");
	    	}
                const loc = this._locationService.locationDataAsKeyValue().currentLocation;
                this.currentTenant = this._locationService.locationDataAsKeyValue().tenantName;
                this.currentLocationName = this._locationService.locationDataAsKeyValue().locationName;
		this.namespace = this.activatedRoute.snapshot.queryParams.namespace;
		this._breadcrumbService.currentPageInfo.next({
		    detailsPage: true,
		    kind: 'platformcomponent',
		    name: this.resourceName,
		    namespace: this.namespace
		});
                this.isSelectedForReportPlatformcomponentPlatform_Component_Details = this._reportMenuService.itemIsChecked(ItemType.PLATFORMCOMPONENT_PLATFORM_COMPONENT_DETAILS_DETAILCARD, this.resourceName);
                this.isSelectedForReportPlatformcomponentTemperature_Details = this._reportMenuService.itemIsChecked(ItemType.PLATFORMCOMPONENT_TEMPERATURE_DETAILS_DETAILCARD, this.resourceName);
                this.isSelectedForReportPlatformcomponentMemory_Details = this._reportMenuService.itemIsChecked(ItemType.PLATFORMCOMPONENT_MEMORY_DETAILS_DETAILCARD, this.resourceName);
                this.isSelectedForReportPlatformcomponentPower_Details = this._reportMenuService.itemIsChecked(ItemType.PLATFORMCOMPONENT_POWER_DETAILS_DETAILCARD, this.resourceName);

                if (loc && loc !== this.currentLocation) {
                    this.currentLocation = loc;
                    if (this.fixedLocation) {
                        this.currentLocation = this.fixedLocation;
                    }
                    var wsUrl = `${environment.baseUrl}ws/obj/${this.currentLocation}/v1/platformcomponents/${this.resourceName}`.replace('http', 'ws');
                    if (this.ws) {
                        this.ws.complete();
                    }
		    this.ws = webSocket(wsUrl);
                    this.ws.pipe(takeUntil(this._unsubscribeAll)).subscribe((rsp) => {
                        if (!rsp) return;
                        let response = JSON.parse(rsp.Object);
                        this.reloadPage(response);
                    });
		    this.loadplatformcomponent();
                }
            });

	    this._breadcrumbService.triggeredEvent.pipe(takeUntil(this._unsubscribeAll)).subscribe((event: Event) => {
	    	switch (event.type) {
		case EventType.DEVICE_DETAIL_SAVE:
		    if (this.objectId) {
			setTimeout(()=> {
  			    this.addplatformcomponent();
			});
		    } else {
			setTimeout(()=> {
			    this.saveplatformcomponent();
			});
		    }
		    break;
		case EventType.DEVICE_DETAIL_CANCEL:
		    if (this.objectId) {
  			this._router.navigate([kindToEndpoint.get('PlatformComponent')], {
  			  queryParams: {
  			    tenant: this.currentTenant,
  			    loc: this.currentLocationName,
  			    objectId: null,
  			  },
  			  replaceUrl: true,
  			});
		    } else {
  			this.loadplatformcomponent();
		    }
		    this._breadcrumbService.isDeviceDetailEditMode.next(null);
		    break;
		case EventType.DEVICE_DETAIL_ADD:
		    this.onAddHandler('PlatformComponent');
		    break;
		case EventType.DEVICE_DETAIL_CLONE:
		    this.onCloneHandler();
		    break;
		case EventType.DEVICE_DETAIL_DELETE:
		    this.deleteHandler();
		    break;
		}
    	    });

            this._timer.setDateRangeObs(this._breadcrumbService.setRange({ label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 }));
            this._timer.setPanel(true);
            
            //get queries
            this.dashboardSvc.getQueries(this.currentLocation).pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
                this.dashboardSvc.setQueriesObs(res['items']);
            });
    
            //get charts
            this.dashboardSvc.getChartPanels(this.currentLocation).pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
                this.dashboardSvc.setChartsObs(res['items']);
            });

            this._breadcrumbService.triggeredLabelRuleEvent.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
			const dialogRef = this._dialogService.open({
          		title: "Show in dashboards",
          		content: LabelTableDialogComponent,
          		width: "90%",
          		height: "245px",
        	});
        	const dialogInstance = dialogRef.content.instance;
        	dialogInstance.dialog = dialogRef;
        	dialogInstance.data = {
          		singleSelection: true,
          		label: this.labels,
        	};
		dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
          		if (
            			response instanceof DialogCloseResult ||
            			response.text == "Cancel"
          		) {
          		} else {
                    		this.labels = dialogInstance.labelData.map(item => ({
                        	name: item.metadata.name,
                        	values: item.selectedValues
                    		}));
    
                    if (this.labels.length > 0 && this.labels.filter(item => item.values.length > 0).length > 0) {
                        this.platformcomponent.metadata.labels = this.labels.filter(item => item.values.length > 0).reduce((acc, cur) => {
                            acc[cur.name] = cur.values[0];
                            return acc;
                        }, {});
                    } else {
                        this.platformcomponent.metadata.labels = {} as Map<string, string>;
                    }
    
		    if (this.objectId) {
			this.addplatformcomponent();
		    } else {
			this.saveplatformcomponent();
		    }
		}
                });
            });

            this._breadcrumbService.platformSelected.pipe(takeUntil(this._unsubscribeAll)).subscribe((platform) => {
		if (this.selectedPlatform == platform?.id) {
			return;
		}
		this.selectedPlatform = platform ? platform.id : null;
		this.handleSelectorChange();
            });

            this._breadcrumbService.selectedItemsChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe((items) => {
		if (items.length == 1) {
		    this.selectedItemName = items[0].item_text.replaceAll("/", ".").toLowerCase();
		} else {
		    this.selectedItemName = null;
		}
		this.handleSelectorChange();
            });
        }

	handleSelectorChange(): void {
	    switch (this.selectedPlatform) {
		default:
		    this.activeDashboards = this.staticDashboards.concat(this.defaultDashboards).filter(item => item.enable());
	    }
            if (this.addChartTimeout) {
                clearTimeout(this.addChartTimeout);
            }
	    this.addChartTimeout = setTimeout(() => {
	        this.activeDashboards = this.activeDashboards.concat(this.listOfCharts).filter(item => item.enable());
	    });
	}

	loadplatformcomponent(): void {
	    if (this.objectId) {
		try {
        	    const item = sessionStorage.getItem(this.objectId);
        	    this.platformcomponent = JSON.parse(item);
                    this.initEmptyFields();
      		} catch {
        	    this.platformcomponent = {} as datatypes.PlatformComponent;
      		}
		this.updateLabels();
		return;
	    }

            this._platformcomponentDetailService.load(this.resourceName, this.namespace, this.currentLocation).pipe(first()).subscribe(
                (response) => {
                    this.reloadPage(response);
	        },
                () => {
                    console.log('some error');
                    this.platformcomponent = null;
                }
            );
	}

        initEmptyFields() {
            this.initObjField(this.platformcomponent, 'spec.name', '');
            this.initObjField(this.platformcomponent, 'spec.powerSupply.enabled', true);
            this.initObjField(this.platformcomponent, 'spec.linecard.powerAdminState', '');
            this.initObjField(this.platformcomponent, 'status.type', '');
            this.initObjField(this.platformcomponent, 'status.id', '');
            this.initObjField(this.platformcomponent, 'status.location', '');
            this.initObjField(this.platformcomponent, 'status.description', '');
            this.initObjField(this.platformcomponent, 'status.mfgName', '');
            this.initObjField(this.platformcomponent, 'status.mfgDate', '');
            this.initObjField(this.platformcomponent, 'status.hardwareVersion', '');
            this.initObjField(this.platformcomponent, 'status.softwareVersion', '');
            this.initObjField(this.platformcomponent, 'status.firmwareVersion', '');
            this.initObjField(this.platformcomponent, 'status.serialNo', '');
            this.initObjField(this.platformcomponent, 'status.partNo', '');
            this.initObjField(this.platformcomponent, 'status.removable', true);
            this.initObjField(this.platformcomponent, 'status.operStatus', '');
            this.initObjField(this.platformcomponent, 'status.empty', true);
            this.initObjField(this.platformcomponent, 'status.parent', '');
            this.initObjField(this.platformcomponent, 'status.temperature.instant', 0);
            this.initObjField(this.platformcomponent, 'status.temperature.avg', 0);
            this.initObjField(this.platformcomponent, 'status.temperature.min', 0);
            this.initObjField(this.platformcomponent, 'status.temperature.max', 0);
            this.initObjField(this.platformcomponent, 'status.temperature.interval', 0);
            this.initObjField(this.platformcomponent, 'status.temperature.minTime', 0);
            this.initObjField(this.platformcomponent, 'status.temperature.maxTime', 0);
            this.initObjField(this.platformcomponent, 'status.temperature.alarmStatus', true);
            this.initObjField(this.platformcomponent, 'status.temperature.alarmThreshold', 0);
            this.initObjField(this.platformcomponent, 'status.temperature.alarmSeverity', '');
            this.initObjField(this.platformcomponent, 'status.memory.available', 0);
            this.initObjField(this.platformcomponent, 'status.memory.utilized', 0);
            this.initObjField(this.platformcomponent, 'status.allocatedPower', 0);
            this.initObjField(this.platformcomponent, 'status.usedPower', 0);
        }

        reloadPage(response: datatypes.PlatformComponent): void {
            this.platformcomponent = response;
            this.initEmptyFields();
            this.updateLabels();
            this.loadRuntimeChart();
	    this.activeSelectors = this.selectors.filter(item => item.enable());
	    this._breadcrumbService.selectedPlatform.next(this.activeSelectors);
	    this.refreshGridster();
        }

	saveplatformcomponentFailed(error: Error): void {
	    	let errorMessage;
	    	if (error instanceof HttpErrorResponse && error.error) {
      			errorMessage = error.error.message;
    		    if (!errorMessage) {
                errorMessage = error.error.error;
            }
    		} else {
      			errorMessage = error.message;
    		}


    		const dialogRef = this._dialogService.open({
      			title: "Cannot save platformcomponent",
      			content: KendoNoticeDialog,
      			width: "500px",
      			height: "250px",
    		});
    		const dialogInstance = dialogRef.content.instance;
    		dialogInstance.message = errorMessage;
    		dialogInstance.dialog = dialogRef;
    		dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
	}

	addplatformcomponent(): void {
	    const location = this.platformcomponent.apiVersion.split("/")[0];
	    const objToSave = <datatypes.PlatformComponent>_.cloneDeep(_.pick(this.platformcomponent, this.fieldsUsed));

        // If Secret object is not modified, lets not include it in the save
        // right now, we will not include if its empty

	    this._platformcomponentDetailService
		.add(objToSave, location)
		.pipe(first())
		.subscribe((platformcomponent: datatypes.PlatformComponent) => {
		    sessionStorage.removeItem(this.objectId);
		    this.objectId = null;
		    this._router.navigate([], {
			queryParams: {
			    tenant: this.currentTenant,
                            loc: this.currentLocationName,
			    objectId: null,
			},
			replaceUrl: true,
		    });
		    this.loadplatformcomponent();
                    this._notificationService.sendMessage({ text: `'${this.platformcomponent.metadata.name}' Saved`, type: 'success' });
		    this._breadcrumbService.isDeviceDetailEditMode.next(null);
		},
		(error) => {
		    this.saveplatformcomponentFailed(error);
		    this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true}));
		}
	    );
	}

	saveplatformcomponent(): void {
	    const location = this.platformcomponent.apiVersion.split("/")[0];
	    // We use PATCH to update. So, lets update only the fields that
	    // are handled by this page
	    const objToSave = <datatypes.PlatformComponent>_.cloneDeep(_.pick(this.platformcomponent, this.fieldsUsed));

        // If Secret object is not modified, lets not include it in the save
        // right now, we will not include if its empty

	    this._platformcomponentDetailService
		.update(objToSave, location)
		.pipe(first())
		.subscribe((platformcomponent: datatypes.PlatformComponent) => {
		    this.loadplatformcomponent();
            this._notificationService.sendMessage({ text: `'${this.platformcomponent.metadata.name}' Saved`, type: 'success' });
		    this._breadcrumbService.isDeviceDetailEditMode.next(null);
		},
		(error) => {
		    this.saveplatformcomponentFailed(error);
		    this._breadcrumbService.isDeviceDetailEditMode.next(new FormValidationInfo({isValid: true}));
		}
	    );
	}

        updateLabels() {
            const labels = this.platformcomponent.metadata.labels || "";
            this.labels =  Object.keys(labels).map(name => ({ name, values: [labels[name]] }));
            const iconStyle = this.labels.length > 0 ? 'label-outline' : 'label-off';
            this._breadcrumbService.iconStyle.next(iconStyle);

            const labelGridsterItem = this.activeDashboards[0];

            const listOfEnabledItems = this.activeDashboards.filter(item => item.dragEnabled).map(item => item.id);
            this.activeDashboards.forEach(item => {
                item.dragEnabled = true;
                item.resizeEnabled = true;
            });
            if (this.options.api && this.options.api.optionsChanged) {
                this.options.api.optionsChanged();
            }

            if(this.labels.length > 0) {
                if(labelGridsterItem.rows === 0 ) {
                    this.activeDashboards.forEach((item, index) => {
                        if(index !== 0) {
                            item.y = item.y + 3;
                        }
                    });
                    if (this.options.api && this.options.api.optionsChanged) {
                        this.options.api.optionsChanged();
                    }
                    labelGridsterItem.y = 0;
                    labelGridsterItem.rows = 3;
                    if (this.options.api && this.options.api.optionsChanged) {
                        this.options.api.optionsChanged();
                    }
                }
            } else {
                if(labelGridsterItem.reportType === ItemType.LABELS) {
                    if(labelGridsterItem.rows === 3) {
                        this.activeDashboards.forEach((item, index) => {
                            if(index === 0) {
                                item.rows = 0;
                            }
                            item.y = item.y - 3;
                        });
                        if (this.options.api && this.options.api.optionsChanged) {
                            this.options.api.optionsChanged();
                        }
                    }
                }
            }


            this.activeDashboards.forEach(record => {
                const enabled = listOfEnabledItems.some(id => record.id === id);
                record.dragEnabled = enabled;
                record.resizeEnabled = enabled;
            });
            if (this.options.api && this.options.api.optionsChanged) {
                this.options.api.optionsChanged();
            }
        }

	refreshGridster(): void {
	    const activeStaticDashboards = this.staticDashboards.filter(item => item.enable());
            const activeDefaultDashboards = this.defaultDashboards.filter(item => item.enable());
            const newDashboards = activeStaticDashboards.concat(activeDefaultDashboards);
            if (newDashboards.length > this.activeDashboards.length - this.listOfCharts.filter(item => item.enable()).length) {
		// Force refresh
                this.activeDashboards = [];
            }
            setTimeout(() => {
                this.activeDashboards = newDashboards;
                this.activeDashboards = this.activeDashboards.concat(this.listOfCharts).filter(item => item.enable());
            }); 
	}

    
	setHeightGridster(index, targetItem, tableHeight) {
		const previousRow: number = targetItem.rows;
		const emptyTableHeight = 120;
		tableHeight = Math.max(emptyTableHeight, tableHeight);
        const totalRowHeight = this.options.fixedRowHeight + this.options.margin;
        const rows = Math.ceil((tableHeight + 50 + this.options.margin) / totalRowHeight);
		if (this.options && this.options.api && this.options.api.getItemComponent) {
			const item = this.options.api.getItemComponent(targetItem);
			this.pushItem(item, previousRow, rows);
		}
	}
	pushItem(itemToPush, previousRow, currentRow) {
		const push = new GridsterPush(itemToPush); // init the service
		itemToPush.$item.rows = currentRow; // move/resize your item

		const listOfEnabledItems = this.activeDashboards.filter(item => item.dragEnabled).map(item => item.id);
		this.activeDashboards.forEach(item => {
			item.dragEnabled = true;
			item.resizeEnabled = true;
		});
		this.updateOptions();

		itemToPush.$item.rows = currentRow;

		if (push.pushItems(push.fromNorth) || push.pushItems(push.fromSouth)) { // push items from a direction
			push.checkPushBack(); // check for items can restore to original position
			push.setPushedItems(); // save the items pushed
			itemToPush.setSize();
			itemToPush.checkItemChanges(itemToPush.$item, itemToPush.item);
		} else {
			itemToPush.$item.rows = previousRow;
			push.restoreItems(); // restore to initial state the pushed items
		}
		this.activeDashboards.forEach(record => {
			const enabled = listOfEnabledItems.some(id => record.id === id);
			record.dragEnabled = enabled;
			record.resizeEnabled = enabled;
		});
		this.updateOptions();

		push.destroy(); // destroy push instance
	}

	updateOptions() {
		if (this.options.api && this.options.api.optionsChanged) {
			this.options.api.optionsChanged();
		}
	}
	
        ngOnDestroy(): void {
            this._breadcrumbService.currentPage.next(PageType.DEFAULT);
            const nextPath = this._router.url.split("?")[0];
// 	    if (this._backLocation && (nextPath === this._prevPath)) {
// 	      const tenant = this.activatedRoute.snapshot.queryParams.tenant
// 	    	this._router.navigate([], {
// 	    	  relativeTo: this.activatedRoute,
// 	    	  queryParams: {tenant: this.currentTenant, loc: this._locationService.convertAPIGroup2Name(this._backLocation)},
// 	    	  replaceUrl: true
// 	    	  });
// 	    }
            this._unsubscribeAll?.next();
            this._unsubscribeAll?.complete();
            this.ws?.complete();
            this._locationService.enableLocationSelector();
            this._locationService.updateLocationDropdownEnabledValue(true);
        }
    
	setPageSize(targetItem, tableHeight) {
	    const totalRowHeight = this.options.fixedRowHeight + this.options.margin;
	    const rows = Math.ceil((tableHeight + 50 + this.options.margin) / totalRowHeight);
	    const fixedRows = targetItem.rows;
            if (rows > fixedRows) {
                targetItem.pageSize = Math.max(1, Math.floor(targetItem.pageSize * (fixedRows/rows)));
            }
	}

        changeGridsterState(widget: any, general = true) {
            if(general) {
                widget.resizeEnabled = !widget.resizeEnabled;
                widget.dragEnabled = !widget.dragEnabled;
            }
            if (this.options.api && this.options.api.optionsChanged) {
                this.options.api.optionsChanged();
            }
        }
            setForPlatformcomponentPlatform_Component_DetailsReport():void {
                this._reportMenuService.setItemObs(ItemType.PLATFORMCOMPONENT_PLATFORM_COMPONENT_DETAILS_DETAILCARD, this.resourceName);
            }
        
            setForPlatformcomponentTemperature_DetailsReport():void {
                this._reportMenuService.setItemObs(ItemType.PLATFORMCOMPONENT_TEMPERATURE_DETAILS_DETAILCARD, this.resourceName);
            }
        
            setForPlatformcomponentMemory_DetailsReport():void {
                this._reportMenuService.setItemObs(ItemType.PLATFORMCOMPONENT_MEMORY_DETAILS_DETAILCARD, this.resourceName);
            }
        
            setForPlatformcomponentPower_DetailsReport():void {
                this._reportMenuService.setItemObs(ItemType.PLATFORMCOMPONENT_POWER_DETAILS_DETAILCARD, this.resourceName);
            }
        
    
    	public onAddHandler(objType: string) {
		const dialogRef = this._dialogService.open({
      			content: KendoObjectDialog,
      			width: "400px",
      			height: "300px",
      			title: "Add New Object",
    		});
    		const dialogInstance = dialogRef.content.instance;
    		dialogInstance.dialog = dialogRef;
    		dialogInstance.location = null;
    		dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
      		if (response instanceof DialogCloseResult || response.text == "Cancel") {
      		} else {
        		const item = dialogInstance.data as any;
        		var obj = {
          			apiVersion: item.location + "/v1",
          			kind: objType,
          			metadata: {
            				name: item.name,
          			},
        	        };
		        if (objType != 'PlatformComponent') {
		                // child object creation
		    	        obj = _.set(obj, 'spec.base.parent_link', this.platformcomponent.metadata.selfLink);
		        }

        	        this.onAddNewClosed(obj);
      		}
    	        });
	}

        public onCloneHandler() {
            const dialogRef = this._dialogService.open({
                content: KendoObjectDialog,
                width: "400px",
                height: "300px",
                title: "Clone Object",
            });
            const dialogInstance = dialogRef.content.instance;
            dialogInstance.dialog = dialogRef;
            dialogInstance.location = null;
            dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
                if (response instanceof DialogCloseResult || response.text == "Cancel") {
                } else {
                    const item = dialogInstance.data as any;
                    var obj = _.cloneDeep(this.platformcomponent);
                    obj.apiVersion = item.location + "/v1";
                    obj.metadata.name = item.name;
                    this.onAddNewClosed(obj);
                }
            });
        }

    	private onAddNewClosed(obj: any): void {
	    const backLocation = this.platformcomponent.apiVersion.split("/")[0];
	    const backLocName = this._locationService.convertAPIGroup2Name(backLocation);
	    const location = obj.apiVersion.split("/")[0];
	    const locationName = this._locationService.convertAPIGroup2Name(location);
	    const id = uuid();
	    const endpoint = kindToEndpoint.get(obj.kind);

	    sessionStorage.setItem(id, JSON.stringify(obj));

	    this._router.navigate([kindToEndpoint.get(obj.kind), obj.metadata.name], {
	    	queryParams: {
	    	  tenant: this._locationService.locationDataAsKeyValue().tenantName,
	    	  loc: locationName,
	    	  backLocation: backLocName,
	    	  objectId: id },
	    	replaceUrl: true,
	    });
	    this._router.navigateByUrl(`/${endpoint}`, { skipLocationChange: true }).then(()=> {
                this._router.navigate([endpoint, obj.metadata.name], {
                    queryParams: {
                      tenant: this._locationService.locationDataAsKeyValue().tenantName,
                      loc: locationName,
                      backLocation: backLocName,
                      objectId: id },
                    replaceUrl: true,
                });
            });
    	}

    	public deleteHandler(): void {
            	const id = this.platformcomponent.metadata.name;
		const dialogRef = this._dialogService.open({
      			title: "Delete PlatformComponent",
      			content: KendoConfirmDialog,
      			width: "420px",
      			height: "200px",
    		});
    		const dialogInstance = dialogRef.content.instance;
    		dialogInstance.message = `Are you sure you want to remove ${id}?`;
    		dialogInstance.dialog = dialogRef;
    		dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {
      		if (response instanceof DialogCloseResult || response.text == "No") {
      		} else {
	    	    const location = this.platformcomponent.apiVersion.split("/", -1).slice(0, -1).join("/");
                    this._platformcomponentDetailService.delete(id, location).pipe(takeUntil(this._unsubscribeAll)).subscribe(
                        () => {
			    this._router.navigate([kindToEndpoint.get('PlatformComponent')], { queryParams: { tenant: this.currentTenant, loc: this.currentLocationName, objectId: null },
			    replaceUrl: true,
			    });
                        },
                        error => alert('Failed to delete ${id}')
                    );
                }
            });
    	}

        onAddToReport(item: any) {
		const dialogRef = this._dialogService.open({
      			title: "Show in reports",
      			content: KendoReportItemDialog,
      			width: "300px",
      			height: "200px",
    		});
    		const dialogInstance = dialogRef.content.instance;
    		dialogInstance.dialog = dialogRef;
    		dialogInstance.data = {
      			title: "Show in reports",
      			item,
    		};
    		dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
        }
    
         onAddToDashboard(item: any) {
		const dialogRef = this._dialogService.open({
      			title: "Show in dashboards",
      			content: KendoDashboardItemDialog,
      			width: "300px",
      			height: "200px",
    		});
    		const dialogInstance = dialogRef.content.instance;
    		dialogInstance.dialog = dialogRef;
    		dialogInstance.data = {
      			title: "Show in dashboards",
      			item,
    		};
    		dialogRef.result.pipe(takeUntil(this._unsubscribeAll)).subscribe((response: DialogResult) => {});
        }
    
        customViewWidget(widget) {
            this._router.navigate(['platformcomponents', 'runtime-chart'], { queryParams: { tenant: this.currentTenant, loc: this.currentLocationName, chartLink: widget.chartLink }})
        }
    
        private loadRuntimeChart() {
            combineLatest(
                this.linksObj.map(linkObj => this.httpClient.get<any>(linkObj.link).pipe(map((res, index) => ({
                    ...res, ...{ id: uuid(), reportType: ItemType.CHART, dragEnabled: false, resizeEnabled: false, x: 0, y: 0, rows: 7, cols: 48 }
                }))))
            ).pipe(takeUntil(this._unsubscribeAll)).subscribe((response: any[]) => {
		this.listOfCharts = response.map( (res, index) => ({...res, ...{ enable: this.linksObj[index].enable, chartLink: this.linksObj[index].link } }));
		// insert chart at the end
		this.handleSelectorChange();
                if (this.items) {
                    this.items = this.items.concat(this.listOfCharts);
                }
            });
        }

    	initObjField(obj, path, value): any {
	    if (!_.has(obj, path)) {
	    	return _.set(obj, path, value);
	    }
	    return obj;
    	}
        pruneObjField(obj, path): void {
            _.unset(obj, path);
        }

    }    
