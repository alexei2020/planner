// tslint:disable
/*
Copyright (c) 2020 WaveSurf Systems Inc. All rights reserved.
*/
import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";
import { SearchResultsPageComponent } from "./component";
const LIST_ROUTE: Route = {
    path: "",
    component: SearchResultsPageComponent
};


@NgModule({
    imports: [
        RouterModule.forChild([LIST_ROUTE]),
    ],
    exports: [RouterModule],
})
export class SearchResultsPageRoutingModule {}
