import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import { takeUntil } from 'rxjs/operators';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

import { DataBindingDirective, GridDataResult } from '@progress/kendo-angular-grid';

import { Subject } from 'rxjs';
import { FuseSearchBarService } from '@fuse/services/search-bar.service';
import {FuseLocationService} from '../../services/location.service';

@Component({
  templateUrl: './component.html',
  styleUrls: ['./component.scss']
})
export class SearchResultsPageComponent implements OnInit, OnDestroy {

  @ViewChild(DataBindingDirective) dataBinding: DataBindingDirective;
  public gridView: GridDataResult;

  private _unsubscribeAll: Subject<any> = new Subject();
  searchKeyword: string = "";
  options: any[] = [];
  options2: any[] = [];
  constructor(private fuseSearchBarService: FuseSearchBarService,
    private activatedRoute: ActivatedRoute,
    private _locationService: FuseLocationService,
    public router: Router) {
    this.gridView = {
      data: [],
      total: 0
    };
  }

  ngOnInit() {
    this.activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe(params => {
      this.searchKeyword = decodeURI(params.keyword);// this.fuseSearchBarService._searchTerm;
      if (this.searchKeyword && this.searchKeyword.length > 0) {
        this.activatedRoute.queryParams
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(queryParams => {
            const loc = this._locationService.locationDataAsKeyValue().currentLocation;
            this.fuseSearchBarService.getNlpData(this.searchKeyword, loc).then((res: any) => {
              if (res && res.items.length == 1) {
                this.redirect(res.items[0]);
              }
              res.items.forEach(item => {
                item.resolvedIntent = JSON.parse(item.resolvedIntent);
              });
              this.options = res.items;
            }).catch(err => {
            })
          })
      }
    });
  }

  redirect(searchItem) {
    const tokens = searchItem.url.split("/");
    const endpointAndFilter = tokens[tokens.length - 1].split("?");
    const endpoint = "/" + endpointAndFilter[0];

    console.log("NAVIGATING TO...", searchItem.url, endpoint, searchItem.queryFilter);
    const loc = this._locationService.locationDataAsKeyValue().locationName;
    const tenantName = this._locationService.locationDataAsKeyValue().tenantName;
    this.router.navigate([endpoint], { relativeTo: this.activatedRoute, queryParams: {
	loc:loc, tenant: tenantName, esFilter: JSON.stringify(searchItem.queryFilter) } } );
  }

  public removeNotification(notification) {
  }

  public updateNotification(value, notification) {
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
