// tslint:disable
/*
    Copyright (c) 2020 WaveSurf Systems Inc. All rights reserved.
    */

import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

import { SearchResultsPageRoutingModule } from "./routing";
import { SearchResultsPageComponent } from "./component";
import { MatListModule } from '@angular/material/list';
@NgModule({
    imports: [CommonModule, SearchResultsPageRoutingModule, MatListModule],
    declarations: [SearchResultsPageComponent],
    exports: [],
})
export class SearchResultsPageModule {}
