
import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { CompactType, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class LabelDetailPageComponent implements OnInit, OnDestroy {
    options: GridsterConfig;
    resizeEvent: EventEmitter<any> = new EventEmitter<any>();
    item1: GridsterItem;
    public data: any;

    private _unsubscribeAll: Subject<any> = new Subject();

    constructor(private activatedRoute: ActivatedRoute) {
        this.options = {
            margin: 5,
            gridType: GridType.VerticalFixed,
            compactType: CompactType.None,
            swapping: false,
            itemResizeCallback: (item) => {
                this.resizeEvent.emit(item);
            },
            pushItems: false,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1000,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            minItemArea: 1,
            resizable: {
                enabled: true
            },
            draggable: {
                enabled: true
            }
        };
    }

    ngOnInit(): void {
        this.activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
            const itemId = params['id'];
        });
        this.item1 = { cols: 96, rows: 2, y: 0, x: 0 };
    }

    ngOnDestroy(): void {
        this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
    }
}

