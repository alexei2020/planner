
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { Subject, Observable } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';

import { GridsterConfig, GridType, CompactType, GridsterItem } from 'angular-gridster2';

import { JsonDialogComponent } from '../../../main/shared/json-dialog/json-dialog.component';
import { LayoutService } from '../../../main/shared/services';
import { Widget } from '../../../main/widget';
import { ReportMenuService } from 'app/main/shared/services/report-menu.service';
import { BreadcrumbService } from '../../../main/shared/breadcrumb-bar.service';
import { PageType } from '../../../main/shared/breadcrumb-bar.service.gen';

@Component({
    selector: 'report-chart-view',
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class ReportChartViewComponent implements OnInit, OnDestroy {
    public filter: string;
    public resetDataSource: any;
    public timer$: Observable<any>;
    public refreshTimer$: Observable<any>;
    public interval$: Observable<any>;
    public options: GridsterConfig;
    public item1: GridsterItem;
    public item2: GridsterItem;

    private _unsubscribeAll: Subject<any> = new Subject();
    timer: any;

    listOfCharts: any[];


    constructor(
        private location: Location,
        private layoutService: LayoutService,
        private activatedRoute: ActivatedRoute,
        private httpClient: HttpClient,
        private _dialog: MatDialog,
        private _breadcrumbService: BreadcrumbService,
        private _reportMenuService: ReportMenuService,
    ) {
        this.options = {
            gridType: GridType.ScrollVertical,
            compactType: CompactType.None,
            margin: 8,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1000,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            minItemArea: 1,
            resizable: {
                enabled: true
            },
            draggable: {
                enabled: true
            }
        };

        this.item1 = { cols: 96, rows: 35, y: 0, x: 0 };
        this.item2 = { cols: 96, rows: 46, y: 0, x: 0 };

        this.activatedRoute
            .queryParams
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((params) => {
                if (params && params['chartLink']) {
                    this.loadRuntimeChart(params['chartLink']);
                } else {
                    const chartId = this.activatedRoute.snapshot.params.resourceName;
                    const layoutName = this.activatedRoute.snapshot.params.dashboard;
                    if (chartId && layoutName) {
                        this._reportMenuService.getReports(params['loc']).pipe(takeUntil(this._unsubscribeAll)).subscribe((res: any) => {
                            const layout = res.items.find((item: any) => item.metadata.name === layoutName).spec.layout;
                            const targetChart = JSON.parse(layout).find((item: any) => item.id ? item.id === chartId : null)
                            if (layout && targetChart) {
                                this.listOfCharts = [];
                                this.listOfCharts.push(targetChart);
                            }
                        });
                    }
                }
            });
            this._breadcrumbService.currentPage.next(PageType.DEFAULT);
    }

    ngOnInit() { }

    ngOnDestroy() {
        this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
    }

    public onFilter(filter: string) {
        this.filter = filter;
    }

    public jsonWidget(widget: Widget) {
        this.layoutService.setSelectedChartObs(widget.id);
        let options = this.layoutService.chartOptions;
        const dialogRef = this._dialog.open(JsonDialogComponent, {
            width: '750px',
            data: options,
            panelClass: 'json-panel'
        });
        dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
            if (res) {
                widget['chartOptions'] = res;
                this.layoutService.editItem(widget);
            }
        });
    }
    public viewWidget() {
        this.location.back();
    }

    public filterChanged(event) {
        this.filter = event;
    }

    public changeGridsterState() {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    private loadRuntimeChart(link) {
        this.httpClient.get<any>(link).pipe(
            map(res => ({
                ...res, ...{ id: uuid(), dragEnabled: false, resizeEnabled: false, x: 0, y: 0, rows: 20, cols: 48 }
            })),
            takeUntil(this._unsubscribeAll)).subscribe((response: any[]) => {
                this.listOfCharts = [{ ...response, cols: 96, rows: 38, y: 0, x: 0 }];
            });
    }
}
