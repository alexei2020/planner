
import { Component, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { CompactType, GridsterItem, GridType, GridsterConfig } from 'angular-gridster2';

import { Subject } from 'rxjs';
import { takeUntil, first } from 'rxjs/operators';

import { NotificationService } from '../../../main/shared/services';
import { Widget } from '../../../main/widget';
import { ReportMenuService } from 'app/main/shared/services/report-menu.service';
import { ItemType } from 'app/main/shared/services/report-menu.enum.gen';
import { BreadcrumbService, EventType } from '../../../main/shared/breadcrumb-bar.service';
import { PageType } from '../../../main/shared/breadcrumb-bar.service.gen';
import { AddDialogComponent } from '../../../main/shared/add-dialog/add-dialog.component';
import { ConfirmDialogComponent } from '../../../main/shared/confirm-dialog/confirm-dialog.component';
import { DialogService } from 'app/main/shared/dialog/dialog.service';

@Component({
    selector: 'report-page',
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class ReportsPageComponent implements OnInit, OnDestroy {
    public prevSelectedLayout: any[];
    public prevLocation: string;
    public ItemType = ItemType;
    public queryParams: any;
    public reportLayouts: any[];
    public reports: any[];
    public selectedLayout: any;

    public options: GridsterConfig = {
        gridType: GridType.VerticalFixed,
        compactType: CompactType.None,
        margin: 5,
        minCols: 96,
        maxCols: 96,
        minRows: 96,
        maxRows: 1300,
        maxItemCols: 96,
        minItemCols: 1,
        maxItemRows: 1000,
        minItemRows: 1,
        maxItemArea: 500000,
        minItemArea: 1,
        fixedRowHeight: 10,
        draggable: {
            delayStart: 0,
            enabled: false,
            ignoreContentClass: 'gridster-item-content',
            ignoreContent: true,
            dragHandleClass: 'drag-handler',
            stop: ($event) => { }
        },
        resizable: {
            enabled: true,
        },
        swap: false,
        pushItems: true,
    };

    public resizeEvent: EventEmitter<any> = new EventEmitter<any>();
    public data: any;
    private _unsubscribeAll: Subject<any> = new Subject();

    public reportConfigurations: GridsterItem[] = [];

    constructor(
        private _dialog: MatDialog,
        private _router: Router,
        private _route: ActivatedRoute,
        private _reportMenuService: ReportMenuService,
        private _breadcrumbService: BreadcrumbService,
        private _noticeService: NotificationService,
        private _dialogService: DialogService,
    ) {
        this.reportConfigurations = [];
    }

    get layout(): Widget[] {
        return this.reportConfigurations;
    }

    ngOnInit(): void {
        this._breadcrumbService.triggeredEvent.pipe(takeUntil(this._unsubscribeAll)).subscribe((event) => {
            switch (event.type) {
                case EventType.REPORT_ADD:
                    this.addItem();
                    break;

                case EventType.REPORT_SAVE:
                    this.saveItem();
                    break;

                case EventType.REPORT_CLONE:
                    this.cloneItem();
                    break;

                case EventType.REPORT_DELETE:
                    this.deleteItem();
                    break;

                default:
                    break;
            }
        });

        this._route.params.subscribe(() => {
            this._breadcrumbService.currentPage.next(PageType.REPORT);
        });

        this._route.queryParams.subscribe((params) => {
            this.queryParams = params;
            //get reports
            if (this.queryParams && this.queryParams.loc && this.prevLocation !== this.queryParams.loc) {
                this.prevLocation = this.queryParams.loc;
                this._reportMenuService.getReports(this.queryParams.loc).subscribe((res: any) => {
                    this.reportLayouts = [];
                    setTimeout(() => {
                        const report = res.items;
                        this.reports = report;
                        this._breadcrumbService.item.next(this.reports);
                        report.map((item) => {
                            this.reportLayouts.push({
                                type: item.metadata.name,
                                name: item.metadata.name,
                                uid: item.metadata.uid,
                                layout: item.spec.layout
                            });
                        });
                        if (
                            this.queryParams === undefined ||
                            this.queryParams.layout === undefined
                        ) {
                            this._router.navigate([], {
                                queryParams: { layout: this.reportLayouts[0].name }
                            });
                            this._breadcrumbService.queryParams.next({
                                layout: this.reportLayouts[0].name
                            });
                            if (this.reportConfigurations.length === 0) {
                                this.reportConfigurations = JSON.parse(this.reportLayouts[0].layout);
                            }
                        } else {
                            this.selectedLayout = this.reportLayouts.filter(
                                (item) => item.name === this.queryParams.layout
                            );
                            if (this.selectedLayout && this.selectedLayout.length > 0 && this.reportConfigurations.length === 0)
                                this.reportConfigurations = this.selectedLayout[0].layout ? JSON.parse(this.selectedLayout[0].layout) : [];
                            else {
                                if (this.reportConfigurations.length === 0) {
                                    this.reportConfigurations = this.reportLayouts[0].layout ? JSON.parse(this.reportLayouts[0].layout) : [];
                                }
                                this._router.navigate([], {
                                    queryParams: {
                                        layout: this.reportLayouts[0].name
                                    }
                                });
                                this._breadcrumbService.queryParams.next({
                                    layout: this.reportLayouts[0].name
                                });
                            }
                        }
                        this.reportConfigurations.forEach(widget => {
                            widget.resizeEnabled = false;
                            widget.dragEnabled = false;
                        });
                        this._breadcrumbService.layouts.next(this.reportLayouts);
                    }, 10);
                }
                    ,
                    () => {
                        this.reportLayouts = [];
                        this._breadcrumbService.layouts.next(this.reportLayouts);
                        this.reports = [];
                        this._breadcrumbService.item.next(this.reports);
                        this.reportConfigurations = [];
                        this._noticeService.openSnackBar('Error loading report', '');
                    });
            }

            if (
                (this.queryParams === undefined ||
                    this.queryParams.layout === undefined) &&
                (this.reportLayouts !== undefined && this.reportLayouts.length > 0)
            ) {
                this._router.navigate([], {
                    queryParams: { layout: this.reportLayouts[0].name }
                });
                this._breadcrumbService.queryParams.next({
                    layout: this.reportLayouts[0].name
                });
                this.reportConfigurations = JSON.parse(this.reportLayouts[0].layout);
            } else {
                if (this.reportLayouts && this.reportLayouts.length > 0) {
                    this.selectedLayout = this.reportLayouts.filter(
                        (item) => item.name === this.queryParams.layout
                    );
                    if (this.prevSelectedLayout) {
                        if (this.prevSelectedLayout[0].uid !== this.selectedLayout[0].uid) {
                            this.reportConfigurations = this.selectedLayout[0].layout ? JSON.parse(this.selectedLayout[0].layout) : [];
                        }
                    } else {
                        this.reportConfigurations = this.selectedLayout[0].layout ? JSON.parse(this.selectedLayout[0].layout) : [];
                    }
                    this.prevSelectedLayout = this.selectedLayout;
                }
                setTimeout(() => {
                    this._breadcrumbService.queryParams.next(params);
                }, 10);
            }
        });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
    }

    changeGridsterChartState() {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }
	changeGridsterState(widget?: any) {
		if (widget) {
			widget.resizeEnabled = !widget.resizeEnabled;
			widget.dragEnabled = !widget.dragEnabled;
		}
		this.gridsterStateChanged();
	}
	public gridsterStateChanged() {
		if (this.options.api && this.options.api.optionsChanged) {
			this.options.api.optionsChanged();
		}
	}

	setHeightGridster(tableHeight, targetItem, tableHeaderHeight, otherItems) {
        const previousRow = targetItem.rows;
        targetItem.rows = Math.ceil((tableHeight + tableHeaderHeight) / 15);
        this.gridsterStateChanged();
        if (otherItems) {
            otherItems = otherItems.map((item) => {
                if (previousRow < targetItem.rows) {
                    if (item.y > targetItem.y) {
                        item.y = item.y + (targetItem.rows - previousRow);
                    }
                } else {
                    if (item.y > targetItem.y) {
                        item.y = item.y - (previousRow - targetItem.rows);
                    }
                }
                return item;
            });
        }
        this.gridsterStateChanged();
    }

    updateLayout(layout) {
        this._router.navigate([], { queryParams: { layout: layout.name } });
    }

    removeReport(item: any, index) {
        this._dialogService
            .openConfirmDialog({ title: 'Remove Widget', msg: 'Are you sure you want to remove widget?' })
            .afterClosed()
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((res) => {
                if (res) {
                    this.reportConfigurations.splice(index, 1); this.reportConfigurations.splice(index, 1); this.reportConfigurations.splice(index, 1); this.reportConfigurations.splice(index, 1);
                }
            });

    }

    private addItem() {
        const dialogRef = this._dialog.open(AddDialogComponent, {
            width: '300px',
            data: { name: null, title: 'Report Name' }
        });
        dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe((response) => {
            if (response) {
                let newItem = {
                    apiVersion: `${this.queryParams['loc']}/v1`,
                    kind: 'Report',
                    metadata: {
                        name: response,
                    },
                    spec: { layout: `[]` }
                }
                this._reportMenuService.addReports(newItem, this.queryParams.loc).pipe(takeUntil(this._unsubscribeAll))
                    .subscribe(
                        (report: any) => {
                            this._noticeService.openSnackBar('Report add successfully', '');
                            const layout = {
                                type: report.metadata.name,
                                name: report.metadata.name,
                                layout: report.spec.layout,
                                uid: report.metadata.uid
                            }
                            this.reports.push(report);
                            this.reportLayouts.push(layout);
                            this._breadcrumbService.layouts.next(this.reportLayouts);
                            this._breadcrumbService.item.next(this.reports);
                            this.updateLayout(layout);
                        },
                        () => {
                            this._noticeService.openSnackBar(`Failed to added report`, ``);
                        }
                    );
            }
        });
    }

    private saveItem() {
        const layout = this.reportConfigurations;
        const selectedUserItem = this.reports.filter(
            (item) => item.metadata.name === this.selectedLayout[0].type
        )[0];
        selectedUserItem.spec.layout = JSON.stringify(layout);
        this._reportMenuService.saveReport(selectedUserItem.metadata.name, selectedUserItem, this.queryParams['loc'])
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                (res: any) => {
                    let objIndex = this.reports.findIndex((item) => item.metadata.name === this.selectedLayout[0].type);
                    this.reports[objIndex] = res;
                    this.reportLayouts[objIndex] = {
                        type: res.metadata.name,
                        name: res.metadata.name,
                        layout: res.spec.layout
                    };
                    this._breadcrumbService.layouts.next(this.reportLayouts);
                    this._breadcrumbService.item.next(this.reports);
                    this.updateLayout(this.selectedLayout[0]);
                    this._noticeService.openSnackBar('Report saved successfully', '');
                },
                () => {
                    this._noticeService.openSnackBar('Failed to saved report', '');
                }
            );
    }

    private cloneItem() {
        const dialogRef = this._dialog.open(AddDialogComponent, {
            data: { name: 'Copy of ' + this.selectedLayout[0].name }
        });
        dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe((response) => {
            if (response) {
                let selectedUserDashboard = this.reports.find((item) => item.metadata.name === this.selectedLayout[0].type);

                if (selectedUserDashboard) {
                    selectedUserDashboard = {
                        ...selectedUserDashboard,
                        metadata: {
                            name: response,
                        },
                        spec: {
                            layout: JSON.stringify(this.reportConfigurations)
                        }
                    }

                    this._reportMenuService.cloneReport(selectedUserDashboard, this.queryParams['loc'])
                        .pipe(takeUntil(this._unsubscribeAll))
                        .subscribe(
                            (report: any) => {
                                // Add regexp
                                /// [a-z0-9]([-a-z0-9][a-z0-9])?(.[a-z0-9]([-a-z0-9][a-z0-9])?)*
                                const layout = {
                                    type: report.metadata.name,
                                    name: report.metadata.name,
                                    layout: report.spec.layout,
                                    uid: report.metadata.uid
                                }
                                this.reportLayouts.push(layout);
                                this.reports.push(report);
                                this._noticeService.openSnackBar('Report saved successfully', '');
                                this._breadcrumbService.layouts.next(this.reportLayouts);
                                this._breadcrumbService.item.next(this.reports);
                                this.updateLayout(layout);
                            },
                            () => {
                                this._noticeService.openSnackBar('Failed to saved report', '');
                            }
                        );
                }
            }
        });
    }

    private deleteItem() {
        const dialogRef = this._dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Are you sure to delete the current report?'
            }
        });

        dialogRef.afterClosed().subscribe((response) => {
            if (response) {
                this._reportMenuService.deleteReport(this.selectedLayout[0].name, this.queryParams['loc'])
                    .pipe(first())
                    .subscribe(
                        () => {
                            this.reportConfigurations = [];
                            this._noticeService.openSnackBar('Report removed successfully', '');
                            this.reports = this.reports.filter(item => item.metadata.uid !== this.selectedLayout[0].uid)
                            this._breadcrumbService.item.next(this.reports);
                            this.reportLayouts = this.reportLayouts.filter(item => item.uid !== this.selectedLayout[0].uid)
                            this._breadcrumbService.layouts.next(this.reportLayouts);
                            this._router.navigate([], { queryParams: {} });
                        },
                        () => {
                            this._noticeService.openSnackBar('Failed to removed report', '');
                        }
                    );
            }
        });
    }

    public viewChartWidget(record) {
        console.log('test', record);
        this._router.navigate(['reports', 'chart', this.selectedLayout[0].name, record.id], { queryParamsHandling: 'preserve' });
    }

}
