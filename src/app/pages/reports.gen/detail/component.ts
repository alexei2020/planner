


import { Component, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { CompactType, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { ActivatedRoute } from '@angular/router';
import { ReportMenuService } from 'app/main/shared/services/report-menu.service';
import { ItemType } from 'app/main/shared/services/report-menu.enum.gen';
import { combineLatest, Subject } from 'rxjs';
import { AddItemDialogComponent } from 'app/main/shared/add-item-dialog/add-item-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from 'app/main/shared/services';
import { takeUntil } from 'rxjs/operators';

@Component({
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class ReportsDetailPageComponent implements OnInit, OnDestroy {
    public pageName: string;
    public resourceName: string;
    public options: GridsterConfig;
    public resizeEvent: EventEmitter<any> = new EventEmitter<any>();
    public data: any;
    public item1: GridsterItem;
    public item2: GridsterItem;
    public item3: GridsterItem;
    public item4: GridsterItem;
    public item5: GridsterItem;
    public item6: GridsterItem;
    public item7: GridsterItem;
    public item8: GridsterItem;
    public item9: GridsterItem;
    public item10: GridsterItem;
    public item11: GridsterItem;
    public item12: GridsterItem;
    public item13: GridsterItem;
    public item14: GridsterItem;
    public item15: GridsterItem;
    public item16: GridsterItem;
    public item17: GridsterItem;
    public item18: GridsterItem;
    public item19: GridsterItem;
    public item20: GridsterItem;
    public item21: GridsterItem;
    public item22: GridsterItem;
    public item23: GridsterItem;
    public item24: GridsterItem;
    public item25: GridsterItem;
    public item26: GridsterItem;
    public item27: GridsterItem;
    public item28: GridsterItem;
    public item29: GridsterItem;
    public item30: GridsterItem;
    public item31: GridsterItem;
    public item32: GridsterItem;
    public item33: GridsterItem;
    public item34: GridsterItem;
    public item35: GridsterItem;
    public item36: GridsterItem;
    public item37: GridsterItem;
    public item38: GridsterItem;
    public item39: GridsterItem;
    public item40: GridsterItem;
    public item41: GridsterItem;
    public item42: GridsterItem;
    public item43: GridsterItem;
    public item44: GridsterItem;
    public item45: GridsterItem;
    public item46: GridsterItem;
    public item47: GridsterItem;
    public item48: GridsterItem;
    public item49: GridsterItem;
    public item50: GridsterItem;
    public item51: GridsterItem;
    public item52: GridsterItem;
    public item53: GridsterItem;

    private _unsubscribeAll: Subject<any> = new Subject();

    public reportConfigurations: GridsterItem[] = [];
    
    constructor(
        private activatedRoute: ActivatedRoute,
        private _dialog: MatDialog,
    ) {
        this.options = {
            swapping: false,
            itemResizeCallback: (item) => {
                this.resizeEvent.emit(item);
            },
            gridType: GridType.ScrollVertical,
            compactType: CompactType.None,
            margin: 8,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1300,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            minItemArea: 1,
            resizable: {
                enabled: false
            },
            draggable: {
                delayStart: 0,
		        enabled: false,
		        ignoreContentClass: 'gridster-item-content',
		        ignoreContent: true,
		        dragHandleClass: 'drag-handler',
		        stop: ($event) => { }
            },
        };
    }

    ngOnInit(): void {
        this.activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
            this.resourceName = params['resourceName'];
            this.pageName = params['pageName'];
        });
        this.item1 = { reportType: ItemType.ADMITDEVICE_ADMIT_DEVICE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item2 = { reportType: ItemType.ATTRIBUTESET_ATTRIBUTE_SET_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item3 = { reportType: ItemType.AUDIT_AUDIT_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item4 = { reportType: ItemType.DEVICE_INTERFACE_CONFIG_DETAILCARD, id: this.resourceName, cols: 24, rows: 32, y: 0, x: 0 };
        this.item5 = { reportType: ItemType.DEVICE_PLATFORM_COMPONENT_DETAILS_DETAILCARD, id: this.resourceName, cols: , rows: , y: , x:  };
        this.item6 = { reportType: ItemType.IFINTERFACE_DETAILCARD, id: this.resourceName, cols: 96, rows: 36, y: 0, x: 0 };
        this.item7 = { reportType: ItemType.PLATFORMCOMPONENT_DETAILCARD, id: this.resourceName, cols: 96, rows: 36, y: 0, x: 0 };
        this.item8 = { reportType: ItemType.DEVICE_SWITCHDETAIL_DETAILCARD, id: this.resourceName, cols: 92, rows: 4, y: 0, x: 0 };
        this.item9 = { reportType: ItemType.DEVICE_ETHERNET_CONFIG_DETAILCARD, id: this.resourceName, cols: 24, rows: 32, y: 0, x: 0 };
        this.item10 = { reportType: ItemType.DEVICE_TEMPERATURE_DETAILS_DETAILCARD, id: this.resourceName, cols: , rows: , y: , x:  };
        this.item11 = { reportType: ItemType.DEVICE_IP_DETAILCARD, id: this.resourceName, cols: 16, rows: 17, y: 0, x: 81 };
        this.item12 = { reportType: ItemType.DEVICE_GENERAL_DETAILCARD, id: this.resourceName, cols: 32, rows: 36, y: 0, x: 0 };
        this.item13 = { reportType: ItemType.DEVICE_MEMORY_DETAILS_DETAILCARD, id: this.resourceName, cols: , rows: , y: , x:  };
        this.item14 = { reportType: ItemType.DEVICE_NEIGHBOR_DISCOVERY_DETAILCARD, id: this.resourceName, cols: 24, rows: 32, y: 0, x: 0 };
        this.item15 = { reportType: ItemType.DEVICE_LAYER2_STATUS_DETAILCARD, id: this.resourceName, cols: 24, rows: 32, y: 0, x: 0 };
        this.item16 = { reportType: ItemType.DEVICE_POWER_DETAILS_DETAILCARD, id: this.resourceName, cols: , rows: , y: , x:  };
        this.item17 = { reportType: ItemType.DEVICE_STATUS_DETAILCARD, id: this.resourceName, cols: 32, rows: 36, y: 0, x: 0 };
        this.item18 = { reportType: ItemType.DEVICE_DEVICESVICONFIG_DETAILCARD, id: this.resourceName, cols: 32, rows: 36, y: 0, x: 0 };
        this.item19 = { reportType: ItemType.DEVICEATTRIBUTESET_DEVICE_ATTRIBUTE_SET_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item20 = { reportType: ItemType.DEVICEUI_DEVICE_UI_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item21 = { reportType: ItemType.ENDPOINT_INDEX_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 0, x: 0 };
        this.item22 = { reportType: ItemType.ENDPOINT_ATTRIBUTES_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 0, x: 24 };
        this.item23 = { reportType: ItemType.ENDPOINT_CONNECTIVITY_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 0, x: 72 };
        this.item24 = { reportType: ItemType.ENDPOINT_AUTHENTICATION_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 44, x: 0 };
        this.item25 = { reportType: ItemType.ENDPOINT_DHCP_DETAILCARD, id: this.resourceName, cols: 24, rows: 44, y: 44, x: 24 };
        this.item26 = { reportType: ItemType.ENDPOINTAUX_END_POINT_AUX_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item27 = { reportType: ItemType.ENDPOINTSET_ENDPOINT_SET_CONDITIONS_DETAILCARD, id: this.resourceName, cols: 96, rows: 10, y: 0, x: 0 };
        this.item28 = { reportType: ItemType.ENDPOINTSET_TEMPLATE_DETAILCARD, id: this.resourceName, cols: 32, rows: 10, y: 0, x: 0 };
        this.item29 = { reportType: ItemType.FILE_FILE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item30 = { reportType: ItemType.FLOORSPACE_FLOOR_SPACE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item31 = { reportType: ItemType.IFINTERFACE_INTERFACE_CONFIG_DETAILCARD, id: this.resourceName, cols: 24, rows: 32, y: 0, x: 0 };
        this.item32 = { reportType: ItemType.IFINTERFACE_ETHERNET_CONFIG_DETAILCARD, id: this.resourceName, cols: 24, rows: 32, y: 0, x: 0 };
        this.item33 = { reportType: ItemType.IFINTERFACE_NEIGHBOR_DISCOVERY_DETAILCARD, id: this.resourceName, cols: 24, rows: 32, y: 0, x: 0 };
        this.item34 = { reportType: ItemType.IFINTERFACE_LAYER2_STATUS_DETAILCARD, id: this.resourceName, cols: 24, rows: 32, y: 0, x: 0 };
        this.item35 = { reportType: ItemType.LOCATION_LOCATION_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item36 = { reportType: ItemType.NETWORKTEMPLATE_TEMPLATE_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 0 };
        this.item37 = { reportType: ItemType.NETWORKTEMPLATE_FEATURES_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 32 };
        this.item38 = { reportType: ItemType.NETWORKTEMPLATE_DNSSERVER_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 64 };
        this.item39 = { reportType: ItemType.NETWORKTEMPLATE_FLOWCONFIG_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 42, x: 0 };
        this.item40 = { reportType: ItemType.NETWORKTEMPLATE_NETWORKMON_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 84, x: 0 };
        this.item41 = { reportType: ItemType.NETWORKTEMPLATE_NETWORKPROV_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 126, x: 0 };
        this.item42 = { reportType: ItemType.PLATFORMCOMPONENT_PLATFORM_COMPONENT_DETAILS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 0 };
        this.item43 = { reportType: ItemType.PLATFORMCOMPONENT_TEMPERATURE_DETAILS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 32 };
        this.item44 = { reportType: ItemType.PLATFORMCOMPONENT_MEMORY_DETAILS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 0, x: 64 };
        this.item45 = { reportType: ItemType.PLATFORMCOMPONENT_POWER_DETAILS_DETAILCARD, id: this.resourceName, cols: 32, rows: 42, y: 42, x: 0 };
        this.item46 = { reportType: ItemType.RADIO_RADIO_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item47 = { reportType: ItemType.RADIOINFO_RADIO_INFO_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item48 = { reportType: ItemType.SNAPSHOTTASK_SNAPSHOT_TASK_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item49 = { reportType: ItemType.SSID_SSID_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item50 = { reportType: ItemType.SUBINTERFACE_SUBINTERFACE_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item51 = { reportType: ItemType.TENANT_TENANT_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item52 = { reportType: ItemType.WALLJACK_WALL_JACK_DETAILS_DETAILCARD, id: this.resourceName, cols: 96, rows: 42, y: 0, x: 0 };
        this.item53 = { cols: 96, rows: 38, y: 0, x: 0 };
    }

    changeGridsterState(widget: any) {
        widget.resizeEnabled = !widget.resizeEnabled;
        widget.dragEnabled = !widget.dragEnabled;
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    onAddToReport(item: any) {
        const dialogRef = this._dialog.open(AddItemDialogComponent, {
            width: '250px',
            data: { title: 'Show in reports', item }
        });
    }
    
    ngOnDestroy(): void {
        this._unsubscribeAll?.next();
        this._unsubscribeAll?.complete();
    }
}

