
import { Component, EventEmitter, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { CompactType, GridsterConfig, GridsterItem, GridsterItemComponent, GridsterPush, GridType, GridsterSwap } from 'angular-gridster2';

import { combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { v4 as uuid } from 'uuid';

import { BreadcrumbService } from '../../../main/shared/breadcrumb-bar.service';
import { ReportMenuService } from '../../../main/shared/services/report-menu.service';
import { PageType } from 'app/main/shared/breadcrumb-bar.service';
import { ItemType } from 'app/main/shared/services/report-menu.enum';
import { MatDialog } from '@angular/material/dialog';
import { AddItemDialogComponent } from '../../../main/shared/add-item-dialog/add-item-dialog.component';
import { AddItemDashboardDialogComponent } from 'app/main/shared/add-item-dashboard-dialog/add-item-dashboard-dialog.component';

@Component({
    templateUrl: './component.html',
    styleUrls: ['./component.scss']
})
export class SystemListPageComponent implements OnInit {
    public options: GridsterConfig;
    public resizeEvent: EventEmitter<any> = new EventEmitter<any>();
    public queryParams: any;
    public resizeble: boolean;
    public prevTableHeight = 0;

    public ItemType = ItemType;

    public list = [
        { id: null, reportType: ItemType.SYSTEM_LIST, x: 0, y: 0, rows: 38, cols: 96, compactEnabled: true, dragEnabled: false, resizeEnabled: false, initCallback: (value1, value2: GridsterItemComponent) => this.initItem(value1, value2, 0) },
    ];
    public listForPush = new Array<GridsterItemComponent>(0);

    public resizeTimer: any;
    public pageSize = { label: '10', value: 10 };
    public pageSizes = [{ label: '5', value: 5 }, { label: '10', value: 10 }, { label: '20', value: 20 }];

    public widget: any;
    public linksObj = [
        'assets/json/chart1.json',
        'assets/json/chart2.json',  
    ]
    public listOfCharts: any[];
    public range = { label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 };
    public data: any;
    public isSelectedForReport: boolean;

    private _unsubscribeAll: Subject<any> = new Subject();
    private itemToPush: GridsterItemComponent;

    constructor(
        private _breadcrumbService: BreadcrumbService,
        private _dialog: MatDialog,
        private _httpClient: HttpClient,
        private _reportMenuService: ReportMenuService,
        private _router: Router,
    ) {

        this._reportMenuService.getSelectedItemObs().pipe(takeUntil(this._unsubscribeAll)).subscribe(items => {
            this.isSelectedForReport = this._reportMenuService.itemIsChecked(ItemType.SYSTEM_LIST);
        });

        this._breadcrumbService.currentPage.next(PageType.SYSTEM_LIST);

        this.options = {
            gridType: GridType.VerticalFixed,
            compactType: CompactType.None,
            margin: 5,
            minCols: 96,
            maxCols: 96,
            minRows: 96,
            maxRows: 1300,
            maxItemCols: 96,
            minItemCols: 1,
            maxItemRows: 1000,
            minItemRows: 1,
            maxItemArea: 500000,
            fixedRowHeight: 10,
            minItemArea: 1,
            mobileBreakpoint: 300,
            keepFixedHeightInMobile: false,
            scrollVertical: true,
            compactUp: false,
            draggable: {
                delayStart: 0,
                enabled: true,
                ignoreContentClass: 'gridster-item-content',
                ignoreContent: true,
                dragHandleClass: 'drag-handler',
                stop: ($event) => { }
            },
            resizable: {
                enabled: true,
            },
            swap: false,
            pushItems: true,
        };
    }

    ngOnInit(): void {
        this.loadChart();
    }


    initItem(item: GridsterItem, itemComponent: GridsterItemComponent, index) {
        this.listForPush[index] = itemComponent;;
    }

    setHeightGridster(index, targetItem, tableHeight) {
        const previousRow: number = targetItem.rows;
        const rows = Math.ceil((tableHeight + 50) / 15);
        if (this.listForPush[index]) {
            this.pushItem(this.listForPush[index], previousRow, rows);
        }
    }

    pushItem(itemToPush, previousRow, currentRow) {
        const push = new GridsterPush(itemToPush); // init the service
        itemToPush.$item.rows = currentRow; // move/resize your item

        const listOfEnabledItems = this.list.filter(item => item.dragEnabled).map(item => item.id);
        this.list.forEach(item => {
            item.dragEnabled = true;
            item.resizeEnabled = true;
        });
        this.updateOptions();

        itemToPush.$item.rows = currentRow;

        if (push.pushItems(push.fromNorth) || push.pushItems(push.fromSouth)) { // push items from a direction
            push.checkPushBack(); // check for items can restore to original position
            push.setPushedItems(); // save the items pushed
            itemToPush.setSize();
            itemToPush.checkItemChanges(itemToPush.$item, itemToPush.item);
        } else {
            itemToPush.$item.rows = previousRow;
            push.restoreItems(); // restore to initial state the pushed items
        }
        this.list.forEach(record => {
            const enabled = listOfEnabledItems.some(id => record.id === id);
            record.dragEnabled = enabled;
            record.resizeEnabled = enabled;
        });
        this.updateOptions();

        push.destroy(); // destroy push instance
    }

    getUid(uidItem: any) {
        const targetItem = this.list.find(item => item.reportType === uidItem.ItemType)
        if (targetItem) {
            targetItem.id = uidItem.uid;
        }
    }

    updateOptions() {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }

    changeGridsterState(widget: any, general = true) {
        if (general) {
            widget.resizeEnabled = !widget.resizeEnabled;
            widget.dragEnabled = !widget.dragEnabled;
        }
        this.updateOptions();
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    public setForReport() {
        this._reportMenuService.setItemObs(ItemType.SYSTEM_LIST);
    }

    onAddToReport(item: any) {
        const dialogRef = this._dialog.open(AddItemDialogComponent, {
            width: '300px',
            data: { title: 'Show in reports', item }
        });
    }

    onAddToDashboard(item: any) {
        const dialogRef = this._dialog.open(AddItemDashboardDialogComponent, {
            width: '300px',
            data: { title: 'Show in dashboards', item }
        });
    }

    customViewWidget(widget) {
        this._router.navigate(['systems', 'runtime-chart'], { queryParams: { chartLink: widget.chartLink } })
    }

    private loadChart() {
        combineLatest(
            this.linksObj.map((link, index) => this._httpClient.get<any>(link).pipe(map((res) => ({
                ...res,
                ...{
                    id: uuid(),
                    reportType: ItemType.CHART,
                    dragEnabled: false,
                    resizeEnabled: true,
                    x: 0, y: 0, rows: 20, cols: 48,
                    chartLink: this.linksObj[index],
                    initCallback: (value1, value2: GridsterItemComponent) => this.initItem(value1, value2, index + 2)
                }
            }))))
        ).pipe(takeUntil(this._unsubscribeAll)).subscribe((response: any[]) => {
            this.list = [...this.list, ...response];
        });
    }
}
