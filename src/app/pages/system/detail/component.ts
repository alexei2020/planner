
    import { Component, EventEmitter, OnInit } from '@angular/core';
    import { HttpClient } from '@angular/common/http';
    
    import { combineLatest } from 'rxjs';
    import { map, first } from 'rxjs/operators';
    
    import { v4 as uuid } from 'uuid';
    
    import { CompactType, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
    import { DashboardService, TimerService } from '../../../main/shared/services';
    import { BreadcrumbService } from '../../..//main/shared/breadcrumb-bar.service';
    import { ActivatedRoute, Router } from '@angular/router';
    import { ReportMenuService } from 'app/main/shared/services/report-menu.service';
    import { ItemType } from 'app/main/shared/services/report-menu.enum';
    import { Subject } from 'rxjs';
    import { takeUntil } from 'rxjs/operators';
    import { AddItemDialogComponent } from 'app/main/shared/add-item-dialog/add-item-dialog.component';
    import { MatDialog } from '@angular/material/dialog';
    import { AddItemDashboardDialogComponent } from 'app/main/shared/add-item-dashboard-dialog/add-item-dashboard-dialog.component';
    import { FuseLocationService } from 'app/services/location.service';
    import { PageType } from 'app/main/shared/breadcrumb-bar.service';
    import { LabelTableDialogComponent } from 'app/resource/label/dialog/component';
    import { SystemDetailService } from 'app/resource/system/services';

    @Component({
        templateUrl: './component.html',
        styleUrls: ['./component.scss']
    })
    export class SystemDetailPageComponent implements OnInit {
        public resourceName: string;
        public options: GridsterConfig;
        public itemType = ItemType;
        public itemLabel = { cols: 96, rows: 3, y: 0, x: 0 };
        public resizeEvent: EventEmitter<any> = new EventEmitter<any>();


        public selectors = [
        ];

	public activeDashboards = [];
	public staticDashboards = [
            { id: uuid(), reportType: ItemType.LABELS, cols: 96, rows: 0, y: 0, x: 0 },
            { id: uuid(), reportType: ItemType.SYSTEM_SYSTEM_DETAILCARD, x: 0, y: 0, rows: 42, cols: 48, compactEnabled: true, dragEnabled: false, resizeEnabled: false },
            { id: uuid(), reportType: ItemType.SYSTEMCPUSTATUS_DETAILCARD, x: 48, y: 0, rows: 42, cols: 48, compactEnabled: true, dragEnabled: false, resizeEnabled: false },
	];
        public defaultDashboards = [
        ];

        item3: GridsterItem;
        public items: GridsterItem[];
        labels: any;
        public data: any;
        public system: any;
        currentLocation: any;
        itemSwitch: GridsterItem;
        public linksObj = [
            'assets/json/chart1.json',
            'assets/json/chart2.json',
        ]
        public listOfCharts: any[];
        public isSelectedForReportSystemSystem: boolean;
        public isSelectedForReportSystemcpustatus: boolean;
    
        private _unsubscribeAll: Subject<any> = new Subject();
    
        constructor(
            private dashboardSvc: DashboardService,
            private _breadcrumbService: BreadcrumbService,
            private _timer: TimerService,
            private httpClient: HttpClient,
            private activatedRoute: ActivatedRoute,
            private _reportMenuService: ReportMenuService,
            private _dialog: MatDialog,
            private _router: Router,
            private _locationService : FuseLocationService,
            private _systemDetailService: SystemDetailService
        ) {
            this._locationService.disableLocationSelector();
            this.options = {
                swapping: false,
                itemResizeCallback: (item) => {
                    this.resizeEvent.emit(item);
                },
                gridType: GridType.VerticalFixed,
                compactType: CompactType.None,
                margin: 5,
                minCols: 96,
                maxCols: 96,
                minRows: 96,
                maxRows: 1300,
                maxItemCols: 96,
                minItemCols: 1,
                maxItemRows: 1000,
                minItemRows: 1,
                maxItemArea: 500000,
                minItemArea: 1,
                fixedRowHeight: 10,
                resizable: {
                    enabled: false
                },
                draggable: {
                    delayStart: 0,
                    enabled: false,
                    ignoreContentClass: 'gridster-item-content',
                    ignoreContent: true,
                    dragHandleClass: 'drag-handler',
                    stop: ($event) => { }
                }
            };
            this._reportMenuService.getSelectedItemObs().pipe(takeUntil(this._unsubscribeAll)).subscribe(items => {
                this.isSelectedForReportSystemSystem = this._reportMenuService.itemIsChecked(ItemType.SYSTEM_SYSTEM_DETAILCARD, this.resourceName);
                this.isSelectedForReportSystemcpustatus = this._reportMenuService.itemIsChecked(ItemType.SYSTEMCPUSTATUS_DETAILCARD, this.resourceName);
            });
            
            this._breadcrumbService.currentPage.next(PageType.DEVICE_DETAIL);
            this._breadcrumbService.platformSelected.next(null);
            this._breadcrumbService.selectedPlatform.next(this.selectors);
        }
    
        ngOnInit(): void {
            this.activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe((params) => {
                this.resourceName = params['resourceName'];
                this.currentLocation = this.activatedRoute.snapshot.queryParams.loc;
            this.isSelectedForReportSystemSystem = this._reportMenuService.itemIsChecked(ItemType.SYSTEM_SYSTEM_DETAILCARD, this.resourceName);
            this.isSelectedForReportSystemcpustatus = this._reportMenuService.itemIsChecked(ItemType.SYSTEMCPUSTATUS_DETAILCARD, this.resourceName);
                this._systemDetailService.load(this.resourceName, this.currentLocation).pipe(first()).subscribe(
                    (response) => {
                        this.system = response;
                        this.updateLabels();
                    },
                    () => {
                        console.log('some error');
                        this.system = null;
                    }
                );
            });

            this.loadRuntimeChart();
            this._timer.setDateRangeObs(this._breadcrumbService.setRange({ label: 'Last 2 days', short: '2d', start: 0, end: 0, step: 2880 }));
            this._timer.setPanel(true);
            
            //get queries
            this.dashboardSvc.getQueries().pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
                this.dashboardSvc.setQueriesObs(res['items']);
            });
    
            //get charts
            this.dashboardSvc.getCharts().pipe(takeUntil(this._unsubscribeAll)).subscribe((res) => {
                this.dashboardSvc.setChartsObs(res['items']);
            });

            this._breadcrumbService.triggeredLabelRuleEvent.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
                const dialogRef = this._dialog.open(LabelTableDialogComponent, {
                    width: '90%',
                    minHeight: '245px',
                    data: {
                        singleSelection: true,
                        label: this.labels
                    }
                });
                dialogRef.afterClosed().pipe(takeUntil(this._unsubscribeAll)).subscribe((response: any[]) => {
                    if (!response) {
                        return;
                    }
    
                    this.labels = response.map(item => ({
                        name: item.metadata.name,
                        values: item.selectedValues
                    }));
    
                    if (this.labels.length > 0 && this.labels.filter(item => item.values.length > 0).length > 0) {
                        this.system.metadata.labels = this.labels.filter(item => item.values.length > 0).reduce((acc, cur) => {
                            acc[cur.name] = cur.values[0];
                            return acc;
                        }, {});
                    } else {
                        this.system.metadata.labels = {}
                    }
    
                    this._systemDetailService.update(this.system, this.currentLocation).pipe(first()).subscribe(system => {
                        this.system = system;
                        this.updateLabels();
                    });
                });
            });

            this._breadcrumbService.platformSelected.pipe(takeUntil(this._unsubscribeAll)).subscribe((platform) => {
                console.log(platform)
		if (platform) {
			switch (platform.id) {
			default:
				this.activeDashboards = this.staticDashboards.concat(this.defaultDashboards);
			}
		} else {
			this.activeDashboards = this.staticDashboards.concat(this.defaultDashboards);
		}
            });

            this._breadcrumbService.optionalDetailsSelected.pipe(takeUntil(this._unsubscribeAll)).subscribe((option) => {
                console.log(option)
            });
        }


        updateLabels() {
            const labels = this.system.metadata.labels || {};
            this.labels =  Object.keys(labels).map(name => ({ name, values: [labels[name]] }));
            const iconStyle = this.labels.length > 0 ? 'label-outline' : 'label-off';
            this._breadcrumbService.iconStyle.next(iconStyle);

            const labelGridsterItem = this.activeDashboards[0];

            const listOfEnabledItems = this.activeDashboards.filter(item => item.dragEnabled).map(item => item.id);
            this.activeDashboards.forEach(item => {
                item.dragEnabled = true;
                item.resizeEnabled = true;
            });
            if (this.options.api && this.options.api.optionsChanged) {
                this.options.api.optionsChanged();
            }

            if(this.labels.length > 0) {
                if(labelGridsterItem.rows === 0 ) {
                    this.activeDashboards.forEach((item, index) => {
                        if(index !== 0) {
                            item.y = item.y + 3;
                        }
                    });
                    if (this.options.api && this.options.api.optionsChanged) {
                        this.options.api.optionsChanged();
                    }
                    labelGridsterItem.y = 0;
                    labelGridsterItem.rows = 3;
                    if (this.options.api && this.options.api.optionsChanged) {
                        this.options.api.optionsChanged();
                    }
                }
            } else {
                if(labelGridsterItem.reportType === ItemType.LABELS) {
                    if(labelGridsterItem.rows === 3) {
                        this.activeDashboards.forEach((item, index) => {
                            if(index === 0) {
                                item.rows = 0;
                            }
                            item.y = item.y - 3;
                        });
                        if (this.options.api && this.options.api.optionsChanged) {
                            this.options.api.optionsChanged();
                        }
                    }
                }
            }


            this.activeDashboards.forEach(record => {
                const enabled = listOfEnabledItems.some(id => record.id === id);
                record.dragEnabled = enabled;
                record.resizeEnabled = enabled;
            });
            if (this.options.api && this.options.api.optionsChanged) {
                this.options.api.optionsChanged();
            }
        }
    
        ngOnDestroy(): void {
            this._unsubscribeAll.next();
            this._unsubscribeAll.complete();
            this._locationService.enableLocationSelector();
        }
    
        changeGridsterState(widget: any, general = true) {
            if(general) {
                widget.resizeEnabled = !widget.resizeEnabled;
                widget.dragEnabled = !widget.dragEnabled;
            }
            if (this.options.api && this.options.api.optionsChanged) {
                this.options.api.optionsChanged();
            }
        }
            setForSystemSystemReport():void {
                this._reportMenuService.setItemObs(ItemType.SYSTEM_SYSTEM_DETAILCARD, this.resourceName);
            }
        
            setForSystemcpustatusReport():void {
                this._reportMenuService.setItemObs(ItemType.SYSTEMCPUSTATUS_DETAILCARD, this.resourceName);
            }
        
    
        onAddToReport(item: any) {
            const dialogRef = this._dialog.open(AddItemDialogComponent, {
                width: '300px',
                data: { title: 'Show in reports', item }
            });
        }
    
         onAddToDashboard(item: any) {
            const dialogRef = this._dialog.open(AddItemDashboardDialogComponent, {
                width: '300px',
                data: { title: 'Show in dashboards', item }
            });
        }
    
        customViewWidget(widget) {
            this._router.navigate(['systems', 'runtime-chart'], { queryParams: { chartLink: widget.chartLink } })
        }
    
        private loadRuntimeChart() {
            combineLatest(
                this.linksObj.map(link => this.httpClient.get<any>(link).pipe(map((res, index) => ({
                    ...res, ...{ id: uuid(), dragEnabled: false, resizeEnabled: false, x: 0, y: 0, rows: 20, cols: 48, chartLink: this.linksObj[index] }
                }))))
            ).pipe(takeUntil(this._unsubscribeAll)).subscribe((response: any[]) => {
                this.listOfCharts = response;
                if (this.items) {
                    this.items = this.items.concat(this.listOfCharts);
                }
            });
        }
    }    
