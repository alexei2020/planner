import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'revision',
    templateUrl: './revision.component.html',
    styleUrls: [
        './revision.component.scss'
    ]
})
export class RevisionComponent {
    public stringContent: string;

    @Input()
    creationTimestamp: Date;

    @Input()
    set content(value: object) {
        this.stringContent = value && typeof value === 'object' ?
            JSON.stringify(value, null, 2) :
            '';
    }

    @Input()
    revision: string;

    @Input()
    isAfter: boolean;

    public modes = ['Raw', 'Parsed'];
    public modeControl = new FormControl(this.modes[0]);
}
