import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil, first } from 'rxjs/operators';

import { BreadcrumbService } from 'app/main/shared/breadcrumb-bar.service';
import { FuseLocationService } from 'app/services/location.service';
import { AuditDetailHelperService, REVISION_ACTION, REVISION_STATE } from 'app/resource/audit-detail/services';

import { EndPointSetList } from '../../typings/backendapi.gen';
import { PageType } from 'app/main/shared/breadcrumb-bar.service.gen';

enum MODE {
	DIFF = 'DIFF',
	SELECT = 'SELECT'
}

@Component({
	selector: 'audit-detail-helper',
	templateUrl: './component.html',
	styleUrls: ['./component.scss']
})
export class AuditDetailPageHelperComponent implements OnInit, OnDestroy {
	private _unsubscribeAll: Subject<any> = new Subject();
	private _prevPath = '';
	private _backLocation: string;
	private _requestUri: string;

	public MODE = MODE;

	public revisions: EndPointSetList;
	public allRevisions: EndPointSetList;
	public currentRevisions: EndPointSetList;
	public resourceName: string;
	public currentLocation: any;
	public namespace: string;
	public versionAfter: string;

	public mode: MODE = MODE.SELECT;

	public left = '';
	public right = '';

	public loading = false;

	constructor(
		private _breadcrumbService: BreadcrumbService,
		private _activatedRoute: ActivatedRoute,
		private _router: Router,
		private _locationService: FuseLocationService,
		private _auditDetailService: AuditDetailHelperService
	) { }

	ngOnInit(): void {
		this._locationService.disableLocationSelector();
		this._breadcrumbService.platformSelected.next(null);
		this._breadcrumbService.currentPage.next(PageType.AUDIT_DETAIL);

		this._activatedRoute.params
			.pipe(takeUntil(this._unsubscribeAll))
			.subscribe((params) => {
				const { loc, backLocation, namespace } = this._activatedRoute.snapshot.queryParams;

				if (backLocation) {
					this._backLocation = backLocation;
					this._prevPath = this._router.url.split("?")[0].replace("/" + this.resourceName, "");
				}
				this.resourceName = params.resourceName;
				this.currentLocation = this._locationService.locationDataAsKeyValue().currentLocation
				this.namespace = namespace;
				this.loadaudit();
			});

		this._auditDetailService.revisionAction$
			.pipe(takeUntil(this._unsubscribeAll))
			.subscribe(({ type, value }) => {
				switch (type) {
					case REVISION_ACTION.CARDS:
						this.mode = value ? MODE.SELECT : MODE.DIFF;
						break;

					case REVISION_ACTION.ALL:
						if (value) {
							if (!this.allRevisions) {
								this.loadRevisions();
							} else {
								this.revisions = this.allRevisions;
								this.mode = MODE.SELECT;
							}
						} else {
							this.revisions = this.currentRevisions;
							this.mode = MODE.SELECT;
						}
						break;
				}
			});
	}


	loadaudit(): void {
		this.loading = true;
		this._auditDetailService.load(this.resourceName, this.namespace, this.currentLocation)
			.pipe(first())
			.subscribe(
				(response) => {
					const {
						status: {
							request_uri,
							version_after,
							version_before
						}
					} = response;
					this.versionAfter = version_after;
					this._requestUri = request_uri.replace('apis', 'history');
					this.loadRevisions([version_after, version_before]);
				},
				() => {
					console.log('some error');
					this.loading = false;
				}
			);
	}

	loadRevisions(versions: string[] = []) {
		this.loading = true;
		this._auditDetailService.revisionState$.next(REVISION_STATE.LOADING);

		const versionString = versions.filter(v => !!v).join(':');

		this._auditDetailService.loadRevisions(this._requestUri, versionString)
			.pipe(first())
			.subscribe((revisions) => {
				this.loading = false;
				this._auditDetailService.revisionState$.next(REVISION_STATE.ALL_LOADED);

				if (!revisions || !revisions.items || !revisions.items.length) {
					if (!versions) {
						this._auditDetailService.revisionState$.next(REVISION_STATE.ERROR);
					}
					return;
				}
				this.revisions = revisions;

				if (versions.length) {
					const [left, right] = revisions.items;
					this.left = left ? JSON.stringify(left, null, 2) : '';
					this.right = right ? JSON.stringify(right, null, 2) : '';
					this.currentRevisions = revisions;
				} else {
					this.allRevisions = revisions;
				}
			}, () => {
				this.loading = false;
				this._auditDetailService.revisionState$.next(REVISION_STATE.ERROR);
			});
	}

	onSelectCards(indexes: number[]) {
		const [left, right] = indexes;

		this.left = indexes.length ? JSON.stringify(this.revisions.items[left], null, 2) : '';
		this.right = indexes.length > 1 ? JSON.stringify(this.revisions.items[right], null, 2) : '';
		this._auditDetailService.revisionSelect$.next(indexes);
	}

	viewCard(indexes: number[]) {
		this.onSelectCards(indexes);
		this._auditDetailService.revisionShow$.next();
	}

	ngOnDestroy(): void {
		const nextPath = this._router.url.split("?")[0];

		// if (this._backLocation && (nextPath === this._prevPath)) {
		// 	this._router.navigate([], { 
		// 		relativeTo: this._activatedRoute, 
		// 		queryParams: { 
		// 			tenant: this._locationService.locationDataAsKeyValue().tenantName,
		// 			loc: this._locationService.convertAPIGroup2Name(this._backLocation)
		// 		},
		// 		replaceUrl: true
		// 	});
		// }
		this._unsubscribeAll.next();
		this._unsubscribeAll.complete();
		this._locationService.enableLocationSelector();
	}
}
